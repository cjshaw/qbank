from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from repository import views
from django.views.decorators.cache import cache_page

urlpatterns = patterns('',
    url(r'^$',
        views.RepositoryService.as_view()),
    url(r'^repositories/?$',
        views.RepositoriesList.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/?$',
        views.RepositoryDetails.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/assets/?$',
        views.RepositoryAssetsList.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/assets/(?P<asset_id>[-.:@%\d\w]+)/?$',
        views.RepositoryAssetDetails.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/assets/(?P<asset_id>[-.:@%\d\w]+)/url/?$',
        cache_page(60 * 60)(views.RepositoryAssetURL.as_view())),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/compositions/?$',
        views.RepositoryCompositionsList.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/compositions/(?P<composition_id>[-.:@%\d\w]+)/?$',
        views.RepositoryCompositionDetails.as_view()),
    url(r'^repositories/(?P<repository_id>[-.:@%\d\w]+)/compositions/(?P<composition_id>[-.:@%\d\w]+)/assets/?$',
        views.RepositoryCompositionAssetsList.as_view()),
    url(r'^hierarchies/roots/?$',
        views.RepositoryHierarchiesRootsList.as_view()),
    url(r'^hierarchies/roots/(?P<repository_id>[-.:@%\d\w]+)/?$',
        views.RepositoryHierarchiesRootDetails.as_view()),
    url(r'^hierarchies/nodes/(?P<repository_id>[-.:@%\d\w]+)/?$',
        views.RepositoryHierarchiesNodeDetails.as_view()),
    url(r'^hierarchies/nodes/(?P<repository_id>[-.:@%\d\w]+)/children/?$',
        views.RepositoryHierarchiesNodeChildrenList.as_view()),

    url(r'^docs/?$',
        views.Documentation.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)
