from bson.errors import InvalidId

from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.db import IntegrityError

from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.permissions import AllowAny

from dlkit.runtime.errors import PermissionDenied, InvalidArgument, IllegalState,\
    NotFound, NoAccess
from dlkit.runtime.primordium import Id, Type
from dlkit.records.registry import COMPOSITION_GENUS_TYPES, COMPOSITION_RECORD_TYPES

EDX_COMPOSITION_GENUS_TYPES_STR = [str(Type(**genus_type))
                                   for k, genus_type in COMPOSITION_GENUS_TYPES.iteritems()]

from qbank.views import DLKitSessionsManager
from utilities import assessment as autils
from utilities import general as gutils
from utilities import repository as rutils


EDX_COMPOSITION_RECORD_TYPE = Type(**COMPOSITION_RECORD_TYPES['edx-composition'])

class Documentation(DLKitSessionsManager):
    """
    Shows the user documentation for talking to the RESTful service
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        return render_to_response('repository/documentation.html',
                                  {},
                                  RequestContext(request))


class RepositoriesList(DLKitSessionsManager):
    """
    List all available repositories.
    api/v2/repository/repositories/

    POST allows you to create a new repository, requires two parameters:
      * name
      * description

    Alternatively, if you provide an assessment repository ID,
    the repository will be orchestrated to have a matching internal identifier.
    The name and description will be set for you.
      * repositoryId
      * name (optional)
      * description (optional)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"name" : "a new repository",
       "description" : "this is a test"}

       OR
       {"repositoryId": "assessment.Bank%3A5547c37cea061a6d3f0ffe71%40cs-macbook-pro"}
    """

    def get(self, request, format=None):
        """
        List all available repositories
        """
        try:
            if len(self.data) == 0:
                repositories = self.rm.repositories
            else:
                querier = self.rm.get_repository_query()

                allowable_query_terms = ['display_name', 'description']
                if any(term in self.data for term in allowable_query_terms):
                    querier = autils.config_osid_object_querier(querier, self.data)
                    repositories = self.rm.get_repositories_by_query(querier)
                else:
                    repositories = self.rm.repositories
            repositories = gutils.extract_items(request, repositories)
            return Response(repositories)
        except PermissionDenied:
            raise exceptions.AuthenticationFailed('Permission denied. You do not have '
                                                  'rights to view repositories.')

    def post(self, request, format=None):
        """
        Create a new repository, if authorized

        """
        try:
            data = gutils.get_data_from_request(request)

            if 'bankId' not in data:
                form = self.rm.get_repository_form_for_create([])
                gutils.verify_keys_present(data, ['name', 'description'])
                finalize_method = self.rm.create_repository
            else:
                repository = self.rm.get_repository(Id(data['bankId']))
                form = self.rm.get_repository_form_for_update(repository.ident)
                finalize_method = self.rm.update_repository

            if 'name' in data:
                form.display_name = data['name']
            if 'description' in data:
                form.description = data['description']

            new_repo = gutils.convert_dl_object(finalize_method(form))

            return gutils.CreatedResponse(new_repo)
        except (PermissionDenied, InvalidArgument, NotFound, KeyError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryAssetDetails(DLKitSessionsManager):
    """
    Get asset details
    api/v2/repository/repositories/<repository_id>/assets/<asset_id>/

    GET, PUT, DELETE
    PUT to modify an existing asset (name or contents). Include only the changed parameters.
        If files are included, all current files are deleted / replaced.
    DELETE to remove from the repository.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "an updated item"}
    """

    def delete(self, request, repository_id, asset_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            # need to manually delete the asset contents
            asset = repository.get_asset(gutils.clean_id(asset_id))
            for asset_content in asset.get_asset_contents():
                repository.delete_asset_content(asset_content.ident)

            repository.delete_asset(gutils.clean_id(asset_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, IllegalState, InvalidId) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, repository_id, asset_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            asset = repository.get_asset(gutils.clean_id(asset_id))
            asset_map = rutils.update_asset_urls(repository, asset)

            asset_map.update({
                '_links': {
                    'self': gutils.build_safe_uri(request),
                }
            })

            return Response(asset_map)
        except (PermissionDenied, NotFound, InvalidId) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, repository_id, asset_id, format=None):
        try:
            gutils.verify_at_least_one_key_present(self.data,
                                                   ['name', 'description', 'files',
                                                    'assignedRepositoryIds', 'url'])

            if 'assignedRepositoryIds' in self.data:
                gutils.verify_min_length(self.data, ['assignedRepositoryIds'], 1)

            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            original_asset = repository.get_asset(gutils.clean_id(asset_id))

            if 'files' in self.data:
                # delete current assets
                for asset_content in original_asset.get_asset_contents():
                    repository.delete_asset_content(asset_content.ident)

                # create the new contents
                for asset in self.data['files'].items():
                    rutils.attach_asset_content_to_asset({
                        'asset': original_asset,
                        'data': asset[1],
                        'repository': repository
                    })

            if 'url' in self.data:
                gutils.verify_keys_present(self.data, ['url', 'assetContentId'])
                ac_form = repository.get_asset_content_form_for_update(gutils.clean_id(self.data['assetContentId']))
                ac_form = gutils.set_form_basics(ac_form, self.data)
                ac_form.set_url(str(self.data['url']))
                repository.update_asset_content(ac_form)

            if 'name' in self.data or 'description' in self.data:
                form = repository.get_asset_form_for_update(gutils.clean_id(asset_id))

                if 'name' in self.data:
                    form.display_name = self.data['name']
                if 'description' in self.data:
                    form.description = self.data['description']

                repository.update_asset(form)

            if 'assignedRepositoryIds' in self.data:
                current_repository_ids = self.rm.get_repository_ids_by_asset(original_asset.ident)
                current_repository_ids = [str(i) for i in current_repository_ids]
                new_repository_ids = self.data['assignedRepositoryIds']
                # need to preserve the order of the provided repositoryIds...

                # add any new repositories
                for repo_id in new_repository_ids:
                    if repo_id not in current_repository_ids:
                        self.rm.assign_asset_to_repository(original_asset.ident,
                                                           gutils.clean_id(repo_id))

                # remove any old repositories that aren't in the new_repository_ids list
                updated_repository_ids = self.rm.get_repository_ids_by_asset(original_asset.ident)
                updated_repository_ids = [str(i) for i in updated_repository_ids]
                for repo_id in updated_repository_ids:
                    if repo_id not in new_repository_ids:
                        self.rm.unassign_asset_from_repository(original_asset.ident,
                                                               gutils.clean_id(repo_id))

                # now both list contents should match, but they  may be in the wrong order

                while rutils.asset_repository_ids_not_in_order(self.rm,
                                                               original_asset.ident,
                                                               self.data['assignedRepositoryIds']):
                    rutils.sort_asset_repository_ids(self.rm,
                                                     original_asset.ident,
                                                     self.data['assignedRepositoryIds'])

                # need to update the repository, in case it changed
                repository = self.rm.get_repositories_by_asset(original_asset.ident).next()

            asset = repository.get_asset(gutils.clean_id(asset_id))

            data = gutils.convert_dl_object(asset)

            return gutils.UpdatedResponse(data)
        except (PermissionDenied, InvalidArgument, NoAccess, InvalidId, KeyError,
                TypeError, IntegrityError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryAssetsList(DLKitSessionsManager):
    """
    Get or add assets to a repository
    api/v2/repository/repositories/<repository_id>/assets/

    GET, POST
    GET to view current assets
    POST to create a new asset

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"assetLabel" : <file object>}
    """

    def get(self, request, repository_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            assets = repository.get_assets()
            data = gutils.extract_items(request, assets)

            # need to replace each URL here with CloudFront URL...
            for asset in data['data']['results']:
                asset = rutils.update_asset_urls(repository, asset)

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, repository_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            if 'enclosedObjectId' in self.data:
                enclosed_object_id = gutils.clean_id(self.data['enclosedObjectId'])
                if enclosed_object_id.get_identifier_namespace() != 'repository.Asset':
                    if enclosed_object_id.get_authority() != repository._authority:
                        raise InvalidArgument()
                    else:
                        gutils.verify_keys_present(self.data, ['providerId'])
                        form = repository.get_asset_form_for_update(enclosed_object_id)
                        form.set_provider(gutils.clean_id(self.data['providerId']))
                        asset = repository.update_asset(form)
                        return_data = asset.object_map
                else:
                    raise InvalidArgument()
            elif 'url' in self.data:
                gutils.verify_keys_present(self.data, ['url', 'name'])

                form = repository.get_asset_form_for_create([])
                form = gutils.set_form_basics(form, self.data)
                asset = repository.create_asset(form)

                ac_form = repository.get_asset_content_form_for_create(asset.ident, [])
                ac_form = gutils.set_form_basics(ac_form, self.data)
                ac_form.set_url(str(self.data['url']))
                repository.create_asset_content(ac_form)
                return_data = gutils.convert_dl_object(repository.get_asset(asset.ident))
            else:
                gutils.verify_keys_present(self.data, ['files'])

                return_data = {}
                for asset in self.data['files'].items():
                    # asset should be a tuple of (asset_label, asset_file_object)
                    created_asset = rutils.create_asset(repository, asset)
                    return_data[created_asset[0]] = created_asset[1]  # (asset_label: asset_id)

            return gutils.CreatedResponse(return_data)
        except (PermissionDenied, InvalidArgument, KeyError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryAssetURL(DLKitSessionsManager):
    """
    Get the first asset content url (i.e. image) for the given ID
    api/v2/repository/repositories/<repository_id>/assets/<asset_id>/url

    GET returns a 302 redirect to a signed CloudFront URL
    """
    def get(self, request, repository_id, asset_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            asset = repository.get_asset(gutils.clean_id(asset_id))
            return HttpResponseRedirect(asset.get_asset_contents().next().get_url())
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class RepositoryCompositionAssetsList(DLKitSessionsManager):
    """
    Get or add assets to a repository
    api/v2/repository/repositories/<repository_id>/compositions/<composition_id>/assets/

    GET, PUT
    GET to view a composition's current assets
    PUT to append one or more assets to the composition. The assets / assessments must already
        exist elsewhere in the system.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"assetIds" : ["asset:1@MIT", "assessment:25@MIT"]}
    """

    def get(self, request, repository_id, composition_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            try:
                assets = repository.get_composition_assets(gutils.clean_id(composition_id))
            except NotFound:
                assets = []

            data = gutils.extract_items(request, assets)
            # need to replace each URL here with CloudFront URL...
            for asset in data['data']['results']:
                asset_repo = rutils.get_object_repository(self.rm,
                                                          asset['id'],
                                                          'asset')
                asset = rutils.update_asset_urls(asset_repo, asset)

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, repository_id, composition_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            data = gutils.get_data_from_request(request)

            gutils.verify_keys_present(data, ['assetIds'])

            # remove current assets first, if they exist
            try:
                for asset in repository.get_composition_assets(Id(composition_id)):
                    repository.remove_asset(asset.ident, Id(composition_id))
            except NotFound:
                pass

            if not isinstance(data['assetIds'], list):
                data['assetIds'] = [data['assetIds']]

            for asset_id in data['assetIds']:
                repository.add_asset(Id(asset_id), Id(composition_id))

            return gutils.UpdatedResponse()
        except (PermissionDenied, InvalidArgument, KeyError) as ex:
            gutils.handle_exceptions(ex)



class RepositoryCompositionDetails(DLKitSessionsManager):
    """
    Get asset details
    api/v2/repository/repositories/<repository_id>/compositions/<composition_id>/

    GET, PUT, DELETE
    PUT to modify an existing composition (name, description, or children).
        Include only the changed parameters.
        If children are included, all current ones are deleted / replaced.
        The order in which children are provided persists, so to change the
        order, PUT them back in the desired order.
    DELETE to remove from the repository. Does NOT remove the children objects.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "an updated composition",
        "childIds": ["asset1", "asset2"]}
    """

    def delete(self, request, repository_id, composition_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            repository.delete_composition(gutils.clean_id(composition_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, IllegalState, InvalidId) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, repository_id, composition_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            composition = repository.get_composition(gutils.clean_id(composition_id))
            composition_map = composition.object_map

            composition_map.update({
                '_links': {
                    'self': gutils.build_safe_uri(request),
                    'assets': gutils.build_safe_uri(request) + 'assets/'
                }
            })

            return Response(composition_map)
        except (PermissionDenied, NotFound, InvalidId) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, repository_id, composition_id, format=None):
        try:
            data = gutils.get_data_from_request(request)

            gutils.verify_at_least_one_key_present(data,
                                                   ['name', 'description', 'childIds',
                                                    'startDate', 'endDate', 'visibleToStudents',
                                                    'draft'])

            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            form = repository.get_composition_form_for_update(gutils.clean_id(composition_id))
            if 'childIds' in data:
                form.clear_children()
                data['childIds'] = rutils.convert_to_id_list(data['childIds'])
                form.set_children(data['childIds'])

            if 'name' in data:
                form.display_name = data['name']

            if 'description' in data:
                form.description = data['description']

            composition = repository.get_composition(gutils.clean_id(composition_id))

            if str(composition.genus_type) in EDX_COMPOSITION_GENUS_TYPES_STR:
                if 'startDate' in data:
                    form = rutils.update_edx_composition_date(form, 'start', data['startDate'])

                if 'endDate' in data:
                    form = rutils.update_edx_composition_date(form, 'end', data['endDate'])

                if 'visibleToStudents' in data:
                    form = rutils.update_edx_composition_boolean(form,
                                                                 'visible_to_students',
                                                                 bool(data['visibleToStudents']))

                if 'draft' in data:
                    form = rutils.update_edx_composition_boolean(form,
                                                                 'draft',
                                                                 bool(data['draft']))

            composition = repository.update_composition(form)

            return gutils.UpdatedResponse(composition.object_map)
        except (PermissionDenied, InvalidArgument, InvalidId, KeyError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryCompositionsList(DLKitSessionsManager):
    """
    Get or add compositions to a repository
    api/v2/repository/repositories/<repository_id>/compositions/

    GET, POST
    GET to view current compositions; can filter by type, i.e. ?course&vertical
    POST to create a new composition

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name": "a vertical",
        "description": "high",
        "childIds": ["abc@1:MIT"]}
    """

    def get(self, request, repository_id, format=None):
        try:
            params = gutils.get_data_from_request(request)

            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            repository.use_federated_repository_view()

            query_results = []
            if len(params) > 0:
                gutils.verify_at_least_one_key_present(params, ['course', 'chapter', 'sequential',
                                                                'split_test', 'vertical', 'page'])
            try:
                for genus_val, empty in params.iteritems():
                    if genus_val != 'page':
                        query_results.append(
                            repository.get_compositions_by_genus_type(
                                str(Type(**COMPOSITION_GENUS_TYPES[genus_val]))))
            except KeyError:
                raise IllegalState('Invalid query genus type provided. Only "course", ' +
                                   '"chapter", "sequential", "split_test", and "vertical" ' +
                                   'are allowed.')

            if len(query_results) > 0:
                compositions = []
                for results in query_results:
                    compositions += list(results)
            else:
                compositions = repository.get_compositions()
            data = gutils.extract_items(request, compositions)

            return Response(data)
        except (PermissionDenied, NotFound, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, repository_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))

            data = gutils.get_data_from_request(request)

            gutils.verify_keys_present(data, ['name', 'description'])

            if 'type' in data and 'edx' in data['type']:
                form = repository.get_composition_form_for_create([EDX_COMPOSITION_RECORD_TYPE])
                edx_type = data['type'].split('-')[-1]  # assumes type is edx-course, edx-vertical, etc.
                try:
                    form.set_genus_type(Type(**(COMPOSITION_GENUS_TYPES[edx_type])))

                    if 'startDate' in data:
                        form = rutils.update_edx_composition_date(form, 'start', data['startDate'])

                    if 'endDate' in data:
                        form = rutils.update_edx_composition_date(form, 'end', data['endDate'])

                    if 'visibleToStudents' in data:
                        form = rutils.update_edx_composition_boolean(form,
                                                                     'visible_to_students',
                                                                     bool(data['visibleToStudents']))

                    if 'draft' in data:
                        form = rutils.update_edx_composition_boolean(form,
                                                                     'draft',
                                                                     bool(data['draft']))

                except KeyError:
                    raise InvalidArgument('Bad genus type provided.')
            else:
                form = repository.get_composition_form_for_create([])
            form.display_name = data['name']
            form.description = data['description']

            if 'childIds' in data:
                data['childIds'] = rutils.convert_to_id_list(data['childIds'])
                form.set_children(data['childIds'])

            composition = repository.create_composition(form)
            return gutils.CreatedResponse(composition.object_map)
        except (PermissionDenied, InvalidArgument, IllegalState, KeyError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryDetails(DLKitSessionsManager):
    """
    Shows details for a specific repository.
    api/v2/repository/repositories/<repository_id>/

    GET, PUT, DELETE
    PUT will update the repository. Only changed attributes need to be sent.
    DELETE will remove the repository.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "a new repository"}
    """
    def delete(self, request, repository_id, format=None):
        try:
            self.rm.delete_repository(gutils.clean_id(repository_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, NotFound, InvalidId) as ex:
            gutils.handle_exceptions(ex)
        except IllegalState as ex:
            modified_ex = type(ex)('Repository is not empty.')
            gutils.handle_exceptions(modified_ex)

    def get(self, request, repository_id, format=None):
        try:
            repository = self.rm.get_repository(gutils.clean_id(repository_id))
            repository = gutils.convert_dl_object(repository)
            repository = gutils.add_links(request,
                                          repository,
                                          {
                                              'assets': 'assets/',
                                              'compositions': 'compositions/'
                                          })
            return Response(repository)
        except (PermissionDenied, InvalidId, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, repository_id, format=None):
        try:
            form = self.rm.get_repository_form_for_update(gutils.clean_id(repository_id))

            data = gutils.get_data_from_request(request)

            gutils.verify_at_least_one_key_present(data, ['name', 'description'])

            # should work for a form or json data
            if 'name' in data:
                form.display_name = data['name']
            if 'description' in data:
                form.description = data['description']

            updated_repository = self.rm.update_repository(form)
            updated_repository = gutils.convert_dl_object(updated_repository)
            updated_repository = gutils.add_links(request,
                                                  updated_repository,
                                                  {
                                                      'assets': 'assets/'
                                                  })

            return gutils.UpdatedResponse(updated_repository)
        except (PermissionDenied, KeyError, InvalidArgument, InvalidId, NotFound) as ex:
            gutils.handle_exceptions(ex)


class RepositoryHierarchiesNodeChildrenList(DLKitSessionsManager):
    """
    List the children for a root repository.
    api/v2/repository/hierarchies/nodes/<repository_id>/children/

    POST allows you to update the children list (bulk update)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"id": "assessment.Bank:54f9e39833bb7293e9da5b44@oki-dev.MIT.EDU"}

    """
    def get(self, request, repository_id, format=None):
        """
        List children of a node
        """
        try:
            nodes = self.rm.get_repository_nodes(gutils.clean_id(repository_id),
                                                 0, 1, False)
            data = gutils.extract_items(request, nodes.get_child_repository_nodes())
            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, repository_id, format=None):
        """
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            # first remove current child repositories, if present
            try:
                self.rm.remove_child_repositories(gutils.clean_id(repository_id))
            except NotFound:
                pass

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for child_id in self.data['ids']:
                child_repository = self.rm.get_repository(gutils.clean_id(child_id))
                self.rm.add_child_repository(gutils.clean_id(repository_id),
                                             child_repository.ident)
            return gutils.CreatedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)


class RepositoryHierarchiesNodeDetails(DLKitSessionsManager):
    """
    List the repository details for a node repository.
    api/v2/repository/hierarchies/nodes/<repository_id>/

    GET only. Can provide ?ancestors and ?descendants values to
              get nodes up and down the hierarchy.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
    """
    def get(self, request, repository_id, format=None):
        """
        List details of a node repository
        """
        try:
            if 'ancestors' in self.data:
                ancestor_levels = int(self.data['ancestors'])
            else:
                ancestor_levels = 0
            if 'descendants' in self.data:
                descendant_levels = int(self.data['descendants'])
            else:
                descendant_levels = 0
            include_siblings = False

            node_data = self.rm.get_repository_nodes(gutils.clean_id(repository_id),
                                                     ancestor_levels,
                                                     descendant_levels,
                                                     include_siblings)

            data = node_data.get_object_node_map()
            gutils.update_links(request, data)

            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)


class RepositoryHierarchiesRootsList(DLKitSessionsManager):
    """
    List all available assessment hierarchy root nodes.
    api/v2/assessment/hierarchies/roots/

    POST allows you to add an existing repository as a root repository in
    the hierarchy.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"id": "assessment.Bank:54f9e39833bb7293e9da5b44@oki-dev.MIT.EDU"}
    """
    def get(self, request, format=None):
        """
        List all available root assessment repositories
        """
        try:
            root_repositories = self.rm.get_root_repositories()
            repositories = gutils.extract_items(request, root_repositories)
            return Response(repositories)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, format=None):
        """
        Add a repository as a root to the hierarchy

        """
        try:
            gutils.verify_keys_present(self.data, ['id'])
            try:
                self.rm.get_repository(gutils.clean_id(self.data['id']))
            except:
                raise InvalidArgument()

            self.rm.add_root_repository(gutils.clean_id(self.data['id']))
            return gutils.CreatedResponse()
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class RepositoryHierarchiesRootDetails(DLKitSessionsManager):
    """
    List the repository details for a root repository. Allow you to remove it as a root
    api/v2/repository/hierarchies/roots/<repository_id>/

    DELETE allows you to remove a root repository.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
    """
    def delete(self, request, repository_id, format=None):
        """
        Remove repository as root repository
        """
        try:
            root_repository_ids = self.rm.get_root_repository_ids()
            if gutils.clean_id(repository_id) in root_repository_ids:
                self.rm.remove_root_repository(gutils.clean_id(repository_id))
            else:
                raise exceptions.NotAcceptable('That repository is not a root.')
            return gutils.DeletedResponse()
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, repository_id, format=None):
        """
        List details of a root repository
        """
        try:
            root_repository_ids = self.rm.get_root_repository_ids()
            if gutils.clean_id(repository_id) in root_repository_ids:
                repository = self.rm.get_repository(gutils.clean_id(repository_id))
            else:
                raise exceptions.NotAcceptable('That repository is not a root.')

            data = repository.object_map
            gutils.update_links(request, data)

            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)


class RepositoryService(DLKitSessionsManager):
    """
    List all available repository services.
    api/v2/repository/
    """

    def get(self, request, format=None):
        """
        List all available repository services. For now, just 'repositories'
        """
        data = {}
        data = gutils.add_links(request,
                                data,
                                {
                                    'repositories': 'repositories/',
                                    'documentation': 'docs/'
                                })
        return Response(data)

