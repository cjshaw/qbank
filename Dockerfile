FROM python:2.7.11

RUN mkdir -p /var/www/webapps/qbank
RUN mkdir /var/www/tmp
RUN touch /var/www/tmp/qbank.log
RUN mkdir -p /var/www/html/qbank
WORKDIR /var/www/webapps/qbank
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
RUN python manage.py collectstatic --noinput
CMD ["bash"]