// Filename: main.js

if (window.location.protocol === 'http:') {
    var unitySource = 'http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2';
} else {
    var unitySource = 'https://ssl-webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2';
}

require.config({
    baseUrl: '/static/js',
    paths: {
        'admin'                 : 'utilities/admin',
        'backbone'              : '//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min',
        'bootstrap'             : '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min',
        'bootstrap-dialog'      : 'vendor/bootstrap-dialog',
        'csrf'                  : 'vendor/csrf',
        'jquery'                : '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min',
        'jquery-migrate'        : '//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min',
        'jquery-select2'        : '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.6/select2.min',
        'jquery-ui-1-10'        : '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min',
        'tocify'                : 'vendor/jquery.tocify',
        'underscore'            : '//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash',
        'unity'                 : unitySource
    },
    shim: {
        'admin': {
            deps: ['jquery', 'underscore'],
            exports: 'Admin'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap-dialog': {
            deps: ['bootstrap','jquery'],
            exports: 'BootstrapDialog'
        },
        'csrf': {
            deps: ['jquery']
        },
        'jquery-migrate': {
            deps: ['jquery']
        },
        'jquery-select2': {
            deps: ['jquery']
        },
        'jquery-ui-1-10': {
            deps: ['jquery']
        },
        'tocify': {
            deps: ['jquery', 'jquery-ui-1-10']
        }
    }
});
require(['app'], function(App) {
    App.initialize();
    require(['views/assessments/documentation']);
    require(['views/lti/dashboard']);
});