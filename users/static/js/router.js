// Filename: router.js
define(['jquery',
        'underscore',
        'backbone',
        'views/navbar/navbar',
        'views/footer/footer',
        'views/users/manage'
], function ($, _, Backbone, NavbarView, FooterView, UserManagementView) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            ''                              : 'home',
            // Default
            '*actions'                      : 'defaultAction'
        }
    });

    var initialize = function () {
        var app_router = new AppRouter;
        app_router.on('route:home', function () {
            var userManagementView = new UserManagementView();
            userManagementView.render();
        });

        app_router.on('route:defaultAction', function (actions) {
            // no configured route
            console.log('No route: ' + actions);
        });


        Backbone.View.prototype.goTo = function (loc) {
            app_router.navigate(loc, {trigger: true});
        };

        var navView = new NavbarView();
        navView.render();

        var footerView = new FooterView();
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});