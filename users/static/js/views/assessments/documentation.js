//Filename: views/assessments/documentation.js

define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui-1-10',
    'tocify',
    'csrf'
], function ($, _, Backbone) {
    var AssessmentsDocumentationView = Backbone.View.extend({
        el: $('#docs_container'),
        initialize: function () {
            $('#docs_nav').tocify({
                theme       : 'bootstrap3',
                context     : '#docs_inner_wrapper',
                selectors   : 'h1,h2,h3'
            });
            return this;
        },
        events : {

        }
    });

    var instance = new AssessmentsDocumentationView();

    return AssessmentsDocumentationView;
});