//Filename: views/user/manage.js

define([
    'jquery',
    'underscore',
    'backbone',
    'admin',
    'text!templates/ortho3dLabel.html',
    'text!templates/correctAnswer.html',
    'text!templates/wrongAnswer.html',
    'csrf',
    'unity'
], function ($, _, Backbone, Admin,
             LabelTemplate, CorrectAnswerTemplate, WrongAnswerTemplate) {

    function convertToObject (input) {
        return JSON.parse(input.replace(/&quot;/g,'"'));
    }

    function fakeAnswer () {
        // Emulate what the Unity App will send back -- for testing
        // purposes only
        // F, S, T
        return JSON.stringify([0, 2, 3]);
    }

    var LTIDashboardView = Backbone.View.extend({
        el: $('#lti_container'),
        initialize: function () {
            var config = {
                height: 400,
                params: { enableDebugging:"0", logoimage: "/static/ortho3d/Gaspard.logo.png" }
            },
                u = new UnityObject2(config),
                clean_sourcedid;

            clean_sourcedid = sourcedid.replace(/&quot;/g,'"');

            // We can change this template depending on the type
            // of problem
            this.$el.html(_.template(LabelTemplate));

            try {
                // Init the view set images and question string
                $('#questionString').text(convertToObject(question)['text']);
                $('#frontViewImg').attr('src', frontView);
                $('#sideViewImg').attr('src', sideView);
                $('#topViewImg').attr('src', topView);
                $('.checkAnswer').show();
            } catch (e) {

            }
            u.initPlugin($("#unityPlayer")[0], "/static/ortho3d/Gaspard.unity3d");

            // Bind events here, since they don't seem to work in the
            // injected page via Backbone
            $(document).on('click', '.checkAnswer', function () {
                // Get answer and send to the server...wait for response on
                // correct / incorrect
                var answer = fakeAnswer();

                $.ajax({
                    url:    'submit/',
                    data: {
                        answer      : answer,
                        takenId     : takenId,
                        sourcedid   : clean_sourcedid
                    },
                    type: 'POST'
                }).success( function (data) {
                    if (data['correct']) {
                        $('.userFeedback').html(_.template(CorrectAnswerTemplate));
                        $('.returnLMS').show();
                    } else {
                        $('.userFeedback').html(_.template(WrongAnswerTemplate));
                    }
                }).error( function (xhr, status, message) {
                    Admin.updateStatus('Server error: ' + message);
                });
            });


            $(document).on('click', '.resetProblem', function () {
                // reset Unity somehow
                // TODO
                $(this).remove();
                $('.returnLMS').remove();
                // Create a new taken and replace the current one
                $.ajax({
                    url : 'retry/',
                    data : {
                        sourcedid : clean_sourcedid
                    }
                }).success(function (data) {
                    takenId = $.parseJSON(data)['id'];
                }).error(function (xhr, status, message) {
                    Admin.updateStatus('Server error: ' + message);
                });
            });

            $(document).on('click', '.returnLMS', function () {
                $.ajax({
                    url: 'return/',
                    data: {
                        sourcedid: clean_sourcedid
                    }
                }).success( function (data) {
                    document.location.href = data;
                }).error( function (xhr, status, message){
                    Admin.updateStatus('Server error: ' + message);
                });
            });

            return this;
        },
        events : {
            // Seems like you cannot use Backbone events inside an LTI-embedded
            // webpage...so stick these in with jQuery in the init()
        }
    });

    new LTIDashboardView();

    return LTIDashboardView;
});