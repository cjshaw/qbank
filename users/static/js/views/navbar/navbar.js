//Filename: views/navbar/navbar.js

define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/navbar.html'
], function ($, _, Backbone, NavbarTemplate) {
    var NavbarView = Backbone.View.extend({
        el: $('#navbar_wrapper'),
        initialize: function () {
            var currentPath = window.location.href,
                    isTouchstone = false,
                    isRelateTouchstone = false,
                    compiledTemplate;
            if (currentPath.indexOf('/touchstone/') >= 0) {
                isTouchstone = true;
            }
            if (currentPath.indexOf('/relate/touchstone/') >= 0) {
                isRelateTouchstone = true;
            }
            compiledTemplate = _.template(NavbarTemplate, {
                isTouchstone: isTouchstone,
                isRelateTouchstone: isRelateTouchstone});
            this.$el.html(compiledTemplate);
            return this;
        }
    });

    return NavbarView;
});