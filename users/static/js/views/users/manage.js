//Filename: views/user/manage.js

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap-dialog',
    'text!templates/userManagement.html',
    'text!templates/group.html',
    'text!templates/noGroup.html',
    'text!templates/noLTIConsumer.html',
    'text!templates/ltiConsumer.html',
    'text!templates/secret.html',
    'jquery-ui-1-10',
    'jquery-select2',
    'csrf'
], function ($, _, Backbone, BootstrapDialog,
             UserManagementTemplate, GroupTemplate, NoGroupTemplate,
             NoLTIConsumerTemplate, LTIConsumerTemplate, LTISecretTemplate) {

    // Make these "global" because we need to modify them
    // if anything changes, and make that accessible
    var allGroups;

    function activeUserExists () {
        if ($('.activeUser').length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function addGroupItem (id, name, role, groupId) {
        $('#' + id).append(_.template(GroupTemplate, {
            'groupId'   : groupId,
            'groupName' : name,
            'role'      : role
        }));
    }

    function addNewGroupToMasterList (newId, newName) {
        var existingIds = _.pluck(allGroups, 'groupId');
        if (existingIds.indexOf(parseInt(newId)) < 0) {
            allGroups.push({
                'groupId'   : parseInt(newId),
                'name'      : newName
            });
        }
    }

    function addUserToGroup (groupId, setStaff) {
        $.ajax({
            type: 'POST',
            url: "/users/groups/",
            data: {
                'groupId'   : groupId,
                'setStaff'  : setStaff,
                'username'  : getActiveUser()
            },
            success: function(response) {
                if (response['success']) {
                    var newId = response['groupId'],
                        newName = response['groupName'],
                        newRole = response['role'];
                    addGroupItem('groupList',
                        newName,
                        newRole,
                        newId);
                    updateStatus('User added to group: ' +
                        newName + '.');
                    removeNoGroup('groupList');
                    addNewGroupToMasterList(newId,
                        newName);
                    if (response['same_user']) {
                        removeNoGroup('myGroups');
                        addGroupItem('myGroups',
                            newName,
                            newRole,
                            newId);
                    }
                } else {
                    updateStatus(response['message']);
                }
            },
            error: function(xhr, status, error) {
                updateStatus('Server error: ' + error);
            }
        });
    }

    function clearStatusBoxes () {
        $('.statusBox').text('');
    }

    function compareGroupNames (a, b) {
        if (a.name < b.name) {
            return -1;
        } else if (a.name > b.name) {
            return 1;
        } else {
            return 0;
        }
    }

    function convertToObject (data) {
        return $.parseJSON(data.replace(/&quot;/g, '"'));
    }

    function createConsumer (newKey) {
        var $c = $('#ltiConsumerList'),
            $s = $('#secretList');
        $.ajax({
            type: 'POST',
            url: "/users/oauth/",
            data: {
                'key' : newKey
            }
        }).success(function(response) {
            if (response['success']) {
                $c.append(_.template(LTIConsumerTemplate, {
                    'consumerId'    : response['data']['id'],
                    'consumerKey'   : response['data']['key']
                }));
                $s.append(_.template(LTISecretTemplate, {
                    'consumerId'    : response['data']['id'],
                    'consumerSecret': response['data']['secret']
                }));
            } else {
                updateStatus(response['message']);
            }
        }).error(function(xhr, status, error) {
            updateStatus('Server error: ' + error);
        });
    }

    function deleteConsumer (ele) {
        var consumerId = $(ele).data('consumerid'),
            $e = $(ele),
            $s = $('#secretList');

        $.ajax({
            type: 'DELETE',
            url: "/users/oauth/" + consumerId
        }).success(function(response) {
            if (response['success']) {
                $e.remove();
                $s.find('.secretKeyItem')
                    .each(function() {
                        if ($(this).data('consumerid') === consumerId) {
                            $(this).remove();
                        }
                    });
            } else {
                updateStatus(response['message']);
            }
        }).error(function(xhr, status, error) {
            updateStatus('Server error: ' + error);
        });
    }

    function getActiveUser () {
        return $('li.activeUser').text();
    }

    function getGroupsForUser (username) {
        var groups;
        $.ajax({
            url: '/users/groups/',
            data: {
                'username'  : username
            }
        }).success( function (response) {
            if (response['success']) {
                groups = response['groups'];
                if (groups.length > 0) {
                    $('#groupList').empty();
                    _.each(groups, function (group) {
                        addGroupItem('groupList',
                            group['name'],
                            group['role'],
                            group['groupId']);
                    });
                } else {
                    $('#groupList').empty()
                        .append(_.template(NoGroupTemplate));
                }
            }
        }).error(function (xhqr, status, message) {
            updateStatus('Server error: ' + message);
        });
    }

    function initConsumerKeys () {
        var consumers, $c, $s;

        $c = $('#ltiConsumerList');
        $s = $('#secretList');

        $c.empty();
        $s.empty();
        $.ajax({
            url: '/users/oauth/'
        }).success( function (response) {
            if (response['success']) {
                consumers = response['data'];
                if (consumers.length > 0) {
                    _.each(consumers, function (consumer) {
                        $c.append(_.template(LTIConsumerTemplate, {
                            'consumerId'    : consumer.id,
                            'consumerKey'  : consumer.key
                        }));
                        $s.append(_.template(LTISecretTemplate, {
                            'consumerId'    : consumer.id,
                            'consumerSecret': consumer.secret
                        }))
                    });
                } else {
                    $c.append(_.template(NoLTIConsumerTemplate));
                }
            }
        }).error(function (xhqr, status, message) {
            updateStatus('Server error: ' + message);
        });
    }

    function makeActiveUser ($u) {
        $u.addClass('activeUser');
        $u.siblings('li')
            .removeClass('activeUser');
    }

    function removeGroupRoleFromList (id, role, groupId) {
        // Cycle through all the groups / roles until
        // you find the specified one, remove that row
        var $ele = $('#' + id),
            items, toRemove = [];
        items = $ele.find('li.groupItem');
        if (items.length > 0) {
            _.each(items, function (item) {
                if ($(item).data('role') == role &&
                    $(item).data('groupid') == groupId) {
                    toRemove.push(item);
                }
            });
            _.each(toRemove, function (item) {
                $(item).remove();
            });
        }
        // add a NoGroup if necessary
        items = $ele.find('li');
        if (items.length === 0) {
            $ele.append(_.template(NoGroupTemplate));
        }
    }

    function removeNoGroup (id) {
        $('#' + id).find('li.emptyGroup')
            .remove();
    }

    function removeUserGroupWithRole ($row) {
        var $p = $row.parent(),
            role, groupId, currentUser, groupName;
        role = $p.data('role');
        groupId = $p.data('groupid');
        groupName = $p.text().trim();

        if ($p.parent().attr('id') == 'myGroups') {
            currentUser = null;
        } else {
            currentUser = getActiveUser();
        }

        $.ajax({
            type: 'POST',
            url: "/users/remove/",
            data: {
                'groupId'     : groupId,
                'role'        : role,
                'username'    : currentUser
            },
            success: function(response) {
                if (response['success']) {
                    if ($p.siblings().length === 0) {
                        $p.parent()
                            .append(_.template(NoGroupTemplate));
                    }
                    $p.remove();
                    if (response['same_user'] && currentUser != null) {
                        // this means the user deleted their own
                        // group from the "Other Users" tab.
                        // Need to remove the group from the
                        // "myGroups" list, too.
                        removeGroupRoleFromList('myGroups', role, groupId);
                    }
                    if (response['me'] == getActiveUser() &&
                        currentUser == null) {
                        // this means the user deleted their own
                        // group from the My Info tab, but
                        // they are the highlighted user in the
                        // Other Users tab...so need to update
                        // their groups in the Other Users tab, too.
                        removeGroupRoleFromList('groupList', role, groupId);
                    }
                } else {
                    updateStatus(response['message']);
                }
            },
            error: function(xhr, status, error) {
                updateStatus('Server error: ' + error);
            }
        });
    }

    function requestNewSecretFor (ele) {
        var $e = $(ele),
            consumerId = $e.data('consumerid');
        $.ajax({
            type: 'PUT',
            url: "/users/oauth/" + consumerId,
            success: function(response) {
                if (response['success']) {
                    $e.replaceWith(_.template(LTISecretTemplate, {
                        'consumerId'    : response['data']['id'],
                        'consumerSecret': response['data']['secret']
                    }));
                } else {
                    updateStatus(response['message']);
                }
            },
            error: function(xhr, status, error) {
                updateStatus('Server error: ' + error);
            }
        });
    }

    function updateStatus (message, time) {
        time = typeof time !== 'undefined' ? time : 30000;
        $('.statusBox').html(message)
                .addClass('red');
        return setTimeout(clearStatusBoxes, time);
    }

//    function updateUserGroups (groupId, groupName, role) {
//        // Need to update the global userGroups, since that
//        // is used to show groups in the Other Users tab.
//        // Use getActiveUser() to find username.
//        // If groupId does not exist already, add it
//        // If groupId does exist already, remove it
//        var existingIds, userGroupList, tmpList, user;
//        user = getActiveUser();
//        userGroupList = userGroups[user];
//        existingIds = _.pluck(userGroupList, 'groupId');
//        if (existingIds.indexOf(parseInt(groupId)) < 0) {
//            // does not exist, so need to add this group
//            tmpList = userGroupList;
//            tmpList.push({
//                'groupId'   : parseInt(groupId),
//                'name' : groupName,
//                'role'      : role
//            })
//        } else {
//            // user already in this group, so remove them
//            tmpList = _.filter(userGroupLists, function (item) {
//                return item['groupId'] != parseInt(groupId);
//            });
//        }
//        userGroups[user] = tmpList;
//    }

    var UserManagementView = Backbone.View.extend({
        el: $('#container'),
        initialize: function () {
            var otherUsersPresent = false,
                compiledTemplate, users, myGroups;

            try {
                users = convertToObject(all_users);
                myGroups = convertToObject(my_groups);

                allGroups = convertToObject(all_groups);
            } catch (e) {
                console.log(e);
                users ={};
                myGroups = {};
                allGroups = {};
                my_public_key = '';
                my_private_key = '';
            }

            if (users.length > 0) {
                otherUsersPresent = true;
            }

            compiledTemplate = _.template(UserManagementTemplate, {
                title       : 'User Management',
                publicKey   : my_public_key,
                privateKey  : my_private_key,
                otherUsers  : otherUsersPresent});
            this.$el.html(compiledTemplate);

            // populate the data from groups / users
            // first do the user's own groups
            if (myGroups.length > 0) {
                _.each(myGroups, function(group){
                    addGroupItem('myGroups',
                        group['name'],
                        group['role'],
                        group['groupId']);
                });
            } else {
                $('#myGroups').append(_.template(NoGroupTemplate));
            }

            // next, populate the user table for other users
            _.each(users, function (user) {
                $('#userList').append('<li class="list-group-item otherUser pointer">' +
                    user + '</li>');
            });

            // initialize the events / jquery ui stuff
            $('#userProfileAccordion').accordion({
                autoHeight: false
            });

            return this;
        },
        events : {
            'click .addConsumer'        : 'addConsumerKey',
            'click .addGroup'           : 'addGroupForUser',
            'click #ltiConsumers'       : 'getOauthKeys',
            'click .otherUser'          : 'populateGroupsForUser',
            'click .regenKeys'          : 'regenerateKeys',
            'click .refreshSecret'      : 'refreshSecret',
            'click .refreshAllSecrets'  : 'refreshAllSecrets',
            'click .remove-consumer'    : 'removeConsumer',
            'click .remove-user'        : 'removeUserFromGroup'
        },
        addConsumerKey : function (e) {
            var newKey, existingKeys;

            existingKeys = _.map($('.ltiConsumer'), function (consumer) {
                return $(consumer).text().trim();
            });

            BootstrapDialog.show({
                title: 'Create LTI Consumer',
                message: '<div class="statusBox"></div>' +
                         '<div class="form-group">' +
                         '  <label for="keyName" class="sr-only">New Consumer Key</label>' +
                         '  <input class="form-control keyName" type="text" placeholder="New Consumer Key"/>' +
                         '</div>',
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-primary',
                    action: function (dialog) {
                        dialog.close();
                    }
                          },{
                    label: 'Create',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        newKey = $(dialog.$modalBody).find('input.keyName').val();
                        if (newKey !== '' && existingKeys.indexOf(newKey) < 0) {
                            createConsumer(newKey);
                            dialog.close();
                        } else {
                            updateStatus('Please enter a unique key.');
                        }
                    }
                }]
            });
        },
        addGroupForUser : function (e) {
            // bring up a dialog with existing groups as searchable...
            // or "enter a new group name" option
            var groups = allGroups,
                $t, groupNames, groupId, setStaff,
                searchTerm;
            if (activeUserExists()) {
                groupNames = _.pluck(groups, 'name');
                groups = _.map(groups, function (obj) {
                    return {
                        'id'    : obj['groupId'],
                        'text'  : obj['name']
                    };
                });
                BootstrapDialog.show({
                    title: 'Assign User to a Group',
                    message: '<div class="statusBox"></div>' +
                             '<input id="groupName" type="hidden" />' +
                             '<div class="form-group">' +
                             '  <label class="checkbox">' +
                             '    <input type="checkbox" id="setStaff"/>' +
                             '    Staff' +
                             '  </label>' +
                             '</div>',
                    onshow: function (dialog) {
                        $t = $(dialog.$modalBody).find('#groupName');
                        $t.select2({
                            placeholder: 'Select existing group or type in new name',
//                            createSearchChoice: function (term, data) {
//                                if (groupNames.indexOf(term) < 0) {
//                                    return {
//                                        'name'      : term,
//                                        'groupId'   : term,
//                                        'role'      : 'None'
//                                    };
//                                }
//                            },
                            id: function(group) {return group.groupId;},
                            ajax: {
                                url: '/users/groups/',
                                dataType: 'json',
                                data: function (term, page) {
                                    searchTerm = term.toLowerCase();
                                    return {
                                        q: term,
                                        page: page
                                    };
                                },
                                results: function(data) {
                                    var noneFound = false;
                                    if (data['success']) {
                                        var groups = data['groups'],
                                            counter = groups.length,
                                            filteredGroups = [],
                                            groupName;
                                        if (counter > 0) {
                                            $.each(groups, function(index, selectedGroup) {
                                                groupName = selectedGroup.name
                                                    .toLowerCase();
                                                if (groupName.indexOf(searchTerm) >= 0) {
                                                    filteredGroups.push(selectedGroup);
                                                }
                                            });
                                            filteredGroups.sort(compareGroupNames);
                                            return {results: filteredGroups};
                                        } else {
                                            noneFound = true;
                                        }
                                    } else {
                                        noneFound = true;
                                    }

                                    if (noneFound) {
                                        var tmp = [{
                                            'name'      : 'None Found',
                                            'groupId'   : 0,
                                            'role'      : 'None'
                                        }];
                                        return {results: tmp};
                                    }
                                }
                            },
                            formatResult: function(selectedGroup) {
                                return selectedGroup.name;
                            },
                            formatSelection: function(selectedGroup) {
                                return selectedGroup.name;
                            },
                            escapeMarkup: function(m) {
                                return m;
                            }
                        });
                    },
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn-primary',
                        action: function (dialog) {
                            dialog.close();
                        }
                              },{
                        label: 'Confirm',
                        cssClass: 'btn-success',
                        action: function (dialog) {
                            // send the group name / id back to the server
                            // server should create group if needed and link
                            // user to it
                            $t = $(dialog.$modalBody).find('#groupName');
                            groupId = $t.val();
                            if (groupId == '') {
                                updateStatus('Please type in a new group name and ' +
                                    'press enter, or select an existing group.');
                            } else {
                                setStaff = $(dialog.$modalBody).find('#setStaff')
                                    .prop('checked');
                                addUserToGroup(groupId, setStaff);
                                dialog.close();
                            }
                        }
                    }]
                });
            } else {
                updateStatus('Pick a user before adding a group.');
            }
        },
        getOauthKeys : function (e) {
            initConsumerKeys();
        },
        populateGroupsForUser : function (e) {
            var $u = $(e.currentTarget),
                groups, username;

            makeActiveUser($u);
            username = $u.text();
            getGroupsForUser(username);
        },
        regenerateKeys : function (e) {
            $.ajax({
                type: 'GET',
                url: "/users/newkeys/",
                success: function(response) {
                    if (response['success']) {
                        $('#publicKey').val(response['my_public_key']);
                        $('#privateKey').val(response['my_private_key']);
                        updateStatus('Keys updated.');
                    } else {
                        updateStatus(response['message']);
                    }
                },
                error: function(xhr, status, error) {
                    updateStatus('Server error: ' + error);
                }
            });
        },
        refreshSecret : function (e) {
            // only refresh the consumer secret key for this item
            var $t = $(e.currentTarget).parent();
            requestNewSecretFor($t);
        },
        refreshAllSecrets : function (e) {
            var secrets = $('.secretKeyItem');
            _.each(secrets, function (secret) {
                requestNewSecretFor(secret);
            });
        },
        removeConsumer : function (e) {
            var $e = $(e.currentTarget).parent()
            BootstrapDialog.show({
                title: 'Confirm Consumer Removal',
                message: 'Are you sure you want to remove this consumer?',
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-primary',
                    action: function (dialog) {
                        dialog.close();
                    }
                          },{
                    label: 'Yes!',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        deleteConsumer($e);
                        dialog.close();
                    }
                }]
            });
        },
        removeUserFromGroup : function (e) {
            var $row = $(e.currentTarget);
            BootstrapDialog.show({
                title: 'Confirm Group Removal',
                message: 'Are you sure you want to remove the user from ' +
                    'this role and group?',
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-primary',
                    action: function (dialog) {
                        dialog.close();
                    }
                          },{
                    label: 'Yes!',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        removeUserGroupWithRole($row);
                        dialog.close();
                    }
                }]
            });
        }
    });

    return UserManagementView;
});