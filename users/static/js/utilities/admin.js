// Administrative utility methods
// File: utilities/admin.js

define(['jquery',
        'underscore'],
    function ($, _) {

    var _admin = {};

    _admin.clearStatusBoxes = function () {
            $('.statusBox').text('');
        };

    _admin.getCookie = function (name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

    _admin.root = function () {
        var page, root;

        // Set the ROOT url, to access all other URLs
        page = document.location.href;

        if (page.indexOf('/touchstone/lti/') >= 0) {
            root = '/touchstone/lti/';
        } else if (page.indexOf('/lti/') >= 0) {
            root = '/lti/';
        } else if (page.indexOf('127.0.0.1') >= 0 ||
                page.indexOf('localhost') >= 0){
            root = '/';
        } else {
            root = '/lti/';
        }

        return root;
    };

    _admin.sendAjax = function (params, on_success, on_fail, on_always) {
        var method = 'GET';
        on_fail = typeof on_fail !== 'undefined' ? on_fail : null;
        on_always = typeof on_always !== 'undefined' ? on_always : null;

        if (params.hasOwnProperty('method')) {
            method = params['method'];
        }
        var data = {};
        if (params.hasOwnProperty('data')) {
            data = params['data'];
        }
        $.ajax({
            data: data,
            type: method,
            url: params['url']
        }).done(function (results) {
            on_success(results);
        }).fail(function(xhr, status, error) {
            if(on_fail) {on_fail();}
            _admin.updateStatus('Server error: ' + error);
        }).always(function(xhr, status, error) {
            if (on_always) {on_always();}
        });
    };

    _admin.setCSRFCookie = function (formId) {
        $('#' + formId).find('input[name="csrfmiddlewaretoken"]')
                .val(_admin.getCookie('csrftoken'));
    };

    _admin.updateStatus = function (msg, time) {
        time = typeof time !== 'undefined' ? time : 1500000;
        $('.statusBox').html(msg)
            .addClass('red');
        setTimeout(_admin.clearStatusBoxes, time);
    };

    return _admin;
});