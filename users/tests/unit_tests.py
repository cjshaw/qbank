# /users/test/
# User unit tests, for user management

import pdb
import logging

import requests

from django.test import TestCase
from django.http import HttpRequest
from django.core.urlresolvers import resolve
from django.template.loader import render_to_string

from ..models import APIUser

from django.contrib.auth.views import login as admin_login

class UserManagementTest(TestCase):
    fixtures = ['test_data.json']
    PUBLIC_KEY = APIUser.objects.get(username="tester").public_key
    PRIVATE_KEY = APIUser.objects.get(username="tester").private_key

    def test_unauthenticated_user_request_redirects_to_login(self):
        """
        An unauthenticated request should render the admin login page
        """
        found = resolve('/users/manage')
        self.assertEqual(found.func, admin_login)

    def test_unauthenticated_api_request_returns_bad_signature(self):
        """
        User should get a bad signature JSON if their signature doesn't match
        """
        self.fail('Finish writing the test!')

    def test_authenticated_asseessment_delete(self):
        """
        DELETE should do nothing here. Error code
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_post(self):
        """
        Not implemented, should return an error...what status code??
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_put(self):
        """
        Not implemented, should return an error...what status code??
        """
        self.fail('Finish writing the test!')

    def test_authenticated_bank_delete(self):
        """
        DELETE should do nothing on this page. Should get an error message
        """
        self.fail('Finish writing the test!')

    def test_authenticated_bank_get(self):
        """
        Should give you a list of banks and links
        """
        self.fail('Finish writing the test!')

    def test_authenticated_bank_post(self):
        """
        User can create a new assessment bank
        """
        self.fail('Finish writing the test!')

    def test_authenticated_bank_put(self):
        """
        PUT should do nothing on this page. Should get an error message
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessments_delete(self):
        """
        DELETE does nothing. Error code.
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessments_get(self):
        """
        Should give you a list of assessments and links
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessments_post(self):
        """
        User can create a new assessment
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessments_put(self):
        """
        PUT should do nothing on this page. Should get an error message
        """
        self.fail('Finish writing the test!')

    def test_authenticated_items_delete(self):
        """
        DELETE does nothing. Error code.
        """
        self.fail('Finish writing the test!')

    def test_authenticated_items_get(self):
        """
        Should give you a list of items and links
        """
        self.fail('Finish writing the test!')

    def test_authenticated_items_post(self):
        """
        User can create a new item
        """
        self.fail('Finish writing the test!')

    def test_authenticated_items_put(self):
        """
        PUT should do nothing on this page. Should get an error message
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_details_delete(self):
        """
        DELETE should remove the assessment from the repository
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_details_get(self):
        """
        Should give you a single assessment with its details
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_details_post(self):
        """
        POST should do nothing. Error code
        """
        self.fail('Finish writing the test!')

    def test_authenticated_assessment_details_put(self):
        """
        PUT should update the attributes of the assessment.
        Name, description, etc.
        """
        self.fail('Finish writing the test!')

    def test_authenticated_item_details_delete(self):
        """
        DELETE should remove the item from the repository
        """
        self.fail('Finish writing the test!')

    def test_authenticated_item_details_get(self):
        """
        Should give you a single item with its details
        """
        self.fail('Finish writing the test!')

    def test_authenticated_item_details_post(self):
        """
        POST should do nothing. Error code
        """
        self.fail('Finish writing the test!')

    def test_authenticated_item_details_put(self):
        """
        PUT should update the attributes of the item.
        Name, description, etc.
        """
        self.fail('Finish writing the test!')
