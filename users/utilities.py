import re
import json
import base64
import requests

from django.conf import settings
from django.db.models.query import QuerySet

from Crypto import Random


# create a class to handle communication to IS&T's Membership service
# this is essentially our Model backend
class Membership(object):

    def __init__(self):
        # get the group id for uuid /assessments
        # create the group if it doesn't exist
        self._url = settings.MEMBERSHIP
        self._cert = settings.CERT
        self._key = settings.KEY
        self._root_uuid = '/assessments'
        try:
            query_url = self._url + 'group?uuid=' + self._root_uuid
            group = self.send_request(query_url)[0]
            # if the group does not exist, it will throw an exception
            # because there is no JSON key at data['response']['docs']
            # if the group does exist, it will be the response index 0
            if isinstance(group, basestring):
                raise LookupError
        except:
            # create the group
            create_url = self._url + 'group'
            data = {
                'headers'   : {'content-type':'application/json'},
                'method'    : 'POST',
                'payload'   : {
                    "uuid"          : self._root_uuid,
                    "groupTemplate" : "department",
                    "name"          : "Assessments",
                    "longName"      : "MIT Assessments Service"
                }
            }
            group = self.send_request(create_url, data)[0]
        finally:
            self._group_id = group['id']
            # add Cole as DepartmentAdmin...otherwise you can do nothing?
            self.add_user_to_assessments('cjshaw@mit.edu', 'DepartmentAdmin')

    def add_user_to_assessments(self, user_email, role):
        """
        If not already in the project, add Learners and
        staff to the overall assessments project
        """
        my_groups = self.user_groups(user_email, full=True)
        is_member = False
        for group in my_groups:
            if (group['groupId'] == self._group_id and
                group['role'] == role):
                is_member = True
        if not is_member:
            self.add_user_to_group(user_email, self._group_id, role)

    def add_user_to_group(self, user_email, group_id, role):
        """
        Add the user with id user_id to group of group_id, with the given role
        Docs say that status = 1 is successful...but that seems wrong. 0 = ok, 1 = error?
        Also, for "Student" roles, I cannot create them--error msg says they must
        be created by  MITSIS
        So for "Learner" roles, must add them to parent group first.
        """
        create_user_url = self._url + 'group/' + str(group_id) + '/member'

        # translate roles for department level (view banks)
        if int(group_id) == self._group_id and role == 'Instructor':
            role = 'DepartmentAdmin'
        elif int(group_id) == self._group_id and role == 'Learner':
            role = 'DepartmentOfficer'
        data = {
            'headers'   : {'content-type':'application/json'},
            'method'    : 'POST',
            'payload'   : [{
                "accountEmail"  : user_email,
                "role"          : role
            }]
        }
        results = self.send_request(create_user_url, data)[0]
        if results['status'] == 0:
            return results
        else:
            raise Exception('User not added: ' + results['error'])

    def all_groups(self):
        """
        Query the IS&T Membership
        service for child groups of /assessments...assume that everything
        found under that is one of our groups (others can add / change things,
        if they are SysAdmins to the Membership Service!!)
        Note:
            * UUID for our groups is /assessments
        """
        child_query = self._url + 'group/' + str(self._group_id) + '/groups'
        all_groups = self.send_request(child_query)
        groups = [{
            'groupId'   : self._group_id,
            'longName'  : 'MIT Assessments Service',
            'name'      : 'Assessments'
        }]
        for item in all_groups:
            groups.append({
                'groupId'   : item['id'],
                'longName'  : item['longName'] if 'longName' in item else item['name'],
                'name'      : item['name']
            })
        return groups

    def clean_name(self, name):
        """
        make a name into an IS&T uuid-able string
        """
        return name.replace(' ','').lower()

    def create_group(self, name, longName=None, _uuid=None):
        """
        Create a child group under /assessments with the given name.
        Returns the groupId
        Use longName to match the Mongo assessments ID...
        """
        # Check to see if the group exists, first
        if _uuid:
            existing_url = self._url + 'group?uuid=' + self._root_uuid + '/' + _uuid
        else:
            existing_url = self._url + 'group?uuid=' + self._root_uuid + '/' + self.clean_name(name)
        try:
            existing_group = self.send_request(existing_url)
            error_msg = 'Could not get group with uuid ' + self._root_uuid
            if error_msg in existing_group:
                raise Exception
            if longName:
                # update the longName if it is provided
                update_longname_url = self._url + 'group/' + str(existing_group['id'])
                data = {
                    'headers'   : {'content-type':'application/json'},
                    'method'    : 'PUT',
                    'payload'   : {
                        'longName'  : longName
                    }
                }
                self.send_request(update_longname_url, data)
            return (existing_group['id'], name)
        except:
            create_group_url = self._url + 'group'

            if not longName:
                longName = name
            if not _uuid:
                raw_key = Random.get_random_bytes(4)
                random_uuid = base64.b64encode(raw_key)
                _uuid = self._root_uuid + '/' + re.sub(r'[^\w\d]', '', random_uuid)
            else:
                _uuid = self._root_uuid + '/' + _uuid

            data = {
                'headers'   : {'content-type':'application/json'},
                'method'    : 'POST',
                'payload'   : {
                    'name'              : name,
                    'longName'          : longName,
                    'groupTemplate'     : 'course',
                    'parentId'          : self._group_id,
                    'uuid'              : _uuid
                }
            }
            results = self.send_request(create_group_url, data)[0]
            return (results['id'], name)

    def delete_group(self, group_id):
        """
        remove the given group from Membership
        """
        delete_url = self._url + 'group/' + str(group_id)
        data = {
            'method': 'DELETE'
        }
        self.send_request(delete_url, data)

    def group_id(self, group_name):
        """
        get a groupId given the group name
        """
        group_url = self._url + 'group?uuid=' + group_name
        return self.send_request(group_url)[0]['id']

    def group_id_from_mc3id(self, mc3_id):
        """
        MC3 id will map to the longName of a group. Need to
        get all groups, then search for the longName match.
        Return groupId
        """
        from urllib import quote
        groups = self.all_groups()
        for group in groups:
            if group['longName'] == quote(mc3_id):
                return group['groupId']
        return None

    def group_name(self, group_id):
        """
        Get the group name of an id
        """
        group_url = self._url + 'group/' + str(group_id)
        return self.send_request(group_url)[0]['name']

    def remove_user_from_group(self, username, group_id, role):
        """
        Remove a specific user from the given group with the specific role
        """
        if role is not 'SysAdmin':
            get_user_id_url = self._url + 'user?accountId=' + username
            user_id = self.send_request(get_user_id_url)[0]['id']
            if (role == 'Instructor' and
                group_id == self._group_id):
                role = 'DepartmentAdmin'
            if (role == 'Learner' and
                group_id == self._group_id):
                role = 'DepartmentOfficer'

            remove_member_url = self._url + 'group/' + str(group_id) + \
                                '/member/' + str(user_id) + '/role/' + role
            data = {
                'method'    : 'DELETE'
            }
            results = self.send_request(remove_member_url, data=data, raw=True)
            if results['responseHeader']['status'] == 0:
                if results['success']['code'] == 0:
                    return True
                else:
                    return False
            else:
                return False

    def send_request(self, url, data=None, raw=None):
        """
        Send a request to the IS&T membership service
        Return the ['response']['docs'] object--typically a list.
        Strips out the unused metadata from the service
        """
        method = requests.get
        payload = None
        headers = None
        if data:
            if 'method' in data:
                method = getattr(requests, data['method'].lower())
            if 'headers' in data:
                headers = data['headers']
            if 'payload' in data:
                payload = json.dumps(data['payload'])
        response = method(url, cert=(self._cert, self._key),
                          data=payload, headers=headers)
        data = response.json()
        if raw:
            return data
        else:
            if data['responseHeader']['status'] == 0:
                try:
                    return data['response']['docs']
                except:
                    # updates do not send docs back
                    return data['success']['message']
            else:
                try:
                    return data['error']['message']
                except:
                    return data['response']['docs']

    def user_groups(self, username, full=None, admin=None):
        """
        Need to proxy the username with the IS&T Membership Service.
        Assumes that this app has SysAdmin privileges to the Service.
        Will need to filter out groups that are not in assessment!
        """
        groups_url = self._url + 'groups/all?proxyUser=' + username
        user_groups = self.send_request(groups_url)
        groups = []
        for item in user_groups:
            # if we want a full set of groups, we need to
            # return membership in the parent group for
            # the whole assessment service.
            # typically we only want the sub-groups,
            # which means the / will filter out the
            # main parent group.
            if item['role'] is not 'SysAdmin':
                if full:
                    search_term = self._root_uuid
                else:
                    search_term = self._root_uuid + '/'

                if 'longName' in item:
                    long_name = item['longName']
                else:
                    long_name = item['name']

                if search_term in item['uuid']:
                    # Need to filter out the auto-populate from Membership
                    # which makes all things filter down from
                    # department level (DepartmentOfficer, DepartmentAdmin)
                    if ((item['groupId'] != self._group_id) and
                        (item['role'] == 'DepartmentOfficer' or
                         item['role'] == 'DepartmentAdmin')):
                        if admin:
                            include = True
                        else:
                            include = False
                    else:
                        include = True
                    if include:
                        groups.append({
                            'groupId'   : item['groupId'],
                            'longName'  : long_name,
                            'name'      : item['name'],
                            'role'      : item['role']
                        })
        sorted_groups = sorted(groups, key=lambda k: k['name'])
        return sorted_groups

    def user_groups_mapping(self, full=False):
        """
        Get all users, then iterate through get_user_groups to find
        the groups for each user. Should return some sort of
        dict structure with username -> groups
        """
        all_users = get_all_users()
        results = {}
        for user in all_users:
            results[user] = self.user_groups(user, full=full)
        return results

    def user_id(self, username):
        """
        Get the Membership user id
        """
        user_exists_url = self._url + 'user?proxyName' + username
        user_account = self.send_request(user_exists_url)[0]
        return user_account['id']

def create_key(key_type):
    if key_type == 'public':
        length = 15
    elif key_type == 'private':
        length = 30
    else:
        raise KeyError('Invalid key type provided: ' + key_type)
    raw_key = Random.get_random_bytes(length)
    encoded = base64.b64encode(raw_key)
    return encoded

def create_new_key(user, key_type):
    """
    In following the AWS RESTful API standard, will create and assign a new
    public key to the user.
    Guidelines:
    http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html
    http://samritchie.net/2011/09/07/implementing-aws-authentication-for-your-own-rest-api/
    http://www.laurentluce.com/posts/python-and-cryptography-with-pycrypto/
    """
    encoded = create_key(key_type)
    if key_type == 'public':
        user.public_key = encoded
    else:
        user.private_key = encoded
    user.save()
    return encoded

def extract_oauth_consumers(consumers):
    """
    Return a list of objects: {id, name, secret}
    """
    if (not isinstance(consumers, list) and
        not isinstance(consumers, QuerySet)):
        consumers = [consumers]

    results = []
    for consumer in consumers:
        if consumer.secret == '':
            consumer.secret = create_key('private')
            consumer.save()
        results.append({
            'id'    : consumer.id,
            'key'   : consumer.key,
            'secret': consumer.secret
        })
    return results

def get_all_users():
    """
    Return all users in the app. Use internal Django model
    to keep track of this.
    """
    from assessments_users.models import APIUser
    all_users = APIUser.objects.filter(
            is_active=True,
            is_staff=True).values_list('username', flat=True)
    return [i for i in all_users]

def is_number(text):
    """
    test if the text is really a digit / number
    """
    try:
        int(text)
    except ValueError:
        return False
    else:
        return True

def log_error(module, ex):
    import logging
    template = "An exception of type {0} occurred in {1}. Arguments:\n{2!r}"
    message = template.format(type(ex).__name__, module, ex.args)
    logging.info(message)
    return message
