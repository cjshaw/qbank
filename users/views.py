import pdb
import json
import logging

from django.core.cache import cache
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.views.decorators.cache import cache_page
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test

from django.utils.http import urlquote_plus

from oauth_provider.models import Consumer

from .utilities import *

@login_required
@user_passes_test(lambda u: u.is_staff)
def groups(request):
    """
    Requires that users are logged in via Touchstone--only way to get here
    request.method == 'POST', add user to group
    request.method == 'DELETE', remove user from that group
    """
    try:
        membership = Membership()
        results = {}
        if request.method == 'POST' and request.user.is_superuser:
            group_id = request.POST['groupId']
            set_staff = json.loads(request.POST['setStaff'])
            user_email = request.POST.get('username', request.user.username)
            if set_staff:
                role = 'Instructor'
            else:
                # "Student" roles can only be created by MITSIS, per error message
                # "Learner" roles need to be in the parent group, too
                role = 'Learner'
                # membership.add_user_to_assessments(user_email, role)
            if is_number(group_id):
                # group exists...add user in the service
                # first need to query for the user ID in Membership
                # then use that plus groupId
                new_group_id = group_id
                new_group_name = membership.group_name(new_group_id)
            else:
                # create child group with name = group_id, then add user
                (new_group_id, new_group_name) = membership.create_group(group_id)
            results['message'] = membership.add_user_to_group(user_email, new_group_id, role)
            results['groupName'] = new_group_name
            results['role'] = role
            results['groupId'] = new_group_id

            # set a flag for if the session user is the same as the proxied
            # user. Used to update the UI for user management
            if user_email == request.user.username:
                results['same_user'] = True
            else:
                results['same_user'] = False
        elif request.method == 'DELETE' and request.user.is_superuser:
            pass
        elif request.method == 'GET' and request.user.is_superuser:
            # get groups for a username, passed in as parameter
            username = request.GET.get('username', None)
            if username:
                results['groups'] = membership.user_groups(username, full=True)
            else:
                # return all groups
                results['groups'] = membership.all_groups()
        else:
            raise Exception('Invalid request / you are not authorized.')
    except Exception as ex:
        results['message'] = log_error('users.views.groups()', ex)
        results['success'] = False
    else:
        results['success'] = True
    finally:
        return HttpResponse(json.dumps(results), content_type='application/json')

def login_user(request):
    """
    Log in the user...only allow if is a superuser
    """
    username = request.POST['username']
    pwd = request.POST['password']
    redirect_url = request.POST.get('next', reverse('users:manage_users'))
    if redirect_url == '':
        redirect_url = '/users/manage/'  #reverse('users:manage_users')

    user = authenticate(username=username, password=pwd)
    if user is not None:
        if user.is_active:
            login(request, user)
            if user.is_authenticated() and user.is_superuser:
                return HttpResponseRedirect(redirect_url)
            else:
                state = "Login error."
                logging.info('User not authenticated: ' + username)
        else:
            state = "Your account is not active."
            logging.info('Not an active user: ' + username)
    else:
        state = "Incorrect username and/or password."
        logging.info('Incorrect username / password: ' + username)

    return render_to_response('users/splash.html',
                              {'state' : state,'next':redirect_url},
                              RequestContext(request))

@login_required
@user_passes_test(lambda u: u.is_staff)
def manage_users(request):
    """
    Requires that users are logged in via Touchstone--only way to get here
    Each regular user should see their groups and their public / private key
        * They should be able to change their public / private key
        * They can un-subscribe from a group
        * They can request access to a group (e-mail should be sent to superusers)
    Superusers should see their own public / private key (no one else's), all users,
        and each user's groups.
        * Should be able to add new users
        * Should be able to assign users to groups
        * Should be able to remove users from groups
    Use the IS&T Membership service for the groups part?
    """
    if request.user.is_superuser:
        all_users = json.dumps(get_all_users())
        # user_groups = json.dumps(membership.user_groups_mapping())
        user_groups = []
    else:
        all_users = []
        user_groups = []

    if request.user.public_key:
        my_public_key = request.user.public_key
    else:
        my_public_key = create_new_key(request.user, 'public')

    if request.user.private_key:
        my_private_key = request.user.private_key
    else:
        my_private_key = create_new_key(request.user, 'private')
    # my_groups = json.dumps(membership.user_groups(request.user.username, full=True))
    # all_groups = json.dumps(membership.all_groups())
    my_groups = []
    all_groups = []
    return render_to_response('users/index.html', {
                                  'all_groups'    : all_groups,
                                  'all_users'     : all_users,
                                  'my_groups'     : my_groups,
                                  'my_private_key': my_private_key,
                                  'my_public_key' : my_public_key,
                                  'user_groups'   : user_groups
                              },
                              RequestContext(request))

@login_required
@user_passes_test(lambda u: u.is_superuser)
def oauth(request):
    """
    Allows users to do oauth management for consumers
    GET all OAuth consumers
    POST to create new consumer with given key, auto-generate / return secret
    """
    try:
        results = {}
        if request.method == 'POST':
            new_key = request.POST['key']
            new_consumer, created = Consumer.objects.get_or_create(key=new_key)
            new_consumer.secret = create_key('private')
            new_consumer.status = 2
            new_consumer.user = request.user
            new_consumer.save()
            results['data'] = extract_oauth_consumers([new_consumer])[0]
        elif request.method == 'GET':
            consumers = Consumer.objects.all()
            results['data'] = extract_oauth_consumers(consumers)
        else:
            raise Exception('Unsupported method!')
    except Exception as ex:
        results = {}
        results['message'] = log_error('users.views.oauth()', ex)
        results['success'] = False
    else:
        results['success'] = True
    finally:
        return HttpResponse(json.dumps(results), content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_superuser)
def oauth_consumer(request, consumer_id):
    """
    Allows users to do oauth management for a specific consumer
    GET this consumer's name and secret key
    PUT to regenerate the secret key
    DELETE to remove the consumer
    """
    try:
        results = {}
        consumer = Consumer.objects.get(pk=consumer_id)
        if request.method == 'PUT':
            consumer.secret = create_key('private')
            consumer.save()
            results['data'] = extract_oauth_consumers([consumer])[0]
        elif request.method == 'GET':
            results['data'] = extract_oauth_consumers([consumer])[0]
        elif request.method == 'DELETE':
            consumer.delete()
        else:
            raise Exception('Unsupported method!')
    except Exception as ex:
        results = {}
        results['message'] = log_error('users.views.oauth_consumer()', ex)
        results['success'] = False
    else:
        results['success'] = True
    finally:
        return HttpResponse(json.dumps(results), content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_staff)
def regen_keys(request):
    """
    Requires that users are logged in via Touchstone--only way to get here
    Create new public / private keys for the user. Save them in the
    database and return them to the browser.
    """
    try:
        results = {}
        results['my_public_key'] = create_new_key(request.user, 'public')
        results['my_private_key'] = create_new_key(request.user, 'private')
    except Exception as ex:
        results['message'] = log_error('users.views.regen_keys()', ex)
        results['success'] = False
    else:
        results['success'] = True
    finally:
        return HttpResponse(json.dumps(results), content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_staff)
def remove(request):
    """
    Remove a user from a specific group with a specific role
    """
    try:
        membership = Membership()
        results = {}
        if request.method == 'POST' and request.user.is_superuser:
            user_to_delete = request.POST.get('username', request.user.username)
            if user_to_delete == '':
                user_to_delete = request.user.username
            role = request.POST['role']
            group_id = request.POST['groupId']
            if membership.remove_user_from_group(user_to_delete, group_id, role):
                results['success'] = True
                # set a flag for if the session user is the same as the proxied
                # user. Used to update the UI for user management
                if user_to_delete == request.user.username:
                    results['same_user'] = True
                else:
                    results['same_user'] = False
                results['me'] = request.user.username
            else:
                raise Exception('Failed to remove ' + user_to_delete +
                                ' from group ' + group_id)
        else:
            raise Exception('Permission denied. You cannot remove users from groups.')
    except Exception as ex:
        results['message'] = log_error('users.views.remove()', ex)
        results['success'] = False
    else:
        results['success'] = True
    finally:
        return HttpResponse(json.dumps(results), content_type='application/json')

def splash(request):
    """
    Splash page that acts as a login.
    """

    return render_to_response('users/splash.html', {},
                              RequestContext(request))
