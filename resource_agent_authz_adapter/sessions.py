"""Resource-Agent AuthZ implementations of resource sessions."""

# pylint: disable=no-init
#     Numerous classes don't require __init__.
# pylint: disable=too-many-ancestors
#     Inheritance defined in specification

from dlkit.authz_adapter.resource import sessions
from dlkit.abstract_osid.osid import errors
#from dlkit.primordium.id.primitives import Id


class ResourceAdminSession(sessions.ResourceAdminSession):
    """This session creates, updates, and deletes ``Resources``.

    """
    def __init__(self, provider_session, authz_session, resource_agent_session, proxy=None):
        # yes, this is redundant, but needs to match expected args for authz_adapter q
        super(ResourceAdminSession, self).__init__(provider_session,
                                                   provider_session=provider_session,
                                                   authz_session=authz_session,
                                                   proxy=proxy)
        self._resource_agent_session = resource_agent_session
        self._id_namespace = 'resource.ResourceAgent'
        self._qualifier_id = provider_session.get_bin_id()

    def get_bin_id(self): # METHOD CAN BE REMOVED - LET AUTHZ_ADAPTER HANDLE THIS
        """Gets the ``Bin``  ``Id`` associated with this session.

        return: (osid.id.Id) - the ``Bin Id`` associated with this
                session
        *compliance: mandatory -- This method must be implemented.*

        """
        return self._provider_session.get_bin_id()

    bin_id = property(fget=get_bin_id)

    def get_bin(self): # METHOD CAN BE REMOVED - LET AUTHZ_ADAPTER HANDLE THIS
        """Gets the ``Bin`` associated with this session.

        return: (osid.resource.Bin) - the ``Bin`` associated with this
                session
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        return self._provider_session.get_bin()

    bin = property(fget=get_bin)

    def can_create_resources(self):
        """Tests if this user can create ``Resources``.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known creating a
        ``Resource`` will result in a ``PermissionDenied``. This is
        intended as a hint to an application that may opt not to offer
        create operations to an unauthorized user.

        return: (boolean) - ``false`` if ``Resource`` creation is not
                authorized, ``true`` otherwise
        *compliance: mandatory -- This method must be implemented.*

        Allow create if authz says yes, or if user does not have
        a resource already assigned.

        """
        ras = self._resource_agent_session
        agent_id = self.effective_agent_id
        resource_exists = False
        try:
            # hacked in for now:
            if ras.get_resource_by_agent(agent_id) is None:
                raise errors.NotFound
            resource_exists = True
        except errors.NotFound:
            resource_exists = False
        finally:
            if self._can('create') or not resource_exists:
                return True
            else:
                raise errors.PermissionDenied('You cannot create resources.')

    def can_create_resource_with_record_types(self, resource_record_types):
        """Tests if this user can create a single ``Resource`` using the desired record types.

        While ``ResourceManager.getResourceRecordTypes()`` can be used
        to examine which records are supported, this method tests which
        record(s) are required for creating a specific ``Resource``.
        Providing an empty array tests if a ``Resource`` can be created
        with no records.

        arg:    resource_record_types (osid.type.Type[]): array of
                resource record types
        return: (boolean) - ``true`` if ``Resource`` creation using the
                specified ``Types`` is supported, ``false`` otherwise
        raise:  NullArgument - ``resource_record_types`` is ``null``
        *compliance: mandatory -- This method must be implemented.*

        """
        if resource_record_types == None:
            raise errors.NullArgument()  # Just 'cause the spec says to :)
        else:
            return self._can('create')

    def get_resource_form_for_create(self, resource_record_types):
        """Gets the resource form for creating new resources.

        A new form should be requested for each create transaction.

        arg:    resource_record_types (osid.type.Type[]): array of
                resource record types
        return: (osid.resource.ResourceForm) - the resource form
        raise:  NullArgument - ``resource_record_types`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        raise:  Unsupported - unable to get form with requested record
                types
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_create_resources():
            return self._provider_session.get_resource_form_for_create(resource_record_types)
        else:
            raise errors.IllegalState()

    def create_resource(self, resource_form):
        """Creates a new ``Resource``.

        arg:    resource_form (osid.resource.ResourceForm): the form for
                this ``Resource``
        return: (osid.resource.Resource) - the new ``Resource``
        raise:  IllegalState - ``resource_form`` already used in a
                create transaction
        raise:  InvalidArgument - one or more of the form elements is
                invalid
        raise:  NullArgument - ``resource_form`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        raise:  Unsupported - ``resource_form`` did not originate from
                ``get_resource_form_for_create()``
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_create_resources():
            return self._provider_session.create_resource(resource_form)
        else:
            raise errors.IllegalState()

    def can_update_resources(self, resource_id=None):
        """Tests if this user can update ``Resources``.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known updating a
        ``Resource`` will result in a ``PermissionDenied``. This is
        intended as a hint to an application that may opt not to offer
        update operations to an unauthorized user.

        return: (boolean) - ``false`` if ``Resource`` modification is
                not authorized, ``true`` otherwise
        *compliance: mandatory -- This method must be implemented.*

        Allow update if authz says yes, or if optional resourceId
        matches the effectiveAgentId
        """
        if resource_id is None:
            return self._can('update')
        else:
            ras = self._resource_agent_session
            agent_id = self.effective_agent_id
            resource_matches_agent = False
            try:
                resource = ras.get_resource_by_agent(agent_id)
                if str(resource.ident) == str(resource_id):
                    resource_matches_agent = True
            except errors.NotFound:
                resource_matches_agent = False
            finally:
                if self._can('update') or resource_matches_agent:
                    return True
                else:
                    raise errors.PermissionDenied('You cannot update resources.')

    def get_resource_form_for_update(self, resource_id):
        """Gets the resource form for updating an existing resource.

        A new resource form should be requested for each update
        transaction.

        arg:    resource_id (osid.id.Id): the ``Id`` of the ``Resource``
        return: (osid.resource.ResourceForm) - the resource form
        raise:  NotFound - ``resource_id`` is not found
        raise:  NullArgument - ``resource_id`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_update_resources(resource_id):
            return self._provider_session.get_resource_form_for_update(resource_id)
        else:
            raise errors.IllegalState()

    def update_resource(self, resource_form):
        """Updates an existing resource.

        arg:    resource_form (osid.resource.ResourceForm): the form
                containing the elements to be updated
        raise:  IllegalState - ``resource_form`` already used in an
                update transaction
        raise:  InvalidArgument - the form contains an invalid value
        raise:  NullArgument - ``resource_form`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        raise:  Unsupported - ``resource_form`` did not originate from
                ``get_resource_form_for_update()``
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_update_resources(resource_form.ident):
            return self._provider_session.update_resource(resource_form)
        else:
            raise errors.IllegalState()

    def can_delete_resources(self, resource_id=None):
        """Tests if this user can delete ``Resources``.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known deleting a
        ``Resource`` will result in a ``PermissionDenied``. This is
        intended as a hint to an application that may opt not to offer
        delete operations to an unauthorized user.

        return: (boolean) - ``false`` if ``Resource`` deletion is not
                authorized, ``true`` otherwise
        *compliance: mandatory -- This method must be implemented.*

        """
        if resource_id is None:
            return self._can('delete')
        else:
            ras = self._resource_agent_session
            agent_id = self.effective_agent_id
            resource_matches_agent = False
            try:
                resource = ras.get_resource_by_agent(agent_id)
                if str(resource.ident) == str(resource_id):
                    resource_matches_agent = True
            except errors.NotFound:
                resource_matches_agent = False
            finally:
                if self._can('delete') or resource_matches_agent:
                    return True
                else:
                    raise errors.PermissionDenied('You cannot delete resources.')

    def delete_resource(self, resource_id):
        """Deletes a ``Resource``.

        arg:    resource_id (osid.id.Id): the ``Id`` of the ``Resource``
                to remove
        raise:  NotFound - ``resource_id`` not found
        raise:  NullArgument - ``resource_id`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_delete_resources(resource_id):
            self._provider_session.delete_resource(resource_id)
        else:
            raise errors.IllegalState()

    def can_manage_resource_aliases(self):
        """Tests if this user can manage ``Id`` aliases for ``Resources``.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known changing an alias
        will result in a ``PermissionDenied``. This is intended as a
        hint to an application that may opt not to offer alias
        operations to an unauthorized user.

        return: (boolean) - ``false`` if ``Resource`` aliasing is not
                authorized, ``true`` otherwise
        *compliance: mandatory -- This method must be implemented.*

        """
        raise errors.Unimplemented()

    def alias_resource(self, resource_id, alias_id):
        """Adds an ``Id`` to a ``Resource`` for the purpose of creating compatibility.

        The primary ``Id`` of the ``Resource`` is determined by the
        provider. The new ``Id`` performs as an alias to the primary
        ``Id``. If the alias is a pointer to another resource it is
        reassigned to the given resource ``Id``.

        arg:    resource_id (osid.id.Id): the ``Id`` of a ``Resource``
        arg:    alias_id (osid.id.Id): the alias ``Id``
        raise:  AlreadyExists - ``alias_id`` is already assigned
        raise:  NotFound - ``resource_id`` not found
        raise:  NullArgument - ``alias_id`` or ``resource_id`` is
                ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        raise errors.Unimplemented()


class ResourceAgentAssignmentSession(sessions.ResourceAgentAssignmentSession):
    """This session provides methods to re-assign ``Resource`` to ``Agents``.

    A ``Resource`` may be associated with multiple ``Agents``. An
    ``Agent`` may map to only one ``Resource``.

    """
    def __init__(self, provider_session, authz_session, resource_agent_session, proxy=None):
        super(ResourceAgentAssignmentSession, self).__init__(provider_session, authz_session, proxy)
        self._resource_agent_session = resource_agent_session
        self._id_namespace = 'resource.ResourceAgent'
        self._qualifier_id = provider_session.get_bin_id()

    def get_bin_id(self): # METHOD CAN BE REMOVED - LET AUTHZ_ADAPTER HANDLE THIS
        """Gets the ``Bin``  ``Id`` associated with this session.

        return: (osid.id.Id) - the ``Bin Id`` associated with this
                session
        *compliance: mandatory -- This method must be implemented.*

        """
        return self._provider_session.get_bin_id()

    bin_id = property(fget=get_bin_id)

    def get_bin(self): # METHOD CAN BE REMOVED - LET AUTHZ_ADAPTER HANDLE THIS
        """Gets the ``Bin`` associated with this session.

        return: (osid.resource.Bin) - the ``Bin`` associated with this
                session
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        return self._provider_session.get_bin()

    bin = property(fget=get_bin)

    def can_assign_agents(self):
        """Tests if this user can alter resource/agent mappings.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known mapping methods in
        this session will result in a ``PermissionDenied``. This is
        intended as a hint to an application that may opt not to offer
        assignment operations to unauthorized users.

        return: (boolean) - ``false`` if mapping is not authorized,
                ``true`` otherwise
        *compliance: mandatory -- This method must be implemented.*

        """
        return self._can('assign') # or with other checks

    def can_assign_agents_to_resource(self, resource_id):
        """Tests if this user can alter resource/agent mappings.

        A return of true does not guarantee successful authorization. A
        return of false indicates that it is known location methods in
        this session will result in a ``PermissionDenied``. This is
        intended as a hint to an application that may opt not to offer
        assignment operations to unauthorized users.

        arg:    resource_id (osid.id.Id): the ``Id`` of the ``Resource``
        return: (boolean) - ``false`` if mapping is not authorized,
                ``true`` otherwise
        raise:  NullArgument - ``resource_id`` is ``null``
        *compliance: mandatory -- This method must be implemented.*

        # this agent can only assign itself to the resource if the
        resource does NOT already have an agent AND the agent is
        not already assigned to a resource, OR if the
        resource is assigned to THIS agent.

        """
        if resource_id is None:
            raise errors.NullArgument('resource_id must be provided.')

        ras = self._resource_agent_session
        agent_id = self.effective_agent_id
        resource_matches_agent = False
        resource_has_no_agents = False
        try:
            # agent assigned to a resource AND it is this resource
            resource = ras.get_resource_by_agent(agent_id)
            if str(resource.ident) == str(resource_id):
                resource_matches_agent = True
        except errors.NotFound:
            # agent is NOT assigned to a resource, now check if the resource
            # already has an agent
            resource_agents = ras.get_agent_ids_by_resource(resource_id)
            if resource_agents.available() > 0:
                resource_matches_agent = False
            else:
                resource_has_no_agents = True
        finally:
            if self._can('assign') or resource_matches_agent or resource_has_no_agents:
                return True
            else:
                return False

    def assign_agent_to_resource(self, agent_id, resource_id):
        """Adds an existing ``Agent`` to a ``Resource``.

        arg:    agent_id (osid.id.Id): the ``Id`` of the ``Agent``
        arg:    resource_id (osid.id.Id): the ``Id`` of the ``Resource``
        raise:  AlreadyExists - ``agent_id`` is already assigned to
                ``resource_id``
        raise:  NotFound - ``agent_id`` or ``resource_id`` not found
        raise:  NullArgument - ``agent_id`` or ``resource_id`` is
                ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_assign_agents_to_resource(resource_id):
            self._provider_session.assign_agent_to_resource(agent_id, resource_id)
        else:
            raise errors.IllegalState()

    def unassign_agent_from_resource(self, agent_id, resource_id):
        """Removes an ``Agent`` from a ``Resource``.

        arg:    agent_id (osid.id.Id): the ``Id`` of the ``Agent``
        arg:    resource_id (osid.id.Id): the ``Id`` of the ``Resource``
        raise:  NotFound - ``agent_id`` or ``resource_id`` not found or
                ``agent_id`` not assigned to ``resource_id``
        raise:  NullArgument - ``agent_id`` or ``resource_id`` is
                ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure
        *compliance: mandatory -- This method must be implemented.*

        """
        if self.can_assign_agents_to_resource(resource_id):
            self._provider_session.unassign_agent_from_resource(agent_id, resource_id)
        else:
            raise errors.IllegalState()
