"""Resource Admin AuthZ adapter implementations of resource managers."""

# pylint: disable=no-init
#     Numerous classes don't require __init__.
# pylint: disable=too-many-ancestors
#     Number of ancestors defined in spec

from dlkit.authz_adapter.resource import managers
from . import sessions


class ResourceManager(managers.ResourceManager):
    """
    The resource manager provides access to resource lookup and creation sessions.

     Provides interoperability tests for various aspects of this service

    """

    def supports_resource_admin(self):
        """Yes, I do"""
        return True

    def supports_resource_agent_assignment(self):
        """Yes, I support that too"""
        return True

    def get_resource_admin_session(self):
        """
        Gets a resource administration session for creating, updating and deleting resources.

        """
        return sessions.ResourceAdminSession(
            self._provider_manager.get_resource_admin_session(),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session())

    resource_admin_session = property(fget=get_resource_admin_session)

    def get_resource_admin_session_for_bin(self, bin_id):
        """
        Gets a resource administration session for the given bin.

        """
        return sessions.ResourceAdminSession(
            self._provider_manager.get_resource_admin_session_for_bin(bin_id),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin(bin_id))

    def get_resource_agent_assignment_session(self):
        """
        Gets the session for assigning agents to resources.

        """
        return sessions.ResourceAgentAssignmentSession(
            self._provider_manager.get_resource_agent_assignment_session(),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin())

    resource_agent_assignment_session = property(fget=get_resource_agent_assignment_session)

    def get_resource_agent_assignment_session_for_bin(self, bin_id):
        """Gets a resource agent session for the given bin.

        """
        return sessions.ResourceAgentAssignmentSession(
            self._provider_manager.get_resource_agent_assignment_session_for_bin(bin_id),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin(bin_id))

class ResourceProxyManager(managers.ResourceProxyManager):
    """
    The resource manager provides access to resource lookup and creation session.

    Provides interoperability tests for various aspects of this service. Methods
    in this manager accept a ``Proxy``

    """

    def supports_resource_admin(self):
        """Yes, I do"""
        return True

    def supports_resource_agent_assignment(self):
        """Yes, I supports that too"""
        return True

    def get_resource_admin_session(self, proxy):
        """
        Gets a resource administration session for creating, updating and deleting resources.

        """
        return sessions.ResourceAdminSession(
            self._provider_manager.get_resource_admin_session(proxy),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session(proxy),
            proxy)

    def get_resource_admin_session_for_bin(self, bin_id, proxy):
        """
        Gets a resource administration session for the given bin.

        """
        return sessions.ResourceAdminSession(
            self._provider_manager.get_resource_admin_session_for_bin(bin_id, proxy),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin(bin_id, proxy),
            proxy)

    def get_resource_agent_assignment_session(self, proxy):
        """
        Gets the session for assigning agents to resources.

        """
        return sessions.ResourceAgentAssignmentSession(
            self._provider_manager.get_resource_agent_assignment_session(proxy),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin(proxy),
            proxy)

    def get_resource_agent_assignment_session_for_bin(self, bin_id, proxy):
        """
        Gets a resource agent session for the given bin.

        """
        return sessions.ResourceAgentAssignmentSession(
            self._provider_manager.get_resource_agent_assignment_session_for_bin(bin_id, proxy),
            self._get_authz_session(),
            self._provider_manager.get_resource_agent_session_for_bin(bin_id, proxy),
            proxy)
