# new user class with key info
# http://stackoverflow.com/questions/16880461/django-1-5-user-model-relationship
from django.contrib.auth.models import AbstractUser
from django.db import models

class APIUser(AbstractUser):
    """
    Add a public key and private key fields to each user, to grant them API access
    Will be used in the signature verification step
    """
    public_key = models.CharField(max_length=200)
    private_key = models.CharField(max_length=200)

class LTIUser(models.Model):
    """
    Stores LTI user id, tool consumer GUID, and role -- ONLY anonymous data
     to help the dlkit ortho3d authorization module check privileges.
    """
    user_id = models.CharField(max_length=50)
    consumer_guid = models.CharField(max_length=200)
    role = models.CharField(max_length=200)
    bank = models.CharField(max_length=300)
