from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'MechRevTest.views.home', name='home'),
    # url(r'^MechRevTest/', include('MechRevTest.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/?', include(admin.site.urls)),

    url(r'/?^$', 'users.views.splash', name='splash'),
    # url(r'^api/v1/?', include('assessments.urls', namespace='assessments')),
    # url(r'^touchstone/api/v1/?', include('assessments.urls', namespace='touchstone')),

    url(r'^api/v2/assessment/?', include('assessmentsv2.urls', namespace='assessmentsv2')),
    url(r'^touchstone/api/v2/assessment/?', include('assessmentsv2.urls', namespace='touchstonev2')),

    url(r'^api/v2/authorization/?', include('authorization.urls', namespace='authorization')),
    url(r'^touchstone/api/v2/authorization/?', include('authorization.urls', namespace='authorization_touchstone')),

    url(r'^api/v2/repository/?', include('repository.urls', namespace='repository')),
    url(r'^touchstone/api/v2/repository/?', include('repository.urls', namespace='repository_touchstone')),

    url(r'^api/v2/resource/?', include('resource.urls', namespace='resource')),
    url(r'^touchstone/api/v2/resource/?', include('resource.urls', namespace='resource_touchstone')),

    url(r'^api/v2/grading/?', include('grading.urls', namespace='grading')),
    url(r'^touchstone/api/v2/grading/?', include('grading.urls', namespace='grading_touchstone')),

    url(r'^api/v2/learning/?', include('learning.urls', namespace='learning')),
    url(r'^touchstone/api/v2/learning/?', include('learning.urls', namespace='learning_touchstone')),

    url(r'^api/v2/logging/?', include('logging_.urls', namespace='logging')),
    url(r'^touchstone/api/v2/logging/?', include('logging_.urls', namespace='logging_touchstone')),

    url(r'^users/?', include('users.urls', namespace='users')),
    url(r'^lti/?', include('lti.urls', namespace='lti')),
)
