# Django settings for VideoSearch project.
# From NB: https://github.com/nbproject/nbproject/blob/master/apps/settings.py
from os.path import abspath, dirname
import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

import random
import string

# from threading import RLock
# from cachetools import LRUCache

def rand_generator(size=24, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

FN_CREDENTIALS = "settings_credentials.py"
def msg_credentials():
    msg = "*** Please edit the %s file with the required settings for authentication. ***" %(FN_CREDENTIALS, )
    stars = "*" * len(msg)
    return "\n\n%s\n%s\n%s\n\n" %(stars, msg, stars)

try:
    import settings_credentials
except ImportError:
    from os.path import dirname, abspath
    import shutil
    thisdir = dirname(abspath(__file__))
    shutil.copy2("%s/%s.skel" % (thisdir, FN_CREDENTIALS), "%s/%s" % (thisdir, FN_CREDENTIALS))
    print msg_credentials()
    exit(1)

# CACHE = LRUCache(maxsize=100)
# LOCK = RLock()

DEBUG = settings_credentials.__dict__.get("DEBUG", False)
TESTING = settings_credentials.__dict__.get("TESTING", False)
TEMPLATE_DEBUG = DEBUG
ADMINS = settings_credentials.__dict__.get("ADMINS", ())
MANAGERS = ADMINS
SERVERNAME = settings_credentials.__dict__.get("SERVERNAME", "localhost")
HTTP_PORT = settings_credentials.__dict__.get("HTTP_PORT", "80")
CRON_EMAIL = settings_credentials.__dict__.get("CRON_EMAIL", "no@one.com")
DATABASES = settings_credentials.DATABASES

# For logging
LOGGING = settings_credentials.__dict__.get('LOGGING')

REMOTE_DEBUG = settings_credentials.__dict__.get("REMOTE_DEBUG", False)

# For Amazon S3
S3_PUBLIC_KEY = settings_credentials.__dict__.get('S3_PUBLIC_KEY', '')
S3_PRIVATE_KEY = settings_credentials.__dict__.get('S3_PRIVATE_KEY', '')
S3_BUCKET = settings_credentials.__dict__.get('S3_BUCKET', '')

S3_TEST_PUBLIC_KEY = settings_credentials.__dict__.get('S3_TEST_PUBLIC_KEY', '')
S3_TEST_PRIVATE_KEY = settings_credentials.__dict__.get('S3_TEST_PRIVATE_KEY', '')
S3_TEST_BUCKET = settings_credentials.__dict__.get('S3_TEST_BUCKET', '')

CLOUDFRONT_PUBLIC_KEY = settings_credentials.__dict__.get('CLOUDFRONT_PUBLIC_KEY', '')
CLOUDFRONT_PRIVATE_KEY = settings_credentials.__dict__.get('CLOUDFRONT_PRIVATE_KEY', '')
CLOUDFRONT_DISTRO = settings_credentials.__dict__.get('CLOUDFRONT_DISTRO', '')
CLOUDFRONT_DISTRO_ID = settings_credentials.__dict__.get('CLOUDFRONT_DISTRO_ID', '')
CLOUDFRONT_SIGNING_KEYPAIR_ID = settings_credentials.__dict__.get('CLOUDFRONT_SIGNING_KEYPAIR_ID', '')
CLOUDFRONT_SIGNING_PRIVATE_KEY_FILE = settings_credentials.__dict__.get('CLOUDFRONT_SIGNING_PRIVATE_KEY_FILE', '')

CLOUDFRONT_TEST_PUBLIC_KEY = settings_credentials.__dict__.get('CLOUDFRONT_TEST_PUBLIC_KEY', '')
CLOUDFRONT_TEST_PRIVATE_KEY = settings_credentials.__dict__.get('CLOUDFRONT_TEST_PRIVATE_KEY', '')
CLOUDFRONT_TEST_DISTRO = settings_credentials.__dict__.get('CLOUDFRONT_TEST_DISTRO', '')
CLOUDFRONT_TEST_DISTRO_ID = settings_credentials.__dict__.get('CLOUDFRONT_TEST_DISTRO_ID', '')
CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID = settings_credentials.__dict__.get('CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID', '')
CLOUDFRONT_TEST_SIGNING_PRIVATE_KEY_FILE = settings_credentials.__dict__.get('CLOUDFRONT_TEST_SIGNING_PRIVATE_KEY_FILE', '')


# For static and media files
MEDIA_ROOT = settings_credentials.__dict__.get('MEDIA_ROOT')
MEDIA_URL = settings_credentials.__dict__.get('MEDIA_URL')
STATIC_ROOT = settings_credentials.__dict__.get('STATIC_ROOT')
STATIC_URL = settings_credentials.__dict__.get('STATIC_URL')

# For logging in
LOGIN_URL = settings_credentials.__dict__.get('LOGIN_URL')

# For MC3 Configuration
MC3_HOST = settings_credentials.__dict__.get('MC3_HOST', 'mc3-dev.mit.edu')
HANDCAR_IMPL = settings_credentials.__dict__.get('HANDCAR_IMPL', 'HANDCAR_MC3_DEV')

#For functional tests
SELENIUM_WEBDRIVER = settings_credentials.__dict__.get('SELENIUM_WEBDRIVER')

# For allowed hosts (Django 1.5+, Debug = False)
ALLOWED_HOSTS = settings_credentials.__dict__.get('ALLOWED_HOSTS')

# For the secure Touchstone login
SECURE_PATH = settings_credentials.__dict__.get('SECURE_PATH')

# Additional locations of static files
STATICFILES_DIRS = settings_credentials.__dict__.get('STATICFILES_DIRS')

# ASSESSMENTS = 'assessments-dev.mit.edu'
# ASSESSMENTS_SERVICE = 'https://' + ASSESSMENTS + '/api/v1/assessment/'
ASSESSMENTS = settings_credentials.__dict__.get('ASSESSMENTS')
ASSESSMENTS_SERVICE = settings_credentials.__dict__.get('ASSESSMENTS_SERVICE')
APP_PUBLIC = settings_credentials.__dict__.get('APP_PUBLIC')
APP_PRIVATE = settings_credentials.__dict__.get('APP_PRIVATE')

LTI_KEY = settings_credentials.__dict__.get('LTI_KEY')
LTI_SECRET = settings_credentials.__dict__.get('LTI_SECRET')

# because dlkit runtime needs all these fields, even if I don't use them...
MC3_HANDCAR_APP_KEY = settings_credentials.__dict__.get('MC3_HANDCAR_APP_KEY', '')
MC3_DEMO_HANDCAR_APP_KEY = settings_credentials.__dict__.get('MC3_DEMO_HANDCAR_APP_KEY', '')
MC3_DEV_HANDCAR_APP_KEY = settings_credentials.__dict__.get('MC3_DEV_HANDCAR_APP_KEY', '')
DLKIT_MONGO_DB_PREFIX = settings_credentials.__dict__.get('DLKIT_MONGO_DB_PREFIX', '')
DLKIT_TEST_MONGO_DB_PREFIX = settings_credentials.__dict__.get('DLKIT_TEST_MONGO_DB_PREFIX', '')
MONGO_HOST_URI = settings_credentials.__dict__.get('MONGO_HOST_URI')
DLKIT_AUTHORITY = settings_credentials.__dict__.get('DLKIT_AUTHORITY')
DLKIT_MONGO_DB_INDEXES = settings_credentials.__dict__.get('DLKIT_MONGO_DB_INDEXES')
DLKIT_MONGO_KEYWORD_FIELDS = settings_credentials.__dict__.get('DLKIT_MONGO_KEYWORD_FIELDS')

WSDL_URL = settings_credentials.__dict__.get('WSDL_URL', None)
WSDL_CERT = settings_credentials.__dict__.get('WSDL_CERT', (None, None))
ROLES_USER = settings_credentials.__dict__.get('ROLES_USER', '')

SECRET_KEY = settings_credentials.__dict__.get('SECRET_KEY', rand_generator())
AUTHZ_USER = settings_credentials.__dict__.get('AUTHZ_USER', rand_generator())

if 'CACHES' in settings_credentials.__dict__:
    CACHES = settings_credentials.__dict__.get('CACHES', {})
CELERY_ALWAYS_EAGER = settings_credentials.__dict__.get('CELERY_ALWAYS_EAGER', '')
CELERY_EAGER_PROPAGATES_EXCEPTIONS = settings_credentials.__dict__.get('CELERY_EAGER_PROPAGATES_EXCEPTIONS', '')
BROKER_URL = settings_credentials.__dict__.get('BROKER_URL', '')
CELERY_RESULT_BACKEND = settings_credentials.__dict__.get('CELERY_RESULT_BACKEND', '')
CELERY_RESULT_DBURI = settings_credentials.__dict__.get('CELERY_RESULT_DBURI', '')
CELERY_RESULT_PERSISTENT = settings_credentials.__dict__.get('CELERY_RESULT_PERSISTENT', '')
CELERY_IGNORE_RESULT = settings_credentials.__dict__.get('CELERY_IGNORE_RESULT', '')
CELERY_TRACK_STARTED = settings_credentials.__dict__.get('CELERY_TRACK_STARTED', '')

if "default" not in DATABASES or "PASSWORD" not in DATABASES["default"] or DATABASES["default"]["PASSWORD"]=="":
    print 'Error: Could not find default database password.'
    print msg_credentials()
    exit(1)

# PROJECT_PATH avoids using a hard-coded path that must be changed with every server deployment
PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

ABS_PATH_TO_FILES = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

if DEBUG and not TESTING:
    MIDDLEWARE_CLASSES = (
        'corsheaders.middleware.CorsMiddleware',
        # 'django.middleware.cache.UpdateCacheMiddleware',
        'django.middleware.common.CommonMiddleware',
        # 'django.middleware.cache.FetchFromCacheMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.RemoteUserMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # Uncomment the next line for simple clickjacking protection:
        # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'assessments.middleware.ProfilerMiddleware',
    )
else:
    MIDDLEWARE_CLASSES = (
        'corsheaders.middleware.CorsMiddleware',
        # 'django.middleware.cache.UpdateCacheMiddleware',
        'django.middleware.common.CommonMiddleware',
        # 'django.middleware.cache.FetchFromCacheMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.RemoteUserMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # Uncomment the next line for simple clickjacking protection:
        # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'django.contrib.auth.backends.RemoteUserBackend',
)

ROOT_URLCONF = 'assessments_main.urls'

# Python dotted path to the WSGI application used by Django's runserver.
# WSGI_APPLICATION = 'resource_bank.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ABS_PATH_TO_FILES+'/templates',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'assessments',
    'assessments_users',
    'assessmentsv2',
    'authorization',
    'repository',
    'resource',
    'utilities',
    'users',
    'south',
    'rest_framework',
    'rest_framework_httpsignature',
    'oauth_provider',
    'lti',
    # 'dlkit_tests',
    'qbank_authz',
    'corsheaders',
    'learning',
    'qbank',
    'logging_',
    'djcelery'
)

SOUTH_TESTS_MIGRATE = False

# to add public / private key fields to users
# http://stackoverflow.com/questions/16880461/django-1-5-user-model-relationship
AUTH_USER_MODEL = 'assessments_users.APIUser'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'assessments.auth.AssessmentsSignatureAuthentication',
        'assessments.auth.LTIAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

# OAUTH_PROVIDER secret key size
# http://code.larlet.fr/django-oauth-plus/wiki/installation
OAUTH_PROVIDER_SECRET_SIZE = 50

TEMPLATE_CONTEXT_PROCESSORS += ("django.core.context_processors.request",)

SESSION_SAVE_EVERY_REQUEST = True

# Django CORS from:
# https://github.com/ottoyiu/django-cors-headers/
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-api-proxy',
    'content-type',
    'accept',
    'origin',
    'request-line',
    'authorization',
    'x-api-key'
)

# sacrifice SEO to support non-slash frameworks like Angular
# http://stackoverflow.com/questions/1596552/django-urls-without-a-trailing-slash-do-not-redirect
APPEND_SLASH = False

if not TESTING:
    try:
        settings_credentials.bootstrap()
    except (AttributeError, ImportError):
        pass
else:
    import logging
    PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )
    DEBUG = False
    TEMPLATE_DEBUG = False
    # logging.disable(logging.CRITICAL)
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'simple_test_db'
        }
    }
    MIDDLEWARE_CLASSES = (
        'corsheaders.middleware.CorsMiddleware',
        # 'django.middleware.cache.UpdateCacheMiddleware',
        'django.middleware.common.CommonMiddleware',
        # 'django.middleware.cache.FetchFromCacheMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # Uncomment the next line for simple clickjacking protection:
        # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )
    CELERY_ALWAYS_EAGER = True
    CELERY_RESULT_DBURI = "sqlite:///task_results.sqlite3"
