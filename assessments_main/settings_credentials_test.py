# Django settings for VideoSearch project.
# From NB: https://github.com/nbproject/nbproject/blob/master/apps/settings.py
from os.path import abspath, dirname
import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

import random
import string

def rand_generator(size=24, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

DEBUG = True
TEMPLATE_DEBUG = DEBUG
MANAGERS = {}

# PROJECT_PATH avoids using a hard-coded path that must be changed with every server deployment
PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

ABS_PATH_TO_FILES = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

if DEBUG:
    MIDDLEWARE_CLASSES = (
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.RemoteUserMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # Uncomment the next line for simple clickjacking protection:
        # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'assessments.middleware.ProfilerMiddleware',
    )
else:
    MIDDLEWARE_CLASSES = (
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.RemoteUserMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # Uncomment the next line for simple clickjacking protection:
        # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'django.contrib.auth.backends.RemoteUserBackend',
)

ROOT_URLCONF = 'assessments_main.urls'

# Python dotted path to the WSGI application used by Django's runserver.
# WSGI_APPLICATION = 'resource_bank.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ABS_PATH_TO_FILES+'/templates',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'assessments',
    'assessments_users',
    'assessmentsv2',
    'authorization',
    'repository',
    'resource',
    'utilities',
    'users',
    'south',
    'rest_framework',
    'rest_framework_httpsignature',
    'oauth_provider',
    'lti',
    'dlkit_runtime',
    'dlkit',
    # 'dlkit_tests',
    'qbank_authz',
    'corsheaders',
    'records',
    'learning',
    'qbank',
    'logging_'
)

SOUTH_TESTS_MIGRATE = False

# to add public / private key fields to users
# http://stackoverflow.com/questions/16880461/django-1-5-user-model-relationship
AUTH_USER_MODEL = 'assessments_users.APIUser'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'assessments.auth.AssessmentsSignatureAuthentication',
        'assessments.auth.LTIAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

SESSION_SERIALIZER='django.contrib.sessions.serializers.JSONSerializer'

# OAUTH_PROVIDER secret key size
# http://code.larlet.fr/django-oauth-plus/wiki/installation
OAUTH_PROVIDER_SECRET_SIZE = 50

TEMPLATE_CONTEXT_PROCESSORS += ("django.core.context_processors.request",)

SESSION_SAVE_EVERY_REQUEST = True

# Django CORS from:
# https://github.com/ottoyiu/django-cors-headers/
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-api-proxy',
    'content-type',
    'accept',
    'origin',
    'request-line',
    'authorization',
    'x-api-key'
)

# sacrifice SEO to support non-slash frameworks like Angular
# http://stackoverflow.com/questions/1596552/django-urls-without-a-trailing-slash-do-not-redirect
APPEND_SLASH = False

#

# TEMPLATE_DEBUG = ""
# ADMINS = {}
# MANAGERS = ""
# HTTP_PORT = ""
# HTTPD_MEDIA = ""
# EMAIL_HOST = ""
# EMAIL_FROM = ""
# EMAIL_BCC = ""

# EMAIL_BACKEND = ""
# EMAIL_FILE_PATH = ""

# PERSONA_EMAIL = ""
# PERSONA_PASSWORD = ""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'qbank', # Or path to database file if using sqlite3.
        'USER': 'root', # Not used with sqlite3.
        'PASSWORD': '', # Not used with sqlite3.
        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
    }
}

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        }
    }
}

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
# /Users/cjshaw/Documents/Projects/RELATE/edx_resource_bank-master/media/
STATIC_ROOT = '/var/www/html/qbank/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# The Chrome Webdriver for Selenium testing
SELENIUM_WEBDRIVER = ''

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['qbank-dev.mit.edu']

SECURE_PATH = 'www.mit.edu'

LOGIN_URL = '/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

MC3_HOST = 'mc3-dev.mit.edu'

# certificate location for talking to IS&T Membership Service
CERT = '/var/www/assessments/users/pqq.cer'
KEY = '/var/www/membership-service/pqq-key.pem'
MEMBERSHIP = 'https://learning-modules-test.mit.edu:8443/service/membership/'

ASSESSMENTS = 'qbank-dev.mit.edu'
ASSESSMENTS_SERVICE = 'https://' + ASSESSMENTS + '/api/v1/assessment/'
#ASSESSMENTS = '127.0.0.1:8000'
#ASSESSMENTS_SERVICE = 'http://' + ASSESSMENTS + '/api/v1/assessment/'
APP_PUBLIC = ''
APP_PRIVATE = ''

LTI_KEY = ''
LTI_SECRET = ''


# AWS S3 and Cloudfront stuff for putting files into S3, then accessing + signing them
S3_PUBLIC_KEY = ''
S3_PRIVATE_KEY = ''
S3_BUCKET = ''

CLOUDFRONT_PUBLIC_KEY = ''
CLOUDFRONT_PRIVATE_KEY = ''
CLOUDFRONT_DISTRO = ''
CLOUDFRONT_DISTRO_ID = ''
CLOUDFRONT_SIGNING_KEYPAIR_ID = ''
CLOUDFRONT_SIGNING_PRIVATE_KEY_FILE = ''

S3_TEST_PUBLIC_KEY = 'AKIAIT3DXRKVISAGIM7Q'
S3_TEST_PRIVATE_KEY = 'lw8nlGbYo4tJpRSoXObA8rdze0qHRVAFcB7WkBK6'
S3_TEST_BUCKET = 'mitodl-repository-test'

CLOUDFRONT_TEST_PUBLIC_KEY = 'AKIAJA67YNZBGSUZHBOA'
CLOUDFRONT_TEST_PRIVATE_KEY = 'VvIT1kjquuN8tawHc1SjYdHOrnquMQ0kCHh8w5sK'
CLOUDFRONT_TEST_DISTRO = 'd1v4o60a4yrgi8.cloudfront.net'
CLOUDFRONT_TEST_DISTRO_ID = 'E1OEKZHRUO35M9'
CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID = 'APKAIGRK7FPIAJR675NA'
CLOUDFRONT_TEST_SIGNING_PRIVATE_KEY_FILE = '{0}/assessments_main/cf.pem'.format(ABS_PATH_TO_FILES)

# because dlkit runtime needs all these fields, even if I don't use them...
MC3_HANDCAR_APP_KEY = ''
MC3_DEMO_HANDCAR_APP_KEY = ''
MC3_DEV_HANDCAR_APP_KEY = 'AGENT_KEYEsHgRHv4kIUVPmnn16KIzj5pyrkiHzMotbElekzuRPmhj34moFj%2BgUAuLoymRxIx'
DLKIT_MONGO_DB_PREFIX = ''
DLKIT_TEST_MONGO_DB_PREFIX = 'test_qbank_'
DLKIT_AUTHORITY = 'bazzim.MIT.EDU'
HANDCAR_IMPL = 'HANDCAR_MC3_DEV'

DLKIT_MONGO_DB_INDEXES = {
    DLKIT_MONGO_DB_PREFIX + 'grading.GradeEntry': ['gradebookColumnId'],
    DLKIT_MONGO_DB_PREFIX + 'assessment.Assessment': ['itemIds'],
    DLKIT_MONGO_DB_PREFIX + 'assessment.Item': ['bankId', 'learningObjectiveIds'],
    DLKIT_MONGO_DB_PREFIX + 'repository.Asset': ['repositoryId']
}

DLKIT_MONGO_KEYWORD_FIELDS = {
    'assessment.Item': ['texts.edxml'],
    'repository.Asset': ['assetContents.0.text.text'],
    'logging.LogEntry': ['text.text']
}

REMOTE_DEBUG = False

AUTHZ_USER = 'test_souper'
MONGO_HOST_URI = None

SECRET_KEY = rand_generator()