import os
import datetime

from assessments_users.models import APIUser

from decimal import Decimal

from utilities import general as gutils
from utilities import resource as resutils
from utilities.testing import create_test_request, QBankBaseTest, add_user_authz_to_settings

from dlkit.runtime.primordium import Id, DataInputStream, Type


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))


class DjangoTestCase(QBankBaseTest):
    """
    A TestCase class that combines minimocktest and django.test.TestCase

    http://pykler.github.io/MiniMockTest/
    """
    def create_new_objective_bank(self):
        lm = gutils.get_session_data(self.req, 'lm')
        form = lm.get_objective_bank_form_for_create([])
        form.display_name = 'my new objective_bank'
        form.description = 'for testing with'
        return lm.create_objective_bank(form)

    def setUp(self):
        super(DjangoTestCase, self).setUp()
        self.url = '/api/v2/learning/'
        self.username = 'instructor@mit.edu'
        self.password = 'jinxem'
        self.user = APIUser.objects.create_user(username=self.username,
                                                password=self.password)
        self.student_name = 'student@mit.edu'
        self.student_password = 'blahblah'
        self.student = APIUser.objects.create_user(username=self.student_name,
                                                   password=self.student_password)
        self.req = create_test_request(self.user)
        gutils.activate_managers(self.req)

        self.test_file = open(ABS_PATH + '/tests/files/Flexure_structure_with_hints.pdf')

    def tearDown(self):
        super(DjangoTestCase, self).tearDown()
        self.test_file.close()


class BasicServiceTests(DjangoTestCase):
    """Test the views for getting the basic service calls

    """
    def setUp(self):
        super(BasicServiceTests, self).setUp()

    def tearDown(self):
        super(BasicServiceTests, self).tearDown()

    def test_authenticated_users_can_see_available_services(self):
        url = self.url
        req = self.get(url)
        self.ok(req)
        self.message(req, 'documentation')
        self.message(req, 'objectivebanks')

    def test_non_authenticated_users_cannot_see_available_services(self):
        url = self.url
        req = self.client.get(url)  # keep it as self.get() to get unauthenticated
        self.code(req, 403)

    def test_instructors_can_get_list_of_objective_banks(self):
        url = self.url + 'objectivebanks/'
        req = self.get(url)
        self.ok(req)
        self.message(req, '"count": 0')

    def test_learners_can_see_list_of_objective_banks(self):
        url = self.url + 'objectivebanks/'
        req = self.get(url,  non_instructor=True)
        self.ok(req)
        self.message(req, '"count": 0')


class DocumentationTests(DjangoTestCase):
    """Test the views for getting the documentation

    """
    def setUp(self):
        super(DocumentationTests, self).setUp()

    def tearDown(self):
        super(DocumentationTests, self).tearDown()

    def test_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url)
        self.ok(req)
        self.message(req, 'Documentation for MIT Learning Service, V2')

    def test_non_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.client.get(url)  # keep this self.client.get() to make it unauthenticated
        self.ok(req)
        self.message(req, 'Documentation for MIT Learning Service, V2')

    def test_student_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url, non_instructor=True)
        self.ok(req)
        self.message(req, 'Documentation for MIT Learning Service, V2')


class ObjectiveBankCrUDTests(DjangoTestCase):
    """Test the views for objective bank crud

    """
    def num_obj_banks(self, val):
        gutils.activate_managers(self.req)
        lm = gutils.get_session_data(self.req, 'lm')

        self.assertEqual(
            lm.get_objective_banks().available(),
            val
        )

    def setUp(self):
        super(ObjectiveBankCrUDTests, self).setUp()
        self.bad_gradebook_id = 'assessment.Bank%3A55203f0be7dde0815228bb41%40bazzim.MIT.EDU'
        self.bank = self.create_new_objective_bank()
        self.url += 'objectivebanks/'
        self.login()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self.bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=self.bank.ident)

    def tearDown(self):
        super(ObjectiveBankCrUDTests, self).tearDown()

    def test_can_get_objective_bank(self):
        url = self.url + str(self.bank.ident)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['id'],
            str(self.bank.ident)
        )

    def test_can_get_objective_banks(self):
        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            data['data']['results'][0]['id'],
            str(self.bank.ident)
        )

    def test_can_create_objective_bank(self):
        self.num_obj_banks(1)
        payload = {
            'name': 'foo2',
            'description': 'testing me'
        }
        req = self.post(self.url, payload)
        self.created(req)
        data = self.json(req)
        self.num_obj_banks(2)
        self.assertEqual(
            data['displayName']['text'],
            payload['name']
        )
        self.assertEqual(
            data['description']['text'],
            payload['description']
        )

    def test_can_update_objective_bank(self):
        self.num_obj_banks(1)
        payload = {
            'name': 'foo2',
            'description': 'testing me'
        }
        url = self.url + str(self.bank.ident)
        req = self.put(url, payload)
        self.updated(req)
        data = self.json(req)
        self.num_obj_banks(1)
        self.assertEqual(
            data['displayName']['text'],
            payload['name']
        )
        self.assertEqual(
            data['description']['text'],
            payload['description']
        )

    def test_can_delete_objective_bank(self):
        self.num_obj_banks(1)
        url = self.url + str(self.bank.ident)
        req = self.delete(url)
        self.deleted(req)
        self.num_obj_banks(0)


class ProficiencyCrUDTests(DjangoTestCase):
    """Test the views for proficiency crud

    """
    def configure_grades(self):
        grades = [
            {'input_score_start_range': 80.0,
             'input_score_end_range': 89.9,
             'output_score': 3.0,
             'label': 'B'},
            {'input_score_start_range': 90.0,
             'input_score_end_range': 100.0,
             'output_score': 4.0,
             'label': 'A'}
        ]
        gm = gutils.get_session_data(self.req, 'gm')
        form = gm.get_gradebook_form_for_create([])
        form.display_name = 'gradebook'
        form.description = 'for testing'
        gradebook = gm.create_gradebook(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=gradebook.ident)

        form = gradebook.get_grade_system_form_for_create([])
        form.display_name = 'grade system'
        form.desription = 'for testing'
        grade_system = gradebook.create_grade_system(form)

        grade_objects = []
        for grade in grades:
            form = gradebook.get_grade_form_for_create(grade_system.ident, [])
            form.display_name = 'grade {0}'.format(grade['label'])
            form.description = 'for testing'
            form.set_input_score_start_range(grade['input_score_start_range'])
            form.set_input_score_end_range(grade['input_score_end_range'])
            form.set_output_score(grade['output_score'])
            grade_objects.append(gradebook.create_grade(form))

        return grade_objects

    def create_proficiency(self, bank):
        form = bank.get_proficiency_form_for_create(self._test_obj_id,
                                                    resutils.get_agent_id(self.username),
                                                    [])
        form.set_completion(Decimal(95.0))
        form.set_level_id(self._grades[0].ident)
        form.set_genus_type(Type(self._student_genus_type))
        return bank.create_proficiency(form)

    def num_proficiencies(self, bank, val):
        self.assertEqual(
            bank.get_proficiencies().available(),
            val
        )

    def perform_query(self, url, expected_count):
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            expected_count
        )

    def setUp(self):
        super(ProficiencyCrUDTests, self).setUp()
        self.bank = self.create_new_objective_bank()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self.bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=self.bank.ident)

        self.url += 'objectivebanks/' + str(self.bank.ident) + '/proficiencies/'
        self._test_obj_id = Id('mc3-objective%3A3834%40MIT-OEIT')
        self._bad_obj_id = Id('mc3-objective%3A3835%40MIT-OEIT')
        self._student_genus_type = 'learning.Proficiency%3Astudent%40ODL.MIT.EDU'
        self._challenge_genus_type = 'learning.Proficiency%3Achallenge%40ODL.MIT.EDU'

        self._grades = self.configure_grades()

        self.login()

    def tearDown(self):
        super(ProficiencyCrUDTests, self).tearDown()

    def test_can_create_proficiency(self):
        self.num_proficiencies(self.bank, 0)
        payload = {
            'objectiveId': str(self._test_obj_id),
            'resourceId': self.username,
            'completion': '95',
            'levelId': str(self._grades[0].ident),
            'startDate': {
                'day': 1,
                'month': 1,
                'year': 2016
            },
            'endDate': {
                'day': 31,
                'month': 1,
                'year': 2016
            },
            'genusTypeId': self._student_genus_type
        }
        req = self.post(self.url, payload)
        self.created(req)
        self.num_proficiencies(self.bank, 1)
        data = self.json(req)
        self.assertEqual(
            data['objectiveId'],
            payload['objectiveId']
        )
        self.assertEqual(
            data['resourceId'],
            str(resutils.get_agent_id(self.username))
        )
        self.assertEqual(
            data['completion'],
            95.0
        )
        self.assertEqual(
            data['levelId'],
            payload['levelId']
        )
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        for attr in ['startDate', 'endDate']:
            for param in ['day', 'month', 'year']:
                self.assertEqual(
                    data[attr][param],
                    payload[attr][param]
                )

    def test_can_get_proficiencies(self):
        self.num_proficiencies(self.bank, 0)
        prof = self.create_proficiency(self.bank)
        self.num_proficiencies(self.bank, 1)
        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            data['data']['results'][0]['id'],
            str(prof.ident)
        )
        self.num_proficiencies(self.bank, 1)

    def test_can_update_proficiency(self):
        self.num_proficiencies(self.bank, 0)
        prof = self.create_proficiency(self.bank)
        self.num_proficiencies(self.bank, 1)

        payload = {
            'objectiveId': str(self._test_obj_id),
            'resourceId': self.username,
            'completion': '65',
            'levelId': str(self._grades[1].ident),
            'startDate': {
                'day': 1,
                'month': 1,
                'year': 2016
            },
            'endDate': {
                'day': 31,
                'month': 1,
                'year': 2016
            },
            'genusTypeId': self._challenge_genus_type
        }

        url = self.url + str(prof.ident)

        req = self.put(url, payload)
        self.updated(req)
        self.num_proficiencies(self.bank, 1)
        data = self.json(req)
        self.assertEqual(
            data['completion'],
            65.0
        )
        self.assertEqual(
            data['levelId'],
            payload['levelId']
        )
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        for attr in ['startDate', 'endDate']:
            for param in ['day', 'month', 'year']:
                self.assertEqual(
                    data[attr][param],
                    payload[attr][param]
                )

    def test_can_delete_proficiency(self):
        self.num_proficiencies(self.bank, 0)
        prof = self.create_proficiency(self.bank)
        self.num_proficiencies(self.bank, 1)

        url = self.url + str(prof.ident)

        req = self.delete(url)
        self.deleted(req)
        self.num_proficiencies(self.bank, 0)

    def test_can_query_proficiencies_by_date_range(self):
        prof = self.create_proficiency(self.bank)

        today = datetime.date.today()
        payload = {
            'startDate': {
                'day': today.day,
                'month': today.month,
                'year': today.year
            },
            'endDate': {
                'day': today.day,
                'month': today.month,
                'year': today.year
            }
        }

        url = self.url + str(prof.ident)

        req = self.put(url, payload)
        self.updated(req)

        today = datetime.date.today().isoformat()
        yesterday = (datetime.date.today() - datetime.timedelta(days=1)).isoformat()
        tomorrow = (datetime.date.today() + datetime.timedelta(days=1)).isoformat()

        # start date only
        url = self.url + '?startDate={0}'.format(yesterday)
        self.perform_query(url, 0)

        url = self.url + '?startDate={0}'.format(today)
        self.perform_query(url, 1)

        url = self.url + '?startDate={0}'.format(tomorrow)
        self.perform_query(url, 0)

        # end date only
        url = self.url + '?endDate={0}'.format(yesterday)
        self.perform_query(url, 0)

        url = self.url + '?endDate={0}'.format(today)
        self.perform_query(url, 1)

        url = self.url + '?endDate={0}'.format(tomorrow)
        self.perform_query(url, 0)

        # date range
        url = self.url + '?endDate={0}&startDate={1}'.format(today, yesterday)
        self.perform_query(url, 1)

        url = self.url + '?endDate={0}&startDate={1}'.format(today, today)
        self.perform_query(url, 1)

        url = self.url + '?endDate={0}&startDate={1}'.format(tomorrow, tomorrow)
        self.perform_query(url, 0)

        url = self.url + '?endDate={0}&startDate={1}'.format(today, tomorrow)
        req = self.get(url)
        self.code(req, 500)

    def test_can_query_proficiencies_by_completion(self):
        self.create_proficiency(self.bank)

        url = self.url + '?completion=95'
        self.perform_query(url, 1)

        url = self.url + '?completion=95.0'
        self.perform_query(url, 1)

        url = self.url + '?completion=94'
        self.perform_query(url, 0)

        url = self.url + '?completion=100'
        self.perform_query(url, 0)

        url = self.url + '?completionStart=94&completionEnd=95.1'
        self.perform_query(url, 1)

        url = self.url + '?completionStart=95.1&completionEnd=95.2'
        self.perform_query(url, 0)

        url = self.url + '?completionStart=94&completionEnd=94.9'
        self.perform_query(url, 0)

        url = self.url + '?completionStart=95&completionEnd=94'
        req = self.get(url)
        self.code(req, 500)

    def test_can_query_proficiencies_by_level_id(self):
        self.create_proficiency(self.bank)

        url = self.url + '?levelId=' + str(self._grades[0].ident)
        self.perform_query(url, 1)

        url = self.url + '?levelId=' + str(self._grades[1].ident)
        self.perform_query(url, 0)

    def test_can_query_proficiencies_by_agent(self):
        self.create_proficiency(self.bank)

        url = self.url + '?resourceId=' + str(self.username)
        self.perform_query(url, 1)

        url = self.url + '?resourceId=' + str(self.student_name)
        self.perform_query(url, 0)

    def test_can_query_proficiencies_by_objective_id(self):
        self.create_proficiency(self.bank)

        url = self.url + '?objectiveId=' + str(self._test_obj_id)
        self.perform_query(url, 1)

        url = self.url + '?objectiveId=' + str(self._bad_obj_id)
        self.perform_query(url, 0)

    def test_can_query_proficiencies_by_genus_type_id(self):
        self.create_proficiency(self.bank)

        url = self.url + '?genusTypeId=' + str(self._student_genus_type)
        self.perform_query(url, 1)

        url = self.url + '?genusTypeId=' + str(self._challenge_genus_type)
        self.perform_query(url, 0)
