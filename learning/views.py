from decimal import Decimal

from dlkit.runtime.errors import PermissionDenied, InvalidArgument, IllegalState
from dlkit.runtime.primitives import DateTime, Type

from django.db import IntegrityError
from django.template import RequestContext
from django.shortcuts import render_to_response

from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from utilities import general as gutils
from utilities import learning as lutils
from utilities import resource as resutils
from qbank.views import DLKitSessionsManager


class Documentation(DLKitSessionsManager):
    """
    Shows the user documentation for talking to the RESTful service
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        return render_to_response('learning/documentation.html',
                                  {},
                                  RequestContext(request))


class LearningService(DLKitSessionsManager):
    """
    List all available learning services.
    api/v2/learning/
    """

    def get(self, request, format=None):
        """
        List all available grading services.
        """
        data = {}
        data = gutils.add_links(request,
                                data,
                                {
                                    'objectivebanks': 'objectivebanks/',
                                    'documentation': 'docs/'
                                })
        return Response(data)


class ObjectiveBankDetails(DLKitSessionsManager):
    """
    Return details of a specific objective bank

    api/v2/learning/objectivebanks/<bank_id>

    GET

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"displayName" : "a learning objective",
        "description" : "Do XYZ"}
   """
    def delete(self, request, bank_id, format=None):
        try:
            self.lm.delete_objective_bank(gutils.clean_id(bank_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, format=None):
        try:
            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))
            bank_map = gutils.convert_dl_object(bank)
            gutils.update_links(request, bank_map)
            return Response(bank_map)
        except (PermissionDenied, IntegrityError) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, format=None):
        try:
            gutils.verify_at_least_one_key_present(self.data,
                                                   ['displayName', 'name', 'description'])

            form = self.lm.get_objective_bank_form_for_update(gutils.clean_id(bank_id))
            form = gutils.set_form_basics(form, self.data)
            bank = self.lm.update_objective_bank(form)
            return gutils.UpdatedResponse(bank.object_map)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)


class ObjectiveBanksList(DLKitSessionsManager):
    """
    Return list of objective banks

    api/v2/learning/objectivebanks/

    GET

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"displayName" : "a learning objective",
        "description" : "Do XYZ"}
   """

    def get(self, request, format=None):
        try:
            banks = gutils.extract_items(request,
                                         self.lm.objective_banks)
            banks['data']['results'] = sorted(banks['data']['results'],
                                              key=lambda k: k['displayName']['text'].lower())
            return Response(banks)
        except (PermissionDenied, IntegrityError) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, format=None):
        """
        Create a new objective bank, if authorized
        """
        try:
            if 'bankId' not in self.data:
                form = self.lm.get_objective_bank_form_for_create([])
                gutils.verify_keys_present(self.data, ['name', 'description'])
                finalize_method = self.lm.create_objective_bank
            else:
                bank = self.lm.get_objective_bank(gutils.clean_id(self.data['bankId']))
                form = self.lm.get_objective_bank_form_for_update(bank.ident)
                finalize_method = self.lm.update_objective_bank

            form = gutils.set_form_basics(form, self.data)

            new_bank = gutils.convert_dl_object(finalize_method(form))

            return gutils.CreatedResponse(new_bank)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ProficiencyDetails(DLKitSessionsManager):
    """
    Return details of a specific proficiency

    api/v2/learning/objectivebanks/<bank_id>/proficiencies/<proficiency_id>

    GET

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"displayName" : "a learning objective",
        "description" : "Do XYZ"}
   """
    def delete(self, request, bank_id, proficiency_id, format=None):
        try:
            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))
            bank.delete_proficiency(gutils.clean_id(proficiency_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, proficiency_id, format=None):
        try:
            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))
            proficiency = bank.get_proficiency(gutils.clean_id(proficiency_id))
            proficiency_map = gutils.convert_dl_object(proficiency)
            gutils.update_links(request, proficiency_map)
            return Response(proficiency_map)
        except (PermissionDenied, IntegrityError) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, proficiency_id, format=None):
        try:
            gutils.verify_at_least_one_key_present(self.data,
                                                   ['displayName', 'name', 'description',
                                                    'completion', 'levelId', 'startDate',
                                                    'endDate'])

            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))

            form = bank.get_proficiency_form_for_update(gutils.clean_id(proficiency_id))
            form = gutils.set_form_basics(form, self.data)

            if 'completion' in self.data:
                form.set_completion(Decimal(self.data['completion']))

            if 'levelId' in self.data:
                form.set_level_id(gutils.clean_id(self.data['levelId']))

            if 'startDate' in self.data:
                gutils.verify_keys_present(self.data['startDate'],
                                           ['day', 'year', 'month'])
                form.set_start_date(DateTime(**self.data['startDate']))

            if 'endDate' in self.data:
                gutils.verify_keys_present(self.data['endDate'],
                                           ['day', 'year', 'month'])
                form.set_end_date(DateTime(**self.data['endDate']))

            proficiency = bank.update_proficiency(form)
            return gutils.UpdatedResponse(proficiency.object_map)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)


class ProficienciesList(DLKitSessionsManager):
    """
    Return list of proficiencies in an objective bank

    api/v2/learning/objectivebanks/<bank_id>/proficiencies

    GET
    POST

    GET query params:
        ['startDate', 'endDate', 'completionStart',
         'completionEnd', 'completion', 'level', 'resourceId', 'objectiveId']
    ** assumes dates are YYYY-MM-DD format

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"displayName" : "a learning objective",
        "description" : "Do XYZ"}
   """

    def get(self, request, bank_id, format=None):
        try:
            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))

            if any(t in self.data
                   for t in ['startDate', 'endDate', 'completionStart',
                             'completionEnd', 'completion', 'levelId', 'resourceId',
                             'objectiveId', 'genusTypeId']):
                querier = bank.get_proficiency_query()

                if 'genusTypeId' in self.data:
                    querier.match_genus_type(Type(self.data['genusTypeId']), True)

                if 'levelId' in self.data:
                    querier.match_level_id(gutils.clean_id(self.data['levelId']), True)

                if 'completion' in self.data:
                    querier.match_completion(Decimal(self.data['completion']),
                                             Decimal(self.data['completion']),
                                             True)
                elif 'completionStart' in self.data and 'completionEnd' in self.data:
                    querier.match_completion(Decimal(self.data['completionStart']),
                                             Decimal(self.data['completionEnd']),
                                             True)

                if 'startDate' in self.data and 'endDate' in self.data:
                    # assumes dates are YYYY-MM-DD format
                    start = lutils.convert_str_to_datetime(self.data['startDate'])
                    end = lutils.convert_str_to_datetime(self.data['endDate'])
                    querier.match_date(start, end, True)
                elif 'startDate' in self.data:
                    end = start = lutils.convert_str_to_datetime(self.data['startDate'])

                    querier.match_start_date(start, end, True)
                elif 'endDate' in self.data:
                    end = start = lutils.convert_str_to_datetime(self.data['endDate'])

                    querier.match_end_date(start, end, True)

                if 'objectiveId' in self.data:
                    querier.match_objective_id(gutils.clean_id(self.data['objectiveId']), True)

                if 'resourceId' in self.data:
                    querier.match_resource_id(resutils.get_agent_id(self.data['resourceId']), True)

                proficiencies = bank.get_proficiencies_by_query(querier)
            else:
                proficiencies = bank.get_proficiencies()

            proficiencies = gutils.extract_items(request,
                                                 proficiencies)
            return Response(proficiencies)
        except (PermissionDenied, IntegrityError, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, format=None):
        """
        Create a new proficiency, if authorized
        """
        try:
            gutils.verify_keys_present(self.data,
                                       ['objectiveId', 'resourceId'])
            bank = self.lm.get_objective_bank(gutils.clean_id(bank_id))
            form = bank.get_proficiency_form_for_create(gutils.clean_id(self.data['objectiveId']),
                                                        resutils.get_agent_id(self.data['resourceId']),
                                                        [])

            form = gutils.set_form_basics(form, self.data)

            if 'completion' in self.data:
                form.set_completion(Decimal(self.data['completion']))

            if 'levelId' in self.data:
                form.set_level_id(gutils.clean_id(self.data['levelId']))

            if 'startDate' in self.data:
                gutils.verify_keys_present(self.data['startDate'],
                                           ['day', 'year', 'month'])
                form.set_start_date(DateTime(**self.data['startDate']))

            if 'endDate' in self.data:
                gutils.verify_keys_present(self.data['endDate'],
                                           ['day', 'year', 'month'])
                form.set_end_date(DateTime(**self.data['endDate']))

            proficiency = gutils.convert_dl_object(bank.create_proficiency(form))

            return gutils.CreatedResponse(proficiency)
        except (PermissionDenied, InvalidArgument, KeyError) as ex:
            gutils.handle_exceptions(ex)
