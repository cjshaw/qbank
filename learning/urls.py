from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from learning import views

urlpatterns = patterns('',
   url(r'^$',
        views.LearningService.as_view()),
    url(r'^docs/?$',
        views.Documentation.as_view()),
    url(r'^objectivebanks/?$',
        views.ObjectiveBanksList.as_view()),
    url(r'^objectivebanks/(?P<bank_id>[-.:@%\d\w]+)/?$',
        views.ObjectiveBankDetails.as_view()),
    url(r'^objectivebanks/(?P<bank_id>[-.:@%\d\w]+)/proficiencies/?$',
        views.ProficienciesList.as_view()),
    url(r'^objectivebanks/(?P<bank_id>[-.:@%\d\w]+)/proficiencies/(?P<proficiency_id>[-.:@%\d\w]+)/?$',
        views.ProficiencyDetails.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)
