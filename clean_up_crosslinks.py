from dlkit_runtime import runtime, proxy_session
from dlkit_runtime.proxy_example import User
from dlkit_runtime.primordium import Type

from dlkit_runtime import configs

def create_test_request(test_user):
    from django.http import HttpRequest
    from django.conf import settings
    from django.utils.importlib import import_module
    #http://stackoverflow.com/questions/16865947/django-httprequest-object-has-no-attribute-session
    test_request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    session_key = None
    test_request.user = test_user
    test_request.session = engine.SessionStore(session_key)
    return test_request


def configure_for_mc3(host):
    if host == 'mc3':
        hostname = 'handcar_mc3'
    elif host == 'mc3-demo':
        hostname = 'handcar_mc3_demo'
    elif host == 'oki-dev':
        hostname = 'handcar_oki_dev'
    configs.service = {
    'id': 'dlkit_runtime_bootstrap_configuration',
    'displayName': 'DLKit Runtime Bootstrap Configuration',
    'description': 'Bootstrap Configuration for DLKit Runtime',
    'parameters':
        {
        'implKey':
            {
            'syntax': 'STRING',
            'displayName': 'Implementation Key',
            'description': 'Implementation key used by Runtime for class loading',
            'values':
                [
                    {'value': 'service', 'priority': 1}
                ]
           },
        'assessmentProviderImpl':
            {
            'syntax': 'STRING',
            'displayName': 'Assessment Provider Implementation',
            'description': 'Implementation for assessment service provider',
            'values':
                [
                    {'value': 'authz_adapter_1', 'priority': 1}
                ]
            },
        'learningProviderImpl':
            {
            'syntax': 'STRING',
            'displayName': 'Learning Provider Implementation',
            'description': 'Implementation for learning service provider',
            'values':
                [
                    {'value': hostname, 'priority': 1}
                ]
            },
        'repositoryProviderImpl':
            {
            'syntax': 'STRING',
            'displayName': 'Repository Provider Implementation',
            'description': 'Implementation for repository service provider',
            'values':
                [
                    {'value': 'authz_adapter_1', 'priority': 1}
                ]
            },
        'commentingProviderImpl':
            {
            'syntax': 'STRING',
            'displayName': 'Commenting Provider Implementation',
            'description': 'Implementation for commenting service provider',
            'values':
                [
                    {'value': 'authz_adapter_1', 'priority': 1}
                ]
            },
        }
    }
    print "Configured to use Handcar on " + host


def clean_up_crosslinks_outcomes():
    main_user = User(username='cjshaw@mit.edu', authenticated=True)
    dummy_req = create_test_request(main_user)
    condition = proxy_session.get_proxy_condition()
    condition.set_http_request(dummy_req)
    proxy = proxy_session.get_proxy(condition)
    lm = runtime.get_service_manager('LEARNING', proxy)

    outcome_genus_type = Type(**{
        'authority': 'MIT-OEIT',
        'namespace': 'mc3-objective',
        'identifier': 'mc3.learning.generic.outcome',
        'display_name': 'Generic Outcome',
        'display_label': 'Generic Outcome',
        'description': "Objective that represents a generic 'phantom' outcome a topic such as Learn 'Topic' or Apply whatever the topic is",
        'domain': 'objective'
    })

    topic_genus_type = Type(**{
        'authority': 'MIT-OEIT',
        'namespace': 'mc3-objective',
        'identifier': 'mc3.learning.topic',
        'display_name': 'Topic',
        'display_label': 'Topic',
        'description': 'Objective that represents a learning topic',
        'domain': 'objective'
    })

    bank_type = Type(**{
        'authority': 'MIT-OEIT',
        'namespace': 'mc3-objectivebank',
        'identifier': 'mc3.learning.objectivebank.crosslinks',
        'display_name': 'Crosslinks',
        'display_label': 'Crosslinks',
        'description': 'These objectives relate to the Crosslinks project',
        'domain': 'objective.bank'
    })

    my_bank = None
    try:
        my_bank = lm.get_objective_banks_by_genus_type(bank_type).next()
    except:
        raise LookupError


    for obj in my_bank.get_root_objectives():
        print "Examining objective: " + str(obj.ident) + ', ' + obj.display_name.text
        if obj.display_name.text == '' and obj.genus_type == outcome_genus_type:
            try:
                print "Deleting objective: " + str(obj.ident)
                my_bank.delete_objective(obj.ident)
            except:
                print 'It has activities attached...what?!?'
                acts = my_bank.get_activities_for_objective(obj.ident)
                for act in acts:
                    try:
                        if act.get_assets().available() == 0:
                            print "Deleting activity: " + str(act.ident)
                            my_bank.delete_activity(act.ident)
                    except:
                        print "Is not an asset-based activity, deleting: " + str(act.ident)
                        my_bank.delete_activity(act.ident)
        elif obj.genus_type == topic_genus_type:
            # clean up the children outcome names so they aren't blank
            my_name = obj.display_name.text
            children = my_bank.get_child_objectives(obj.ident)
            for child in children:
                # if child.genus_type == outcome_genus_type:
                if child.display_name.text == '' and child.genus_type == outcome_genus_type:
                    form = my_bank.get_objective_form_for_update(child.ident)
                    if 'bloom.learn' in str(child.object_map['cognitiveProcessId']):
                        new_name = 'Learn ' + my_name
                    elif 'bloom.apply' in str(child.object_map['cognitiveProcessId']):
                        new_name = 'Apply ' + my_name
                    else:
                        new_name = 'Outcome for ' + my_name
                    form.display_name = new_name
                    updated_objective = my_bank.update_objective(form)
                    print "Updated name of: " + str(child.ident) + ' to ' + new_name
    
