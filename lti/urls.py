from django.conf.urls import patterns, url
from lti import views

urlpatterns = patterns('',
    url(r'^$',
        views.provider,
        name='provider'),
    url(r'^submit/$',
        views.check_response,
        name='check_response'),
    url(r'^retry/$',
        views.retry,
        name='retry'),
    url(r'^return/$',
        views.return_lms,
        name='return_lms'),
)
