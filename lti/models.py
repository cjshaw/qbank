from django.db import models

from oauth_provider.models import Consumer

class Results(models.Model):
    """
    Track the outcome_service, return link and grades with each result_sourcedid
    Link to the right OAuth consumer. Assume a 1-1 mapping between user (from
    LTIAuthenticat) and OAuth consumer.
    """
    consumer = models.ForeignKey(Consumer)
    sourcedid = models.CharField(max_length=500)
    # sourcedid = models.BigIntegerField()
    grade = models.FloatField(default = 0.01)
    return_link = models.CharField(max_length=500)
    outcome_service = models.CharField(max_length=500)
    roles = models.CharField(max_length=250)
    user_id = models.CharField(max_length=50)
    consumer_tool_guid = models.CharField(max_length=250)
    raw_post = models.TextField()
    bank_id = models.CharField(max_length=250)
    offering_id = models.CharField(max_length=250)
