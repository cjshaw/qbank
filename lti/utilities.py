from __future__ import unicode_literals

import os
import re
import glob
import hmac
import time
import base64
import pickle
import random
import string
import requests
import oauth2 as oauth

from django.conf import settings
from django.utils.http import unquote

from copy import deepcopy
from datetime import datetime
from time import mktime
from wsgiref.handlers import format_date_time

from requests_oauthlib import OAuth1, OAuth1Session
from oauthlib.oauth1 import Client

from hashlib import sha1

from http_signature.requests_auth import HTTPSignatureAuth

from ims_lti_py.tool_provider import DjangoToolProvider

class AssessmentRequests(object):
    """
    Modified requests class that automatically handles the authorization part
    """
    def __init__(self, username='taaccct_student', lti_user=None, consumer_guid=None, lti_role=None, lti_bank=None):
        self._pub_key = settings.APP_PUBLIC
        self._pri_key = settings.APP_PRIVATE
        self._assessments_host = settings.ASSESSMENTS
        self._service = settings.ASSESSMENTS_SERVICE
        self._sig_headers = ['request-line','accept','date','host','x-api-proxy']
        self._headers = {
            'Host'                              : str(self._assessments_host),
            'content-type'                      : str('application/json'),
            'Accept'                            : str('application/json'),
            'X-Api-Key'                         : str(self._pub_key),
            'X-Api-Proxy'                       : str(username)
        }
        if lti_user and consumer_guid and lti_role and lti_bank:
            self._headers['LTI-User-ID'] = str(lti_user)
            self._headers['LTI-Tool-Consumer-Instance-GUID'] = str(consumer_guid)
            self._headers['LTI-User-Role'] = str(lti_role)
            self._headers['LTI-Bank'] = str(lti_bank)
            self._sig_headers = ['request-line','accept','date','host','x-api-proxy',
                                 'lti-user-id','lti-tool-consumer-instance-guid',
                                 'lti-user-role','lti-bank']
        self._auth = HTTPSignatureAuth(key_id=self._pub_key,
            secret=self._pri_key,
            algorithm='hmac-sha256',
            headers=self._sig_headers)
        self.url = self._service + 'banks/'

    def get(self, url):
        if '@' not in url:
            url = unquote(url)
        now_headers = deepcopy(self._headers)
        now_headers['Date'] = get_now()
        req = requests.get(url, auth=self._auth, headers=now_headers)
        if req.status_code == 200:
            try:
                return req.json()
            except:
                return req.content
        else:
            raise Exception('Request error ' + str(req.status_code) + ': ' + req.content)

    def post(self, url, data=None):
        if '@' not in url:
            url = unquote(url)
        now_headers = deepcopy(self._headers)
        now_headers['Date'] = get_now()
        req = requests.post(url, data=data,
                            auth=self._auth, headers=now_headers)
        if req.status_code == 200:
            return req.json()
        else:
            raise Exception('Request error ' + str(req.status_code) + ': ' + req.content)

# trying to provide an LTI class with oauth_body_hash
# following this issue: https://github.com/requests/requests-oauthlib/issues/125
class LTIBodyHashClient(Client):
    def get_oauth_params(self, request):
        params = super(LTIBodyHashClient, self).get_oauth_params(request)
        digest = base64.b64encode(sha1(request.body.encode('UTF-8')).digest())
        params.append(('oauth_body_hash', unicode(digest)))
        return params


def clean_id(_id):
    """
    if _id has been url encoded, unencode it
    """
    if '%40' in _id:
        return unquote(_id)
    else:
        return _id

def clean_up_files(sourcedid):
    """
    Files should be at the static_disk_loc, and start with the sourcedid.
    Delete all of them.
    :param sourcedid:
    :return:
    """
    disk_loc = static_disk_loc()
    for filename in glob.glob(disk_loc + '/' + str(sourcedid) + '*'):
        os.remove(filename)

def dequote(sourcedid):
    return re.sub(r'&quot;','"',sourcedid)

def get_now():
    return format_date_time(mktime(datetime.now().timetuple()))

def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def log_error(module, ex):
    import logging
    template = "An exception of type {0} occurred in {1}. Arguments:\n{2!r}"
    message = template.format(type(ex).__name__, module, ex.args)
    logging.info(message)
    return message

def send_results(request, results_handle):
    """
    Following the oauth example:
    https://github.com/simplegeo/python-oauth2
    """
    consumer = results_handle.consumer

    try:
        request_post = pickle.loads(results_handle.raw_post)
    except:
        request_post = pickle.loads(str(results_handle.raw_post))

    return_tool = DjangoToolProvider(consumer.key, consumer.secret, request_post)
    results = return_tool.post_replace_result(results_handle.grade, {'message_identifier': 'from MIT'})

def static_disk_loc():
    try:
        from mod_wsgi import version
        disk_loc = settings.STATIC_ROOT + 'student_files/'
    except:
        disk_loc = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/users/static/student_files/'
    return disk_loc
