import os
import re
import json
import pickle

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseNotFound, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required, user_passes_test

from assessments.auth import LTIAuthentication

from oauth_provider.models import Consumer

from .models import Results
from .utilities import *

@csrf_exempt
def check_response(request):
    """
    Check with Assessment Service if student is right or wrong.
    """
    try:
        if request.method != 'POST':
            raise Exception('Invalid method.')

        answer = json.loads(request.POST['answer'])

        taken_id = request.POST['takenId']
        sourcedid = request.POST['sourcedid']

        # need to reconstruct the LTI request here, so that
        # the username and consumer_guid are passed
        # to the AssessmentService
        results_handle = Results.objects.get(
            sourcedid=sourcedid)
        consumer_guid = results_handle.consumer_tool_guid
        roles = results_handle.roles
        user_id = results_handle.user_id
        bank_id = results_handle.bank_id
        assessments = AssessmentRequests('taaccct_student',
                                         user_id,
                                         consumer_guid,
                                         roles,
                                         bank_id)

        front_face_enum = answer[0]
        side_face_enum = answer[1]
        top_face_enum = answer[2]

        url = assessments.url + clean_id(bank_id) + '/assessmentstaken/' + clean_id(taken_id) + '/submit/'
        payload = json.dumps({
            'integerValues'    : {
                'frontFaceValue' : front_face_enum,
                'sideFaceValue'  : side_face_enum,
                'topFaceValue'   : top_face_enum
            }
        })
        success = assessments.post(url, payload)
        clean_sourcedid = dequote(sourcedid)
        result_handle = Results.objects.get(sourcedid=clean_sourcedid)
        if success['correct']:
            result_handle.grade = 1.00
        else:
            result_handle.grade = 0.01  # for Blackboard Learn? 
            # result_handle.grade = 0.01  # Moodle doesn't seem to handle 0.00?
        result_handle.save()

        response = HttpResponse(json.dumps(success), content_type='application/json')
    except Exception as ex:
        log_error('views.check_response()', ex)
        response = HttpResponseNotFound()
    finally:
        return response

@csrf_exempt
def provider(request):
    """
    Take the LTI POST request and verify that the signature matches,
    using OAuth provider
    """
    try:
        if request.method != 'POST':
            raise Exception('Invalid method.')

        question_string = manip = front = side = top = None

        consumer_key = settings.LTI_KEY
        consumer_secret = settings.LTI_SECRET

        tool = DjangoToolProvider(consumer_key, consumer_secret, request.POST)
        if not tool.is_valid_request(request):
            raise Exception('Bad key / signature')

        # get the problem from the AssessmentService and
        # send it to the page
        bank_id = request.POST['custom_bankid']
        offering_id = request.POST['custom_offeringid']

        if request.POST['tool_consumer_info_product_family_code'] == 'moodle':
            sourcedid = json.loads(request.POST['lis_result_sourcedid'])['data']['launchid']
        else:
            if 'lis_result_sourcedid' in request.POST:
                sourcedid = request.POST['lis_result_sourcedid'] 
            else:
                sourcedid = id_generator()

        if 'lis_outcome_service_url' in request.POST:
            outcome_service = request.POST['lis_outcome_service_url']
        else:
            outcome_service = ''
        return_link = request.POST['launch_presentation_return_url']

        user_id = request.POST['user_id']

        if 'roles' in request.POST:
            roles = request.POST['roles']
        else:
            roles = 'Learner'


        consumer_guid = request.POST['tool_consumer_instance_guid']
        assessments = AssessmentRequests('taaccct_student',
                                         user_id,
                                         consumer_guid,
                                         roles,
                                         bank_id)

        # Using the offering_id, create an assessment taken for this user.
        # [7/9/14 10:53:42 AM] Jeff Merriman: Also, you can now try creating
        # AssessmentTaken objects.  The taker id of the AssessmentTaken will
        # default to the logged in effective agent id
        # [7/9/14 10:57:16 AM] Jeff Merriman: you can also now try all the
        # AssessmentSession methods.  remember that before you can start
        # getting Questions, you will need to get_first_assessment_section
        # and use the returned AssessmentSections Id to then
        # get_first_question(assessment_section_id), or for
        # get_questions(assessment_section_id)

        offering_url = assessments.url + clean_id(bank_id) + '/assessmentsoffered/' + clean_id(offering_id) + '/'

        # create the assessment taken
        taken_url = offering_url + 'assessmentstaken/'
        new_taken = assessments.post(taken_url)
        taken_id = new_taken['id']

        # get the first question for the taken
        take_url = assessments.url + clean_id(bank_id) + '/assessmentstaken/' + clean_id(taken_id) + '/take/'
        question = assessments.get(take_url)  # a dict, which we will have to test to see the type
        question_string = question['text']['text']

        # if question is an ortho type, need to get the files
        if 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU' in question['recordTypeIds']:
            files_url = assessments.url + clean_id(bank_id) + '/assessmentstaken/' + clean_id(taken_id) + '/files/'
            files = assessments.get(files_url)
        else:
            files = {}

        # temporarily store these files in the static dir
        disk_loc = static_disk_loc()
        static_url = settings.STATIC_URL + 'student_files/'

        context = {
            'bankId'       : bank_id,
            'takenId'      : taken_id,
            'question'      : json.dumps(question_string),
            'sourcedid'     : sourcedid
        }

        for file_name, file_data in files.iteritems():
            if file_name == 'manip':
                static_name = str(sourcedid) + '_manip.unity3d'
                context[file_name] = static_url + static_name
            else:
                static_name = str(sourcedid) + '_' + file_name + 'view.jpg'
                context[file_name + 'View'] = static_url + static_name

            write_name = disk_loc + static_name
            dir = os.path.dirname(write_name)
            if not os.path.exists(dir):
                os.makedirs(dir)

            with open(write_name, 'wb+') as file:
                file.write(base64.b64decode(file_data))
                file.close()

        # find the right consumer for the given user.
        # assume 1-1 mapping between OAuth consumer and user
        consumer = Consumer.objects.get(key=consumer_key, secret=consumer_secret)
        # consumer = Consumer.objects.get(user=user, status=2)

        # From Ivan's code, save the POST to the session
        # needed for the DjangoToolProvider to recreate
        # the results XML?
        request_dict = {}
        for key in request.POST.keys():
            request_dict[key] = request.POST[key]

        # save the LTI response url in the LTI Results table
        new_result, created = Results.objects.get_or_create(
            consumer=consumer,
            sourcedid=sourcedid,
            outcome_service=outcome_service,
            consumer_tool_guid=consumer_guid,
            roles=roles,
            user_id=user_id,
            bank_id=bank_id,
            offering_id=offering_id
        )
        new_result.return_link = return_link
        new_result.raw_post = pickle.dumps(request_dict)
        new_result.save()
        return render_to_response('lti/dashboard.html',
                                  context,
                                  RequestContext(request))
    except Exception as ex:
        log_error('lti.views.provider()', ex)
        return HttpResponseNotFound()

@csrf_exempt
def retry(request):
    """
    Create a new taken and send it to the page...
    """
    try:
        if request.method != 'GET':
            raise Exception('Invalid method.')

        sourcedid = request.GET['sourcedid']

        # need to reconstruct the LTI request here, so that
        # the username and consumer_guid are passed
        # to the AssessmentService
        results_handle = Results.objects.get(
            sourcedid=sourcedid)
        consumer_guid = results_handle.consumer_tool_guid
        roles = results_handle.roles
        user_id = results_handle.user_id
        bank_id = results_handle.bank_id
        offering_id = results_handle.offering_id
        assessments = AssessmentRequests('taaccct_student',
                                         user_id,
                                         consumer_guid,
                                         roles,
                                         bank_id)
        offering_url = assessments.url + clean_id(bank_id) + '/assessmentsoffered/' + clean_id(offering_id) + '/'

        # create the assessment taken
        taken_url = offering_url + 'assessmentstaken/'
        new_taken = assessments.post(taken_url)

        return HttpResponse(json.dumps(new_taken))
    except Exception as ex:
        log_error('lti.views.retry()', ex)
        return HttpResponseNotFound()

@csrf_exempt
def return_lms(request):
    """
    Return to the LMS
    Send the grade back to the outcomes endpoint
        * Need to OAuth sign the body of this XML structure...
        https://canvas.instructure.com/doc/api/file.assignment_tools.html
        http://www.imsglobal.org/LTI/v1p1p1/ltiIMGv1p1p1.html#_Toc330273030
        https://github.com/simplegeo/python-oauth2
    """
    try:
        if request.method != 'GET':
            raise Exception('Invalid method.')

        sourcedid = request.GET['sourcedid']
        results_handle = Results.objects.get(sourcedid=dequote(sourcedid))

        # Delete the image files on the server, using sourcedid
        clean_up_files(sourcedid)

        # Send grades back to LTI Consumer
        # put this in a try / except because Blackboard
        # may not take grades...no sourcedid for "web link"
        # providers
        try:
            send_results(request, results_handle)
        except:
            pass

        # return to LTI Consumer
        return_url = results_handle.return_link
        
        # delete this from LTI table...we don't want to store these things
        results_handle.delete()

        return HttpResponse(return_url)
    except Exception as ex:
        log_error('lti.views.return_lms()', ex)
        return HttpResponseNotFound()

