#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    try:
        del os.environ['DJANGO_SETTINGS_MODULE']
    except KeyError:
        pass

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "assessments_main.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
