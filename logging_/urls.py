from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from logging_ import views

urlpatterns = patterns('',
    url(r'^$',
        views.LoggingService.as_view()),
    url(r'^logs/?$',
        views.LogsList.as_view()),
    url(r'^logs/(?P<log_id>[-.:@%\d\w]+)/?$',
        views.LogDetails.as_view()),
    url(r'^logs/(?P<log_id>[-.:@%\d\w]+)/logentries/?$',
        views.LogEntriesList.as_view()),
    url(r'^logs/(?P<log_id>[-.:@%\d\w]+)/logentries/(?P<entry_id>[-.:@%\d\w]+)/?$',
        views.LogEntryDetails.as_view()),
    url(r'^docs/?$',
        views.Documentation.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)
