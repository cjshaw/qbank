import json

from bson.errors import InvalidId

from django.template import RequestContext
from django.shortcuts import render_to_response

from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from dlkit.runtime.errors import PermissionDenied, InvalidArgument, IllegalState, NotFound
from dlkit.runtime.primordium import Id, Type, DateTime

from qbank.views import DLKitSessionsManager

from dlkit.records.registry import LOG_ENTRY_RECORD_TYPES

from utilities import general as gutils
from utilities import resource as resutils

TEXT_BLOB_RECORD_TYPE = Type(**LOG_ENTRY_RECORD_TYPES['text-blob'])


class Documentation(DLKitSessionsManager):
    """
    Shows the user documentation for talking to the RESTful service
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        return render_to_response('logging/documentation.html',
                                  {},
                                  RequestContext(request))


class LogDetails(DLKitSessionsManager):
    """
    Shows details for a specific log.
    api/v2/loggnig/logs/<log_id>/

    GET, PUT, DELETE
    PUT will update the log. Only changed attributes need to be sent.
    DELETE will remove the log.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       PUT {"name" : "a new log"}
    """
    def delete(self, request, log_id, format=None):
        try:
            self.logm.delete_log(gutils.clean_id(log_id))
            return gutils.DeletedResponse()
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)
        except IllegalState as ex:
            modified_ex = type(ex)('Log is not empty.')
            gutils.handle_exceptions(modified_ex)

    def get(self, request, log_id, format=None):
        try:
            log = self.logm.get_log(gutils.clean_id(log_id))
            log = gutils.convert_dl_object(log)
            log = gutils.add_links(request,
                                   log,
                                   {
                                       'logEntries': 'logentries/'
                                   })
            return Response(log)
        except (PermissionDenied, InvalidId, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, log_id, format=None):
        try:
            form = self.logm.get_log_form_for_update(gutils.clean_id(log_id))

            gutils.verify_at_least_one_key_present(self.data, ['name', 'description'])

            # should work for a form or json data
            if 'name' in self.data:
                form.display_name = self.data['name']
            if 'description' in self.data:
                form.description = self.data['description']

            updated_log = self.logm.update_log(form)
            updated_log = gutils.convert_dl_object(updated_log)
            updated_log = gutils.add_links(request,
                                           updated_log,
                                           {
                                               'logEntries': 'logentries/'
                                           })

            return gutils.UpdatedResponse(updated_log)
        except (PermissionDenied, KeyError, InvalidArgument, NotFound) as ex:
            gutils.handle_exceptions(ex)


class LogsList(DLKitSessionsManager):
    """
    List all available logs.
    api/v2/logging/logs/

    POST allows you to create a new log, requires two parameters:
      * name
      * description

    Alternatively, if you provide an assessment bank ID,
    the log will be orchestrated to have a matching internal identifier.
    The name and description will be set for you, but can optionally be set if
    provided.
      * bankId
      * name (optional)
      * description (optional)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"name" : "a new log",
       "description" : "this is a test"}

       OR
       {"bankId": "assessment.Bank%3A5547c37cea061a6d3f0ffe71%40cs-macbook-pro"}
    """

    def get(self, request, format=None):
        """
        List all available logs
        """
        try:
            logs = self.logm.logs
            logs = gutils.extract_items(request, logs)
            return Response(logs)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, format=None):
        """
        Create a new log, if authorized

        """
        try:
            if 'bankId' not in self.data:
                gutils.verify_keys_present(self.data, ['name', 'description'])
                form = self.logm.get_log_form_for_create([])
                finalize_method = self.logm.create_log
            else:
                log = self.logm.get_log(Id(self.data['bankId']))
                form = self.logm.get_log_form_for_update(log.ident)
                finalize_method = self.logm.update_log

            if 'name' in self.data:
                form.display_name = self.data['name']
            if 'description' in self.data:
                form.description = self.data['description']

            new_log = gutils.convert_dl_object(finalize_method(form))

            return gutils.CreatedResponse(new_log)
        except (PermissionDenied, InvalidArgument, NotFound, KeyError) as ex:
            gutils.handle_exceptions(ex)


class LogEntriesList(DLKitSessionsManager):
    """
    Get or add log entry
    api/v2/logging/logs/<log_id>/logentries/

    GET, POST
    GET to view current log entries. Query params agent_id, start_date, end_date. Date
        queries should be in mm-dd-yyyy format.
    POST to create a new log entry

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"data" : "<JSON string blob, or whatever text blob you want>"}
    """

    def get(self, request, log_id, format=None):
        try:
            log = self.logm.get_log(gutils.clean_id(log_id))

            query_terms = ['agent_id', 'start_date',
                           'end_date', 'keyword']
            if any(t in self.data for t in query_terms):
                log_entry_query = log.get_log_entry_query()

                if 'agent_id' in self.data:
                    log_entry_query.match_agent_id(resutils.get_agent_id(self.data['agent_id']),
                                                   True)

                if any(t in self.data for t in ['start_date', 'end_date']):
                    # we only want the date with 00:00:00 time as the default start
                    start_time = DateTime.strptime(DateTime.utcnow().strftime('%m-%d-%Y'),
                                                   '%m-%d-%Y')
                    end_time = DateTime.utcnow()
                    if 'start_date' in self.data and 'end_date' not in self.data:
                        start_time = DateTime.strptime(self.data['start_date'],
                                                       '%m-%d-%Y')
                    elif 'end_date' in self.data and 'start_date' not in self.data:
                        end_time = DateTime.strptime(self.data['end_date'] + ' 23:59:59',
                                                     '%m-%d-%Y %H:%M:%S')
                    else:
                        start_time = DateTime.strptime(self.data['start_date'],
                                                       '%m-%d-%Y')
                        end_time = DateTime.strptime(self.data['end_date'] + ' 23:59:59',
                                                     '%m-%d-%Y %H:%M:%S')

                    log_entry_query.match_timestamp(start_time, end_time, True)

                if 'keyword' in self.data:
                    if not isinstance(self.data['keyword'], list):
                        self.data['keyword'] = [self.data['keyword']]
                    for keyword in self.data['keyword']:
                        log_entry_query.match_keyword(keyword, match=True)

                entries = log.get_log_entries_by_query(log_entry_query)
            else:
                entries = log.get_log_entries()

            data = gutils.extract_items(request, entries)

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, log_id, format=None):
        try:
            gutils.verify_keys_present(self.data, ['data'])

            log = self.logm.get_log(gutils.clean_id(log_id))
            form = log.get_log_entry_form_for_create([TEXT_BLOB_RECORD_TYPE])

            if isinstance(self.data['data'], dict):
                blob = json.dumps(self.data['data'])
            else:
                blob = str(self.data['data'])

            form.set_text(blob)
            entry = log.create_log_entry(form)

            return gutils.CreatedResponse(entry.object_map)
        except (PermissionDenied, InvalidArgument, IllegalState, KeyError) as ex:
            gutils.handle_exceptions(ex)


class LogEntryDetails(DLKitSessionsManager):
    """
    Get log entry details
    api/v2/logging/logs/<log_id>/logentries/<entry_id>/

    GET, PUT, DELETE
    PUT to modify an existing log entry (name, score / grade, etc.).
        Include only the changed parameters.
    DELETE to remove the log entry.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"data" : "foo"}
    """

    def delete(self, request, log_id, entry_id, format=None):
        try:
            log = self.logm.get_log(gutils.clean_id(log_id))
            log.delete_log_entry(gutils.clean_id(entry_id))

            return gutils.DeletedResponse()
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, log_id, entry_id, format=None):
        try:
            log = self.logm.get_log(gutils.clean_id(log_id))
            entry = log.get_log_entry(gutils.clean_id(entry_id))
            entry_map = entry.object_map

            entry_map.update({
                '_links': {
                    'self': gutils.build_safe_uri(request),
                }
            })

            return Response(entry_map)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, log_id, entry_id, format=None):
        try:
            gutils.verify_at_least_one_key_present(self.data,
                                                   ['data'])

            log = self.logm.get_log(gutils.clean_id(log_id))

            form = log.get_log_entry_form_for_update(gutils.clean_id(entry_id))

            if 'data' in self.data:
                if isinstance(self.data['data'], dict):
                    blob = json.dumps(self.data['data'])
                else:
                    blob = str(self.data['data'])
                form.set_text(blob)

            log.update_log_entry(form)

            entry = log.get_log_entry(gutils.clean_id(entry_id))

            return gutils.UpdatedResponse(entry.object_map)
        except (PermissionDenied, InvalidArgument, KeyError) as ex:
            gutils.handle_exceptions(ex)


class LoggingService(DLKitSessionsManager):
    """
    List all available logging services.
    api/v2/logging/
    """

    def get(self, request, format=None):
        """
        List all available logging services.
        """
        data = {}
        data = gutils.add_links(request,
                                data,
                                {
                                    'logs': 'logs/',
                                    'documentation': 'docs/'
                                })
        return Response(data)

