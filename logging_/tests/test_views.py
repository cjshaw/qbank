import os
import datetime
import json
import envoy

from minimocktest import MockTestCase
from django.conf import settings
from rest_framework.test import APITestCase, APIClient

from assessments_users.models import APIUser

from copy import deepcopy

from utilities import general as gutils
from utilities.testing import create_test_request, QBankBaseTest, add_user_authz_to_settings,\
    create_test_bank

from dlkit.runtime.primordium import Id, Type, DateTime

from dlkit.records.registry import LOG_ENTRY_RECORD_TYPES

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

TEXT_BLOB_RECORD_TYPE = Type(**LOG_ENTRY_RECORD_TYPES['text-blob'])


class DjangoTestCase(QBankBaseTest):
    """
    """
    def create_new_log(self):
        payload = {
            'name': 'my new log',
            'description': 'for testing with'
        }
        req = self.new_log_post(payload)
        return self.json(req)

    def new_log_post(self, payload):
        url = self.url + 'logs/'
        return self.post(url, payload)

    def setUp(self):
        super(DjangoTestCase, self).setUp()
        self.url = '/api/v2/logging/'
        self.username = 'instructor@mit.edu'
        self.password = 'jinxem'
        self.user = APIUser.objects.create_user(username=self.username,
                                                password=self.password)
        self.student_name = 'student@mit.edu'
        self.student_password = 'blahblah'
        self.student = APIUser.objects.create_user(username=self.student_name,
                                                   password=self.student_password)
        self.req = create_test_request(self.user)

        self.test_file = open(ABS_PATH + '/tests/files/Flexure_structure_with_hints.pdf')

    def setup_entry(self, log_id, data):
        gutils.activate_managers(self.req)
        logm = gutils.get_session_data(self.req, 'logm')

        log = logm.get_log(Id(log_id))

        form = log.get_log_entry_form_for_create([TEXT_BLOB_RECORD_TYPE])
        form.set_text(data)

        new_entry = log.create_log_entry(form)

        return new_entry.object_map

    def tearDown(self):
        super(DjangoTestCase, self).tearDown()
        self.test_file.close()


class BasicServiceTests(DjangoTestCase):
    """Test the views for getting the basic service calls

    """
    def setUp(self):
        super(BasicServiceTests, self).setUp()

    def tearDown(self):
        super(BasicServiceTests, self).tearDown()

    def test_authenticated_users_can_see_available_services(self):
        url = self.url
        req = self.get(url)
        self.ok(req)
        self.message(req, 'documentation')
        self.message(req, 'logs')

    def test_non_authenticated_users_cannot_see_available_services(self):
        url = self.url
        req = self.client.get(url)  # don't use self.get() to keep it unauthenticated
        self.code(req, 403)

    def test_instructors_can_get_list_of_logs(self):
        url = self.url + 'logs/'
        req = self.get(url)
        self.ok(req)
        self.message(req, '"count": 0')

    def test_learners_cannot_see_list_of_logs(self):
        url = self.url + 'logs/'
        req = self.get(url, non_instructor=True)
        self.code(req, 403)
        # self.ok(req)
        # self.message(req, '"count": 0')


class DocumentationTests(DjangoTestCase):
    """Test the views for getting the documentation

    """
    def setUp(self):
        super(DocumentationTests, self).setUp()

    def tearDown(self):
        super(DocumentationTests, self).tearDown()

    def test_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url)
        self.ok(req)
        self.message(req, 'Documentation for MIT Logging Service, V2')

    def test_non_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.client.get(url)  # don't use self.get() to keep it unauthenticated
        self.ok(req)
        self.message(req, 'Documentation for MIT Logging Service, V2')

    def test_student_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url, non_instructor=True)
        self.ok(req)
        self.message(req, 'Documentation for MIT Logging Service, V2')


class LogCrUDTests(DjangoTestCase):
    """Test the views for log crud

    """
    def num_logs(self, val):
        gutils.activate_managers(self.req)
        logm = gutils.get_session_data(self.req, 'logm')

        self.assertEqual(
            logm.logs.available(),
            val
        )

    def setUp(self):
        super(LogCrUDTests, self).setUp()
        # also need a test assessment bank here to do orchestration with
        self.assessment_bank = create_test_bank(self)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self.assessment_bank['id'])

        self.bad_log_id = 'assessment.Bank%3A55203f0be7dde0815228bb41%40bazzim.MIT.EDU'

    def tearDown(self):
        super(LogCrUDTests, self).tearDown()

    def test_can_create_new_log(self):
        self.num_logs(0)
        payload = {
            'name': 'my new log',
            'description': 'for testing with'
        }
        req = self.new_log_post(payload)
        self.created(req)
        log = self.json(req)
        self.assertEqual(
            log['displayName']['text'],
            payload['name']
        )
        self.assertEqual(
            log['description']['text'],
            payload['description']
        )
        self.num_logs(1)

    def test_can_create_orchestrated_log_with_default_attributes(self):
        self.num_logs(0)
        url = self.url + 'logs/'
        payload = {
            'bankId': self.assessment_bank['id']
        }
        req = self.post(url, payload)
        self.created(req)
        log = self.json(req)
        self.assertEqual(
            log['displayName']['text'],
            'Orchestrated assessment Log'
        )
        self.assertEqual(
            log['description']['text'],
            'Orchestrated assessment Log'
        )
        self.assertEqual(
            Id(self.assessment_bank['id']).identifier,
            Id(log['id']).identifier
        )
        self.num_logs(1)

    def test_can_create_orchestrated_log_and_set_attributes(self):
        url = self.url + 'logs/'
        payload = {
            'bankId': self.assessment_bank['id'],
            'name': 'my new orchestra',
            'description': 'for my assessment bank'
        }
        req = self.post(url, payload)
        self.created(req)
        log = self.json(req)
        self.assertEqual(
            log['displayName']['text'],
            payload['name']
        )
        self.assertEqual(
            log['description']['text'],
            payload['description']
        )
        self.assertEqual(
            Id(self.assessment_bank['id']).identifier,
            Id(log['id']).identifier
        )

    def test_missing_parameters_throws_exception_on_create(self):
        self.num_logs(0)

        url = self.url + 'logs/'
        basic_payload = {
            'name': 'my new log',
            'description': 'for testing with'
        }
        blacklist = ['name', 'description']

        for item in blacklist:
            payload = deepcopy(basic_payload)
            del payload[item]
            req = self.post(url, payload)
            self.code(req, 500)
            self.message(req,
                         '"' + item + '" required in input parameters but not provided.')

        self.num_logs(0)

    def test_can_get_log_details(self):
        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        url = self.url + 'logs/' + str(log['id'])
        req = self.get(url)
        self.ok(req)
        log_details = self.json(req)
        for attr, val in log.iteritems():
            self.assertEqual(
                val,
                log_details[attr]
            )
        self.message(req, '"logEntries":')

    def test_invalid_log_id_throws_exception(self):
        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        url = self.url + 'logs/x'
        req = self.get(url)
        self.code(req, 500)
        self.message(req, 'Invalid ID.')

    def test_bad_log_id_throws_exception(self):
        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        url = self.url + 'logs/' + self.bad_log_id
        req = self.get(url)
        self.code(req, 500)
        self.message(req, 'Object not found.')

    def test_can_delete_log(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.num_logs(1)

        url = self.url + 'logs/' + str(log['id'])
        req = self.delete(url)
        self.deleted(req)

        self.num_logs(0)

    def test_trying_to_delete_log_with_log_entries_throws_exception(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.setup_entry(log['id'], 'foo')

        self.num_logs(1)

        url = self.url + 'logs/' + str(log['id'])
        req = self.delete(url)
        self.code(req, 500)
        self.message(req, 'Log is not empty.')

        self.num_logs(1)

    def test_trying_to_delete_log_with_invalid_id_throws_exception(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.num_logs(1)

        url = self.url + 'logs/' + self.bad_log_id
        req = self.delete(url)
        self.code(req, 500)
        self.message(req, 'Object not found.')

        self.num_logs(1)

    def test_can_update_log(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.num_logs(1)

        url = self.url + 'logs/' + str(log['id'])

        test_cases = [('name', 'a new name'),
                      ('description', 'foobar')]
        for case in test_cases:
            payload = {
                case[0]: case[1]
            }
            req = self.put(url, payload)
            self.updated(req)
            updated_log = self.json(req)
            if case[0] == 'name':
                self.assertEqual(
                    updated_log['displayName']['text'],
                    case[1]
                )
            else:
                self.assertEqual(
                    updated_log['description']['text'],
                    case[1]
                )

        self.num_logs(1)

    def test_update_with_invalid_id_throws_exception(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.num_logs(1)

        url = self.url + 'logs/' + self.bad_log_id

        test_cases = [('name', 'a new name'),
                      ('description', 'foobar')]
        for case in test_cases:
            payload = {
                case[0]: case[1]
            }
            req = self.put(url, payload)
            self.code(req, 500)
            self.message(req, 'Object not found.')

        self.num_logs(1)

    def test_update_with_no_params_throws_exception(self):
        self.num_logs(0)

        log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log['id'])

        self.num_logs(1)

        url = self.url + 'logs/' + str(log['id'])

        test_cases = [('foo', 'bar'),
                      ('bankId', 'foobar')]
        for case in test_cases:
            payload = {
                case[0]: case[1]
            }
            req = self.put(url, payload)
            self.code(req, 500)
            self.message(req,
                         'At least one of the following must be passed in: ' +
                         '["name", "description"]')

        self.num_logs(1)
        req = self.get(url)
        log_fresh = self.json(req)

        params_to_test = ['id', 'displayName', 'description']
        for param in params_to_test:
            self.assertEqual(
                log[param],
                log_fresh[param]
            )

    def test_student_cannot_view_logs(self):
        self.create_new_log()
        self.num_logs(1)

        url = self.url + 'logs/'
        req = self.get(url, non_instructor=True)
        self.code(req, 403)


class LogEntryCrUDTests(DjangoTestCase):
    """Test the views for log entries crud

    """
    def num_entries(self, val):
        gutils.activate_managers(self.req)
        logm = gutils.get_session_data(self.req, 'logm')

        log = logm.get_log(Id(self.log['id']))
        self.assertEqual(
            log.get_log_entries().available(),
            val
        )

    def setUp(self):
        super(LogEntryCrUDTests, self).setUp()
        self.bad_log_id = 'assessment.Bank%3A55203f0be7dde0815228bb41%40bazzim.MIT.EDU'
        self.log = self.create_new_log()

        test_file = '/tests/files/ps_2015_beam_2gages.pdf'
        test_file2 = '/tests/files/Backstage_v2_quick_guide.docx'

        self.test_file = open(ABS_PATH + test_file, 'r')
        self.test_file2 = open(ABS_PATH + test_file2, 'r')

        self.student2_name = 'astudent2'
        self.student2_password = 'blahblah'
        self.student2 = APIUser.objects.create_user(username=self.student2_name,
                                                    password=self.student2_password)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self.log['id'])
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=self.log['id'])
        add_user_authz_to_settings('student',
                                   self.student2_name,
                                   catalog_id=self.log['id'])

    def tearDown(self):
        super(LogEntryCrUDTests, self).tearDown()
        self.test_file.close()
        self.test_file2.close()

    def test_can_get_log_entries(self):
        self.num_entries(0)
        self.setup_entry(self.log['id'], 'foo')
        self.num_entries(1)
        url = '{0}logs/{1}/logentries/'.format(self.url,
                                               self.log['id'])
        req = self.get(url)
        self.ok(req)
        entries = self.json(req)['data']['results']
        self.assertEqual(
            len(entries),
            1
        )
        self.assertEqual(
            entries[0]['text']['text'],
            'foo'
        )

    def test_can_create_log_entry(self):
        self.num_entries(0)
        url = '{0}logs/{1}/logentries/'.format(self.url,
                                               self.log['id'])

        payload = {
            'data': json.dumps({"actor": "agentId"})
        }

        req = self.post(url, payload)
        self.created(req)

        data = self.json(req)
        self.assertEqual(
            data['text']['text'],
            payload['data']
        )
        self.num_entries(1)

    def test_creating_log_entry_requires_data_parameter(self):
        self.num_entries(0)
        url = '{0}logs/{1}/logentries/'.format(self.url,
                                               self.log['id'])

        payload = {
            'foo': json.dumps({"actor": "agentId"})
        }

        req = self.post(url, payload)
        self.code(req, 500)
        self.message(req,
                     '"data" required in input parameters but not provided.')
        self.num_entries(0)

    def test_can_update_log_entry(self):
        self.num_entries(0)
        entry = self.setup_entry(self.log['id'], "foo")

        url = '{0}logs/{1}/logentries/{2}/'.format(self.url,
                                                   self.log['id'],
                                                   entry['id'])

        test_cases = [
            {'data': json.dumps({"baz": "zim"})}
        ]

        for payload in test_cases:
            req = self.put(url, payload)
            self.updated(req)
            data = self.json(req)

            self.assertEqual(
                data['id'],
                entry['id']
            )
            key = payload.keys()[0]
            if key == 'data':
                self.assertEqual(
                    data['text']['text'],
                    payload[key]
                )
            else:
                self.assertEqual(
                    data[key],
                    payload[key]
                )

        self.num_entries(1)

    def test_can_delete_log_entry(self):
        self.num_entries(0)
        entry = self.setup_entry(self.log['id'], "foo")

        self.num_entries(1)

        url = '{0}logs/{1}/logentries/{2}/'.format(self.url,
                                                   self.log['id'],
                                                   entry['id'])

        req = self.delete(url)
        self.deleted(req)

        self.num_entries(0)


class LogEntryQueryTests(DjangoTestCase):
    """Test the views for log entries query

    """
    def num_entries(self, val):
        gutils.activate_managers(self.req)
        logm = gutils.get_session_data(self.req, 'logm')

        log = logm.get_log(Id(self.log['id']))
        self.assertEqual(
            log.get_log_entries().available(),
            val
        )

    def setUp(self):
        super(LogEntryQueryTests, self).setUp()
        self.bad_log_id = 'assessment.Bank%3A55203f0be7dde0815228bb41%40bazzim.MIT.EDU'
        self.log = self.create_new_log()

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self.log['id'])

        self.login()

    def tearDown(self):
        super(LogEntryQueryTests, self).tearDown()

    def test_can_query_by_agent_id(self):
        entry = self.setup_entry(self.log['id'], 'foo')
        url = '{0}logs/{1}/logentries?agent_id={2}'.format(self.url,
                                                           self.log['id'],
                                                           self.username)

        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            data['data']['results'][0]['id'],
            entry['id']
        )

        url = '{0}logs/{1}/logentries?agent_id={2}'.format(self.url,
                                                           self.log['id'],
                                                           self.student_name)

        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

    def test_can_query_by_timestamp(self):
        def test_time_pair(_start, _end, expected_matches):
            _url = '{0}logs/{1}/logentries?start_date={2}&end_date={3}'.format(self.url,
                                                                               self.log['id'],
                                                                               _start,
                                                                               _end)

            req = self.get(_url)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['count'],
                len(expected_matches)
            )
            if len(expected_matches) > 0:
                for ind, match in enumerate(expected_matches):
                    self.assertEqual(
                        data['data']['results'][ind]['id'],
                        match['id']
                    )

        entry = self.setup_entry(self.log['id'], 'foo')
        yesterday = (DateTime.now() - datetime.timedelta(days=1)).strftime('%m-%d-%Y')
        today = DateTime.now().strftime('%m-%d-%Y')
        tomorrow = (DateTime.now() + datetime.timedelta(days=1)).strftime('%m-%d-%Y')

        test_time_pair(yesterday, today, [entry])
        test_time_pair(today, yesterday, [])
        test_time_pair(yesterday, tomorrow, [entry])
        test_time_pair(today, tomorrow, [entry])
        test_time_pair(tomorrow, today, [])
        test_time_pair(today, today, [entry])
        test_time_pair(tomorrow, yesterday, [])

    def test_can_query_by_timestamp_and_agent_id(self):
        def test_agent_and_time_pair(_agent, _start, _end, expected_matches):
            _url = '{0}logs/{1}/logentries?agent_id={2}&start_date={3}&end_date={4}'.format(self.url,
                                                                                            self.log['id'],
                                                                                            _agent,
                                                                                            _start,
                                                                                            _end)

            req = self.get(_url)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['count'],
                len(expected_matches)
            )
            if len(expected_matches) > 0:
                for ind, match in enumerate(expected_matches):
                    self.assertEqual(
                        data['data']['results'][ind]['id'],
                        match['id']
                    )
        entry = self.setup_entry(self.log['id'], 'foo')

        yesterday = (DateTime.now() - datetime.timedelta(days=1)).strftime('%m-%d-%Y')
        today = DateTime.now().strftime('%m-%d-%Y')
        tomorrow = (DateTime.now() + datetime.timedelta(days=1)).strftime('%m-%d-%Y')

        test_agent_and_time_pair(self.username, yesterday, today, [entry])
        test_agent_and_time_pair(self.student_name, yesterday, today, [])
        test_agent_and_time_pair(self.username, today, today, [entry])
        test_agent_and_time_pair(self.student_name, today, today, [])
        test_agent_and_time_pair(self.username, today, tomorrow, [entry])
        test_agent_and_time_pair(self.student_name, today, tomorrow, [])
        test_agent_and_time_pair(self.username, yesterday, tomorrow, [entry])
        test_agent_and_time_pair(self.student_name, yesterday, tomorrow, [])

        test_agent_and_time_pair(self.username, tomorrow, today, [])
        test_agent_and_time_pair(self.student_name, tomorrow, today, [])
        test_agent_and_time_pair(self.username, tomorrow, yesterday, [])
        test_agent_and_time_pair(self.student_name, tomorrow, yesterday, [])
        test_agent_and_time_pair(self.username, today, yesterday, [])
        test_agent_and_time_pair(self.student_name, today, yesterday, [])

    def test_can_query_by_start_date_only(self):
        def test_time(_start, expected_matches):
            _url = '{0}logs/{1}/logentries?start_date={2}'.format(self.url,
                                                                  self.log['id'],
                                                                  _start)

            req = self.get(_url)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['count'],
                len(expected_matches)
            )
            if len(expected_matches) > 0:
                for ind, match in enumerate(expected_matches):
                    self.assertEqual(
                        data['data']['results'][ind]['id'],
                        match['id']
                    )

        entry = self.setup_entry(self.log['id'], 'foo')
        yesterday = (DateTime.now() - datetime.timedelta(days=1)).strftime('%m-%d-%Y')
        today = DateTime.now().strftime('%m-%d-%Y')
        tomorrow = (DateTime.now() + datetime.timedelta(days=1)).strftime('%m-%d-%Y')

        test_time(yesterday, [entry])
        test_time(today, [entry])
        test_time(tomorrow, [])

    def test_can_query_by_end_date_only(self):
        def test_time(_end, expected_matches):
            _url = '{0}logs/{1}/logentries?end_date={2}'.format(self.url,
                                                                self.log['id'],
                                                                _end)

            req = self.get(_url)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['count'],
                len(expected_matches)
            )
            if len(expected_matches) > 0:
                for ind, match in enumerate(expected_matches):
                    self.assertEqual(
                        data['data']['results'][ind]['id'],
                        match['id']
                    )

        entry = self.setup_entry(self.log['id'], 'foo')
        yesterday = (DateTime.now() - datetime.timedelta(days=1)).strftime('%m-%d-%Y')
        today = DateTime.now().strftime('%m-%d-%Y')
        tomorrow = (DateTime.now() + datetime.timedelta(days=1)).strftime('%m-%d-%Y')

        test_time(yesterday, [])
        test_time(today, [entry])
        test_time(tomorrow, [entry])

    def test_students_cannot_search(self):
        entry = self.setup_entry(self.log['id'], 'foo')
        url = '{0}logs/{1}/logentries?agent_id={2}'.format(self.url,
                                                           self.log['id'],
                                                           self.username)

        req = self.get(url, non_instructor=True)
        self.code(req, 403)

    def test_can_do_keyword_query(self):
        def test_words(_words, expected_matches):
            _url = '{0}logs/{1}/logentries?{2}'.format(self.url,
                                                       self.log['id'],
                                                       "&".join(["keyword={0}".format(_w) for _w in _words]))

            req = self.get(_url)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['count'],
                len(expected_matches)
            )
            if len(expected_matches) > 0:
                for ind, match in enumerate(expected_matches):
                    self.assertEqual(
                        data['data']['results'][ind]['id'],
                        match['id']
                    )

        entry = self.setup_entry(self.log['id'], json.dumps({
            "blah": "foobar"
        }))

        test_words(["foo"], [entry])
        test_words(["foo", "bar"], [entry])
        test_words(["foo", "baz"], [])
        test_words(["baz"], [])
        test_words(["blah"], [entry])
