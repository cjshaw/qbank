from dlkit.mongo.assessment.managers import AssessmentManager
from dlkit.mongo.primitives import *
am = AssessmentManager()
bls = am.get_bank_lookup_session()
bas = am.get_bank_admin_session()

BANK_NAME = 'Ortho 3D'
MANIP_SAMPLE = './sample_files/MountingBracket.unity3d'
SMALL_OVS_SAMPLE_1 = './sample_files/MountingBracketVar1.thirdangle.ovs.small.jpg'
LARGE_OVS_SAMPLE_1 = './sample_files/MountingBracketVar1.thirdangle.ovs.jpg'
SMALL_OVS_SAMPLE_2 = './sample_files/MountingBracket.thirdangle.ovs.small.jpg'
LARGE_OVS_SAMPLE_2 = './sample_files/MountingBracket.thirdangle.ovs.jpg'
SMALL_OVS_SAMPLE_3 = './sample_files/MountingBracketVar1b.thirdangle.ovs.small.jpg'
LARGE_OVS_SAMPLE_3 = './sample_files/MountingBracketVar1b.thirdangle.ovs.jpg'
SMALL_OVS_SAMPLE_4 = './sample_files/MountingBracketVar2.thirdangle.ovs.small.jpg'
LARGE_OVS_SAMPLE_4 = './sample_files/MountingBracketVar2.thirdangle.ovs.jpg'
CORRECT_OVS_NUM = 2
DISPLAY_NAME = 'Sample Ortho Multi-Choice II'
DESCRIPTION = 'This is an updated Sample Ortho Multi-Choice'
QUESTION_TEXT = 'Select the correct orthographic view for this object.'

MULTI_CHOICE_ORTHO_QUESTION_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'question-record-type',
    'identifier': 'multi-choice-ortho',
    'display_name': 'Multiple Choice Ortho',
    'display_label': 'Multi Choice Ortho',
    'description': 'Assessment Question record extension for questions that require choosing from ortho projections',
    'domain': 'assessment.Question',
    })

MULTI_CHOICE_ORTHO_ANSWER_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'answer-record-type',
    'identifier': 'multi-choice-ortho',
    'display_name': 'Multiple Choice Ortho',
    'display_label': 'Multi Choice Ortho',
    'description': 'Assessment Answer record extension for answers that require choosing from ortho projections',
    'domain': 'assessment.Answer',
    })


def load_multi_choice_sample(bank_name=BANK_NAME, manip=MANIP_SAMPLE, 
        small_ovs_1=SMALL_OVS_SAMPLE_1, large_ovs_1=LARGE_OVS_SAMPLE_1,
        small_ovs_2=SMALL_OVS_SAMPLE_2, large_ovs_2=LARGE_OVS_SAMPLE_2,
        small_ovs_3=SMALL_OVS_SAMPLE_3, large_ovs_3=LARGE_OVS_SAMPLE_3,
        small_ovs_4=SMALL_OVS_SAMPLE_4, large_ovs_4=LARGE_OVS_SAMPLE_4,
        display_name=DISPLAY_NAME, description=DESCRIPTION, 
        question_text=QUESTION_TEXT, correct_ovs_num=CORRECT_OVS_NUM):

    try:
        bank = get_bank_by_name(bank_name)
    except NotFound:
        print 'creating bank: ' + bank_name
        bfc = bas.get_bank_form_for_create([])
        bfc.set_display_name(bank_name)
        bfc.set_description('Assessment bank for ' + bank_name)
        bank = bas.create_bank(bfc)
    else:
        print 'found bank: ' + bank_name

    ias = am.get_item_admin_session_for_bank(bank.ident)
    ils = am.get_item_lookup_session_for_bank(bank.ident)
    ifc = ias.get_item_form_for_create([])
    ifc.set_display_name(display_name)
    ifc.set_description(description)
    print 'creating item: ' + display_name
    item = ias.create_item(ifc)

    qfc = ias.get_question_form_for_create(item.ident, [MULTI_CHOICE_ORTHO_QUESTION_TYPE])
    qfc.set_text(question_text)
    qfc.set_manip(DataInputStream(open(manip)))
    qfc.set_ortho_choice(ovs_sm = DataInputStream(open(small_ovs_1)), ovs_lg = DataInputStream(open(large_ovs_1)), name='1')
    qfc.set_ortho_choice(ovs_sm = DataInputStream(open(small_ovs_2)), ovs_lg = DataInputStream(open(large_ovs_2)), name='2')
    qfc.set_ortho_choice(ovs_sm = DataInputStream(open(small_ovs_3)), ovs_lg = DataInputStream(open(large_ovs_3)), name='3')
    qfc.set_ortho_choice(ovs_sm = DataInputStream(open(small_ovs_4)), ovs_lg = DataInputStream(open(large_ovs_4)), name='4')
    print 'creating question: ' + display_name
    ias.create_question(qfc)
    
    afc = ias.get_answer_form_for_create(item.ident, [MULTI_CHOICE_ORTHO_ANSWER_TYPE])
    item = ils.get_item(item.ident)
    question = item.get_question()
    for choice in question.get_choices():
        if choice['name'] == str(correct_ovs_num):
            correct_choice_id = choice['id']
    afc.set_choice_id(correct_choice_id)
    print 'setting correct answer id: ' + correct_choice_id
    ias.create_answer(afc)
    return ils.get_item(item.ident)

def get_bank_by_name(bank_name):
    bank = None
    for b in bls.banks:
        if b.display_name.text == bank_name:
            bank = b
    if bank is None:
        raise NotFound('No Bank named \'' + bank_name + '\' was found.')
    return bank


class NotFound(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
