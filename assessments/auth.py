# guidelines:
# https://github.com/etoccalino/django-rest-framework-httpsignature
import pytz

from datetime import datetime
from dateutil import parser

from rest_framework_httpsignature.authentication import SignatureAuthentication
from rest_framework import exceptions

from http_signature import HeaderSigner

from django.contrib.auth.models import AnonymousUser
from django.conf import settings

from assessments_users.models import APIUser
from utilities.authorization import user_has_authz, user_can_proxy

# ============
# These imports are used for the LTIAuthentication
from rest_framework.authentication import OAuthAuthentication
from rest_framework.compat import oauth, oauth_provider, oauth_provider_store


class OpenCrosslinksAuthentication(SignatureAuthentication):
    """
    So Luwen can dev / build Crosslinks - QBank integration locally
    Deprecated -- integrated directly into AssessmentsSignatureAuthentication
    """
    API_KEY_HEADER = 'X-Api-Key'

    def authenticate(self, request):
        """
        https://assessments-dev.mit.edu/touchstone/api/v1/assessment/banks/assessment.Bank:54e7604a33bb72c1ede45b9d@oki-dev.MIT.EDU
        """
        if '/api/v1/assessment/banks/assessment.Bank:54e7604a33bb72c1ede45b9d@oki-dev.MIT.EDU' not in request.path:
            raise exceptions.AuthenticationFailed('No user found.')

        user = APIUser.objects.get(username='luwenh@mit.edu')
        return (user, None)


class AssessmentsSignatureAuthentication(SignatureAuthentication):
    """
    Verifies that signature matches with user's private key
    """
    API_KEY_HEADER = 'X-Api-Key'

    def authenticate(self, request):
        """
        Check for API key header.
        Over-ride this so that we can enforce that the
        signature includes headers of:
        * request-line
        * host
        * date
        * accept
        * x-api-proxy
        """
        proxy_user_header = self.header_canonical('x-api-proxy')
        proxy_username = request.META.get(proxy_user_header)

        # for open banks, create unknown users and register as students
        # banks_to_create_students = settings.OPEN_CATALOGS

        # deprecate this functionality because Crosslinks no longer uses QBank
        # for assessments...
        # if (any(open_bank in request.build_absolute_uri() for open_bank in banks_to_create_students) or
        #         (bankless_bank is not None and
        #          any(bankless_bank.ident.identifier in open_bank for open_bank in banks_to_create_students))):
        #     authorization_header = self.header_canonical('Authorization')
        #     sent_string = request.META.get(authorization_header)
        #     if not sent_string:
        #         # if sent_string, then trying to authenticate and
        #         # we want to follow the normal steps (perhaps is an instructor)
        #         if proxy_username:
        #             # allow for touchstone users to bypass this, if not using proxy_username
        #             try:
        #                 user = APIUser.objects.get(username=proxy_username)
        #             except APIUser.DoesNotExist:
        #                 user = APIUser.objects.create_user(proxy_username)
        #             return (user, None)
        # else:
        #     # pass, follow the normal process for non-open banks
        #     # if proxy_username:
        #     #     try:
        #     #         user = APIUser.objects.get(username=proxy_username)
        #     #     except APIUser.DoesNotExist:
        #     #         raise exceptions.AuthenticationFailed('Authentication credentials were not provided.')
        #     pass

        api_key_header = self.header_canonical(self.API_KEY_HEADER)
        api_key = request.META.get(api_key_header)
        if not api_key:
            return None

        # Check if request has a "Signature" request header.
        authorization_header = self.header_canonical('Authorization')
        sent_string = request.META.get(authorization_header)
        if not sent_string:
            raise exceptions.AuthenticationFailed('No signature provided')
        sent_signature = self.get_signature_from_signature_string(sent_string)

        # Fetch credentials for API key from the data store.
        user, secret = self.fetch_user_data(api_key)
        if not secret:
            raise exceptions.AuthenticationFailed('No user found.')

        # Check that this user has authorizations in DLKit
        if not user_has_authz(user):
            raise exceptions.AuthenticationFailed('You do not have authorizations to use this service.')

        # make sure the user can proxy other users in this catalog
        if not user_can_proxy(user, proxy_username, request):
            raise exceptions.AuthenticationFailed('You cannot proxy users in this catalog.')

        # Build expected string from the headers:
        # 'request-line','host','accept','date','x-api-proxy'
        # first try non-LTI headers
        standard_headers = ['request-line','accept','date','host','x-api-proxy']
        lti_headers = ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']
        good_signature = False

        # reject if date is not within 300 seconds
        if not self.is_valid_date(request):
            raise exceptions.AuthenticationFailed('Invalid date header')

        if 'HTTP_LTI_USER_ID' in request.META:
            computed_string = self.calculate_signature(request,
                                                       api_key,
                                                       secret,
                                                       lti_headers)
        else:
            computed_string = self.calculate_signature(request,
                                                       api_key,
                                                       secret,
                                                       standard_headers)
        computed_signature = self.get_signature_from_signature_string(
            computed_string)

        if computed_signature == sent_signature:
            good_signature = True

        if not good_signature:
            raise exceptions.AuthenticationFailed('Bad signature')

        return (user, None)

    def is_valid_date(self, request):
        if settings.DEBUG:
            return True
        date_header = request.META.get('HTTP_DATE', None)
        if date_header:
            date = parser.parse(date_header)
            now = datetime.utcnow().replace(tzinfo=pytz.utc)
            if (now - date).total_seconds() <= 300:
                return True
        return False

    def calculate_signature(self, request, user_api_key, user_secret, expected_headers):
        """
        Calculate the signature with the headers we're going to require.
        """
        method = request.method
        path = request.get_full_path()
        signer = HeaderSigner(
            key_id=user_api_key, secret=user_secret,
            headers=expected_headers, algorithm='hmac-sha256')
        unsigned = self.build_dict_to_sign(request, expected_headers)
        signed_headers = signer.sign(unsigned, method=method, path=path)
        return signed_headers['authorization']

    def fetch_user_data(self, api_key):
        try:
            user = APIUser.objects.get(
                public_key=api_key)
        except APIUser.DoesNotExist:
            return (AnonymousUser(), None)
        else:
            return (user, str(user.private_key))

class LTIAuthentication(OAuthAuthentication):
    """
    Implements LTI 1.1, based off of OAuth 1.0a.
    http://www.imsglobal.org/lti/blti/bltiv1p0/ltiBLTIimgv1p0.html
    """
    def authenticate(self, request):
        """
        Ignore the oauth_token

        Returns two-tuple of (user, token) if authentication succeeds,
        or None otherwise.
        """
        try:
            oauth_request = oauth_provider.utils.get_oauth_request(request)
        except oauth.Error as err:
            raise exceptions.AuthenticationFailed(err.message)

        if not oauth_request:
            return None

        oauth_params = oauth_provider.consts.OAUTH_PARAMETERS_NAMES

        lti_oauth_params = tuple(x for x in oauth_params if x != 'oauth_token')

        found = any(param for param in lti_oauth_params if param in oauth_request)
        missing = list(param for param in lti_oauth_params if param not in oauth_request)

        if not found:
            # OAuth authentication was not attempted.
            return None

        if missing:
            # OAuth was attempted but missing parameters.
            msg = 'Missing parameters: %s' % (', '.join(missing))
            raise exceptions.AuthenticationFailed(msg)

        if not self.check_nonce(request, oauth_request):
            msg = 'Nonce check failed'
            raise exceptions.AuthenticationFailed(msg)

        try:
            consumer_key = oauth_request.get_parameter('oauth_consumer_key')
            consumer = oauth_provider_store.get_consumer(request, oauth_request, consumer_key)
        except oauth_provider.store.InvalidConsumerError:
            msg = 'Invalid consumer token: %s' % oauth_request.get_parameter('oauth_consumer_key')
            raise exceptions.AuthenticationFailed(msg)

        if consumer.status != oauth_provider.consts.ACCEPTED:
            msg = 'Invalid consumer key status: %s' % consumer.get_status_display()
            raise exceptions.AuthenticationFailed(msg)

        # try:
        #     token_param = oauth_request.get_parameter('oauth_token')
        #     token = oauth_provider_store.get_access_token(request, oauth_request, consumer, token_param)
        # except oauth_provider.store.InvalidTokenError:
        #     msg = 'Invalid access token: %s' % oauth_request.get_parameter('oauth_token')
        #     raise exceptions.AuthenticationFailed(msg)
        #
        # try:
        #     self.validate_token(request, consumer, token)
        # except oauth.Error as err:
        #     raise exceptions.AuthenticationFailed(err.message)

        user = consumer.user

        if not user.is_active:
            msg = 'User inactive or deleted: %s' % user.username
            raise exceptions.AuthenticationFailed(msg)

        return (user, None)


    def validate_token(self, request, consumer, token):
        """
        Check the token and raise an `oauth.Error` exception if invalid.
        """
        oauth_server, oauth_request = oauth_provider.utils.initialize_server_request(request)
        oauth_server.verify_request(oauth_request, consumer, token)
