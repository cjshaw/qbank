# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'LTIUser'
        db.delete_table(u'assessments_ltiuser')

        # Deleting model 'APIUser'
        db.delete_table(u'assessments_apiuser')

        # Removing M2M table for field groups on 'APIUser'
        db.delete_table(db.shorten_name(u'assessments_apiuser_groups'))

        # Removing M2M table for field user_permissions on 'APIUser'
        db.delete_table(db.shorten_name(u'assessments_apiuser_user_permissions'))


    def backwards(self, orm):
        # Adding model 'LTIUser'
        db.create_table(u'assessments_ltiuser', (
            ('user_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('consumer_guid', self.gf('django.db.models.fields.CharField')(max_length=200)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bank', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal(u'assessments', ['LTIUser'])

        # Adding model 'APIUser'
        db.create_table(u'assessments_apiuser', (
            ('username', self.gf('django.db.models.fields.CharField')(max_length=30, unique=True)),
            ('public_key', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('private_key', self.gf('django.db.models.fields.CharField')(max_length=200)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'assessments', ['APIUser'])

        # Adding M2M table for field groups on 'APIUser'
        m2m_table_name = db.shorten_name(u'assessments_apiuser_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('apiuser', models.ForeignKey(orm[u'assessments.apiuser'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['apiuser_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'APIUser'
        m2m_table_name = db.shorten_name(u'assessments_apiuser_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('apiuser', models.ForeignKey(orm[u'assessments.apiuser'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['apiuser_id', 'permission_id'])


    models = {
        
    }

    complete_apps = ['assessments']
