from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _

from assessments_users.models import APIUser

from oauth_provider.models import Consumer
from users.utilities import create_key


# http://stackoverflow.com/questions/19709096/django-adding-remote-users-with-admin-site
class APIUserCreationForm(forms.ModelForm):
    """
    A form that creates a staff-level user, from the given username.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
    }
    username = forms.RegexField(label=_("Username"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    class Meta:
        model = APIUser
        fields = ("username",)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            APIUser._default_manager.get(username=username)
        except APIUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def save(self, commit=True):
        self.is_active = True
        self.is_staff = True
        user = super(APIUserCreationForm, self).save(commit=False)
        if commit:
            user.save()
        return user



class ConsumerCreationForm(forms.ModelForm):
    """
    A form that creates a consumer, but pre-populates the Key and Secret fields
    with randomness.
    """
    name = forms.RegexField(label=_("Name"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})

    class Meta:
        model = Consumer
        fields = ("name","description","status","user")

    def save(self, commit=True):
        self.key = create_key('public')
        self.secret = create_key('private')
        consumer = super(ConsumerCreationForm, self).save(commit=False)
        if commit:
            consumer.save()
        return consumer
