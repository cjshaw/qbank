# now we register this user model
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from assessments_users.models import APIUser
from .forms import APIUserCreationForm

# now we register the consumer model
from oauth_provider.admin import Consumer, ConsumerAdmin
from .forms import ConsumerCreationForm


# To remove the password field for remote user adding
# http://stackoverflow.com/questions/6858028/extending-new-user-form-in-the-admin-django
class APIUserAdmin(UserAdmin):
    add_form = APIUserCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email',)}
        ),
    )

admin.site.register(APIUser, APIUserAdmin)


# To remove the password field for remote user adding
# http://stackoverflow.com/questions/6858028/extending-new-user-form-in-the-admin-django
class OAuthConsumerAdmin(ConsumerAdmin):
    add_form = ConsumerCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'description','status','user',)}
        ),
    )

admin.site.unregister(Consumer)
admin.site.register(Consumer, OAuthConsumerAdmin)
