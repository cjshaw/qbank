# to test the authentication, need to follow the example in the README of
# https://github.com/etoccalino/django-rest-framework-httpsignature

# July 3, 2014, make this depend NOT on IS&T Membership, but rather Ortho3D hardcoded format
#  * Learner -> taaccct_student user
#  * Instructor -> taaccct_instructor user

import re
import pdb
import json
import time
import logging
import requests
import base64

from rest_framework_httpsignature.authentication import SignatureAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.test import APIRequestFactory, APITestCase
from rest_framework import status

from http_signature.requests_auth import HTTPSignatureAuth

from urllib import unquote, quote

from copy import deepcopy

from assessments_users.models import APIUser

from dlkit.services.primitives import Id

# https://docs.djangoproject.com/en/1.6/topics/testing/tools/#overriding-settings
from django.test.utils import override_settings

from Crypto import Random

def calculate_signature(auth, headers, method, path):
    """
    Should return the encoded signature from an HTTPSignatureAuth object
    Need to use the .sign(headers, method, path) of the header_signer object
    in auth
    """
    signed_headers = auth.header_signer.sign(headers, method=method, path=path)
    return signed_headers['authorization']


class APISignatureAuthentication(SignatureAuthentication):
    """
    Adapted from
    https://github.com/etoccalino/django-rest-framework-httpsignature/blob/master/rest_framework_httpsignature/tests.py
    """
    API_KEY_HEADER = 'X-Api-Key'
    def __init__(self, user):
        self.user = user

    def fetch_user_data(self, api_key):
        return (self.user, str(APIUser.objects.get(username="tester").private_key))


# AWS test settings, so we don't pollute the production bucket
@override_settings(S3_BUCKET = 'mitodl-repository-test',
                   CLOUDFRONT_DISTRO = 'd1v4o60a4yrgi8.cloudfront.net',
                   CLOUDFRONT_DISTRO_ID = 'E1OEKZHRUO35M9')
class APITest(APITestCase):
    fixtures = ['test_data.json']

    def already_taken(self, _req):
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Assessment already completed."}'
        )

    def answer_not_found(self, _req):
        """
        Answer was not found -- bad answer_id passed in
        """
        ERRORS = ['{"detail": "Answer not found."}']
        self.assertEqual(_req.status_code, 500)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def bad_input(self, _req):
        """
        Input was a bad format...i.e. a string or non-JSON thing
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Poorly formatted input data."}'
        )

    def bad_multi_choice_response(self, _req):
        """
        choiceIds needs to be a list format, even for one item
        :param _req:
        :return:
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "choiceIds should be a list for multiple-choice questions."}'
        )

    def clean_memberships(self):
        """
        Reset so user has no memberships
        """
        groups = self.mem.user_groups(self._username, full=True)
        sorted_groups = sorted(groups, key=lambda k: k['groupId'])
        for group in sorted_groups:
            self.mem.remove_user_from_group(self._username,
                                            group['groupId'],
                                            group['role'])
        self.wait_5s()

    def code(self, _req, _code):
        self.assertEqual(
            int(_req.status_code),
            int(_code)
        )

    def convert_user_to_bank_instructor(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as an instructor
        """
        # group_id = self.mem.group_id_from_mc3id(item_id)
        # self.mem.remove_user_from_group(self._username,
        #                                 group_id,
        #                                 'Learner')
        # self.mem.add_user_to_group(self._username,
        #                            group_id,
        #                            'Instructor')
        # self.wait_5s()
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_bank_learner(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as a learner
        """
        # group_id = self.mem.group_id_from_mc3id(item_id)
        # self.mem.remove_user_from_group(self._username,
        #                                 group_id,
        #                                 'Instructor')
        # self.mem.add_user_to_group(self._username,
        #                            group_id,
        #                            'Learner')
        # self.wait_5s()
        self._username = self._learner
        self.reset_header_to_new_user()

    def convert_user_to_instructor(self):
        """
        Convert user to instructor in department,
        i.e. DepartmentAdmin
        """
        # self.mem.remove_user_from_group(self._username,
        #                                 self.mem._group_id,
        #                                 'Learner')
        # self.mem.add_user_to_assessments(self._username,
        #                                  'Instructor')
        # self.wait_5s()
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_learner(self):
        """
        Convert user to a basic learner in the department,
        i.e. a DepartmentOfficer
        """
        # self.mem.remove_user_from_group(self._username,
        #                                 self.mem._group_id,
        #                                 'Instructor')
        # self.mem.add_user_to_assessments(self._username,
        #                                  'Learner')
        # self.wait_5s()
        self._username = self._learner
        self.reset_header_to_new_user()

    def create_test_bank(self, _auth, _name, _desc, _form=False):
        """
        helper method to create a test assessment bank
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        sig = calculate_signature(_auth, self.headers, 'POST', test_endpoint)
        payload = {
            "name": _name,
            "description": _desc
        }
        self.sign_client(sig)
        if _form:
            req = self.client.post(test_endpoint, payload, format='multipart')
        else:
            req = self.client.post(test_endpoint, payload, format='json')
        # req.content should have the new bank item, with its ID (which
        # we will need to clean it up)
        self.assertEqual(req.status_code, 200)
        self.verify_text(req, 'Bank', _name, _desc)

        # create a group for this bank and add the user
        # as a learner
        # raw_key = Random.get_random_bytes(4)
        # random_uuid = base64.b64encode(raw_key)
        # group_id, group_name = self.mem.create_group(_name,
        #                                              json.loads(req.content)['id'],
        #                                              re.sub(r'[^\w\d]', '', random_uuid))
        # try:
        #     self.mem.add_user_to_group(self._username,
        #                                group_id,
        #                                'Learner')
        #     self.wait_5s()
        # except:
        #     # already a member
        #     pass

        return unquote(json.loads(req.content)['id'])

    def delete_item(self, _auth, _url):
        """
        helper method that sends a delete request to the url
        calculates the signature for you
        """
        delete_sig = calculate_signature(_auth, self.headers, 'DELETE', _url)
        self.sign_client(delete_sig)
        req = self.client.delete(_url)
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.content, '')

    def encode(self, _file):
        try:
            _file.seek(0)
            return base64.b64encode(_file.read())
        except AttributeError:
            return base64.b64encode(_file)

    def extract_answers(self, item):
        """
        Extract the answer part of an item
        """
        if 'answers' in item:
            return item['answers']
        else:
            return item['data']['results'][0]['answers']

    def extract_question(self, item):
        """
        Extract the question part of an item
        """
        if 'question' in item:
            return item['question']
        else:
            return item['data']['results'][0]['question']

    def filename(self, filepath):
        return filepath.split('/')[-1]

    def include_choices(self, _req):
        """
        Check that an "include choices" error message is included
        :param _req:
        :return:
        """
        errors = ['{"detail": "At least two choice set attribute(s) required for Ortho-3D items."}',
                  '{"detail": "Large and small image files attribute(s) required for Ortho-3D items."}']
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertIn(
            _req.content,
            errors
        )

    def is_cloudfront_url(self, _url):
        self.assertIn(
            self._cloudfront_root,
            _url
        )

        expected_params = ['?Expires=','&Signature=','&Key-Pair-Id=APKAIGRK7FPIAJR675NA']

        for param in expected_params:
            self.assertIn(
                param,
                _url
            )



    def load(self, _req):
        return json.loads(_req.content)

    def message(self, _req, _msg, equal=False):
        data = self.load(_req)
        if equal:
            self.assertEqual(
                str(_msg),
                str(data['detail'])
            )
        else:
            self.assertNotEqual(
                str(_msg),
                str(data['detail'])
            )

    def missing_orthoview(self, _req):
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertEqual(
            _req.content,
            '{"detail": "All three view set attribute(s) required for Ortho-3D items."}'
        )

    def need_items(self, _req):
        """
        Need items in assessment before creating an offering
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Cannot create an assessment offering for an assessment with no items."}'
        )

    def not_allowed(self, _req, _type):
        """
        verify that the _type method is not allowed. Status code 405
        and the text matches not allowed text
        """
        self.assertEqual(_req.status_code, 405)
        self.assertEqual(
            _req.content,
            '{"detail": "Method \'' + _type + '\' not allowed."}'
        )

    def not_empty(self, _req):
        """
        Verify that the bank is not empty and cannot be deleted
        """
        ERRORS = ['{"detail": "This Item is being used in one or more Assessments. Delink it first, before deleting it."}',
                  '{"detail": "Bank is not empty. Please delete its contents first."}',
                  '{"detail": "Assessment still has AssessmentOffered. Delete the offerings first."}']
        self.assertEqual(_req.status_code, 406)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def not_responded(self, _req):
        self.ok(_req)
        self.assertEqual(
            _req.content,
            '{"responded": false}'
        )

    def ok(self, _req):
        """
        checks for status code 200 and that the request was okay
        """
        self.assertEqual(_req.status_code, 200)

    def reset_files(self):
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        self.choice1sm.seek(0)
        self.choice1big.seek(0)

    def reset_header_to_new_user(self):
        """
        When self._username changes, need to update the X-Api-Proxy in the header
        """
        self.headers['X-Api-Proxy'] = self._username

    def responded(self, _req, _correct=False):
        expected_response = {
            "responded" : True,
            "correct"   : _correct
        }
        self.ok(_req)
        self.assertEqual(
            json.loads(_req.content),
            expected_response
        )

    def set_up_edx_item(self, _auth):
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(_auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'

        metadata = {
            'attempts'      : 4,
            'markdown'      : 'fake markdown',
            'rerandomize'   : 'fake rerandomize',
            'showanswer'    : 'fake showanswer',
            'weight'        : 1.23
        }

        irt = {
            'difficulty'        : -1.34,
            'discrimination'    : 2.41
        }

        learning_objectives = ['mc3-objective:21273@MIT-OEIT']  # also test that this gets quote-safed

        post_sig = calculate_signature(_auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "attempts"              : metadata['attempts'],
            "markdown"              : metadata['markdown'],
            "rerandomize"           : metadata['rerandomize'],
            "showanswer"            : metadata['showanswer'],
            "weight"                : metadata['weight'],
            "difficulty"            : irt['difficulty'],
            "discrimination"        : irt['discrimination'],
            "name"                  : item_name,
            "description"           : item_desc,
            "question"              : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"               : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
            "learningObjectiveIds"  : learning_objectives
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            self.assertEqual(
                item[key],
                value
            )

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

        for index, _id in enumerate(learning_objectives):
            self.assertEqual(
                str(item['learningObjectiveIds'][index]),
                str(quote(_id))
            )

        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        return {
            'bank_endpoint'         : bank_endpoint,
            'irt'                   : irt,
            'item_endpoint'         : item_details_endpoint,
            'item_id'               : item_id,
            'item_name'             : item_name,
            'learning_objectives'   : learning_objectives,
            'metadata'              : metadata
        }


    def setUp(self):
        from django.contrib.auth.models import AnonymousUser
        from datetime import datetime
        from time import mktime
        from wsgiref.handlers import format_date_time
        from users.utilities import Membership

        self.anon = AnonymousUser()
        self._app_user = 'tester'
        self._instructor = 'taaccct_instructor'
        self._learner = 'taaccct_student'
        self._username = self._instructor
        # self._username = 'cjshaw@mit.edu'
        self.user = APIUser.objects.get(username=self._app_user)
        self.public_key = str(self.user.public_key)
        self.private_key = str(self.user.private_key)
        self.signature_headers = ['request-line','accept','date','host','x-api-proxy']
        self.headers = {
            'Date'          : format_date_time(mktime(datetime.now().timetuple())),
            'Host'          : 'testserver:80',
            'Accept'        : 'application/json',
            'X-Api-Key'     : self.public_key,
            'X-Api-Proxy'   : self._username
        }
        self.auth = APISignatureAuthentication(self.user)
        self.endpoint = '/api/v1/'
        self.factory = APIRequestFactory()

        self._manip_file = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/myAssetBundle.unity3d'
        self._manip_filename = self.filename(self._manip_file)
        self.manip = open(self._manip_file, 'rb')

        self._front = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/front.jpg'
        self._front_filename = self.filename(self._front)
        self.front = open(self._front, 'rb')

        self._side = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/side.jpg'
        self._side_filename = self.filename(self._side)
        self.side = open(self._side, 'rb')

        self._top = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/top.jpg'
        self._top_filename = self.filename(self._top)
        self.top = open(self._top, 'rb')

        self._choice0sm = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice0sm.jpg'
        self._choice0sm_filename = self.filename(self._choice0sm)
        self.choice0sm = open(self._choice0sm, 'rb')

        self._choice0big = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice0big.jpg'
        self._choice0big_filename = self.filename(self._choice0big)
        self.choice0big = open(self._choice0big, 'rb')

        self._choice1sm = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice1sm.jpg'
        self._choice1sm_filename = self.filename(self._choice1sm)
        self.choice1sm = open(self._choice1sm, 'rb')

        self._choice1big = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice1big.jpg'
        self._choice1big_filename = self.filename(self._choice1big)
        self.choice1big = open(self._choice1big, 'rb')

        self._aws_root = 'https://mitodl-repository-test.s3.amazonaws.com/'
        self._cloudfront_root = 'https://d1v4o60a4yrgi8.cloudfront.net/'

        # Create the Membership() handle and add test user to the Assessments
        # project as an admin. Catches and ignores any exceptions thrown
        # self.mem = Membership()
        # self.clean_memberships()
        # try:
        #     self.mem.add_user_to_assessments(self._username, 'Instructor')
        # except:
        #     # print "Test user already in Assessments as instructor"
        #     pass
        # finally:
        #     self.wait_for_membership_to_appear()
        #     self.wait_5s()

    def sign_client(self, sig, opt_headers=None):
        self.client.credentials()
        if opt_headers:
            self.client.credentials(
                HTTP_ACCEPT=opt_headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=opt_headers['Date'],
                HTTP_HOST=opt_headers['Host'],
                HTTP_X_API_KEY=opt_headers['X-Api-Key'],
                HTTP_X_API_PROXY=opt_headers['X-Api-Proxy'],
                HTTP_LTI_BANK=opt_headers['LTI-Bank'],
                HTTP_LTI_USER_ID=opt_headers['LTI-User-ID'],
                HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID=opt_headers['LTI-Tool-Consumer-Instance-GUID'],
                HTTP_LTI_USER_ROLE=opt_headers['LTI-User-Role']
            )
        else:
            self.client.credentials(
                HTTP_ACCEPT=self.headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=self.headers['Date'],
                HTTP_HOST=self.headers['Host'],
                HTTP_X_API_KEY=self.headers['X-Api-Key'],
                HTTP_X_API_PROXY=self.headers['X-Api-Proxy']
            )

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        import boto
        from django.conf import settings
        # self.clean_memberships()

        self.manip.close()
        self.front.close()
        self.side.close()
        self.top.close()
        self.choice0sm.close()
        self.choice0big.close()
        self.choice1sm.close()
        self.choice1big.close()

        # clean up S3 bucket
        put_public_key = settings.S3_TEST_PUBLIC_KEY
        put_private_key = settings.S3_TEST_PRIVATE_KEY
        s3_bucket = settings.S3_TEST_BUCKET

        connection = boto.connect_s3(put_public_key, put_private_key)

        bucket = connection.create_bucket(s3_bucket)
        for key in bucket.list():
            if 'logs' not in key.name:
                key.delete()

        # cannot delete department-level groups, so cycle through all
        # children groups and delete those, to clean up
        # all_groups = self.mem.all_groups()
        # for group in all_groups:
        #     if group['groupId'] != self.mem._group_id:
        #         self.mem.delete_group(group['groupId'])

    def unauthorized(self, _req):
        """
        confirms that status code for unauthorized access is 403, and text is the
        error message
        """
        ERRORS = ['{"detail": "Authentication credentials were not provided."}',
                  '{"detail": "Bad signature"}',
                  '{"detail": "Permission denied. You do not have rights to use this service."}',
                  '{"detail": "Permission denied. You do not have rights to view assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment banks."}',
                  '{"detail": "Permission denied. You do not have rights to create new assessment banks."}',
                  '{"detail": "Permission denied. You do not have rights to view this bank\'s details."}',
                  '{"detail": "Permission denied. You do not have rights to edit this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create new assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view item details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to assign items to assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete an assessment\'s items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment takens in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create assessment takens in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item files in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item questions in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item questions in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view items in this bank."}']
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def user_not_found(self, _req):
        """
        User is not found--bad public key
        """
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            '{"detail": "No user found."}'
        )

    def verify_answers(self, _data, _a_strs, _a_types):
        """
        verify just the answers part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
                if 'data' in data:
                    data = data['data']['results']
            except:
                data = json.loads(_data)
        except:
            data = _data

        if 'answers' in data:
            answers = data['answers']
        elif 'results' in data:
            answers = data['results']
        else:
            answers = data

        if isinstance(answers, list):
            ind = 0
            for _a in _a_strs:
                if _a_types[ind] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                    self.assertEqual(
                        answers[ind]['text']['text'],
                        _a
                    )
                self.assertIn(
                    _a_types[ind],
                    answers[ind]['recordTypeIds']
                )
                ind += 1
        elif isinstance(answers, dict):
            if _a_types[0] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                self.assertEqual(
                    answers['text']['text'],
                    _a_strs[0]
                )
            self.assertIn(
                _a_types[0],
                answers['recordTypeIds']
            )
        else:
            self.fail('Bad answer format.')

    def verify_data_length(self, _req, _length):
        """
        Check that the data array is of the specified length
        """
        data = json.loads(_req.content)
        self.assertEqual(
            len(data['data']),
            _length
        )

    def verify_first_angle(self, _req, _angle):
        """
        Check if the req angle matches expected
        :param _req:
        :param _angle:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        self.assertEqual(
            item['question']['firstAngle'],
            _angle
        )

    def verify_links(self, _req, _type):
        """
        helper method to check that basic _links are included in
        the returned object. Includes a check for self.
        Depending on the _type, it will also check for the drilldown
        links...like for bank, should have assessments, items
        """
        search_type = _type.lower()
        self.assertIn(
            '"_links": ',
            _req.content
        )
        self.assertIn(
            '"self": ',
            _req.content
        )
        if search_type == 'bank':
            self.assertIn(
                '"assessments": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'assessmentdetails':
            self.assertIn(
                '"offerings": ',
                _req.content
            )
            self.assertIn(
                '"takens": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'service':
            self.assertIn(
                '"banks": ',
                _req.content
            )
            self.assertIn(
                '"itemTypes": ',
                _req.content
            )
        elif search_type == 'itemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'assessmentitemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'itemfiles':
            self.assertIn(
                '"front": ',
                _req.content
            )
            self.assertIn(
                '"top": ',
                _req.content
            )
            self.assertIn(
                '"manip": ',
                _req.content
            )
            self.assertIn(
                '"side": ',
                _req.content
            )
        elif search_type == 'takenquestions':
            self.assertIn(
                '"data": ',
                _req.content
            )
        elif search_type == 'takenquestion':
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"submit": ',
                _req.content
            )

    def verify_manip_file(self, _req, _manip_id):
        """
        make sure the manipulatable file is in the object correctly
        Probably some set of Ids:
            "manipId": "repository.Asset%3A53aac9afea061a05081dec55%40birdland.mit.edu",
            "orthoViewSet": {}
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            self.assertEqual(
                item['data'][0]['question']['manipId'],
                _manip_id
            )
        elif 'question' in item:
            self.assertEqual(
                item['question']['manipId'],
                _manip_id
            )
        else:
            self.assertEqual(
                item['manipId'],
                _manip_id
            )

    def verify_no_data(self, _req):
        """
        Verify that the data object in _req is empty
        """
        data = json.loads(_req.content)
        self.assertEqual(
            data['data'],
            {'count'    : 0,
             'previous' : None,
             'results'  : [],
             'next'     : None}
        )
        self.verify_data_length(_req, 4)

    def verify_offerings(self, _req, _type, _data):
        """
        Check that the offerings match...
        _type may not be used here; assume AssessmentOffered always?
        _data objects need to have an offering ID in them
        """
        def check_expected_against_one_item(expected, item):
            for key, value in expected.iteritems():
                if isinstance(value, basestring):
                    self.assertEqual(
                        item[key],
                        value
                    )
                elif isinstance(value, dict):
                    for inner_key, inner_value in value.iteritems():
                        self.assertEqual(
                            item[key][inner_key],
                            inner_value
                        )

        data = json.loads(_req.content)
        if 'data' in data:
            data = data['data']['results']
        elif 'results' in data:
            data = data['results']

        if isinstance(_data, list):
            for expected in _data:
                if isinstance(data, list):
                    for item in data:
                        if item['id'] == expected['id']:
                            check_expected_against_one_item(expected, item)
                            break
                else:
                    check_expected_against_one_item(expected, data)
        elif isinstance(_data, dict):
            if isinstance(data, list):
                for item in data:
                    if item['id'] == _data['id']:
                        check_expected_against_one_item(_data, item)
                        break
            else:
                check_expected_against_one_item(_data, data)

    def verify_ortho_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data']['results'][0]
        if 'answers' in item:
            item = item['answers'][0]
        self.assertEqual(
            item['integerValues']['frontFaceValue'],
            _data['frontFaceValue']
        )
        self.assertEqual(
            item['integerValues']['sideFaceValue'],
            _data['sideFaceValue']
        )
        self.assertEqual(
            item['integerValues']['topFaceValue'],
            _data['topFaceValue']
        )

    def verify_ortho_choices(self, _req, _expected):
        """
        make sure the files and IDs match. Assume two choices for now.
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for index, choice in enumerate(item['choices']):
            self.assertEqual(
                choice['id'],
                _expected[index]['id']
            )
            if index == 0:
                expected_file_sm = self.choice0sm
                expected_file_big = self.choice0big
            else:
                expected_file_sm = self.choice1sm
                expected_file_big = self.choice1big

            self.is_cloudfront_url(choice['smallOrthoViewSet'])
            self.is_cloudfront_url(choice['largeOrthoViewSet'])

            expected_file_sm.seek(0)
            expected_file_big.seek(0)

            self.assertEqual(
                requests.get(choice['smallOrthoViewSet']).content,
                expected_file_sm.read()
            )

            self.assertEqual(
                requests.get(choice['largeOrthoViewSet']).content,
                expected_file_big.read()
            )

    def verify_ortho_files(self, _req, _expected):
        """
        make sure the files match
        Because these are signed URLs for AWS Cloudfront, the signature & expire times
        will not match -- verify this.
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for file_label, file_data in item['files'].iteritems():
            self.is_cloudfront_url(file_data)
            if 'http' in _expected[file_label]:
                self.is_cloudfront_url(_expected[file_label])

                # expire times should not match
                self.assertNotEqual(
                    file_data,
                    _expected[file_label]
                )

                self.assertEqual(
                    file_data.split('?Expires')[0],
                    _expected[file_label].split('?Expires')[0]
                )
            else:
                # expected is the actual b64 encoded file
                raw_file = requests.get(file_data).content
                self.assertEqual(
                    base64.b64encode(raw_file),
                    _expected[file_label]
                )


    def verify_ortho_multi_choice_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data'][0]
        if 'answers' in item:
            item = item['answers'][0]

        if not isinstance(_data, list):
            raise Exception('_data should be a list')

        for ans in item['choiceIds']:
            self.assertIn(
                ans,
                _data
            )

    def verify_question(self, _data, _q_str, _q_type):
        """
        verify just the question part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
            except:
                data = json.loads(_data)
        except:
            data = _data
        if 'question' in data:
            question = data['question']
        elif 'data' in data:
            try:
                question = data['data']['results'][0]['question']
            except:
                question = data['data']['results'][0]
        else:
            question = data

        self.assertEqual(
            question['text']['text'],
            _q_str
        )
        self.assertIn(
            _q_type,
            question['recordTypeIds']
        )

    def verify_questions_answers(self, _req, _q_str, _q_type, _a_str, _a_type, _id=None):
        """
        helper method to verify the questions & answers in a returned item
        takes care of all the language stuff
        Assumes multiple answers, to _a_str and _a_type must be lists
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                    item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req

        self.verify_question(data, _q_str, _q_type)

        self.verify_answers(data, _a_str, _a_type)

    def verify_question_status(self, question, responded, correct=None):
        self.assertEqual(
            question['responded'],
            responded
        )
        if correct:
            self.assertEqual(
                question['correct'],
                correct  # True
            )
        else:
            try:
                self.assertNotIn(
                    'correct',
                    question
                )
            except:
                self.assertEqual(
                    question['correct'],
                    correct  # False
                )

    def verify_submission(self, _req, _expected_result, _has_next=None):
        data = json.loads(_req.content)
        self.assertEqual(
            data['correct'],
            _expected_result
        )
        if _has_next:
            self.assertEqual(
                data['hasNext'],
                _has_next
            )

    def verify_taking_files(self, _req):
        """
        Check that the taken files match the uploaded ones
        :param _req:
        :return:
        """
        data = json.loads(_req.content)
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)


        for file_label, file_url in data.iteritems():
            self.is_cloudfront_url(file_url)

        self.assertEqual(
            requests.get(data['manip']).content,
            self.manip.read()
        )
        self.assertEqual(
            requests.get(data['front']).content,
            self.front.read()
        )
        self.assertEqual(
            requests.get(data['side']).content,
            self.side.read()
        )
        self.assertEqual(
            requests.get(data['top']).content,
            self.top.read()
        )

    def verify_text(self, _req, _type, _name, _desc, _id=None, _genus=None):
        """
        helper method to verify the text in a returned object
        takes care of all the language stuff
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                    item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req
        self.assertEqual(
            data['displayName']['text'],
            _name
        )
        self.assertEqual(
            data['description']['text'],
            _desc
        )
        self.assertEqual(
            data['type'],
            _type
        )

        if _genus:
            self.assertEqual(
                data['genusTypeId'],
                _genus
            )

    def verify_types(self, _req):
        """
        Make sure all the types in mongo/assessment/records are returned
        """
        from dlkit.mongo.assessment.records.types import QUESTION_RECORD_TYPES
        types_list = json.loads(_req.content)
        # self.assertEqual(len(types_list), len(ITEM_RECORD_TYPES))
        # right now ITEM_RECORD_TYPES is a dict with only one type...
        # TODO: fix this...once dlkit is fixed
        # canonical = QUESTION_RECORD_TYPES
        canonical = [
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Match Ortho Faces"
                },
                "description": {
                    "text": "Match the manipulatable object to the given faces."
                },
                "id": "question%3Amatch-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Define Ortho Faces"
                },
                "description": {
                    "text": "Define the best plane, top, and side faces of the manipulatable object."
                },
                "id": "question%3Adefine-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Ortho View Set"
                },
                "description": {
                    "text": "From the choices provided, select the ortho view set that matches the given manipulatable."
                },
                "id": "question%3Achoose-viewset%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Manipulatable"
                },
                "description": {
                    "text": "From the choices provided, select the manipulatable that matches the given ortho view set."
                },
                "id": "question%3Achoose-manip%40ODL.MIT.EDU"
            },
            {
                'displayName'       : {
                    'text'  : 'edX Multiple Choice'
                },
                'description'       : {
                    'text'  : 'Multiple Choice question for edX.'
                },
                'id'                : 'question%3Amulti-choice-edx%40ODL.MIT.EDU',
                'recordType'        : 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
            }
        ]
        canonical_display_names = []
        canonical_descriptions = []
        canonical_ids = []
        canonical_record_types = []
        for item in canonical:
            canonical_display_names.append(item['displayName']['text'])
            canonical_descriptions.append(item['description']['text'])
            canonical_ids.append(item['id'])
            canonical_record_types.append(item['recordType'])

        ind = 0
        for item in types_list:
            self.assertEqual(
                item['displayName']['text'],
                canonical_display_names[ind]
            )
            self.assertEqual(
                item['description']['text'],
                canonical_descriptions[ind]
            )
            self.assertEqual(
                item['id'],
                canonical_ids[ind]
            )
            self.assertEqual(
                item['recordType'],
                canonical_record_types[ind]
            )
            ind += 1

    # def verify_view_set(self, _req, _data):
    #     """
    #     verify that file Ids are correct for the different viewsets
    #     """
    #     try:
    #         try:
    #             item = json.loads(_req.content)
    #         except:
    #             item = json.loads(_req)
    #     except:
    #         item = _req
    #     if 'data' in item:
    #         item = item['data'][0]
    #     if 'question' in item:
    #         item = item['question']
    #     for key, value in _data.iteritems():
    #         self.assertEqual(
    #             item['orthoViewSet'][key],
    #             value
    #         )

    def wait_5s(self, default=5):
        """
        Trying to wait 5 seconds to let Membership catch up with group changes...
        """
        time.sleep(default)

    def wait_for_membership_to_appear(self):
        """
        Because it seems like the Membership service has a delay
        in creating new Departments -> membership showing up?
        Need to verify that user has membership.
        Seems to affect the instructor -> POST new bank
        """
        found_one = False
        while not found_one:
            self.wait_5s(15)
            if len(self.mem.user_groups(self._username, full=True)) > 0:
                found_one = True

    def wrong_taken_post_url(self, _req):
        """
        Can only create a taken from the /offering/ endpoint
        Trying from the /assessments/ endpoint should throw this error
        """
        msg = 'Can only create AssessmentTaken from an AssessmentOffered root URL.'
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "' + msg + '"}'
        )

    def test_unauthenticated_user_request_returns_unauthorized(self):
        """
        An unauthenticated request should respond with error
        """
        req = self.client.get(self.endpoint + 'assessment/')
        self.unauthorized(req)

    def test_unauthenticated_api_request_returns_unauthorized(self):
        """
        User should get a bad signature JSON if their signature doesn't match
        """
        fake_private = 'afakeprivateKey!23'
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=fake_private,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.unauthorized(req)

    def test_incomplete_request_signature(self):
        """
        Make sure that all of the following headers are required:
        ['request-line','accept','date','host','x-api-proxy']
        """
        partial_headers = ['request-line','accept','date','host']
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=partial_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.unauthorized(req)

    def test_authenticated_instructor_assessment_delete(self):
        """
        DELETE should do nothing here. Error code 405.
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'DELETE', test_endpoint)
        self.sign_client(sig)
        req = self.client.delete(test_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_authenticated_learner_assessment_delete(self):
        """
        User should get same error as above--method not allowed. Error code 405
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'DELETE', test_endpoint)
        self.sign_client(sig)
        req = self.client.delete(test_endpoint)
        self.not_allowed(req, 'DELETE')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_links(req, 'Service')

    def test_authenticated_learner_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        For someone who is just a department officer, this should also return
        the results (officers can lookup)
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_links(req, 'Service')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_post(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'POST', test_endpoint)
        self.sign_client(sig)
        req = self.client.post(test_endpoint)
        self.not_allowed(req, 'POST')

    def test_authenticated_learner_assessment_post(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'POST', test_endpoint)
        self.sign_client(sig)
        req = self.client.post(test_endpoint)
        self.not_allowed(req, 'POST')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_put(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'PUT', test_endpoint)
        self.sign_client(sig)
        req = self.client.put(test_endpoint)
        self.not_allowed(req, 'PUT')

    def test_authenticated_learner_assessment_put(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'PUT', test_endpoint)
        self.sign_client(sig)
        req = self.client.put(test_endpoint)
        self.not_allowed(req, 'PUT')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_bank_delete(self):
        """
        DELETE should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'DELETE', test_endpoint)
        self.sign_client(sig)
        req = self.client.delete(test_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_authenticated_learner_bank_delete(self):
        """
        DELETE should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'DELETE', test_endpoint)
        self.sign_client(sig)
        req = self.client.delete(test_endpoint)
        self.not_allowed(req, 'DELETE')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank_id = self.create_test_bank(auth, 'atestbank', 'for testing')
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.
        delete_endpoint = test_endpoint + bank_id + '/'
        self.delete_item(auth, delete_endpoint)

    def test_authenticated_learner_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank_id = self.create_test_bank(auth, 'atestbank', 'for testing')
        self.convert_user_to_learner()
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.

        self.convert_user_to_instructor()
        delete_endpoint = test_endpoint + bank_id + '/'
        self.delete_item(auth, delete_endpoint)

    def test_authenticated_instructor_bank_post_and_crud(self):
        """
        User can create a new assessment bank. Need to do self-cleanup,
        because the Mongo backend is not part of the test database...that
        means Django will not wipe it clean after every test!
        Once a bank is created, user can GET it, PUT to update it, and
        DELETE it. POST should return an error code 405.
        Do these bank detail tests here, because we have a known
        bank ID
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        name = 'atestbank'
        desc = 'for testing purposes only'
        item_id = self.create_test_bank(auth, name, desc)

        # verify that the bank now appears in the bank_details call
        bank_endpoint = self.endpoint + 'assessment/banks/' + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', bank_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # Department Officer cannot update banks
        self.convert_user_to_learner()
        new_name = 'a new bank name'
        payload = {
            "name": new_name
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.unauthorized(req)

        # DepartmentAdmin should be able to edit the bank
        self.convert_user_to_instructor()
        put_sig = calculate_signature(auth, self.headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', bank_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, desc)
        self.verify_links(req, 'Bank')

        # change user to a "bank" learner, and
        # verify that you cannot update the bank's description
        self.convert_user_to_learner()
        new_desc = 'a new bank description'
        payload = {
            "description": new_desc
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.unauthorized(req)

        # check that a bank instructor can
        self.convert_user_to_instructor()
        put_sig = calculate_signature(auth, self.headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, new_desc)

        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, new_desc)
        self.verify_links(req, 'Bank')

        # verify that POST returns error message / code 405
        post_sig = calculate_signature(auth, self.headers, 'POST', bank_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(bank_endpoint)
        self.not_allowed(req, 'POST')

        # now grab the id and delete it, to clean up the database
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_bank_post_and_crud(self):
        """
        Learners can do nothing with POST and CRuD
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_learner()

        # verify that the bank now appears in the bank_details call
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', bank_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that you cannot update the bank's name
        new_name = 'a new bank name'
        payload = {
            "name": new_name
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.unauthorized(req)

        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that you cannot update the bank's description
        new_desc = 'a new bank description'
        payload = {
            "description": new_desc
        }
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, payload, format='json')
        self.unauthorized(req)

        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that POST returns error message / code 405
        post_sig = calculate_signature(auth, self.headers, 'POST', bank_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(bank_endpoint)
        self.not_allowed(req, 'POST')

        self.convert_user_to_instructor()
        # now grab the id and delete it, to clean up the database
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_bank_put(self):
        """
        PUT should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'PUT', test_endpoint)
        self.sign_client(sig)
        req = self.client.put(test_endpoint)
        self.not_allowed(req, 'PUT')

    def test_authenticated_learner_bank_put(self):
        """
        PUT should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'PUT', test_endpoint)
        self.sign_client(sig)
        req = self.client.put(test_endpoint)
        self.not_allowed(req, 'PUT')

    def test_authenticated_instructor_assessments_crud(self):
        """
        Create a test bank and test all things associated with assessments
        and a single assessment
        DELETE on root assessments/ does nothing. Error code 405.
        GET on root assessments/ gets you a list
        POST on root assessments/ creates a new assessment
        PUT on root assessments/ does nothing. Error code 405.

        For a single assessment detail:
        DELETE will delete that assessment
        GET brings up the assessment details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'

        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessment_endpoint = bank_endpoint + 'assessments/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_endpoint)
        self.not_allowed(req, 'PUT')

        # GET should be unauthorized for a learner
        self.convert_user_to_bank_learner(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_endpoint)
        self.unauthorized(req)

        # Use POST to create an assessment--Learner cannot create
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessment_endpoint, payload, format='json')
        self.unauthorized(req)

        # Use POST to create an assessment--Instructor can
        self.convert_user_to_bank_instructor(bank_id)
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Now test PUT / GET / POST / DELETE on the new assessment item
        # POST does nothing
        assessment_detail_endpoint = assessment_endpoint + assessment_id + '/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_detail_endpoint)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link. Allowed for Instructor
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        # PUT not allowed with Learner
        self.convert_user_to_bank_learner(bank_id)
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_detail_endpoint)
        self.sign_client(put_sig)
        new_assessment_name = 'a new assessment name'
        new_assessment_desc = 'to trick students'
        payload = {
            "name": new_assessment_name
        }
        req = self.client.put(assessment_detail_endpoint, payload, format='json')
        self.unauthorized(req)

        # PUT modifies the assessment, with the changes reflected in GET.
        # Allowed for Instructor
        self.convert_user_to_bank_instructor(bank_id)
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_detail_endpoint)
        self.sign_client(put_sig)

        req = self.client.put(assessment_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         assessment_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_detail_endpoint)
        self.sign_client(get_sig)

        self.sign_client(get_sig)
        req = self.client.get(assessment_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        self.sign_client(put_sig)
        payload = {
            "description": new_assessment_desc
        }
        req = self.client.put(assessment_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)

        self.sign_client(get_sig)
        req = self.client.get(assessment_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        # trying to delete the bank as Learner / Department Officer
        # should throw unauthorized exception
        self.convert_user_to_learner()
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.unauthorized(req)
        self.convert_user_to_instructor()
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)

        # trying to delete the bank with assessments should throw an error
        req = self.client.delete(bank_endpoint)
        self.not_empty(req)

        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessments_crud(self):
        """
        Create a test bank and test all things associated with assessments
        and a single assessment
        DELETE on root assessments/ does nothing. Error code 405.
        GET on root assessments/ gets you a list
        POST on root assessments/ creates a new assessment
        PUT on root assessments/ does nothing. Error code 405.

        For a single assessment detail:
        DELETE will delete that assessment
        GET brings up the assessment details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description

        ** User should be a DepartmentAdmin but a Learner for the bank
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessment_endpoint = bank_endpoint + 'assessments/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_endpoint)
        self.not_allowed(req, 'PUT')

        # GET should be unauthorized for learner
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_endpoint)
        self.unauthorized(req)

        # Use POST to create an assessment--it should throw a 403 for a learner
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessment_endpoint, payload, format='json')
        self.unauthorized(req)

        # trying to delete the bank should be okay, because user is also
        # DepartmentAdmin
        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_items_crud(self):
        """
        Create a test bank and test all things associated with items
        and a single item
        DELETE on root items/ does nothing. Error code 405.
        GET on root items/ gets you a list
        POST on root items/ creates a new item
        PUT on root items/ does nothing. Error code 405.

        For a single item detail:
        DELETE will delete that item
        GET brings up the item details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(items_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(items_endpoint)
        self.not_allowed(req, 'PUT')

        # GET for a Learner is unauthorized
        self.convert_user_to_bank_learner(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(items_endpoint, payload, format='json')
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, payload, format='json')
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        post_sig = calculate_signature(auth, self.headers, 'POST', item_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_detail_endpoint)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')

        # GET should still not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.unauthorized(req)

        # PUT should not work for a Learner, throw unauthorized
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        new_item_name = 'a new item name'
        new_item_desc = 'to trick students'
        payload = {
            "name": new_item_name
        }
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.unauthorized(req)

        # PUT should work now. Modifies the item, with the changes reflected in GET
        self.convert_user_to_bank_instructor(bank_id)
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         item_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')

        self.sign_client(put_sig)
        payload = {
            "description": new_item_desc
        }
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)

        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)
        self.verify_links(req, 'ItemDetails')

        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_items_crud(self):
        """
        Create a test bank and test all things associated with items
        and a single item
        DELETE on root items/ does nothing. Error code 405.
        GET on root items/ gets you a list
        POST on root items/ creates a new item
        PUT on root items/ does nothing. Error code 405.

        For a single item detail:
        DELETE will delete that item
        GET brings up the item details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(items_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(items_endpoint)
        self.not_allowed(req, 'PUT')

        # GET should return all items for this bank--currently none
        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.unauthorized(req)

        # Use POST to create an item--it should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(items_endpoint, payload, format='json')
        self.unauthorized(req)

        # trying to delete the bank should be okay, because user is
        # also DepartmentAdmin
        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_question_string_item_crud(self):
        """
        Test ability for user to POST a new question string and
        response string item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint, payload, format='json')
        self.unauthorized(req)

        # Learners cannot GET?
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        post_sig = calculate_signature(auth, self.headers, 'POST', item_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_detail_endpoint)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        # GET of an item should not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.unauthorized(req)

        # PUT should work for Instructor.
        # Can modify the question and answers, reflected in GET
        new_question_string = 'what day is it?'
        new_answer_string = 'Saturday'

        payload = {
            'question': {
                'id' : question_id,
                'questionString': new_question_string,
                'type': question_type
            }
        }
        self.convert_user_to_bank_instructor(bank_id)

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        self.sign_client(put_sig)
        payload = {
            'answers': [{
                'id' : answer_id,
                'responseString': new_answer_string,
                'type': answer_type
            }]
        }
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])

        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        # Verify that GET, PUT to question/ endpoint works
        item_question_endpoint = item_detail_endpoint + 'question/'
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', item_question_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_question_endpoint)
        self.not_allowed(req, 'DELETE')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', item_question_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_question_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req, new_question_string, question_type)

        newer_question_string = 'yet another new question?'
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        payload = {
            "id"             : question_id,
            "questionString" : newer_question_string,
            "type"           : question_type
        }
        req = self.client.put(item_question_endpoint, payload, format='json')

        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req, newer_question_string, question_type)

        # Verify that GET, POST (answers/) and
        # GET, DELETE, PUT to answers/<id> endpoint work
        # Verify that invalid answer_id returns "Answer not found."
        item_answers_endpoint = item_detail_endpoint + 'answers/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', item_answers_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_answers_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_answers_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_answers_endpoint)
        self.not_allowed(req, 'PUT')

        get_sig = calculate_signature(auth, self.headers, 'GET', item_answers_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req, [new_answer_string], [answer_type])

        second_answer_string = "a second answer"
        payload = [{
            "responseString"    : second_answer_string,
            "type"              : answer_type
        }]
        post_sig = calculate_signature(auth, self.headers, 'POST', item_answers_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answers_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req,
                            [new_answer_string, second_answer_string],
                            [answer_type, answer_type])

        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [new_answer_string, second_answer_string],
                            [answer_type, answer_type])

        fake_item_answer_detail_endpont = item_answers_endpoint + 'fakeid/'
        get_sig = calculate_signature(auth, self.headers, 'GET', fake_item_answer_detail_endpont)
        self.sign_client(get_sig)
        req = self.client.get(fake_item_answer_detail_endpont)
        self.answer_not_found(req)

        item_answer_detail_endpoint = item_answers_endpoint + unquote(answer_id) + '/'
        # Check that POST returns error code 405--we don't support this
        post_sig = calculate_signature(auth, self.headers, 'POST', item_answer_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answer_detail_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', item_answer_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req, [new_answer_string], [answer_type])

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_answer_detail_endpoint)
        self.sign_client(put_sig)
        newer_answer_string = 'yes, another one'
        payload = {
            "responseString"    : newer_answer_string,
            "type"              : answer_type
        }
        req = self.client.put(item_answer_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

        del_sig = calculate_signature(auth, self.headers, 'DELETE', item_answer_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_answer_detail_endpoint)
        self.ok(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_answers_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [second_answer_string],
                            [answer_type])
        # self.verify_data_length(req, 1)
        self.assertEqual(
            self.load(req)['data']['count'],
            1
        )


        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_question_string_item_crud(self):
        """
        Learner should not be able to create a new item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint, payload, format='json')
        self.unauthorized(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_item_types_get(self):
        """
        Should return a list of item types supported by the service
        """
        test_endpoint = self.endpoint + 'assessment/types/items/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_types(req)

    def test_authenticated_instructor_link_items_to_assessment(self):
        """
        Test link, view, delete of items to assessments
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        items_endpoint = bank_endpoint + 'items/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', assessments_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessments_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        # answer_string = 'dessert'
        # answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            }
            # "answers": [{
            #     "type": answer_type,
            #     "responseString": answer_string
            # }]
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        question_id = self.extract_question(item)['id']
        # answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_question(req,
                             question_string,
                             question_type)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_links(req, 'Item')

        # Now start working on the assessment/items endpoint, to test
        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_items_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_items_endpoint)
        self.not_allowed(req, 'PUT')

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_items_endpoint)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_links(req, 'AssessmentItems')

        # should now appear in the Assessment Items List
        self.sign_client(get_sig)
        req = self.client.get(assessment_items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_links(req, 'AssessmentItems')

        assessment_item_details_endpoint = assessment_items_endpoint + item_id + '/'

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth,
                                      self.headers,
                                      'PUT',
                                      assessment_item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_item_details_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth,
                                       self.headers,
                                       'POST',
                                       assessment_item_details_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_item_details_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth,
                                      self.headers,
                                      'GET',
                                      assessment_item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_item_details_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'AssessmentItemDetails')

        # Trying to delete the item now
        # should raise an error--cannot delete an item that is
        # assigned to an assignment!
        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      item_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_detail_endpoint)
        self.not_empty(req)

        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_item_details_endpoint)

        self.sign_client(del_sig)
        req = self.client.delete(assessment_item_details_endpoint)
        self.ok(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_items_endpoint)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_link_items_to_assessment(self):
        """
        A learner should not be able to link items to assessments
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        items_endpoint = bank_endpoint + 'items/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        get_sig = calculate_signature(auth, self.headers, 'GET', assessments_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessments_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            }
            # "answers": [{
            #     "type": answer_type,
            #     "responseString": answer_string
            # }]
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        question_id = self.extract_question(item)['id']
        # answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_question(req,
                             question_string,
                             question_type)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_links(req, 'Item')

        # link the item to the assessment
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_links(req, 'AssessmentItems')

        # Now verify that learners cannot see this!
        self.convert_user_to_bank_learner(bank_id)

        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_items_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_items_endpoint)
        self.not_allowed(req, 'PUT')

        # GET and POST should be unauthorized
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_items_endpoint)
        self.unauthorized(req)

        payload = {
            'itemIds' : [item_id]
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.unauthorized(req)

        assessment_item_details_endpoint = assessment_items_endpoint + item_id + '/'

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth,
                                      self.headers,
                                      'PUT',
                                      assessment_item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_item_details_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth,
                                       self.headers,
                                       'POST',
                                       assessment_item_details_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_item_details_endpoint)
        self.not_allowed(req, 'POST')

        # GET and DELETE for learner should be unauthorized
        get_sig = calculate_signature(auth,
                                      self.headers,
                                      'GET',
                                      assessment_item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_item_details_endpoint)
        self.unauthorized(req)

        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_item_details_endpoint)

        self.sign_client(del_sig)
        req = self.client.delete(assessment_item_details_endpoint)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)

        # delink the item from the assessment
        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_item_details_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_item_details_endpoint)
        self.ok(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_bad_public_key_returns_unknown_user(self):
        """
        User should get a bad signature JSON if their public key is not found
        """
        fake_public = 'afakepublicKey!23'
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=fake_public,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sup_headers = self.headers
        sup_headers['X-Api-Key'] = fake_public
        sig = calculate_signature(auth, sup_headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.user_not_found(req)

    def test_authenticated_instructor_assessment_offered_crud(self):
        """
        Instructors should be able to add assessment offered.
        Cannot create offered unless an assessment has items

        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        # PUT and DELETE should not work on this endpoint
        put_sig = calculate_signature(auth,
                                       self.headers,
                                       'PUT',
                                       assessment_offering_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_endpoint)
        self.not_allowed(req, 'PUT')

        del_sig = calculate_signature(auth,
                                       self.headers,
                                       'DELETE',
                                       assessment_offering_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_endpoint)
        self.not_allowed(req, 'DELETE')

        # GET should return an empty list
        get_sig = calculate_signature(auth,
                                       self.headers,
                                       'GET',
                                       assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')

        # Use POST to create an offering
        # Inputting something other than a list or dict object should result in an error
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)
        bad_payload = 'this is a bad input format'
        req = self.client.post(assessment_offering_endpoint, bad_payload, format='json')
        self.bad_input(req)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        # POST at this point should throw an exception -- no items in the assessment
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.need_items(req)

        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }

        items_post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(items_post_sig)
        req = self.client.post(items_endpoint, payload, format='json')
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])

        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Now POST to offerings should work
        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']
        payload.update({
            'id'    : quote_safe_offering_id
        })
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.ok(req)
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])
        self.verify_links(req, 'AssessmentOffering')

        # For the offering detail endpoint, GET, PUT, DELETE should work
        # Check that POST returns error code 405--we don't support this
        post_sig = calculate_signature(auth,
                                       self.headers,
                                       'POST',
                                       assessment_offering_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_detail_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth,
                                      self.headers,
                                      'GET',
                                      assessment_offering_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_detail_endpoint)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffering', [payload])
        self.verify_links(req, 'AssessmentOfferingDetails')

        # PUT to the offering URL should modify the start time or duration
        new_start_time = {
            "startTime" : {
                "day"   : 1,
                "month" : 2,
                "year"  : 2015
            }
        }
        expected_payload = new_start_time
        expected_payload.update({
            "duration"  : {
                "days"  : 2
            },
            "id"        : quote_safe_offering_id
        })

        put_sig = calculate_signature(auth,
                                      self.headers,
                                      'PUT',
                                      assessment_offering_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_detail_endpoint,
                              new_start_time,
                              format='json')
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', [expected_payload])

        # PUT with a list of length 1 should also work
        new_duration = [{
            "duration"  : {
                "days"      : 5,
                "minutes"   : 120
            }
        }]
        expected_payload = new_duration
        expected_payload[0].update(new_start_time)
        expected_payload[0].update({
            "id"    : quote_safe_offering_id
        })
        req = self.client.put(assessment_offering_detail_endpoint,
                              new_duration,
                              format='json')
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', expected_payload)

        funny_payload = {
            "duration"  : {
                "hours" :   2
            }
        }
        expected_converted_payload = funny_payload
        expected_converted_payload.update(new_start_time)
        expected_converted_payload.update({
            "id"    : quote_safe_offering_id
        })
        req = self.client.put(assessment_offering_detail_endpoint,
                              funny_payload,
                              format='json')
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffering', expected_converted_payload)

        # check that the attributes changed in GET
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_detail_endpoint)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', expected_converted_payload)

        # Delete the offering now
        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_offering_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_detail_endpoint)
        self.ok(req)

        get_sig = calculate_signature(auth,
                                      self.headers,
                                      'GET',
                                      assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')
        self.verify_no_data(req)

        # test that you can POST / create multiple offerings with a list
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(post_sig)
        bad_payload = 'this is a bad input format'
        req = self.client.post(assessment_offering_endpoint, bad_payload, format='json')
        self.bad_input(req)

        payload = [{
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
                   },{
            "startTime" : {
                "day"   : 9,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 20
            }
        }]
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering1_id = unquote(offering['results'][0]['id'])
        offering2_id = unquote(offering['results'][1]['id'])

        payload[0].update({
            'id'    : offering1_id
        })
        payload[1].update({
            'id'    : offering2_id
        })

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.ok(req)
        self.verify_offerings(req,
                             'AssessmentOffering',
                             payload)
        self.verify_links(req, 'AssessmentOffering')

        offering1_endpoint = bank_endpoint + 'assessmentsoffered/' + offering1_id + '/'
        offering2_endpoint = bank_endpoint + 'assessmentsoffered/' + offering2_id + '/'

        item_endpoint = items_endpoint + item_id + '/'

        self.delete_item(auth, offering1_endpoint)
        self.delete_item(auth, offering2_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessment_offered_crud(self):
        """
        Learners should be able to see and do nothing with assessments offered
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'

        # Use POST to create an offering
        # Inputting something other than a list or dict object should result in an error
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        # POST at this point should throw an exception -- no items in the assessment
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.need_items(req)

        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }

        items_post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(items_post_sig)
        req = self.client.post(items_endpoint, payload, format='json')
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])

        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']
        payload.update({
            'id'    : quote_safe_offering_id
        })
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # check that Learner can get to none of these endpoints
        # For the offering detail endpoint, GET, PUT, DELETE should be unauthorized
        # Check that POST returns error code 405--we don't support this
        self.convert_user_to_bank_learner(bank_id)

        post_sig = calculate_signature(auth,
                                       self.headers,
                                       'POST',
                                       assessment_offering_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_detail_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.unauthorized(req)

        # PUT to the offering URL should modify the start time or duration
        new_start_time = {
            "startTime" : {
                "day"   : 1,
                "month" : 2,
                "year"  : 2015
            }
        }
        put_sig = calculate_signature(auth,
                                      self.headers,
                                      'PUT',
                                      assessment_offering_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_detail_endpoint,
                              new_start_time,
                              format='json')
        self.unauthorized(req)

        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_offering_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_detail_endpoint)
        self.unauthorized(req)

        get_sig = calculate_signature(auth,
                                      self.headers,
                                      'GET',
                                      assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.unauthorized(req)

        # PUT and DELETE should not work on this endpoint
        put_sig = calculate_signature(auth,
                                       self.headers,
                                       'PUT',
                                       assessment_offering_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_endpoint)
        self.not_allowed(req, 'PUT')

        del_sig = calculate_signature(auth,
                                       self.headers,
                                       'DELETE',
                                       assessment_offering_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_endpoint)
        self.not_allowed(req, 'DELETE')

        # GET should be unauthorized
        get_sig = calculate_signature(auth,
                                       self.headers,
                                       'GET',
                                       assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_endpoint)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        # check that cannot delete an assessment with offerings assigned
        del_sig = calculate_signature(auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_detail_endpoint)
        self.not_empty(req)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_ortho_3d_crud(self):
        """
        Test an instructor can upload a manipulatable AssetBundle file
        and 3 images as an ortho viewset to create a new item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint, stringified_payload)
        self.unauthorized(req)

        # Learners cannot GET?
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.unauthorized(req)
        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']

        # expected_files = self.extract_question(item)['files']
        self.reset_files()
        expected_files = {
            'manip'     : self.encode(self.manip),
            'frontView' : self.encode(self.front),
            'sideView'  : self.encode(self.side),
            'topView'   : self.encode(self.top)
        }
        expected_answer = payload['answers'][0]['integerValues']

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        post_sig = calculate_signature(auth, self.headers, 'POST', item_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_detail_endpoint)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        # can download the file with the /files/ endpoint...assume the zip file
        # contains the manipulatable
        item_files_endpoint = item_detail_endpoint + 'files/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_files_endpoint)
        self.ok(req)
        self.verify_links(req, 'ItemFiles')

        files_list = json.loads(req.content)['data']
        for file_obj in files_list:
            # just check that 200 returned...don't verify file content
            # list of one-object dicts, like:
            # "data": [
            #     {
            #         "html_ex02_p05": "iVBORw0KGgoAAAANSUhEUgAAAtAAAAIcCAYAAADffZlTAAAEJGlDQ1BJQ0MgUHJvZmlsZQAAOBGFVd9v21QUPolvUqQWPyBYR4eKxa9VU1u5GxqtxgZJk6XtShal6dgqJOQ6N4mpGwfb6baqT3uBNwb8AUDZAw9IPCENBmJ72fbAtElThyqqSUh76MQPISbtBVXhu3ZiJ1PEXPX6yznfOec7517bRD1fabWaGVWIlquunc8klZOnFpSeTYrSs9RLA9Sr6U4tkcvNEi7BFffO6+EdigjL7ZHu/k72I796i9zRiSJPwG4VHX0Z+AxRzNRrtksUvwf7+Gm3BtzzHPDTNgQCqwKXfZwSeNHHJz1OIT8JjtAq6xWtCLwGPLzYZi+3YV8DGMiT4VVuG7oiZpGzrZJhcs/hL49xtzH/Dy6bdfTsXYNY+5yluWO4D4neK/ZUvok/17X0HPBLsF+vuUlhfwX4j/rSfAJ4H1H0qZJ9dN7nR19frRTeBt4Fe9FwpwtN+2p1MXscGLHR9SXrmMgjONd1ZxKzpBeA71b4tNhj6JGoyFNp4GHgwUp9qplfmnFW5oTdy7NamcwCI49kv6fN5IAHgD+0rbyoBc3SOjczohbyS1drbq6pQdqumllRC/0ymTtej8gpbbuVwpQfyw66dqEZyxZKxtHpJn+tZnpnEdrYBbueF9qQn93S7HQGGHnYP7w6L+YGHNtd1FJitqPAR+hERCNOFi1i1alKO6RQnjKUxL1GNjwlMsiEhcPLYTEiT9ISbN15OY/jx4SMshe9LaJRpTvHr3C/ybFYP1PZAfwfYrPsMBtnE6SwN9ib7AhLwTrBDgUKcm06FSrTfSj187xPdVQWOk5Q8vxAfSiIUc7Z7xr6zY/+hpqwSyv0I0/QMTRb7RMgBxNodTfSPqdraz/sDjzKBrv4zu2+a2t0/HHzjd2Lbcc2sG7GtsL42K+xLfxtUgI7YHqKlqHK8HbCCXgjHT1cAdMlDetv4FnQ2lLasaOl6vmB0CMmwT/IPszSueHQqv6i/qluqF+oF9TfO2qEGTumJH0qfSv9KH0nfS/9TIp0Wboi/SRdlb6RLgU5u++9nyXYe69fYRPdil1o1WufNSdTTsp75BfllPy8/LI8G7AUuV8ek6fkvfDsCfbNDP0dvRh0CrNqTbV7LfEEGDQPJQadBtfGVMWEq3QWWdufk6ZSNsjG2PQjp3ZcnOWWing6noonSInvi0/Ex+IzAreevPhe+CawpgP1/pMTMDo64G0sTCXIM+KdOnFWRfQKdJvQzV1+Bt8OokmrdtY2yhVX2a+qrykJfMq4Ml3VR4cVzTQVz+UoNne4vcKLoyS+gyKO6EHe+75Fdt0Mbe5bRIf/wjvrVmhbqBN97RD1vxrahvBOfOYzoosH9bq94uejSOQGkVM6sN/7HelL4t10t9F4gPdVzydEOx83Gv+uNxo7XyL/FtFl8z9ZAHF4bBsrEwAAAAlwSFlzAAALEwAACxMBAJqcGAAAQABJREFUeAHsnQdgHVeZ/Y9678W23LvjmjjF6b2HJCSkQEhIBZaQsIElgaVtAiGwlMCftmRDWQIsgYUkBEjv1ak4jrvjJtmyrd7Le9LT/zv36T4/yXJs2ZIs6Z2xRzNv3sydmd/MmznzzbnfjeuyDupEQAREQAREQAREQAREQAT2iUD8Ps2lmURABERABERABERABERABBwBCWidCCIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREQAJa54AIiIAIiIAIiIAIiIAI9IOABHQ/YGlWERABERABERABERABEZCA1jkgAiIgAiIgAiIgAiIgAv0gIAHdD1iaVQREQAREQAREQAREQAQkoHUOiIAIiIAIiIAIiIAIiEA/CEhA9wOWZhUBERABERABERABERABCWidAyIgAiIgAiIgAiIgAiLQDwIS0P2ApVlFQAREQAREQAREQAREIFEIREAEREAEBodAV1fX4BS8h1Lj4uL28I0mi4AIiIAIDCQBCeiBpKmyREAERiWB3kLYf+awEx1o6qhHU2e4b+weNnc2uGltoVa0Wx/sakdrqMUN2zpbEehqQ0dXECH718W/XRyzcRt2hacgMS7R+iQkoHton914fBJS4lKRnpCJ1Lh0pCVkIC0+A6nWp8Wn2/QsZCfkIScxH9nWZyZk23GJgxfYfugPVu/PfrqGIiACIiACfROQgO6bi6aKgAjECAEvhrm7HGdPYVsR2Gb9VuwMbkVVcAfqOipRHaxAXWcVav3Qximch3sXj3hkxueYmM5DlgnrgsQxyLe+KHmcGxYmjUWB9RyOSZ6ApLgUJ7ajhXX0+HDfX22fCIiACAw2gTi7WQztO8bB3iOVLwIiIAK9CPjLHIfs2ywSvK19I8raN7jh1sBG7AyUWb8VlcFy1HRW9Cohtj7mJRRhbNJEjEuZjDEcJk+2fhJKUqZicupMF+mmoPai2g9ji5L2VgREIJYJSEDH8tHXvovAKCLgRTJ3ieOMIpe1vYdNrauxsW01trSvjQjmgRTIjO6mdKUjJZSOVOv9uP+cGEo2A0YyEkJJSOqycdcnuSHtGFweXSZG+c+G9Q35CAZTkJddj4SkVoTiOnf1ZhihZSRkf4NxAQTj28N9nB+2uc/t8a1ojWtCW0KTG7bHtQ7okS5MHIuJyTMwMWUGJqXMxCQT1ZNTZ1k/G0nxyRFhzZVKXA8oehUmAiIwTAhIQA+TA6HNEAER2HcCXixzGOrqRKkJ5bUty7C+dTk2t63BJhPMW4MbTER37HuhUXNS1GZ05iI7VIDsjgJkdubZ5xykdWQhsysXGaEcN3TjyDaRGO+Eoo/K9h6y6N7TvLD0Q+5LZ2cnHni3C1vr43Dh3BCmFib2EKDR+81x37N8P97XkA8TLWg0Md2I5jjzZsfXotH6lgTzbSfWoTmhDk2ur0VDXLUJ9hCL7HdHz/aEpBmYnjoP09PC/Yy0+ZiYOgMJ9p3fVz/s9wq0gAiIgAgMEwIS0MPkQGgzREAE+iZAQciOQ1bGW9u8zInlda3vgP177SucJaPvpfuemmBV8XI7ipHXORa5wWLrxyC3sxg5oULkdhUhu6sAiVZRLz4+LIw5jO4TEqyEXn30973HKRj76rl1XkxSPLe0tODnT+/E5upOXH1sNhZOzUNKSkqPnfA8QiFWOAyLaD/OYXTPMv1nP86h7/13HLIsDjtCQdTHVaEurgK18RWoT6xEQ2IVGpKqUJOwA7WJO1wcvMdG7eUDKzzOSF2AOWmHYU76YhySsRgU1knxYa81F/cc9lKUvhYBERCBYUFAlQiHxWHQRoiACHgCEVFokeWNZr9Y2fw63m1+DStb3sDG9pX9iirndBaisGMCCgPjURicgPyOcSgIlSAPxU4gUwRT7DoxnJyApCTLeGHTEhMt60VU76f1FsbRn71A5jSO+6EXhv577qef5veZw46ODtTV1dl3Yf91cnIy8vPzkZaW5sqKnpfjntOehtGi2ItjP60vMc31+35Mx9jIOKeFgibM2804YuKbArsmbgeq4rehOqEctck7UJO4HZVJW1GX0Ld3vN0yjqxsfcP1qAnvCbOLTEuZa6J6MRZmHo2FGcdgWtohtv+7ovm991mfRUAERGC4EJCAHi5HQtshAjFIgMKOHYdM+/ZO4yt4u+lFLG9+FWta30ZzqHGfqGSa3WJscCrGBKeYWJ6A4s5JGBOaZCndMp34pBhOSDKBnG4+ZBPJvud0jnuxHC2oKYCdsO4W2X0JYm6cF8N+GD2t9zg/v1/HdfiO5XH93L7o6f773kPPktP7Gu89jZ97C2p+jo5OO8HcLayDwaAT1RyOCY5FIDDXfe7ssIh2eziq3Rpqwo64zahILkVFYikqk8vcsDGhWzVHbTRtJeva7C2C9Q/X/tp9k5mQg3lpR2JBxtFYZIJ6gQnrrMTcPhlHFaVRERABERhyAhLQQ45cKxSB2CXgRRyH1YGd+KeJ5bcbX8Sy5pewvm25y4O8NzqFHeMxLjgNxW2WGaJjGkpC082FbJYLimQKztRdAplRXN97oexFMYd+GQpUL5C5fopXL4h7D/32+en+84EOd63TJ0bqafvYW/n92R5/HFimH48eRo9TVEcLa0akvbDmkII6ELAKjTZsb2+3YzPBfXbR7MbwvI1dtdiWsB7bkze6focNaQfp3TEl4GtNT7me3xkBzExdiMWZJ+KIrJNteAJykgp2Oza9y9FnERABERhsAhLQg01Y5YtAjBOgGGPPCPObDc/htYan8HrjM9gUWL1XMlmhPIwPzMS4dsv4EJyNCR2zkBGfHYkYJ6eFBTJ9wl4oc+gjyl4g+6EXqdFDboQXn3641w0b4TNE72f0eO/dihbS/M4fy+ihj1h7+weHFNNeWFNct7cXYUJgSlhotwTR2djp8meXJ76HrUnrsC11HbYmr7PKjXU9NsHOnEiU+v6qHztBPT1lHg7PPMkE9Sk4KudUayQmp8cDT48C9EEEREAEBomABPQggVWxIhCrBLy44iv65Y2vOsH8WtPTWGUeZiZh21PHaCMtGJPaD7F+LiZ1zEE+xjoxzOhxcmayq1CXmprqhhTKnO4jy140M7JMUegjyl4g9h7uaTuGy3QvXg/m9rwfM7990UOOR9tC+hLVjFKH+yKUtE/EwsBxCDabPaS+A1XYhi2JlkElZS22pazDjqRNPd5KUFCz0ij7P1b/1KqCJmBe+lFYknk6js05C/Mzj0J8XPj4+20/mPy0bhEQgdFLQAJ69B5b7ZkIDBkBCif21YEdeLn+MbxY/w+83vz0+7bSl9CViPFByyHcZoLZ/LRTOua5aCKFcEpaSg+xHB1h5vdeNHuR7IfRoil6fMhADMCK+CDBjmJxOHeerx/6bY0W1JzWW1R72wfFNaPT4Qh1O9ra2lDYXoip7XPCkerWoHtrsSlxBTanrMCWlJXYnrShR4o9PpAtb3nV9fdWfMO1tnhk5qk4LudsnJBzHgqtpUVuX+9t9NuqoQiIgAjsLwEJ6P0lp+VEIIYJeJHEaOOq5jfxQt3f8XLjo1jd+tb7UhkbmIqpbQsxvX0RpnYuQHpiphPDKZkpYGTZ914wR0eYGVmOFspeFPnh+65YXw4ZAX88/JAr5rHjOcPjyaHvvbe6t+2DEWoKavZj28ZjcfvJCNQH0NLZiM2JK7Ep+V1sSluOchPU0V1TqB7PNjzoepQBc9OOwInZH8CJuedjVvqiSGXM6G2LXl7jIiACIrCvBCSg95WU5hOBGCfgRTPTmL3d8AKeqXsQz9X/FRUd2/ZIJrezCNNaD8W0dhPNwUORk1DgvMopWSkuPRsFM9O09SWYKZajBTNXIuGzR9TD/gt/7PyQGxwtqr31g8PegtqL6dbWVoxpK8Gh7SciUBdAfWc11ie9jQ2py7Ax9R30zvaxqvVNsP/5zttd0+THZZ2LU/MuwpHZp7g0htyG6O3hZ3UiIAIisC8EJKD3hZLmEYEYJeAjhYFQG16tewLP1j+EFxr+ZsJl97RkRBTfFY+JgTmY2Xo4ZgWOQEnX9LBgNktGWn6aE8sUzBTOvtIfo5I+uhwtmCVsRv9J548xhzz27Lyo5jlCMe2j1NGVEimoKaYLWwtR0joJR7efg0BTANvjNmJd8ltYn/YmSpNX97B77AiW4S8197g+JyEfJ2VfgNNyP4QlOadHmh/32zP6yQ+PPaxc9k8ULjrUbYzYD49joq3YdwIS0PvOSnOKQEwQ8JHmYCiApXVP4vHa+/F8w8N7zMmcFsrErLYjwqI5eLjl7Q23npeal4r09HTXUwz5KDOFMwUze940o/uYALyXnTQkrjOnQ0x2/nzgznvrB89Jnj8c+qwf0f5pimn2+S35mN42H6c2fhiNnbVYnfi6E9Pvpf0TbXHNEZ58AHy49n9cnxmfYzaP83F67iU4NvcsJCeEW36UoIvgGvARHsNNr74KvPE6snbuROKSoxGfnR15iBrwFapAERgEAhLQgwBVRYrASCPgRXMo1OnyMj9W8wc8U/8A6uwVeV9dVmc+5rQuwSGtR2N6aBHSktNdxb/0grBg9lFmL5oZZe5tyZBA6YuspvUmEC2o+Z0X1Ty3fHQ6ukIio9NsEr25uQBjWsfj6LZz0NbYio3xy7Em7TWsTluKhoRd5zV904/U/c712fF5Tkifm/9RHJp1nJ2z4Ye83tukz/tPgMesdMUKBJe+ikkTJiB1/nx0vvEaAnn5SDpssbtO6Nqw/3y15NARkIAeOtZakwgMOwIUzuzXN7+Lv1X/Bk/U/RGVHeV9bme+Ne88xwTzvLZjMTl0iEUEzb+cneYizBkZGRFPs7dmUOj0Fs19FqyJItAPAtGCmucYs7LwnONDm49O0+5BIc3KiBTT7AtaCjG/9Ri0V7VhU/xKrEx9GWvSl1rz45WRtTeEavFAzb2uH5s0CWfnfhhn51+BmRkLIm9KIjNrpN8EKJ55TKr//jdMsYfqFDtGsGtHwhFHImH9erS//jpCs2Yh1Zqwl4juN14tMMQE4uzmGaMvCoeYtFYnAsOEgBfN9cFqPFL9v/hHzX1Y3fZ2n1uX3VmAeS3HYUHbiZjcRdEcrvxHwdzbnuHzMPPG5/2sugn2ibXPiRR9NTU1+M4Da7GhsgPXn1SIkxdPNX2REeHZ54KaGCHgb2cUaj467b3T3jfd3Nxs0elmZ/mgmNsct8qJ6ZXpL6E+oSpSVvTIrNRFuCD/Gpxb8NFIS4g6t6MJ7X2cx4a2mzd/8hNMaGnGOLNsJPLN1OmnA7m5sCcgYPt2BLZsQRwFdU6Ozvu9Y9UcB5GABPRBhK9Vi8BQEuANjBk0Xqp9FH+r+Q1eavwHgl2B3TYhPZSFuS3HWrTuBMwwe0Zq6q4oc+9Ic3QFQAoKiYrdcO7zBAq92tpa/N8LG7Gtph3nLC7GYXMmugcV/0Cyz4VpRvdmhRh43nsxTauHT5EXtnnsEtNtba3YkLAcy9Oex6q0V9Aa37QbxaS4ZJyUdQEuKLgWx+SegYT48Etcnfe7odptAs/vV3/5SxSUbsGUgnyk2cN4XEIi4qZPB8aXmIjOA0w0o7EBnatXI5ida5aOw5xlZ7fCNEEEhgEBCehhcBC0CSIwWAQoHtjvbC/DA5W/wMM1v+4z7RwbNZnddhQObT4Vc0JHOk8zX4lnZma6CCijzfzMV+V9+ZkHa/tjqVyKu8bGRlRWVjqRl5eXh3x7lU3uEmgHfib434KPTlPQeTHNCohNTU0uMu2EdVsj1iS9gXfTX8Da1NfRERfcbQPGJE3AB/I+houKbsC4lMmyeOxGaNcEWmtKly9H66OPoCQhHhnJKUhINIuXRaBh3mcUF4ej0IxE51nP9+IbNyAQ7ERo5kykyNKxC6bGhg0BCehhcyi0ISIwcAQoFjpDHXip7lE8UPXfrpETe6m92wrGB2ZgYfMpOCxwins1TaHMKDOFsxfNtG1INO+GbsAnUNhR0NFeQDFN4cxjQGuMBPTA4t6TmKaQpoCOFtP1gRq8k/IclmU8jW3J7+22IZatHMdmno1Liz6F43LPNtuBKh5GQ+J5TfvMyv/8NibGxyHXri1JrB/B89oi0PZUDrvghAW0PTQ6OweFtGXuQcVOdJilIzBjJlInTpKlIxqsxg86AQnog34ItAEiMDAEvCioCVTgL5X/jYdqfgHmvu3dZYRysLDpZCxuOx3j46Y7oUbBnJWV5cQzhZsXzb4iIAWcRFxvkgP7mcfPWw04TtuG5z+wa1Jp0QT878az95FpH5WOFtNbu9bjrbQn8W7G82iJb4wuxo2XJE3BxQUfxweLrkdeUlHMR6XJlr7nF772VcwywVyYmeEexp332US0haGZVsUSyFsOcApmCufonpYOezPQuWYNOumJXrjIPVDuBl4TROAgEJCAPgjQtUoRGEgCXgCsafon7q/8MR6vux+BrvbdVjG1fQEObzoTCzpOQEZqZsSe4W0azNXMSDMjnhRvEs27IRySCTyevtNDiycxNEP/W6KY5lsAimlGT/lWgELai+nmtiasTHoFb2U8jg0p7+y2cfRKn5FzKa4o/lccknl4zP6WXKVB8z1nb1iPiSaMU3h9McGcwOhzkolni9ZbW5BhAc3850kWjaZopo0jx3oOKag5z6ZNZunoCFs6LFKt38Zup50mDDEBCeghBq7VicBAEeDNnpUCn615CH+o+BGWtb68W9GsEEhf8xGtZ5v3cKqzBPQVbaZoZrRTonk3hJoQowS8mKZ/11c+ZFSaYppedYpp2j3KOzfh9bRH8E7Gs31WPDw0/Xh8tPgWnJx3gat0GCvCj8y2Ln8HO371S8woLES6WTVcph67zsQz6syekWf20SKaUWmzkSFi5zBBzQqG9mYMFRXoKC1Fh1k6UiZOdGdmrPCM0Z/hsN5tCehhfXi0cSKwOwHe2JuDDWbTuBf3V/3YbBqlu81Eb/NRTedhUfAkZKZmu2gzLRq9o83RFgHdiHbDOOQTeGyXb6pBrTVLvWCKVSLMskwFZp9Rd/AI8Jiw8xYPRlW9V50i2ovphrY6LE9+Hm9kPoZtSet322DaOz5ceLPZO65DZpKJQutG67Elq8qtZSi75+coCXUhKy3Vieck96BO73O3eOapzfObIjqOPce7P1v2HxeNZgTaR6IZnW5uQcgsHR3ZtHQsjDz4O6D6IwJDSEACeghha1UisL8EfDSsOrAD/7vzR/hL9T1oDNX1KC6+Kx5zW4/FkuYPYEbcIhdtpmj2wtl7m2XR6IFt2HzgMaZl4K4/rcCyjXW49aLZOHru2IidZthsaAxviP8deosHhbT3SlNIs2dUemPXCrya8VeXDi8U17PybmZCNi7O+wQ+OvYWFCaPcyJ6NAlpMiKXV7/8JUy2dBr5Vr8i2QSzf8sVrjxoAppiuUdvAtoJaU63k4yCOtmydJhQ7uGLpqDmcla5sLmqGgmLFyNFlo4Y/lUevF23dyXqREAEhisBf8Pe3LIWv935fdfccG9/c0YoG4c1nYFj2j6AoqTxyMjJQLY1UhBdKbB3vubhur+xvF0UZfTbBi3Cya7F7AIU1KzQqW54EKDQZe8rePJ3xQdT/tZyTdh5e0dOYw5mNi1CZdM2vJL6MN7OfDJi72jqbMB9Vd/D/dU/xnm5V+HqsbdiYtoMV+5oENI8Z5+/6y6UtLch165DJpXDzGwYT36M6HfaQwUjzRTJ9j/c2bRO+87S3IUn2ud2+y1YbnSriQj7YVhv6QQ5pIieNg0Z2VnoWPZPtM+cheTx491x8aVpKAKDTUACerAJq3wR2A8CXjivbVqGe7ffiecaH7JYzq7KZSySTWsf03ghjgyehazUHGQVZTnh7G0aPpOGKgTuxwE4CIvwmNMeQO8oO0bx6L/l9NEgrA4C0kFdJY+Jt0AxusrfG1NA5pjNwFc6zG3IxdjGSTi14gq8nfIUlmb+DdVJ5W67+CD8YO0v8NfaX+H0nEtw9ZjbMCfzsBEtpHnurnvmaWRt34YxFhVOsIiyl8MR8UwBzZSaIRPQ/JJDY+l6RqCduLZ5OM6Ov4cmy3hC8RxgbwKaQ4rowiIkZmQi3iwdoapKhBbI0hGGpr9DQUACeigoax0isI8EvHBe1fSmE84vNP5ttyXpbz628SIcGjoJGemZyC7Mjgjn3jYNLizxtRvCYTmBx94LZm4gI9Ls1Q1vAvx9+Z6Cmo0NRUelaevIa8xDQUMRjq07H8vjX8RL2Q9EfNLMz/5E/Z9cf3zmufjEuK9hXtaRkTKH997v2jqeq1uWLUPbU09iAnM9e/FsfJwU7rJzucuEMgU0ewpknt8Uz14s+8g0RTUDBnGMX1vH6fZGBh1eQFNEMyJtvWXriF+0CPFm6Wh54XkkHHoYkmXpCHPT30ElIAE9qHhVuAjsGwEvnJc3LsUvLOL8ctOjuy04s20xjjPhPDvuCFcZkDYN9ow484Ytm8ZuyEboBBMO1oWs8pW6kUPAP6hGR6WZGpJRado7WOGwoaEB2Q0fwKH1J2MN3sBLWX/pkQbvpaZH8NL6R3Bc5jlOSM/POmpECGmKZ74xKfvN/2BCqBNp1gAQJTCFM3uOUw+H7DuOO1bR4tnetDgR7UR19wI8/eO7p1N40/LBKHSd1f3w4pmfafMwwUxLR7pdDzvfXY6mgkJkzJ1rRTrpbgWpE4GBJyABPfBMVaII7DMBL5xXNL6O/yr/GpY2P7nbsnNal+DkpssxNWEeMnMy3StiCmfemH3uZt60fRRstwI0QQREYEgJ8LcYLaRp72Crkvzd0t5RX19vQjobc5uWYEP9u05Ir05bGrFp8QH65fWPjhghTevGS3d+A8WtLcjKzAKFhbM425CeZ/ZdJpjjTNB2MIpMGc3ItF23XIyZUWgKaieabTwss22aCeCubhHNcephiu3mpu5otJXlxLQNmamjoAAJFlDIZMMr1nR4aP58V3nRllInAgNOQAJ6wJGqQBHYOwEvnNc1Lcc92283j/NfeyxkL4VdRo2TGi/DlKRDkJWX5YSzz6ghf3MPXKPmg49icod4jqgb2QT8Q60X1N7ewbdGjEozIp1Vn4XpjQtQ2rgOz2Xej1Xpr+wmpE/IOg83lnwDszIWDbsHZVdp8P/9EOllpSg0/3eCKWdKYGrdBI7YeczIMysMtlvEeHVVDV4u34ZWE93nzJyBOSZ6U9mcN6PFFMtcMhI5NlHN0ly0mmXZeLz9LizjkLN0WMMquyoXUkxbb1zjLL1douWLbn/pJXSavSOZ0yjS1YnAABKQgB5AmCpKBPZGwAvn0tb1+Pm22/FEwx8jN0suyyo381qOx0lNl2FS0ixkF2T3iDj3Fs57W5++FwEROPgEvJDunb3DC2lGpHMacjC5YTa2NK3dTUi/2PgPvLT2EZyV82F8quQOTEibPiyEND379D3nlG5BUUY6EhlVNsFM8WxtmZrQJXtLz9jRiZ0trfjpsncw1UR20JY7ZfIk/OfLr+DGIw7HCZMnd4tkWyY6Eu2ydHQLZqYDZFjbRaIZrbZxRrNp6aCNw1UupC+aItpS302ZghTL0hF48QU0TZuO9EMOcZYOCemD/3sYLVsgAT1ajqT2Y1gT8MK5sr0c95Tfgb/V/Q86usLZFvyGM4fzaY0fxaTkWcgqCEeco60a0fmb/TIajkYCJgxc59THaNzBmN4nCjifE9mnwfPZO5xHuj67TyHNLDyP1f8BTzX8GR/Mux4fH/cVFKYcvDzS9D0za0zZr36BcZayLtXqYTDizEBvHJUz36BYxUHK6T+tWYdmizjnmZXl8tmzkGd2lo320FCYlo5XyrbiiLFjkdYjXaMJ5B5ZOnwk2gqnOnff2QgnM2pttphIBUMKaIrpXHqj85F8zDFIXrcOnStXoHPuPDW8EtO/voHdeQnogeWp0kRgNwIUz83BRvxm+3fx++ofoDVkF/uojpUDT2u8EtMS57uIM1/teuHMiLOEcxQsjYrAKCFAIR3tk2Z9BkakmQbPeaS7hfTm5tV4Mus+rE99y+15R1cQf675Of5edx+uKPhXXDPuNtey4VBGVnlNo3Xj1a/fjnxLMZdhgpi6Ns40sxu6cbNuWPaMdXW1qLLWAyflZOGquXOQZrmzW0zgbjP7ygulZThv+nQXkU61qDQ90i7KTIsGnyO9/zk6X7Szc/BL66jWOSMr3La1m4iuCls6fDSalg56o80LnVBWhraXX0Jo4SIkydLh8OnPgRGQgD4wflpaBPZIgDeZzlAHHqz4Jf575x2o6tjRY94p7fNwasOVmJ2wGNl52c4TyZsno1ESzj1Q6YMIjFoCvYU0f/s+Ik0hnVWXZRHpOVjb+BaeyvktSpNXOxZtoRb8qvJbLo/0J8fcjouKrzf/sTWT7UTl4OKieH75Bz9AyoYNyLNGZJx1w1bJCoG0KFNEm/p1Pv6JZu24aOZUFKVZpWerNFhvjQWtr63DzU88hSazdswuzEOdVT7cWlfvotAb6+twmmXUYAA7Lt5K9P5nCmUK6W7N7MQ199XKZMAb8RaO7rARKyeSL9o3vmKCGVOmIJWWjldeRoNl6cg66ihl6XDHSX/2l4AE9P6S03IisAcCFM58vfli7T/wo/IvYlMgfMPzsxcHJ+HM+mswP+5YZOf2FM6MQini7EnF5nCX/qFSUBcrBPYkpPk2ygvpWQ2LYTk78HT277AjabNDU92xE3dt+xT+WPUT/GvJd3Bs7lmD6vWl77n07bcQXPqKRZWzkWi+ZMsBFI48u1M2nHGDAjjevktLTEB6UqYTvq0WGX52SynuXPoG6iw6nJGUiL+aveP5TZtx3MSJ2GRR6SKLZj+5cRM+efjhGG+CN9ki1s6rQdsGy6cPOjxiQ1tJJHe0j1qbNY5ZOmjlcHYODr03OhfJS5Z0WzreRcchsnQYRHX7SUACej/BaTER6E2Awpn9Rnvleve2z+HVpid6zJLVmYdTGq7A0Z3nIjsrx0Wcadfga1sJ5x6o9KGbAM8ndbFFoC8h7VPg1VmFucy6szG//li8Fv8Yns75HRoTahygDe0r8ZlN5+GYzLNwiwnpGZnzB7yiYSTf8733YKIJ3RSzXLimuc3rHG+NnnjvM1PW0c9hU50to8usGDyVv/zKUmxubECjieexFpn+wNQpuHzOHOSnp6Ha8khfnDkbFVbZsNoaTbn1yafw1ROOw6KScaaTKY6tAPaU6i5LR7cv2rdq6MS1fed+MvadlYfq6m7xbALa2zoYjZ5nwnnrVheN7rLWCxNl6YitH9kA7a0E9ACBVDGxTYA3lqaOevx86x34v9qf2ZtEi3p0d8ldqTiu4SKc2H4JCrKKIsKZKel8Hmff3LZfRkMREIHYJtBbSPNaES2kT627BIuqT8ILqX+2PNIPIBhngtG6V5sexxvrn8al+TfiX8bfjszEnAGxKvBhjtaNpz55A8abwGVjKcy0wT6Br02okM2L3EUPh3XUsxTSXG6dWVH+sakM71ZVo8wq/J0wYRy+dvQS5KSmIDsl1Qn9vDQO483OlmKVDZNw9cL5+KVl7fi8VU6cRNHrSrQBxTlVsrNz2FrckJ852YYU15zFWTrsOuwbXqEfmj2j0SzPMn8k2zU4+OoraLZUemlHHOk86bakOhHYJwIS0PuESTOJQN8EeHNgjtOHKn6Nn+34Cmo6KyIzMhvq4uYzcHrzlRibPhG5JblOPPOVbHTLgUPhWYxslEaGPQHqAHZhGRIe19/YJdCXkKZHmg/g9Efn1H4cR1aejSfSf4N3Mp+186bLZfj5Q/WP8Hjd/bhp3F24oOgaJ6IP5FrDxlIeufVzKDSbRa6tP8GufYw4u8wb1K/22UWevYilkLXpr1XswIaGRty7apUT07cffRRmWl2P8RaBdpUGuRwPrwso2x/7XGDZOd6trMLSbeV4YXMpProwOzyvPw38vLRz+PWFVXNYYLvp3dFoNiHOLB3OzhEViaaYNiGdtOQoJK1bj9ZXX0XS0UcrS4dnrOFeCUhA7xWRZhCB3QlQOLNf1fQm7iq9EavbwjXk/ZwT2+fgvIZPYEbyQuSOCQtnVhBkBImNKTDizF6dCEQToMCZUphkUb0Q8jKGpkJY9Po1PnwJ8Nzw9SOY/o4R6YiQrs1Ccf2XcEzlBXgk+15sSVnpdoQP9F/fegMeqL4XX5z4YxySefh+2Toonlf8/e9IW78eRVmZZtewyDN7W4trabA7+ksty4AwhXTAlqmzhk5+ZR7npRWVmGqC/4yJE7DAmt1eUFxo89jMTHNn88eZ4OW/LuZ2tnKfs+wczNTRan7rw8eNsWwenS6/dIetICnRZIvN43q3BW4jug+cU9YmoqOFta2Ak2npqKGlg1Ho7mg0bR3M0mGWjrRt2xAwER2aP8+ydOQ5Tt2FaiACfRKQgO4TiyaKwJ4J0K7RGKzDz7Z91aWTCrmrc3j+rM58V0HwyK4zkZOXgzy7WdDn7DNrqMntPXON9W8okCiMTltQgDbLVJBrFUx5vqgTgWgC3u7l7V98m8V6FIxIZ9RmYHL9HLzZ8iQez/51xB+9ovU1fGzd0ZY/+gbcPOEuZCfl7fMDPK93G19/HbV//hNKTLQnmULmWUnNzN5UsCXEsFzMFhBgRJnCOGDLbLGo7+dffwvbLIXdbDuXp1gT3zfOPwTJdk77+V0JFLeuhUFb3grc2tho0edK/GbFKhxRXISdjU3WEEsHKlvarFJhpqXFq8eZ06Yi0crhA4VbqNPEOAMS7BnOdn5pimgbdz8hjltv5ViNzG4RbeLZVS6kpSMPMHGfbBw7bF9b8vORuvhwx+hAova2dnWjmIA9PPKZUZ0IiMDeCPCnwpvJ41V/xA+2f97S0m2PLJLQlYhjmz6I09o+Yj7n4ohw9j5nXuh5IdbFOIJMI70I8NxqtyhZswkPRvwojPjGwp87vWbXRxFwb8F43jAzBh+6mpqaUFtb6/rqxgo8mfo7LM36Gzqxq9GmgsQx+Ny4u3FW4eV7FYgsm42lPPkxs6FZqrkcS7GXROFqPSPB8Qn2Js0ENYUzxS/Dz4wmP7ilDL/ZsMkqDDYhJzkJty5agLNNoDIHtBO5jDcz6uyWcwuavo1Du9nhvvPG23hw4yY02XpnWRBiQVERmm38/JkzXeMrGVbG8qoq/JvZLSaaHS7RMnl0r7xbRFt5DIO7Yjne/dkJbDfR5rNhcgos6XY4Ak1PdHcfMoHebK0rrrUCFp9/vmOkU00E+iIgAd0XFU0TgV4EeCMpbVmPb5fehNdanurx7bS2RTi/4VOYnGYtbFnEmX20z9lHjHospA8i0IuAf0CjGOI4zxvf0EavWfVRBHoQ8OcOK/m1WgU/tmjohfSWtrX4R9Z/473Uf/ZY5uiMM83W8RPLpjG9T5HIMlneo7fcjDzL91xsD3TJJpp5TlJA+3PTWdFMj1JI02Lx3VVrUWZi+7WqGhSb4L57yRGYYkI3OyW5W8x2BxJcQCEsuBmErm5tx2deeNEqHDZYarx4zLAUeR+aNROnT5lsafCYyo4WlgRU2P4FTGjf8cJLuGLhApxnwjosmCmUbTYKZQpoimQnnjnRei+g/TTOY+WZAne/t6C1phhobUOlNQzz9xUrcc7X/gOTrJEXvhVS4MMQqtuNgCwcuyHRBBHYRcDdRDoD+O32u3FvxdfR3tUW+TKzMxdnN1wP2jVyC3ORb6/9fEMo9DnzBqMLbwSXRvZCgOeKFyV+Vp0/noSG70cg+tzhGws2xsK3F7R2ZNRkoKTu23ir5Wk8mnOv2TpqXVFLm5/Ah9cdihuKvoIrx33WrBUpPa5XfAvy+J1fR8qaNZZmLj1cWdA8EXREmPQ0SWqVCM3DHDLLBsVzZ1wIK038vlFdgw32FuWskrG4YeZ0zLBczokWqWarhHFW45DOaeeysDKY6I5+5afKtuKF8h1YWW3bZlr36jmzcNW8Q5CXmubEuvsdUHCbCJ5oHmy2bHjujOl41vJHsxnwMWZfcQtyyyzY4SwcHKdIdnaO7undVhG3A1x3wDzYFOS2bUF7cG00Eb1q+w5MvfhipNDOYQz0BsjQquuTgAR0n1g0MdYJUDizX9u0DF/fcgPWtO+K3vAWcETT2Ti79VoUZ41zwplRZ5/P2QtniZ9YP4v2b/913uwfNy1lWtFEJgUfo8Ic+tR3vDal15yPObVH4tHUX+LNzMdNvHaBrRn+ZOeXXLaO2yf/CnMyD3Nl8I3bxtdeQ+rqVSi0YECSlWsSNKxLbTmX79mEs2vbxL7oYqYLmyfTggbpJpa/Yk12Z1squtkUtjbdZnBiuauT0efw2xVaNhixfnb7dhPQ2/D30q3WUmE8fnLi8WbbKLRUdhT0tlK3bPfRtcgzKxoWmqB/eWs51lue51LzNBdbVhCK63BtQS7E0W4h3WO6fcfKi+4xIOzdDlqZQWsRsbUjiFIT/zumTcfiGTMlnB1E/Xk/ArJwvB8dfReTBCic2ztace+2O3Ff9XddSigPYmxgKi6o/zRmpxzmrBqMOsuu4eloeKAEeO6t2FyL6sZ2zJ2Ui6KccI7cAy1Xy8ceAZ5LFMLRto6amhpn7VjT/hYezv0pdiZtiYBJjEvExwpuxQ0lX0GXWaafufIjKDIrSKZ5mJMTzLphdgdn3bDmtZ0tzYSp08b2hxk5mMPZclsgaGK6xSr1FVteZ85AUe+8zk7I2iQObVq9ZehYZlkx7n5nBTY0NeP4scU4xRpNudAiy9n0J7vlTPBauWzR0Ea6p8Xjf1evxp/WrMf2lhY8ePGFmGheZr8eLhe2bzjJ75aJfI6ydXR1R51Z4TFg4rnM8kX/dWcFDr3oYkvKMc8FRvjgoQh05BTRSC8CikD3AqKPsUuANxz279S/gm+UfaJHE9ysJHhSw+U4vfMKE8757uLqWxGUXSN2z5mB3HOeexQ7Dy/dYg1h1OKWC2bh+PljZQUaSMgxVBYFpbcEUQTS1sGKqcwIlF6TjqkN8/Bswh/xXPb91ghLwAUKfln5LTxT/RAu+8k8jLPobrr5lhMsYMtUdWwfxUlSpp6zKC7/Of+zMbVYr8nbkItUJ5lAzjCxzfOZy3VRMJtIDc9v02280awRz5hV4nsrVqMh0I5Txo3DqeNLcMn0aaaXLb5t8zjR60Lc9iBgue6cCLf1PLZxI5aZ0F1t4vvS2bNca4jh+cPr4fII2bhZSpyY5lYz6kwN7kQ4M4VY1Nn6gNk2giakKyzzx4PvrsBRn/gkppqn2gdF/P5xSXUi0JuABHRvIvockwR4sW/vaMN/bfsP/K7q+3Yr4C0h3E0MzMFF9Z/B1LRDUFBc4MSzsmt4OhoOFAFGC5mFgyKaHSuDcdxF+xhVUycC+0FgT7YOprw7r+Y6zKs+Fn/J+iHKUtbA0nUg48EahNaWIsUq/nWw8h7Pve4+zoSoE6gmh+PsfA1RJFtEml/TXuz0LsetIK7XvgyLaM5n/zpNrLba+H+tXY8HzLJhS+BIawXwk+Z5XmS2DWfXcGKXNouwaOYu0yLSblHicvM+32cV/N6sqDLv8xicMH48Ci21Xni5sGAO55LuFvcU77yWh4tzG0jfdVg4U0CH0GDZS94pL8dxJp7HTJ0aSTvKyoP67ZG+uj0RkIDeExlNjwkCFM7s6XX+6uarsSGwIrLfbIL7tPorcVLoQ8gryEeBXeh9TmddXCOYNDJABHgeMmWYF9AU0z4jxwCtQsXEMAGKQV63GJXm0KdJZDR6bO0P8Fzrny3t3X0oXVSPlesa0Lgz0SoAZiM3OYFy2KLL1KEmPy1qyywZ1Kasn0ex7BS0E6s2E6WyRYC7XOW9MHCK6ZDZOmrsgfDOd1dhtbVMyAfGo000f2nRQoxJTwtXMvRWDS7WLdC53U3mUV5ZW4Obnn0RLRa9PtLsHkus8uCZkyc5cR2OMNsGuW3gFth22fIcevEfjoKz3iArDFpvEehWa42w1GwtNYccgukmxv1bRUbrJZ4JT937EZCAfj86+m5UE6Bg6egM4n/Kv4N7K79h3j1LqN/dTW1fgIsb/hWTMma6iDO9zj7q7F+L+nk1FIGBIMDzkYKZwoJd9PhAlK8yRMDbOigQaeug/YxCmv1ZVVdietVh+L/kH+EvF6zBUS+VoO3d8ZhqInqsfc8gLuPIVg/Q9KrZNSzcnMAIMwO8No0ZOWi7cJYNi/Iy4sxoL8Utp7Gtk4fLtuLduloTxCFcM30Krpo+zSobmt/Z5nQ2DEadrSyKb1o2OvlQab+J7y57By+V70SzRaEZcb58xgycOXWyC35wFU4px5tNxFZC4Ushzd+T66xAlsVPkciz/cbYUuI2y/n8oNlBLrvyKnedZxYlVrwkJxdBD5egvyLQJwEJ6D6xaOJoJ0CRsrl5Lf6j9FqwlS7fJXUl4/SGj+HkzkuQV7gr6syUUIo6e0oaDgWBiAAYipVpHTFFwEdX/XWNopEimte467d9HU8H/4IXjnwQ28Y04pRXpqGxIQdTLH1cmglROiwopBmNTjLRnGii2aemc1+SpAlrhqdZsZDi2lS080+fPqYYb9XU4jOzZyLT1pVlIp7nOWfxYpvi15Vn1+hXTNz+as1alLe0Wp7oNszNz8NPTjrBiW62iOiiyi5qzXGuzyLdHLpNsGkU0d1COmjDDud5pu+5Ezut0ZllJqJPu6qnePZZlLjV6kTg/QhIQL8fHX036gjwYk3x/NeKX+N75beYH685so8lgRm4tOHfMC19LgrG7vI6+2iNIhIRVBoZRAJObwxi+SpaBEiA1zP/No1DRqN5rWOfVPYRTK2eh4cn3Iu/n7kWh749Dk3lYzHdotF5Np/zO5s2ZdVACtakbrFshbpIdJercWgVBm0OimF6pymqS1JT8L1DF5lGjnNNevNazIqGTjR3R4kpiuvNynTfxg3Y3tyKV3dWOpH+hcMW4YRxJSgysZ9gvmsXsbb1WcHh/zbuIuH20dlKIuXGuWgzRXPQtoPWjVoT42+WlmHK1degZNo0l78/utVPXev1G9kXAhLQ+0JJ84wKAhTP9YEa3LXlRjzZ8KfIPlmbWjix4TKc0XElCguKIl5nXlB5U+HFVBfUCC6NDBEBRaCHCHSMr4bXNto5KKK9kGamjsytmRizfSKe6Lofz570FFrebkf7qgkWic5ylo5k2jWMHR0UrEyYZGNWndA+Wc8sGK4ioUWgzVrBac7iYevicux2RZ7tg4lm99lE9Har1PeWtWJ4//qNqLUI8UnWIMtV1iLghOxMW3cGV2ZrMle2zRtel5VHv7X7zGnWuVXyj0lps5oErXxv32i1MjdXVyHrnHNROGGC8z3Tnqc3jI6c/vSDgAR0P2Bp1pFJgBdmRjqW1b+Mr5Z+DNs7tkR2pKCjBJfV3YrZaYchvyhs2WAKI0WdI4g0IgIiMMoJ+CABrRzRKe+YBzlz68cxo3oRHjn81ygfuxqnmqWjPpBj3mhrnMVEa4iRX9OvYUuHtVRo05jSLpySg3YK2jhM2IZ1sxPLjFS7jkMT1PzH8HGrVTLcYhUMv/jWPy0VXiJusQZZxlkg41irNOh8zE48U6gzTZ6tg2LclWHl2zWeo/Rb28BtT4ji2VUYZKVBZvLosHzP9XjOItgXLVjgcvlTPCsV6Sg/wQdp9ySgBwmsih0eBCieWVHwF+V34ZcVd1rcwmqMd3eHNZ+Gi9puQnHeOBQWFirDhgej4UEl4N92MLqnTgSGkoCPRlM4MyJLQe3yRpelY9z2KXis5D48fPZyHPrWODRvC1s68i1XNMUzXRoU00kmVJ0/mSrWdK3LhtEjQswz277kAhS7Fq326e+4r/+1dh0KzUayuCAPF04cj2LL0MG8zbyWl1vDKQEbTrTtS6F/2pYN5322LbB1uMqHti20i1BNB7vCeZ4ZfWb2jR3me37NRPRx1lgKW49lpUF6vxl5VycC/SUgAd1fYpp/xBBg1LmybTu+svlKvNnyXGS7U0LpuLD+JixJOMt5nZmezte+5oXU1eKOzK0RERABEYgdAhTRvA5GR6NpZ6Ooztn6Gbza9QSePvGPaF1mKRdXjsekTLN0mMhNseXc2z4buiwdJpITaOVgVJgeaRO2bMY7bLUwntTRLorsp8Wj0dLKTbF1Ndjwu4sPddvBRlvqLKVjTXsQP1u7FlfMnIbx6RluXabCnRBnhDscRbciu8u0xhRd1NmnrKsx3/OLpaWY+8lPYbJl8WDKOi+ew8tS8asTgX0nIAG976w05wgh4C0bb9Y9hy+XfhTVnTsjWz6xfQ4ub7gNU7JnuagzoxD+FZ6vmR6ZWSMiIAIiEKMEeD1kFNr7gyk2KaLTS9MxYecM/O2we52l45SXzdJRl42pOVnIMGsEA8vUxqxgyCwdSc7zzIizTWfOO+soWJ2tg3NaK4O0X3CpAms23GLNmJmdhTrzQtdZc9+ZFo1+qKzM7BdduHbGNEywbXD+ay5j5YR7V2g4om2jHZbOLmBRa9o2WHGwORjAusoqlFx8CUqmTIm8baR1Q9d9A6ZuvwhIQO8XNi00XAlQPHd2duDX5f+Jeypuj1g26LE7oeESnNNxLQqLiiLimTcFev4UdR6uRzQGt8vOYXbhvzG4/9rlYUOAQpfXR5/ujnVDeM1kX2hZOZ4Y+wf8+qJnccbLU9FcNgbTc7KRb/NQDrNiIc9hRpATrZx4611au27vMl0WLuGciVyuh7YLNtJy08zpWNfYjNuXr7TIdirGpqRioaWvKzA7ySwrP+yFZuMtVmXRlcGV2GcT/C4gbWUFbR0+8sxKg2X1Dag/4ggsXLQo4nvmvkg8D5tTbURuiAT0iDxs2ui+CNCyUdte6VoUfLX58cgsGaFsfKj2czgs5SQUlhS6LBu+oiBfVbqLd2RujYiACIiACEQToNBktJZWN0alKaAZmc4s+wQmVs3G4yf81lk6OszSwQhxidkwukzIMq0cnwddNNoKtPi0hTJsulk7ukz0xjMdnenecBeOKBdY+XMtmn37grkmhE182zWaopyRZgZILEISFsvM7mHmaYpy54OmYDfhHKB47o48MwpN3/NL1nDL4XPnucZSuN20p0g8e+4a7i8BCej9JaflhhUBiueVDW/g1s2XYmdHWWTbJgbm4IqGf8ek7BkossgzWxRkpRilLIog0sgwIsCHOT3QDaMDok2JEOB5yWg0bRwcUoQyMs1+bPkkPHTYz7HNsnSw4ZWGuoBZOrKRaQKZ1baTrKf4TbQ+iRUHXYyajZ6wkXBT0Awl0x/thkC2LUdRHBbHFNvMFx0W0MyyEU+xHBHSVna3kA5auT7yzEqDVeZ7fnzDRhxz6xcwbeZM9wDA67/eOkYOq0YOgIAE9AHA06IHn4D3O/+t4jf4dvmn0d7VFtmoYxsvxPkdnzThXByxbPCiT/EskRLBpJHhRsCidq7j+291IjCMCPC6ybd23vrmo9G8ruZt+woeHfdb/PKiF3G4Zek4ed1kTDMRXcAsHXYuMyJNMc2sHIkmlBN4nptuDjeIYjPQgmFCOZzWzkQzzdQ2iVqb62U0m12cLc+INsW1K9hEOIV3B8WzLRO2b3RahcQAVlVW4MRbb8OYqVOd79mLf13/wyz198AISEAfGD8tfRAJUDwHOttxd+nn8aean0a2JLUrHRfV3oKjks9A4ThZNiJgNDLsCfDGPrkg2XlBC7L0oDfsD1iMbmC0pYPWDvqJXeMrZTdifO10PHnE/1oz3RZ9XlmC5oxMjM9Id6LXtLDzPVMCJ5oittonJn5tIrWw5WtmuRTbFNJONNuQrRrGMSJtk9nTWc3Ud75Cos1oTo6wbYNR5w4roM0qH261fNItx5+I/AkTI75nvXk0gOoGjIAE9IChVEFDScCnqPvipg9jWetLkVUXdUzAlfVfxYys+S7qzBR1vLDzIs8LsiIPEVQaGWYEeG7yBn/K/Hwc3dpqr5uzXbRP5+wwO1DaHEeA5yWtELy+MipNEU07R1ppGsbQ0jHnf9HQdBQOL38L9ZYFg82AZ9p81McMLjOKzPzOiYw8OwsGvdH8lqLaenammJm4g35pF6HmNZxZPboj1V2WbSNky9MrTd9zh/XtHZ2WL7oZT5ogv/yUU51tj3VelHEjjFR/B46ABPTAsVRJQ0TA+50/v/lDqOjYFlnrnNYl+EjrF1BSMDFi2fCvGhnZUCcCw5kAz1GKEKZW5Ktm2Y2G89HStpEARXS0pcNHo7ssQ8ZbL01BRVYL/m9uC6a3PIoT35uMGc7SEc7S4YS0lREyoctUd8zSQeFMfzTzRjvB7PJIM1cHLR2W4M6+j6eXw4lubgBT1nU64RyuOGi5/+3h8yXL4nH6p26MNJZCYU+xr4dRHjV1A0VAAnqgSKqcQSfg/c5PVv0fbt96nfmdW906LSaBUxuuwDld16BwTJGrLMja4hQjyrIx6IdFKxggAry5+ygZz3UKap2/AwRXxQwqAZ6rPHcZ6eW5+9sXK9CW0Im87HjkZA1QbM8AAEAASURBVG7BG9nbEcgOAm9ORaM9HE6wqDVbLWTAmXqYyyTaeKJl1vAVAhlljjP7RpfLI02tzBnN/GHL2ZiLRHfawmxl0Eeg660Blncqq3D0Z25ByfTpzvdM8ex/RxLQg3oaxFzhEtAxd8hH5g7zAttpF8p7t92J/668I7ITbFXwsvpbcXjyKSguLnYp6tQwSgSPRkYQAd7c2dPG4Tvd8D0JDUcKgYffrMCq7UGkWQDjxMkmijsvwpTOSXhq/u/RmrcOp748BQ21AVfBMDshsdvOQRcHLR0mpBldtig0JXNXp/0mmOeOwprRaSpndmbVoHj2wpnR51bzPZc2NKDgwx9B0ZQpkTc58j2HkenvwBOQgB54pipxgAlQPLcGm3HHphvwROMfI6Xnd4zFx+pvx8ysBU48905RF5lRIyIwgghINI+gg6VNdQR4jaa1bunqnfj7Gzvt7UkCzp4NzCie5B4KS2pLUFIzBX+Z/GPcO+YdLLYsHSeunWy+6CzXQAqj0Gx4xUWlrZwkWjVsmhPM9pkmDhd2ton8x+R3bG3QC+h2azyrvLkVbxSPwUWHHhapNKi3kDpBB5OABPRg0lXZB0wgXFmwHP+24WKsbH8jUt6U9vm4uuU/MD5/shPP9I16v7MESASTRkYYAQqRVaW1qKxvx5yJORiTm+YEyAjbDW1ujBHgdXpjeS1+9sgGV0nw5BnxWDwjxwlZitjGxkZkb7NWCrcV4f74u/H2kvfQkdWBuNfN0pHRgYmZGUgxjWynv4s9h8yqkWTqOdEEM8Uzs27EWW3CsJCONwG9q7EUpq6rsGa/l6dn4IRLLo1UGuT9gNYNdSIwWAQkoAeLrMo9YAK8KG9oWonPbPwAdnSURso7vPlMXBr8LMYUjY34nVnhyvvcIjNqRARGEAGK5w5rdvgfr5XhlTXV+PR5M1C4qETn9Qg6hrG4qbxO1zW24nsPrLH0cSHMLo7HsTPSnZ2OWZDojWalWAppXqdv2Ho7/hz/U7y78FWUjmvA5U/OQX2NZemwCobZVtEvnOrOxDT/dVs6XMVCJ6TNtkHxbELbVxqssXzPb1RWYtEXv4zJM2ZEGkvx9wMFVGLxrByafZaAHhrOWks/CdDv/Fbd87h1yyVoCNW6pe2lHs6svwZnxl+JorHhyoJKT9RPsJp92BKggG6zSFrQUn6xa2lptfGgq0woETBsD1tMbxjP2YCdo3c/uAo76wMozozDBxaEM8n4t4KsYMgMGOx9lo6rym7FY42/x3Pj/oJ7Ln4HR75VgrY1E52ILjSRbfp4lzfaBDqj0Syn09bnbRsctpjveaPle5587fUYN2VKpLEU+Z5j+rQcsp2XgB4y1FrRvhDgBZkRjUcr/oBvlN+AQFe7WyypKwWX196Go9LOiFQWZFTDZy3Yl7I1jwgMZwI89ymY2bNrb293EWme4+pEYLgR4PnKNya/eGwdVpY1IT05DhctSkJhfq6zUXgLhX/442cvphmNvrDsOhSaN/rh3HvwxpJyl6Wj7t0SzArmmaXD0jiaaKZpI2QGaQ6T7L7gBLQN2WAK8z1vs5R1wdPPxNzFh/fwPXM9fr3DjZu2Z/QQkIAePcdyxO8JL8iMPN9X/j38uOLfI/uT0ZnjKgsuyFrSo7IgIxq8UKoTgdFAwJ//fIBkx6EfHw37p30YPQT8ufr0P7fiqXcqXQ7n8+fFY0JxjhPPfTWZzWs1hTMj0z4inZR0PvKqzBed+z0sX1CJ0rENuOypOWhwlo4cZ+lgNNpuDa51Tv4e2NIgm+ve2daKNzOycNSiRW6dTF1Ki4jE8+g5z4b7nkhAD/cjFCPbF45mBPGdLbfgz3X/Fdnrwo7xuK7hTkzPnRsRz6osGMGjkVFMgL8JdSIwHAlQyK4prcEvn9zsNu+UmXGYMyHb+Z5pq/MWit7bTnHL7zgPPcoU0hTVmdtz8Nucu1CVuBP3XBS2dLSumWBZOnJQlGaWDlYwtMJCFvGmdaPafM/PV1TjxDv+FdNmznS+Z90XetPW58EmIAE92IRV/l4JUCi0d7ThaxuvwZONf4rMPylwCK5t/jomFkyNZNpQZcEIHo2MUgJM08WOlajUicBwI0DxXFHbhO8/uAZBSyV32Ph4HD09IyKe92aro7WCwplRaopoCmr2WTv+E7/O+ga2Jm/Am2bpCFrDKy3vTsHkYDom2bxptGVYa4TN5nteb/meT/mP21FilQZ9y50sg2XLujHczpjRuz0S0KP32I6IPaN4bgo04LaNl2Fp8xORbZ7beiyubPsSxhWNd+JZr+ciaDQSIwQUgY6RAz2CdpPiua09gO8/sBq1zR0oyYnDGXNTnYUiNzc3YqHY2y5R5FI8s5VARqV9NPrGHd/Gb+O+hTVJb2P5wkpssSwd578y3xpe6cBMyxmdar7n8pYWdJ1/IYqmTu0hnmXd2Bt1fT/QBGQgHWiiKm+fCfBiXNNagRvXn9VDPB/ZfDauDdyOCWMnYdy4ce4iqcjzPmPVjCIgAiIw4AT4QMdKg/c8shbv7WhBdiorDSajID/PXaN7Vxrc2wZ4Ec1re2FhISZOnIipE6bjhpQ7cETiqUjMBxrGteO+M9/CO3PqsaK6BmWWpeblnFwsPvMst05aQWgBoQBX5HlvxPX9QBNQBHqgiaq8fSJA8byjtQw3bzgXGwOrIsuc2HgpLoq7EcXjil2O5+hmuSMzaUQERjEBb93wVo5RvKvatRFCgOKZFbwfemUznl9ZbQ2cABfMT0BJUTjjRkZGhrNl7I+IpfilCGYE20eir9r+BWS25eCF/AcRZy3bP3nECgRzrHKhtV545rWfdOLZv5XkMupE4GAQkIA+GNRjfJ0Uz1ua1uPTG8/C9o4tERrn1N+As5OucpaNoqIi55Gjr42v5tSJgAiIgAgMPQGKZ16zl71XgT++tM1twBmz4zGjxFoWzM8Hgxzef7y/W8drPL3TjChznB7pS7ffhIyWbDyW9xsnol+YvwZFixpw1PjwfLR/cD5ZN/aXupY7UAIS0AdKUMv3iwAvxJua1uDGjWeioiN8MY7visdF9f+Kk1IuwtixY11lFJ8GSeK5X3g1swiIgAgMKAFes8urGvHDh9ej01oGPHpyPA6fmhmpNDhQQQ5e61kWBbm3d5y37Wqkt2Thr7k/RUdaF2pD5fhm43X4f3gYhYmFsm4M6JFWYf0loNBef4lp/v0mwAvxuobl+OSGUyPiOaErEVfUfRmnpH3I+Z0ZefYRDYnn/UatBUcwgXAOjnDarhG8G9r0UUCA1+zm1nZ858+r0NTWiSn5cTh5Tlq/Kw3uK4poEV1cXOx80adnXYoPB76AlGxLe5cH1KVW4HPl52ND80rLD61MNfvKVvMNPAEJ6IFnqhL7IMAL8ar6t/AvG09HdedON0dSV7JrIOXYzHMi4vlAvHR9rFaTREAEREAE9oOArzT444fXoKy6DfnpcbhgYTLyrSEUpo4brEZLGH2mNYP3AgZUJkyYgBNzPoCrm29HcmKys3PUdFXiU5vOwOqGt9XY0H4cWy0yMAQkoAeGo0p5HwKsfPJO7av49KazUB+qdnMmd6Xi6rqv46jM05x4Zi1sief3gaivYobA/lTEihk42tEhIeDF8++feQ9vvFeHZKun98GFiRhbmOeiz4N9rY4W0bw3jB8/HktyT8c19V9HcleKWTzg7iW8p7xb95pE9JCcFVpJbwIS0L2J6POAEmDkeXn9Uty85Vw0hGpd2ald6biu7ps4IvvkiOeZFUKUimhA0auwEUsg/Fpab6dH7AEc0RvuKw0uXbUDD7++wzXrc968REwZm9OjfspgP+ixfN4TeG8oKChwgZYjck/G9Q13gfcQdvWdNbhp8zl4p+5VlyVkRIPXxo84AhLQI+6QjZwNpnhmdOAzm89Dc6jBbXh6KAvX134Lh+Uc7y6IvDD2N3/oyCGgLRWB/hGgaJhUmIK5YxNQmBVuWa1/JWhuETgwArxubyyvxU8f2eDawjxxejwWTMrskXFjqOqneBHNewQzfrBdAN47rqszER3KcDvaFKrHzZvPxfK6pYpEH9ih19L9JBBnT5ty4fcTmmbfOwFehFfUvWEXtnMikWeK5xvqv435uUdizJgx7oKoBlL2zlJzxAYBWp2amppQWVmJ1tZWMM+tf8AcKsESG6S1l3siwOt2bUMLvvSbZdhZH8Ds4nh8aHG6Sy3Kc9G3HDjY0efe20eZwt9HmzWkUlNTgx07dmBF/ev4Zc6X0BLf6GbPis/Fj6c8igW5Ryn1aW+A+jwoBBSBHhSssV0oL8KsMBgtnhktuL7+LieemaqO0QRFnmP7PNHe9yRAUcIGJVhBi5WnmBP3QPPr9lyDPonAnglQpAaCQfzgoVVOPBdnxuEDC8LnI89JXq/5IDfU4plb3FckekHuEheQSevMdDvVGKqzt53nYnW9Khbu+Sjrm4EkIAE9kDRVlnuFtqZhGW7atCvynBJKd+KZFzyJZ50kItA3AYoENibBNI4ULINdUavvrdDUWCRA8cxmun/52DqsKG1CenKcqzRYmB9uaXA4BDt6i2jeS/g284bGb0XsHKxnw3sP06UykKNOBAaTgAT0YNKNsbJ5wdrYuBo32wXMZ9twFQbrv4lFucdExLO3bcQYHu2uCLwvAQoEnweXQtq3sva+C+lLEThAAt4e8cyyrXjynUrE23l4/rwETBwTFs++USuen8OhY8VC3kP4FpMiekHOElzX8E2khNLc5tWFqkxEn40NjaskoofDARvF2yABPYoP7lDuGsXz1qaN1jz32ajprHCr5gXtmtpv4NCcY92FzucO5QVQnQiIQN8EKFR83/ccmioCA0eA1+41pTX4xRObXaEnz4jDnAlZA97S4MBtMVx2Dopo3lMoonmPuab+G5biLtWthm0NUESXNW+QiB5I8CqrBwEJ6B449GF/CPACXNFS7l6dVXRudUWwkZSr6+/AYbnHR8Szfw24P+vQMiIQCwQYDVxTWovnlm9HeXWzWlqLhYN+EPfRXbtrm3D3Q2sR7OzCoePjcMyMjIh45puQ4VqBlYEY3lMoolkpfXHOCda2wB1IsjzR7Co6tuGmDedgZ8s2ieiDeI6N5lVLQI/mozsE+8YLcG1bFW7eeC7Kgu+5NVqDq6557sNzTnLi2VcYHK4X4iHApFWIwF4JeB/qY29uxQ8fWo1/vlfpMg9wujoRGGgCvHa3tQfw/QdWo6YpiJKcOJw5d/Ca6R7o7Wd5vKdQRHs7xxE5J+PK+q+C9yB2Wzs24DMbz0Nde7VEtCOiPwNJQAJ6IGnGWFm8sTe21+OWjRfgvcC7bu/jEY/L6m/F0VlnRlLV8QLHC91w8dDF2GHS7o4QAvw9tbe3u56bzFR2QcuKIAE9Qg7gCNpMnlOsNHjPI2vx3o4WZKXE4SJrprsgP9xMt39bONyv2dw+3lu8nYOR6CVZp+Py+tvsThSWN7w33fLeBWgONOq3NILO0ZGwqRLQI+EoDcNt5AW4LdiKL2y6HCvaXots4Qfrb8YJ6edHIs+8sEk8R/BoRAT2SIC/qUAgYMIm6OZpt+igBPQecemL/STA84w5lR96ZQueX1mNRFMBFy5IQElxuNLgSMv+QhHt7RyMRFNEH5/+AVxYf1OE0LvtS3HbxsvcPYv7r04EBoKABPRAUIyxMngBYvTiG5s+gddanozs/TkNN+C0tMt6RJ55YRvuUYzIDmhEBA4iAf6u+FqdPTuKHN3sD+IBGYWr9ufYsvcq8MeXwvVVzpgdjxkl2c4GwRSKIzH3eG8RzYqFp6VehrMbroscxaUtT+COTdfroTRCRCMHSkAC+kAJxtjyXjz/qOzf8Wjj7yN7f1LTZTg3+Wonnn2LVRLPETwaEYH9IiABvV/YtNAeCPDhrLyqET98eD06Q11YMjkeh0/NjFQapHjmG8OR2HkRzdYSvSf6vORrwXuT7x5vvB8/tnsXA0D6bXkqGu4vgbDTfn+X1nIxRYAXHEbF/rD9x/hdzfcj+7645XR8KOEmZ9uQeI5g0YgI7BeBXW+Y9ap5vwBqoT4JUDw3t7bjO39ehaa2TkzJj8Mpc0ZWpcE+dyxqohfRtKHwfsX+4vJPo6mlDm+lP+Hm/F3t3ShOmoCPlNzsrB96QxoFUKP9IiAB3S9csTuzF89PVP4JP6z4fATEzLbD8dGuL2LM2DEuijHS/HORHdGICIiACIxSArx+M+r6k4fXoKy6DXnpcbjAKg3mWwo4n59/tNRV6S2iGfS5YvttaGyrwbrUN90R5j2sMGkczii6RCJ6lJ7zQ7FbI/NdzVCQ0Tp6EGD04s3a53FH+XWw53r33fjATFwX+DrGFZegsLBQTQ/3IKYPIiACInDwCXjx/L/PbsDr79Uh2dqxumhhIsYW5jmrw2gMelBEsyVP7ltRURHGjSnBdcGvY3xwpjsgVtMAt2+7Bm/VvRCpc3Dwj5S2YKQRkIAeaUfsIGwvxfPmprX4YtllCHS1uy0o6CjBx1u/hQnFkyLieSRWPjkIOLVKEXhfAnbvd90uK8f7zq4vRWCPBCieef1eumoH/vradjffefMSMWVsjntjONya6d7jjuzHFxTRvCd5Ec171Q0tdyG/Y6wrLYB23FZ6CTY2rpaI3g++WsTykAuCCLwfAV58a1or8NlNF6I+VONmzQhl4+PN38LUopnu6X6k1tx+v/3WdyIgAiIw0gnw+r2pvBY/fWSDe2944vR4LJiU2SPjxkitNLgvx8aLaD4o8C3p1MKZuKHpW8gI5bjFG0K1+LctF7l7HFmpE4H+EJCA7g+tGJuX0YvWQAtu23QZyjrCrQwmdiXhYw23Y1b+Aiees7OzMZybe42xQ6bdFQEREAFHgIKwrrEV331gjeU/DmF2cTyOnxnOUJGTk4OUlJQRm3GjP4eYDwi8R/FeVVxcjNn5C3FNA5v8TnbFsAVd3uPYrgHveepEYF8JSEDvK6kYm8/75r655V/wz7YXI3t/SePncFjO8e5CFEsX4QgAjYjAIBPodnB01zQY5JWp+FFJgNfvgLVi+YOHVmFnfQBFmXH4wIIUV2GQlQZjrXVYimg+MPCeRRG9KOdYfKjhc5Fjz3vcXZtvVHq7CBGN7AsBCeh9oRRj83jx/Ktt3+qR6/m0xitxYtqF7gKUm5sbMxGMGDv82l0REIERTCBy/X58HVaUNiEtKQ4XL0pCYX64pcGR0kz3QB8CL6L5AEERfWL6BTjd7mm++0fjffifbd+RiPZANNwrAQnovSKKrRl48eWrv+eqH8Y9VbdHdv7QllPxwaRPuoZSeAGKldd/EQAaEYEhJsDfojoR6A8BnjNM2/bMsq14Ylkl4q0i3QXzE6yyd47zPY/mSoP7wilaRLPJ7wvtnrao5eTIov9V9VU8U/Wguwfq9xfBopE9EJCA3gOYWJ1M8byuYbmlq7s2kq5ucvtcXBn6d/fUzhaeYjWCEavnhPZ7iAl0ezji4M0cQ7x+rW7EEuD1e01pDX7xxGa3DyfPiMOcCVmjoqXBgTgoPkc072G8lzESfVXoS5gUOMQVzxStXy+/3t0DyVKdCLwfAQno96MTY9/xglHTWonbtlyK5lCj2/vcziJc2x7O9cxWBkdjztAYO8za3WFMgDf4SQXJmDs2AYVZieBndSKwLwR4/a6obcLdD61FsLMLi0ricMyMDCcUVdl7F0H+pnyOaN7TxhWPx/VtdyKno8jN1NLV5O6BvBdKRO/iprHdCUhA784kJqfwdVUg2I6vbLkSWzs2OAaspXxN8x0u9Q9TAMX667+YPDG000NGwN/Yj52dh48ck48FU3LVStqQ0R/ZK6LQa2sP4PsPrEZNUxAlOXE4c144yhrd0uDI3suB23r/W/Pp7SYXTHf3Op+Zg/fAr2y5yt0TZeUYOO6jrSQJ6NF2RPdjf3iBYDOv/6/si3i99elICR9q+Czm5xzl8mcq13MEi0ZEYFAI8KbOugUUPGw9jRkDmH6L09WJwJ4I+Ov3PY+sxXs7WpCVEmctDSZbpcFwM92y3PVNjr8rNrTCext/bwtyj7LMHJ+NzPx661P4ydavqFJhhIhGehOQgO5NJMY+8+LLSiePVd6P++t+FNn7k5ouxUkZH+xxI2cFDHUiIAKDQ4A3dApm3tApomWXGhzOo6lUL57/+uoWPL+yGonxcbhwQQJKisMZN3QOvf/R5j2Nvzk+rFJE8553YvMlkYV+X3s3Hq34gyoVRohoJJqAFFE0jRgc56u/9Q3v4lvbPxXZ+1ltR+DihE9H0tWlpqbGRML9CACNiMBBIEABzRs6o2K8qdOnqYfWg3AgRsgqKZ55/X5nQyXuf3Gr2+oz58RjRkl2j5YG9Qbj/Q8of2O8xzE1KysVfij+JsxsWxxZ6Fs7PoW19e/IDx0hohFPQALak4jBIS++DW21+GLph9Ha1ewI5HeOxdXBr2FM8VgXBePrP15gdBGOwRNEu3xQCPC35vuDsgFa6YggwOt3eVUjfvjwenSGurBkcjwWT8lQxo1+Hj3+1niP472Ob35477u243YUdIxzJbV1teDfSz/i7pVkrk4EPAEJaE8ixob+1d+dpZ9Eacc6t/esQHFV09cwqWiquwjr9V+MnRTa3YNOgL/LdVvr8Nzy7dha1aymhQ/6ERmeG0Ah19zaju/8ZRWa2joxtSAep8wJVxpkJFVvDft33CiiozNzTCiYgqtbbo8098175J2l/yI/dP+wjvq5JaBH/SHefQd5k6bv+fflP8QzzQ9EZriw6SZXkUIZNyJINCICQ0bAP9Q+8dZW/PCh1Xhz7U73O+V0dSLgCfjz5CcPr0FZVRvy0uNw/oIk5Fv0NDrjBkWhun0n4EW0z8wxL/cIXND46UgBzzT/BX/Y/mP9JiNENCIBHWPnAC++jF68XfMiflb95cjeH9FyFk5Lu9RVpGDOUPow5b+M4NGICAw6Af4229vbEQgE3LpaW1vduAT0oKMfMSvw4vkPz27A6+/VITkB+OCCRIwtzHO+Z701PLBDyXse7328BzKQdHr6ZTi85cxIoT+p+nf8s/Zl+aEjRGJ7RAI6xo4/L8BMEP+1bVejo6vD7X1JYDo+gs878czXf2qmO8ZOCu3usCDA32YwGLQ+LKDbLa8v00tKQA+Lw3PQN4LnAYMfS1fvwEOvbXfbc968REwZm+0sd8rTPzCHiCLaVypkZg7eG8cFp7nCO7qC+MrWK+0eWiERPTC4R3QpEtAj+vD1b+P9DfobpZ/Azs4yt3BKKB3XtN1uEYwSVRrsH07NLQIDSoC/T1qrfEUlDv34gK5IhY1IAjwXNpXX4mePbLAGp4ETp8dj/sRwpUGfp19vDQ/80PauVDiucLy7R/Jeya6icyt4D+XDrh5uD5z3SC5BAnokH71+bLu/Of9x+0/xYsvfIkte0vxZzMyf715X6fVfBItGROCgE9DN+aAfgmGzARTPdY2t+O4Da9AaCGFWURyOn5nuIs/MYay3hgN7qLwfmvdEWjlm5s7HZU3/FlnJiy1/B++lfODV7zSCJeZGJKBj5JDzAryq/i38tOpLkT0+qvkcnJB+vloajBDRiAgMAwLddQa7XJxxGGyPNuGgEqBAC1i08wcPrcLO+gCKMq3S4MJU98aQlQaVanRwDg9FdHRLhcdlnIclLedGVsZ76ZqGf+otUYRI7I1IQMfAMad4bmirw1fKrkQA7W6PxwQn47K4z6qlwRg4/tpFERCBkUmA4pk++F89vh4rSpuQlhSHixclWTPd4ZYG1Uz34B5XWmKiWyq8FLeA9052vJd+ufRKNLbXS0QP7mEYtqVLQA/bQzMwG+YvwN/f+jmUdbznCk3qSsHHWr8GeruiIxgDs0aVIgIiIAIicKAEeO2mReCZZVvxxLIKxFtE9IL5CZhQnOMybqjS4IES3rflKaJ9Iyu8Z17V+tUe+aG/V/ZZVfbdN5Sjbi4J6FF3SHftUOQCXPUg/tF4X+SLC5tuxCF5hzn/XHp6OhISEtTSYISORkTgIBPoTt9r7REe5A3R6g8mAb45XFNag188sdltxskz4jBnQpYTz0o1OnRHhlYO3iN5rywoKMDcvMU4v+lTkQ3gvfWpqr/IDx0hEjsjEtCj+FhTQO9oLsO3d+xKBj+v7TjL93yZfM+j+Lhr10RABEY2AYrnitom3P3QWgQ7u3Do+HgcMyPDiWdWGqStQBk3hu4YR/uhWanwjLQPY37b8ZEN+M6Om929lvdcdbFDQAJ6lB5r/pCZZufOsk+iPlTt9jKrMx9XdN7mxLOvuc0LgzoREAEREIHhQYDiuc1ygN/94GrUNAVRkhOHM+amOvEc3dLg8Nja2NkK3iuZ7YT3Toroj4a+AN5T2fEe+82yf1Fqu9g5HdyeSkCPwgPurRt/2vEzvNb2ZGQPL2+5FZMKpvXwPUtAR/BoRASGBQH/SKtY1rA4HEO6Ebx2s9Lgfz+yFuu3tyArJQ4XLUxGQV5u5Loty92QHpLIynivjPZDT8ifAt5Tfbe07Qn8ecc9snJ4IDEwlIAehQeZEYz3GlZayrpdTXUf23IBjso8zXm4lO95FB507ZIIiMCIJuADHw+/ugXPraxGYnwcLlyQgHFFObpuD5MjSxGdmJgI3kPph+Y99ZiW8yNbx6a+NzauVlaOCJHRPSIBPcqOLy/CgWA7vrH142jvanV7VxSciA/F3Szf8yg71tqd0Ucg+o0Qf8vs1Y1+AjzODHwse68Sf3hxq9vhM+fEY0bJrma6mZM4+vwY/VSG5x7yGPj80LRyXBL/GRR1THAb29bVgtvLrnP3YP12h+fxG8itkoAeSJoHuSz+YPn673fbf4BVgTfc1iQgAVe2fcmlrMvNzUVqaqoqnxzk46TVi4AIiEA0AYrn8qpG/PDhdegMdWHJ5HgsnhKOcjLjhioNRtM6+OO0cvBe+v/buw/wOM773vf/XfTeQbATJMFeRIlFzVa3RcmyKFmWXJJzYidxchzbeU7OTXnOPTn3npzjGyeyY9mx7LgksSxFlq1uSZRkdVmSSUqyxCawd6IQvS6wwGLv+3+XMwQhAFwAsyiD7+iBMNwyO/N5FzO/ffctek2dWTRbPh/67+ZKm2R3rDL8jtxf9U8MbTfxxZTwPSBAJ5x4/F7Aabrxr43/x33RqzvuklV5G20HFIasc1lYQWBSCmjt1pzCVFlRliSludQ4TspC8nin9LzdEeqWf3z0A2nvikh5UVCuWZZhz9lUeniM7dHm9O/UGdqusLBQVudtkqs7PuNuXa/B2oxSy5bFvwIEaJ+UrdY+9/SE5eun/8Q03eiyR1XWs0A+mfwl21YrJyfHfu3EV4A+KXAOw3cC+rep7SsvW1ogn72sUNaW5zNGu+9K+fwDcr41/N5T++RkfZcUZJppulenSKGZorv/iBuct893mwz/0jJxmnJoe+hPJv+xlPWU213TWQr/v9M6KkeYZliTobAStA8E6ATBjudmnZPw/abpxt7wDvvS+nXS57r/RkqLZthhd/QrQE7C41kqvBYCIxPQv08dJkuDU0lJCX+3I+Obco92zts/f/Ww7DjYLKmmBcCW1clSVlxga5/p7D35i1T/Zp2pvvVa+7nuv5ZgNBar9oS3y4PV36Upx+QvxlHvIQF61HST54n6NZH2/P3Xxv/t7pQ23ViZu8GeiHUaUoY+cmlYQWBSCjg1WjpFs4Zo/U3HsUlZVGPeKQ3Pet7eVlkjT2yrttu7aUWyLCg712lQv42g0mPM1AndgJaPXlv1GqtNOfSae1Xnne5r/qjxf8nRtn005XBF/LVCgJ7i5aknYp0w5R9Of2XQphtchKd4AbP700ZAL8baOUlDs9ZqaYBitjl/Fr+G52rTafClXfWybG6efObSArlsxQyZOXOm/eZB3wOU/dQo+/4ffLUpx5bkP5HSnnl253UkrL8//WUmWJkaRTnivQyYAMY4SSNmmxxP0KLTUTcer/5X+fsz/8XuVFCC8rX278mmsmukrKzM1mJR+zw5you9QAABBBDwn4BeiyORiLS3t0tNTY1sq3lZ/jn7q9Jn/tPlb0q/L7fP/CP7oZhvFfxT/tRAT+Gy1D/aM51V8v1+E6ZcZiZMWZ27iVmrpnC5suvTV0D/pg+dbpFXd1XLiTPtdECavm8FjnwKCfRvyqHNr9bkXipXdG5xj0Cv0Xqt1r9vFv8IEKCnaFnqH6I23fhO1V9JS1+jPYr8SIncHvyyHXWDphtTtGDZ7WkroH/T+o3Sy++flnueqLTtY7VWi4vutH1LcOBTSOBDTTmCfyp6Tdalta9JvnP6r2nKMYXKM55dTY7nQTxmcgnoBVUvrG81Pi8vdPzS3bnbQ38uZaWzRQfe1978fFXk0rCCwKQX0L/r7u5u+6M729XVJeFw2E7YwN9y4otP2yWfPHFCvv2tb0lLW5skB5IkkGwaxZm26XbR8655TF+kT8wckfamWLkETCeydJk1a7YsWbpE1l50kSxYsMC2ZU/8XvMKk0lA3w967dVrcFnhbLn9zNfk37L/1u7iC52/kJsaf0+uLLmRTv2TqdDGsC8E6DHgTdRT9ULb0d0m36z5c3cXVnVdKZdlf9w23WDCFJeFFQSmjID+Xeu3Svqji4ZpXdcLMkviBdS/pblZTp46JQ2NTdJh2rM21tdJj/lWQCN0wATqwqICKTJDDGZnZuoNpozCEgp12rav7SZ0R80sgsXm/suvvELuuvMuuejidTYsJX7veYXJIKAB2plgRZtyXNZ5o2zvelb2pr9ld0+v2RfnvivZGblUcE2GAhvjPhCgxwg43k/Xk7x+zXtf9d1SFTlmXz6tL1Pu7PuvdhgdnTCF4Y/Gu1R4PQTGLqB/21oLqj+69F8f+9bZwoUENPgsX7lSfvDDH0rEnGO1NvoHP/iBbN26VZKSU2TF8gr52//5P2XZsgozQkbs0tlnyky/DewKdcnRo0flpZdelBeee15++eBD8ru335H//IUvyK1bbpXcvLwLvTz3+0RAQ7Reg/VarEPb3XXqv8nX+96T7mBITkeOyL9Vf0O+PO/vGKLSB+VNgJ5ihagX1eNtB+XBlm+7e35j6AuyoLDCDn+ktVUMf+TSsILAlBXQQM0yvgI6fFxpaal9Ua15zswwNc2mGNLN7cuWLJXLLr/MDjE4cK+0rObOmyvr118iGzZskB98//uye/duuff790pftE/uvOsuO1bwwOfxb38K6DVYr8V55oPTgo4KubHxi/Jk1r32YH/efI/cVPB7sih/Od9OTPHipxPhFCpAPUnrV7r3VP+lO+bzzJ6F8vG0z9lPutp0g9rnKVSg7CoCgwgQnAdBmYCbOjs65fiJ47YTZ6oJQ3NMQNbxuQdbtNZRw3defr7ceOONsmXLFikyX+EfP3JUHn/0MXnn7bcHexq3+VTAqYXWa7LWQus1Wq/Vuug039+p/it7LedvfWq/AQjQU6T89A9Nvyr8TcMz8pZpU+Usn+r+cykqLLZfFzFdt6PCbwQQQGD0AnqubWxslOqqKtuJMDs3RxYuigWgC201w4SmlatWyYLyhWYU4Kjs3fuBbNu2ze0ceqHnc78/BDRE6zVZm3LoNVqv1c6i1/DfNGy113RCtKMy9X4ToKdImekfWUdXm9xz5i/dPV4Xuk7W5V4p+abWIz09naYbrgwrCCCAwOgFdPSTKhOeNUQHkpKlwHwVv3Tp0rg3WFRYZIcT1RDV0dEmB/btlzbTyZBlegloUw69Nus1Wq/VF3dd5wJ8t+6vpLO7g2EqXZGpt0KAngJlpuFZOw7eX/NPpuPgUbvH2nHw04Gv2VE3srKyaLoxBcqRXUQgHgGTuexi/uxZJkhAw+6+fZW2c2CKGcpuRtlMmTcvNj1zPLuUlJxk2rfGuhjpyBwaxLvNsIQs00vAacqh12gdleP26FckPWra1ZvlVO9hecBc0/XaTi301HxfEKCnQLlpx8HazlMDOg7+gczNX+h2HNQ/VBYEEEAAgbEL6JTMBw8ckF7TlEO/hp81e7Zt3xzvlrvseN6xwBz7HBQ4O3J0vFvgcX4R0Guz06FwfsFiuSH0++6hPdD8LanpPOmOvOPewcqUECBAT/Jicmqff1D9/0go2mH3tqRnrnzMdErQT7SM+TzJC5DdQwCBKSfQ2dEhx4+dMM3izCQpmVlSvmD+iJrI1dfVSZ350UW/xi8qKmQUjin3LvBmhzVAO2NDa1OOj6f+nug1XBe9puu1nVpob6zHeysE6PEWH+Hrae3z3uZ35PmOB91nbgl/WYoLS2znBO35Te2zS8MKAgggMCYBDTO1tWek9kyNnTwlNydbFi9ZEvc2tQPiKTMZS3VNjR0CLyMzw8xQuNTOThf3RnigrwT0Gq3Xap2hsLigRLZ0f9k9vl93/Nxe4/VazzK1BAjQk7i89A9KO7P8c+3fmN7csT+uiu6LZVP2DbbpBh0HJ3HhsWsIjFLAbYxFs6xRCo7taZ2dnXLs6BE7u2BSUlAKiopNDfSCuDdaXV0te/fsNbMaNpnnRGXxogq5aN1FTO0dt6A/H9i/Q+HG7OtlSfcl9kD12v69M//dXusJ0VOr7AnQk7S8tOmG/jG90vCEvB9+w+5lMBqU2yNfpePgJC0zdgsBLwXoWOSlZvzb0im8Dx06ZKfp1nH1Z84oldlz5sS9gfd/9568vX2HdHV1m3N1odx082bZdOmlcT+fB/pTQGuh9f2kHQp1bOjbIl+RoPlPl/e6X5fXGp+y13z+7qdO+ROgJ2lZ6R9RKNwp36/7H+4ebuzaLMvz1tmvgbRTAk03XBpWEEAAAU8EtAOhBmgzNILt/DVrzty42y8fPHhQtj67VQ4dPiRZ2dly++23y+133GGb23myc2xkSgvoNVuv3dqUQ6/lG7pudI/n+2f+b+kKhxiRwxWZ/CsE6ElYRhqetR3d47U/kdORI3YPdeibWwN/aseTpOPgJCw0dgkBDwT0AjurMFVWlCXJjFz6N3hAOuJNtLS0yPGjx00FhdjawsWLF8W1jWNHj8mP/uWH8vRTT9nO3V/4wh/In33tqzJ3bqzDWFwb4UG+FtC/7/4dCreYa3paX4Y95pORQ/JIzQ+ZXGUKvQNiA1VOoR2eDruqAbq1q1nua/pH93CvDt0lcwoX2E+uOqyStqdiQQAB/wjoxVW/4t1UUSCrZqVKtqnB1Iut3s4yPgJacWEnUGlqMOfYJCkoLJClS4afQEU7He7Ytl2+971/ltdfe13KF5bLl770J7Llttskx8xgyIJAfwG9dus1XGuh5+SVyzWNn5HnMv/dPuT+5rvllpL/LAVZRfzd90ebpOsE6ElWMBqe9YT8YM13pLGv1u5dbl+hbE79T7b2OSMjw15UJ9luszsIIDBGAQ3K+vWuDnWl3zLpul5oCdBjhB3B07XT9mHTfKOjo9OeZ/PNDIQFpr1qc3PzeVvR/inNjU2yfcd2eenFF2XbW9skNS1V/vCP/lA+//u/L0tGMGrHeRvmH9NCQD8Y67Vc/9Y3t/8neavvSWkNNppr/hn5ec135Uvz/9Z2OuVvf3K/HQjQk6x89MRc31kjv2j9Z3fPbuj6fSktLWPYOleEFQT8J6AXSx3qSmue9YO01lRRAz2+5awB+sD+A/ZrdLXftWuX3GnaMPdfdGKUiKnk6OjsMEG7QyoWV8hX//xrcuuWW6Vs5sz+D2UdgUEFnL/1nJwcKc0vkxvO/L48mvkd+9iHWr8rn+r8kpTmzKKybFC9yXMjAXrylIW9aGrt809r/1E6oq12z4p6Z8l16XcybN0kKid2BYFECOhF1bmwOtvXf7OMn4AG6CNHDtsXzDbjP99xx6fl85///LmZ4kx59EX7pMNM9b1r92554L77paqmSn775lty0UUXmSm/y/jGYPyKa0q/kjOsXZ75luO61jvl1d5fSkNytb3231d7t/zXjLvth2jOAZO3mAnQk6hstPb5VPtRebLtJ+5e3RT+QykqK7adWbR9JH9MLg0rCPhSgL/xiSlWrfVvM8G4qrpGgiYo5+XmyaZNm2T5yhWD7tCll18u69atk2/d/U159rln5VTVKfnLv/orueFjH6OPyqBi3NhfQP/OnWHtivKLZXPNF+WB5K/bhzzR/mO5q+MrMi95EbXQ/dEm2To90SZJgThtn3965h8kLN12r2b3LJaPZt5C7fMkKSN2A4FEC+h54Eh1q7y6q1qO1bYxpFWiwfttv7en17Z/bjWjcARN841804Fw0aKF/R7x4dUNGzfKpz59h8w3U33v3rVbfvHQQ7Lvg8oPP5BbEBhEwKmF1g6FV2XeKjN7Yu+37miX/Fvt3zPF9yBmk+kmAvQkKY1Y7fMRea7jP9w9urnnj0wngwJqn10RVhDwr4DzIfq1XVVyzxOV8tbeGi6g41jc4Z6wVFZWSld3l60ZnDljhsyJYwi65StWyArzo8v77+2U995/71yTj3Hcf15q6gn0r4XWa/0nev7YPQjNAifbD/NeckUm3woBehKUiXPh/Pfab0iv9Ng9mtezTDZmXe8OW8fXupOgoNgFBBIooOcBbYPb1dVlX0V/67/1dpbEC4TD3bJnzx6JRkTS0tNMeJ4nuaZ96oWWwoICKSwqss02mluaRKfydsrwQs/lfgT02u5MrrIp6waZF15mUTQL/LT2H/gQPYnfIgToSVA4Wvt8ov2QPB/6ubs3N/X+oTucFW2fXRZWEPCtgAblnp4e+6MHqSFMOxUToMenyLvN1NuHzEyCgWBUMs0QYwsXxr5Ov9Crd5sPOWHzXDH9PXvDZnQOMzKHliMLAvEIOLXQOnSlHdau94vu057v/Dm10K7G5FshQE9wmejFMTbyhvmkGXVqn5fLhqxr7bB1+smU2ucJLiReHoFxENBzgX6Y1h9d+q+Pw8tP65fQCVRqamulxtQea7tUrXleFOcMhGfM82rNj5afDn2XZsbu1t8sCMQr4NRC67B2G7OvG1AL/Y98kI4XcpwfR4AeZ/CBL6cXyeNtB+XXoYfcu26O/KHtOMiU3S4JKwhMSwENZfrDklgBrcQ4dviItLa0mvCbIkXFxWZGwfhqoOvOnIkFaHMuT003M0iaEKTjebMgEK+ABmj90KXXfB3W7iaTAZzl+c4HqYV2MCbZbwL0BBaIXhj1q76fnfmmW/s8v2eFrM+8xrZ9pvZ5AguHl0ZgogTOBmZi8/gVQLi7W3aaSVPCvT2Skpwks8pmSklJyQV3QCtAas/USUNjo2nBcbYta3aOpBKgL2jHA84X6F8Lrd9An98Wmlro87Umx78I0BNYDvbk23lKXug8V/t8U+SLbttn/URK840JLCBeGgEEpoWAtmPeu3evaTcTNR0I0+3oGzrV8oWW1tZWqTp1Sjra20wb6IBkpmdKZnaWaUfNpfVCdtx/vsCHa6G/6D7g16YtdE3HSbd5l3sHKxMqwF/5BPFr7bN+bfjgme+cN+6z1j5rO6hU046O8DxBhcPLIoDAtBHQc3FHe7uZgfCIHf85y0ylHm8HwoaGeqmurRYdQ1o7EershXr+HrjQDGegCP8eTOD8WujrZE5PhX1Yj0kJmhXoVDyY2sTdRoCeIHutfW4M1cnTHT919+CG3t+j7bOrwQoCCCCQeAENJRqe6+vrTAfCJCkoyJclS5fE9cL1dfVSW31Gek0nRF0yszJlYM21nutPHD9htl9Pe/a4VKfvgwbWQl/f+3kXQ7OCZgZ9P7FMDgEC9ASUg1P7/Msz35fOaLvdg5LIHLky82ZqnyegPHhJBCaVgKnJtAuNoMelWHpM843KffvssIEaYEpN2+e58+bF9dqhUEj0x1mysrIl29Rg918aGxrk0Ucfkf3mNXS0DxYEhhPoXwutmUCzgS6aFR458y/UQg+HN873EaDHGVxfTj9Btne3yuNtP3Jf/brw50ztc77thUvbZ5eFFQQQQCChAl2mA+EHZgKVSG/E1EAHZOacOVJsRuGIZ0lLSzdtplPNQ6PagsOcvzNMDXSm+1TtD3rQjC1dXVUlhfn5doZD905WEBhE4Pxa6Hy5ruez7qMea/uhdHS3UQvtikzsCgF6nP219llrIZ6s+3dpiTbYV8+LFMvVGbdR+zzOZcHLIYDA9BPQ829LS4s0mpEztFnFvsp9smPHDjuMmK39S06x9+n9bW1ttsZvKKUyne7bBO6k5GTzkIBtApKUdO6y2tnZIe+Z6b11RKXSmTOH2gy3I3CegL4PtR+Utqe/Ku02yY0U2fubo/U2O+h7mHb155FNyD/0r55lHAX0Ta9Txj7S8gP3Va8O3ykFMwpt7TOzDrosrCAwLQVowZG4Ytfgoe2df/rv/y71Zvi5blP7rM03Tp8+HRu7Odonzz3/vJwyI2vkmA6BC8oXym233y6LKxYPulOz586RDes3yLa3fisnTp2U+jozJrQZF3rhokUS7g7Lq6+8Kh/s3S1XX3ONFBYWDroNbkRgoED/WujCvCK5uvbT8quMf7EPe7jlXvn0jD+132bo41gmToAAPY72Tu3zKw1PSnXfMfvK6dEsuTb1DjvuMyNvjGNh8FIIIDDtBLT5nIbjN157XZJTkiXXNKuYa2qQyxcscC36+iLS1Nwkx48fl2YzscrV117j3jdwRc/ZH9t8o1TX1MhDP3/QNNc4LM89+6x92KEDB+R18zqXXXG5fOzjH2dUpYF4/HtYAZ0R06mFvr7pLvl19H7pCnSY7HBcNEPcmHaXfU8RoodlTOidBOiE8p6/cQ3QOnHKw83fd+/Y2LXZdFqZSe2zK8IKAgggkBgBnSHwqquukmdf+LUNH/qN32BL1ARtHVlDQ4y2cx5uKSsrkz/50z+RZcuX2fD8u3felbe375CyGWVy0y03y42bN9sKkuG2wX0IDBTQYKzvz6ysLCnJK5ONdZvl9YxH7MM0Q1xXcrvb7Gjgc/n3+AgMfvYYn9eedq+itR97Wt6W3T2/tccejAblY8mfs+2ctI2cnqxZEEBg+groRXNWQaqsKOuWmfkp1Fom4K2g51mdMvlCi3YNjHfJLyiQW7dskVs++UnbbjpsRvbQ0TgGDmkX7/Z4HAIqEPsAl2YzwseaPidvRB+TvkCfzRB7W96RdamXkxsm8K1CgB4nfA3PWvv8UMN33VdcHf6ILMhfYj9h0vbZZWEFgWkp4NQ4rV9cIMvKUmwA47wwtd4KGnjy8vKm1k6zt5NWwDknaC30/JwKWdVypexKe93u7y8avier8jdQCz2BpUeAHid8DdDVnSfk9a5fua94vcRqn9PN1LHUPrssrCAwLQX0YqltHvNNu1ytudRvpegX4d1bQZvQ6Xn40Olm+Y/XTpj1qGycH5QVc7LN5CkF9gOLNvHgXOydOVsau4C+HzUj5Obmyg0tn5NdEgvQr3U9IbWdp2Ru6kIbosf+SmxhpAK0GRip2Cgerydune3qsfofS0TMlK9mmdezXFZnb6KWaRSePAUBPwpogNYAp1/99w90ejvL2AU0PFfVt8n/eWiP7DraJJnRVpmV3Ws/qGhA0dp+wvPYndmCtwL9a6FXZ18qc3uW2hfoFdOfqp6JVbzVHtnWCNAj8xrVo/XEHQp3ytb2+93nXx2JjbxB7bNLwgoC01pAL5Qa4DTIac2zE+gI0GN/W+g5uCPULXc/+oG0dUVkfkFArl6aboeW0xp/zsNjN2YLiRPQ84J+K6W10NdG7nJf6Nn2B6Szu4OJVVyR8V0hQCfY2x26rvEJaYqesa+WHcmXKzJu5ivDBNuzeQSmooAGZudnKu7/ZNtn5xvAe5/aLyfquyQ/IyC3rkmV4qJCW9PvhGc+qEy2kmN/HAEN0M63U5dn3CSaIXTRTPGKyRZMrOJIje9vAnSCvfXkrZ0Hn2j+iftKl/bcLAW5hfYTpU7bzYIAAgiogJ4vjtW2yau7quVoTRuzjY3xbeGE51+8dkS2H2yS1OSA3LYmWWYU59vaZ+2cRUfNMSLz9HER0KygtdD5OQWiGcJZnmz+V5sx9L3OMr4CBOgEe+tXhwfbdsvu3rND10lQrku50619ptYjwQXA5hGYIgJO2HtzT7Xc80SlvLaryvad4MI4ugJUNz3/bq+slcd+W2U3ctPyoJTPzJOioiL6n4yOlWdNkIBmBa2F1um9r035tEkSsfi2q/ctOdS2h2YcE1AujMKRQHTngvhY44/cV1keNp0A8hYxcYorwgoCCKiAni90/OCuri4Lor/131rzNF4ftHUfQqGQtLa2SkdHrG2l1tBqx0Ydnk3bZk+VRcPz0epmuXfrIdG6uY8sDMqa+Tm25lnbkjLixlQpSfZTBfQcoH+LOob5vOzFsqxlo3yQus3iPN70E1mav3ZczxWUiggBOoHvAj2Bt3Y1y0uh2OxB+lJXyx1MnJJAczaNwFQV0PCqzb00NOvS3d1t/61tdBO96Lmqvb1dTpjpq/dVVsoHe/dKTW2tqQGPSKqp9Zq3YL5s3LhRVq9ZI4VmyLeAaZM5mRc9npb2kNz92D7TgbtPKkoC8pElmTY86wcBJq6azKXHvg0loG2h9b2rtdBXN98hH0gsQL/Y+Uv5ctf/lqKUEoa0GwovAbcToBOAqpvUi6E27H+x8RHpjLbZVymOzJL1mVczcUqCzNksAlNZQM8Z+qPhTxf9rT96WyKXHhPYj5ng/Marr8mvnn5aTp48IbNnzZHlK5bZi3VNTY08/tjj8sxTT8ldn/2sfOpTn5LSGTPGrVZ8pMeuXmHzQeTbphlMbXO3FGeJ3LI63XYY1OEBtR2pBpHxqtUf6f7zeASGEnBqobXt/vrMa6QoPFMakqqlwwzJqFnjjswv8d4eCi8BtxOgE4Cqm7QncXNh2tr6gPsKm3pvkpziXIZMckVYQQABR2AiAp0213jvvffk/vvuk1deelnyCvLltts+JXfceYdUVFTYi3FjY6M8eP/98uMf/0T+7V//TTLSM8z9n7a1YM6+T5bfet7VMfd/+uuDsvt4m2SkmE6Da5OlpCjWaVDD83g2iZksLuyHfwT0w59+K5WTkyuX1twszyTFBih4pvV++WT4D+gUO45FPbm/hxtHCK9fSmuOjrbvk7292+2mtcH/VSm32dpnbXs3ERdLr4+R7SGAwNQV0DbOr5la5298/evyqyd/JYXFRfLlP/sz+Yv/6y9k6dKlNjzr0RUWFspV11wja9ddJKdOnpRnnn5K9u7Zk/Ca8ZHKanjWb/1e2XlannvvjARNm9FbVibJvBmx8KztuBlxY6SqPH6yCWh2cIa0uyr1Nrcz4Qe9O2zmcL7Bmmz77cf9IUAnoFSdWpCnmu5zt740vEFmZy9wh64jQLs0rCCAwDgLaM3zm2+8If/0rW/J9h07pLi4WD59551y12c+Y89RA3dn5qxZUl5eLqmm/eXuPXvlnbffdjs7DnzsRP1bg8P+E43y4+eP2l24piIgy8w03foBQNuM0mlwokqG1/VSQLODM6SdZool4fXu5p9u+hkj97gaiV8hQCfAWE/koe5O0Yb9znKlfNL2ZKfziiPCbwQQmAgB7ai48/2d8oN775Vd778vOVnZcuUVV8idd91lvxoebJ/0K2MduSLNjMLRbkbo2L9vn1RVxYaGG+zx432bnnPrmzvkm4/vk55IVFaVBeTSRVl2uDo6DY53afB6iRZwOhPqtypXRG9xX+6lzoelO9zl9qNw72AlIQIE6ASw6teIbzQ9e27mwb582ZhxA0PXJcCaTSKAQPwCem46cuSIPPCz+2Tb9h2SlJwi5YsXyZ2f+6zMnj17yA1p0wf98J9kfveYNsbHjp+QqtOnh3z8eN6h3/h1dYflm49VSlNHr8zKC8jm1Rm25plpusezJHit8RLQWmhnSLuN6TdIVl+efemGaI282fScbco0XvsynV+HAO1x6TvNN55v/bm75UvCN0hedj6dB10RVhBAYGiB2KgbJhd6vmiHwGeeelq2PrPVtmHOzc2Wj37pJ+d4AAA83ElEQVTkI3LZZZcN+1raMU/HpY709tgh7BrqG6S+vn7C20E759sfP3tADlZ3SHaayJY1KVJkOkPqiBs6Zi6dBoctWu6cogJOZ0Kd1fiSnuvdo9ja8oBtxkFbaJckYSsEaI9p9U3bFKqXt8Mvulv+aPKttvOgTkJA22eXhRUEEBhHgbAZV3qnabLxi188JJ2hTklOSpYFpl3zTZs3X3CCFGd8aj2/mcovM9lKp7S3TexU4xqetUb9qW3H5ZU99ZIcDMiW1SkyqyTfNt1gmu5xfHPxUuMuoFlCM4W+z69Kus19fc0ejZ11NONwRRK3QoD20NY5ob/U9JiEpdtueWZvuVRkraEmxENnNoUAAiMT0HOTtll+4vEnzGQpJ80IG0mSlZku6y6+WFaZyVEutGiADnV22olV9LE9YZ3wpWfCaqD1eDTM7zxcJ//x2km7+9cvDcjiWTnuNN2MdnShUuX+qSygAdrpTKgZY2bvQns4mj1ebn7cfrjUvxOWxAkQoD201TerziL2UvvD7lY39H3cdh7UT4r6lQsLAgggMJyAqeC1i5eXPh1149133pWXXzLfjJnzVMDU1paYyVA++tGPSlocMx12tHdIc1Oz/WpYL9xaCy3Ojp7d3/H8peG5qr5N7nnygET6orJhXlDWl2fb8KydHTnfjmdp8FoTJdC/M+GGvo+5u6EZRD/0EqBdkoSskOg8ZLUn9Y7jsrv3t3ar5jIjV6Z8wtY+UxviITSbQgCBEQlo7fPWrVulsbFJgklmHFnTfKOiYoms37Ahru1o2+k60+a5L2JmSTTJPsk8P8V0QNQwPd6Lnmc7Qt1y96MfSFtXROYXBOTaZXQaHO9y4PUmXkD//jRbaFt/zRqaOXTZ07tNqjtP0IwjwUVEgPYIWD/paUeb55seMteXWN3Rwp41MjurnM6DHhmzGQSmm4AXNUja+W9/5T7Zvn2bubwGtQJacnJz5OJ162yNbTymDQ31ptNg3dla56ikZaSZi3bGuAdo5zx771P75UR9l+RnBOTWNalSVFhgOw3qcHtaKzcRwT4eRx6DgNcCTmdCzRrlPavt5vukT15o/qXNJF6cQ7zeZ79sjwDtUUnqm1S/Mnkl9Ji7xU3RGxn72dVgBQEELiSgwW9GXqqsKEuSWQXe1PA2NDTIq6++Kg11Dab2WUzADEhp6QxZv3FjXEFTz2064kaTacKhi1YPZGZkSaYZP3o8g6oTnn/5+hHZfrBJUpPNNN1rkmVGcWymQToN2uLhf9NMoH8zDs0czvJyx6M043AwEvSbAO0RrH6teKStUo5GPrBbTIomy2Vpm5l50CNfNoOA3wU0jGqnoIsX5ctnLyuUTRUFY556WkepOHnipLz11pv2mzENv3b0jQXlsmLlirhIdcrvmupqaWttiX1BbDaSl58rOkHJeC0anvUcu72yVh59KzaBy+ZlAVlQlut2GmSa7vEqDV5nMgk4542MjAy53GQOzR66HI7ssZlE/25YEiNAgPbA1akZeaX1cXdrS3vWS0l2mW2+oRdFFgQQQGA4Ab0Qauc3nfyjpKTE/tbJS8ZSy9vR3m6m3t4tp0yI1poqrT7OyM6SZcuX2W/Hhtsf576mpiY5bSZNCZl2x7ovwaSgaTJRZJpM5DsPSfhvDQFHq5vl3q2HbA34lQuDsmZ+bMQN7TTINN0JLwJeYBILaMbQ5ksl2TNFs4ezaCbRpqWaUVi8FyBAe2Cqb04dfeM3nU+5W7skcJ0dn5HOgy4JKwggMIyAhlM9X+j0vDoJiP4e6/mj0YTft7dtky5zfjKbtz/FBYWydu0aW9s9zO64d9WdOSPVphNir5lEJWq2oR0QS0pNwDf7OB6LhueW9pCZaXCfhMJ9UlESkI8uybQ1z0zTPR4lwGtMdgHn3KGdCdcHzk2qoplEswkBOjElGKvrT8y2p81W9QR/ouOgHOnba485SZJlY+oN9hMhXytOm7cBB4rAmAUGdoAbS+2znpfOmPC7e+8HputgbAmamqqCoiKZNXuOvS+eHT548KCcqqq2D9WKrNS0dPP82Xaq7HieP5bH2MoJ07fk209USk1ztxRniXxiVZr9gKE19fq19UCzsbwez0VgKgroeUKzhv49bEi9Xv4j+g2JSK/NJJpNlmWujX0DNRUPbhLvMwF6jIWjJ3j9iuSllkfdLS3puURKCstEv361X5u697CCAAIIDC8wltDcf8s6+saxI0elrrbG3KwR2lQfmwDc2NggDz/8S9MWOr6mZfsr90utqYHW/YpG+6SwqEBmzpxlKwj6v57X68659b4XDsru422SkSJy29oUKS0usOGdabq9Fmd7U1lAs4ZmDm06WtF4sexL2WEP5+WWx2VxwUr7jZNX55ap7OTlvhOgx6hpa0i0+UboaXdLF0evZexnV4MVBBCIV0DPJyfrOuRITZvMKcqURbNybXCN9/n9H9du2j/vP7BfdBIUrXl2egBWnaqS++/7Wf+HDrEeMB33TD2WNt0w/ZC0CYiZs0RmmBE8yspmDPEcb25WB61Bf2XnaXn2d2ckaF78lpVJMrc0z4Znbd7Ct3veWLMVfwhoONYmX/rB8pKGa2WfxAL0GyabfKHnr23/CgK0t2VNgB6jp57kT3UeMT1ed9stJUmSXJb+cftVCif4MeLydASmkYCGRh01Y1tljTz42gm5ef1MmV+aOeqg2NbWJvsqK23oNaM/m/wckMKCIrnsisskPy/fzuA3HK8G5irTefC999+TpoYmG6D1Al2+sFzmzJk73FPHfJ+eV/efbJQfP3/UbuuaiqAsmxObaTAnJ4dOg2MWZgN+E9Bw7DTj2Jh2gzwUvds044jIwchOOdVxRCoyVvKNuMeFToAeA6he8LT5xust52qfF/VcJMU03xiDKk9FYHoK6PlEO/zotNu6dHd32x/tYT+amiMdgUNHz4jVPJtfZhDoZSuXy//7d39nRvnQGuQLD2/15ONPyOEjR6WxoTE2AUtmlixatFhmJLAGWsNzfXOHmWmwUnoiUVk1MyiXLqLToH1T8D8EhhFwmnGUZs2ShU1r5WDK7+yjX299Wsrzl9KMYxi70dzl9C0ZzXOn/XP0gqeTp2zrfN61WCsfpfmGq8EKAgjEK+CcTzRE66K/9fyit4900Zrs5uZmadCpu02C1i2kpiTLnFmz7RB5SWY6bw3mw/3oa588ddKE5zob4HU/ysrKZOGihfYcN9J9iufx+hpd3WEz4kalNHX0yqy8gGxelW6bbWinQWemwXi2xWMQmG4CTjMOnVToIrnKPfzfdjw36nOJuxFWPiRAgP4QSfw3aE1JS1ej7I1sd5+0IfU6Rt9wNVhBAIGRCOg5RX900RCs66MJ0Fp7XVdXJ91am22aYmj1cZrpoT/bNL3Q0BzPopOnHD50WDo6YjXi+rzFS5aYGuhF8Tx9xI/R49Rv9H787AE5WN0h2WkiW9akSJEZb1qH9aPT4IhJecI0E3CacegHTc0izqIZRbOKc25xbuf32AQI0KP005O9XuB+2/prM1hMj93KzN6FMidzIaNvjNKUpyGAgDcCem5qb2uXcI+O/6wJWiTLdC6av2Be3C9QZYauO3nihERMqI2a/3JN2+M1q1bK3HnxbyPeF3POp09vPy6v7KmXZDPd+K2rkmVWCdN0x2vI4xBQAacZh2aRmb3lFkUzyrbWF2xmGc0HcmQHFyBAD+5ywVud2pK3Op51H7u67wqab7garCCAwEQJaE1TWEfPMEFal4AZ4ionK0dmmiYY8Sx6fjtx/JicOnXKPDnWhKS8fKGsXL0m7hkM43kdfYy+lu7vzsN18sCrJ+3Trl8akIrZsWm6nU6DzgeBeLfL4xCYjgJOMw79xmaVySTO8pZpxsGshI6GN78J0KN01JN+d7hL3u15xd3C+hSab7gYrCCAwKgEYvXFdsjmUT3ffdLZpiD670AgKFk52VJcUurePdxK2DQBOWKab9TXN+iTbbO0iy+5WFasXDHc00Z1n4bnqvo2uefJA3ZkkPVzg7K+PDbihk7TrdObM57+qGh50jQU6N+MY33yta6AZhXNLJpdWLwRIECP0lG/It3Z+ltpizbbLeT2FcryjItpvjFKT56GAAIeC5jrpDPOhoZyrZHStsTxLC0tLbL/0EEJd8faP8+dO0/Wb9gopaXxBfB4XkMfo+G5I9RtOw22dUVkfmFArlueQafBeAF5HAKDCDjNOFZkrpecvtjffGu00WYWzS4s3ggQoEfhqJ/g9E24veNF99nLejeaC1QWtSWuCCsIIDBRArYWyoy6kXS2/bM24cjKypS8/LwL7pKe3xoaGuTI4cO2RjjdzG526aaNsm7dRXF3QLzgi5gH6OvoV8r3PrVPjteFJD/DtHtenSpFhQU26DsjbtB0Ix5NHoPAOQEN0PrNjWaS5b2b3Ds0s9CMw+UY8woBehSEtn2hGWLqd12vus9eHbycyVNcDVYQQGC0Amczrw6cMepFJ1TI0glHzEW0/1e28TSF0BE89h84IDpjoTbfKF+4UD5y1dWedh50wvPDrx+R7QebJTU5ILetSZYZxXQaHHWh80QEzgr0b8ah2cRZNLPo8JSaYVjGLkCAHoWhnvybuurkcF9s9kGd4Wtd+kdpvjEKS56CAALeC6SaGQNLiksk04wHO9IkXm+Gv3v1lVeks7NTcnNz5JprrpGNGzd41g5Zz596Ad+xr1YeecuEdLNsXhaUBWWxToNM0+39+4EtTj8BpxmHZhPNKLpoZmnqqj/vQ/X0k/HuiAnQI7TUk78239jR/rJpXxj7FDcnskSKM8povjFCSx6OAAKJEUg2AXrGjFKZNXOWHYJOQ7SG1r7I8DVPWjt18NAhefM3b0gwKWiabayT6z/2MTPzYHyjd8RzNLofR6ub5d5nDtsJXq4oD8qa+ec6Dep04fHUlMfzWjwGgekq4ARozSZzeissg2aWt0120QyjWYZlbAIE6BH66ZtO2xDt6HjJfeaKvk0033A1WEEAgbEInLuuje0CpzP3rVq9yuxKQPrMRrVGubGxcdhdq62tlV8//7ycrqqSxRUVcvunPmVD9LBPGsGdGp5b2kOm0+A+6QxHpKIkIFctZZruERDyUATiEnCacWSYCZSWR8+1g36782XaQccleOEHEaAvbHTeIzRA6xS7O3t+496+JukKO8yTztRFhxeXhRUEEJhAgYLCQrnsiiuksLjYTIbSIw319XL4yOEh90gD9o7tO+S5Z7ZKtul8dOddd8r1N9xgz21DPmkEd9hzp6nhvueJSqlp7pZi07rkltXptsNg/2m6OYeOAJWHIjCEgP4daSbRzriaUZzl/fBvbIahBtoRGf1vAvQI7bQG5VjnfqmPVttnpvVlyMrMjTTfGKEjD0cAgcQKaM3T2rVr5crLL7cf7Ktram3TDA3KA5euri55e8cOuf+++yTUFZKP3fhxufXWW6WoqGjgQ0f1b+ebu5+9cFB2HW+TjBQxnQZNO+2iWKdBHWJPOz4SnkfFy5MQGFRAm3HoaBwrMzZKWjTDPqYuetpmGM0yLGMTIECPwE8vAtp26N3219xnLYyYmbkyTG93026Pk7/LwgoCCIxCQM8hM/JSZEVZkswuSB3zOWXOnDnyud/7vKxYsVLaW1vlueeekxdfeFGqTRONNvPv1uZmOWGm6375xZfkR//yL7bpxh2f/rT8xX/7C5kzd+4ojuDDT9Hzpl6sX915Wrb+7owEzTHesjJZ5s7Is+M902nww2bcgoAXAno+0WySk5krC3vXuJvUDEM7aJdj1CvJo37mNHyiXgi0k83O0Jvu0S8LrLdfkVB74pKwggACoxDQi51+5bq2PF8WFifZiU/Gel5JM2M4r9+wQf7sa1+Vn/zoR3LYjO189ze+IR+5+ipZsmSJbQt58OBB2b1zl2SbWf+++pWvyM2fvMUG21EcwqBP0fC8/2Sj/Oj5o/b+ayqCsmxOrNOgM003nQYHpeNGBMYkoOcUPYdoMw7NKpWy3W5PM8xdPV+2tdP6GJbRCRCgR+CmFwIN0Ht7Y29CfeqqlEvt8HW0fx4BJA9FAIEPCeiFTL9uzcvLsxc8XdcAPNYLnNbwbt68WWbPmi1bn37ajPG8X9773Xuye9cuyckyQbakRLZs2SLXXHetVJhQ7eWi58z65g7babAnEpVVM4Ny6SI6DXppzLYQGEpAzx2aTfQ8sjJlkzwevdc+VDOMMx40H16H0rvw7QToCxu5j9CLwfHOA9IkZ+xt2v55SfZamm+4QqwggMBoBfRip1+3auDV9sv6b6090kW//RpLkNYaqI1mNsFL1l8izabZRqOZabDXNEcrNh0Mndcb7X4P9Tzd567usHzr8UppbO+RWXkB2bwqnWm6hwLjdgQSIOCcV5ZmXCRp7RnSHQzZDHMidFBWZK9LwCtOn00SoOMsa6cd3+86XnefsbBvjWSmZxGgXRFWEEBgLAJ6sdOa2u8+uV/SU5MkQ3/SzFewKWYqbtPzrjQvXWYWZsiMggxJMh2EdHGCtfN7uNfX2ijtGOhV58ChXkvPlzrc50+eOyAHqjokO01ki+k0WFSQb0fd0E6DfGs3lB63I+CdgBOgNassbF0tlcEdduPaDnpZ0doxfzj3bk+n3pYI0HGWmV4Q9CuP90NvuM9YKpfQfMPVYAUBBMYqoN9yNbV2yrb9DcNuSjvileSlSVlBuswryTJtivNk2dw8Kcg51+QjnkA97IuM8k49V2oHpWe2m86Ju+slORiQW1clyawSpukeJSlPQ2DUAnoecJpxaGaplFiA3tX1ltzZ819oBz1qWRECdJx4Tvvnyt533GesTN5k33zUpLgkrCCAwCgFnOBZ29Aqly5Ml2BfWNJSAtLbZ2qlzU93r0hzKCrNnfoTkdrmLvuz82izPLXjtH1VDdVLZuXIinn5sn5JkQnZsaYg4xWm9Rj0XLnrcJ088OoJu0/XLw1Ixexc23TD6TQ4XvszyqLgaQj4RsAJ0HY4O5NZnpAf2GPb27ODdtBjLGUCdJyAemGo7TolDRIb/9l075ElGWv59BanHw9DAIHhBZwA/cGJFtl2pEvWzhLZND/JTGsdcJtp6KyC9nHRoLR1iTSaMF3bLlLdGpWq5j6pa+m2P29W1suPnz8k80szZUNFkWxYUiyLZ+XaKbLjCa/6GgdOt0p5WbakmCm943mOHp2G5+qGNvn2rw6Y4B+V9XODsr48NuKGdo7UizidloZ/H3AvAl4L6N+v/u0tMe2gU0NpEpZum2U00+TkLPf65abN9gjQcRS1c2Hb3bHNffSc3iWSlW4uLoz/7JqwggACYxPQpg/6o4ueW0pLi+2IHPpvDafarlh/tDlZdmavlOT2ymLzeL0vKkFp6DBhuk3kRFNUjtRF5PiZTvvzyJsnpSgnVa5dO0M+fslsKcxJt6F4sGCs23pzb43p/LdPPn5xmfzxjUviaq+sz+sIdcvdj1ZKWygi8wsDct3yDDoNauGxIDCBAvp3rucTzSxz2pbIkeTddm8005QXLmESo1GWDQE6DjgN0HrR2tv1tvvohbKa9s+uBisIIDBWAeeDugZRXbRpWEFBgakhyrG1tnq7E7Cd306Y7u7uFv3JzAhLWV6PrC4zQXtpkpxqFjnaKHKork8a2sLy8Bsn5bG3TsnGJYWyef1sWTm/4LxaaedcZ/oo2uXVXWfkjstN4M7LtvsTu/XD/3eed+9T++R4XUjyM0y759WpUlRYYI9BRwHRmufBAvuHt8YtCCDgpYD+3TntoDW7HJFYgNZMc1Pv5/gmfZTYBOg44JyLw76ed91HVwTNVyFnv47kouCysIIAAmMQ0HONGbTObkFX9aKn5xkdzi52X2xIO13XHw3V+qM10s5POBwWnZpbf7IyusykLD1yzeKACdNJsrPaNM2ojchv9zXYn/klmXLXR+fLpmWlNuDqNjWc56VFpLw4RY7W98iL752WT31k8ZABWJ+jQf7h3xyV7QebJTU5YKbpTpYZxXQaHMNbgaci4JmAZhT9AKvnEs0uL8qDdtv7e35n/3b1b5hl5AIE6DjM9M0VCnfK0b697qNXpK23X4nQns8lYQUBBDwQ6H8x0wtf/5/+m+//OJ0oQYO03qYBWMN0/yDd2dlpvjELydyCsLSWi+ypEdlVZca1r+uUfzRNLhaVnZDPX10uaxYWuq938dwkG6Bf2d0gn9g4d9CvefX19HV37KuVR96MdWTcvCwoC8py7VB5TNPdv8RYR2DiBDSraDOOFenrxTSCtotmGs022dHsiduxKfzKBOgLFJ5zgdjX+Z6YS5J9dGGkTEpzZ9s3I7XPFwDkbgQQSIhA/3OPrusFUs9XWlutNU06GYsGWK0d1uYdoVBIOjo6JMv85GeFZMM80yytJijbjvXJ4ZoO+buH9siq+bnyOVMjXZyVKqvmZsmvP+iWM6098t6hernMTIIysMJAw/Ox6ma595nDtt78ivKgrJkf6zSYa6YG1wv2wOckBIONIoDAsAJ6jrD9KtJnS0FohjQl1drOhPs735ei/OvtuaP/OWXYjXGnFYiNxA/GsAJao1MZOjd8XXl0lW3/rBcq3nDD0nEnAgiMo4Cej/RHQ2v/IK1hVmcdLCsrk1mzZtnfOqnJ+vmp8sVNQfnIoiRJN00v9hxvlf/xwG555Lc1kpySKmtnxy4RL+ystbXaGpidRddb2kNy92P7pDMckYqSgFy1NNN2GtQRN7RWnPDsaPEbgYkV0POCnhP073KhyTDO8kHobbfjsnMbv+MToAb6Ak5ao6Nfhx7o3uk+sjywgvbPrgYrCCAwWQX6B2ptT60XUO3QpzXTGqrb29ultbVVLk/rkNUzw/L2yYC8e6JXfr2zXt4+lCQfKU+SpEBE9p7slKr6NlkwKzYMnZ4Xw+a8eM8TlVLT3C0l2QH5xKo022FQOz7SaXCyviPYr+kq4Hyw1m+nygMr5V15yVJottFvqfRvWh/DEr8ANdAXsNJaFn1zHY2ca/+8KGU1X01ewI27EUBgcgnoxVFDtH6Nq807NECnZeXJ4eZUaY5kS1p6pnx0YUA+sy4opTkBaeqIyK/2hMW05hAzpLO88F61PRc658SfvXhIdh1vEzPDuGxZnSylxQW29lmn6ebbuclV9uwNAiqg3wjp379mGGfRbKOVhP2/XXLu4/fwAtRAD+9jP5V1htvldPSw+8iKtDX2TcinNZeEFQQQ8EjAqQQyFUIJWZzzlv5+5p0aefrt2ORQ+mLajKMwS6QoPSIZSSInW4LS2h3bjd9UNsunPxKSXHMRfm1XlWx9t1Z0SvFPrAzK3Bl5NjxnZWURnhNSamwUgbEL6N+8BmjNMBKKbU+zjWacnGjO2F9gmm2BAD1MgetXGvqp7EBol5jpDewjiyOzzRBPhVwkhnHjLgQQmPwC+s3axoWZcqYhU6qbw1LfFpFQj5nRsEWkykzKMnDpDPfJK+9XyZK5BfKj54/au69eHJTlc3LsiBs6XrV+PUy754Fy/BuBySGgAVq/HdIMU9IxW+qCp222ORjaLSV9M2jGMcJiIkBfAEw7EB4Ive8+al50qb1I8BWlS8IKAghMMQGtHNCfwqwkuXl1hhmdQ5uqibR3menA2yNm5I1eM6thnzR3BaW5O0m6es0IH6Yy4ZVdtbL1vQbpiURl1cygXLY404ZnOg1OsTcAuzstBZwArR905/QtsQFaIQ50vS+bItdMS5OxHDQBehg9vcBogD4Y3uU+an5guf0KRGtZ9M3IggACCHgpYMbRsJszEdfLzZ63LeerXO1MqOc57fSn40Znm7aQhblhWVgSG0c6NjlLSLpMzXR7JEVeP5YqTaFeKTPf9m5emc403eep8g8EJreA/t077aAXmCzznrxid1gzjmYdPReQa+IvQwL0MFbafEMvIEcjH7iPWpS8ivbPrgYrCCAwFQX0IqnfommbZR3WSs9z2qRDf/RCGgvO50/GsnVPyIRnkcyUqJmmO8VM051vR93QToPaOZEL71R8J7DP001A/061HfRCk2Wcz+iacfRvXgM0S/wCBOhhrPTN1NMTlqroEfdR5akrCNCuBisIIDBVBZwQ7UwXruc750crD5wKBJ2E5YX3qmR3dcgMaSdy83KR2aVM0z1Vy539nt4CboBOXSlytoPwqb5DNuvo37yeD1jiEyBAD+HkXEhOdx8177FYd9XsaJ6UpM+kA+EQZtyMAAJjE9CLW3FOsqwoS5J5RakJr9XV19Of/oue+3TRi6nWUh843SaPv91kb7uuQmT5vAI7KYt2GtSarIHPtw/kfwggMCkF9O9V/641y2R35Ul7oMXMSNglmnUKooU04xhBqRGgh8HSC8iRrnPNN8r6ym0HQr6uHAaNuxBAYFQCemHTc8vqBfkyvzBox2qeiM7K/QPxmeZO+a6ZprvXDAS9fm5QNi7OlpKSEtFOg4y4Mapi5kkITKiAc57Rv1/NNIeSYoMkaNZZ0bduQvdtqr34h8cqmmpHkKD91VoYbQt4NFzpvsJsWWRrXBimySVhBQEEPBLQC5te1DScakjVGf20fXL/QOvRS11wM1p50BHqlrsfrZS2UMQE+oBctzyDToMXlOMBCEx+Aacj4SxZ6O6sZh2nI6F7IyvDClADPQSPBmjtUHOsZ5/7iLnBCrf5xkRc1NwdYQUBBHwnoOcUp2OfzhTo/Hu8zzXOue/ep/bJ8bqQ5GcETKfBVNNpsMCGeqbp9t1bjwOaRgJ6PnHOLXMDpk3W2eV4z36beZwmXM7t/B5agAA9hI1zETkZOeA+Yn7yUjdAuzeyggACCHgkMHB4zIkKz4/85qhsP9gsqWZmwtvWJMuMYjoNelTEbAaBCRdwAvT8lKVydo44OWGyjlYaEqDjLx6acAxhpW+int6w1Mhx9xELUpcToF0NVhBAIBECTg3RRIRnbbqxY1+tPPzmaXtom5cFZUFZrp0sRceMnog22YkwZpsITGcBJ0CXm0zjLDVyzGYeArQjcuHfBOhBjPQNpD/V3SfEjIRqH5Hdly/5aUWMdzqIFzchgIA3AnreqWrokFd3Vcu+E03jWhuk4flYdbPcazoN6jgcV5QHZc38bBuec3Nz6f/hTRGzFQQmXEADtHZYzk8rFs02umjW0czj5J8J38kpsAME6CEKSS8mJ7oPuveWROfYCwgjcLgkrCCAgIcCeuHSTjy/O1gn9zxRKc+/e2rcvlLV811Le0jufqxSOsMRqSgJylVLM22nQabp9rCQ2RQCk0DACdA6DKVmG2fRzKPnApb4BAjQgzg5F7JT4cPuvaXRufbrS0bgcElYQQABDwX0vKPTaYdCsXHndQIT/dHbE7nY1zWzkGlor2kOS0l2QD6xKtV2GNSRQOg0mEh9to3AxAholtEmWZptnEUzDyNxOBoX/k2AHsTICdCne8/NQDgzaYHb/m+82yYOsovchAACPhPQ84524tEQrYtOravriQzQzmve/+Ih2XW8TTJSRLasNpMsFMU6Deo03bR79tkbjcOZ9gKaYfRH/7Y12ziLZh4CtKNx4d8E6CGM9EJW1XvUvXdmcAHtn10NVhBAIBEC+vVpX1/EbjoSiU2nnagArdvV13t152l55t1aCZoL6idWJsncGXm23XNWVhbhORGFzDYRmAQCTjMOzTbOoplHsw9LfAIE6EGc9MKin8Jq+s6NwDE7ZSEXk0GsuAkBBLwVSFRgHriXGp4PnGyUHz0fqyi4anFAls/JseFZp+lmpsGBYvwbAf8IODXQmm2cpTZ6ghpoByOO3wToQZD0AqZD2NUHYkM56UPmpi6iBnoQK25CAAHvBPSiNh6LhueGlg7TaXCf9ESisrIsKJcvzrLhmU6D41ECvAYCEyvg1EDP6Reg6+S09EZ6EtpsbGKP2ttXJ0AP8NTwrD91PVVmfPHYV6nZ0XzJTs0jQA+w4p8IIOC9gBOhE9V1UM9v3aZt9TfNiBuN7T0yMzcgN61KZ5pu74uSLSIwaQWcAK3ZRjOOLhHplTPh0zYD6XmCZXgBAvQgPlo7Ux0+4d5T2DfDNt/QIexYEEAAgakqoBdFbeP4o60H5EBVh2Slmk6Da1LMNN35dtQN7TTIUJ1TtXTZbwRGJqB/6zqUnWYcZ9HsoxmI5cICBOgBRnqB0TdPTf8ALWVcVAY48U8EEJhaAnpu074dW3ccl1f21EuSOfvfakbcmF3KNN1TqyTZWwS8EXBqoQtNxnEWzT6agaiBdkSG/k2AHsRGLzK1vSfde4oCM90APV5tFN0XZwUBBKaVgNMM2stvUJ2KgV2H6+T+V2LnthuWBGXJ7FzbdEM7DWpNFOe3afVW42CnsYD+rTsBWjOOs5zpPWU/aDv/5vfQAgToATZOLU1d5FwHwpLgbDdAD3g4/0QAAQQmvYBtltbQJt/+1QHp7YvKJXODsn5htjvTICNuTPoiZAcR8FzACdDFwVnuts9EYgGaGmiXZMgVAvQAGjdA950L0KVJsQA94KH8EwEEEEiogBcXMQ3PnV3dcvejldIWisi8goBct4xOgwktODaOwBQR0HbQM5LOTedd12cGUDDfwntx7pkiBKPeTQL0IHT65qnvq3bvKU2eQw20q8EKAghMFQG9CGqnwe/9ap8crwtJfkZAtqxNM50GC2ynwYyMDNEpfWm6MVVKlP1EwDsB/bvXv/8SU0noLA3RappwOBgX+E2AHgCkFxytsWmWOvee0pTZXGRcDVYQQCCxAt4MZOeE50ffOCrbDzZLalJAbluTLDOK8mzTDWYaTGwpsnUEJruA04RjRuq5Guim6Bk6EcZZcAToflB6wdGf7t6QdAba7D1BCUpBSgk10P2cWEUAgckt4FQE7NhXK798I9Yc7cblQVlQluvONJicnEzN8+QuRvYOgYQKOAFaM04wGouDmn00Azl5KKE7MMU3ToAeUID6pmmMnHFv1QHGk5NSbA20eyMrCCCAQAIE9IJWnJMsK8qSZH5x2qgDrn6Ldqy6We595rDodAiXlwdl7fxsG55zc3PtiBv61S0LAghMbwE9D2jGyZbYZCqqoRlIsxDL8ALJw989/e7VN019T4174Dl9hdQ+uxqsIIBAogSc2qAV8/Jldl5AtH3yaIaW0/Dc0h4y03RXSmc4IhUlQblqSYY74kZaWhoVAokqRLaLwBQTcM47mnVakxrt3msGIkBfuCAJ0AOM9OLT2Fvr3ponRbR/djVYQQCBRAnohUwDs9YQ67Byuq5hV2+Pd9GLnnYa/M6TlVLTHJaS7IDcvMrMNFZYaDsNpqencz6LF5PHITANBPT8orXQmnVOyyF7xJqBNAuxDC9AgO7noxefgQHatBh0LzgjuZD12yyrCCCAwAUF9Pyi7ZK1c5/WPjv/jve844Tnn714UHYea5OMFDNNt5lpsLQoNuKGTtNNu+cLFgMPQGDaCOi5RX80QGvWcRYnQOs5Jd7zj/Pc6fSbAN2vtPXNokPY9W8DXRCMdSDs9zBWEUAAgYQI6IWs/wWr//pwL+h8+H9t52l5+p1aCZqL4idWBmXuDFOvVFQk2dnZhOfhALkPgWksoGNBa9ZxlqZIHWNBOxjD/CZAD8Cx7Qf7Gtxb84LFbg20eyMrCCCAQIIE4g3N/V9ez1sHTjbKD58/am++alFAls/JcUfcYKbB/lqsI4CAI6DnG9uEw2Qd2+PY3NHcV08TDgdomN8E6H44Ti1OW1+ze2tOMJ8A7WqwggACiRTQc1BtU0j2nWqRkrw0WTGv4Lwa6cFeW8NzQ0uH6TS4T3oiUVlZFpTLK7JseM7Ly7PtqPUCyYIAAggMJqDnB806Eond297XwljQg0ENuI2z6gAQvRh1RFvcW50A7d7ACgIIIJAAAacJ2a4j9XLPE5WydcdJ2yFQbx9q0fu6w2H5lhlxo7G9R2bmBuSmVUzTPZQXtyOAwPkCTg20DdBn72qPNlMDfT7ToP8iQPdj0YuR/nRIq3trTlKBrYF2b2AFAQQQSICAnnt6enokFArZrYdNMO7q6hpyOCl9vI648eNnD8j+qg7JSjWdBtekmGm68+2IG9ppUNs2jqZJSAIOj00igMAkFbA10CbrOEtHtNWed/QcwzK0AAF6gE2sBvpcgM41byq9AHERGgDFPxFAwFMBJxB3d3fb7WqY1p/BLmJ6m3Z43rrjhLy8u16SzJn8VjPixuzSfKbp9rRU2BgC/hZw8o1mHWfRSkTNQizDCxCgB/johakzcH6Apv3gACT+iQACCRHQi1ZfX6whYiTSN2hPeD1H6eN2HamT+185YffjhqVJUjErx4bnnJycUU3AkpADYqMIIDDpBTTjDAzQg31wn/QHMs47SIA+C65vFufC1CltbjFouyDnE5p7IysIIIDABAloeK5uaJNvP3lAevuicvGcoKwvP9dpkBE3JqhgeFkEpqCAk2/6t4EOBdrsh3QnF03BwxqXXSZAD2DujLSZjqixGqBUSZO05AzaQA8w4p8IIDAxAhqeO7u65e5HK6UtFJF5BQG5fjmdBiemNHhVBPwhoDXQmnU08+iiGSgUaffHwSXwKAjQ/XD101aor8O9JTUaC8+0f3ZJWEEAgQkS0POTdhr83q/2yfG6kORnBGTL2jTTaTA206DOXqgXQs5XE1RAvCwCU1RAzxl67tDM4yydfe2D9r9w7ue3CAG637vgwwE6nQtSPx9WEUBgYgSc8PzoG0dl+8FmSU0KyG1rkmVGUR6dBiemSHhVBHwjcC5Ap7vHpJWJet5hGVqAAN3PxgboyLka6DTJoDannw+rCCAw/gJ6XtKmGzv21crDb562O3Dj8qAsKMt1ZxpMTk7mXDX+RcMrIuAbAQ3RmnmcJWSyEAHa0Rj8NzMR9nOxAbp/Ew5J56LUz4dVBBAYfwENzydqW+XeZw6L6TMoly8Iytr52TY85+bm2hE39OtXFgQQQGC0AhqgU03mcRZqoB2JoX9z1j1ro+F5YIB2aqD1jaU/LAgggEDiBWJfm+q3pxqeW9pDZpruSukMR6SiJChXLc2wzTaYpjvxJcErIOB3ASff6O/zaqDPNuGgFnrodwA10P1shgrQ/R7CKgIIIJAwgf4f1PV8pJ0Gf/jccalpDktxVkBuXpViw3NBQYGkp9NHI2EFwYYRmGYCQwXoacYwosOlBrofl16wuvo63VucGmj3BlYQQACBcRDQc1GfmWnwsW3Vsut4u6Sbqo7b1qZIaVFsxA2dppt2z+NQELwEAtNEYGCA1iyk5yGWoQWogT5ro28U/emJxqbR1ZtTAmk03Rj6vcM9CCCQCAG9ZkX7pLkzIpV7WyVoWo/dsipZ5pbGOg1mZ2cTnhPhzjYRmMYCGqA18ziLZiEnFzm38ft8AWqg+3nomyUive4tyYHY54v+X6u6d7KCAAIIJEAgato990V6pKY9yW79ynKR5XNinQZ1mm5mGkwAOptEYBoLOBnHyTxK0RvtpQb6Au8JAnQ/IFsD3ddj3jSmAsj8BKNJ1ED382EVAQQSL5CTHpD01BSJSkAqCnvkskUZdsQNOg0m3p5XQGC6CmiI1szj5J/eqGYhmnAM934gQPfT0R7vh3p3m/RsbjQ/9X1Vthd8v4ewigACCCREQC9g+lNemi43VvTInJyw3LA02XYazM/PdzsNJuTF2SgCCExrAc0/mnmc/KNZSG9jGVqANtBnbfSTlvZ4b+itkbMtN6Q93CoR05GHT2FDv4G4BwEEvBHQ8JySkiLaTGPlgkKpmBW2Nc9FRUWinQaTkvhGzBtptoIAAv0FNONo1mmPtEogNXaPZiHNROSf/lLnrxOgz3roJ62uri4Jd4VFsmM3Rrr7JBwO8yns/PcM/0IAgQQIOAFah6jTETb0gpaVlWV/GHEjAeBsEgEErIDmH806kW7z1fvZAK1ZSDMRtdBDv0kI0GdtnE9gvT3nOhGaNvTUQA/93uEeBBDwWEBrmbW2OS0tzV64NDjrLIMarlkQQACBRAg4+Sfae67Ns2YhvoEfXps20AN8ZgTmubeUBcrthYuLl0vCCgIIJFhAQ7Q25dAQretM051gcDaPwDQX0IyjP7MDi12JWcGFfHB3NQZfoQb6rIu+ebS2Jze1wJXKS4l9lUqAdklYQQCBcRDgnDMOyLwEAghYASf/5KTmuSJZKTn0u3A1Bl+hBvqsi76BdHxVrfVxFp0qV//NxcwR4TcCCCCAAAII+ElAM45mnf75R9c1E5F/hi5pAvRZG32TaA20/jiL82/eQI4IvxFAAAEEEEDATwLkn9GVJgG6n5u+ifqH5YH/7vdQVhFAAAEEEEAAAV8IDMw7A//ti4P0+CAI0B6DsjkEEEAAAQQQQAABfwsQoP1dvhwdAggggAACCCCAgMcCBGiPQdkcAggggAACCCCAgL8FCND+Ll+ODgEEEEAAAQQQQMBjAQK0x6BsDgEEEEAAAQQQQMDfAgRof5cvR4cAAggggAACCCDgsQAB2mNQNocAAggggAACCCDgbwECtL/Ll6NDAAEEEEAAAQQQ8FiAAO0xKJtDAAEEEEAAAQQQ8LcAAdrf5cvRIYAAAggggAACCHgsQID2GJTNIYAAAggggAACCPhbgADt7/Ll6BBAAAEEEEAAAQQ8FiBAewzK5hBAAAEEEEAAAQT8LUCA9nf5cnQIIIAAAggggAACHgsQoD0GZXMIIIAAAggggAAC/hYgQPu7fDk6BBBAAAEEEEAAAY8FCNAeg7I5BBBAAAEEEEAAAX8LEKD9Xb4cHQIIIIAAAggggIDHAgRoj0HZHAIIIIAAAggggIC/BQjQ/i5fjg4BBBBAAAEEEEDAYwECtMegbA4BBBBAAAEEEEDA3wIEaH+XL0eHAAIIIIAAAggg4LEAAdpjUDaHAAIIIIAAAggg4G8BArS/y5ejQwABBBBAAAEEEPBYgADtMSibQwABBBBAAAEEEPC3AAHa3+XL0SGAAAIIIIAAAgh4LECA9hiUzSGAAAIIIIAAAgj4W4AA7e/y5egQQAABBBBAAAEEPBYgQHsMyuYQQAABBBBAAAEE/C1AgPZ3+XJ0CCCAAAIIIIAAAh4LEKA9BmVzCCCAAAIIIIAAAv4WIED7u3w5OgQQQAABBBBAAAGPBQjQHoOyOQQQQAABBBBAAAF/CxCg/V2+HB0CCCCAAAIIIICAxwIEaI9B2RwCCCCAAAIIIICAvwUI0P4uX44OAQQQQAABBBBAwGMBArTHoGwOAQQQQAABBBBAwN8CBGh/ly9HhwACCCCAAAIIIOCxAAHaY1A2hwACCCCAAAIIIOBvAQK0v8uXo0MAAQQQQAABBBDwWIAA7TEom0MAAQQQQAABBBDwtwAB2t/ly9EhgAACCCCAAAIIeCxAgPYYlM0hgAACCCCAAAII+FuAAO3v8uXoEEAAAQQQQAABBDwWIEB7DMrmEEAAAQQQQAABBPwtQID2d/lydAgggAACCCCAAAIeCxCgPQZlcwgggAACCCCAAAL+FiBA+7t8OToEEEAAAQQQQAABjwUI0B6DsjkEEEAAAQQQQAABfwsQoP1dvhwdAggggAACCCCAgMcCBGiPQdkcAggggAACCCCAgL8FCND+Ll+ODgEEEEAAAQQQQMBjAQK0x6BsDgEEEEAAAQQQQMDfAgRof5cvR4cAAggggAACCCDgsQAB2mNQNocAAggggAACCCDgbwECtL/Ll6NDAAEEEEAAAQQQ8FiAAO0xKJtDAAEEEEAAAQQQ8LcAAdrf5cvRIYAAAggggAACCHgsQID2GJTNIYAAAggggAACCPhbgADt7/Ll6BBAAAEEEEAAAQQ8FiBAewzK5hBAAAEEEEAAAQT8LUCA9nf5cnQIIIAAAggggAACHgsQoD0GZXMIIIAAAggggAAC/hYgQPu7fDk6BBBAAAEEEEAAAY8FCNAeg7I5BBBAAAEEEEAAAX8LEKD9Xb4cHQIIIIAAAggggIDHAgRoj0HZHAIIIIAAAggggIC/BQjQ/i5fjg4BBBBAAAEEEEDAYwECtMegbA4BBBBAAAEEEEDA3wIEaH+XL0eHAAIIIIAAAggg4LEAAdpjUDaHAAIIIIAAAggg4G8BArS/y5ejQwABBBBAAAEEEPBYgADtMSibQwABBBBAAAEEEPC3AAHa3+XL0SGAAAIIIIAAAgh4LECA9hiUzSGAAAIIIIAAAgj4W4AA7e/y5egQQAABBBBAAAEEPBYgQHsMyuYQQAABBBBAAAEE/C1AgPZ3+XJ0CCCAAAIIIIAAAh4LEKA9BmVzCCCAAAIIIIAAAv4WIED7u3w5OgQQQAABBBBAAAGPBQjQHoOyOQQQQAABBBBAAAF/CxCg/V2+HB0CCCCAAAIIIICAxwIEaI9B2RwCCCCAAAIIIICAvwUI0P4uX44OAQQQQAABBBBAwGMBArTHoGwOAQQQQAABBBBAwN8CBGh/ly9HhwACCCCAAAIIIOCxAAHaY1A2hwACCCCAAAIIIOBvAQK0v8uXo0MAAQQQQAABBBDwWIAA7TEom0MAAQQQQAABBBDwtwAB2t/ly9EhgAACCCCAAAIIeCxAgPYYlM0hgAACCCCAAAII+FuAAO3v8uXoEEAAAQQQQAABBDwWIEB7DMrmEEAAAQQQQAABBPwtQID2d/lydAgggAACCCCAAAIeCxCgPQZlcwgggAACCCCAAAL+FiBA+7t8OToEEEAAAQQQQAABjwUI0B6DsjkEEEAAAQQQQAABfwsQoP1dvhwdAggggAACCCCAgMcCBGiPQdkcAggggAACCCCAgL8FCND+Ll+ODgEEEEAAAQQQQMBjAQK0x6BsDgEEEEAAAQQQQMDfAgRof5cvR4cAAggggAACCCDgsQAB2mNQNocAAggggAACCCDgbwECtL/Ll6NDAAEEEEAAAQQQ8FiAAO0xKJtDAAEEEEAAAQQQ8LcAAdrf5cvRIYAAAggggAACCHgsQID2GJTNIYAAAggggAACCPhbgADt7/Ll6BBAAAEEEEAAAQQ8FiBAewzK5hBAAAEEEEAAAQT8LUCA9nf5cnQIIIAAAggggAACHgsQoD0GZXMIIIAAAggggAAC/hYgQPu7fDk6BBBAAAEEEEAAAY8FCNAeg7I5BBBAAAEEEEAAAX8LEKD9Xb4cHQIIIIAAAggggIDHAgRoj0HZHAIIIIAAAggggIC/BQjQ/i5fjg4BBBBAAAEEEEDAYwECtMegbA4BBBBAAAEEEEDA3wIEaH+XL0eHAAIIIIAAAggg4LEAAdpjUDaHAAIIIIAAAggg4G8BArS/y5ejQwABBBBAAAEEEPBYgADtMSibQwABBBBAAAEEEPC3AAHa3+XL0SGAAAIIIIAAAgh4LECA9hiUzSGAAAIIIIAAAgj4W4AA7e/y5egQQAABBBBAAAEEPBYgQHsMyuYQQAABBBBAAAEE/C1AgPZ3+XJ0CCCAAAIIIIAAAh4LEKA9BmVzCCCAAAIIIIAAAv4WIED7u3w5OgQQQAABBBBAAAGPBQjQHoOyOQQQQAABBBBAAAF/CxCg/V2+HB0CCCCAAAIIIICAxwIEaI9B2RwCCCCAAAIIIICAvwUI0P4uX44OAQQQQAABBBBAwGMBArTHoGwOAQQQQAABBBBAwN8CBGh/ly9HhwACCCCAAAIIIOCxAAHaY1A2hwACCCCAAAIIIOBvAQK0v8uXo0MAAQQQQAABBBDwWIAA7TEom0MAAQQQQAABBBDwtwAB2t/ly9EhgAACCCCAAAIIeCxAgPYYlM0hgAACCCCAAAII+FuAAO3v8uXoEEAAAQQQQAABBDwWIEB7DMrmEEAAAQQQQAABBPwtQID2d/lydAgggAACCCCAAAIeCxCgPQZlcwgggAACCCCAAAL+FiBA+7t8OToEEEAAAQQQQAABjwUI0B6DsjkEEEAAAQQQQAABfwsQoP1dvhwdAggggAACCCCAgMcCBGiPQdkcAggggAACCCCAgL8FCND+Ll+ODgEEEEAAAQQQQMBjAQK0x6BsDgEEEEAAAQQQQMDfAgRof5cvR4cAAggggAACCCDgsQAB2mNQNocAAggggAACCCDgbwECtL/Ll6NDAAEEEEAAAQQQ8FiAAO0xKJtDAAEEEEAAAQQQ8LcAAdrf5cvRIYAAAggggAACCHgsQID2GJTNIYAAAggggAACCPhbgADt7/Ll6BBAAAEEEEAAAQQ8FiBAewzK5hBAAAEEEEAAAQT8LUCA9nf5cnQIIIAAAggggAACHgsQoD0GZXMIIIAAAggggAAC/hYgQPu7fDk6BBBAAAEEEEAAAY8FCNAeg7I5BBBAAAEEEEAAAX8LEKD9Xb4cHQIIIIAAAggggIDHAgRoj0HZHAIIIIAAAggggIC/BZL9fXgjO7pAICBLk9bJlZ232ScuzVknehsLAggggAACCCDgVwHyz8hLlgB91kzfPMnJybIx+3pZ1L3O3lqUXWRvI0SP/I3FMxBAAAEEEEBg8guQf0ZXRgTos27BYFDS0tKksLBQ0tPT7a2ZmZn2Nr2PBQEEEEAAAQQQ8JsA+Wd0JRqImmV0T/Xfs/r6+iQSiYj+1kXfVElJSfa3/46WI0IAAQQQQAABBMTmHvLPyN4JBOgBXgM/T9B8YwAQ/0QAAQQQQAAB3wmQf0ZWpATokXnxaAQQQAABBBBAAIFpLkDj3mn+BuDwEUAAAQQQQAABBEYmQIAemRePRgABBBBAAAEEEJjmAgToaf4G4PARQAABBBBAAAEERiZAgB6ZF49GAAEEEEAAAQQQmOYCBOhp/gbg8BFAAAEEEEAAAQRGJkCAHpkXj0YAAQQQQAABBBCY5gIE6Gn+BuDwEUAAAQQQQAABBEYmQIAemRePRgABBBBAAAEEEJjmAgToaf4G4PARQAABBBBAAAEERibw/wPhqRBO9loBfAAAAABJRU5ErkJggg=="
            #     }
            # ]
            file_link = file_obj[file_obj.keys()[0]]
            self.is_cloudfront_url(file_link)
            req = requests.get(file_link)
            self.ok(req)
        # downloadables = ['manip','front','side','top','all']
        # for downloadable in downloadables:
        #     endpoint = item_files_endpoint + downloadable + '/'
        #     dl_sig = calculate_signature(auth, self.headers, 'GET', endpoint)
        #     self.sign_client(dl_sig)
        #     req = self.client.get(endpoint)
        #     self.ok(req)

        # GET of item files should not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', item_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_files_endpoint)
        self.unauthorized(req)

        # GET of an item should not work for Learner
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.unauthorized(req)

        # PUT should work for Instructor.
        self.convert_user_to_bank_instructor(bank_id)
        # Can modify the question files, reflected in GET
        new_manip_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/image001.jpg'
        new_manip = open(new_manip_path, 'rb')

        item_question_endpoint = item_detail_endpoint + 'question/'
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', item_question_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_question_endpoint)
        self.not_allowed(req, 'DELETE')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', item_question_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_question_endpoint)
        self.not_allowed(req, 'POST')

        payload = {
            "manip" : new_manip
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        expected_files_new_manip = deepcopy(expected_files)
        new_manip.seek(0)
        expected_files_new_manip['manip'] = base64.b64encode(new_manip.read())
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files_new_manip)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')  # Item questions do not have links, for now
        self.verify_ortho_files(req, expected_files_new_manip)

        # Now check that updating the view set works
        new_front_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/front2.jpg'
        new_front = open(new_front_path, 'rb')

        payload = {
            "frontView" : new_front
        }

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        new_expected_files = self.extract_question(json.loads(req.content))['files']
        # need to compare to encoded versions here because expected_files are
        # actual files
        self.assertNotEqual(
            expected_files_new_manip['frontView'],
            self.encode(requests.get(new_expected_files['frontView']).content)
        )
        self.assertEqual(
            expected_files_new_manip['sideView'],
            self.encode(requests.get(new_expected_files['sideView']).content)
        )
        self.assertEqual(
            expected_files_new_manip['topView'],
            self.encode(requests.get(new_expected_files['topView']).content)
        )
        self.assertEqual(
            expected_files_new_manip['manip'],
            self.encode(requests.get(new_expected_files['manip']).content)
        )
        # self.verify_ortho_files(req, new_expected_files)  # redundant

        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new_expected_files)

        new_side_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/side2.jpg'
        new_side = open(new_side_path, 'rb')

        payload = {
            "sideView" : new_side
        }

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)

        new2_expected_files = self.extract_question(json.loads(req.content))['files']

        # here also need to convert new_expected_files
        self.assertNotEqual(
            self.encode(requests.get(new_expected_files['sideView']).content),
            self.encode(requests.get(new2_expected_files['sideView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new_expected_files['frontView']).content),
            self.encode(requests.get(new2_expected_files['frontView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new_expected_files['topView']).content),
            self.encode(requests.get(new2_expected_files['topView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new_expected_files['manip']).content),
            self.encode(requests.get(new2_expected_files['manip']).content)
        )
        # self.verify_ortho_files(req, new2_expected_files)# redundant

        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new2_expected_files)

        new_top_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/top2.jpg'
        new_top = open(new_top_path, 'rb')

        payload = {
            "topView" : new_top
        }

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        new3_expected_files = self.extract_question(json.loads(req.content))['files']
        self.assertNotEqual(
            self.encode(requests.get(new2_expected_files['topView']).content),
            self.encode(requests.get(new3_expected_files['topView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new2_expected_files['frontView']).content),
            self.encode(requests.get(new3_expected_files['frontView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new2_expected_files['sideView']).content),
            self.encode(requests.get(new3_expected_files['sideView']).content)
        )
        self.assertEqual(
            self.encode(requests.get(new2_expected_files['manip']).content),
            self.encode(requests.get(new3_expected_files['manip']).content)
        )
        # self.verify_ortho_files(req, new3_expected_files)  # redundant

        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new3_expected_files)

        # ITEM ANSWERS endpoint
        # Verify that GET, POST (answers/) and
        # GET, DELETE, PUT to answers/<id> endpoint work
        # Verify that invalid answer_id returns "Answer not found."
        item_answers_endpoint = item_detail_endpoint + 'answers/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', item_answers_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_answers_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_answers_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_answers_endpoint)
        self.not_allowed(req, 'PUT')

        item_answer_detail_endpoint = item_answers_endpoint + unquote(answer_id) + '/'
        # Check that POST returns error code 405--we don't support this
        post_sig = calculate_signature(auth, self.headers, 'POST', item_answer_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answer_detail_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', item_answer_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])

        put_sig = calculate_signature(auth, self.headers, 'PUT', item_answer_detail_endpoint)
        self.sign_client(put_sig)
        new_response_set = {
            "frontFaceValue" : 4,
            "sideFaceValue"  : 5,
            "topFaceValue"   : 6
        }
        payload = {
            'integerValues': new_response_set
        }
        req = self.client.put(item_answer_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])
        self.verify_ortho_answers(req, new_response_set)

        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])
        self.verify_ortho_answers(req, new_response_set)

        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        del_sig = calculate_signature(auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)

        # Create a new item with an incomplete viewset
        # should thrown an error
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }

        payload_missing_front = deepcopy(payload)
        payload_missing_front['manip'] = self.manip
        payload_missing_front['sideView'] = self.side
        payload_missing_front['topView'] = self.top
        self.manip.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        payload_missing_front['question'] = json.dumps(payload['question'])
        payload_missing_front['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint, payload_missing_front)
        self.missing_orthoview(req)

        payload_missing_top = deepcopy(payload)
        payload_missing_top['manip'] = self.manip
        payload_missing_top['frontView'] = self.front
        payload_missing_top['sideView'] = self.side
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        payload_missing_top['question'] = json.dumps(payload['question'])
        payload_missing_top['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint, payload_missing_top)
        self.missing_orthoview(req)

        payload_missing_side = deepcopy(payload)
        payload_missing_side['manip'] = self.manip
        payload_missing_side['frontView'] = self.front
        payload_missing_side['topView'] = self.top
        self.manip.seek(0)
        self.front.seek(0)
        self.top.seek(0)
        payload_missing_side['question'] = json.dumps(payload['question'])
        payload_missing_side['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint, payload_missing_side)
        self.missing_orthoview(req)

        self.delete_item(auth, bank_endpoint)

    def test_lti_request_signature(self):
        """
        Make sure that lti headers are accepted:
        ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role']
        """
        lti_headers = ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']
        test_endpoint = self.endpoint + 'assessment/'

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = '80'
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)
        self.ok(req)

    def test_authenticated_instructor_assessment_taking(self):
        """
        POSTing to takens should create a new one.
        Should only be able to POST from the /assessmentsoffered/ endpoint
        But can GET from the /assessments/ endpoint, too.

        Without submitting a response, POSTing again should
        return the same one.

         -- Check that for Ortho3D types, a taken has files.

        Submitting and then POSTing should return a new
        taken.

        This should work for both instructors and learners

        NOTE: this tests the obfuscated way of taking assessments (no list of questions)
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        expected_files = self.extract_question(item)['files']

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }


        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can GET. Cannot POST. PUT and DELETE not supported
        assessment_takens_endpoint = assessment_detail_endpoint + 'assessmentstaken/'
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_takens_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_takens_endpoint)
        self.ok(req)

        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_takens_endpoint)
        self.wrong_taken_post_url(req)

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_takens_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_takens_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_takens_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_takens_endpoint)
        self.not_allowed(req, 'PUT')

        # Can GET and POST. PUT and DELETE not supported
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        get_sig = calculate_signature(auth, self.headers, 'GET', assessment_offering_takens_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_takens_endpoint)
        self.ok(req)

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', assessment_offering_takens_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_takens_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_offering_takens_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_takens_endpoint)
        self.not_allowed(req, 'PUT')

        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can GET the taken details (learner cannot).
        # POST, PUT, DELETE not supported
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', taken_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_endpoint)
        self.ok(req)

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', taken_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_endpoint)
        self.not_allowed(req, 'POST')

        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken_copy = json.loads(req.content)
        taken_copy_id = unquote(taken_copy['id'])
        self.assertEqual(taken_id, taken_copy_id)

        # Only GET of this endpoint is supported
        take_endpoint = taken_endpoint + 'take/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', take_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(take_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', take_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(take_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', take_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(take_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.ok(req)
        question = json.loads(req.content)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        files_endpoint = taken_endpoint + 'files/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(files_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(files_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(files_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # Can submit a wrong response
        submit_endpoint = taken_endpoint + 'submit/'
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

        # to submit again, have to get a new taken
        # Now getting another taken will return a new one
        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        new_taken = json.loads(req.content)
        quoted_new_taken_id = new_taken['id']
        new_taken_id = unquote(quoted_new_taken_id)
        self.assertNotEqual(taken_id, quoted_new_taken_id)

        new_taken_endpoint = bank_endpoint + 'assessmentstaken/' + new_taken_id + '/'
        new_submit_endpoint = new_taken_endpoint + 'submit/'
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', new_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(new_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

        # Trying to retake / reGET the files or taken or resubmit
        # should throw an exception.
        req = self.client.post(new_submit_endpoint, right_response, format='json')
        self.already_taken(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.already_taken(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.already_taken(req)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, new_taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessment_taking(self):
        """
        POSTing to takens should create a new one.
        Should only be able to POST from the /assessmentsoffered/ endpoint
        But can GET from the /assessments/ endpoint, too.

        Without submitting a response, POSTing again should
        return the same one.

         -- Check that for Ortho3D types, a taken has files.

        Submitting and then POSTing should return a new
        taken.

        This should work for both instructors and learners
         -- TODO: Duplicate this for learners

         NOTE: this tests the obfuscated way of taking assessments (no list of questions)
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        expected_files = self.extract_question(item)['files']

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }


        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # From here, a learner should be able to do everything
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken_copy = json.loads(req.content)
        taken_copy_id = unquote(taken_copy['id'])
        self.assertEqual(taken_id, taken_copy_id)

        # Only GET of this endpoint is supported
        take_endpoint = taken_endpoint + 'take/'
        get_sig = calculate_signature(auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.ok(req)
        question = json.loads(req.content)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        files_endpoint = taken_endpoint + 'files/'

        get_sig = calculate_signature(auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # Can submit a wrong response
        submit_endpoint = taken_endpoint + 'submit/'
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        post_sig = calculate_signature(auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

        # to submit again, have to get a new taken
        # Now getting another taken will return a new one
        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        new_taken = json.loads(req.content)
        new_taken_id = unquote(new_taken['id'])
        self.assertNotEqual(taken_id, new_taken_id)

        new_taken_endpoint = bank_endpoint + 'assessmentstaken/' + new_taken_id + '/'
        new_take_endpoint = new_taken_endpoint + 'take/'
        new_files_endpoint = new_taken_endpoint + 'files/'
        new_submit_endpoint = new_taken_endpoint + 'submit/'
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', new_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(new_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

        # Trying to retake / reGET the files or taken or resubmit
        # should throw an exception.
        req = self.client.post(new_submit_endpoint, right_response, format='json')
        self.already_taken(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', new_take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(new_take_endpoint)
        self.already_taken(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', new_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(new_files_endpoint)
        self.already_taken(req)

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, new_taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken

        NOTE: this tests the simple, question-dump (client in control)
        for taking assessments
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }


        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', taken_questions_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_questions_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', taken_questions_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_questions_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_questions_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_questions_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)


        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_files_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_files_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_files_endpoint)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE should not work on the status URLs
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_status_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_status_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_status_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_status_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_status_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_status_endpoint)
        self.not_allowed(req, 'POST')


        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responsded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_2_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken
        Same as above, but for a learner

        NOTE: this tests the simple, question-dump (client in control)
        for taking assessments
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }


        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Learner can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', taken_questions_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_questions_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', taken_questions_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_questions_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_questions_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_questions_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)
        self.verify_question_status(question_1, False)
        self.verify_question_status(question_2, False)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_endpoint)
        self.not_allowed(req, 'POST')

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_files_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_files_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_files_endpoint)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE should not work on the status URLs
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_status_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_status_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_status_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_status_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_status_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_status_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, True, False)

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, False)
        self.verify_question_status(question_2, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the question status now should return a responded right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, True, True)

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, True)
        self.verify_question_status(question_2, False)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_2_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # convert back to instructor to do clean-up
        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    # def test_authenticated_instructor_euler_crud(self):
    #     """
    #     Does the Euler-rotation type question work?
    #     :return:
    #     """
    #     self.fail('Finish writing the test!')
    #
    def test_authenticated_instructor_learner_form_posting(self):
        """
        All POST endpoints should accept multipart/form-data as well
        as just JSON.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc, True)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='multipart')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='multipart')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }


        self.sign_client(offering_post_sig)
        payload_stringified = {
            'startTime' : json.dumps(payload['startTime']),
            'duration'  : json.dumps(payload['duration'])
        }
        req = self.client.post(assessment_offering_endpoint, payload_stringified, format='multipart')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)


        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        wrong_response_stringified = {
            'integerValues' : json.dumps(wrong_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint, wrong_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        right_response_stringified = {
            'integerValues' : json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint, right_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)


        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        right_response_stringified = {
            'integerValues' : json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_2_submit_endpoint, right_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # convert back to instructor to do clean-up
        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_items_first_angle(self):
        """
        Test that can set the firstAngle attribute of a question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, False)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')
        self.verify_first_angle(req, False)

        # Now try to update the firstAngle
        item_detail_endpoint = items_endpoint + item_id + '/'
        payload = {
            'question' : {
                'firstAngle'    : True
            }
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, True)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')
        self.verify_first_angle(req, True)

        self.delete_item(auth, item_detail_endpoint)

        # now test that you can create an item initially with the firstAngle set
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"          : question_type,
                "questionString": question_string,
                "firstAngle"    : True
            },
            "answers"   : [{
                "type"          : answer_type,
                "integerValues" : {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        self.verify_first_angle(req, True)

        item_id = unquote(json.loads(req.content)['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_first_angle(req, True)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

   # # BUILD A TEST FOR SENDING SINGLE ITEM ID TO ASSESSMENT, NOT A LIST

    def test_authenticated_instructor_ortho_3d_multi_choice_crud(self):
        """
        Test an instructor can upload a manipulatable AssetBundle file
        and multiple 2-image ortho choice sets to create a multiple
        choice question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "choiceId": 2
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint, stringified_payload)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        self.manip.seek(0)
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)

        # this should throw an exception -- need to include some choices!
        req = self.client.post(items_endpoint, stringified_payload)
        self.include_choices(req)

        # even with one choice set, should throw an exception
        stringified_payload['choice0small'] = self.choice0sm
        stringified_payload['choice0big'] = self.choice0big
        self.manip.seek(0)
        req = self.client.post(items_endpoint, stringified_payload)
        self.include_choices(req)

        # should be okay with two choices
        stringified_payload['choice1small'] = self.choice1sm
        stringified_payload['choice1big'] = self.choice1big
        self.manip.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']

        expected_choices = self.extract_question(item)['choices']
        expected_answer = [self.extract_answers(item)[0]['choiceIds'][0]]

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])
        self.verify_ortho_multi_choice_answers(req, expected_answer)
        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip':self.encode(self.manip)})

        item_details_endpoint = items_endpoint + item_id + '/'

        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        # check that the questions are present in the taken
        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip':self.encode(self.manip)})

        # Submitting a non-list response is not good, even if it is right
        bad_response = {
            'choiceIds': expected_answer[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, bad_response)
        self.bad_multi_choice_response(req)

        # Now can submit a response to this endpoint
        response = {
            'choiceIds': [expected_answer[0]]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # Should also be able to update items with multiple answers!
        # POST to the item_answers_endpoint
        item_answers_endpoint = item_details_endpoint + 'answers/'
        payload = {
            'type'     : answer_type,
            'choiceId' : 1
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', item_answers_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answers_endpoint, payload)
        self.ok(req)

        all_answers = json.loads(req.content)['results']  # should be a list of two objects
        self.assertEqual(
            json.loads(req.content)['count'],
            2
        )
        expected_answers = []
        for ans in all_answers:
            expected_answers.append(ans['choiceIds'][0])
        expected_answer_1 = expected_answers[0]
        expected_answer_2 = expected_answers[1]

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.verify_ortho_multi_choice_answers(req, expected_answers)

        # DELETE the old taken, create a new one so the updated answer
        # is a part of it
        self.delete_item(auth, taken_endpoint)
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # TODO: submitting a single answer (even if right) will be wrong here
        incomplete_response = {
            'choiceIds': [expected_answer_1]
        }
        taken_questions_endpoint = taken_endpoint + 'questions/'
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, incomplete_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # TODO: submitting both answers is okay
        response = {
            'choiceIds': [expected_answer_1, expected_answer_2]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # TODO: submitting both answers in the wrong order is okay
        response = {
            'choiceIds': [expected_answer_2, expected_answer_1]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_can_set_item_genus_type_and_file_names(self):
        """
        When creating a new item, can define a specific genus type
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        item_genus = 'question%3Achoose-viewset%40ODL.MIT.EDU'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        manip_name = 'A bracket'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "genus"         : item_genus,
            "question"      : {
                "choiceNames"   : ["view 1", "view 2"],
                "genus"         : item_genus,
                "promptName"    : manip_name,
                "type"          : question_type,
                "questionString": question_string
            },
            "answers": [{
                "genus"     : item_genus,
                "type"      : answer_type,
                "choiceId"  : 1
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['choice0small'] = self.choice0sm
        stringified_payload['choice0big'] = self.choice0big
        stringified_payload['choice1small'] = self.choice1sm
        stringified_payload['choice1big'] = self.choice1big

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         _genus=item_genus)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id,
                         _genus=item_genus)
        self.verify_links(req, 'Item')
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )

        item_details_endpoint = items_endpoint + item_id + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_missing_parameters(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        params = ['questionString','choices','choiceId']
        for param in params:
            test_payload = deepcopy(payload)
            if param == 'choiceId':
                del test_payload['answers'][0][param]
            else:
                del test_payload['question'][param]
            req = self.client.post(items_endpoint,
                                   test_payload,
                                   format='json')
            self.code(req, 500)
            self.message(req,
                         '"' + param + '" required in input parameters but not provided.',
                         True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_high(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 4
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.code(req, 500)
        self.message(req,
                     'Correct answer 4 is not valid. Not that many choices!',
                     True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_low(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 0
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')

        self.code(req, 500)
        self.message(req,
                     'Correct answer 0 is not valid. Must be between 1 and # of choices.',
                     True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_not_enough_choices(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 1
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.code(req, 500)
        self.message(req,
                     '"choices" is shorter than 2.',
                     True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_with_named_choices(self):
        """
        Test an instructor can create named choices with a dict
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text' : 'I hope so.',
             'name' : 'yes'},
            {'text' : 'I don\'t think I can.',
             'name' : 'no'},
            {'text' : 'Maybe tomorrow.',
             'name' : 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)

        item = self.load(req)
        item_details_endpoint = items_endpoint + unquote(item['id']) + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_crud(self):
        """
        Test an instructor can create and respond to an edX multiple choice
        type question.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        expected_answers = item['answers'][0]['choiceIds']

        # attach to an assessment -> offering -> taken
        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        # Submitting a non-list response is not good, even if it is right
        bad_response = {
            'choiceIds': expected_answers[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, bad_response)
        self.bad_multi_choice_response(req)

        # Now can submit a response to this endpoint
        response = {
            'choiceIds': expected_answers
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)


    def test_authenticated_instructor_edx_multi_choice_with_files(self):
        """
        In case instructor wants to upload image files, etc., with the problem
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text' : 'I hope so.',
             'name' : 'yes'},
            {'text' : 'I don\'t think I can.',
             'name' : 'no'},
            {'text' : 'Maybe tomorrow.',
             'name' : 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)

        self.assertIn('fileIds', item)
        self.assertTrue(len(item['fileIds']) == 1)
        self.assertIn('fileIds', item['question'])
        self.assertTrue(len(item['question']['fileIds']) == 1)
        self.assertIn('files', item['question'])
        self.assertTrue(len(item['question']['files']) == 1)
        self.assertEqual(
            item['fileIds']['manip'],
            item['question']['fileIds']['manip']
        )

        item_details_endpoint = items_endpoint + unquote(item['id']) + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authorization_hints_instructor(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        get_sig = calculate_signature(auth, self.headers, 'GET', authz_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(authz_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments','assessments_offered','assessments_taken',
                         'assessment_banks','items']
        nested_keys = ['can_create','can_delete','can_lookup','can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                self.assertTrue(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

        self.delete_item(auth, bank_endpoint)


    def test_authorization_hints_learner(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        self.convert_user_to_bank_learner(bank_id)

        get_sig = calculate_signature(auth, self.headers, 'GET', authz_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(authz_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments','assessments_offered','assessments_taken',
                         'assessment_banks','items']
        nested_keys = ['can_create','can_delete','can_lookup','can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                if (verb == 'can_lookup' and
                    key == 'assessments_taken'):
                    self.assertTrue(authz)
                elif (verb == 'can_lookup' and
                    key == 'assessment_banks'):
                    self.assertTrue(authz)
                elif (verb == 'can_take'):
                    self.assertTrue(authz)
                elif (verb == 'can_create' and
                      key == 'assessments_taken'):
                    self.assertTrue(authz)
                else:
                    self.assertFalse(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_list_pagination(self):
        list_endpoints = ['banks','items','assessments','assessmentsoffered','assessmentstaken']
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        bank_endpoints = []

        for i in range(0, 20):
            name = 'atestbank ' + str(i)
            desc = 'for testing purposes only ' + str(i)
            bank_id = self.create_test_bank(auth, name, desc)
            bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
            bank_endpoints.append(bank_endpoint)

        banks_endpoint = self.endpoint + 'assessment/banks/'
        get_sig = calculate_signature(auth, self.headers, 'GET', banks_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(banks_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_fields = ['count','next','previous','results']
        for field in expected_fields:
            self.assertIn(
                field,
                data['data']
            )

        self.assertNotEqual(
            data['data']['next'],
            None
        )

        self.assertEqual(
            data['data']['previous'],
            None
        )

        self.assertTrue(data['data']['count'] >= 20)

        self.assertEqual(
            len(data['data']['results']),
            10
        )

        page2 = data['data']['next']
        # total hack
        page2 = page2.replace('http://testserver:80', '')

        sig2 = calculate_signature(auth, self.headers, 'GET', page2)
        self.sign_client(sig2)
        req2 = self.client.get(page2)
        self.ok(req2)
        data2 = self.load(req2)
        self.assertNotEqual(
            data['data']['results'],
            data2['data']['results']
        )

        self.assertEqual(
            len(data2['data']['results']),
            10
        )

        self.assertTrue(data2['data']['count'] >= 20)

        if data2['data']['count'] > 20:
            self.assertNotEqual(
                data2['data']['next'],
                None
            )
        else:
            self.assertEqual(
                data2['data']['next'],
                None
            )

        self.assertNotEqual(
            data2['data']['previous'],
            None
        )

        for endpoint in list_endpoints:
            if endpoint != 'banks':  # because this was already checked
                url = bank_endpoints[0] + endpoint + '/'
                get_sig = calculate_signature(auth, self.headers, 'GET', banks_endpoint)
                self.sign_client(get_sig)

                req = self.client.get(banks_endpoint)
                self.ok(req)
                data = self.load(req)

                for field in expected_fields:
                    self.assertIn(
                        field,
                        data['data']
                    )

        for url in bank_endpoints:
            self.delete_item(auth, url)

    def test_can_get_edxml_item_format_from_item_details(self):
        """
        Do this for learners, assume instructors can also get to this.
        Other tests should reveal if instructors cannot.
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        item_edxml_endpoint = item_details_endpoint + 'edxml/'

        # have to be instructor to use this endpoint
        self.convert_user_to_bank_learner(bank_id)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(item_edxml_endpoint)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_edxml_endpoint)
        self.ok(req)

        data = self.load(req)
        self.assertIn(
            'manip.generic',  # no file extension for manip, because not truly an edX includable file type...we only support .png, .jpg, etc.
            data['files'].keys()
        )
        self.reset_files()

        # test that the S3 URL is not given
        returned_url = data['files']['manip.generic']
        self.assertNotEqual(
            returned_url,
            self._aws_root + str(Id(bank_id).identifier) + '/' + self._manip_filename
        )

        # verify that a signed Cloudfront URL is returned, but don't verify the signature
        # assume that boto calculates the right signature?
        self.is_cloudfront_url(returned_url)

        self.assertEqual(
            str(data['data']),
            '<problem display_name="a really complicated item" max_attempts="0" rerandomize="never" showanswer="closed">\n <p>\n  can you manipulate this?\n </p>\n <multiplechoiceresponse>\n  <choicegroup direction="vertical">\n   <choice correct="false" name="Choice 1">\n    <text>\n     yes\n    </text>\n   </choice>\n   <choice correct="true" name="Choice 2">\n    <text>\n     no\n    </text>\n   </choice>\n   <choice correct="false" name="Choice 3">\n    <text>\n     maybe\n    </text>\n   </choice>\n  </choicegroup>\n </multiplechoiceresponse>\n</problem>'
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # def test_can_get_edxml_item_format_from_assessment_taken(self):
    #     """
    #     Do this for learners, assume instructors can also get to this.
    #     Other tests should reveal if instructors cannot.
    #     :return:
    #     """
    #     # CANNOT support the following, yet. Permissions will not work, because
    #     # need the Item (usually to fill in the right answer), yet learners who
    #     # access takens only cannot get to the item, only the question.
    #
    #     # but learners should be able to access the same data from takens/<id>/question/<id>/edxml/
    #     # assessments_endpoint = bank_endpoint + 'assessments/'
    #     # assessment_name = 'a really hard assessment'
    #     # assessment_desc = 'meant to differentiate students'
    #     # payload = {
    #     #     "name": assessment_name,
    #     #     "description": assessment_desc,
    #     #     "itemIds"   : [item_id]
    #     # }
    #     # post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
    #     # self.sign_client(post_sig)
    #     # req = self.client.post(assessments_endpoint, payload, format='json')
    #     # self.ok(req)
    #     # assessment_id = unquote(json.loads(req.content)['id'])
    #     #
    #     # assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
    #     # assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
    #     # assessment_items_endpoint = assessment_detail_endpoint + 'items/'
    #     #
    #     # # Use POST to create an offering
    #     # offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
    #     # self.sign_client(offering_post_sig)
    #     #
    #     # payload = {
    #     #     "startTime" : {
    #     #         "day"   : 1,
    #     #         "month" : 1,
    #     #         "year"  : 2015
    #     #     },
    #     #     "duration"  : {
    #     #         "days"  : 2
    #     #     }
    #     # }
    #     #
    #     # self.sign_client(offering_post_sig)
    #     # req = self.client.post(assessment_offering_endpoint, payload, format='json')
    #     # self.ok(req)
    #     # offering = json.loads(req.content)
    #     # offering_id = unquote(offering['id'])
    #     #
    #     # assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
    #     #
    #     # # Can POST to create a new taken
    #     # assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
    #     # post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
    #     # self.sign_client(post_sig)
    #     # req = self.client.post(assessment_offering_takens_endpoint)
    #     # self.ok(req)
    #     # taken = json.loads(req.content)
    #     # taken_id = unquote(taken['id'])
    #     #
    #     # taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
    #     #
    #     # taken_questions_endpoint = taken_endpoint + 'questions/'
    #     #
    #     # taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
    #     # taken_edxml_endpoint = taken_question_details_endpoint + 'edxml/'
    #     #
    #     # self.convert_user_to_bank_learner(bank_id)
    #     #
    #     # get_sig = calculate_signature(auth, self.headers, 'GET', taken_edxml_endpoint)
    #     # self.sign_client(get_sig)
    #     # req = self.client.get(taken_edxml_endpoint)
    #     # self.ok(req)
    #     #
    #     # data = self.load(req)
    #     # self.assertIn(
    #     #     'manip.generic',  # no file extension for manip, because not truly an edX includable file type...we only support .png, .jpg, etc.
    #     #     data['files'].keys()
    #     # )
    #     # self.reset_files()
    #     # self.assertEqual(
    #     #     data['files']['manip.generic'],
    #     #     base64.b64encode(self.manip.read())
    #     # )
    #     #
    #     # self.assertEqual(
    #     #     str(data['data']),
    #     #     '<problem display_name="a really complicated item" max_attempts="0" rerandomize="never" showanswer="closed">\n <p>\n  can you manipulate this?\n </p>\n <multiplechoiceresponse>\n  <choicegroup direction="vertical">\n   <choice correct="false" name="Choice 1">\n    <text>\n     yes\n    </text>\n   </choice>\n   <choice correct="true" name="Choice 2">\n    <text>\n     no\n    </text>\n   </choice>\n   <choice correct="false" name="Choice 3">\n    <text>\n     maybe\n    </text>\n   </choice>\n  </choicegroup>\n </multiplechoiceresponse>\n</problem>'
    #     # )
    #
    #     self.fail('finish writing the test')

    def test_non_supported_item_formats_in_item_details(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        output_format = 'qti'
        item_edxml_endpoint = item_details_endpoint + output_format + '/'

        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_edxml_endpoint)
        self.code(req, 500)
        self.message(req,
                     '"' + output_format + '" is not a supported item text format.',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # def test_non_supported_item_formats_in_assessment_taken(self):
    #     # CANNOT support the following, yet. Permissions will not work, because
    #     # need the Item (usually to fill in the right answer), yet learners who
    #     # access takens only cannot get to the item, only the question.
    #     self.fail('finish writing the test')

    def test_edX_item_metadata_on_create(self):
        # can add metadata like max_attempts to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            self.assertEqual(
                item[key],
                value
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edX_item_metadata_update(self):
        # can update metadata like max_attempts to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        new_metadata = {
            'attempts'      : metadata['attempts'] - 1,
            'markdown'      : 'second fake markdown',
            'rerandomize'   : 'second fake rerandomize',
            'showanswer'    : 'second fake showanswer',
            'weight'        : metadata['weight'] / 2
        }

        for key, value in new_metadata.iteritems():
            new_payload = {
                key : value
            }
            put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
            self.sign_client(put_sig)
            req = self.client.put(item_details_endpoint,
                                   new_payload,
                                   format='json')
            self.ok(req)
            item = self.load(req)
            self.assertEqual(
                item[key],
                value
            )
            self.assertNotEqual(
                item[key],
                metadata[key]
            )

            get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
            self.sign_client(get_sig)
            req = self.client.get(item_details_endpoint)
            self.ok(req)
            item = self.load(req)

            self.assertEqual(
                item[key],
                value
            )
            self.assertNotEqual(
                item[key],
                metadata[key]
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)


    def test_edX_item_irt_on_create(self):
        # can add IRT like difficulty to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edX_item_irt_update(self):
        # can update IRT like difficulty to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        new_irt = {
            'difficulty'     : irt['difficulty'] + 2,
            'discrimination' : irt['discrimination'] * -1
        }

        for key, value in new_irt.iteritems():
            new_payload = {
                key : value
            }
            put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
            self.sign_client(put_sig)
            req = self.client.put(item_details_endpoint,
                                   new_payload,
                                   format='json')
            self.ok(req)
            item = self.load(req)
            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

            get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
            self.sign_client(get_sig)
            req = self.client.get(item_details_endpoint)
            self.ok(req)
            item = self.load(req)

            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?max_difficulty='

        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_difficulty = irt['difficulty'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_min_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_and_min_difficulty_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear += str(max_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 2
        query_endpoint_should_not_appear += str(max_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_max_and_min_difficulty_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        bad_min = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_difficulty='
        bad_max = irt['difficulty'] - 1
        query_endpoint_should_appear += str(bad_max)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.code(req, 500)
        self.message(req,
                     'max_difficulty cannot be less than min_difficulty',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # we don't support this yet
    # def test_item_query_irt_max_and_min_difficulty_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_max_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?max_discrimination='

        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_discrimination = irt['discrimination'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_min_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_and_min_discrimination_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear += str(max_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 2
        query_endpoint_should_not_appear += str(max_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_max_and_min_discrimination_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        bad_min = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_discrimination='
        bad_max = irt['discrimination'] - 1
        query_endpoint_should_appear += str(bad_max)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.code(req, 500)
        self.message(req,
                     'max_discrimination cannot be less than min_discrimination',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)
    # we don't support this yet
    # def test_item_query_irt_max_and_min_discrimination_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_display_name(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_name = query_setup_data['item_name']

        query_endpoint = bank_endpoint + 'items/query/?display_name='

        word_in_name = item_name.split(' ')[2]
        query_endpoint_should_appear = query_endpoint + word_in_name

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        word_not_in_name = 'xkcd'
        query_endpoint_should_not_appear = query_endpoint + word_not_in_name
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_lti_consumer_instance_guid_defaults_to_ip(self):
        """
        Cannot count on the LTI consumers providing the lti_tool_consumer_instance_guid
        because it is only a recommended parameter. So default to the requester IP.
        """
        from assessments_users.models import LTIUser
        from ..utilities import id_generator
        lti_headers = ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']
        test_endpoint = self.endpoint + 'assessment/banks/'

        random_id = id_generator()

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = ''
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)

        self.ok(req)

        self.assertTrue(LTIUser.objects.filter(user_id=random_id).exists())
        self.assertEqual(
            len(LTIUser.objects.filter(user_id=random_id)),
            1
        )


        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Learner'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)

        self.ok(req)

        self.assertEqual(
            len(LTIUser.objects.filter(user_id=random_id)),
            2
        )

        self.assertNotEqual(
            LTIUser.objects.filter(user_id=random_id).first().consumer_guid,
            LTIUser.objects.filter(user_id=random_id).last().consumer_guid
        )

    def test_review_options_flag_works_for_during_and_after_attempt(self):
        """
        For the Reviewable offered and taken records
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # verify the "reviewWhetherCorrect" is True
        self.assertTrue(taken['reviewWhetherCorrect'])

        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set to only view correct after attempt
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime"     : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"      : {
                "days"  : 2
            },
            "reviewOptions" : {
                "whetherCorrect" : {
                    "duringAttempt" : False
                }
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        self.convert_user_to_bank_learner(bank_id)

        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # verify the "reviewWhetherCorrect" is False
        self.assertFalse(taken['reviewWhetherCorrect'])

        # now submitting an answer should let reviewWhetherCorrect be True
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }

        finish_taken_endpoint = taken_endpoint + 'finish/'
        taken_questions_endpoint = taken_endpoint + 'questions/'
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)

        post_sig = calculate_signature(auth, self.headers, 'POST', finish_taken_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(finish_taken_endpoint)
        self.ok(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_endpoint)
        self.ok(req)
        taken = json.loads(req.content)

        self.assertTrue(taken['reviewWhetherCorrect'])

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)

        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_update_review_options_flag(self):
        """
        For the Reviewable offered and taken records, can  change the reviewOptions
        flag on a created item
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        payload = {
            "reviewOptions" : {
                "whetherCorrect" : {
                    "duringAttempt" : False
                }
            }
        }
        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        offering_put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_offering_detail_endpoint)
        self.sign_client(offering_put_sig)
        req = self.client.put(assessment_offering_detail_endpoint, payload, format='json')

        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        self.delete_item(auth, assessment_offering_detail_endpoint)

        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_set_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_set_non_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'mc3-objective%3A21273%40MIT-OEIT'  # also test if this does NOT get re-quoted
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : [lo_test_id]
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            [lo_test_id]
        )


        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_update_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds"  : new_los
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_details_endpoint, payload, format='json')
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_update_non_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : los
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)

        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            los
        )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds"  : new_los
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_details_endpoint, payload, format='json')
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )


        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edx_item_query_learning_objective_id(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_id = query_setup_data['item_id']
        los = query_setup_data['learning_objectives']
        query_endpoint = bank_endpoint + 'items/query/?learning_objective=' + los[0]  # can query with un-quote safed id
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/query/?learning_objective=wrongId'
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_non_edx_item_query_learning_objective_id(self):
        # not supported in their item records yet, so this should
        # return no results?
        # And yet it does work -- so there is something I do not
        # understand about query objects and items...
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : los
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'

        query_endpoint = bank_endpoint + 'items/query/?learning_objective=' + los[0]
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)

        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/query/?learning_objective=wrongId'
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_set_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set maxAttempts on create
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime"     : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"      : {
                "days"  : 2
            },
            "maxAttempts" : 2
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_update_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # try again, but set maxAttempts on update
        offering_put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_offering_detail_endpoint)
        self.sign_client(offering_put_sig)

        payload = {
            "maxAttempts" : 2
        }

        self.sign_client(offering_put_sig)
        req = self.client.put(assessment_offering_detail_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_default_max_attempts_allows_infinite_attempts(self):
        # ok, don't really test to infinity, but test several
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 25
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(assessment_offering_takens_endpoint)
            self.ok(req)
            taken = json.loads(req.content)
            taken_id = unquote(taken['id'])

            taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
            taken_endpoints.append(taken_endpoint)
            taken_finish_endpoint = taken_endpoint + 'finish/'
            post_sig = calculate_signature(auth, self.headers, 'POST', taken_finish_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(taken_finish_endpoint)
            self.ok(req)
            # finish the assessment taken, so next time we create one, it should
            # create a new one

        self.convert_user_to_bank_instructor(bank_id)

        for taken_endpoint in taken_endpoints:
            self.delete_item(auth, taken_endpoint)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_max_attempts_throws_exception_if_taker_tries_to_exceed(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)


        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            },
            "maxAttempts" : 2
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts == 2
        self.assertEquals(offering['maxAttempts'], 2)

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 5
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(assessment_offering_takens_endpoint)
            if attempt >= payload['maxAttempts']:
                self.code(req, 403)
                self.message(req,
                             'You have exceeded the maximum number of allowed attempts.',
                             True)
            else:
                self.ok(req)
                taken = json.loads(req.content)
                taken_id = unquote(taken['id'])

                taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
                taken_endpoints.append(taken_endpoint)
                taken_finish_endpoint = taken_endpoint + 'finish/'
                post_sig = calculate_signature(auth, self.headers, 'POST', taken_finish_endpoint)
                self.sign_client(post_sig)
                req = self.client.post(taken_finish_endpoint)
                self.ok(req)
                # finish the assessment taken, so next time we create one, it should
                # create a new one

        self.convert_user_to_bank_instructor(bank_id)

        for taken_endpoint in taken_endpoints:
            self.delete_item(auth, taken_endpoint)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)
