# to test the authentication, need to follow the example in the README of
# https://github.com/etoccalino/django-rest-framework-httpsignature
# modified to work with assessments-dev.mit.edu

import pdb
import json
import time
import base64
import logging

import requests

from django.http import HttpRequest
from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.test import RequestFactory
from django.contrib.auth.views import login as admin_login

from rest_framework_httpsignature.authentication import SignatureAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.test import APIRequestFactory, APITestCase
from rest_framework import status

from http_signature.requests_auth import HTTPSignatureAuth

from urllib import unquote, quote

from ..models import APIUser

from copy import deepcopy

from Crypto import Random


class APISignatureAuthentication(SignatureAuthentication):
    """
    Adapted from
    https://github.com/etoccalino/django-rest-framework-httpsignature/blob/master/rest_framework_httpsignature/tests.py
    """
    API_KEY_HEADER = 'X-Api-Key'
    def __init__(self, user):
        self.user = user

    def fetch_user_data(self, api_key):
        return (self.user, str(APIUser.objects.get(username="tester").private_key))


class APITest(APITestCase):
    fixtures = ['test_data.json']

    def already_taken(self, _req):
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Assessment already completed."}'
        )

    def answer_not_found(self, _req):
        """
        Answer was not found -- bad answer_id passed in
        """
        ERRORS = ['{"detail": "Answer not found."}']
        self.assertEqual(_req.status_code, 500)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def bad_input(self, _req):
        """
        Input was a bad format...i.e. a string or non-JSON thing
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Poorly formatted input data."}'
        )

    def bad_multi_choice_response(self, _req):
        """
        choiceIds needs to be a list format, even for one item
        :param _req:
        :return:
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "choiceIds should be a list for multiple-choice questions."}'
        )

    def code(self, _req, _code):
        self.assertEqual(
            int(_req.status_code),
            int(_code)
        )

    def convert_user_to_bank_instructor(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as an instructor
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_bank_learner(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as a learner
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def convert_user_to_instructor(self):
        """
        Convert user to instructor in department,
        i.e. DepartmentAdmin
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_learner(self):
        """
        Convert user to a basic learner in the department,
        i.e. a DepartmentOfficer
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def create_test_bank(self, _auth, _name, _desc, _form=False):
        """
        helper method to create a test assessment bank
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        payload = {
            "name": _name,
            "description": _desc
        }

        if _form:
            req = self.client.post(test_endpoint,
                                   data=payload,
                                   auth=_auth,
                                   headers=self.headers,
                                   files={})
        else:
            req = self.client.post(test_endpoint,
                               data=json.dumps(payload),
                               auth=_auth,
                               headers=self.json_headers)

        # req.content should have the new bank item, with its ID (which
        # we will need to clean it up)
        self.assertEqual(req.status_code, 200)
        self.verify_text(req, 'Bank', _name, _desc)

        return unquote(json.loads(req.content)['id'])

    def delete_item(self, _auth, _url):
        """
        helper method that sends a delete request to the url
        calculates the signature for you
        """
        req = self.client.delete(_url, auth=_auth, headers=self.headers)
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.content, '')

    def encode(self, _file):
        _file.seek(0)
        return base64.b64encode(_file.read())

    def extract_answers(self, item):
        """
        Extract the answer part of an item
        """
        if 'answers' in item:
            return item['answers']
        else:
            return item['data']['results'][0]['answers']

    def extract_question(self, item):
        """
        Extract the question part of an item
        """
        if 'question' in item:
            return item['question']
        else:
            return item['data']['results'][0]['question']

    def include_choices(self, _req):
        """
        Check that an "include choices" error message is included
        :param _req:
        :return:
        """
        errors = ['{"detail": "At least two choice set attribute(s) required for Ortho-3D items."}',
                  '{"detail": "Large and small image files attribute(s) required for Ortho-3D items."}']
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertIn(
            _req.content,
            errors
        )

    def load(self, _req):
        return json.loads(_req.content)

    def message(self, _req, _msg, equal=False):
        data = self.load(_req)
        if equal:
            self.assertEqual(
                str(_msg),
                str(data['detail'])
            )
        else:
            self.assertNotEqual(
                str(_msg),
                str(data['detail'])
            )

    def missing_orthoview(self, _req):
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertEqual(
            _req.content,
            '{"detail": "All three view set attribute(s) required for Ortho-3D items."}'
        )

    def need_items(self, _req):
        """
        Need items in assessment before creating an offering
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Cannot create an assessment offering for an assessment with no items."}'
        )

    def not_allowed(self, _req, _type):
        """
        verify that the _type method is not allowed. Status code 405
        and the text matches not allowed text
        """
        self.assertEqual(_req.status_code, 405)
        self.assertEqual(
            _req.content,
            '{"detail": "Method \'' + _type + '\' not allowed."}'
        )

    def not_empty(self, _req):
        """
        Verify that the bank is not empty and cannot be deleted
        """
        ERRORS = ['{"detail": "This Item is being used in one or more Assessments. Delink it first, before deleting it."}',
                  '{"detail": "Bank is not empty. Please delete its contents first."}',
                  '{"detail": "Assessment still has AssessmentOffered. Delete the offerings first."}']
        self.assertEqual(_req.status_code, 406)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def ok(self, _req):
        """
        checks for status code 200 and that the request was okay
        """
        self.assertEqual(_req.status_code, 200)

    def not_responded(self, _req):
        self.ok(_req)
        self.assertEqual(
            _req.content,
            '{"responded": false}'
        )

    def reset_files(self):
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        self.choice1sm.seek(0)
        self.choice1big.seek(0)

    def reset_header_to_new_user(self):
        """
        When self._username changes, need to update the X-Api-Proxy in the header
        """
        self.headers['X-Api-Proxy'] = self._username
        self.json_headers['X-Api-Proxy'] = self._username

    def responded(self, _req, _correct=False):
        expected_response = {
            "responded" : True,
            "correct"   : _correct
        }
        self.ok(_req)
        self.assertEqual(
            json.loads(_req.content),
            expected_response
        )

    def set_up_edx_item(self, _auth):
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(_auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'

        metadata = {
            'attempts'      : 4,
            'markdown'      : 'fake markdown',
            'rerandomize'   : 'fake rerandomize',
            'showanswer'    : 'fake showanswer',
            'weight'        : 1.23
        }

        irt = {
            'difficulty'        : -1.34,
            'discrimination'    : 2.41
        }

        learning_objectives = ['mc3-objective:21273@MIT-OEIT']  # also test that this gets quote-safed

        payload = {
            "attempts"      : metadata['attempts'],
            "markdown"      : metadata['markdown'],
            "rerandomize"   : metadata['rerandomize'],
            "showanswer"    : metadata['showanswer'],
            "weight"        : metadata['weight'],
            "difficulty"    : irt['difficulty'],
            "discrimination": irt['discrimination'],
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
            "learningObjectiveIds"  : learning_objectives
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=_auth,
                               headers=self.json_headers)
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            self.assertEqual(
                item[key],
                value
            )

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

        for index, _id in enumerate(learning_objectives):
            self.assertEqual(
                str(item['learningObjectiveIds'][index]),
                str(quote(_id))
            )

        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        return {
            'bank_endpoint'         : bank_endpoint,
            'irt'                   : irt,
            'item_endpoint'         : item_details_endpoint,
            'item_id'               : item_id,
            'item_name'             : item_name,
            'learning_objectives'   : learning_objectives,
            'metadata'              : metadata
        }


    def setUp(self):
        from django.contrib.auth.models import AnonymousUser
        from datetime import datetime
        from time import mktime
        from wsgiref.handlers import format_date_time

        self.anon = AnonymousUser()
        self._app_user = 'tester'
        self._instructor = 'taaccct_instructor'
        self._learner = 'taaccct_student'
        self._username = self._instructor
        self._host = 'assessments-dev.mit.edu'
        self.user = APIUser.objects.get(username=self._app_user)
        self.public_key = self.user.public_key
        self.private_key = str(self.user.private_key)
        self.signature_headers = ['request-line','accept','date','host','x-api-proxy']
        self.headers = {
            'Date'          : format_date_time(mktime(datetime.now().timetuple())),
            'Host'          : self._host,
            'Accept'        : 'application/json',
            'X-Api-Key'     : self.public_key,
            'X-Api-Proxy'   : str(self._username)
        }
        self.json_headers = deepcopy(self.headers)
        self.json_headers['content-type'] = 'application/json'
        self.form_headers = deepcopy(self.headers)
        self.form_headers['content-type'] = 'multipart/form-data'
        self.auth = APISignatureAuthentication(self.user)
        self.endpoint = 'https://' + self._host + '/api/v1/'
        self.client = requests

        self._manip_file = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/myAssetBundle.unity3d'
        self.manip = open(self._manip_file, 'rb')

        self._front = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/front.jpg'
        self.front = open(self._front, 'rb')

        self._side = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/side.jpg'
        self.side = open(self._side, 'rb')

        self._top = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/top.jpg'
        self.top = open(self._top, 'rb')

        self._choice0sm = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice0sm.jpg'
        self.choice0sm = open(self._choice0sm, 'rb')

        self._choice0big = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice0big.jpg'
        self.choice0big = open(self._choice0big, 'rb')

        self._choice1sm = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice1sm.jpg'
        self.choice1sm = open(self._choice1sm, 'rb')

        self._choice1big = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/choice1big.jpg'
        self.choice1big = open(self._choice1big, 'rb')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """

        self.manip.close()
        self.front.close()
        self.side.close()
        self.top.close()

        self.choice0sm.close()
        self.choice0big.close()
        self.choice1sm.close()
        self.choice1big.close()

    def unauthorized(self, _req):
        """
        confirms that status code for unauthorized access is 403, and text is the
        error message
        """
        ERRORS = ['{"detail": "Authentication credentials were not provided."}',
                  '{"detail": "Bad signature"}',
                  '{"detail": "Permission denied. You do not have rights to use this service."}',
                  '{"detail": "Permission denied. You do not have rights to view assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment banks."}',
                  '{"detail": "Permission denied. You do not have rights to create new assessment banks."}',
                  '{"detail": "Permission denied. You do not have rights to view this bank\'s details."}',
                  '{"detail": "Permission denied. You do not have rights to edit this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create new assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view item details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item details in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to assign items to assessments in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete an assessment\'s items in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit assessment offerings in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view assessment takens in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create assessment takens in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item files in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item questions in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item questions in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to create item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to get item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to edit item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to delete item answers in this bank."}',
                  '{"detail": "Permission denied. You do not have rights to view items in this bank."}']
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def user_not_found(self, _req):
        """
        User is not found--bad public key
        """
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            '{"detail": "No user found."}'
        )

    def verify_answers(self, _data, _a_strs, _a_types):
        """
        verify just the answers part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
                if 'data' in data:
                    data = data['data']['results']
            except:
                data = json.loads(_data)
        except:
            data = _data

        if 'answers' in data:
            answers = data['answers']
        elif 'results' in data:
            answers = data['results']
        else:
            answers = data

        if isinstance(answers, list):
            ind = 0
            for _a in _a_strs:
                if _a_types[ind] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                    self.assertEqual(
                        answers[ind]['text']['text'],
                        _a
                    )
                self.assertIn(
                    _a_types[ind],
                    answers[ind]['recordTypeIds']
                )
                ind += 1
        elif isinstance(answers, dict):
            if _a_types[0] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                self.assertEqual(
                    answers['text']['text'],
                    _a_strs[0]
                )
            self.assertIn(
                _a_types[0],
                answers['recordTypeIds']
            )
        else:
            self.fail('Bad answer format.')

    def verify_data_length(self, _req, _length):
        """
        Check that the data array is of the specified length
        """
        data = json.loads(_req.content)
        self.assertEqual(
            len(data['data']),
            _length
        )

    def verify_first_angle(self, _req, _angle):
        """
        Check if the req angle matches expected
        :param _req:
        :param _angle:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        self.assertEqual(
            item['question']['firstAngle'],
            _angle
        )

    def verify_links(self, _req, _type):
        """
        helper method to check that basic _links are included in
        the returned object. Includes a check for self.
        Depending on the _type, it will also check for the drilldown
        links...like for bank, should have assessments, items
        """
        search_type = _type.lower()
        self.assertIn(
            '"_links": ',
            _req.content
        )
        self.assertIn(
            '"self": ',
            _req.content
        )
        if search_type == 'bank':
            self.assertIn(
                '"assessments": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'assessmentdetails':
            self.assertIn(
                '"offerings": ',
                _req.content
            )
            self.assertIn(
                '"takens": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'service':
            self.assertIn(
                '"banks": ',
                _req.content
            )
            self.assertIn(
                '"itemTypes": ',
                _req.content
            )
        elif search_type == 'itemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'assessmentitemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'itemfiles':
            self.assertIn(
                '"front": ',
                _req.content
            )
            self.assertIn(
                '"top": ',
                _req.content
            )
            self.assertIn(
                '"manip": ',
                _req.content
            )
            self.assertIn(
                '"side": ',
                _req.content
            )
        elif search_type == 'takenquestions':
            self.assertIn(
                '"data": ',
                _req.content
            )
        elif search_type == 'takenquestion':
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"submit": ',
                _req.content
            )

    def verify_manip_file(self, _req, _manip_id):
        """
        make sure the manipulatable file is in the object correctly
        Probably some set of Ids:
            "manipId": "repository.Asset%3A53aac9afea061a05081dec55%40birdland.mit.edu",
            "orthoViewSet": {}
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            self.assertEqual(
                item['data'][0]['question']['manipId'],
                _manip_id
            )
        elif 'question' in item:
            self.assertEqual(
                item['question']['manipId'],
                _manip_id
            )
        else:
            self.assertEqual(
                item['manipId'],
                _manip_id
            )

    def verify_no_data(self, _req):
        """
        Verify that the data object in _req is empty
        """
        data = json.loads(_req.content)
        self.assertEqual(
            data['data'],
            {'count'    : 0,
             'previous' : None,
             'results'  : [],
             'next'     : None}
        )
        self.verify_data_length(_req, 4)

    def verify_offerings(self, _req, _type, _data):
        """
        Check that the offerings match...
        _type may not be used here; assume AssessmentOffered always?
        _data objects need to have an offering ID in them
        """
        def check_expected_against_one_item(expected, item):
            for key, value in expected.iteritems():
                if isinstance(value, basestring):
                    self.assertEqual(
                        item[key],
                        value
                    )
                elif isinstance(value, dict):
                    for inner_key, inner_value in value.iteritems():
                        self.assertEqual(
                            item[key][inner_key],
                            inner_value
                        )

        data = json.loads(_req.content)
        if 'data' in data:
            data = data['data']['results']
        elif 'results' in data:
            data = data['results']



        if isinstance(_data, list):
            for expected in _data:
                if isinstance(data, list):
                    for item in data:
                        if item['id'] == expected['id']:
                            check_expected_against_one_item(expected, item)
                            break
                else:
                    check_expected_against_one_item(expected, data)
        elif isinstance(_data, dict):
            if isinstance(data, list):
                for item in data:
                    if item['id'] == _data['id']:
                        check_expected_against_one_item(_data, item)
                        break
            else:
                check_expected_against_one_item(_data, data)

    def verify_ortho_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data']['results'][0]
        if 'answers' in item:
            item = item['answers'][0]
        self.assertEqual(
            item['integerValues']['frontFaceValue'],
            _data['frontFaceValue']
        )
        self.assertEqual(
            item['integerValues']['sideFaceValue'],
            _data['sideFaceValue']
        )
        self.assertEqual(
            item['integerValues']['topFaceValue'],
            _data['topFaceValue']
        )


    def verify_ortho_choices(self, _req, _expected):
        """
        make sure the files and IDs match. Assume two choices for now.
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for index, choice in enumerate(item['choices']):
            self.assertEqual(
                choice['id'],
                _expected[index]['id']
            )
            if index == 0:
                expected_file_sm = self.choice0sm
                expected_file_big = self.choice0big
            else:
                expected_file_sm = self.choice1sm
                expected_file_big = self.choice1big

            self.assertEqual(
                choice['smallOrthoViewSet'],
                self.encode(expected_file_sm)
            )

            self.assertEqual(
                choice['largeOrthoViewSet'],
                self.encode(expected_file_big)
            )

    def verify_ortho_files(self, _req, _expected):
        """
        make sure the files match
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for file_label, file_data in item['files'].iteritems():
            self.assertEqual(
                file_data,
                _expected[file_label]
            )

    def verify_ortho_multi_choice_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data'][0]
        if 'answers' in item:
            item = item['answers'][0]

        if not isinstance(_data, list):
            raise Exception('_data should be a list')

        for ans in item['choiceIds']:
            self.assertIn(
                ans,
                _data
            )

    def verify_question(self, _data, _q_str, _q_type):
        """
        verify just the question part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
            except:
                data = json.loads(_data)
        except:
            data = _data

        if 'question' in data:
            question = data['question']
        elif 'data' in data:
            try:
                question = data['data']['results'][0]['question']
            except:
                question = data['data']['results'][0]
        else:
            question = data

        self.assertEqual(
            question['text']['text'],
            _q_str
        )
        self.assertIn(
            _q_type,
            question['recordTypeIds']
        )

    def verify_questions_answers(self, _req, _q_str, _q_type, _a_str, _a_type, _id=None):
        """
        helper method to verify the questions & answers in a returned item
        takes care of all the language stuff
        Assumes multiple answers, to _a_str and _a_type must be lists
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                    item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req

        self.verify_question(data, _q_str, _q_type)

        self.verify_answers(data, _a_str, _a_type)

    def verify_question_status(self, question, responded, correct=None):
        self.assertEqual(
            question['responded'],
            responded
        )
        if correct:
            self.assertEqual(
                question['correct'],
                correct  # True
            )
        else:
            try:
                self.assertNotIn(
                    'correct',
                    question
                )
            except:
                self.assertEqual(
                    question['correct'],
                    correct  # False
                )

    def verify_submission(self, _req, _expected_result, _has_next=None):
        data = json.loads(_req.content)
        self.assertEqual(
            data['correct'],
            _expected_result
        )
        if _has_next:
            self.assertEqual(
                data['hasNext'],
                _has_next
            )

    def verify_taking_files(self, _req):
        """
        Check that the taken files match the uploaded ones
        :param _req:
        :return:
        """
        data = json.loads(_req.content)
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        self.assertEqual(
            data['manip'],
            base64.b64encode(self.manip.read())
        )
        self.assertEqual(
            data['front'],
            base64.b64encode(self.front.read())
        )
        self.assertEqual(
            data['side'],
            base64.b64encode(self.side.read())
        )
        self.assertEqual(
            data['top'],
            base64.b64encode(self.top.read())
        )

    def verify_text(self, _req, _type, _name, _desc, _id=None, _genus=None):
        """
        helper method to verify the text in a returned object
        takes care of all the language stuff
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                    item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req
        self.assertEqual(
            data['displayName']['text'],
            _name
        )
        self.assertEqual(
            data['description']['text'],
            _desc
        )
        self.assertEqual(
            data['type'],
            _type
        )

        if _genus:
            self.assertEqual(
                data['genusTypeId'],
                _genus
            )


    def verify_types(self, _req):
        """
        Make sure all the types in mongo/assessment/records are returned
        """
        from dlkit.mongo.assessment.records.types import QUESTION_RECORD_TYPES
        types_list = json.loads(_req.content)
        # self.assertEqual(len(types_list), len(ITEM_RECORD_TYPES))
        # right now ITEM_RECORD_TYPES is a dict with only one type...
        # TODO: fix this...once dlkit is fixed
        # canonical = QUESTION_RECORD_TYPES
        canonical = [
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Match Ortho Faces"
                },
                "description": {
                    "text": "Match the manipulatable object to the given faces."
                },
                "id": "question%3Amatch-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Define Ortho Faces"
                },
                "description": {
                    "text": "Define the best plane, top, and side faces of the manipulatable object."
                },
                "id": "question%3Adefine-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Ortho View Set"
                },
                "description": {
                    "text": "From the choices provided, select the ortho view set that matches the given manipulatable."
                },
                "id": "question%3Achoose-viewset%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Manipulatable"
                },
                "description": {
                    "text": "From the choices provided, select the manipulatable that matches the given ortho view set."
                },
                "id": "question%3Achoose-manip%40ODL.MIT.EDU"
            },
            {
                'displayName'       : {
                    'text'  : 'edX Multiple Choice'
                },
                'description'       : {
                    'text'  : 'Multiple Choice question for edX.'
                },
                'id'                : 'question%3Amulti-choice-edx%40ODL.MIT.EDU',
                'recordType'        : 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
            }
        ]
        canonical_display_names = []
        canonical_descriptions = []
        canonical_ids = []
        canonical_record_types = []
        for item in canonical:
            canonical_display_names.append(item['displayName']['text'])
            canonical_descriptions.append(item['description']['text'])
            canonical_ids.append(item['id'])
            canonical_record_types.append(item['recordType'])

        ind = 0
        for item in types_list:
            self.assertEqual(
                item['displayName']['text'],
                canonical_display_names[ind]
            )
            self.assertEqual(
                item['description']['text'],
                canonical_descriptions[ind]
            )
            self.assertEqual(
                item['id'],
                canonical_ids[ind]
            )
            self.assertEqual(
                item['recordType'],
                canonical_record_types[ind]
            )
            ind += 1

    def verify_view_set(self, _req, _data):
        """
        verify that file Ids are correct for the different viewsets
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data'][0]
        if 'question' in item:
            item = item['question']
        for key, value in _data.iteritems():
            self.assertEqual(
                item['orthoViewSet'][key],
                value
            )

    def wrong_taken_post_url(self, _req):
        """
        Can only create a taken from the /offering/ endpoint
        Trying from the /assessments/ endpoint should throw this error
        """
        msg = 'Can only create AssessmentTaken from an AssessmentOffered root URL.'
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "' + msg + '"}'
        )

    def test_unauthenticated_user_request_returns_unauthorized(self):
        """
        An unauthenticated request should respond with error
        """
        req = self.client.get(self.endpoint + 'assessment/')
        self.unauthorized(req)

    def test_unauthenticated_api_request_returns_unauthorized(self):
        """
        User should get a bad signature JSON if their signature doesn't match
        """
        fake_private = 'afakeprivateKey!23'
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=fake_private,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

    def test_incomplete_request_signature(self):
        """
        Make sure that all of the following headers are required:
        ['request-line','accept','date','host','x-api-proxy']
        """
        partial_headers = ['request-line','accept','date','host']
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=partial_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

    def test_authenticated_instructor_assessment_delete(self):
        """
        DELETE should do nothing here. Error code 405.
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.delete(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

    def test_authenticated_learner_assessment_delete(self):
        """
        User should get same error as above--method not allowed. Error code 405
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.delete(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'Service')

    def test_authenticated_learner_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        For someone who is just a department officer, this should also return
        the results (officers can lookup)
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'Service')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_post(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.post(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

    def test_authenticated_learner_assessment_post(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.post(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_assessment_put(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.put(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

    def test_authenticated_learner_assessment_put(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        test_endpoint = self.endpoint + 'assessment/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.put(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_bank_delete(self):
        """
        DELETE should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.delete(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

    def test_authenticated_learner_bank_delete(self):
        """
        DELETE should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.delete(test_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')
        self.convert_user_to_instructor()

    def test_authenticated_instructor_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank_id = self.create_test_bank(auth, 'atestbank', 'for testing')
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.
        delete_endpoint = test_endpoint + bank_id + '/'
        self.delete_item(auth, delete_endpoint)

    def test_authenticated_learner_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank_id = self.create_test_bank(auth, 'atestbank', 'for testing')
        self.convert_user_to_learner()
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.

        self.convert_user_to_instructor()
        delete_endpoint = test_endpoint + bank_id + '/'
        self.delete_item(auth, delete_endpoint)

    def test_authenticated_instructor_bank_post_and_crud(self):
        """
        User can create a new assessment bank. Need to do self-cleanup,
        because the Mongo backend is not part of the test database...that
        means Django will not wipe it clean after every test!
        Once a bank is created, user can GET it, PUT to update it, and
        DELETE it. POST should return an error code 405.
        Do these bank detail tests here, because we have a known
        bank ID
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        name = 'atestbank'
        desc = 'for testing purposes only'
        item_id = self.create_test_bank(auth, name, desc)

        # verify that the bank now appears in the bank_details call
        bank_endpoint = self.endpoint + 'assessment/banks/' + item_id + '/'
        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # Department Officer cannot update banks
        self.convert_user_to_learner()
        new_name = 'a new bank name'
        payload = {
            "name": new_name
        }
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # DepartmentAdmin should be able to edit the bank
        self.convert_user_to_instructor()
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, desc)

        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, desc)
        self.verify_links(req, 'Bank')

        # change user to a "bank" learner, and
        # verify that you cannot update the bank's description
        self.convert_user_to_learner()
        new_desc = 'a new bank description'
        payload = {
            "description": new_desc
        }
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # check that a bank instructor can
        self.convert_user_to_instructor()
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, new_desc)

        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, new_desc)
        self.verify_links(req, 'Bank')

        # verify that POST returns error message / code 405
        req = self.client.post(bank_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        # now grab the id and delete it, to clean up the database
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_bank_post_and_crud(self):
        """
        Learners can do nothing with POST and CRuD
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_learner()

        # verify that the bank now appears in the bank_details call
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that you cannot update the bank's name
        new_name = 'a new bank name'
        payload = {
            "name": new_name
        }
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that you cannot update the bank's description
        new_desc = 'a new bank description'
        payload = {
            "description": new_desc
        }
        req = self.client.put(bank_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        req = self.client.get(bank_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req, 'Bank', name, desc)
        self.verify_links(req, 'Bank')

        # verify that POST returns error message / code 405
        req = self.client.post(bank_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        self.convert_user_to_instructor()
        # now grab the id and delete it, to clean up the database
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_bank_put(self):
        """
        PUT should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.put(test_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'PUT')

    def test_authenticated_learner_bank_put(self):
        """
        PUT should do nothing on this page. Should get an error code 405
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        self.convert_user_to_learner()
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.put(test_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'PUT')

    def test_authenticated_instructor_assessments_crud(self):
        """
        Create a test bank and test all things associated with assessments
        and a single assessment
        DELETE on root assessments/ does nothing. Error code 405.
        GET on root assessments/ gets you a list
        POST on root assessments/ creates a new assessment
        PUT on root assessments/ does nothing. Error code 405.

        For a single assessment detail:
        DELETE will delete that assessment
        GET brings up the assessment details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'

        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessment_endpoint = bank_endpoint + 'assessments/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET should be unauthorized for a learner
        self.convert_user_to_bank_learner(bank_id)
        req = self.client.get(assessment_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an assessment--Learner cannot create
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessment_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # Use POST to create an assessment--Instructor can
        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.post(assessment_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        req = self.client.get(assessment_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Now test PUT / GET / POST / DELETE on the new assessment item
        # POST does nothing
        assessment_detail_endpoint = assessment_endpoint + assessment_id + '/'
        req = self.client.post(assessment_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link. Allowed for Instructor
        req = self.client.get(assessment_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        # PUT not allowed with Learner
        self.convert_user_to_bank_learner(bank_id)
        new_assessment_name = 'a new assessment name'
        new_assessment_desc = 'to trick students'
        payload = {
            "name": new_assessment_name
        }
        req = self.client.put(assessment_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # PUT modifies the assessment, with the changes reflected in GET.
        # Allowed for Instructor
        self.convert_user_to_bank_instructor(bank_id)

        req = self.client.put(assessment_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         assessment_desc)

        req = self.client.get(assessment_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        payload = {
            "description": new_assessment_desc
        }
        req = self.client.put(assessment_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)

        req = self.client.get(assessment_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

        # trying to delete the bank as Learner / Department Officer
        # should throw unauthorized exception
        self.convert_user_to_learner()
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with assessments should throw an error
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessments_crud(self):
        """
        Create a test bank and test all things associated with assessments
        and a single assessment
        DELETE on root assessments/ does nothing. Error code 405.
        GET on root assessments/ gets you a list
        POST on root assessments/ creates a new assessment
        PUT on root assessments/ does nothing. Error code 405.

        For a single assessment detail:
        DELETE will delete that assessment
        GET brings up the assessment details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description

        ** User should be a DepartmentAdmin but a Learner for the bank
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessment_endpoint = bank_endpoint + 'assessments/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET should be unauthorized for learner
        req = self.client.get(assessment_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an assessment--it should throw a 403 for a learner
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessment_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # trying to delete the bank should be okay, because user is also
        # DepartmentAdmin
        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_items_crud(self):
        """
        Create a test bank and test all things associated with items
        and a single item
        DELETE on root items/ does nothing. Error code 405.
        GET on root items/ gets you a list
        POST on root items/ creates a new item
        PUT on root items/ does nothing. Error code 405.

        For a single item detail:
        DELETE will delete that item
        GET brings up the item details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET for a Learner is unauthorized
        self.convert_user_to_bank_learner(bank_id)
        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        req = self.client.post(item_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')

        # GET should still not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # PUT should not work for a Learner, throw unauthorized
        new_item_name = 'a new item name'
        new_item_desc = 'to trick students'
        payload = {
            "name": new_item_name
        }
        req = self.client.put(item_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # PUT should work now. Modifies the item, with the changes reflected in GET
        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.put(item_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         item_desc)

        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')

        payload = {
            "description": new_item_desc
        }
        req = self.client.put(item_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)

        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)
        self.verify_links(req, 'ItemDetails')

        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_items_crud(self):
        """
        Create a test bank and test all things associated with items
        and a single item
        DELETE on root items/ does nothing. Error code 405.
        GET on root items/ gets you a list
        POST on root items/ creates a new item
        PUT on root items/ does nothing. Error code 405.

        For a single item detail:
        DELETE will delete that item
        GET brings up the item details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET should return all items for this bank--currently none
        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an item--it should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # trying to delete the bank should be okay, because user is
        # also DepartmentAdmin
        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_question_string_item_crud(self):
        """
        Test ability for user to POST a new question string and
        response string item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)
        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        # Learners cannot GET?
        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        req = self.client.post(item_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        # GET of an item should not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # PUT should work for Instructor.
        # Can modify the question and answers, reflected in GET
        new_question_string = 'what day is it?'
        new_answer_string = 'Saturday'

        payload = {
            'question': {
                'id' : question_id,
                'questionString': new_question_string,
                'type': question_type
            }
        }
        self.convert_user_to_bank_instructor(bank_id)

        req = self.client.put(item_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        payload = {
            'answers': [{
                'id' : answer_id,
                'responseString': new_answer_string,
                'type': answer_type
            }]
        }
        req = self.client.put(item_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])

        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

        # Verify that GET, PUT to question/ endpoint works
        item_question_endpoint = item_detail_endpoint + 'question/'
        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(item_question_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # POST to this root url also returns a 405
        req = self.client.post(item_question_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req, new_question_string, question_type)

        newer_question_string = 'yet another new question?'
        payload = {
            "id"             : question_id,
            "questionString" : newer_question_string,
            "type"           : question_type
        }
        req = self.client.put(item_question_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req, newer_question_string, question_type)

        # Verify that GET, POST (answers/) and
        # GET, DELETE, PUT to answers/<id> endpoint work
        # Verify that invalid answer_id returns "Answer not found."
        item_answers_endpoint = item_detail_endpoint + 'answers/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(item_answers_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(item_answers_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        req = self.client.get(item_answers_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req, [new_answer_string], [answer_type])

        second_answer_string = "a second answer"
        payload = [{
            "responseString"    : second_answer_string,
            "type"              : answer_type
        }]
        req = self.client.post(item_answers_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_answers(req,
                            [new_answer_string, second_answer_string],
                            [answer_type, answer_type])

        req = self.client.get(item_answers_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req,
                            [new_answer_string, second_answer_string],
                            [answer_type, answer_type])

        fake_item_answer_detail_endpoint = item_answers_endpoint + 'fakeid/'
        req = self.client.get(fake_item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.answer_not_found(req)

        item_answer_detail_endpoint = item_answers_endpoint + unquote(answer_id) + '/'
        # Check that POST returns error code 405--we don't support this
        req = self.client.post(item_answer_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req, [new_answer_string], [answer_type])

        newer_answer_string = 'yes, another one'
        payload = {
            "responseString"    : newer_answer_string,
            "type"              : answer_type
        }
        req = self.client.put(item_answer_detail_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

        req = self.client.get(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

        req = self.client.delete(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        req = self.client.get(item_answers_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req,
                            [second_answer_string],
                            [answer_type])
        self.verify_data_length(req, 4)


        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_question_string_item_crud(self):
        """
        Learner should not be able to create a new item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)
        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.unauthorized(req)

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_item_types_get(self):
        """
        Should return a list of item types supported by the service
        """
        test_endpoint = self.endpoint + 'assessment/types/items/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_types(req)

    def test_authenticated_instructor_link_items_to_assessment(self):
        """
        Test link, view, delete of items to assessments
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        items_endpoint = bank_endpoint + 'items/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        req = self.client.get(assessments_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint,
                              auth=auth,
                              data=json.dumps(payload),
                              headers=self.json_headers)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')

        # Now start working on the assessment/items endpoint, to test
        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        req = self.client.get(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'AssessmentItems')

        # should now appear in the Assessment Items List
        req = self.client.get(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'AssessmentItems')

        assessment_item_details_endpoint = assessment_items_endpoint + item_id + '/'

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'AssessmentItemDetails')

        # Trying to delete the item now
        # should raise an error--cannot delete an item that is
        # assigned to an assignment!
        req = self.client.delete(item_detail_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        req = self.client.delete(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        req = self.client.get(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_link_items_to_assessment(self):
        """
        A learner should not be able to link items to assessments
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        items_endpoint = bank_endpoint + 'items/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }

        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        req = self.client.get(assessments_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'

        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')

        # link the item to the assessment
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'AssessmentItems')

        # Now verify that learners cannot see this!
        self.convert_user_to_bank_learner(bank_id)

        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET and POST should be unauthorized
        req = self.client.get(assessment_items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.unauthorized(req)

        assessment_item_details_endpoint = assessment_items_endpoint + item_id + '/'

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # GET and DELETE for learner should be unauthorized
        req = self.client.get(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        req = self.client.delete(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)

        # delink the item from the assessment
        req = self.client.delete(assessment_item_details_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_bad_public_key_returns_unknown_user(self):
        """
        User should get a bad signature JSON if their public key is not found
        """
        fake_public = 'afakepublicKey!23'
        test_endpoint = self.endpoint + 'assessment/banks/'
        auth = HTTPSignatureAuth(key_id=fake_public,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sup_headers = self.headers
        sup_headers['X-Api-Key'] = fake_public
        req = self.client.get(test_endpoint, auth=auth, headers=self.headers)
        self.user_not_found(req)

    def test_authenticated_instructor_assessment_offered_crud(self):
        """
        Instructors should be able to add assessment offered.
        Cannot create offered unless an assessment has items

        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        # PUT and DELETE should not work on this endpoint
        req = self.client.put(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        req = self.client.delete(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # GET should return an empty list
        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')

        # Use POST to create an offering
        # Inputting something other than a list or dict object should result in an error
        bad_payload = 'this is a bad input format'
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(bad_payload),
                               auth=auth,
                               headers=self.json_headers)
        self.bad_input(req)

        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        # POST at this point should throw an exception -- no items in the assessment
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.need_items(req)

        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])

        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        # Now POST to offerings should work
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']
        payload.update({
            'id'    : quote_safe_offering_id
        })
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])
        self.verify_links(req, 'AssessmentOffering')

        # For the offering detail endpoint, GET, PUT, DELETE should work
        # Check that POST returns error code 405--we don't support this
        req = self.client.post(assessment_offering_detail_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(assessment_offering_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffering', [payload])
        self.verify_links(req, 'AssessmentOfferingDetails')

        # PUT to the offering URL should modify the start time or duration
        new_start_time = {
            "startTime" : {
                "day"   : 1,
                "month" : 2,
                "year"  : 2015
            }
        }
        expected_payload = new_start_time
        expected_payload.update({
            "duration"  : {
                "days"  : 2
            },
            "id"        : quote_safe_offering_id
        })

        req = self.client.put(assessment_offering_detail_endpoint,
                               data=json.dumps(new_start_time),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', [expected_payload])

        # PUT with a list of length 1 should also work
        new_duration = [{
            "duration"  : {
                "days"      : 5,
                "minutes"   : 120
            }
        }]
        expected_payload = new_duration
        expected_payload[0].update(new_start_time)
        expected_payload[0].update({
            "id"    : quote_safe_offering_id
        })
        req = self.client.put(assessment_offering_detail_endpoint,
                               data=json.dumps(new_duration),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', expected_payload)

        funny_payload = {
            "duration"  : {
                "hours" :   2
            }
        }
        expected_converted_payload = funny_payload
        expected_converted_payload.update(new_start_time)
        expected_converted_payload.update({
            "id"    : quote_safe_offering_id
        })
        req = self.client.put(assessment_offering_detail_endpoint,
                               data=json.dumps(funny_payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffering', expected_converted_payload)

        # check that the attributes changed in GET
        req = self.client.get(assessment_offering_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', expected_converted_payload)

        # Delete the offering now
        req = self.client.delete(assessment_offering_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')
        self.verify_no_data(req)

        # test that you can POST / create multiple offerings with a list
        bad_payload = 'this is a bad input format'
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(bad_payload),
                               auth=auth,
                               headers=self.json_headers)
        self.bad_input(req)

        payload = [{
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
                   },{
            "startTime" : {
                "day"   : 9,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 20
            }
        }]
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering1_id = unquote(offering['results'][0]['id'])
        offering2_id = unquote(offering['results'][1]['id'])

        payload[0].update({
            'id'    : offering1_id
        })
        payload[1].update({
            'id'    : offering2_id
        })

        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_offerings(req,
                             'AssessmentOffering',
                             payload)
        self.verify_links(req, 'AssessmentOffering')

        offering1_endpoint = bank_endpoint + 'assessmentsoffered/' + offering1_id + '/'
        offering2_endpoint = bank_endpoint + 'assessmentsoffered/' + offering2_id + '/'

        item_endpoint = items_endpoint + item_id + '/'

        self.delete_item(auth, offering1_endpoint)
        self.delete_item(auth, offering2_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessment_offered_crud(self):
        """
        Learners should be able to see and do nothing with assessments offered
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'

        self.convert_user_to_bank_instructor(bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'

        # Use POST to create an offering
        # Inputting something other than a list or dict object should result in an error
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        # POST at this point should throw an exception -- no items in the assessment
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.need_items(req)

        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])

        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should also work and create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']
        payload.update({
            'id'    : quote_safe_offering_id
        })
        self.verify_offerings(req,
                             'AssessmentOffering',
                             [payload])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # check that Learner can get to none of these endpoints
        # For the offering detail endpoint, GET, PUT, DELETE should be unauthorized
        # Check that POST returns error code 405--we don't support this
        self.convert_user_to_bank_learner(bank_id)

        req = self.client.post(assessment_offering_detail_endpoint,
                               auth=auth,
                               headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # PUT to the offering URL should modify the start time or duration
        new_start_time = {
            "startTime" : {
                "day"   : 1,
                "month" : 2,
                "year"  : 2015
            }
        }
        req = self.client.put(assessment_offering_detail_endpoint,
                              data=json.dumps(new_start_time),
                              auth=auth,
                              headers=self.json_headers)
        self.unauthorized(req)

        req = self.client.delete(assessment_offering_detail_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # PUT and DELETE should not work on this endpoint
        req = self.client.put(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        req = self.client.delete(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # GET should be unauthorized
        req = self.client.get(assessment_offering_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        # check that cannot delete an assessment with offerings assigned
        req = self.client.delete(assessment_detail_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_ortho_3d_crud(self):
        """
        Test an instructor can upload a manipulatable AssetBundle file
        and 3 images as an ortho viewset to create a new item
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'

        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)  # because this has files, leave it to form-encoded?
        self.unauthorized(req)

        # Learners cannot GET?
        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']

        expected_files = self.extract_question(item)['files']
        expected_answer = payload['answers'][0]['integerValues']

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        # Now test PUT / GET / POST / DELETE on the new item
        # POST does nothing
        item_detail_endpoint = items_endpoint + item_id + '/'
        req = self.client.post(item_detail_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # GET displays it, with self link
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

        # can download the file with the /files/ endpoint...assume the zip file
        # contains the manipulatable
        item_files_endpoint = item_detail_endpoint + 'files/'
        req = self.client.get(item_files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_links(req, 'ItemFiles')

        downloadables = ['manip','front','side','top','all']
        for downloadable in downloadables:
            endpoint = item_files_endpoint + downloadable + '/'
            req = self.client.get(endpoint, auth=auth, headers=self.headers)
            self.ok(req)

        # GET of item files should not work for Learner
        self.convert_user_to_bank_learner(bank_id)
        req = self.client.get(item_files_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # GET of an item should not work for Learner
        req = self.client.get(item_detail_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)

        # PUT should work for Instructor.
        self.convert_user_to_bank_instructor(bank_id)
        # Can modify the question files, reflected in GET
        new_manip_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/image001.jpg'
        new_manip = open(new_manip_path, 'rb')

        item_question_endpoint = item_detail_endpoint + 'question/'
        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(item_question_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # POST to this root url also returns a 405
        req = self.client.post(item_question_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        payload = {
            "manip" : new_manip
        }
        req = self.client.put(item_question_endpoint,
                               files=payload,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        expected_files_new_manip = deepcopy(expected_files)
        new_manip.seek(0)
        expected_files_new_manip['manip'] = base64.b64encode(new_manip.read())
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files_new_manip)

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')  # Item questions do not have links, for now
        self.verify_ortho_files(req, expected_files_new_manip)

        # Now check that updating the view set works
        new_front_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/front2.jpg'
        new_front = open(new_front_path, 'rb')

        payload = {
            "frontView" : new_front
        }

        req = self.client.put(item_question_endpoint,
                               files=payload,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        new_expected_files = self.extract_question(json.loads(req.content))['files']
        self.assertNotEqual(
            expected_files_new_manip['frontView'],
            new_expected_files['frontView']
        )
        self.assertEqual(
            expected_files_new_manip['sideView'],
            new_expected_files['sideView']
        )
        self.assertEqual(
            expected_files_new_manip['topView'],
            new_expected_files['topView']
        )
        self.assertEqual(
            expected_files_new_manip['manip'],
            new_expected_files['manip']
        )
        self.verify_ortho_files(req, new_expected_files)

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new_expected_files)

        new_side_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/side2.jpg'
        new_side = open(new_side_path, 'rb')

        payload = {
            "sideView" : new_side
        }

        req = self.client.put(item_question_endpoint,
                               files=payload,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        new2_expected_files = self.extract_question(json.loads(req.content))['files']
        self.assertNotEqual(
            new_expected_files['sideView'],
            new2_expected_files['sideView']
        )
        self.assertEqual(
            new_expected_files['frontView'],
            new2_expected_files['frontView']
        )
        self.assertEqual(
            new_expected_files['topView'],
            new2_expected_files['topView']
        )
        self.assertEqual(
            new_expected_files['manip'],
            new2_expected_files['manip']
        )
        self.verify_ortho_files(req, new2_expected_files)

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new2_expected_files)

        new_top_path = '/Users/cjshaw/Documents/Projects/AssessmentService/assessments/assessments/tests/top2.jpg'
        new_top = open(new_top_path, 'rb')

        payload = {
            "topView" : new_top
        }

        req = self.client.put(item_question_endpoint,
                               files=payload,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        new3_expected_files = self.extract_question(json.loads(req.content))['files']
        self.assertNotEqual(
            new2_expected_files['topView'],
            new3_expected_files['topView']
        )
        self.assertEqual(
            new2_expected_files['frontView'],
            new3_expected_files['frontView']
        )
        self.assertEqual(
            new2_expected_files['sideView'],
            new3_expected_files['sideView']
        )
        self.assertEqual(
            new2_expected_files['manip'],
            new3_expected_files['manip']
        )
        self.verify_ortho_files(req, new3_expected_files)

        req = self.client.get(item_question_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        # self.verify_links(req, 'ItemDetails')
        self.verify_ortho_files(req, new3_expected_files)

        # ITEM ANSWERS endpoint
        # Verify that GET, POST (answers/) and
        # GET, DELETE, PUT to answers/<id> endpoint work
        # Verify that invalid answer_id returns "Answer not found."
        item_answers_endpoint = item_detail_endpoint + 'answers/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(item_answers_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(item_answers_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        item_answer_detail_endpoint = item_answers_endpoint + unquote(answer_id) + '/'
        # Check that POST returns error code 405--we don't support this
        req = self.client.post(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])

        new_response_set = {
            "frontFaceValue" : 4,
            "sideFaceValue"  : 5,
            "topFaceValue"   : 6
        }
        payload = {
            'integerValues': new_response_set
        }
        req = self.client.put(item_answer_detail_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])
        self.verify_ortho_answers(req, new_response_set)

        req = self.client.get(item_answer_detail_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string],
                            [answer_type])
        self.verify_ortho_answers(req, new_response_set)

        # trying to delete the bank as a DepartmentOfficer should
        # throw an exception
        self.convert_user_to_learner()
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.unauthorized(req)
        self.convert_user_to_instructor()

        # trying to delete the bank with items should throw an error
        req = self.client.delete(bank_endpoint, auth=auth, headers=self.headers)
        self.not_empty(req)

        self.delete_item(auth, item_detail_endpoint)

        # Create a new item with an incomplete viewset
        # should thrown an error
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }

        self.manip.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        payload_missing_front_files = {
            'manip'     : self.manip,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        payload_missing_front = deepcopy(payload)
        payload_missing_front['question'] = json.dumps(payload['question'])
        payload_missing_front['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint,
                               data=payload_missing_front,
                               files=payload_missing_front_files,
                               auth=auth,
                               headers=self.headers)
        self.missing_orthoview(req)

        self.manip.seek(0)
        self.side.seek(0)
        self.front.seek(0)
        payload_missing_top_files = {
            'manip'     : self.manip,
            'sideView'  : self.side,
            'frontView' : self.front
        }
        payload_missing_top = deepcopy(payload)
        payload_missing_top['question'] = json.dumps(payload['question'])
        payload_missing_top['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=payload_missing_top,
                               files=payload_missing_top_files,
                               auth=auth,
                               headers=self.headers)
        self.missing_orthoview(req)


        self.manip.seek(0)
        self.top.seek(0)
        self.front.seek(0)
        payload_missing_side_files = {
            'manip'     : self.manip,
            'topView'   : self.top,
            'frontView' : self.front
        }
        payload_missing_side = deepcopy(payload)
        payload_missing_side['question'] = json.dumps(payload['question'])
        payload_missing_side['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=payload_missing_side,
                               files=payload_missing_side_files,
                               auth=auth,
                               headers=self.headers)
        self.missing_orthoview(req)

        self.delete_item(auth, bank_endpoint)

    def test_lti_request_signature(self):
        """
        Make sure that lti headers are accepted:
        ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role']
        """
        lti_headers = ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']
        test_endpoint = self.endpoint + 'assessment/'

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = '80'
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'None'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        req = self.client.get(test_endpoint, auth=auth, headers=modified_headers)
        self.ok(req)

    def test_authenticated_instructor_assessment_taking(self):
        """
        POSTing to takens should create a new one.
        Should only be able to POST from the /assessmentsoffered/ endpoint
        But can GET from the /assessments/ endpoint, too.

        Without submitting a response, POSTing again should
        return the same one.

         -- Check that for Ortho3D types, a taken has files.

        Submitting and then POSTing should return a new
        taken.

        This should work for both instructors and learners
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               files=files,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        expected_files = self.extract_question(item)['files']

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can GET. Cannot POST. PUT and DELETE not supported
        assessment_takens_endpoint = assessment_detail_endpoint + 'assessmentstaken/'
        req = self.client.get(assessment_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        req = self.client.post(assessment_takens_endpoint, auth=auth, headers=self.headers)
        self.wrong_taken_post_url(req)

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_takens_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_takens_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # Can GET and POST. PUT and DELETE not supported
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.get(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can GET the taken details (learner cannot).
        # POST, PUT, DELETE not supported
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
        req = self.client.get(taken_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # PUT to this root url also returns a 405
        req = self.client.put(taken_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(taken_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken_copy = json.loads(req.content)
        taken_copy_id = unquote(taken_copy['id'])
        self.assertEqual(taken_id, taken_copy_id)

        # Only GET of this endpoint is supported
        take_endpoint = taken_endpoint + 'take/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(take_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(take_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(take_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(take_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        question = json.loads(req.content)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        files_endpoint = taken_endpoint + 'files/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        # Can submit a wrong response
        submit_endpoint = taken_endpoint + 'submit/'
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        req = self.client.get(submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'GET')

        req = self.client.post(submit_endpoint,
                               data=json.dumps(wrong_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

        # to submit again, have to get a new taken
        # Now getting another taken will return a new one
        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        new_taken = json.loads(req.content)
        new_taken_id = unquote(new_taken['id'])
        self.assertNotEqual(taken_id, new_taken_id)

        new_taken_endpoint = bank_endpoint + 'assessmentstaken/' + new_taken_id + '/'
        new_submit_endpoint = new_taken_endpoint + 'submit/'
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        req = self.client.post(new_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

        # Trying to retake / reGET the files or taken or resubmit
        # should throw an exception.
        req = self.client.post(new_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.already_taken(req)

        req = self.client.get(take_endpoint, auth=auth, headers=self.headers)
        self.already_taken(req)

        req = self.client.get(files_endpoint, auth=auth, headers=self.headers)
        self.already_taken(req)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, new_taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_assessment_taking(self):
        """
        POSTing to takens should create a new one.
        Should only be able to POST from the /assessmentsoffered/ endpoint
        But can GET from the /assessments/ endpoint, too.

        Without submitting a response, POSTing again should
        return the same one.

         -- Check that for Ortho3D types, a taken has files.

        Submitting and then POSTing should return a new
        taken.

        This should work for both instructors and learners
         -- TODO: Duplicate this for learners
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        expected_files = self.extract_question(item)['files']

        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # From here, a learner should be able to do everything
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken_copy = json.loads(req.content)
        taken_copy_id = unquote(taken_copy['id'])
        self.assertEqual(taken_id, taken_copy_id)

        # Only GET of this endpoint is supported
        take_endpoint = taken_endpoint + 'take/'
        req = self.client.get(take_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        question = json.loads(req.content)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_files(req, expected_files)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        files_endpoint = taken_endpoint + 'files/'

        req = self.client.get(files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        # Can submit a wrong response
        submit_endpoint = taken_endpoint + 'submit/'
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        req = self.client.post(submit_endpoint,
                               data=json.dumps(wrong_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

        # to submit again, have to get a new taken
        # Now getting another taken will return a new one
        # POSTing to assessment_offering_takens_endpoint
        # returns the same taken
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        new_taken = json.loads(req.content)
        new_taken_id = unquote(new_taken['id'])
        self.assertNotEqual(taken_id, new_taken_id)

        new_taken_endpoint = bank_endpoint + 'assessmentstaken/' + new_taken_id + '/'
        new_take_endpoint = new_taken_endpoint + 'take/'
        new_files_endpoint = new_taken_endpoint + 'files/'
        new_submit_endpoint = new_taken_endpoint + 'submit/'
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        req = self.client.post(new_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

        # Trying to retake / reGET the files or taken or resubmit
        # should throw an exception.
        req = self.client.post(new_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.already_taken(req)

        req = self.client.get(new_take_endpoint, auth=auth, headers=self.headers)
        self.already_taken(req)

        req = self.client.get(new_files_endpoint, auth=auth, headers=self.headers)
        self.already_taken(req)

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, new_taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_endpoint = items_endpoint + item_id + '/'
        self.delete_item(auth, item_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }

        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)


        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE on the status endpoint are not supported
        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this url also returns a 405
        req = self.client.put(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this url also returns a 405
        req = self.client.post(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        req = self.client.get(question_2_files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        req = self.client.get(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'GET')

        req = self.client.post(question_1_submit_endpoint,
                               data=json.dumps(wrong_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded, incorrect
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        req = self.client.post(question_1_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, correct
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        req = self.client.get(question_2_status_endpoint, auth=auth, headers=self.headers)
        self.not_responded(req)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        req = self.client.post(question_2_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_learner_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken
        Same as above, but for a learner
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        req = self.client.get(items_endpoint, auth=auth, headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, False)
        self.verify_question_status(question_2, False)


        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(question_1_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        req = self.client.post(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE on the status endpoint are not supported
        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this url also returns a 405
        req = self.client.put(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # POST to this url also returns a 405
        req = self.client.post(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'POST')

        req = self.client.get(question_1_files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        req = self.client.get(question_2_files_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        req = self.client.get(question_1_endpoint, auth=auth, headers=self.headers)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        req = self.client.get(question_1_submit_endpoint, auth=auth, headers=self.headers)
        self.not_allowed(req, 'GET')

        req = self.client.post(question_1_submit_endpoint,
                               data=json.dumps(wrong_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded, incorrect
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.responded(req, False)

        # checking on the question status now should return a responded but wrong
        req = self.client.get(question_1_endpoint, auth=auth, headers=self.headers)
        question = json.loads(req.content)
        self.verify_question_status(question, True, False)

        req = self.client.get(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, False)
        self.verify_question_status(question_2, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        req = self.client.post(question_1_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, correct
        req = self.client.get(question_1_status_endpoint, auth=auth, headers=self.headers)
        self.responded(req, True)

        # checking on the question status now should return a responded right
        req = self.client.get(question_1_endpoint, auth=auth, headers=self.headers)
        question = json.loads(req.content)
        self.verify_question_status(question, True, True)

        req = self.client.get(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, True)
        self.verify_question_status(question_2, False)

        # checking on the second question status now should return a not-responded
        req = self.client.get(question_2_status_endpoint, auth=auth, headers=self.headers)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        req = self.client.get(question_2_endpoint, auth=auth, headers=self.headers)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        req = self.client.post(question_2_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # convert back to instructor to do clean-up
        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_learner_form_posting(self):
        """
        All POST endpoints should accept multipart/form-data as well
        as just JSON.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc, True)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        payload_files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=payload_files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue" : 5,
                    "topFaceValue"  : 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        payload_files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=payload_files)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        expected_files_2 = self.extract_question(item)['files']


        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=payload,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id, item_2_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=payload,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)

        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        payload_stringified = {
            'startTime' : json.dumps(payload['startTime']),
            'duration'  : json.dumps(payload['duration'])
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=payload_stringified,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        req = self.client.post(assessment_offering_takens_endpoint,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        req = self.client.get(taken_questions_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]
        question_2 = questions['data']['results'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)


        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        req = self.client.get(question_1_files_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        req = self.client.get(question_2_files_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        req = self.client.get(question_1_status_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        req = self.client.delete(question_1_submit_endpoint,
                                 auth=auth,
                                 headers=self.headers)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        req = self.client.put(question_1_submit_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        req = self.client.get(question_1_submit_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_allowed(req, 'GET')

        wrong_response_stringified = {
            'integerValues' : json.dumps(wrong_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint,
                               data=wrong_response_stringified,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        req = self.client.get(question_1_status_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }
        right_response_stringified = {
            'integerValues' : json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint,
                               data=right_response_stringified,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responsded, right
        req = self.client.get(question_1_status_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        req = self.client.get(question_2_status_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.not_responded(req)


        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue" : 4,
                "sideFaceValue"  : 5,
                "topFaceValue"   : 0
            }
        }
        right_response_stringified = {
            'integerValues' : json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_2_submit_endpoint,
                               data=right_response_stringified,
                               auth=auth,
                               headers=self.headers,
                               files={})
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # convert back to instructor to do clean-up
        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, item_2_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_items_first_angle(self):
        """
        Test that can set the firstAngle attribute of a question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        payload_files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=payload_files)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, False)

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')
        self.verify_first_angle(req, False)

        # Now try to update the firstAngle
        item_detail_endpoint = items_endpoint + item_id + '/'
        payload = {
            'question' : {
                'firstAngle'    : True
            }
        }
        req = self.client.put(item_detail_endpoint,
                              data=json.dumps(payload),
                              auth=auth,
                              headers=self.json_headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, True)

        req = self.client.get(item_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')
        self.verify_first_angle(req, True)

        self.delete_item(auth, item_detail_endpoint)

        # now test that you can create an item initially with the firstAngle set
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"          : question_type,
                "questionString": question_string,
                "firstAngle"    : True
            },
            "answers"   : [{
                "type"          : answer_type,
                "integerValues" : {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        payload_files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=payload_files)
        self.ok(req)
        self.verify_first_angle(req, True)

        item_id = unquote(json.loads(req.content)['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'

        req = self.client.get(item_detail_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_first_angle(req, True)

        self.delete_item(auth, item_detail_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_ortho_3d_multi_choice_crud(self):
        """
        Test an instructor can upload a manipulatable AssetBundle file
        and multiple 2-image ortho choice sets to create a multiple
        choice question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "choiceId": 1
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        files = {
            'manip'  : self.manip
        }
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        self.manip.seek(0)
        # this should throw an exception -- need to include some choices!
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)
        self.include_choices(req)

        # even with one choice set, should throw an exception
        files['choice0small'] = self.choice0sm
        files['choice0big'] = self.choice0big
        self.manip.seek(0)
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)
        self.include_choices(req)

        # should be okay with two choices
        files['choice1small'] = self.choice1sm
        files['choice1big'] = self.choice1big
        self.manip.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]

        expected_choices = self.extract_question(item)['choices']
        expected_answer = [self.extract_answers(item)[0]['choiceIds'][0]]

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                     question_string,
                                     question_type,
                                     [answer_string],
                                     [answer_type])
        self.verify_ortho_multi_choice_answers(req, expected_answer)
        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip':self.encode(self.manip)})

        item_details_endpoint = items_endpoint + item_id + '/'

        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint,
                               data=json.dumps({}),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        # check that the questions are present in the taken
        req = self.client.get(taken_questions_endpoint, auth=auth, headers=self.headers)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip':self.encode(self.manip)})

        # Submitting a non-list response is not good, even if it is right
        bad_response = {
            'choiceIds': expected_answer[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(bad_response),
                               auth=auth,
                               headers=self.json_headers)
        self.bad_multi_choice_response(req)

        # Now can submit a response to this endpoint
        response = {
            'choiceIds': [expected_answer[0]]
        }
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # Should also be able to update items with multiple answers!
        # POST to the item_answers_endpoint
        item_answers_endpoint = item_details_endpoint + 'answers/'
        payload = {
            'type'     : answer_type,
            'choiceId' : 1
        }
        req = self.client.post(item_answers_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        all_answers = json.loads(req.content)['results']  # should be a list of two objects
        expected_answers = []
        for ans in all_answers:
            expected_answers.append(ans['choiceIds'][0])
        expected_answer_1 = expected_answers[0]
        expected_answer_2 = expected_answers[1]

        req = self.client.get(item_details_endpoint, auth=auth, headers=self.headers)
        self.verify_ortho_multi_choice_answers(req, expected_answers)

        # DELETE the old taken, create a new one so the updated answer
        # is a part of it
        self.delete_item(auth, taken_endpoint)
        req = self.client.post(assessment_offering_takens_endpoint,
                               data=json.dumps({}),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # TODO: submitting a single answer (even if right) will be wrong here
        incomplete_response = {
            'choiceIds': [expected_answer_1]
        }
        taken_questions_endpoint = taken_endpoint + 'questions/'
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(incomplete_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # TODO: submitting both answers is okay
        response = {
            'choiceIds': [expected_answer_1, expected_answer_2]
        }
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # TODO: submitting both answers in the wrong order is okay
        response = {
            'choiceIds': [expected_answer_2, expected_answer_1]
        }
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_can_set_item_genus_type_and_file_names(self):
        """
        When creating a new item, can define a specific genus type
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        item_genus = 'question%3Amatch-ortho-viewset%40ODL.MIT.EDU'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        manip_name = 'A bracket'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "genus"         : item_genus,
            "question"      : {
                "choiceNames"   : ["view 1", "view 2"],
                "genus"         : item_genus,
                "promptName"    : manip_name,
                "type"          : question_type,
                "questionString": question_string
            },
            "answers": [{
                "genus"     : item_genus,
                "type"      : answer_type,
                "choiceId"  : 1
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        files = {}
        files['manip'] = self.manip
        files['choice0small'] = self.choice0sm
        files['choice0big'] = self.choice0big
        files['choice1small'] = self.choice1sm
        files['choice1big'] = self.choice1big

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)

        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         _genus=item_genus)

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id,
                         _genus=item_genus)
        self.verify_links(req, 'Item')
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )

        item_details_endpoint = items_endpoint + item_id + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_missing_parameters(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        params = ['questionString','choices','choiceId']
        for param in params:
            test_payload = deepcopy(payload)
            if param == 'choiceId':
                del test_payload['answers'][0][param]
            else:
                del test_payload['question'][param]
            req = self.client.post(items_endpoint,
                                   data=json.dumps(test_payload),
                                   auth=auth,
                                   headers=self.json_headers)
            self.code(req, 500)
            self.message(req,
                         '"' + param + '" required in input parameters but not provided.',
                         True)

        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_high(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 4
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.code(req, 500)
        self.message(req,
                     'Correct answer 4 is not valid. Not that many choices!',
                     True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_low(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 0
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.code(req, 500)
        self.message(req,
                     'Correct answer 0 is not valid. Must be between 1 and # of choices.',
                     True)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_answer_not_enough_choices(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 1
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.code(req, 500)
        self.message(req,
                     '"choices" is shorter than 2.',
                     True)

        self.delete_item(auth, bank_endpoint)


    def test_authenticated_instructor_edx_multi_choice_with_named_choices(self):
        """
        Test an instructor can create named choices with a dict
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text' : 'I hope so.',
             'name' : 'yes'},
            {'text' : 'I don\'t think I can.',
             'name' : 'no'},
            {'text' : 'Maybe tomorrow.',
             'name' : 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        item = self.load(req)
        item_details_endpoint = items_endpoint + unquote(item['id']) + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_crud(self):
        """
        Test an instructor can create and respond to an edX multiple choice
        type question.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        expected_answers = item['answers'][0]['choiceIds']

        # attach to an assessment -> offering -> taken
        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        # Submitting a non-list response is not good, even if it is right
        bad_response = {
            'choiceIds': expected_answers[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(bad_response),
                               auth=auth,
                               headers=self.json_headers)
        self.bad_multi_choice_response(req)

        # Now can submit a response to this endpoint
        response = {
            'choiceIds': expected_answers
        }
        req = self.client.post(taken_submit_endpoint,
                               data=json.dumps(response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)
        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authenticated_instructor_edx_multi_choice_with_files(self):
        """
        In case instructor wants to upload image files, etc., with the problem
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text' : 'I hope so.',
             'name' : 'yes'},
            {'text' : 'I don\'t think I can.',
             'name' : 'no'},
            {'text' : 'Maybe tomorrow.',
             'name' : 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

        files = {
            'manip' : self.manip
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               files=files,
                               headers=self.headers)
        self.ok(req)
        item = self.load(req)

        self.assertIn('fileIds', item)
        self.assertTrue(len(item['fileIds']) == 1)
        self.assertIn('fileIds', item['question'])
        self.assertTrue(len(item['question']['fileIds']) == 1)
        self.assertIn('files', item['question'])
        self.assertTrue(len(item['question']['files']) == 1)
        self.assertEqual(
            item['fileIds']['manip'],
            item['question']['fileIds']['manip']
        )

        item_details_endpoint = items_endpoint + unquote(item['id']) + '/'

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_authorization_hints_instructor(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        req = self.client.get(authz_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments','assessments_offered','assessments_taken',
                         'assessment_banks','items']
        nested_keys = ['can_create','can_delete','can_lookup','can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                self.assertTrue(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

        self.delete_item(auth, bank_endpoint)


    def test_authorization_hints_learner(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        self.convert_user_to_bank_learner(bank_id)

        req = self.client.get(authz_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments','assessments_offered','assessments_taken',
                         'assessment_banks','items']
        nested_keys = ['can_create','can_delete','can_lookup','can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                if (verb == 'can_lookup' and
                    key == 'assessments_taken'):
                    self.assertTrue(authz)
                elif (verb == 'can_lookup' and
                    key == 'assessment_banks'):
                    self.assertTrue(authz)
                elif (verb == 'can_take'):
                    self.assertTrue(authz)
                elif (verb == 'can_create' and
                      key == 'assessments_taken'):
                    self.assertTrue(authz)
                else:
                    self.assertFalse(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

        self.convert_user_to_bank_instructor(bank_id)
        self.delete_item(auth, bank_endpoint)

    def test_list_pagination(self):
        list_endpoints = ['banks','items','assessments','assessmentsoffered','assessmentstaken']
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        bank_endpoints = []

        for i in range(0, 20):
            name = 'atestbank ' + str(i)
            desc = 'for testing purposes only ' + str(i)
            bank_id = self.create_test_bank(auth, name, desc)
            bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
            bank_endpoints.append(bank_endpoint)

        banks_endpoint = self.endpoint + 'assessment/banks/'

        req = self.client.get(banks_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = self.load(req)

        expected_fields = ['count','next','previous','results']
        for field in expected_fields:
            self.assertIn(
                field,
                data['data']
            )

        self.assertNotEqual(
            data['data']['next'],
            None
        )

        self.assertEqual(
            data['data']['previous'],
            None
        )

        self.assertTrue(data['data']['count'] >= 20)

        self.assertEqual(
            len(data['data']['results']),
            10
        )

        page2 = data['data']['next']
        # total hack
        page2 = page2.replace('http://testserver:80', '')

        req2 = self.client.get(page2,
                               auth=auth,
                               headers=self.headers)
        self.ok(req2)
        data2 = self.load(req2)
        self.assertNotEqual(
            data['data']['results'],
            data2['data']['results']
        )

        self.assertEqual(
            len(data2['data']['results']),
            10
        )

        self.assertTrue(data2['data']['count'] >= 20)

        if data2['data']['count'] > 20:
            self.assertNotEqual(
                data2['data']['next'],
                None
            )
        else:
            self.assertEqual(
                data2['data']['next'],
                None
            )

        self.assertNotEqual(
            data2['data']['previous'],
            None
        )

        for endpoint in list_endpoints:
            if endpoint != 'banks':  # because this was already checked
                url = bank_endpoints[0] + endpoint + '/'

                req = self.client.get(banks_endpoint,
                                      auth=auth,
                                      headers=self.headers)
                self.ok(req)
                data = self.load(req)

                for field in expected_fields:
                    self.assertIn(
                        field,
                        data['data']
                    )

        for url in bank_endpoints:
            self.delete_item(auth, url)

    def test_can_get_edxml_item_format_from_item_details(self):
        """
        Do this for learners, assume instructors can also get to this.
        Other tests should reveal if instructors cannot.
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        files = {
            'manip' : self.manip
        }

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        item_edxml_endpoint = item_details_endpoint + 'edxml/'

        # have to be instructor to use this endpoint
        self.convert_user_to_bank_learner(bank_id)

        req = self.client.get(item_edxml_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.unauthorized(req)

        self.convert_user_to_bank_instructor(bank_id)
        req = self.client.get(item_edxml_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        data = self.load(req)
        self.assertIn(
            'manip.generic',  # no file extension for manip, because not truly an edX includable file type...we only support .png, .jpg, etc.
            data['files'].keys()
        )
        self.reset_files()
        self.assertEqual(
            data['files']['manip.generic'],
            base64.b64encode(self.manip.read())
        )

        self.assertEqual(
            str(data['data']),
            '<problem display_name="a really complicated item" max_attempts="0" rerandomize="never" showanswer="closed">\n <p>\n  can you manipulate this?\n </p>\n <multiplechoiceresponse>\n  <choicegroup direction="vertical">\n   <choice correct="false" name="Choice 1">\n    <text>\n     yes\n    </text>\n   </choice>\n   <choice correct="true" name="Choice 2">\n    <text>\n     no\n    </text>\n   </choice>\n   <choice correct="false" name="Choice 3">\n    <text>\n     maybe\n    </text>\n   </choice>\n  </choicegroup>\n </multiplechoiceresponse>\n</problem>'
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # def test_can_get_edxml_item_format_from_assessment_taken(self):
    #     """
    #     Do this for learners, assume instructors can also get to this.
    #     Other tests should reveal if instructors cannot.
    #     :return:
    #     """
    #     # CANNOT support the following, yet. Permissions will not work, because
    #     # need the Item (usually to fill in the right answer), yet learners who
    #     # access takens only cannot get to the item, only the question.
    #
    #     # but learners should be able to access the same data from takens/<id>/question/<id>/edxml/
    #     # assessments_endpoint = bank_endpoint + 'assessments/'
    #     # assessment_name = 'a really hard assessment'
    #     # assessment_desc = 'meant to differentiate students'
    #     # payload = {
    #     #     "name": assessment_name,
    #     #     "description": assessment_desc,
    #     #     "itemIds"   : [item_id]
    #     # }
    #     # post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
    #     # self.sign_client(post_sig)
    #     # req = self.client.post(assessments_endpoint, payload, format='json')
    #     # self.ok(req)
    #     # assessment_id = unquote(json.loads(req.content)['id'])
    #     #
    #     # assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
    #     # assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
    #     # assessment_items_endpoint = assessment_detail_endpoint + 'items/'
    #     #
    #     # # Use POST to create an offering
    #     # offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
    #     # self.sign_client(offering_post_sig)
    #     #
    #     # payload = {
    #     #     "startTime" : {
    #     #         "day"   : 1,
    #     #         "month" : 1,
    #     #         "year"  : 2015
    #     #     },
    #     #     "duration"  : {
    #     #         "days"  : 2
    #     #     }
    #     # }
    #     #
    #     # self.sign_client(offering_post_sig)
    #     # req = self.client.post(assessment_offering_endpoint, payload, format='json')
    #     # self.ok(req)
    #     # offering = json.loads(req.content)
    #     # offering_id = unquote(offering['id'])
    #     #
    #     # assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
    #     #
    #     # # Can POST to create a new taken
    #     # assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
    #     # post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
    #     # self.sign_client(post_sig)
    #     # req = self.client.post(assessment_offering_takens_endpoint)
    #     # self.ok(req)
    #     # taken = json.loads(req.content)
    #     # taken_id = unquote(taken['id'])
    #     #
    #     # taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
    #     #
    #     # taken_questions_endpoint = taken_endpoint + 'questions/'
    #     #
    #     # taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
    #     # taken_edxml_endpoint = taken_question_details_endpoint + 'edxml/'
    #     #
    #     # self.convert_user_to_bank_learner(bank_id)
    #     #
    #     # get_sig = calculate_signature(auth, self.headers, 'GET', taken_edxml_endpoint)
    #     # self.sign_client(get_sig)
    #     # req = self.client.get(taken_edxml_endpoint)
    #     # self.ok(req)
    #     #
    #     # data = self.load(req)
    #     # self.assertIn(
    #     #     'manip.generic',  # no file extension for manip, because not truly an edX includable file type...we only support .png, .jpg, etc.
    #     #     data['files'].keys()
    #     # )
    #     # self.reset_files()
    #     # self.assertEqual(
    #     #     data['files']['manip.generic'],
    #     #     base64.b64encode(self.manip.read())
    #     # )
    #     #
    #     # self.assertEqual(
    #     #     str(data['data']),
    #     #     '<problem display_name="a really complicated item" max_attempts="0" rerandomize="never" showanswer="closed">\n <p>\n  can you manipulate this?\n </p>\n <multiplechoiceresponse>\n  <choicegroup direction="vertical">\n   <choice correct="false" name="Choice 1">\n    <text>\n     yes\n    </text>\n   </choice>\n   <choice correct="true" name="Choice 2">\n    <text>\n     no\n    </text>\n   </choice>\n   <choice correct="false" name="Choice 3">\n    <text>\n     maybe\n    </text>\n   </choice>\n  </choicegroup>\n </multiplechoiceresponse>\n</problem>'
    #     # )
    #
    #     self.fail('finish writing the test')

    def test_non_supported_item_formats_in_item_details(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        files = {
            'manip'     : self.manip
        }

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               files=files,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        output_format = 'qti'
        item_edxml_endpoint = item_details_endpoint + output_format + '/'

        req = self.client.get(item_edxml_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.code(req, 500)
        self.message(req,
                     '"' + output_format + '" is not a supported item text format.',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # def test_non_supported_item_formats_in_assessment_taken(self):
    #     # CANNOT support the following, yet. Permissions will not work, because
    #     # need the Item (usually to fill in the right answer), yet learners who
    #     # access takens only cannot get to the item, only the question.
    #     self.fail('finish writing the test')

    def test_edX_item_metadata_on_create(self):
        # can add metadata like max_attempts to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            self.assertEqual(
                item[key],
                value
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edX_item_metadata_update(self):
        # can update metadata like max_attempts to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        new_metadata = {
            'attempts'      : metadata['attempts'] - 1,
            'markdown'      : 'second fake markdown',
            'rerandomize'   : 'second fake rerandomize',
            'showanswer'    : 'second fake showanswer',
            'weight'        : metadata['weight'] / 2
        }

        for key, value in new_metadata.iteritems():
            new_payload = {
                key : value
            }
            req = self.client.put(item_details_endpoint,
                                  auth=auth,
                                  data=json.dumps(new_payload),
                                  headers=self.json_headers)
            self.ok(req)
            item = self.load(req)
            self.assertEqual(
                item[key],
                value
            )
            self.assertNotEqual(
                item[key],
                metadata[key]
            )

            req = self.client.get(item_details_endpoint,
                                  auth=auth,
                                  headers=self.headers)
            self.ok(req)
            item = self.load(req)

            self.assertEqual(
                item[key],
                value
            )
            self.assertNotEqual(
                item[key],
                metadata[key]
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)


    def test_edX_item_irt_on_create(self):
        # can add IRT like difficulty to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = self.load(req)

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edX_item_irt_update(self):
        # can update IRT like difficulty to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        new_irt = {
            'difficulty'     : irt['difficulty'] + 2,
            'discrimination' : irt['discrimination'] * -1
        }

        for key, value in new_irt.iteritems():
            new_payload = {
                key : value
            }
            req = self.client.put(item_details_endpoint,
                                  auth=auth,
                                  data=json.dumps(new_payload),
                                  headers=self.json_headers)
            self.ok(req)
            item = self.load(req)
            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

            req = self.client.get(item_details_endpoint,
                                  auth=auth,
                                  headers=self.headers)
            self.ok(req)
            item = self.load(req)

            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?max_difficulty='

        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_difficulty)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_difficulty = irt['difficulty'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_difficulty)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_min_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_and_min_difficulty_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear += str(max_difficulty)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 2
        query_endpoint_should_not_appear += str(max_difficulty)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_max_and_min_difficulty_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_difficulty='

        bad_min = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_difficulty='
        bad_max = irt['difficulty'] - 1
        query_endpoint_should_appear += str(bad_max)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.code(req, 500)
        self.message(req,
                     'max_difficulty cannot be less than min_difficulty',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    # we don't support this yet
    # def test_item_query_irt_max_and_min_difficulty_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_max_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?max_discrimination='

        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_discrimination)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_discrimination = irt['discrimination'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_discrimination)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_min_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_irt_max_and_min_discrimination_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear += str(max_discrimination)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 2
        query_endpoint_should_not_appear += str(max_discrimination)
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_item_query_max_and_min_discrimination_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/query/?min_discrimination='

        bad_min = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_discrimination='
        bad_max = irt['discrimination'] - 1
        query_endpoint_should_appear += str(bad_max)

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.code(req, 500)
        self.message(req,
                     'max_discrimination cannot be less than min_discrimination',
                     True)

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)
    # we don't support this yet
    # def test_item_query_irt_max_and_min_discrimination_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_display_name(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_name = query_setup_data['item_name']

        query_endpoint = bank_endpoint + 'items/query/?display_name='

        word_in_name = item_name.split(' ')[2]
        query_endpoint_should_appear = query_endpoint + word_in_name

        req = self.client.get(query_endpoint_should_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        word_not_in_name = 'xkcd'
        query_endpoint_should_not_appear = query_endpoint + word_not_in_name
        req = self.client.get(query_endpoint_should_not_appear,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_lti_consumer_instance_guid_defaults_to_ip(self):
        """
        Cannot count on the LTI consumers providing the lti_tool_consumer_instance_guid
        because it is only a recommended parameter. So default to the requester IP.
        **
        Not a great remote test, because we need to check that two different users are
        created
        """
        #from ..models import LTIUser
        from ..utilities import id_generator
        lti_headers = ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']
        test_endpoint = self.endpoint + 'assessment/banks/'

        random_id = id_generator()

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = ''
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        req = self.client.get(test_endpoint,
                              auth=auth,
                              headers=modified_headers)

        self.ok(req)

        # not sure how to check this yet remotely yet...rely on 200 code
        # self.assertTrue(LTIUser.objects.filter(user_id=random_id).exists())
        # self.assertEqual(
        #     len(LTIUser.objects.filter(user_id=random_id)),
        #     1
        # )


        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Learner'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        req = self.client.get(test_endpoint,
                              auth=auth,
                              headers=modified_headers)

        self.ok(req)

        # not sure how to check this yet remotely yet...rely on 200 code
        # self.assertEqual(
        #     len(LTIUser.objects.filter(user_id=random_id)),
        #     2
        # )
        #
        # self.assertNotEqual(
        #     LTIUser.objects.filter(user_id=random_id).first().consumer_guid,
        #     LTIUser.objects.filter(user_id=random_id).last().consumer_guid
        # )

    def test_review_options_flag_works_for_during_and_after_attempt(self):
        """
        For the Reviewable offered and taken records
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }

        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # verify the "reviewWhetherCorrect" is True
        self.assertTrue(taken['reviewWhetherCorrect'])

        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set to only view correct after attempt
        payload = {
            "startTime"     : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"      : {
                "days"  : 2
            },
            "reviewOptions" : {
                "whetherCorrect" : {
                    "duringAttempt" : False
                }
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        self.convert_user_to_bank_learner(bank_id)

        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        req = self.client.post(assessment_offering_takens_endpoint,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # verify the "reviewWhetherCorrect" is False
        self.assertFalse(taken['reviewWhetherCorrect'])

        # now submitting an answer should let reviewWhetherCorrect be True
        right_response = {
            "integerValues": {
                "frontFaceValue" : 1,
                "sideFaceValue"  : 2,
                "topFaceValue"   : 3
            }
        }

        finish_taken_endpoint = taken_endpoint + 'finish/'
        taken_questions_endpoint = taken_endpoint + 'questions/'
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'

        req = self.client.post(question_1_submit_endpoint,
                               data=json.dumps(right_response),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)

        req = self.client.post(finish_taken_endpoint,
                               auth=auth,
                               headers=self.headers)
        self.ok(req)

        req = self.client.get(taken_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        taken = json.loads(req.content)

        self.assertTrue(taken['reviewWhetherCorrect'])

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)

        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_update_review_options_flag(self):
        """
        For the Reviewable offered and taken records, can  change the reviewOptions
        flag on a created item
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)

        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        payload = {
            "reviewOptions" : {
                "whetherCorrect" : {
                    "duringAttempt" : False
                }
            }
        }
        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        req = self.client.put(assessment_offering_detail_endpoint,
                              data=json.dumps(payload),
                              auth=auth,
                              headers=self.json_headers)

        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt','afterDeadline','beforeDeadline','duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        self.delete_item(auth, assessment_offering_detail_endpoint)

        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_set_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_set_non_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'mc3-objective%3A21273%40MIT-OEIT'  # test that this does not get re-quote safed
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : [lo_test_id]
        }
        stringified_payload = deepcopy(payload)

        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            [lo_test_id]
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_update_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds"  : new_los
        }
        req = self.client.put(item_details_endpoint,
                              data=json.dumps(payload),
                              auth=auth,
                              headers=self.json_headers)
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_update_non_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : los
        }
        stringified_payload = deepcopy(payload)

        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        req = self.client.get(item_details_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            los
        )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds"  : new_los
        }
        req = self.client.put(item_details_endpoint,
                              data=json.dumps(payload),
                              auth=auth,
                              headers=self.json_headers)
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_edx_item_query_learning_objective_id(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_id = query_setup_data['item_id']
        los = query_setup_data['learning_objectives']

        query_endpoint = bank_endpoint + 'items/query/?learning_objective=' + los[0]
        req = self.client.get(query_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/query/?learning_objective=wrongId'
        req = self.client.get(query_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_non_edx_item_query_learning_objective_id(self):
        # not supported in their item records yet, so this should
        # return no results?
        # And yet it does work -- so there is something I do not
        # understand about query objects and items...
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
            "learningObjectiveIds"  : los
        }
        stringified_payload = deepcopy(payload)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'

        query_endpoint = bank_endpoint + 'items/query/?learning_objective=' + los[0]
        req = self.client.get(query_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/query/?learning_objective=wrongId'
        req = self.client.get(query_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

        self.delete_item(auth, item_details_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_set_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set maxAttempts on create
        payload = {
            "startTime"     : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"      : {
                "days"  : 2
            },
            "maxAttempts" : 2
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_can_update_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)

        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # try again, but set maxAttempts on update
        payload = {
            "maxAttempts" : 2
        }
        req = self.client.put(assessment_offering_detail_endpoint,
                              data=json.dumps(payload),
                              auth=auth,
                              headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_default_max_attempts_allows_infinite_attempts(self):
        # ok, don't really test to infinity, but test several
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            }
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 25
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            req = self.client.post(assessment_offering_takens_endpoint,
                                   auth=auth,
                                   headers=self.headers)
            self.ok(req)
            taken = json.loads(req.content)
            taken_id = unquote(taken['id'])

            taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
            taken_endpoints.append(taken_endpoint)
            taken_finish_endpoint = taken_endpoint + 'finish/'
            req = self.client.post(taken_finish_endpoint,
                                   auth=auth,
                                   headers=self.headers)
            self.ok(req)
            # finish the assessment taken, so next time we create one, it should
            # create a new one

        self.convert_user_to_bank_instructor(bank_id)

        for taken_endpoint in taken_endpoints:
            self.delete_item(auth, taken_endpoint)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)

    def test_max_attempts_throws_exception_if_taker_tries_to_exceed(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        name = 'atestbank'
        desc = 'for testing purposes only'
        bank_id = self.create_test_bank(auth, name, desc)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue" : 2,
                    "topFaceValue"  : 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        files = {
            'manip'     : self.manip,
            'frontView' : self.front,
            'sideView'  : self.side,
            'topView'   : self.top
        }
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               data=stringified_payload,
                               auth=auth,
                               headers=self.headers,
                               files=files)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        req = self.client.get(items_endpoint,
                              auth=auth,
                              headers=self.headers)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(assessments_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds' : [item_1_id]
        }
        req = self.client.post(assessment_items_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)


        # Use POST to create an offering
        payload = {
            "startTime" : {
                "day"   : 1,
                "month" : 1,
                "year"  : 2015
            },
            "duration"  : {
                "days"  : 2
            },
            "maxAttempts" : 2
        }
        req = self.client.post(assessment_offering_endpoint,
                               data=json.dumps(payload),
                               auth=auth,
                               headers=self.json_headers)
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts == 2
        self.assertEquals(offering['maxAttempts'], 2)

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 5
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            req = self.client.post(assessment_offering_takens_endpoint,
                                   auth=auth,
                                   headers=self.headers)
            if attempt >= payload['maxAttempts']:
                self.code(req, 403)
                self.message(req,
                             'You have exceeded the maximum number of allowed attempts.',
                             True)
            else:
                self.ok(req)
                taken = json.loads(req.content)
                taken_id = unquote(taken['id'])

                taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
                taken_endpoints.append(taken_endpoint)
                taken_finish_endpoint = taken_endpoint + 'finish/'
                req = self.client.post(taken_finish_endpoint,
                                       auth=auth,
                                       headers=self.headers)
                self.ok(req)
                # finish the assessment taken, so next time we create one, it should
                # create a new one

        self.convert_user_to_bank_instructor(bank_id)

        for taken_endpoint in taken_endpoints:
            self.delete_item(auth, taken_endpoint)

        self.delete_item(auth, assessment_offering_detail_endpoint)
        self.delete_item(auth, assessment_detail_endpoint)

        item_1_endpoint = items_endpoint + item_1_id + '/'
        self.delete_item(auth, item_1_endpoint)
        self.delete_item(auth, bank_endpoint)
