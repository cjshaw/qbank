import datetime

from dlkit.runtime.primitives import DateTime

def convert_str_to_datetime(str_datetime):
    datetime_obj = datetime.datetime.strptime(str_datetime, '%Y-%m-%d')
    return DateTime(day=datetime_obj.day,
                    month=datetime_obj.month,
                    year=datetime_obj.year)
