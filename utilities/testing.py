"""useful utility methods"""
import json
import envoy

from copy import deepcopy

from django.conf import settings

from dlkit.runtime.primordium import Type, Id
from dlkit.runtime.proxy_example import User

# Note that changing the runtime configs works with testing, but
# trying to change dlkit_configs.configs does not...
import dlkit.runtime.configs

from rest_framework_httpsignature.authentication import SignatureAuthentication
from rest_framework.test import APITestCase, APIClient

from utilities.authorization import get_vault, create_vault
from utilities.general import clean_id, get_session_data, create_function_id,\
    create_qualifier_id, activate_managers, create_agent_id

BOOTSTRAP_VAULT_GENUS = Type(**{
    'identifier': 'bootstrap-vault',
    'namespace': 'authorization.Vault',
    'authority': 'ODL.MIT.EDU'
})

SUPER_USER_AUTHZ_GENUS = Type(**{
    'identifier': 'super-user-authz',
    'namespace': 'authorization.Authorization',
    'authority': 'ODL.MIT.EDU'
})

BASE_AUTHORIZATIONS = (
    ('assessment.Bank', 'lookup', 'assessment.Bank'),
    ('authorization.Vault', 'lookup', 'authorization.Vault'),
    ('commenting.Book', 'lookup', 'commenting.Book'),
    ('hierarchy.Hierarchy', 'lookup', 'hierarchy.Hierarchy'),
    ('learning.ObjectiveBank', 'lookup', 'learning.ObjectiveBank'),
    ('repository.Repository', 'lookup', 'repository.Repository'),
    ('resource.Bin', 'lookup', 'resource.Bin'),
)

SUPER_USER_FUNCTIONS = (
    ('create', 'authorization.Authorization'),
    ('delete', 'authorization.Authorization'),
    ('lookup', 'authorization.Authorization'),
    ('search', 'authorization.Authorization'),
    ('create', 'authorization.Vault'),
    ('delete', 'authorization.Vault'),
    ('search', 'authorization.Vault'),
)

PROXY_USER_FUNCTIONS = (
    ('proxy', 'users.Proxy'),
)

INSTRUCTOR_FUNCTIONS = (
    ('assessment.Answer', 'lookup', 'assessment.Bank'),
    ('assessment.Answer', 'create', 'assessment.Bank'),
    ('assessment.Answer', 'delete', 'assessment.Bank'),
    ('assessment.Answer', 'update', 'assessment.Bank'),
    ('assessment.Assessment', 'author', 'assessment.Bank'),
    ('assessment.Assessment', 'lookup', 'assessment.Bank'),
    ('assessment.Assessment', 'create', 'assessment.Bank'),
    ('assessment.Assessment', 'delete', 'assessment.Bank'),
    ('assessment.Assessment', 'search', 'assessment.Bank'),
    ('assessment.Assessment', 'update', 'assessment.Bank'),
    ('assessment.Assessment', 'take', 'assessment.Bank'),
    ('assessment.AssessmentBank', 'assign', 'assessment.Bank'),
    ('assessment.AssessmentBank', 'lookup', 'assessment.Bank'),
    ('assessment.AssessmentOffered', 'lookup', 'assessment.Bank'),
    ('assessment.AssessmentOffered', 'create', 'assessment.Bank'),
    ('assessment.AssessmentOffered', 'delete', 'assessment.Bank'),
    ('assessment.AssessmentOffered', 'update', 'assessment.Bank'),
    ('assessment.AssessmentResults', 'access', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'lookup', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'create', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'delete', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'search', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'update', 'assessment.Bank'),
    ('assessment.Item', 'alias', 'assessment.Bank'),
    ('assessment.Item', 'lookup', 'assessment.Bank'),
    ('assessment.Item', 'create', 'assessment.Bank'),
    ('assessment.Item', 'delete', 'assessment.Bank'),
    ('assessment.Item', 'update', 'assessment.Bank'),
    ('assessment.Item', 'search', 'assessment.Bank'),
    ('assessment.ItemBank', 'assign', 'assessment.Bank'),
    ('assessment.Question', 'lookup', 'assessment.Bank'),
    ('assessment.Question', 'create', 'assessment.Bank'),
    ('assessment.Question', 'delete', 'assessment.Bank'),
    ('assessment.Question', 'update', 'assessment.Bank'),
    ('assessment.Bank', 'access', 'assessment.Bank'),
    ('assessment.Bank', 'alias', 'assessment.Bank'),
    ('assessment.Bank', 'create', 'assessment.Bank'),
    ('assessment.Bank', 'delete', 'assessment.Bank'),
    ('assessment.Bank', 'modify', 'assessment.Bank'),
    ('assessment.Bank', 'search', 'assessment.Bank'),
    ('assessment.Bank', 'update', 'assessment.Bank'),
    ('assessment_authoring.AssessmentPart', 'lookup', 'assessment.Bank'),
    ('assessment_authoring.AssessmentPart', 'create', 'assessment.Bank'),
    ('assessment_authoring.AssessmentPart', 'delete', 'assessment.Bank'),
    ('assessment_authoring.AssessmentPart', 'update', 'assessment.Bank'),
    ('commenting.Book', 'access', 'commenting.Book'),
    ('commenting.Book', 'create', 'commenting.Book'),
    ('commenting.Book', 'delete', 'commenting.Book'),
    ('commenting.Book', 'modify', 'commenting.Book'),
    ('commenting.Book', 'update', 'commenting.Book'),
    ('commenting.Comment', 'author', 'commenting.Book'),
    ('commenting.Comment', 'lookup', 'commenting.Book'),
    ('commenting.Comment', 'create', 'commenting.Book'),
    ('commenting.Comment', 'delete', 'commenting.Book'),
    ('commenting.Comment', 'update', 'commenting.Book'),
    ('hierarchy.Hierarchy', 'update', 'hierarchy.Hierarchy'),
    ('learning.ObjectiveBank', 'create', 'learning.ObjectiveBank'),
    ('learning.ObjectiveBank', 'delete', 'learning.ObjectiveBank'),
    ('learning.ObjectiveBank', 'update', 'learning.ObjectiveBank'),
    ('learning.Proficiency', 'create', 'learning.ObjectiveBank'),
    ('learning.Proficiency', 'delete', 'learning.ObjectiveBank'),
    ('learning.Proficiency', 'lookup', 'learning.ObjectiveBank'),
    ('learning.Proficiency', 'search', 'learning.ObjectiveBank'),
    ('learning.Proficiency', 'update', 'learning.ObjectiveBank'),
    ('logging.Log', 'lookup', 'logging.Log'),
    ('logging.Log', 'create', 'logging.Log'),
    ('logging.Log', 'delete', 'logging.Log'),
    ('logging.Log', 'update', 'logging.Log'),
    ('logging.LogEntry', 'alias', 'logging.Log'),
    ('logging.LogEntry', 'create', 'logging.Log'),
    ('logging.LogEntry', 'delete', 'logging.Log'),
    ('logging.LogEntry', 'lookup', 'logging.Log'),
    ('logging.LogEntry', 'search', 'logging.Log'),
    ('logging.LogEntry', 'update', 'logging.Log'),
    ('repository.Repository', 'access', 'repository.Repository'),
    ('repository.Repository', 'author', 'repository.Repository'),
    ('repository.Repository', 'create', 'repository.Repository'),
    ('repository.Repository', 'delete', 'repository.Repository'),
    ('repository.Repository', 'modify', 'repository.Repository'),
    ('repository.Repository', 'search', 'repository.Repository'),
    ('repository.Repository', 'update', 'repository.Repository'),
    ('repository.Asset', 'author', 'repository.Repository'),
    ('repository.Asset', 'lookup', 'repository.Repository'),
    ('repository.Asset', 'create', 'repository.Repository'),
    ('repository.Asset', 'delete', 'repository.Repository'),
    ('repository.Asset', 'search', 'repository.Repository'),
    ('repository.Asset', 'update', 'repository.Repository'),
    ('repository.AssetComposition', 'access', 'repository.Repository'),
    ('repository.AssetComposition', 'lookup', 'repository.Repository'),
    ('repository.AssetComposition', 'compose', 'repository.Repository'),
    ('repository.AssetRepository', 'assign', 'repository.Repository'),
    ('repository.AssetRepository', 'lookup', 'repository.Repository'),
    ('repository.Composition', 'author', 'repository.Repository'),
    ('repository.Composition', 'lookup', 'repository.Repository'),
    ('repository.Composition', 'create', 'repository.Repository'),
    ('repository.Composition', 'delete', 'repository.Repository'),
    ('repository.Composition', 'search', 'repository.Repository'),
    ('repository.Composition', 'update', 'repository.Repository'),
    ('resource.Bin', 'access', 'resource.Bin'),
    ('resource.Bin', 'author', 'resource.Bin'),
    ('resource.Bin', 'create', 'resource.Bin'),
    ('resource.Bin', 'delete', 'resource.Bin'),
    ('resource.Bin', 'modify', 'resource.Bin'),
    ('resource.Bin', 'update', 'resource.Bin'),
    ('resource.Resource', 'author', 'resource.Bin'),
    ('resource.Resource', 'lookup', 'resource.Bin'),
    ('resource.Resource', 'create', 'resource.Bin'),
    ('resource.Resource', 'delete', 'resource.Bin'),
    ('resource.Resource', 'search', 'resource.Bin'),
    ('resource.Resource', 'update', 'resource.Bin'),
    ('resource.ResourceAgent', 'assign', 'resource.Bin'),
    ('resource.ResourceAgent', 'delete', 'resource.Bin'),
    ('resource.ResourceAgent', 'lookup', 'resource.Bin'),
    ('grading.Gradebook', 'lookup', 'grading.Gradebook'),
    ('grading.Gradebook', 'create', 'grading.Gradebook'),
    ('grading.Gradebook', 'delete', 'grading.Gradebook'),
    ('grading.Gradebook', 'update', 'grading.Gradebook'),
    ('grading.GradeEntry', 'create', 'grading.Gradebook'),
    ('grading.GradeEntry', 'delete', 'grading.Gradebook'),
    ('grading.GradeEntry', 'lookup', 'grading.Gradebook'),
    ('grading.GradeEntry', 'update', 'grading.Gradebook'),
    ('grading.GradeSystem', 'create', 'grading.Gradebook'),
    ('grading.GradeSystem', 'delete', 'grading.Gradebook'),
    ('grading.GradeSystem', 'lookup', 'grading.Gradebook'),
    ('grading.GradeSystem', 'update', 'grading.Gradebook'),
    ('grading.GradebookColumn', 'create', 'grading.Gradebook'),
    ('grading.GradebookColumn', 'delete', 'grading.Gradebook'),
    ('grading.GradebookColumn', 'lookup', 'grading.Gradebook'),
    ('grading.GradebookColumn', 'update', 'grading.Gradebook'),
)

STUDENT_FUNCTIONS = (
    ('assessment.AssessmentTaken', 'create', 'assessment.Bank'),
    ('assessment.AssessmentTaken', 'lookup', 'assessment.Bank'),
    ('assessment.Assessment', 'take', 'assessment.Bank'),
    ('commenting.Comment', 'lookup', 'commenting.Book'),
    ('repository.Asset', 'create', 'repository.Repository'),
    ('repository.Asset', 'delete', 'repository.Repository'),
    ('repository.Asset', 'lookup', 'repository.Repository'),
    ('repository.Asset', 'search', 'repository.Repository'),
    ('resource.Resource', 'lookup', 'resource.Bin'),
)

SUBPACKAGES = (
    ('assessment_authoring', 'assessment'),
)


class APISignatureAuthentication(SignatureAuthentication):
    """
    Adapted from
    https://github.com/etoccalino/django-rest-framework-httpsignature/blob/master/rest_framework_httpsignature/tests.py
    """
    API_KEY_HEADER = 'X-Api-Key'
    def __init__(self, user):
        self.user = user

    def fetch_user_data(self, api_key):
        return (self.user, str(self.user.private_key))


class QBankBaseTest(APITestCase):
    def _pre_setup(self):
        APITestCase._pre_setup(self)
        # optional: shortcut client handle for quick testing
        self.client = APIClient()

    def _post_teardown(self):
        APITestCase._post_teardown(self)

    def _get_test_bank(self):
        req = create_test_request(self.user)
        activate_managers(req)
        am = get_session_data(req, 'am')
        querier = am.get_bank_query()
        querier.match_genus_type(Type('assessment.Bank%3Atest-bank%40ODL.MIT.EDU'), True)
        bank = am.get_banks_by_query(querier).next()
        return am.get_bank(bank.ident)  # to make sure we get a services bank

    def code(self, _req, _code):
        self.assertEqual(
            int(_req.status_code),
            int(_code)
        )

    def created(self, _req):
        self.code(_req, 201)

    def delete(self, url, non_instructor=False):
        self.login(non_instructor=non_instructor)
        return self.client.delete(url)

    def deleted(self, _req):
        self.code(_req, 204)

    def get(self, url, data=None, non_instructor=False):
        self.login(non_instructor=non_instructor)
        if data is not None:
            return self.client.get(url, data=data, format=json)
        else:
            return self.client.get(url)

    def filename(self, file_):
        try:
            return file_.name.split('/')[-1].split('.')[0]
        except AttributeError:
            return file_.split('/')[-1].split('.')[0]

    def is_cloudfront_url(self, _url):
        self.assertIn(
            self._cloudfront_root,
            _url
        )

        expected_params = ['?Expires=','&Signature=','&Key-Pair-Id={0}'.format(settings.CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID)]

        for param in expected_params:
            self.assertIn(
                param,
                _url
            )

    def json(self, _req):
        return json.loads(_req.content)

    def login(self, non_instructor=False):
        self.client.logout()
        if non_instructor:
            self.client.login(username=self.student_name, password=self.student_password)
        else:
            self.client.login(username=self.username, password=self.password)

    def message(self, _req, _msg):
        try:
            data = self.json(_req)
            if 'detail' in data:
                self.assertIn(_msg, data['detail'])
            elif isinstance(data, dict):
                self.assertIn(_msg, json.dumps(data))
        except ValueError:
            self.assertIn(_msg, str(_req.content))

    def not_allowed(self, _req, _type):
        """
        verify that the _type method is not allowed. Status code 405
        and the text matches not allowed text
        """
        self.assertEqual(_req.status_code, 405)
        self.assertEqual(
            _req.content,
            '{"detail": "Method \'' + _type + '\' not allowed."}'
        )

    def ok(self, _req):
        """
        checks for status code 200 and that the request was okay
        """
        self.assertEqual(_req.status_code, 200)

    def post(self, url, payload=None, non_instructor=False, files=False):
        self.login(non_instructor=non_instructor)
        if files:
            return self.client.post(url, data=payload)
        else:
            return self.client.post(url, data=payload, format='json')

    def put(self, url, payload=None, non_instructor=False, files=False):
        self.login(non_instructor=non_instructor)
        if files:
            return self.client.put(url, data=payload)
        else:
            return self.client.put(url, data=payload, format='json')

    def setUp(self):
        envoy.run('mongo test_qbank_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_assessment_authoring --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_logging --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_resource --eval "db.dropDatabase()"')
        configure_test_bucket()

        self._aws_root = 'https://{0}.s3.amazonaws.com/'.format(settings.S3_TEST_BUCKET)
        self._cloudfront_root = 'https://{0}/'.format(settings.CLOUDFRONT_TEST_DISTRO)

        super(QBankBaseTest, self).setUp()

    def set_proxy_user(self, username):
        """
        """
        self.client.credentials(
            HTTP_X_API_PROXY=username
        )

    def tearDown(self):
        """
        """
        envoy.run('mongo test_qbank_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_assessment_authoring --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_logging --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_resource --eval "db.dropDatabase()"')

        super(QBankBaseTest, self).tearDown()

    def updated(self, _req):
        self.code(_req, 202)


def get_super_authz_user_request():
    return get_authz_user_request(settings.AUTHZ_USER)

def get_authz_user_request(username):
    authz_user = User(username=username, authenticated=True)
    req = create_test_request(authz_user)
    activate_managers(req)
    return req

def add_user_authz_to_settings(role, username, catalog_id=None, authority='MIT-ODL'):
    if isinstance(catalog_id, basestring):
        catalog_id = clean_id(catalog_id)
    agent = create_agent_id(username, authority=authority)

    if catalog_id is None:
        qualifiers = ('ROOT', 24 * '0')
        catalog_id = create_qualifier_id(24 * '0', 'authorization.Vault')
    else:
        qualifiers = (catalog_id,)

    # first, add the base authorizations to the user for the catalog_id and ROOT / '0' * 24
    req = get_super_authz_user_request()
    vault = get_vault(req)

    create_base_authorizations(vault,
                               agent,
                               qualifiers=qualifiers)

    # then, depending on role, add additional functions
    if role == 'instructor':
        authorization_iterator(vault,
                               agent,
                               qualifiers,
                               INSTRUCTOR_FUNCTIONS)
    elif role == 'student':
        authorization_iterator(vault,
                               agent,
                               qualifiers,
                               STUDENT_FUNCTIONS)

def calculate_signature(auth, headers, method, path):
    """
    Should return the encoded signature from an HTTPSignatureAuth object
    Need to use the .sign(headers, method, path) of the header_signer object
    in auth
    """
    signed_headers = auth.header_signer.sign(headers, method=method, path=path)
    return signed_headers['authorization']

def configure_proxyable_authz(main_user, catalog_id=None):
    if isinstance(catalog_id, basestring):
        catalog_id = clean_id(catalog_id)

    if catalog_id is None:
        catalog_id = create_qualifier_id(24 * '0', 'authorization.Vault')

        qualifiers = ('ROOT', 24 * '0')
    else:
        qualifiers = (catalog_id,)

    req = get_super_authz_user_request()
    vault = get_vault(req)

    for function_tuple in PROXY_USER_FUNCTIONS:
        function = create_function_id(function_tuple[0],
                                      function_tuple[1])

        for qualifier in qualifiers:
            if not isinstance(qualifier, Id):
                qualifier = create_qualifier_id(qualifier, function_tuple[1])

            create_authz(vault, create_agent_id(main_user), function, qualifier)

def create_authz(vault, agent, function, qualifier, is_super=False, end_date=None):
    form = vault.get_authorization_form_for_create_for_agent(agent, function, qualifier, [])

    if is_super:
        form.set_genus_type(SUPER_USER_AUTHZ_GENUS)

    if end_date is not None:
        form.set_end_date(end_date)

    return vault.create_authorization(form)

def create_super_authz_authorizations(vault):
    req = get_super_authz_user_request()
    authzm = get_session_data(req, 'authzm')

    agent_id = authzm.effective_agent_id

    for function_tuple in SUPER_USER_FUNCTIONS:
        function = create_function_id(function_tuple[0],
                                      function_tuple[1])

        create_authz(vault, agent_id, function, vault.ident, is_super=True)

def authorization_iterator(vault, agent, qualifiers, authz_list, is_super=False):
    def first(namespace):
        return str(namespace).split('.')[0]

    for qualifier in qualifiers:
        for function_tuple in authz_list:
            namespace = function_tuple[0]
            function_name = function_tuple[1]
            function = create_function_id(function_name, namespace)

            if not isinstance(qualifier, Id):
                qualifier_id = create_qualifier_id(qualifier, function_tuple[2])
            else:
                qualifier_id = qualifier

            # also need to handle subpackages!!
            is_subpackage = False
            for subpackage in SUBPACKAGES:
                sub = subpackage[0]
                parent = subpackage[1]
                if first(qualifier_id.namespace) == parent and first(function.namespace) == sub:
                    is_subpackage = True

            if (first(qualifier_id.namespace) == first(function.namespace) or
                    is_subpackage):
                create_authz(vault, agent, function, qualifier_id, is_super)

def create_base_authorizations(vault, agent, qualifiers=(), is_super=False):
    if len(qualifiers) == 0:
        qualifiers = ('ROOT',
                      24 * '0',
                      Id('assessment.Bank%3A{0}%40bazzim.MIT.EDU'.format(24 * '0')))
    authorization_iterator(vault, agent, qualifiers, BASE_AUTHORIZATIONS, is_super)

def create_authz_superuser():
    original_config = open_up_services_config()

    req = get_super_authz_user_request()
    authzm = get_session_data(req, 'authzm')
    vault = create_vault(req)

    create_base_authorizations(vault, authzm.effective_agent_id, is_super=True)
    create_super_authz_authorizations(vault)

    restore_services_config(original_config)

def configure_test_bucket():
    """use test settings, not the production settings"""
    dlkit.runtime.configs.AWS_ADAPTER_1 = {
        'id': 'aws_adapter_configuration_1',
        'displayName': 'AWS Adapter Configuration',
        'description': 'Configuration for AWS Adapter',
        'parameters': {
            'implKey': {
                'syntax': 'STRING',
                'displayName': 'Implementation Key',
                'description': 'Implementation key used by Runtime for class loading',
                'values': [
                    {'value': 'aws_adapter', 'priority': 1}
                ]
            },
            'cloudFrontPublicKey': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Public Key',
                'description': 'Public key for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_PUBLIC_KEY, 'priority': 1}
                ]
            },
            'cloudFrontPrivateKey': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Private Key',
                'description': 'Private key for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_PRIVATE_KEY, 'priority': 1}
                ]
            },
            'cloudFrontSigningKeypairId': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Signing Keypair ID',
                'description': 'Signing keypair id for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID, 'priority': 1}
                ]
            },
            'cloudFrontSigningPrivateKeyFile': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Signing Private Key File',
                'description': 'Signing Private Key File for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_SIGNING_PRIVATE_KEY_FILE, 'priority': 1}
                ]
            },
            'cloudFrontDistro': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Distro',
                'description': 'CloudFront Distr-o-bution.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_DISTRO, 'priority': 1}
                ]
            },
            'cloudFrontDistroId': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Distro Id',
                'description': 'CloudFront Distr-o-bution Id.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_DISTRO_ID, 'priority': 1}
                ]
            },
            'S3PrivateKey': {
                'syntax': 'STRING',
                'displayName': 'S3 Private Key',
                'description': 'Private Key for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_PRIVATE_KEY, 'priority': 1}
                ]
            },
            'S3PublicKey': {
                'syntax': 'STRING',
                'displayName': 'S3 Public Key',
                'description': 'Public Key for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_PUBLIC_KEY, 'priority': 1}
                ]
            },
            'S3Bucket': {
                'syntax': 'STRING',
                'displayName': 'S3 Bucket',
                'description': 'Bucket for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_BUCKET, 'priority': 1}
                ]
            },
            'repositoryProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Repository Provider Implementation',
                'description': 'Implementation for repository service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
        }
    }

    dlkit.runtime.configs.JSON_1 = {
        'id': 'json_configuration_1',
        'displayName': 'JSON Configuration',
        'description': 'Configuration for JSON Implementation',
        'parameters': {
            'implKey': {
                'syntax': 'STRING',
                'displayName': 'Implementation Key',
                'description': 'Implementation key used by Runtime for class loading',
                'values': [
                    {'value': 'json', 'priority': 1}
                ]
            },
            'authority': {
                'syntax': 'STRING',
                'displayName': 'Mongo Authority',
                'description': 'Authority.',
                'values': [
                    {'value': settings.DLKIT_AUTHORITY, 'priority': 1}
                ]
            },
            'mongoDBNamePrefix': {
                'syntax': 'STRING',
                'displayName': 'Mongo DB Name Prefix',
                'description': 'Prefix for naming mongo databases.',
                'values': [
                    {'value': settings.DLKIT_TEST_MONGO_DB_PREFIX, 'priority': 1}
                ]
            },
            'mongoHostURI': {
                'syntax': 'STRING',
                'displayName': 'Mongo Host URI',
                'description': 'URI for setting the MongoClient host.',
                'values': [
                    {'value': settings.MONGO_HOST_URI, 'priority': 1}
                ]
            },
            'repositoryProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Repository Provider Implementation',
                'description': 'Implementation for repository service provider',
                'values': [
                    {'value': 'AWS_ADAPTER_1', 'priority': 1}
                ]
            },
            'assetContentRecordTypeForFiles': {
                'syntax': 'TYPE',
                'displayName': 'Asset Content Type for Files',
                'description': 'Asset Content Type for Records that store Files in a repository',
                'values': [
                    {'value': Type(**{
                       'authority': 'odl.mit.edu',
                       'namespace': 'asset_content_record_type',
                       'identifier': 'amazon-web-services'
                    }), 'priority': 1}
                ]
            },
            'recordsRegistry': {
                'syntax': 'STRING',
                'displayName': 'Python path to the extension records registry file',
                'description': 'dot-separated path to the extension records registry file',
                'values': [
                    {'value': 'dlkit.records.registry', 'priority': 1}
                ]
            },
            'learningProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Learning Provider Implementation',
                'description': 'Implementation for learning service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'keywordFields': {
                'syntax': 'OBJECT',
                'displayName': 'Keyword Fields',
                'description': 'Text fields to include in keyword queries',
                'values': [
                    {'value': settings.DLKIT_MONGO_KEYWORD_FIELDS, 'priority': 1}
                ]
            },
            'magicItemLookupSessions': {
                'syntax': 'STRING',
                'displayName': 'Which magic item lookup sessions to try',
                'description': 'To handle magic IDs.',
                'values': [
                    {'value': 'dlkit.records.adaptive.multi_choice_questions.randomized_questions.RandomizedMCItemLookupSession', 'priority': 1}
                ]
            },
            'magicAssessmentPartLookupSessions': {
                'syntax': 'STRING',
                'displayName': 'Which magic assessment part lookup sessions to try',
                'description': 'To handle magic IDs.',
                'values': [
                    {'value': 'dlkit.records.adaptive.magic_parts.assessment_part_records.MagicAssessmentPartLookupSession', 'priority': 1}
                ]
            },
            'localImpl': {
                'syntax': 'STRING',
                'displayName': 'Implementation identifier for local service provider',
                'description': 'Implementation identifier for local service provider.  Typically the same identifier as the Mongo configuration',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'useCachingForQualifierIds': {
                'syntax': 'BOOLEAN',
                'displayName': 'Flag to use memcached for authz qualifier_ids or not',
                'description': 'Flag to use memcached for authz qualifier_ids or not',
                'values': [
                    {'value': True, 'priority': 1}
                ]
            },
        }
    }

    # create a super-user who can create authorizations
    # create_authz_superuser()
    envoy.run('mongorestore --db test_qbank_assessment --drop assessmentsv2/fixtures/test_qbank_assessment')
    envoy.run('mongorestore --db test_qbank_authorization --drop assessmentsv2/fixtures/test_qbank_authorization')

def create_test_bank(test_instance):
    """
    helper method to create a test assessment bank
    """
    test_endpoint = '/api/v2/assessment/banks/'
    test_instance.login()
    payload = {
        "name": "a test bank",
        "description": "for testing"
    }
    req = test_instance.client.post(test_endpoint, payload, format='json')
    return json.loads(req.content)

def create_test_request(test_user):
    from django.http import HttpRequest
    from django.conf import settings
    import uuid
    from django.utils.importlib import import_module
    #http://stackoverflow.com/questions/16865947/django-httprequest-object-has-no-attribute-session
    test_request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    session_key = str(uuid.uuid4())
    test_request.user = test_user
    test_request.session = engine.SessionStore(session_key)
    return test_request

def open_up_services_config():
    previous_version = deepcopy(dlkit.runtime.configs.SERVICE)

    dlkit.runtime.configs.SERVICE = {
        'id': 'dlkit.runtime_bootstrap_configuration',
        'displayName': 'DLKit Runtime Bootstrap Configuration',
        'description': 'Bootstrap Configuration for DLKit Runtime',
        'parameters': {
            'implKey': {
                'syntax': 'STRING',
                'displayName': 'Implementation Key',
                'description': 'Implementation key used by Runtime for class loading',
                'values': [
                    {'value': 'service', 'priority': 1}
                ]
            },
            'assessmentProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Assessment Provider Implementation',
                'description': 'Implementation for assessment service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'assessment_authoringProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Assessment Authoring Provider Implementation',
                'description': 'Implementation for assessment authoring service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'authorizationProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Authorization Provider Implementation',
                'description': 'Implementation for authorization service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'learningProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Learning Provider Implementation',
                'description': 'Implementation for learning service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'repositoryProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Repository Provider Implementation',
                'description': 'Implementation for repository service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'commentingProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Commenting Provider Implementation',
                'description': 'Implementation for commenting service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'resourceProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Resource Provider Implementation',
                'description': 'Implementation for resource service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'gradingProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Grading Provider Implementation',
                'description': 'Implementation for grading provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
            'loggingProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Logging Provider Implementation',
                'description': 'Implementation for logging provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
        }
    }

    return previous_version

def remove_authz(vault, agent_id, function, qualifier):
    querier = vault.get_authorization_query()

    querier.match_agent_id(agent_id, True)
    querier.match_function_id(function, True)
    querier.match_qualifier_id(qualifier, True)

    for authz in vault.get_authorizations_by_query(querier):
        vault.delete_authorization(authz.ident)

def remove_proxy_privileges(user, catalog_id=None):
    if isinstance(catalog_id, basestring):
        catalog_id = clean_id(catalog_id)

    if catalog_id is None:
        catalog_id = create_qualifier_id(24 * '0', 'authorization.Vault')
        qualifiers = ('ROOT', 24 * '0')
    else:
        qualifiers = (catalog_id,)

    req = get_super_authz_user_request()
    vault = get_vault(req)

    for function_tuple in PROXY_USER_FUNCTIONS:
        function = create_function_id(function_tuple[0],
                                      function_tuple[1])

        for qualifier in qualifiers:
            if not isinstance(qualifier, Id):
                qualifier = create_qualifier_id(qualifier, function_tuple[1])

            remove_authz(vault, create_agent_id(user), function, qualifier)

def restore_services_config(original_version):
    dlkit.runtime.configs.SERVICE = original_version
