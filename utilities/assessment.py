import os


from dlkit.abstract_osid.learning import objects as abc_learning_objects
from dlkit.abstract_osid.type import objects as abc_type_objects

from dlkit.json_.assessment.assessment_utilities import get_assessment_part_lookup_session
from dlkit.json_.osid.osid_errors import InvalidArgument, NullArgument,\
    Unsupported, NotFound, PermissionDenied
from dlkit.runtime.primitives import DataInputStream, DisplayText
from dlkit.records.registry import ANSWER_GENUS_TYPES, ANSWER_RECORD_TYPES,\
    ITEM_RECORD_TYPES,\
    ITEM_GENUS_TYPES,\
    ASSET_CONTENT_GENUS_TYPES,\
    ASSET_GENUS_TYPES,\
    ASSESSMENT_OFFERED_RECORD_TYPES,\
    ASSESSMENT_RECORD_TYPES,\
    ASSESSMENT_PART_RECORD_TYPES,\
    ASSESSMENT_PART_GENUS_TYPES

from dlkit.runtime.primordium import Id, Duration, DateTime
from dlkit.runtime.proxy_example import SimpleRequest

from django.utils.http import quote

from inflection import underscore

from assessmentsv2.types import WORDIGNORECASE_STRING_MATCH_TYPE
from .general import *


EDX_FILE_ASSET_GENUS_TYPE = Type(**ASSET_GENUS_TYPES['edx-file-asset'])
EDX_IMAGE_ASSET_GENUS_TYPE = Type(**ASSET_GENUS_TYPES['edx-image-asset'])
EDX_ITEM_RECORD_TYPE = Type(**ITEM_RECORD_TYPES['edx_item'])
EDX_ITEM_MULTIPLE_CHOICE_TYPE = Type(**ITEM_RECORD_TYPES['edx_multiple_choice_item'])
EDX_MULTI_CHOICE_PROBLEM_TYPE = Type(**ITEM_GENUS_TYPES['multi-choice-edx'])
EDX_NUMERIC_RESPONSE_PROBLEM_GENUS_TYPE = Type(**ITEM_GENUS_TYPES['numeric-response-edx'])
GENERIC_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['generic'])
ITEM_WITH_WRONG_ANSWERS_RECORD_TYPE = Type(**ITEM_RECORD_TYPES['wrong-answer'])
JAVASCRIPT_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['javascript'])
JPG_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['jpg'])
JSON_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['json'])
LATEX_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['latex'])
PNG_ASSET_CONTENT_GENUS_TYPE = Type(**ASSET_CONTENT_GENUS_TYPES['png'])
REVIEWABLE_OFFERED = Type(**ASSESSMENT_OFFERED_RECORD_TYPES['review-options'])

ANSWER_WITH_FEEDBACK = Type(**ANSWER_RECORD_TYPES['answer-with-feedback'])
RIGHT_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['right-answer'])
WRONG_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['wrong-answer'])

SIMPLE_SEQUENCE_ASSESSMENT_RECORD_TYPE = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])
FBW_PHASE_I_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-i'])
FBW_PHASE_II_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-ii'])
OBJECTIVE_BASED_ASSESSMENT_PART_RECORD = Type(**ASSESSMENT_PART_RECORD_TYPES['objective-based'])
MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING = Type(**ASSESSMENT_PART_RECORD_TYPES['scaffold-down'])
SIMPLE_SEQUENCE_PART_RECORD_TYPE = Type(**ASSESSMENT_PART_RECORD_TYPES['simple-child-sequencing'])
LO_ASSESSMENT_SECTION = Type(**ASSESSMENT_PART_GENUS_TYPES['fbw-specify-lo'])


class DLEncoder(json.JSONEncoder):
    """
    Custom JSON encoder for DLKit objects
    """
    def default(self, obj):
        if (isinstance(obj, abc_repository_objects.Asset) or
            isinstance(obj, abc_learning_objects.Activity) or
            isinstance(obj, abc_learning_objects.Objective) or
            isinstance(obj, abc_grading_objects.Grade) or
            isinstance(obj, abc_assessment_objects.Answer) or
            isinstance(obj, abc_assessment_objects.Bank) or
            isinstance(obj, abc_assessment_objects.Item) or
            isinstance(obj, abc_assessment_objects.Assessment) or
            isinstance(obj, abc_assessment_objects.AssessmentOffered) or
            isinstance(obj, abc_assessment_objects.AssessmentTaken)):
            return dl_dumps(obj.object_map)
        elif (isinstance(obj, abc_repository_objects.AssetList) or
            isinstance(obj, abc_repository_objects.AssetContentList) or
            isinstance(obj, abc_learning_objects.ActivityList) or
            isinstance(obj, abc_learning_objects.ObjectiveList) or
            isinstance(obj, abc_type_objects.TypeList) or
            isinstance(obj, abc_grading_objects.GradeList) or
            isinstance(obj, abc_assessment_objects.AnswerList) or
            isinstance(obj, abc_assessment_objects.BankList) or
            isinstance(obj, abc_assessment_objects.ItemList) or
            isinstance(obj, abc_assessment_objects.AssessmentList) or
            isinstance(obj, abc_assessment_objects.AssessmentOfferedList) or
            isinstance(obj, abc_assessment_objects.AssessmentTakenList)):
            result = []
            for o in obj:
                result.append(o.object_map)
            return dl_dumps(result)
        elif (isinstance(obj, abc_learning_objects.ObjectiveBank)):
            return dl_dumps(obj._catalog.object_map)
        elif (isinstance(obj, abc_learning_objects.ObjectiveBankList)):
            result = []
            for o in obj:
                result.append(o._catalog.object_map)
            return dl_dumps(result)
        elif (isinstance(obj, list)):
            result = []
            for o in obj:
                result.append(o.object_map)
            return dl_dumps(result)
        else:
            return dl_dumps(obj.object_map)
        return json.JSONEncoder.default(self, obj)


class DLSerializer(DefaultObjectSerializer):
    def to_native(self, obj):
        import timeit
        import logging
        #logging.info(timeit.timeit(obj[0].get_object_map))
        results = []
        for item in obj:
            try:
                item_map = item.object_map
                # if 'fileIds' in item_map:
                #     item_map.update({
                #         'files' : item.get_files()
                #     })
                results.append(item_map)
            except:
                results.append(item)
        return results

class DLPaginationSerializer(PaginationSerializer):
    """To return an object's object_map instead of the __dict__ or dir() values that
    the built-in serializer returns

    """
    class Meta:
        object_serializer_class = DLSerializer

def add_file_ids_to_form(form, file_ids):
    """
    Add existing asset_ids to a form
    :param form:
    :param image_ids:
    :return:
    """
    for label, file_id in file_ids.iteritems():
        form.add_asset(Id(file_id['assetId']),
                       asset_content_id=Id(file_id['assetContentId']),
                       label=label,
                       asset_content_type=Id(file_id['assetContentTypeId']))
    return form

def add_files_to_form(form, files):
    """
    Whether an item form or a question form
    :param form:
    :return:
    """
    def _clean(label):
        return re.sub(r'[^\w\d]', '_', label)

    def _get_file_label(file_path):
        # http://stackoverflow.com/questions/678236/how-to-get-the-filename-without-the-extension-from-a-path-in-python
        return os.path.splitext(os.path.basename(file_path))[0]

    def _get_file_extension(file_name):
        return os.path.splitext(os.path.basename(file_name))[-1]

    def _infer_display_name(text):
        text = text.strip()
        if text == '':
            return 'Unknown Display Name'
        if '_' not in text:
            return text

        if text.split('_')[0].startswith('lec'):
            first_part = text.split('_')[0]
            text = 'Lecture ' + first_part.split('lec')[-1] + '_' + text.split('_')[-1]
        elif text.split('_')[0].startswith('ps'):
            first_part = text.split('_')[0]
            text = 'Problem Set ' + first_part.split('ps')[-1] + '_' + text.split('_')[-1]
        elif text.split('_')[0].startswith('ex'):
            first_part = text.split('_')[0]
            text = 'Exam ' + first_part.split('ex')[-1] + '_' + text.split('_')[-1]

        if text.split('_')[-1].startswith('p'):
            second_part = text.split('_')[-1]
            text = text.split('_')[0] + ': Problem ' + second_part.split('p')[-1]
        elif text.split('_')[-1].startswith('Q'):
            second_part = text.split('_')[-1]
            text = text.split('_')[0] + ': Question ' + second_part.split('Q')[-1]
        return text

    for file_name, file_data in files.iteritems():
        # default assume is a file
        prettify_type = 'file'
        genus = EDX_FILE_ASSET_GENUS_TYPE
        file_type = _get_file_extension(file_name).lower()
        label = _get_file_label(file_name)
        if file_type:
            if 'png' in file_type:
                ac_genus_type = PNG_ASSET_CONTENT_GENUS_TYPE
                genus = EDX_IMAGE_ASSET_GENUS_TYPE
                prettify_type = 'image'
            elif 'jpg' in file_type:
                ac_genus_type = JPG_ASSET_CONTENT_GENUS_TYPE
                genus = EDX_IMAGE_ASSET_GENUS_TYPE
                prettify_type = 'image'
            elif 'json' in file_type:
                ac_genus_type = JSON_ASSET_CONTENT_GENUS_TYPE
            elif 'tex' in file_type:
                ac_genus_type = LATEX_ASSET_CONTENT_GENUS_TYPE
            elif 'javascript' in file_type:
                ac_genus_type = JAVASCRIPT_ASSET_CONTENT_GENUS_TYPE
            else:
                ac_genus_type = GENERIC_ASSET_CONTENT_GENUS_TYPE
        else:
            ac_genus_type = GENERIC_ASSET_CONTENT_GENUS_TYPE

        display_name = _infer_display_name(label) + ' ' + prettify_type.title()
        description = ('Supporting ' + prettify_type + ' for assessment Question: ' +
                       _infer_display_name(label))
        form.add_file(file_data,
                      label=_clean(label),
                      asset_type=genus,
                      asset_content_type=ac_genus_type,
                      asset_name=display_name,
                      asset_description=description)
    return form

def add_sections_to_assessment(am, bank, assessment_id, data):
    for section in data:
        part_form = bank.get_assessment_part_form_for_create_for_assessment(assessment_id,
                                                                            [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                             SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        if 'learningObjectiveId' in section or 'minimumProficiency' in section:
            if 'learningObjectiveId' in section:
                part_form.set_learning_objective_id(clean_id(section['learningObjectiveId']))
            if 'minimumProficiency' in section:
                part_form.set_minimum_proficiency(clean_id(section['minimumProficiency']))

        part = bank.create_assessment_part_for_assessment(part_form)
        if 'type' not in section and 'itemIds' in section:
            if 'scaffold' in section and section['scaffold']:
                for item_id in section['itemIds']:
                    # need to set the bankId on the magic part
                    item_bank = get_object_bank(am,
                                                object_id=clean_id(item_id),
                                                object_type='item')
                    magic_part_form = bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                                   [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                    SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                    magic_part_form.display_name = 'magic part!'
                    magic_part_form.set_item_ids([clean_id(item_id)])
                    magic_part_form.set_item_bank_id(item_bank.ident)

                    if 'quota' in section:
                        magic_part_form.set_waypoint_quota(int(section['quota']))

                    bank.create_assessment_part_for_assessment_part(magic_part_form)
            else:
                for item_id in section['itemIds']:
                    bank.add_item(clean_id(item_id), part.ident)
        elif 'type' in section and section['type'] == str(LO_ASSESSMENT_SECTION):
            # need to set the bankId on the magic part
            # so need this from the client
            for i in range(0, int(section['quota'])):
                magic_part_form = bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                               [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                magic_part_form.display_name = 'magic part!'
                magic_part_form.set_item_bank_id(clean_id(section['itemBankId']))
                magic_part_form.set_learning_objective_ids([clean_id(section['learningObjectiveId'])])
                if 'waypointQuota' in section:
                    waypoint_quota = section['waypointQuota']
                else:
                    waypoint_quota = 1
                magic_part_form.set_waypoint_quota(waypoint_quota)

                bank.create_assessment_part_for_assessment_part(magic_part_form)

def check_assessment_has_items(bank, assessment_id):
    """
    Before creating an assessment offered, check that the assessment
    has items in it.
    :param assessment_id:
    :return:
    """
    items = bank.get_assessment_items(assessment_id)
    if not items.available():
        raise LookupError('No items')

def clean_up_post(bank, item):
    if bank and item:
        if isinstance(item, abc_assessment_objects.Item):
            bank.delete_item(item.ident)
        elif isinstance(item, abc_assessment_objects.Assessment):
            bank.delete_assessment(item.ident)
        elif isinstance(item, abc_assessment_objects.Answer):
            bank.delete_answer(item.ident)

def config_item_querier(querier, params):
    # make sure that max > min
    for field in ['difficulty','discrimination']:
        if 'max_' + field in params and 'min_' + field in params:
            if float(params['max_' + field]) < float(params['min_' + field]):
                raise IntegrityError('max_{0} cannot be less than min_{0}'.format(field))

    if 'max_difficulty' in params:
        querier.match_maximum_difficulty(float(params['max_difficulty']), True)

    if 'min_difficulty' in params:
        querier.match_minimum_difficulty(float(params['min_difficulty']), True)

    if 'max_discrimination' in params:
        querier.match_maximum_discrimination(float(params['max_discrimination']), True)

    if 'min_discrimination' in params:
        querier.match_minimum_discrimination(float(params['min_discrimination']), True)

    querier = config_osid_object_querier(querier, params)

    if 'learning_objective' in params:
        if '@' in params['learning_objective']:
            search_id = quote(params['learning_objective'])
        else:
            search_id = params['learning_objective']
        querier.match_learning_objective_id(search_id, True)

    if 'genus_type_id' in params:
        querier.match_genus_type(Type(params['genus_type_id']), True)

    return querier

def config_osid_object_querier(querier, params):
    if 'display_name' in params:
        querier.match_display_name(str(params['display_name']),
                                   WORDIGNORECASE_STRING_MATCH_TYPE,
                                   True)

    if 'description' in params:
        querier.match_description(str(params['description']),
                                  WORDIGNORECASE_STRING_MATCH_TYPE,
                                  True)

    if 'genus_type_id' in params:
        querier.match_genus_type(Type(params['genus_type_id']), True)

    if 'genusTypeId' in params:
        if '@' in params['genusTypeId']:
            params['genusTypeId'] = quote(params['genusTypeId'])
        querier.match_genus_type(Type(params['genusTypeId']), True)

    return querier

def convert_to_items_uri(request, item_id):
    """
    When items first appear in assessments, they should also present
    a link to the actual "item" url, to view things like answers
    and questions. So a method is needed to modify the URI from
    an /assessments/ one to an /items/ one
    """
    full_url = request.build_absolute_uri()
    assessments_index = full_url.find('/assessments/')
    url_start = full_url[0:assessments_index]
    return url_start + '/items/' + item_id + '/'

def create_new_assessment(am, bank, data):
    # default
    form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD_TYPE])

    if 'recordTypeIds' in data:
        #for now, only handle one record type id in a list
        if str(FBW_PHASE_I_ASSESSMENT_RECORD) in data['recordTypeIds']:
            form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD_TYPE,
                                                        FBW_PHASE_I_ASSESSMENT_RECORD])
            if 'hasSpawnedFollowOnPhase' in data:
                form.set_follow_on_phase_state(bool(data['hasSpawnedFollowOnPhase']))
        elif str(FBW_PHASE_II_ASSESSMENT_RECORD) in data['recordTypeIds']:
            form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD_TYPE,
                                                        FBW_PHASE_II_ASSESSMENT_RECORD])
            if 'sourceAssessmentTakenId' in data:
                form.set_source_assessment_taken_id(clean_id(data['sourceAssessmentTakenId']))

    form = set_form_basics(form, data)

    new_assessment = bank.create_assessment(form)

    # if sections are defined, then this is a complicated assessment
    # and we need to create parts!
    if 'sections' in data:
        for section in data['sections']:
            part_form = bank.get_assessment_part_form_for_create_for_assessment(new_assessment.ident,
                                                                                [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                                 SIMPLE_SEQUENCE_PART_RECORD_TYPE])
            if 'learningObjectiveId' in section or 'minimumProficiency' in section:
                if 'learningObjectiveId' in section:
                    part_form.set_learning_objective_id(clean_id(section['learningObjectiveId']))
                if 'minimumProficiency' in section:
                    part_form.set_minimum_proficiency(clean_id(section['minimumProficiency']))

            part = bank.create_assessment_part_for_assessment(part_form)

            if 'type' not in section and 'itemIds' in section:
                # keep this for original FbW mission format
                # if "scaffold" is True in this data, then we need to make each
                # item in a child / magic assessment part. Otherwise, just
                # stick them in the part directly
                if 'scaffold' in section and section['scaffold']:
                    for item_id in section['itemIds']:
                        # need to set the bankId on the magic part
                        item_bank = get_object_bank(am,
                                                    object_id=clean_id(item_id),
                                                    object_type='item')
                        magic_part_form = bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                                            [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                             SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                        magic_part_form.display_name = 'magic part!'
                        magic_part_form.set_item_ids([clean_id(item_id)])
                        magic_part_form.set_item_bank_id(item_bank.ident)

                        if 'quota' in section:
                            magic_part_form.set_waypoint_quota(int(section['quota']))

                        bank.create_assessment_part_for_assessment_part(magic_part_form)
                else:
                    for item_id in section['itemIds']:
                        bank.add_item(clean_id(item_id), part.ident)
            elif 'type' in section and section['type'] == str(LO_ASSESSMENT_SECTION):
                # need to set the bankId on the magic part
                # so need this from the client
                for i in range(0, int(section['quota'])):
                    magic_part_form = bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                                        [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                    magic_part_form.display_name = 'magic part!'
                    magic_part_form.set_item_bank_id(clean_id(section['itemBankId']))
                    magic_part_form.set_learning_objective_ids([clean_id(section['learningObjectiveId'])])
                    if 'waypointQuota' in section:
                        waypoint_quota = section['waypointQuota']
                    else:
                        waypoint_quota = 1
                    magic_part_form.set_waypoint_quota(waypoint_quota)

                    bank.create_assessment_part_for_assessment_part(magic_part_form)
    else:
        # basic assessment authoring
        # if item IDs are included in the assessment, append them.
        if 'itemIds' in data:
            if isinstance(data, QueryDict):
                items = data.getlist('itemIds')
            elif isinstance(data['itemIds'], basestring):
                items = json.loads(data['itemIds'])
            else:
                items = data['itemIds']

            if not isinstance(items, list):
                try:
                    clean_id(items)  # use this as proxy to test if a valid OSID ID
                    items = [items]
                except:
                    raise InvalidArgument

            for item_id in items:
                try:
                    bank.add_item(new_assessment.ident, clean_id(item_id))
                except:
                    raise NotFound()

    # attach any assessment offerings or taken to the new object
    if 'offerings' in data:
        set_assessment_offerings(bank,
                                 data['offerings'],
                                 new_assessment.ident)
    if 'assessmentsOffered' in data:
        set_assessment_offerings(bank,
                                 data['assessmentsOffered'],
                                 new_assessment.ident)

    return new_assessment

def create_new_item(bank, data):
    if ('question' in data and
            'type' in data['question'] and
            'edx' in data['question']['type']):
        # should have body / setup
        # should have list of choices
        # should have set of right answers
        # metadata (if not present, it is okay):
        #  * max attempts
        #  * weight
        #  * showanswer
        #  * rerandomize
        #  * author username
        #  * student display name
        #  * author comments
        #  * extra python script
        # any files?
        if 'multi-choice' in data['question']['type']:
            form = bank.get_item_form_for_create([EDX_ITEM_MULTIPLE_CHOICE_TYPE,
                                                  ITEM_WITH_WRONG_ANSWERS_RECORD_TYPE])
        else:
            form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE,
                                                  ITEM_WITH_WRONG_ANSWERS_RECORD_TYPE])
        form.display_name = data['name']
        form.description = data['description']
        question_type = data['question']['type']
        if 'multi-choice' in question_type:
            form.set_genus_type(EDX_MULTI_CHOICE_PROBLEM_TYPE)
        elif 'numeric-response' in question_type:
            form.set_genus_type(EDX_NUMERIC_RESPONSE_PROBLEM_GENUS_TYPE)

        if 'learningObjectiveIds' in data:
            form = set_item_learning_objectives(data, form)

        expected = ['question']
        verify_keys_present(data, expected)

        expected = ['questionString']
        verify_keys_present(data['question'], expected)

        form.add_text(data['question']['questionString'], 'questionString')

        optional = ['python_script','latex','edxml','solution']
        for opt in optional:
            if opt in data:
                form.add_text(data[opt], opt)

        metadata = ['attempts','markdown','showanswer','weight']
        # metadata = ['attempts','markdown','rerandomize','showanswer','weight']
                    # 'author','author_comments','student_display_name']
        for datum in metadata:
            if datum in data:
                method = getattr(form, 'add_' + datum)
                method(data[datum])

        irt = ['difficulty','discrimination']
        for datum in irt:
            if datum in data:
                method = getattr(form, 'set_' + datum + '_value')
                method(data[datum])

        if 'files' in data:
            files_list = {}
            for filename, file in data['files'].iteritems():
                files_list[filename] = DataInputStream(file)
            form = add_files_to_form(form, files_list)
    else:
        form = bank.get_item_form_for_create([ITEM_WITH_WRONG_ANSWERS_RECORD_TYPE])
        form.display_name = str(data['name'])
        form.description = str(data['description'])
        if 'genus' in data:
            form.set_genus_type(Type(data['genus']))

        if 'learningObjectiveIds' in data:
            form = set_item_learning_objectives(data, form)

    new_item = bank.create_item(form)
    return new_item

def delete_part_and_children(bank, part_id):
    apls = get_assessment_part_lookup_session(runtime=bank._osid_object._runtime,
                                              proxy=bank._osid_object._proxy)
    apls.use_federated_bank_view()
    apls.use_unsequestered_assessment_part_view()
    part = apls.get_assessment_part(part_id)
    try:
        for child_id in part.get_child_ids():
            delete_part_and_children(bank, child_id)
    except IllegalState:
        # no child ids
        pass
    bank.delete_assessment_part(part.ident)

def find_answer_in_answers(ans_id, ans_list):
    for ans in ans_list:
        if ans.ident == ans_id:
            return ans
    return None

def get_active_bank(request, bank_id):
    if not isinstance(bank_id, basestring):
        bank_id = str(bank_id)
    existing_bank = get_session_data(request, 'bank')
    if existing_bank:
        if str(existing_bank.get_id()) == bank_id:
            bank = existing_bank
        else:
            bank = get_bank(request, bank_id)
    else:
        bank = get_bank(request, bank_id)
    return bank

def get_answer_records(answer):
    """answer is a dictionary"""
    # check for wrong-answer genus type to get the right
    # record types for feedback
    a_type = Type(answer['type'])
    if 'genus' in answer and answer['genus'] == str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])):
        a_types = [a_type, Type(**ANSWER_RECORD_TYPES['answer-with-feedback'])]
    else:
        a_types = [a_type]
    return a_types

def get_bank(request, identifier=None):
    try:
        am = get_session_data(request, 'am')
        result = None
        if (isinstance(identifier, basestring) or
            isinstance(identifier, Id)):
            identifier = convert_to_osid_id(identifier)
            result = am.get_bank(identifier)
        else:
            result = am.get_banks()
    except Exception as ex:
        log_error('assessments.utilities.get_bank()', ex)
        result = None
    finally:
        set_session_data(request, 'am', am)
        return result

def get_choice_files(files):
    """
    Adapted from http://stackoverflow.com/questions/4558983/slicing-a-dictionary-by-keys-that-start-with-a-certain-string
    :param files:
    :return:
    """
    # return {k:v for k,v in files.iteritems() if k.startswith('choice')}
    return dict((k, files[k]) for k in files.keys() if k.startswith('choice'))

def get_next_question(bank, section, question):
    # don't check with has_next_question() because then
    # it will take double the time. Just handle the
    # IllegalState here
    try:
        next_question = bank.get_next_question(section.ident, question.ident)
        return next_question.object_map
    except IllegalState:
        return None

def get_object_bank(manager, object_id, object_type='item', bank_id=None):
    """Get the object's bank even without the bankId"""
    # primarily used for Item and AssessmentsOffered
    if bank_id is None:
        if isinstance(object_id, basestring):
            object_id = clean_id(object_id)
        lookup_session = getattr(manager,
                                 'get_{0}_lookup_session'.format(object_type))(proxy=manager._proxy)
        lookup_session.use_federated_bank_view()
        object_ = getattr(lookup_session, 'get_{0}'.format(object_type))(object_id)
        bank_id = object_.object_map['assignedBankIds'][0]
    if isinstance(bank_id, basestring):
        bank_id = clean_id(bank_id)
    return manager.get_bank(bank_id)

def get_object_bank_from_request(request):
    """parse out the right params before passing to get_object_bank
    Do NOT use the convenience methods here with session, etc."""
    valid_params = ['/assessment/items/', '/assessment/assessmentsoffered/']
    path = request.path
    if any(p in path for p in valid_params):
        test_request = SimpleRequest(username=request.META.get('HTTP_X_API_PROXY', ''))
        condition = PROXY_SESSION.get_proxy_condition()
        condition.set_http_request(test_request)
        proxy = PROXY_SESSION.get_proxy(condition)
        am = RUNTIME.get_service_manager('ASSESSMENT', proxy=proxy)
        try:
            if '/items/' in path:
                object_id = path.split('/items/')[-1].replace('/', '')
                if object_id not in ['', None]:
                    return get_object_bank(am, object_id, object_type='item')
                else:
                    return None
            else:
                object_id = path.split('/assessmentsoffered/')[-1].replace('/', '')
                if object_id not in ['', None]:
                    return get_object_bank(am, object_id, object_type='assessment_offered')
                else:
                    return None
        except (NotFound, PermissionDenied):
            return None
    else:
        return None

def get_ovs_file_set(files, index):
    choice_files = get_choice_files(files)
    if len(choice_files.keys()) % 2 != 0:
        raise NullArgument('Large and small image files')
    small_file = choice_files['choice' + str(index) + 'small']
    big_file = choice_files['choice' + str(index) + 'big']
    return (small_file, big_file)

def get_question_and_section(bank, taken_id, question_id=None):
    if 'assessment.AssessmentTaken' in taken_id:
        first_section = bank.get_first_assessment_section(clean_id(taken_id))
        # with caching, have to re-get the section to get its updated information...
        first_section = bank.get_assessment_section(first_section.ident)

        question = None
        real_section = first_section
        while question is None:
            try:
                if question_id is None:
                    question = bank.get_first_unanswered_question(first_section.ident)
                else:
                    if isinstance(question_id, basestring):
                        question_id = clean_id(question_id)
                    question = bank.get_question(real_section.ident,
                                                 question_id)
            except (IllegalState, NotFound):
                # IllegalState when no more unanswered questions
                # NotFound if the question is not in the section
                real_section = bank.get_next_assessment_section(real_section.ident)
    elif 'assessment.AssessmentSection' in taken_id:
        real_section = bank.get_assessment_section(clean_id(taken_id))
        question = bank.get_question(real_section.ident,
                                     clean_id(question_id))
    else:
        raise InvalidArgument()
    return question, real_section

def get_question_status(bank, section, question_id):
    """
    Return the question status of answered or not, and if so, right or wrong
    :param bank:
    :param section:
    :param question:
    :return:
    """
    # student_response = bank.get_response(section.ident, question_id)
    student_response = section.get_response(question_id)
    if student_response.is_answered():
        # Now need to actually check the answers against the
        # item answers.
        try:
            correct = student_response.is_correct()
        except (AttributeError, IllegalState):
            answers = bank.get_answers(section.ident, question_id)
            # compare these answers to the submitted response
            response = student_response._my_map
            response.update({
                'type' : str(response['recordTypeIds'][0]).replace('answer-record-type', 'answer-record-type')
            })
            correct = validate_response(student_response._my_map, answers)
        data = {
            'responded': True,
            'isCorrect': correct
        }
    else:  # no responses
        data = {
            'responded': False
        }
    return data

def get_response_map(bank, section, question_id):
    # append the student's last response and status if available
    if section.is_question_answered(question_id):
        # response = bank.get_response(section.ident, question_id)
        response = section.get_response(question_id)
        response_map = response.object_map
        if response_map['isCorrect'] is None:
            # to make this work with validate_response()
            response_map['type'] = response_map['recordTypeIds']
            correct = validate_response(response_map,
                                        bank.get_answers(section.ident,
                                                         question_id))
            response_map.update({
                'isCorrect': correct
            })

        try:
            response_map['confusedLearningObjectiveIds'] = [str(lo) for lo in section.get_confused_learning_objective_ids(question_id)]
        except IllegalState:
            pass
        try:
            response_map['feedback'] = update_json_response_with_feedback(bank,
                                                                          section,
                                                                          question_id,
                                                                          response_map['isCorrect'])
        except IllegalState:
            pass
    else:
        response_map = None  # no response
    return response_map

def get_response_submissions(response):
    if is_label_ortho_faces(response):
        submission = response['integerValues']
    elif is_multiple_choice(response):
        if isinstance(response, dict):
            submission = response['choiceIds']
        else:
            submission = response.getlist('choiceIds')
    elif is_numeric_response(response):
        submission = float(response['decimalValue'])
    else:
        raise Unsupported
    return submission

def get_taken_section_map(taken, update=False, with_files=False, bank=None):
    # let's get the questions first ... we need to inject that information into
    # the response, so that UI side we can match on the original, canonical itemId
    # also need to include the questions's learningObjectiveIds
    def section_map(_section):
        s_map = _section.object_map
        s_map['id'] = str(_section.ident)
        s_map['type'] = 'AssessmentSection'

        if update:
            # this should get called only when "taking" assessments, and not for results
            # This assumes that section object maps now get questions via the mixins.py
            question_maps = []

            questions = _section.get_questions(update=update)
            for index, question in enumerate(questions):
                question_map = question.object_map
                if with_files:
                    question_map['files'] = question.get_files()
                response_map = get_response_map(bank,
                                                _section,
                                                question.ident)
                responded = False
                if response_map is not None:
                    responded = True
                question_map.update({
                    'itemId': _section._my_map['questions'][index]['itemId'],
                    'response': response_map,
                    'responded': responded
                })
                if responded:
                    question_map['isCorrect'] = response_map['isCorrect']
                question_maps.append(question_map)
            s_map['questions'] = question_maps

        return s_map

    section_maps = []

    try:
        sections = taken._get_assessment_sections()
        section_maps = [section_map(s) for s in sections]
    except KeyError:
        # no sections -- never got the question
        pass

    return section_maps

def is_label_ortho_faces(response):
    if isinstance(response['type'], list):
        return any(lof in r
                   for r in response['type']
                   for lof in ['label-ortho-faces'])
    else:
        return any(lof in response['type'] for lof in ['label-ortho-faces'])

def is_multiple_choice(response):
    if isinstance(response['type'], list):
        return any(mc in r
                   for r in response['type']
                   for mc in ['multi-choice-ortho',
                              'multi-choice-edx',
                              'multi-choice-with-files-and-feedback',
                              'multi-choice-fbw'])
    else:
        return any(mc in response['type'] for mc in ['multi-choice-ortho',
                                                     'multi-choice-edx',
                                                     'multi-choice-with-files-and-feedback',
                                                     'multi-choice-fbw'])

def is_numeric_response(response):
    if 'type' in response:
        if isinstance(response['type'], list):
            return any(num in r
                       for r in response['type']
                       for num in ['numeric-response-edx'])
        else:
            return response['type'] == 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
    elif 'genusTypeId' in response:
        return response['genusTypeId'] == 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
    else:
        return False

def is_right_answer(answer):
    return (answer.genus_type == Type(**ANSWER_GENUS_TYPES['right-answer']) or
            str(answer.genus_type).lower() == 'genustype%3adefault%40dlkit.mit.edu')

def set_answer_form_genus_and_feedback(answer, answer_form):
    """answer is a dictionary"""
    if 'genus' in answer:
        answer_form.genus_type = Type(answer['genus'])
        if answer['genus'] == str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])):
            if 'feedback' in answer:
                if str(ANSWER_WITH_FEEDBACK) not in answer_form._my_map['recordTypeIds']:
                    # this is so bad. Don't do this normally.
                    answer_form._for_update = False
                    record = answer_form.get_answer_form_record(ANSWER_WITH_FEEDBACK)
                    record._init_metadata()
                    record._init_map()
                    answer_form._for_update = True
                answer_form.set_feedback(str(answer['feedback']))
            if 'confusedLearningObjectiveIds' in answer:
                if not isinstance(answer['confusedLearningObjectiveIds'], list):
                    los = [answer['confusedLearningObjectiveIds']]
                else:
                    los = answer['confusedLearningObjectiveIds']
                answer_form.set_confused_learning_objective_ids(los)
    else:
        # default is correct answer, if not supplied
        answer_form.set_genus_type(Type(**ANSWER_GENUS_TYPES['right-answer']))
        try:
            # remove the feedback components
            del answer_form._my_map['texts']['feedback']
            del answer_form._my_map['recordTypeIds'][str(Type(**ANSWER_RECORD_TYPES['answer-with-feedback']))]
        except KeyError:
            pass
    return answer_form

def set_assessment_offerings(bank, offerings, assessment_id, update=False):
    return_data = []
    for offering in offerings:
        if isinstance(offering, basestring):
            offering = json.loads(offering)

        if update:
            offering_form = bank.get_assessment_offered_form_for_update(assessment_id)
            execute = bank.update_assessment_offered
        else:
            # use our new Offered Record object, which lets us do
            # "can_review_whether_correct()" on the Taken.
            offering_form = bank.get_assessment_offered_form_for_create(assessment_id,
                                                                        [REVIEWABLE_OFFERED])
            execute = bank.create_assessment_offered

        if 'duration' in offering:
            if isinstance(offering['duration'], basestring):
                duration = json.loads(offering['duration'])
            else:
                duration = offering['duration']
            offering_form.duration = Duration(**duration)
        if 'gradeSystem' in offering:
            offering_form.grade_system = Id(offering['gradeSystem'])
        if 'level' in offering:
            offering_form.level = Id(offering['level'])

        if 'deadline' in offering:
            if isinstance(offering['deadline'], basestring):
                deadline = json.loads(offering['deadline'])
            else:
                deadline = offering['deadline']
            offering_form.deadline = DateTime(**deadline)

        if 'startTime' in offering:
            if isinstance(offering['startTime'], basestring):
                start_time = json.loads(offering['startTime'])
            else:
                start_time = offering['startTime']
            offering_form.start_time = DateTime(**start_time)
        if 'scoreSystem' in offering:
            offering_form.score_system = Id(offering['scoreSystem'])

        if 'reviewOptions' in offering and 'whetherCorrect' in offering['reviewOptions']:
            for timing, value in offering['reviewOptions']['whetherCorrect'].iteritems():
                offering_form.set_review_whether_correct(**{underscore(timing) : value})

        if 'reviewOptions' in offering and 'solution' in offering['reviewOptions']:
            for timing, value in offering['reviewOptions']['solution'].iteritems():
                offering_form.set_review_solution(**{underscore(timing) : value})

        if 'maxAttempts' in offering:
            offering_form.set_max_attempts(offering['maxAttempts'])

        new_offering = execute(offering_form)
        return_data.append(new_offering)
    return return_data

def set_item_learning_objectives(data, form):
    # over-writes current ID list
    id_list = []
    if not isinstance(data['learningObjectiveIds'], list):
        data['learningObjectiveIds'] = [data['learningObjectiveIds']]
    for _id in data['learningObjectiveIds']:
        if '@' in _id:
            id_list.append(Id(quote(_id)))
        else:
            id_list.append(Id(_id))
    form.set_learning_objectives(id_list)
    return form

def supported_types():
    results = []
    results.append({
        'displayName'       : {
            'text'  : 'Match Ortho Faces'
        },
        'description'       : {
            'text'  : 'Match the manipulatable object to the given faces.'
        },
        'id'                : 'question%3Amatch-ortho-faces%40ODL.MIT.EDU',
        'recordType'        : 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
    })
    results.append({
        'displayName'       : {
            'text'  : 'Define Ortho Faces'
        },
        'description'       : {
            'text'  : 'Define the best plane, top, and side faces of the manipulatable object.'
        },
        'id'                : 'question%3Adefine-ortho-faces%40ODL.MIT.EDU',
        'recordType'        : 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
    })
    results.append({
        'displayName'       : {
            'text'  : 'Select Matching Ortho View Set'
        },
        'description'       : {
            'text'  : 'From the choices provided, select the ortho view set that matches the given manipulatable.'
        },
        'id'                : 'question%3Achoose-viewset%40ODL.MIT.EDU',
        'recordType'        : 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
    })
    results.append({
        'displayName'       : {
            'text'  : 'Select Matching Manipulatable'
        },
        'description'       : {
            'text'  : 'From the choices provided, select the manipulatable that matches the given ortho view set.'
        },
        'id'                : 'question%3Achoose-manip%40ODL.MIT.EDU',
        'recordType'        : 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
    })
    results.append({
        'displayName'       : {
            'text'  : 'edX Multiple Choice'
        },
        'description'       : {
            'text'  : 'Multiple Choice question for edX.'
        },
        'id'                : 'question%3Amulti-choice-edx%40ODL.MIT.EDU',
        'recordType'        : 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
    })
    return results

def update_answer_form(answer, form, question=None):
    if answer['type'] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
        if 'responseString' in answer:
            form.set_text(answer['responseString'])
    elif is_label_ortho_faces(answer):
        if 'integerValues' in answer:
            form.set_face_values(front_face_value=answer['integerValues']['frontFaceValue'],
                                 side_face_value=answer['integerValues']['sideFaceValue'],
                                 top_face_value=answer['integerValues']['topFaceValue'])
    elif answer['type'] == 'answer-record-type%3Aeuler-rotation%40ODL.MIT.EDU':
        if 'integerValues' in answer:
            form.set_euler_angle_values(x_angle=answer['integerValues']['xAngle'],
                                        y_angle=answer['integerValues']['yAngle'],
                                        z_angle=answer['integerValues']['zAngle'])
    elif (answer['type'] == 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU' or
          answer['type'] == 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'):
        if question is None and 'choiceId' in answer:
            raise InvalidArgument('Missing question parameter for multi-choice')
        if not form.is_for_update():
            verify_keys_present(answer, ['choiceId'])
        if 'choiceId' in answer:
            # need to find the actual choiceIds (MC3 IDs), and match the index
            # to the one(s) passed in as part of the answer
            choices = question.get_choices()
            if int(answer['choiceId']) > len(choices):
                raise KeyError('Correct answer ' + str(answer['choiceId']) + ' is not valid. '
                               'Not that many choices!')
            elif int(answer['choiceId']) < 1:
                raise KeyError('Correct answer ' + str(answer['choiceId']) + ' is not valid. '
                               'Must be between 1 and # of choices.')

            # choices are 0 indexed
            choice_id = choices[int(answer['choiceId']) - 1]  # not sure if we need the OSID Id or string
            form.add_choice_id(choice_id['id'])  # just include the MongoDB ObjectId, not the whole dict
    elif answer['type'] == 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU':
        # no correct answers here...
        return form
    elif answer['type'] == 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU':
        if 'decimalValue' in answer:
            form.set_decimal_value(float(answer['decimalValue']))
        if 'tolerance' in answer:
            form.set_tolerance_value(float(answer['tolerance']))
    else:
        raise Unsupported()

    return form

def update_json_response_with_item_metadata(bank, request_data, response_data, am):
    if 'raw' in request_data:
        iterator = response_data
    else:
        iterator = response_data['data']['results']

    if 'files' in request_data:
        for item in iterator:
            dlkit_item = bank.get_item(clean_id(item['id']))

            if 'fileIds' in item:
                item['files'] = dlkit_item.get_files()
            if item['question'] and 'fileIds' in item['question']:
                item['question']['files'] = dlkit_item.get_question().get_files()

    if 'unrandomized' in request_data:
        for item in iterator:
            dlkit_item = bank.get_item(clean_id(item['id']))
            try:
                item['question']['choices'] = dlkit_item.get_question().get_unrandomized_choices()
            except AttributeError:
                # item is not randomized MC
                pass

    if 'wronganswers' in request_data:
        for item in iterator:
            item_bank = get_object_bank(am, item['id'], 'item')
            dlkit_item = item_bank.get_item(clean_id(item['id']))

            try:
                wrong_answers = dlkit_item.get_wrong_answers()
                wrong_answers_map = []
                for wrong_answer in wrong_answers:
                    wrong_answer_map = convert_dl_object(wrong_answer)
                    if wrong_answer.has_files():
                        wrong_answer_map['files'] = wrong_answer.get_files()
                    wrong_answers_map.append(wrong_answer_map)

                item['answers'] += wrong_answers_map
            except AttributeError:
                pass

    return response_data

def update_json_response_with_feedback(bank, section, question_id, correct):
    """ move this logic out of views, since it's re-used in both Submit endpoints
    :param bank:
    :param data_map:
    :param section:
    :param question:
    :return:
    """
    def get_best_answer_to_use():
        response = bank.get_response(section.ident, question_id)
        response_map = response.object_map
        response_map['type'] = response_map['recordTypeIds']
        submissions = get_response_submissions(response_map)
        answers = bank.get_answers(section.ident, question_id)
        exact_answer_match = None
        default_answer_match = None
        for answer in answers:
            correct_submissions = 0
            answer_choice_ids = list(answer.get_choice_ids())
            number_choices = len(answer_choice_ids)
            if len(submissions) == number_choices:
                for index, choice_id in enumerate(answer_choice_ids):
                    if is_multiple_choice(response_map):
                        if str(choice_id) in submissions:
                            correct_submissions += 1
            if not correct and str(answer.genus_type) == str(WRONG_ANSWER_GENUS):
                # take the first wrong answer by default ... just in case
                # we don't have an exact match
                default_answer_match = answer
            elif correct and str(answer.genus_type) == str(RIGHT_ANSWER_GENUS):
                default_answer_match = answer

            if (correct_submissions == number_choices and
                    len(submissions) == number_choices):
                exact_answer_match = answer
                break

        # now that we have either an exact match or a default (wrong) answer
        # let's calculate the feedback and the confused LOs
        answer_to_use = default_answer_match
        if exact_answer_match is not None:
            answer_to_use = exact_answer_match
        return answer_to_use

    feedback = None
    try:
        taken = section.get_assessment_taken()
        feedback = taken.get_solution_for_question(question_id, section=section)['explanation']
        if isinstance(feedback, basestring):
            feedback = {
                'text': feedback
            }
    except (IllegalState, TypeError, AttributeError):
        # update with answer feedback, if available
        # for now, just support this for multiple choice questions...
        try:
            best_answer_to_use = get_best_answer_to_use()
            feedback = best_answer_to_use.feedback
        except (KeyError, AttributeError, IllegalState, Unsupported):
            pass

    if isinstance(feedback, DisplayText):
        return {
            'text': feedback.text,
            'languageTypeId': str(feedback.language_type),
            'scriptTypeId': str(feedback.script_type),
            'formatTypeId': str(feedback.format_type)
        }
    return feedback


def update_json_response_with_feedback_and_confused_los(bank, data_map, section, question_id, correct):
    """ move this logic out of views, since it's re-used in both Submit endpoints
    :param bank:
    :param data_map:
    :param section:
    :param question:
    :return:
    """
    def get_best_answer_to_use():
        submissions = get_response_submissions(data_map)
        answers = bank.get_answers(section.ident, question_id)
        exact_answer_match = None
        default_answer_match = None
        for answer in answers:
            correct_submissions = 0
            answer_choice_ids = list(answer.get_choice_ids())
            number_choices = len(answer_choice_ids)
            if len(submissions) == number_choices:
                for index, choice_id in enumerate(answer_choice_ids):
                    if is_multiple_choice(data_map):
                        if str(choice_id) in submissions:
                            correct_submissions += 1
            if not correct and str(answer.genus_type) == str(WRONG_ANSWER_GENUS):
                # take the first wrong answer by default ... just in case
                # we don't have an exact match
                default_answer_match = answer
            elif correct and str(answer.genus_type) == str(RIGHT_ANSWER_GENUS):
                default_answer_match = answer

            if (correct_submissions == number_choices and
                    len(submissions) == number_choices):
                exact_answer_match = answer
                break

        # now that we have either an exact match or a default (wrong) answer
        # let's calculate the feedback and the confused LOs
        answer_to_use = default_answer_match
        if exact_answer_match is not None:
            answer_to_use = exact_answer_match
        return answer_to_use

    feedback = None
    confused_los = None
    if not is_multiple_choice(data_map):
        return feedback, confused_los

    best_answer_to_use = None
    # best_answer_to_use = get_best_answer_to_use()
    try:
        taken = section.get_assessment_taken()
        feedback = taken.get_solution_for_question(question_id)['explanation']
        if isinstance(feedback, basestring):
            feedback = {
                'text': feedback
            }
    except (IllegalState, TypeError, AttributeError):
        # update with answer feedback, if available
        # for now, just support this for multiple choice questions...
        if is_multiple_choice(data_map):
            best_answer_to_use = get_best_answer_to_use()
            try:
                feedback = best_answer_to_use.feedback
            except (KeyError, AttributeError):
                pass
    if is_multiple_choice(data_map):
        try:
            confused_los = best_answer_to_use.confused_learning_objective_ids
        except (KeyError, AttributeError):
            pass

    return feedback, confused_los

def update_item_metadata(data, form):
    """Update the metadata / IRT for an edX item

    :param request:
    :param data:
    :param form:
    :return:
    """
    if ('type' in data and
        'edx' in data['type']):
        valid_fields = ['attempts','markdown','showanswer','weight',
                        'difficulty','discrimination']
        for field in valid_fields:
            if field in data:
                if hasattr(form, 'add_' + field):
                    update_method = getattr(form, 'add_' + field)
                elif hasattr(form, 'set_' + field):
                    update_method = getattr(form, 'set_' + field)
                else:
                    update_method = getattr(form, 'set_' + field + '_value')
                # These forms are very strict (Java), so
                # have to know the exact input type. We
                # can't predict, so try a couple variations
                # if this fails...yes we're silly.
                val = data[field]
                try:
                    try:
                        try:
                            update_method(str(val))
                        except:
                            update_method(int(val))
                    except:
                        update_method(float(val))
                except:
                    raise LookupError
    else:
        # do nothing here for other types of problems
        pass

    return form

def update_question_form(request, question, form, create=False):
    """
    Check the create flag--if creating the question, then all 3 viewset files
    are needed. If not creating, can update only a single file.
    """
    if question['type'] == 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
        form.set_text(question['questionString'])
    elif (is_label_ortho_faces(question) or
          question['type'] == 'question-record-type%3Aeuler-rotation%40ODL.MIT.EDU'):
        # need to differentiate on create here because update might not use all
        # the fields, whereas we need to enforce a minimum of data on create
        if create:
            if 'questionString' in question:
                form.set_text(question['questionString'])
            else:
                raise NullArgument('questionString')

            if 'firstAngle' in question:
                form.set_first_angle_projection(question['firstAngle'])

            files = request.FILES
            if 'manip' in files:
                form.set_manip(DataInputStream(files['manip']))
            else:
                raise NullArgument('manip file')
            if not ('frontView' in files and 'sideView' in files and 'topView' in files):
                raise NullArgument('All three view set attribute(s) required for Ortho-3D items.')
            else:
                form.set_ortho_view_set(front_view=DataInputStream(files['frontView']),
                                        side_view=DataInputStream(files['sideView']),
                                        top_view=DataInputStream(files['topView']))
        else:
            if 'questionString' in question:
                form.set_text(question['questionString'])
            if 'firstAngle' in question:
                form.set_first_angle_projection(question['firstAngle'])
            files = request.FILES
            if 'manip' in files:
                form.set_manip(DataInputStream(files['manip']))
            if 'frontView' in files:
                form.set_ovs_view(DataInputStream(files['frontView']), 'frontView')
            if 'sideView' in files:
                form.set_ovs_view(DataInputStream(files['sideView']), 'sideView')
            if 'topView' in files:
                form.set_ovs_view(DataInputStream(files['topView']), 'topView')
    elif question['type'] == 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU':
        # need to differentiate on create here because update might not use all
        # the fields, whereas we need to enforce a minimum of data on create
        if create:
            if 'questionString' in question:
                form.set_text(question['questionString'])
            else:
                raise NullArgument('questionString')

            if 'firstAngle' in question:
                form.set_first_angle_projection(question['firstAngle'])

            files = request.FILES
            if 'manip' in files:
                if 'promptName' in question:
                    manip_name = question['promptName']
                else:
                    manip_name = 'A manipulatable'

                # TODO set the manip name to the question['promptName']
                # and find the right choice / ovs to go with it
                if 'rightAnswer' in question:
                    right_answer_sm, right_answer_lg = get_ovs_file_set(files,
                                                                        question['rightAnswer'])
                    form.set_manip(DataInputStream(files['manip']),
                                   DataInputStream(right_answer_sm),
                                   DataInputStream(right_answer_lg),
                                   manip_name)
                else:
                    form.set_manip(DataInputStream(files['manip']),
                                   name=manip_name)

                if not ('choice0small' in files and 'choice0big' in files):
                    raise NullArgument('At least two choice set attribute(s) required for Ortho-3D items.')
                elif not ('choice1small' in files and 'choice1big' in files):
                    raise NullArgument('At least two choice set attribute(s) required for Ortho-3D items.')
                else:
                    choice_files = get_choice_files(files)
                    if len(choice_files.keys()) % 2 != 0:
                        raise NullArgument('Large and small image files')
                    num_files = len(choice_files.keys()) / 2
                    for i in range(0,num_files):
                        # this goes with the code ~20 lines above, where
                        # the right choice files are saved with the manip...
                        # but, regardless, make a choice for each provided
                        # viewset. Trust the consumer to pair things up
                        # properly. Need the choiceId to set the answer
                        if 'rightAnswer' in question and i == int(question['rightAnswer']):
                            # save this as a choice anyways
                            small_file = DataInputStream(choice_files['choice' + str(i) + 'small'])
                            big_file = DataInputStream(choice_files['choice' + str(i) + 'big'])
                        else:
                            small_file = DataInputStream(choice_files['choice' + str(i) + 'small'])
                            big_file = DataInputStream(choice_files['choice' + str(i) + 'big'])
                        if 'choiceNames' in question:
                            name = question['choiceNames'][i]
                        else:
                            name = ''
                        form.set_ortho_choice(small_asset_data=small_file,
                                              large_asset_data=big_file,
                                              name=name)
            else:
                # is a match the ortho manip, so has choice#manip and
                # primary object of viewset
                raise NullArgument('manip file')

        else:
            if 'questionString' in question:
                form.set_text(question['questionString'])
            if 'firstAngle' in question:
                form.set_first_angle_projection(question['firstAngle'])
            files = request.FILES
            if 'manip' in files:
                form.set_manip(DataInputStream(files['manip']))

            # TODO: change a choice set
    elif question['type'] == 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU':
        if "rerandomize" in question:
            form.add_rerandomize(question['rerandomize'])
        if create:
            expected = ['questionString','choices']
            verify_keys_present(question, expected)

            should_be_list = ['choices']
            verify_min_length(question, should_be_list, 2)

            form.set_text(str(question['questionString']))
            # files get set after the form is returned, because
            # need the new_item
            # now manage the choices
            for ind, choice in enumerate(question['choices']):
                if isinstance(choice, dict):
                    form.add_choice(choice.get('text', ''),
                                    choice.get('name', 'Choice ' + str(int(ind) + 1)))
                else:
                    form.add_choice(choice, 'Choice ' + str(int(ind) + 1))
        else:
            if 'questionString' in question:
                form.set_text(str(question['questionString']))
            if 'choices' in question:
                # delete the old choices first
                for current_choice in form.my_osid_object_form._my_map['choices']:
                    form.clear_choice(current_choice)
                # now add the new ones
                for ind, choice in enumerate(question['choices']):
                    if isinstance(choice, dict):
                        form.add_choice(choice.get('text', ''),
                                        choice.get('name', 'Choice ' + str(int(ind) + 1)))
                    else:
                        form.add_choice(choice, 'Choice ' + str(int(ind) + 1))
    elif question['type'] == 'question-record-type%3Afiles-submission%40ODL.MIT.EDU':
        form.set_text(str(question['questionString']))
    elif question['type'] == 'question-record-type%3Anumeric-response-edx%40ODL.MIT.EDU':
        if create:
            form.set_text(str(question['questionString']))
        else:
            if 'questionString' in question:
                form.set_text(str(question['questionString']))
    else:
        raise Unsupported()

    return form

def update_question_provider(rm, bank, question):
    # check if a providerId is available
    query_session = rm.get_asset_query_session_for_repository(bank.ident,
                                                              proxy=rm._proxy)
    query_form = query_session.get_asset_query()
    query_form.match_enclosed_object_id(clean_id(question['id']))
    query_result = query_session.get_assets_by_query(query_form)
    if query_result.available() > 0:
        asset = query_result.next()
        question.update({
            'providerId': str(asset.provider_id)
        })

def update_response_form(response, form):
    """
    Put the response data into the form and send it back
    :param response: JSON data from user request
    :param form: responseForm, with methods that match the corresponding answerForm
    :return: updated form
    """
    if response['type'] == 'answer-record-type%3Aresponse-string%40ODL.MIT.EDU':
        if 'responseString' in response:
            form.set_response_string(response['responseString'])
    elif is_label_ortho_faces(response):
        if 'integerValues' in response:
            if isinstance(response['integerValues'], basestring):
                values = json.loads(response['integerValues'])
            else:
                values = response['integerValues']
            form.set_face_values(front_face_value=values['frontFaceValue'],
                                 side_face_value=values['sideFaceValue'],
                                 top_face_value=values['topFaceValue'])
    elif is_multiple_choice(response):
        try:
            response['choiceIds'] = response.getlist('choiceIds')
        except Exception:
            pass
        if isinstance(response['choiceIds'], list):
            for choice in response['choiceIds']:
                form.add_choice_id(choice)
        else:
            raise InvalidArgument('ChoiceIds should be a list.')
    elif response['type'] == 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU':
        for file_label, file_data in response['files'].iteritems():
            form.add_file(DataInputStream(file_data), file_label)
    elif response['type'] == 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU':
        if 'decimalValue' in response:
            form.set_decimal_value(float(response['decimalValue']))
        if 'tolerance' in response:
            form.set_tolerance_value(float(response['tolerance']))
    else:
        raise Unsupported()
    return form

def update_with_section_metadata(bank, data):
    if 'data' in data:
        iterator = data['data']['results']
    else:
        iterator = data

    for assessment_map in iterator:
        assessment = bank.get_assessment(clean_id(assessment_map['id']))
        assessment_map['sections'] = []
        for child_id in assessment.get_child_ids():
            assessment_part = bank.get_assessment_part(child_id)
            assessment_map['sections'].append(assessment_part.object_map)

def validate_response(response, answers):
    correct = False
    # for longer submissions / multi-answer questions, need to make
    # sure that all of them match...
    if response['type'] == 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU':
        return True  # always say True because the file was accepted

    submission = get_response_submissions(response)

    if is_multiple_choice(response):
        right_answers = [a for a in answers
                         if is_right_answer(a)]
        num_right = 0
        num_total = 0
        for answer in right_answers:
            num_total += answer.get_choice_ids().available()

        for answer in right_answers:
            for index, choice_id in enumerate(answer.get_choice_ids()):
                if str(choice_id) in submission:  # order doesn't matter
                    num_right += 1
        if num_right > 0 and len(submission) == num_right and num_total >= len(submission):
            correct = True
    else:
        for answer in answers:
            ans_type = answer.object_map['recordTypeIds'][0]
            if ans_type == 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU':
                if isinstance(submission, basestring):
                    submission = json.loads(submission)
                if (int(answer.get_front_face_value()) == int(submission['frontFaceValue']) and
                    int(answer.get_side_face_value()) == int(submission['sideFaceValue']) and
                    int(answer.get_top_face_value()) == int(submission['topFaceValue'])):
                    correct = True
                    break
            elif (ans_type == 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU' or
                  ans_type == 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'):
                if not isinstance(submission, list):
                    raise InvalidArgument('ChoiceIds should be a list, in a student response.')
                if len(submission) == 1:
                    if answer.get_choice_ids()[0] == submission[0]:
                        correct = True
                        break
            elif ans_type == 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU':
                expected = answer.get_decimal()
                tolerance = answer.get_tolerance()
                if (expected - tolerance) <= submission <= (expected + tolerance):
                    correct = True
                    break
    return correct
