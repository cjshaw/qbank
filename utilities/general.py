import re
import pickle
import random
import string
import traceback
import simplejson as json

from bson.errors import InvalidId

from django.http import QueryDict
from django.db import IntegrityError
from django.utils.http import unquote, quote
from django.conf import settings
from django.core.cache import cache

from copy import deepcopy

from dlkit.abstract_osid.assessment import objects as abc_assessment_objects
from dlkit.abstract_osid.grading import objects as abc_grading_objects
from dlkit.abstract_osid.osid import objects as osid_objects
from dlkit.abstract_osid.repository import objects as abc_repository_objects

from dlkit_configs import configs  # probably a bad idea...
from dlkit.runtime.primordium import Id, Type
from dlkit.runtime.errors import PermissionDenied, InvalidArgument, NotFound,\
    NoAccess, IllegalState
from dlkit.runtime import PROXY_SESSION, RUNTIME

# http://www.django-rest-framework.org/api-guide/pagination
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from rest_framework import exceptions, status
from rest_framework.response import Response
from rest_framework.pagination import PaginationSerializer,\
    DefaultObjectSerializer

from assessments_users.models import APIUser

CONFIGURED_AUTHORITY = configs.JSON_1['parameters']['authority']['values'][0]['value']
MANAGERS = [('am', 'ASSESSMENT'),
            ('authzm', 'AUTHORIZATION'),
            ('cm', 'COMMENTING'),
            ('gm', 'GRADING'),
            ('lm', 'LEARNING'),
            ('logm', 'LOGGING'),
            ('resm', 'RESOURCE'),
            ('rm', 'REPOSITORY')]

class CreatedResponse(Response):
    def __init__(self, *args, **kwargs):
        super(CreatedResponse, self).__init__(status=status.HTTP_201_CREATED, *args, **kwargs)


class DeletedResponse(Response):
    def __init__(self, *args, **kwargs):
        super(DeletedResponse, self).__init__(status=status.HTTP_204_NO_CONTENT, *args, **kwargs)


class ErrorResponse(Response):
    def __init__(self, *args, **kwargs):
        super(ErrorResponse, self).__init__(status=status.HTTP_404_NOT_FOUND, *args, **kwargs)


class UpdatedResponse(Response):
    def __init__(self, *args, **kwargs):
        super(UpdatedResponse, self).__init__(status=status.HTTP_202_ACCEPTED, *args, **kwargs)


class DLEncoder(json.JSONEncoder):
    """
    Custom JSON encoder for DLKit objects
    """
    def default(self, obj):
        if (isinstance(obj, osid_objects.OsidObject)):
            return dl_dumps(obj.object_map)
        elif (isinstance(obj, osid_objects.OsidList)):
            result = []
            for o in obj:
                result.append(o.object_map)
            return dl_dumps(result)
        elif (isinstance(obj, osid_objects.OsidCatalog)):
            return dl_dumps(obj._catalog.object_map)
        elif (isinstance(obj, list)):
            result = []
            for o in obj:
                result.append(o.object_map)
            return dl_dumps(result)
        else:
            return dl_dumps(obj.object_map)
        return json.JSONEncoder.default(self, obj)


class DLSerializer(DefaultObjectSerializer):
    def to_native(self, obj):
        results = []
        for item in obj:
            try:
                item_map = item.object_map
                results.append(item_map)
            except AttributeError:
                try:
                    node_map = item.get_object_node_map()
                    results.append(node_map)
                except AttributeError:
                    results.append(item)
        return results


class DLPaginationSerializer(PaginationSerializer):
    """To return an object's object_map instead of the __dict__ or dir() values that
    the built-in serializer returns

    """
    class Meta:
        object_serializer_class = DLSerializer


def activate_managers(request):
    """
    Create initial managers and store them in the user session
    """
    for manager in MANAGERS:
        nickname = manager[0]
        service_name = manager[1]

        condition = PROXY_SESSION.get_proxy_condition()
        condition.set_http_request(request)
        proxy = PROXY_SESSION.get_proxy(condition)

        key = '{0}-{1}'.format(convert_to_ascii(request.user.username.replace(' ', '-')),
                               nickname)
        # if key not in settings.CACHE:
        #     with settings.LOCK:
        #         settings.CACHE[key] = RUNTIME.get_service_manager(service_name,
        #                                                  proxy=proxy)
        if cache.get(key) is None:
            cache.set(key, RUNTIME.get_service_manager(service_name,
                                                       proxy=proxy))
    return request

def add_links(request, object, links):
    object.update({
        '_links': {
            'self': build_safe_uri(request)
        }
    })
    for field, path in links.iteritems():
        object['_links'].update({
            field: build_safe_uri(request) + path
        })

    return object

def append_slash(url):
    if url[-1] != '/':
        url += '/'
    return url

def build_safe_uri(request):
    """
    because Django's request.build_absolute_uri() does not url-escape the
    IDs, it leaves in : and @. Which means that the URIs are not compatible
    with the data stored in the Mongo impl. For example, deleting
    an assessment bank should confirm that there are no assessments, first.
    But the bankId attribute of assessments is stored url-escaped.
    So none will be found, if we rely on the non-url-escaped URIs
    generated by Django.
    """
    uri = ''
    if request.is_secure():
        uri += 'https://'
    else:
        uri += 'http://'
    uri += request.get_host()
    uri += quote(request.get_full_path())

    return append_slash(uri)

def cache_bank(request, bank):
    key = '{0}-bank-{1}'.format(convert_to_ascii(request.user.username.replace(' ', '-')),
                                str(bank.ident))
    cache.set(key, bank)
    # with settings.LOCK:
    #     settings.CACHE[key] = bank

def clean_id(_id):
    """
    Django seems to un-url-safe the IDs passed in to the rest framework views,
    so we need to url-safe them, then convert them to OSID IDs
    """
    if _id.find('@') >= 0:
        return Id(quote(_id))
    else:
        return Id(_id)

def clean_up_dl_objects(data):
    """
    Because dl objects need to be parsed out of any dict into json format
    before they can be rendered in the browser, yet we cannot just do
    json dumps because then they'll be strings...but they need
    to be objects to be rendered properly.
    """
    if isinstance(data, dict):
        results = {}
        for key, value in data.iteritems():
            if (isinstance(value, abc_assessment_objects.Bank) or
                isinstance(value, abc_assessment_objects.Assessment)):
                results[key] = convert_dl_object(value)
            else:
                results[key] = value
        return results
    else:
        return data

def clean_up_post(bank=None, item=None):
    if bank is not None and item is not None:
        if isinstance(item, abc_assessment_objects.Item):
            bank.delete_item(item.ident)
        elif isinstance(item, abc_assessment_objects.Assessment):
            bank.delete_assessment(item.ident)
        elif isinstance(item, abc_assessment_objects.Answer):
            bank.delete_answer(item.ident)

def convert_dl_object(obj):
    """
    convert a DLKit object into a "real" json-able object
    """
    try:
        #return json.loads(json.loads(json.dumps(obj, cls=DLEncoder)))
        return obj.object_map 
    except:
        return obj

def convert_to_ascii(username):
    # let's also remove any funky unicode so Django and memcached don't complain
    if isinstance(username, unicode):
        username = username.encode('ascii', errors='ignore')
    return username

def convert_to_osid_id(id):
    if isinstance(id, basestring):
        return Id(id)
    else:
        return id

def create_agent_id(username, authority='MIT-ODL'):
    return Id(identifier=username,
              namespace='osid.agent.Agent',
              authority=authority)

def create_function_id(function, namespace):
    return Id(identifier=function,
              namespace=namespace,
              authority='ODL.MIT.EDU')

def create_qualifier_id(identifier, namespace, authority=CONFIGURED_AUTHORITY):
    if identifier == 'ROOT':
        authority = 'ODL.MIT.EDU'
    return Id(identifier=identifier,
              namespace=namespace,
              authority=authority)

def dl_dumps(obj):
    try:
        clean_obj = strip_object_ids(obj)
        return json.dumps(clean_obj)
    except:
        return pickle.dumps(obj)

def extract_items(request, a_list, bank=None, section=None, raw=False):
    from .assessment import get_question_status  # import here to prevent circular imports

    if raw:
        try:
            return [o.object_map for o in a_list]
        except AttributeError:
            # not an osid object
            return a_list

    results = {
        '_links': {
            'self'      : build_safe_uri(request)
        },
        'data'  : []
    }
    if (not isinstance(a_list, list) and
        not isinstance(a_list, osid_objects.OsidList)):
        a_list = [a_list]

    try:
        list_len = a_list.available()
    except AttributeError:
        list_len = len(a_list)
    if list_len > 0:
        paginated = paginate(list(a_list), request)

        #for item in a_list:
        results.update({
            'data': paginated
        })
        for index, item in enumerate(paginated['results']):
            # for questions, need to add in their status
            if (isinstance(item, abc_assessment_objects.Question) or
                (isinstance(item, dict) and
                'type' in item and
                'Question' in item['type'])):
                if isinstance(item, dict):
                    item_id = Id(item['id'])
                else:
                    item_id = item.ident
                status = get_question_status(bank, section, item_id)
                # item_json.update(status)
                results['data']['results'][index].update(status)

            # results['data'].append(item_json)

        root_url_base = append_slash(
            request.build_absolute_uri().split('?')[0].replace('/query', ''))
        root_url_offered_or_taken = append_slash(
            request.build_absolute_uri().split('?page')[0])

        #for item in serialized_data['results']:
        for index, item in enumerate(paginated['results']):
            if 'id' in item:
                item_id = item['id']
                # make assessment offerings point two levels back, to just
                # <bank_id>/offerings/<offering_id>
                if (isinstance(item, abc_assessment_objects.AssessmentOffered) or
                        item['type'] == 'AssessmentOffered'):
                    results['data']['results'][index]['_link'] = root_url_offered_or_taken + \
                                                                 '../../../assessmentsoffered/' + \
                                                                 my_unquote(item_id) + '/'
                elif (isinstance(item, abc_assessment_objects.AssessmentTaken) or
                      item['type'] == 'AssessmentTaken'):
                    results['data']['results'][index]['_link'] = root_url_offered_or_taken + \
                                                                 '../../../assessmentstaken/' + \
                                                                 my_unquote(item_id) + '/'
                elif ((isinstance(item, abc_repository_objects.Asset) or
                        item['type'] == 'Asset') and
                        '/compositions/' in root_url_base):
                    results['data']['results'][index]['_link'] = root_url_base + '../../../assets/' + \
                                                                 my_unquote(item_id) + '/'
                elif ((isinstance(item, abc_grading_objects.GradeEntry) or
                        item['type'] == 'GradeEntry') and
                        '/columns/' in root_url_base):
                    results['data']['results'][index]['_link'] = '{0}../../../entries/{1}/'.format(root_url_base,
                                                                                                   my_unquote(item_id))
                elif item['type'] == 'BankNode':
                    results['data']['results'][index]['_link'] = '{0}../../{1}/'.format(root_url_base,
                                                                                        my_unquote(item_id))
                else:
                    results['data']['results'][index]['_link'] = root_url_base + my_unquote(item_id) + '/'
    else:
        results['data'] = {'count': 0, 'next': None, 'results': [], 'previous': None}
    return results

def get_bank(request, bank_id):
    if not isinstance(bank_id, basestring):
        bank_id = str(bank_id)
    key = '{0}-bank-{1}'.format(convert_to_ascii(request.user.username.replace(' ', '-')),
                                bank_id)
    if cache.get(key) is not None:
        return cache.get(key)
    # if key in settings.CACHE:
    #     with settings.LOCK:
    #         return settings.CACHE[key]
    else:
        am = get_session_data(request, 'am')
        bank = am.get_bank(Id(bank_id))
        # bank.disable_session_management()
        return bank

def get_data_from_request(request):
    """
    Because data might be in bad JSON form, might be in a string...
    need to return an object, always
    """
    try:
        try:
            if len(request.POST) > 0:
                if '_content_type' in request.POST:
                    data = request.DATA
                else:
                    data = request.POST
            else:
                body = request.body
                try:
                    # total hack...not sure why sometimes a request with
                    # no files throws ParseError on request.FILES
                    files = request.FILES
                except:
                    files = ''
                if len(body) > 0 and len(files) == 0:
                    data = body
                    if data == '':
                        raise Exception
                    else:
                        try:
                            data = json.loads(data)
                        except:
                            data = re.sub(r"([^\\])'", r'\1"', data)  # replace all non-escaped single quotes with double quote, to make JSON compatible
                        # data = data.replace("'", '"')
                else:
                    data = request.DATA
        except:
            data = request.DATA
    except:
        raise InvalidArgument()

    if isinstance(data, basestring):
        try:
            data = json.loads(data)

            if (not isinstance(data, dict) and
                not isinstance(data, list)):
                raise InvalidArgument()
        except:
            raise InvalidArgument()

    if len(data) == 0:
        data = request.GET

    if isinstance(data, QueryDict):
        data = deepcopy(data)

    if len(request.FILES) > 0:
        data['files'] = request.FILES  # yes, overwrite whatever is there...

    # unpack any nested objects
    if (isinstance(data, dict) or
            isinstance(data, QueryDict)):
        for key, val in data.iteritems():
            if isinstance(val, basestring):
                try:
                    data[key] = json.loads(val)
                except (TypeError, ValueError):
                    pass

    return data

def get_session_data(request, item_type):
    # get a manager
    try:
        key = '{0}-{1}'.format(convert_to_ascii(request.user.username.replace(' ', '-')),
                               item_type)
        manager = cache.get(key)
        if manager is None:
        # if key not in settings.CACHE:
            service_name = [m[1] for m in MANAGERS if m[0] == item_type][0]
            condition = PROXY_SESSION.get_proxy_condition()
            condition.set_http_request(request)
            proxy = PROXY_SESSION.get_proxy(condition)
            # with settings.LOCK:
            #     settings.CACHE[key] = RUNTIME.get_service_manager(service_name,
            #                                              proxy=proxy)
            manager = RUNTIME.get_service_manager(service_name,
                                                  proxy=proxy)
            cache.set(key, manager)
        return manager
        # with settings.LOCK:
        #     return settings.CACHE[key]
    except Exception as ex:
        log_error('utilities.get_session_data()', ex)

def handle_exceptions(ex):
    log_error(traceback.format_exc(10), ex)
    if isinstance(ex, PermissionDenied):
        raise exceptions.AuthenticationFailed('Permission denied.')
    elif isinstance(ex, InvalidArgument):
        if len(ex.args) == 0:
            raise exceptions.APIException('Poorly formatted input data.')
        else:
            raise exceptions.APIException(ex.args)
    # elif isinstance(ex, KeyError):
    #     raise exceptions.APIException(ex.args)
    elif isinstance(ex, NotFound):
        raise exceptions.APIException('Object not found.')
    elif isinstance(ex, InvalidId):
        raise exceptions.APIException('Invalid ID.')
    elif isinstance(ex, NoAccess):
        raise exceptions.APIException('You cannot edit those fields.')
    elif isinstance(ex, exceptions.NotAcceptable):
        raise exceptions.NotAcceptable(ex.args)
    elif isinstance(ex, IllegalState):
        if len(ex.args) == 0:
            raise exceptions.APIException('Illegal state: you cannot do that because '
                                          'system conditions have changed. For example, '
                                          'the assessment has already been taken, or you '
                                          'have exceeded the number of allowed attempts.')
        else:
            raise exceptions.APIException(ex.args)
    else:
        raise exceptions.APIException(ex.args)

def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def clear_cache():
    # flush the cache on all PUT / POST operations to
    # assessments / offereds / items / authorization
    # ideally would do this on a pre-view basis, like
    # https://gist.github.com/dpnova/1223933
    # but it's nigh-impossible to reconstruct the URLs
    # because of catalog hierarchy
    # need to clear the Django MEMCACHED
    cache.clear()
    # settings.CACHE.clear()

def is_lti_request(request):
    if ('HTTP_LTI_USER_ID' in request.META and
        'HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID' in request.META and
        'HTTP_LTI_USER_ROLE' in request.META and
        'HTTP_LTI_BANK' in request.META):
        return True
    else:
        return False

def log_error(module, ex):
    import logging
    template = "An exception of type {0} occurred in {1}. Arguments:\n{2!r}"
    message = template.format(type(ex).__name__, module, ex.args)
    logging.info(message)
    return message

def manage_lti_headers(request):
    if is_lti_request(request):
        store_lti_user(request)

def my_unquote(str):
    if '%40' in str:
        return unquote(str)
    else:
        return str

def paginate(data, request, items_per_page=10):
    # http://www.django-rest-framework.org/api-guide/pagination
    page_num = request.QUERY_PARAMS.get('page')
    if page_num == 'all':
        items_per_page = len(data)
        page_num = 1
    paginator = Paginator(data, items_per_page)
    try:
        page = paginator.page(page_num)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)

    serializer = DLPaginationSerializer(instance=page, context={'request': request})
    # serializer = PaginationSerializer(instance=page, context={'request': request})

    return serializer.data

def set_form_basics(form, data):
    def _grab_first_match(keys):
        # filtered = {k:v for k, v in data.iteritems() if k in keys}
        # return filtered[filtered.keys()[0]]
        value = data[set(keys).intersection(data.keys()).pop()]
        if not isinstance(value, basestring):
            value = str(value)
        return value

    name_keys = ['name', 'displayName', 'displayname', 'display_name']
    description_keys = ['desc', 'description']
    genus_keys = ['genus', 'genusTypeId', 'genusType', 'genus_type_id', 'genus_type']

    if any(_name in data for _name in name_keys):
        form.display_name = _grab_first_match(name_keys)

    if any(_desc in data for _desc in description_keys):
        form.description = _grab_first_match(description_keys)

    if any(_genus in data for _genus in genus_keys):
        form.set_genus_type(Type(_grab_first_match(genus_keys)))

    return form

def set_session_data(request, item_type, data):
    key = '{0}-{1}'.format(convert_to_ascii(request.user.username.replace(' ', '-')),
                           item_type)
    cache.set(key, data)
    # with settings.LOCK:
    #     settings.CACHE[key] = data

def set_user(request):
    """
    Users can either be authenticated via session or RESTful API
    with HTTP Signature. DLKit only handles users who are
    authenticated via sessions, so we need to create a session
    for remote users who are authenticated via HTTP Signature.
    Session-authenticated users just need to pass through.
    HTTP Signature users need to have a session created.
    * NOT sure this is the best way to do it, but currently
        seems like the only way without heavy modification of DLKit

    Also check for LTI Headers. If they are present, store the
    user data / role / GUID in the LTI table.
    """
    from django.contrib.auth import authenticate, login, logout
    from dlkit.authz_adapter.osid.osid_errors import PermissionDenied
    username = convert_to_ascii(request.META.get('HTTP_X_API_PROXY', request.user.username))

    if username == '':
        username = 'Anonymous'
    # if username == request.user.username:
    if is_lti_request(request) and username == request.user.username:
        # This may break if LTI users have whitespace or unicode?
        # User is authenticated via Django session
        # pass them into the proxy
        # still need to check for LTI headers
        manage_lti_headers(request)
    else:
        # some app is making a request via the RESTful API
        # log in this user and clear out all other users
        logout(request)
        # create the proxied user as a student, if they do not exist
        try:
            APIUser.objects.get(username=username)
        except APIUser.DoesNotExist:
            APIUser.objects.create_user(username)

        remote_user = authenticate(remote_user=username)
        if remote_user is not None:
            # if remote_user.is_active and remote_user.is_staff:
            if remote_user.is_active:  # kind of weak...ideally would check this against each bank authz...for Touchstone, will always be True
                manage_lti_headers(request)
                login(request, remote_user)
            else:
                raise PermissionDenied()
        else:
            raise PermissionDenied()

def store_lti_user(request):
    """
    The RESTful app needs to keep a record of LTI users and roles,
    so that DLKit can check for membership when doing authorization
    checks. Keep this anonymous, no personal information!
    """
    from assessments_users.models import LTIUser
    lti_user_id = request.META['HTTP_LTI_USER_ID']
    lti_tool_guid = request.META['HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID']
    lti_user_role = request.META['HTTP_LTI_USER_ROLE']
    lti_bank = request.META['HTTP_LTI_BANK']

    # Nov 17, 2014
    # cjshaw@mit.edu
    # set a default lti_tool_guid because it is not required
    # so edX doesn't set it. Create default with request IP address
    # https://stackoverflow.com/questions/4581789/how-do-i-get-user-ip-address-in-django
    if not lti_tool_guid or lti_tool_guid == '':
        forwarded = request.META.get('HTTP_X_FORWARDED_FOR')
        if forwarded:
            lti_tool_guid = forwarded.split(',')[-1].strip()
        else:
            lti_tool_guid = request.META.get('REMOTE_ADDR')

    new_user, created = LTIUser.objects.get_or_create(
        user_id=lti_user_id,
        consumer_guid=lti_tool_guid,
        role=lti_user_role,
        bank=lti_bank)


def strip_object_ids(obj):
    """
    Recursively strip out the _id attribute from Mongo...replace it with 'id' = str(_id)
    Otherwise it breaks json.dumps()
    """
    results = {}
    for key, value in obj.iteritems():
        if isinstance(value, list):
            results[key] = []
            for ele in value:
                if isinstance(ele, dict):
                    results[key].append(strip_object_ids(ele))
                else:
                    results[key].append(ele)
        elif isinstance(value, dict):
            results[key] = strip_object_ids(value)
        else:
            if key == '_id':
                results['id'] = str(value)
            else:
                results[key] = value
    return results

def update_links(request, obj):
    """add links for browsable API"""
    obj.update({
        '_links': {
            'self': build_safe_uri(request)
        }
    })

    root_url_base = request.build_absolute_uri().split('?')[0]
    if 'type' in obj:
        obj_type = obj['type']
    else:  # main service page
        obj['_links'].update({
            'banks'         : build_safe_uri(request) + 'banks/',
            'documentation' : build_safe_uri(request) + 'docs/',
            'hierarchies'   : build_safe_uri(request) + 'hierarchies/',
            'itemTypes'     : build_safe_uri(request) + 'types/items/'
        })
        obj_type = ''

    if obj_type == 'Bank':  # assessmentBank
        if 'hierarchies' in root_url_base:
            node_url = build_safe_uri(request).replace('/roots/', '/nodes/')
            obj['_links'].update({
                'children': node_url + 'children/',
                'self': node_url
            })
        else:
            obj['_links'].update({
                'assessments': build_safe_uri(request) + 'assessments/',
                'items': build_safe_uri(request) + 'items/',
            })
    elif obj_type == 'BankNode':
        obj['_links'].update({
            'children': build_safe_uri(request) + 'children/'
        })
    elif obj_type == 'Assessment':  # assessment.Assessment
        obj['_links'].update({
            'items': build_safe_uri(request) + 'items/',
            'offerings': build_safe_uri(request) + 'assessmentsoffered/',
            'takens': build_safe_uri(request) + 'assessmentstaken/'
        })
    elif obj_type == 'Item':  # assessment.Item
        if not 'assessment.Assessment' in root_url_base:
            # because for assessmentItemDetails, it points here...but
            # we don't want to link to the same place.
            obj['_links'].update({
                'answers': build_safe_uri(request) + 'answers/',
                'edxml': build_safe_uri(request) + 'edxml/',
                'files': build_safe_uri(request) + 'files/',
                'question': build_safe_uri(request) + 'question/'
            })
    elif obj_type == 'AssessmentOffered':
        obj['_links'].update({
            'items': build_safe_uri(request) + '../../items/',
            'takens': build_safe_uri(request) + 'assessmentstaken/'
        })
    elif obj_type == 'AssessmentTaken':
        obj['_links'].update({
            'questions': build_safe_uri(request) + 'questions/',
            'finish': build_safe_uri(request) + 'finish/'
        })
    elif obj_type == 'Question':
        obj['_links'].update({
            'edxml': build_safe_uri(request) + 'edxml/',
            'files': build_safe_uri(request) + 'files/',
            'status': build_safe_uri(request) + 'status/',
            'submit': build_safe_uri(request) + 'submit/'
        })
    elif obj_type == 'ObjectiveBank':
        obj['_links'].update({
            'proficiencies': build_safe_uri(request) + 'proficiencies/'
        })


def verify_at_least_one_key_present(_data, _keys_list):
    """
    at least one of the keys is present
    """
    present = False

    for key in _keys_list:
        if key in _data:
            present = True

    if not present:
        raise KeyError('At least one of the following must be passed in: ' + json.dumps(_keys_list))


def verify_keys_present(my_dict, list_of_keys):
    if not isinstance(list_of_keys, list):
        list_of_keys = [list_of_keys]
    for key in list_of_keys:
        if key not in my_dict:
            raise KeyError('"' + key + '" required in input parameters but not provided.')


def verify_min_length(my_dict, list_of_keys, expected_len):
    for key in list_of_keys:
        if not isinstance(my_dict[key], list):
            raise TypeError('"' + key + '" is not a list.')
        else:
            if len(my_dict[key]) < int(expected_len):
                raise IntegrityError('"' + key + '" is shorter than ' + str(expected_len) + '.')
