import inflection

from django.core.urlresolvers import resolve

from dlkit.runtime import PROXY_SESSION, RUNTIME
from dlkit.runtime.errors import PermissionDenied

from .assessment import get_object_bank_from_request
from .general import get_session_data, clean_id, create_function_id,\
    create_agent_id, create_qualifier_id


def allow_user_to_add_authorizations(vault, username):
    from utilities.testing import create_authz

    qualifiers = ('ROOT', 24 * '0')

    for root_qualifier in qualifiers:
        qualifier_id = create_qualifier_id(root_qualifier,
                                           'authorization.Vault',
                                           authority='ODL.MIT.EDU')
        function_id = create_function_id('lookup', 'authorization.Vault')
        create_authz(vault, create_agent_id(username), function_id, qualifier_id, is_super=True)

    functions = ['create', 'lookup', 'delete', 'update', 'search']
    for function in functions:
        function_id = create_function_id(function, 'authorization.Authorization')

        create_authz(vault, create_agent_id(username), function_id, vault.ident)

def check_authz_on_authz(vault, authorization_id):
    # make sure that someone is not trying to delete
    # a hidden, "super" authorization
    from .testing import SUPER_USER_AUTHZ_GENUS
    querier = vault.get_authorization_query()
    querier.match_id(clean_id(authorization_id), True)
    querier.match_genus_type(SUPER_USER_AUTHZ_GENUS, False)

    if vault.get_authorizations_by_query(querier).available() == 0:
        raise PermissionDenied

def create_vault(request):
    from .testing import BOOTSTRAP_VAULT_GENUS
    authzm = get_session_data(request, 'authzm')
    form = authzm.get_vault_form_for_create([])
    form.display_name = "System Vault"
    form.description = "Created during bootstrapping"
    form.set_genus_type(BOOTSTRAP_VAULT_GENUS)
    return authzm.create_vault(form)

def get_authz_query_session():
    from .testing import get_super_authz_user_request
    req = get_super_authz_user_request()
    return get_vault(req)

def get_non_super_authz(vault):
    from .testing import SUPER_USER_AUTHZ_GENUS
    querier = vault.get_authorization_query()
    querier.match_genus_type(SUPER_USER_AUTHZ_GENUS, False)
    return vault.get_authorizations_by_query(querier)

def get_vault(request):
    from .testing import BOOTSTRAP_VAULT_GENUS
    authzm = get_session_data(request, 'authzm')
    if authzm is None:
        service_name = 'AUTHORIZATION'
        condition = PROXY_SESSION.get_proxy_condition()
        condition.set_http_request(request)
        proxy = PROXY_SESSION.get_proxy(condition)
        authzm = RUNTIME.get_service_manager(service_name,
                                             proxy=proxy)
    return authzm.get_vaults_by_genus_type(BOOTSTRAP_VAULT_GENUS).next()

def user_can_proxy(user, proxy_username, request):
    path = request.path
    if proxy_username is None:
        return True

    if proxy_username == user.username:
        return True

    # check against the catalog
    aqs = get_authz_query_session()
    function_id = create_function_id('proxy', 'users.Proxy')

    potential_bank = get_object_bank_from_request(request)
    if potential_bank is not None:
        qualifier_id = potential_bank.ident
    else:
        request_kwargs = resolve(path)[2]
        catalog_types = ['bank', 'repository', 'bin', 'gradebook', 'log']
        if any(['{0}_id'.format(c) in request_kwargs
                for c in catalog_types]):
            catalog_name = [c for c in catalog_types if '{0}_id'.format(c) in request_kwargs][0]
            # assume first match
            qualifier_id = clean_id(request_kwargs['{0}_id'.format(catalog_name)])
        else:
            qualifier_id = create_qualifier_id('ROOT',
                                               'users.Proxy',
                                               authority='ODL.MIT.EDU')

    return aqs.is_authorized(
        agent_id=create_agent_id(user.username),
        function_id=function_id,
        qualifier_id=qualifier_id)

def user_has_authz(user):
    aqs = get_authz_query_session()

    querier = aqs.get_authorization_query()
    querier.match_agent_id(create_agent_id(user.username), True)

    return aqs.get_authorizations_by_query(querier).available() > 0
