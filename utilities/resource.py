from dlkit.runtime import RUNTIME, PROXY_SESSION
from dlkit.runtime.errors import IllegalState

from dlkit.runtime.primordium import Id
#####
# for creating a proxy agent
#####
from dlkit.runtime.proxy_example import SimpleRequest


def get_agent_id(agent_id):
    """Not a great hack...depends too much on internal DLKit knowledge"""
    # TODO: change this for FBW
    # if '@mit.edu' not in agent_id:
    #     agent_id += '@mit.edu'
    test_request = SimpleRequest(agent_id)
    condition = PROXY_SESSION.get_proxy_condition()
    condition.set_http_request(test_request)
    proxy = PROXY_SESSION.get_proxy(condition)
    resm = RUNTIME.get_service_manager('RESOURCE', proxy=proxy)
    return resm.effective_agent_id

def update_resource_avatar_urls(bin_, resource):
    """update the resource avatar URLs with CloudFront URLs
    """
    if isinstance(resource, dict):
        resource_object = bin_.get_resource(Id(resource['id']))
        resource_map = resource
    else:
        resource_object = resource
        resource_map = resource.object_map

    try:
        avatar_asset = resource_object.get_avatar()

        for asset_content in avatar_asset.get_asset_contents():
            # should only be one
            resource_map['avatarURL'] = asset_content.get_url()
    except IllegalState:
        pass  # no avatar for that user

    return resource_map
