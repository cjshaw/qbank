import logging
import traceback

from dlkit.runtime.errors import InvalidArgument
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.exceptions import APIException

from utilities import general as gutils


class DLKitSessionsManager(APIView):
    """ base class to handle all the dlkit session management
    """
    def initial(self, request, *args, **kwargs):
        """set up the resource manager"""
        super(DLKitSessionsManager, self).initial(request, *args, **kwargs)
        gutils.set_user(request)
        gutils.activate_managers(request)
        self._managers = ['am', 'authzm', 'cm', 'gm', 'lm', 'logm', 'resm', 'rm']
        for manager in self._managers:
            setattr(self, manager, gutils.get_session_data(request, manager))
        try:
            self.data = gutils.get_data_from_request(request)
        except InvalidArgument as ex:
            gutils.handle_exceptions(ex)
        if 'bank_id' in kwargs:
            self.bank = gutils.get_bank(request, gutils.clean_id(kwargs['bank_id']))
        else:
            self.bank = None

    def finalize_response(self, request, response, *args, **kwargs):
        """save the updated repository manager"""
        try:
            for manager in self._managers:
                gutils.set_session_data(request, manager, getattr(self, manager))
            if self.bank is not None:
                gutils.cache_bank(request, self.bank)
        except AttributeError:
            pass  # with an exception, the RM may not be set
        return super(DLKitSessionsManager, self).finalize_response(request,
                                                                   response,
                                                                   *args,
                                                                   **kwargs)

    def handle_exception(self, exc):
        """if in debug mode, pass back a traceback"""
        if settings.REMOTE_DEBUG:
            logging.info(traceback.format_exc())
            return APIException(traceback.format_exc())
        else:
            return super(DLKitSessionsManager, self).handle_exception(exc)

