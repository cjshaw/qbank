"""
osid.managers.py
"""
from dlkit.abstract_osid.osid import managers as abc_osid_managers
from dlkit.abstract_osid.osid import markers as abc_osid_markers


class Sourceable(abc_osid_markers.Sourceable):
    """sourceable objects"""

    def __init__(self):
        super(Sourceable, self).__init__()

    def get_provider_id(self):
        pass

    provider_id = property(fget=get_provider_id)

    def get_provider(self):
        pass

    provider = property(fget=get_provider)

    def get_branding_ids(self):
        pass

    branding_ids = property(fget=get_branding_ids)

    def get_branding(self):
        pass

    branding = property(fget=get_branding)

    def get_license(self):
        pass

    license = property(fget=get_license)


class OsidProfile(abc_osid_managers.OsidProfile, Sourceable):
    """osid configuration profile"""

    def get_id(self):
        from .. import profile
        from dlkit.membership.primitives import Id
        return Id(**profile.ID)

    id_ = property(fget=get_id)
    ident = property(fget=get_id)

    def get_display_name(self):
        from .. import profile
        from dlkit.membership.primitives import Type, DisplayText
        return DisplayText(text=profile.DISPLAYNAME,
                           language_type=Type(**profile.LANGUAGETYPE),
                           script_type=Type(**profile.SCRIPTTYPE),
                           format_type=Type(**profile.FORMATTYPE))

    display_name = property(fget=get_display_name)

    def get_description(self):
        from .. import profile
        from dlkit.membership.primitives import Type, DisplayText
        return DisplayText(text=profile.DESCRIPTION,
                           language_type=Type(**profile.LANGUAGETYPE),
                           script_type=Type(**profile.SCRIPTTYPE),
                           format_type=Type(**profile.FORMATTYPE))

    description = property(fget=get_description)

    def get_version(self):
        from .. import profile
        from dlkit_runtime.primordium import Version
        from dlkit_runtime.primordium import Type
        return Version(components=profile.VERSIONCOMPONENTS,
                       scheme=Type(**profile.VERSIONSCHEME))

    version = property(fget=get_version)

    def get_release_date(self):
        pass

    release_date = property(fget=get_release_date)

    def supports_osid_version(self, version=None):
        from .. import profile
        from dlkit_runtime.primordium import Version
        from dlkit_runtime.primordium import Type
        return Version(components=profile.OSIDVERSION,
                       scheme=Type(**profile.VERSIONSCHEME))

    def get_locales(self):
        # NEED TO IMPLEMENT
        pass

    locales = property(fget=get_locales)

    def supports_journal_rollback(self):
        # Perhaps someday I will support journaling
        return False

    def supports_journal_branching(self):
        # Perhaps someday I will support journaling
        return False

    def get_branch_id(self):
        from dlkit.membership.osid.osid_errors import Unimplemented
        raise Unimplemented()

    branch_id = property(fget=get_branch_id)

    def get_branch(self):
        from dlkit.membership.osid.osid_errors import Unimplemented
        raise Unimplemented()

    branch = property(fget=get_branch)

    def get_proxy_record_types(self):
        pass

    proxy_record_types = property(fget=get_proxy_record_types)

    def supports_proxy_record_type(self, proxy_record_type=None):
        pass


class OsidManager(abc_osid_managers.OsidManager, OsidProfile):
    """no proxies here"""

    def initialize(self, runtime=None):
        pass

    def rollback_service(self, rollback_time=None):
        pass

    def change_branch(self, branch_id=None):
        pass


class OsidProxyManager(abc_osid_managers.OsidProxyManager, OsidProfile):
    """when a proxy is being used"""

    def initialize(self, runtime=None):
        pass

    def rollback_service(self, rollback_time=None, proxy=None):
        pass

    def change_branch(self, branch_id=None, proxy=None):
        pass

