"""
# -*- coding: utf-8 -*-
"""
from dlkit_runtime.errors import NotFound


class Genus(object):
    """Genus type object"""

    generic_types = {
        'DEFAULT': 'Default',
        'UNKNOWN': 'Unkown'
    }

    def __init__(self):
        self.type_set = {
            'Gen': self.generic_types
        }

    def get_type_data(self, name):
        """return a type object"""
        try:
            return {
                'authority': 'dlkit.mit.edu',
                'namespace': 'GenusType',
                'identifier': name,
                'domain': 'Generic Types',
                'display_name': self.generic_types[name] + ' Generic Type',
                'display_label': self.generic_types[name],
                'description': ('The ' + self.generic_types[name] +
                                ' Type. This type has no symantic meaning.')
            }
        except IndexError:
            raise NotFound('GenusType: ' + name)


class Language(object):
    """Language type object"""

    def __init__(self):
        pass

    def get_type_data(self, name):
        """return a type object"""
        if name == 'DEFAULT':
            from .profile import LANGUAGETYPE
            return LANGUAGETYPE
        else:
            raise NotFound('DEFAULT Language Type')


class Script(object):
    """Script type object"""

    def __init__(self):
        pass

    def get_type_data(self, name):
        """return a type object"""
        if name == 'DEFAULT':
            from .profile import SCRIPTTYPE
            return SCRIPTTYPE
        else:
            raise NotFound('DEFAULT Script Type')


class Format(object):
    """Format type object"""

    def __init__(self):
        pass

    def get_type_data(self, name):
        """return a type object"""
        if name == 'DEFAULT':
            from .profile import FORMATTYPE
            return FORMATTYPE
        else:
            raise NotFound('DEFAULT Format Type')

