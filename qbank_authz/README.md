This is an extension for the Ortho-3D project, to manage groups.
Assumes all users are students, and authorized to view only (take assessments)

Copyright (c) 2014 Massachusetts Institute of Technology
