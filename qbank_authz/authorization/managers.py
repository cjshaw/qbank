"""
Qbank authorization.managers.py
"""

from .sessions import AuthorizationSession as membership_auth
from ..osid.osid_errors import Unimplemented, NullArgument, OperationFailed, IllegalState
from dlkit.manager_impls.authorization import managers as manager_utils
from dlkit_runtime.primordium import Id


class AuthorizationProfile(manager_utils.AuthorizationProfile):
    """osid profile for authorization"""

    def supports_authorization(self):
        return True

    def supports_authorization_lookup(self):
        return True


class AuthorizationManager(AuthorizationProfile, manager_utils.AuthorizationManager):
    """when not using a proxy"""
    def __init__(self):
        pass

    def initialize(self, runtime):
        pass

    def get_authorization_session(self):
        """Gets an ``AuthorizationSession`` which is responsible for performing
            authorization checks.

        return: (osid.authorization.AuthorizationSession) - an
                authorization session for this service
        raise:  OperationFailed - unable to complete request
        raise:  Unimplemented - ``supports_authorization()`` is
                ``false``
        *compliance: optional -- This method must be implemented if
            ``supports_authorization()`` is ``true``.*

        """
        if not self.supports_authorization():
            raise Unimplemented()
        try:
            return membership_auth()
        except AttributeError:
            raise OperationFailed()

    authorization_session = property(fget=get_authorization_session)


class AuthorizationProxyManager(AuthorizationProfile, manager_utils.AuthorizationProxyManager):
    """The authorization manager provides access to authorization sessions and
        provides interoperability tests for various aspects of this service.

    Methods in this manager support the passing of a ``Proxy`` object.
    The sessions included in this manager are:

      * ``AuthorizationSession:`` a session to performs authorization
        checks
      * ``AuthorizationLookupSession:`` a session to look up
        ``Authorizations``
      * ``AuthorizationSearchSession:`` a session to search
        ``Authorizations``
      * ``AuthorizationAdminSession:`` a session to create, modify and
        delete ``Authorizations``
      * ``AuthorizationNotificationSession: a`` session to receive
        messages pertaining to ``Authorization`` changes
      * ``AuthorizationVaultSession:`` a session to look up
        authorization to vault mappings
      * ``AuthorizationVaultAssignmentSession:`` a session to manage
        authorization to vault mappings
      * ``AuthorizationSmartVaultSession:`` a session to manage smart
        authorization vault

      * ``FunctionLookupSession:`` a session to look up ``Functions``
      * ``FunctionQuerySession:`` a session to query ``Functions``
      * ``FunctionSearchSession:`` a session to search ``Functions``
      * ``FunctionAdminSession:`` a session to create, modify and delete
        ``Functions``
      * ``FunctionNotificationSession: a`` session to receive messages
        pertaining to ``Function`` changes
      * ``FunctionVaultSession:`` a session for looking up function and
        vault mappings
      * ``FunctionVaultAssignmentSession:`` a session for managing
        function and vault mappings
      * ``FunctionSmartVaultSession:`` a session to manage dynamic
        function vaults

      * ``QualifierLookupSession:`` a session to look up ``Qualifiers``
      * ``QualifierQuerySession:`` a session to query ``Qualifiers``
      * ``QualifierSearchSession:`` a session to search ``Qualifiers``
      * ``QualifierAdminSession:`` a session to create, modify and
        delete ``Qualifiers``
      * ``QualifierNotificationSession: a`` session to receive messages
        pertaining to ``Qualifier`` changes
      * ``QualifierHierarchySession:`` a session for traversing
        qualifier hierarchies
      * ``QualifierHierarchyDesignSession:`` a session for managing
        qualifier hierarchies
      * ``QualifierVaultSession:`` a session for looking up qualifier
        and vault mappings
      * ``QualifierVaultAssignmentSession:`` a session for managing
        qualifier and vault mappings
      * ``QualifierSmartVaultSession:`` a session to manage dynamic
        qualifier vaults

      * ``VaultLookupSession:`` a session to lookup vaults
      * ``VaultQuerySession:`` a session to query Vaults
      * ``VaultSearchSession`` : a session to search vaults
      * ``VaultAdminSession`` : a session to create, modify and delete
        vaults
      * ``VaultNotificationSession`` : a session to receive messages
        pertaining to ``Vault`` changes
      * ``VaultHierarchySession`` : a session to traverse the ``Vault``
        hierarchy
      * ``VaultHierarchyDesignSession`` : a session to manage the
        ``Vault`` hierarchy


    """

    def __init__(self):
        super(AuthorizationProxyManager, self).__init__()

    def get_authorization_session(self, proxy=None):
        """Gets an ``AuthorizationSession`` which is responsible for
            performing authorization checks.

        arg:    proxy (osid.proxy.Proxy): a proxy
        return: (osid.authorization.AuthorizationSession) - an
                authorization session for this service
        raise:  NullArgument - ``proxy`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  Unimplemented - ``supports_authorization()`` is
                ``false``
        *compliance: optional -- This method must be implemented if
            ``supports_authorization()`` is ``true``.*

        """
        return AuthorizationManager().get_authorization_session()

    def get_authorization_lookup_session(self, proxy=None):
        """Gets the ``OsidSession`` associated with the authorization lookup service.

        arg:    proxy (osid.proxy.Proxy): a proxy
        return: (osid.authorization.AuthorizationLookupSession) - an
                ``AuthorizationLookupSession``
        raise:  NullArgument - ``proxy`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  Unimplemented - ``supports_authorization_lookup()`` is
                ``false``
        *compliance: optional -- This method must be implemented if
            ``supports_authorization_lookup()`` is ``true``.*

        """
        return AuthorizationManager().get_authorization_lookup_session()
