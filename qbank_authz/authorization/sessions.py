"""
authorization.sessions.py
"""
from dlkit.abstract_osid.authorization import sessions as abc_authorization_sessions
from ..osid.osid_errors import Unimplemented, NullArgument, InvalidArgument
from ..osid import sessions
from django.conf import settings


class AuthorizationSession(abc_authorization_sessions.AuthorizationSession, sessions.OsidSession):
    """where all the heavy lifting is done"""

    _session_name = 'AuthorizationSession'

    def __init__(self):
        super(AuthorizationSession, self).__init__()

    def get_vault_id(self):
        raise Unimplemented()

    vault_id = property(fget=get_vault_id)

    def get_vault(self):
        raise Unimplemented()

    vault = property(fget=get_vault)

    def can_access_authorizations(self):
        return True

    def is_authorized(self, agent_id=None, function_id=None, qualifier_id=None):
        """Determines if the given agent is authorized.

        An agent is authorized if an active authorization exists whose
        ``Agent,`` ``Function`` and ``Qualifier`` matches the supplied
        parameters. Authorizations may be defined using groupings or
        hieratchical structures for both the ``Agent`` and the
        ``Qualifier`` but are queried in the de-nornmalized form.

        The ``Agent`` is generally determined through the use of an
        Authentication OSID. The ``Function`` and ``Qualifier`` are
        already known as they map to the desired authorization to
        validate.

        arg:    agent_id (osid.id.Id): the ``Id`` of an ``Agent``
        arg:    function_id (osid.id.Id): the ``Id`` of a ``Function``
        arg:    qualifier_id (osid.id.Id): the ``Id`` of a ``Qualifier``
        return: (boolean) - ``true`` if the user is authorized,
                ``false`` othersise
        raise:  NotFound - ``function_id`` is not found
        raise:  NullArgument - ``agent_id`` , ``function_id`` or
                ``qualifier_id`` is ``null``
        raise:  OperationFailed - unable to complete request
        raise:  PermissionDenied - authorization failure making request
        *compliance: mandatory -- This method must be implemented.*
        *implementation notes*: Authorizations may be stored in a
        normalized form with respect to various Resources and created
        using specific nodes in a ``Function`` or ``Qualifer``
        hierarchy. The provider needs to maintain a de-normalized
        implicit authorization store or expand the applicable
        hierarchies on the fly to honor this query.  Querying the
        authorization service may in itself require a separate
        authorization. A ``PermissionDenied`` is a result of this
        authorization failure. If no explicit or implicit authorization
        exists for the queried tuple, this method should return
        ``false``.

        ----------------
        Modified for the IS&T Membership Service
        Let agentId.identifier map to Kerberos usernames
        Let functionId.authority equal MIT-ODL
        Let functionId.namespace equal "assessment.Item"
        Let functionId.identifier equal verb-of-action (i.e. lookup)
        Let qualifierId.identifier map to the Membership groupId (need a dict somewhere?)
        [5/23/14 4:27:57 PM] Jeff Merriman: assessment.Item:lookup@MIT-ODL
        [5/23/14 4:28:12 PM] Jeff Merriman: repository.Asset:create@MIT-ODL

        Could possibly stick the full qualifierId (== MC3 ID) as the longName of the
        section...it's too long for the name. I'll need to keep a mapping
        somewhere, or query by longName from a list of sections.

        maybe  agent:@vkumar%40mit.edu@<hostname> which captures the hostname
        of the application asserting that this is vkumar
        """
        from .settings import FUNC_MAP
        from dlkit.abstract_osid.id.primitives import Id

        if not agent_id:
            raise NullArgument('You must pass in an Agent ID, to check authorizations.')
        if not function_id:
            raise NullArgument('You must pass in a Function ID, to check authorizations.')
        if not qualifier_id:
            raise NullArgument('You must pass in a Qualifier ID, to check authorizations.')

        if not isinstance(agent_id, Id):
            raise InvalidArgument('Agent ID is not an Id object.')
        if not isinstance(function_id, Id):
            raise InvalidArgument('Function ID is not an Id object.')
        if not isinstance(qualifier_id, Id):
            raise InvalidArgument('Qualifier ID is not an Id object.')

        username = agent_id.get_identifier()
        function = str(function_id)
        group_name = str(qualifier_id)

        if username in settings.ADMINS:
            role = 'Instructor'
        elif username in settings.AUTHORIZATIONS.keys():
            approved_catalogs = settings.AUTHORIZATIONS[username]
            if qualifier_id.get_identifier() in approved_catalogs:
                role = 'Instructor'
            elif qualifier_id.get_identifier() in settings.PRIVATE_CATALOGS:
                role = 'ViewCatalog'
            else:
                role = 'Learner'
        elif qualifier_id.get_identifier() in settings.PRIVATE_CATALOGS:
            role = 'ViewCatalog'
        else:
            role = 'Learner'

        if function in FUNC_MAP[role]:
            return True

        return False

    def get_authorization_condition(self, function_id=None):
        raise Unimplemented()

    def is_authorized_on_condition(self,
                                   agent_id=None,
                                   function_id=None,
                                   qualifier_id=None,
                                   condition=None):
        raise Unimplemented()

