# qbank
## Overview
This is a RESTful API on top of DLKit, to provide access to an assessment repository.

Uses a MongoDB back end.

RESTful API requires use of signature verfication via public / private key, like Amazon AWS.
Currently, we use a library that follows the draft IETF standards for signing HTTP requests:

    https://datatracker.ietf.org/doc/draft-cavage-http-signatures/

Apps that will use a signed HTTP request will need to pass in the username of the
user they with to authenticate. This should be in an 'X-Api-Proxy' header. The app's
public key should go in the 'X-Api-Key' header.

When signing an API request, the following headers MUST be included in the signature calculation:

    ['request-line','accept','date','host','x-api-proxy']

Where request-line include both method and path of the request, and x-api-proxy is the
 username of the student / user. They MUST also appear in that
order. The HTTP_AUTHORIZATION header should look something like:

    'Signature headers="request-line accept date host x-api-proxy",keyId="some-key",algorithm="hmac-sha256",signature="5CUB27wqW+oPx0ZI/xMhCaU3v/3My4JMeeP5PbWO9Xg="'

You can compare your hashing algorithm with the following input / output.

Example one:

    Headers     : {
      'Date': 'Tue, 17 Jun 2014 19:26:25 GMT',
      'Host': 'testserver:80',
      'X-Api-Proxy': 'cjshaw@mit.edu',
      'X-Api-Key': 'afakepublicKey!23',
      'Accept': 'application/json'
    }
    Secret key  : mdnAf0vTHSU/Ap1qbzZDFblHdiepcPZZ7C7B6p98
    Method      : 'GET'
    Path        : '/api/v2/assessment/banks/'

    Output      : 'Signature headers="request-line accept date host x-api-proxy",keyId="afakepublicKey!23",algorithm="hmac-sha256",signature="dc6evsmhillWBQCePbXLQQuTyu6xvfHT5v2bHznCcOQ="'

Example two:

    input doc to be signed:
        POST /api/v2/assessment/banks/ HTTP/1.1
        accept: application/json
        date: Mon, 14 Mar 2016 14:41:27 GMT
        host: testserver:80
        x-api-proxy: taaccct_instructor
    hashed but not encoded signature:
       '\xf7y\x8e\xb5\xca\xf0>H \xf2\xf7\xd2\x10\\s\r\xeen\xcb\x9a\xcd\x94&\xef\x80\xc8\xd0\xa2\xfa5\nZ'
    expected signature:
      '93mOtcrwPkgg8vfSEFxzDe5uy5rNlCbvgMjQovo1Clo='
    with public key: sIcaXKd67Y80MufpCB73
    and private key: LKswkklexT14vbudS4jOGzHvcEG48O1dAvhcVSJQ

    Headers     : {
      'Date': 'Mon, 14 Mar 2016 14:41:27 GMT',
      'Host': 'testserver:80',
      'X-Api-Proxy': 'taaccct_instructor',
      'X-Api-Key': 'sIcaXKd67Y80MufpCB73',
      'Accept': 'application/json'
    }
    Secret key  : LKswkklexT14vbudS4jOGzHvcEG48O1dAvhcVSJQ
    Method      : 'POST'
    Path        : '/api/v2/assessment/banks/'

    Output      : 'Signature headers="request-line accept date host x-api-proxy",keyId="sIcaXKd67Y80MufpCB73",algorithm="hmac-sha256",signature="93mOtcrwPkgg8vfSEFxzDe5uy5rNlCbvgMjQovo1Clo="'
    
Note that the path requested must be un-encoded. Example:

    '/api/v1/assessment/banks/assessment.Bank:53a071c7ea061a0abff13681@birdland.mit.edu/'

Instead of:

    '/api/v1/assessment/banks/assessment.Bank%3A53a071c7ea061a0abff13681%40birdland.mit.edu/'

If passing in LTI user data, a second set of parameters can be used in the
signature calculation (in this order):

    ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']

Files are passed back as base64 encoded versions of the files. RESTful consumers will need
to base64 decode the data to unpack the files.

Note that running the unit tests for the assessments app will also do remote
testing on assessments-dev.mit.edu and assessments.mit.edu. Comment those files
out if you do not want to run the remote tests.

## Updating Python dependencies
This project uses `pip` to install Python dependencies.  To add a new package:

```
$ pip install new-package-name
$ pip freeze > requirements.txt
```

The freeze command traverses the dependency tree recursively and flattens it.  After doing this, check in the updated `requirements.txt`.

## Local development on OSX with Docker

You can run the project locally in Docker containers.  This [blog post](http://www.ybrikman.com/writing/2015/05/19/docker-osx-dev/) has great background motivation on why it's useful to use Docker for local development.

First, install VirtualBox and Docker Toolbox.

  - Install VirtualBox 5.0.8: https://www.virtualbox.org/wiki/Downloads
  - Install Docker Toolbox: http://docs.docker.com/mac/started/
  - Use docker-machine to create a new Docker host: `https://docs.docker.com/machine/get-started/`
  - For convenience, add the IP from `docker-machine ip default` as a line in `/etc/hosts` so you can work with `http://docker:3000` in your browser.
  - Install https://github.com/adlogix/docker-machine-nfs to use NFS for sharing files between the host machine and the VirtualBoxVM.  This is much faster than VirtualBox shared folders.

Work with project using Docker:
  1. Use the Dockerfile to build a container image called `qbank`.  This is fairly slow, but you only need to rebuild this when the `requirements.txt` changes:
  ```
  $ docker build -t qbank .
  ```
  2. Spin up a new container from that image, with the local filesystem mounted into the container, and run a bash shell:
  ```
  $ docker run -v $(pwd):/mnt/assessments -it qbank bash
  ```
  3. Work within the container!  You can run tests, etc. like you would normally.

Copyright (c) 2014 Massachusetts Institute of Technology
