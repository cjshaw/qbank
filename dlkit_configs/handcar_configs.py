try:
    from django.conf import settings
except ImportError:
    from ..dlkit_runtime_project import settings


HANDCAR_MC3 = {
    'id': 'handcar_mc3',
    'displayName': 'Handcar MC3 Configuration',
    'description': 'Configuration for Handcar MC3 Production Service',
    'parameters':
        {
        'implKey':
            {
            'syntax': 'STRING',
            'displayName': 'Implementation Key',
            'description': 'Implementation key used by Runtime for class loading',
            'values':
                [
                    {'value': 'handcar', 'priority': 1}
                ]
           },
        'hostName':
            {
            'syntax': 'STRING',
            'displayName': 'Host Name',
            'description': 'Host Name for Handcar RESTFul Service Provider',
            'values':
                [
                    {'value': 'mc3.mit.edu', 'priority': 1}
                ]
           },
        'appKey':
            {
            'syntax': 'STRING',
            'displayName': 'App Key',
            'description': 'Agent Key for Handcar service provider',
            'values':
                [
                    {'value': settings.MC3_HANDCAR_APP_KEY, 'priority': 1}
                ]
            }
        }
    }


HANDCAR_MC3_DEMO = {
    'id': 'handcar_mc3_demo',
    'displayName': 'Handcar MC3 Demo Configuration',
    'description': 'Configuration for Handcar MC3 Demo Service',
    'parameters':
        {
        'implKey':
            {
            'syntax': 'STRING',
            'displayName': 'Implementation Key',
            'description': 'Implementation key used by Runtime for class loading',
            'values':
                [
                    {'value': 'handcar', 'priority': 1}
                ]
           },
        'hostName':
            {
            'syntax': 'STRING',
            'displayName': 'Host Name',
            'description': 'Host Name for Handcar RESTFul Service Provider',
            'values':
                [
                    {'value': 'mc3-demo.mit.edu', 'priority': 1}
                ]
           },
        'appKey':
            {
            'syntax': 'STRING',
            'displayName': 'App Key',
            'description': 'Agent Key for Handcar service provider',
            'values':
                [
                    {'value': settings.MC3_DEMO_HANDCAR_APP_KEY, 'priority': 1}
                ]
            }
        }
    }

HANDCAR_MC3_DEV = {
    'id': 'handcar_mc3_dev',
    'displayName': 'Handcar MC3 Dev Configuration',
    'description': 'Configuration for Handcar MC3 Dev Service',
    'parameters':
        {
        'implKey':
            {
            'syntax': 'STRING',
            'displayName': 'Implementation Key',
            'description': 'Implementation key used by Runtime for class loading',
            'values':
                [
                    {'value': 'handcar', 'priority': 1}
                ]
           },
        'hostName':
            {
            'syntax': 'STRING',
            'displayName': 'Host Name',
            'description': 'Host Name for Handcar RESTFul Service Provider',
            'values':
                [
                    {'value': 'mc3-dev.mit.edu', 'priority': 1}
                ]
           },
        'appKey':
            {
            'syntax': 'STRING',
            'displayName': 'App Key',
            'description': 'Agent Key for Handcar service provider',
            'values':
                [
                    {'value': settings.MC3_DEV_HANDCAR_APP_KEY, 'priority': 1}
                ]
            }
        }
    }


