from dlkit.services.primitives import Type
from .locale_types import String

WORDIGNORECASE_STRING_MATCH_TYPE = Type(**String().get_type_data('WORDIGNORECASE'))
