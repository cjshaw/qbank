# test FBW Auth is like Open Banks
# Test taken queries
import os

from assessments_users.models import APIUser

from django.conf import settings

from dlkit.records.registry import QUESTION_RECORD_TYPES,\
    ANSWER_RECORD_TYPES,\
    ITEM_RECORD_TYPES, ITEM_GENUS_TYPES,\
    ASSESSMENT_RECORD_TYPES
from dlkit.runtime.primordium import Type

from urllib import unquote

from utilities.assessment import activate_managers
from utilities.general import get_session_data, clean_id
from utilities.testing import create_test_request, QBankBaseTest,\
    add_user_authz_to_settings, configure_proxyable_authz

EDX_ITEM_RECORD_TYPE = Type(**ITEM_RECORD_TYPES['edx_item'])
NUMERIC_RESPONSE_ITEM_GENUS_TYPE = Type(**ITEM_GENUS_TYPES['numeric-response-edx'])
NUMERIC_RESPONSE_ANSWER_RECORD_TYPE = Type(**ANSWER_RECORD_TYPES['numeric-response-edx'])
NUMERIC_RESPONSE_QUESTION_RECORD_TYPE = Type(**QUESTION_RECORD_TYPES['numeric-response-edx'])
SIMPLE_SEQUENCING_ASSESSMENT_RECORD_TYPE = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))


class BaseTest(QBankBaseTest):
    def create_test_bank(self, _name="test bank", _desc="for testing"):
        """
        helper method to create a test assessment bank
        """
        form = self.am.get_bank_form_for_create([])
        form.display_name = _name
        form.description = _desc
        return self.am.create_bank(form)

    def setUp(self):
        super(BaseTest, self).setUp()

        self.username = 'instructor@mit.edu'
        self.password = 'jinxem'
        self.user = APIUser.objects.create_user(username=self.username,
                                                password=self.password)
        self.student_name = 'student@mit.edu'
        self.student_password = 'blahblah'
        self.student = APIUser.objects.create_user(username=self.student_name,
                                                   password=self.student_password)

        self.endpoint = '/api/v2/'

        self.req = create_test_request(self.user)
        activate_managers(self.req)
        self.rm = get_session_data(self.req, 'rm')
        self.am = get_session_data(self.req, 'am')

        self._bank = self._get_test_bank()

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(BaseTest, self).tearDown()


class NumericAnswerTests(BaseTest):
    def create_assessment_offered_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCING_ASSESSMENT_RECORD_TYPE])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, item_id)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        return new_offered

    def create_item(self, bank_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(NUMERIC_RESPONSE_ITEM_GENUS_TYPE)
        new_item = bank.create_item(form)

        form = bank.get_question_form_for_create(item_id=new_item.ident,
                                                 question_record_types=[NUMERIC_RESPONSE_QUESTION_RECORD_TYPE])
        form.set_text('foo?')
        bank.create_question(form)

        self.right_answer = float(2.04)
        self.tolerance = float(0.71)
        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[NUMERIC_RESPONSE_ANSWER_RECORD_TYPE])
        form.set_decimal_value(self.right_answer)
        form.set_tolerance_value(self.tolerance)

        bank.create_answer(form)

        return bank.get_item(new_item.ident)

    def create_taken_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        bank = self.am.get_bank(bank_id)

        new_offered = self.create_assessment_offered_for_item(bank_id, item_id)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return taken, new_offered

    def setUp(self):
        super(NumericAnswerTests, self).setUp()

        self._item = self.create_item(self._bank.ident)
        self._taken, self._offered = self.create_taken_for_item(self._bank.ident, self._item.ident)

        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/items/'

        self.login()

    def tearDown(self):
        super(NumericAnswerTests, self).tearDown()

    def test_can_create_item(self):
        self.assertEqual(
            str(self._item.ident),
            str(self._item.ident)
        )

    def test_can_create_item_via_rest(self):
        payload = {
            'name': 'a clix testing question',
            'description': 'for testing clix items',
            'question': {
                'type': 'question-record-type%3Anumeric-response-edx%40ODL.MIT.EDU',
                'questionString': 'give me a number'
            },
            'answers': [{
                'decimalValue': -10.01,
                'tolerance': 0.1,
                'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
            }]
        }

        req = self.client.post(self.url, data=payload, format='json')
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['displayName']['text'],
            payload['name']
        )
        self.assertEqual(
            data['question']['text']['text'],
            payload['question']['questionString']
        )

        self.assertEqual(
            data['answers'][0]['decimalValue'],
            payload['answers'][0]['decimalValue']
        )

        self.assertEqual(
            data['answers'][0]['decimalValues']['tolerance'],
            payload['answers'][0]['tolerance']
        )

    def test_students_cannot_create_items(self):
        self.login(non_instructor=True)
        payload = {
            'name': 'a clix testing question',
            'description': 'for testing clix items',
            'question': {
                'type': 'question-record-type%3Anumeric-response-edx%40ODL.MIT.EDU',
                'questionString': 'give me a number'
            },
            'answers': [{
                'decimalValue': -10.01,
                'tolerance': 0.1,
                'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
            }]
        }

        req = self.client.post(self.url, data=payload, format='json')
        self.code(req, 403)

    def test_can_update_item_via_rest(self):
        url = self.url + unquote(str(self._item.ident))
        payload = {
            'answers': [{
                'id': str(self._item.get_answers().next().ident),
                'decimalValue': -10.01,
                'tolerance': 0.1,
                'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
            }]
        }

        req = self.client.put(url, data=payload, format='json')
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['id'],
            str(self._item.ident)
        )

        self.assertEqual(
            data['answers'][0]['decimalValue'],
            payload['answers'][0]['decimalValue']
        )

        self.assertEqual(
            data['answers'][0]['decimalValues']['tolerance'],
            payload['answers'][0]['tolerance']
        )

    def test_students_cannot_update_items(self):
        self.login(non_instructor=True)
        url = self.url + unquote(str(self._item.ident))
        payload = {
            'answers': [{
                'id': str(self._item.get_answers().next().ident),
                'decimalValue': -10.01,
                'tolerance': 0.1,
                'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
            }]
        }

        req = self.client.put(url, data=payload, format='json')
        self.code(req, 500)

    def test_valid_response_returns_correct(self):
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/{3}/submit'.format(self.endpoint,
                                                                                         unquote(str(self._bank.ident)),
                                                                                         unquote(str(self._taken.ident)),
                                                                                         unquote(str(self._item.ident)))
        payload = {
            'decimalValue': 1.5,
            'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
        }

        req = self.client.post(url, data=payload, format='json')
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertEqual(
            data['feedback'],
            'No feedback available.'
        )

    def test_invalid_response_returns_incorrect(self):
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/{3}/submit'.format(self.endpoint,
                                                                                         unquote(str(self._bank.ident)),
                                                                                         unquote(str(self._taken.ident)),
                                                                                         unquote(str(self._item.ident)))
        payload = {
            'decimalValue': 1.2,
            'type': 'answer-record-type%3Anumeric-response-edx%40ODL.MIT.EDU'
        }

        req = self.client.post(url, data=payload, format='json')
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback'],
            'No feedback available.'
        )


class DragAndDropTests(BaseTest):
    def create_assessment_offered_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCING_ASSESSMENT_RECORD_TYPE])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, item_id)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        return new_offered

    def create_item(self, bank_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(NUMERIC_RESPONSE_ITEM_GENUS_TYPE)
        new_item = bank.create_item(form)

        form = bank.get_question_form_for_create(item_id=new_item.ident,
                                                 question_record_types=[NUMERIC_RESPONSE_QUESTION_RECORD_TYPE])
        form.set_text('foo?')
        bank.create_question(form)

        self.right_answer = float(2.04)
        self.tolerance = float(0.71)
        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[NUMERIC_RESPONSE_ANSWER_RECORD_TYPE])
        form.set_decimal_value(self.right_answer)
        form.set_tolerance_value(self.tolerance)

        bank.create_answer(form)

        return bank.get_item(new_item.ident)

    def create_taken_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        bank = self.am.get_bank(bank_id)

        new_offered = self.create_assessment_offered_for_item(bank_id, item_id)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return taken, new_offered

    def setUp(self):
        super(DragAndDropTests, self).setUp()

        self._item = self.create_item(self._bank.ident)
        self._taken, self._offered = self.create_taken_for_item(self._bank.ident, self._item.ident)

        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/items/'

        self.login()

    def tearDown(self):
        super(DragAndDropTests, self).tearDown()
