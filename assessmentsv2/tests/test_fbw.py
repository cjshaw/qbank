# -*- coding: utf-8 -*-

# test FBW Auth is like Open Banks
# Test taken queries
import os
import boto
import json
import envoy
import time
import datetime
import unittest

from assessments_users.models import APIUser

from django.conf import settings
from django.test.utils import override_settings

from dlkit.json_.assessment.assessment_utilities import get_assessment_part_lookup_session

from http_signature.requests_auth import HTTPSignatureAuth

from random import randint

from dlkit.records.registry import ASSESSMENT_TAKEN_RECORD_TYPES, ITEM_RECORD_TYPES,\
    ITEM_GENUS_TYPES, ANSWER_GENUS_TYPES, QUESTION_RECORD_TYPES, ANSWER_RECORD_TYPES,\
    QUESTION_GENUS_TYPES, ASSESSMENT_RECORD_TYPES, ASSESSMENT_OFFERED_RECORD_TYPES,\
    ASSESSMENT_PART_RECORD_TYPES, ASSESSMENT_PART_GENUS_TYPES
from dlkit.runtime.primordium import Id, Type
from dlkit.runtime.errors import IllegalState

from mock import patch

from rest_framework.test import APITestCase, APIClient

from sqlalchemy import create_engine

from urllib import unquote

from utilities.assessment import activate_managers
from utilities.general import get_session_data, clear_cache
from utilities.testing import configure_test_bucket, calculate_signature,\
    create_test_request, QBankBaseTest,\
    add_user_authz_to_settings, configure_proxyable_authz
from utilities import resource as resutils

EDX_ITEM_RECORD_TYPE = Type(**ITEM_RECORD_TYPES['edx_item'])
EDX_MULTI_CHOICE_PROBLEM_TYPE = Type(**ITEM_GENUS_TYPES['multi-choice-edx'])
EDX_MULTI_CHOICE_QUESTION_TYPE = Type('question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU')
EDX_MULTI_CHOICE_ANSWER_TYPE = Type('answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU')
ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['advanced-query'])

FBW_ITEM_GENUS = Type(**ITEM_GENUS_TYPES['multi-choice-fbw'])
ITEM_WITH_SOLUTION_RECORD = Type(**ITEM_RECORD_TYPES['with-solution'])
MC_ANSWER_RECORD = Type(**ANSWER_RECORD_TYPES['multi-choice-with-files-and-feedback'])
MC_QUESTION_GENUS = Type(**QUESTION_GENUS_TYPES['multiple-choice'])
MC_QUESTION_RECORD = Type(**QUESTION_RECORD_TYPES['multi-choice-with-text-and-files'])
MC_RANDOMIZED_RECORD = Type(**QUESTION_RECORD_TYPES['multi-choice-randomized'])
MC_RANDOMIZED_ITEM_RECORD = Type(**ITEM_RECORD_TYPES['multi-choice-randomized'])
PROVENANCE_ITEM_RECORD = Type(**ITEM_RECORD_TYPES['provenance'])
SIMPLE_SEQUENCE_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])
SOURCEABLE_RECORD = Type(**ITEM_RECORD_TYPES['sourceable'])
WRONG_ANSWER_ITEM_RECORD = Type(**ITEM_RECORD_TYPES['wrong-answer'])

RIGHT_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['right-answer'])
WRONG_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['wrong-answer'])

REVIEWABLE_OFFERED = Type(**ASSESSMENT_OFFERED_RECORD_TYPES['review-options'])
REVIEWABLE_TAKEN = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['review-options'])
ADVANCED_QUERY_TAKEN = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['advanced-query'])

MC_ITEM_RECORD = Type(**ITEM_RECORD_TYPES['multi-choice'])
OBJECTIVE_BASED_ASSESSMENT_PART_RECORD = Type(**ASSESSMENT_PART_RECORD_TYPES['objective-based'])
MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING = Type(**ASSESSMENT_PART_RECORD_TYPES['scaffold-down'])
SIMPLE_SEQUENCE_PART_RECORD_TYPE = Type(**ASSESSMENT_PART_RECORD_TYPES['simple-child-sequencing'])

LO_ASSESSMENT_SECTION = Type(**ASSESSMENT_PART_GENUS_TYPES['fbw-specify-lo'])

FBW_PHASE_I_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-i'])
FBW_PHASE_II_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-ii'])

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))


class BaseTest(QBankBaseTest):
    def create_assessment_offered_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        if isinstance(item_id, basestring):
            item_id = Id(item_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, item_id)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        return new_offered

    def create_fbw_mc_item(self, bank_id, seed=None):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        bank = self.am.get_bank(bank_id)
        form = bank.get_item_form_for_create([WRONG_ANSWER_ITEM_RECORD,
                                              SOURCEABLE_RECORD,
                                              PROVENANCE_ITEM_RECORD,
                                              MC_RANDOMIZED_ITEM_RECORD])
        form.display_name = 'FbW MC item'
        item = bank.create_item(form)

        form = bank.get_question_form_for_create(item.ident,
                                                 [MC_RANDOMIZED_RECORD])

        form.display_name = "my question"
        if seed is None:
            form.set_text('what is life about? {0}'.format(str(randint(0, 10))))
        else:
            form.set_text('what is life about? {0}'.format(str(seed)))
        for index, choice in enumerate(['1', '42', '81']):
            form.add_choice(choice, 'Choice {0}'.format(str(index)))
        question = bank.create_question(form)

        choices = question.get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]

        form = bank.get_answer_form_for_create(item.ident,
                                               [MC_ANSWER_RECORD])
        form.add_choice_id(right_answer['id'])
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        form.set_feedback('right!')
        bank.create_answer(form)

        form = bank.get_answer_form_for_create(item.ident,
                                               [MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer['id'])
        self._confused_lo_id = 'foo%3A123%40MIT'
        form.set_confused_learning_objective_ids([self._confused_lo_id])
        form.set_genus_type(WRONG_ANSWER_GENUS)
        form.set_feedback('wrong ...')
        bank.create_answer(form)

        item = bank.get_item(item.ident)
        return item

    def create_item(self, bank_id):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        bank = self.am.get_bank(bank_id)
        form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(EDX_MULTI_CHOICE_PROBLEM_TYPE)
        new_item = bank.create_item(form)

        form = bank.get_question_form_for_create(item_id=new_item.ident,
                                                 question_record_types=[EDX_MULTI_CHOICE_QUESTION_TYPE])
        form.set_text('foo')
        # files get set after the form is returned, because
        # need the new_item
        # now manage the choices
        for ind in range(0, 4):
            form.add_choice(str(ind), 'Choice ' + str(int(ind) + 1))
        question = bank.create_question(form)

        # set the first choice as correct
        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[EDX_MULTI_CHOICE_ANSWER_TYPE])
        right_answer = question.get_choices()[0]['id']
        wrong_answer = question.get_choices()[1]['id']
        form.add_choice_id(right_answer)
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        bank.create_answer(form)

        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[EDX_MULTI_CHOICE_ANSWER_TYPE])
        form.add_choice_id(wrong_answer)
        form.set_genus_type(WRONG_ANSWER_GENUS)
        bank.create_answer(form)

        new_item = bank.get_item(new_item.ident)

        return new_item, right_answer, wrong_answer

    def create_matching_repo(self, bank_id):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        return self.rm.get_repository(bank_id)

    def create_resource_agent(self):
        resutils.activate_managers(self.req)
        resm = get_session_data(self.req, 'resm')

        bin = resm.get_bin(self._bank.ident)
        old_proxy = self.headers['X-Api-Proxy']
        self.headers['X-Api-Proxy'] = "MITx"
        files = {"avatar": open(ABS_PATH + '/tests/images/mitx_logo_rgb1.jpg', 'rb')}
        data = {"name": "MITx", "description": "a resource"}

        url = self.endpoint + 'resource/bins/' + unquote(str(bin.ident)) + '/resources/'

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        req = self.client.post(url, data=data, files=files, headers=self.headers)

        self.headers['X-Api-Proxy'] = old_proxy
        return self.json(req)['id']

    def create_taken_for_item(self, bank_id, item_id):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        if isinstance(item_id, basestring):
            item_id = Id(item_id)

        bank = self.am.get_bank(bank_id)

        new_offered = self.create_assessment_offered_for_item(bank_id, item_id)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return taken, new_offered

    def create_test_bank(self, _name="test bank", _desc="for testing"):
        """
        helper method to create a test assessment bank
        """
        form = self.am.get_bank_form_for_create([])
        form.display_name = _name
        form.description = _desc
        return self.am.create_bank(form)

    def num_items(self, bank, value):
        self.assertEqual(
            bank.get_items().available(),
            value
        )

    def remove_proxy_user(self):
        """
        """
        self.client.credentials()

    def setUp(self):
        super(BaseTest, self).setUp()
        self.username = 'instructor@mit.edu'
        self.password = 'jinxem'
        self.user = APIUser.objects.create_user(username=self.username,
                                                password=self.password)
        self.student_name = 'student@mit.edu'
        self.student_password = 'blahblah'
        self.student = APIUser.objects.create_user(username=self.student_name,
                                                   password=self.student_password)

        self.endpoint = '/api/v2/'

        self.req = create_test_request(self.user)
        activate_managers(self.req)
        self.rm = get_session_data(self.req, 'rm')
        self.am = get_session_data(self.req, 'am')

        self._bank = self._get_test_bank()

        self._item, self.right_answer, self.wrong_answer = self.create_item(self._bank.ident)
        self._taken, self._offered = self.create_taken_for_item(self._bank.ident, self._item.ident)

    def set_proxy_user(self, username):
        """
        """
        self.client.credentials(
            HTTP_X_API_PROXY=username
        )

    def tearDown(self):
        """
        """
        super(BaseTest, self).tearDown()


class BankTests(BaseTest):
    def setUp(self):
        super(BankTests, self).setUp()
        self.url = self.endpoint + 'assessment/banks/'
        self.login()

    def tearDown(self):
        super(BankTests, self).tearDown()

    def test_can_create_fly_by_wire_bank(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'bank-genus-type%3Afbw-bank%40ODL.MIT.EDU'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )

    def test_can_alias_banks_with_d2l_alias_id_on_create(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'bank-genus-type%3Afbw-bank%40ODL.MIT.EDU',
            'aliasId': 'assessment.Bank%3A123456%40COM.D2L.ACC'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        qbank_id = data['id']
        url = '{0}{1}'.format(self.url, payload['aliasId'])
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(qbank_id, data['id'])

    def test_can_update_bank_with_new_d2l_alias_id(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'bank-genus-type%3Afbw-bank%40ODL.MIT.EDU'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        qbank_id = data['id']
        url = '{0}{1}'.format(self.url, qbank_id)
        payload = {
            'aliasId': 'assessment.Bank%3A123456%40COM.D2L.ACC'
        }
        req = self.put(url, payload)
        self.ok(req)

        url = '{0}{1}'.format(self.url, payload['aliasId'])
        req = self.get(url)
        data = self.json(req)
        self.assertEqual(qbank_id, data['id'])

    def test_can_assign_objective_bank_id_on_create(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'bank-genus-type%3Afbw-bank%40ODL.MIT.EDU',
            'objectiveBankId': 'mc3-objectivebank%3A1%40MIT-OEIT'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        self.assertEqual(
            data['objectiveBankId'],
            payload['objectiveBankId']
        )
        qbank_id = data['id']
        url = '{0}{1}'.format(self.url, qbank_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['objectiveBankId'],
            payload['objectiveBankId']
        )

    def test_can_update_with_objective_bank_id(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'bank-genus-type%3Afbw-bank%40ODL.MIT.EDU'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['genusTypeId'],
            payload['genusTypeId']
        )
        self.assertNotIn('objectiveBankId', data)

        qbank_id = data['id']
        url = '{0}{1}'.format(self.url, qbank_id)

        payload = {
            'objectiveBankId': 'mc3-objectivebank%3A1%40MIT-OEIT'
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        req = self.get(url)
        data = self.json(req)
        self.assertEqual(
            data['objectiveBankId'],
            payload['objectiveBankId']
        )


class BankQueryTests(BaseTest):
    def setUp(self):
        super(BankQueryTests, self).setUp()
        self.url = self.endpoint + 'assessment/banks/'
        self.login()

    def tearDown(self):
        super(BankQueryTests, self).tearDown()

    def test_can_query_by_genus_type(self):
        payload = {
            'displayName': 'foo bank',
            'description': 'bar bank',
            'genusTypeId': 'assessment-bank-genus%3Afbw-content-libraries%40ODL.MIT.EDU'
        }

        req = self.post(self.url, payload)
        self.ok(req)
        data = self.json(req)
        bank_id = data['id']

        url = '{0}?genus_type_id=assessment-bank-genus%3Afbw-content-libraries%40ODL.MIT.EDU'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        self.assertEqual(
            len(data),
            1
        )
        self.assertEqual(
            data[0]['id'],
            bank_id
        )


class ProxyUserTests(BaseTest):
    def setUp(self):
        super(ProxyUserTests, self).setUp()

    def tearDown(self):
        super(ProxyUserTests, self).tearDown()

    def test_proxied_users_are_created_with_student_access(self):
        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/items/'
        self.login()
        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            2
        )

        self.set_proxy_user('foobar')

        configure_proxyable_authz(self.username,
                                  self._bank.ident)
        add_user_authz_to_settings('student',
                                   'foobar')
        add_user_authz_to_settings('student',
                                   'foobar',
                                   self._bank.ident)

        req = self.get(self.url)
        self.num_items(self._bank, 1)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            3
        )

    def test_unicode_usernames_okay(self):
        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/items/'
        self.login()
        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            2
        )

        self.set_proxy_user(u'fñoobár')

        configure_proxyable_authz(self.username,
                                  self._bank.ident)
        add_user_authz_to_settings('student',
                                   'foobr')
        add_user_authz_to_settings('student',
                                   'foobr',
                                   self._bank.ident)

        req = self.get(self.url)
        self.num_items(self._bank, 1)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            3
        )

    def test_usernames_with_strings_okay(self):
        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/items/'
        self.login()
        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            2
        )

        self.set_proxy_user('f oobar')

        configure_proxyable_authz(self.username,
                                  self._bank.ident)
        add_user_authz_to_settings('student',
                                   'f oobar')
        add_user_authz_to_settings('student',
                                   'f oobar',
                                   self._bank.ident)

        req = self.get(self.url)
        self.num_items(self._bank, 1)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

        self.assertEqual(
            len(APIUser.objects.all()),
            3
        )


@patch('dlkit.json_.assessment.sessions.DateTime')
class ItemQueryTests(BaseTest):
    def set_up_responses(self, mock_datetime):
        items = [self._item]
        offereds = [self._offered]
        right_answers = [self.right_answer]
        wrong_answers = [self.wrong_answer]
        for i in range(0, self.max_attempts):
            new_item, right_answer, wrong_answer = self.create_item(self._bank.ident)
            items.append(new_item)
            right_answers.append(right_answer)
            wrong_answers.append(wrong_answer)
            offereds.append(self.create_assessment_offered_for_item(self._bank.ident, new_item.ident))

        for i in range(0, self.num_weeks):
            for j in range(0, self.max_attempts):
                # now set the response
                # j False ones, then a True one
                for k in range(0, j):
                    form = self._student_bank.get_assessment_taken_form_for_create(offereds[j].ident,
                        [ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD])

                    taken = self._student_bank.create_assessment_taken(form)

                    mock_datetime.now.return_value = datetime.datetime.utcnow() - datetime.timedelta(weeks=i)
                    first_section = self._student_bank.get_first_assessment_section(taken.ident)

                    response_form = self._student_bank.get_response_form(assessment_section_id=first_section.ident,
                                                                         item_id=items[j].ident)
                    response_form.add_choice_id(wrong_answers[j])
                    self._student_bank.submit_response(first_section.ident,
                                                       items[j].ident,
                                                       response_form)

                form = self._student_bank.get_assessment_taken_form_for_create(offereds[j].ident,
                    [ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD])

                taken = self._student_bank.create_assessment_taken(form)

                mock_datetime.now.return_value = datetime.datetime.utcnow() - datetime.timedelta(weeks=i)
                first_section = self._student_bank.get_first_assessment_section(taken.ident)

                response_form = self._student_bank.get_response_form(assessment_section_id=first_section.ident,
                                                                     item_id=items[j].ident)
                response_form.add_choice_id(right_answers[j])
                self._student_bank.submit_response(first_section.ident,
                                                   items[j].ident,
                                                   response_form)

    def setUp(self):
        super(ItemQueryTests, self).setUp()

        self.student_req = create_test_request(self.student)
        activate_managers(self.student_req)
        self.student_rm = get_session_data(self.student_req, 'rm')
        self.student_am = get_session_data(self.student_req, 'am')

        self._student_bank = self.student_am.get_bank(self._bank.ident)

        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._student_bank.ident)) + '/items/'
        self.login()
        self.max_attempts = 10
        self.num_weeks = 5

    def tearDown(self):
        super(ItemQueryTests, self).tearDown()

    def test_can_query_for_wrong_responses_with_parameters(self, mock_datetime):
        self.set_up_responses(mock_datetime)

        self.num_items(self._bank, 11)

        num_attempts = range(0, 5)
        num_weeks = range(0, 2)

        for w in num_weeks:
            for a in num_attempts:
                url = '{0}?num_weeks={1}&num_attempts={2}&agent={3}'.format(self.url,
                                                                            w,
                                                                            a,
                                                                            self.student_name)
                req = self.get(url)
                self.ok(req)

                data = self.json(req)

                if a == 0 or w == 0:
                    expected_number = 0
                else:
                    expected_number = (self.max_attempts - a) * w
                self.assertEqual(
                    data['data']['count'],
                    expected_number
                )

    def test_can_get_wrong_answers_in_items_list(self, mock_datetime):
        url = '{0}assessment/banks/{1}/items?wronganswers'.format(self.endpoint,
                                                                  unquote(str(self._bank.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        item_to_test = [i for i in data if i['id'] == str(self._item.ident)][0]
        self.assertEqual(
            len(item_to_test['answers']),
            2
        )
        answer_genus_types = [a['genusTypeId'] for a in item_to_test['answers']]
        self.assertIn(str(RIGHT_ANSWER_GENUS), answer_genus_types)
        self.assertIn(str(WRONG_ANSWER_GENUS), answer_genus_types)

    def test_can_get_wrong_answers_in_assessment_items_list(self, mock_datetime):
        assessment_id_str = self._offered.object_map['assessmentId']
        url = '{0}assessment/banks/{1}/assessments/{2}/items?wronganswers'.format(self.endpoint,
                                                                                  unquote(str(self._bank.ident)),
                                                                                  unquote(assessment_id_str))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results'][0]
        self.assertEqual(
            len(data['answers']),
            2
        )
        answer_genus_types = [a['genusTypeId'] for a in data['answers']]
        self.assertIn(str(RIGHT_ANSWER_GENUS), answer_genus_types)
        self.assertIn(str(WRONG_ANSWER_GENUS), answer_genus_types)

    def test_can_get_unrandomized_question_choices(self, mock_datetime):
        self._fbw_item = self.create_fbw_mc_item(self._bank.ident)
        item_map = self._fbw_item._my_map
        question_map = item_map['question']
        original_choices = question_map['choices']

        for j in range(0, 10):
            url = '{0}assessment/banks/{1}/items?unrandomized&page=all'.format(self.endpoint,
                                                                               unquote(str(self._bank.ident)))
            req = self.get(url)
            self.ok(req)
            data = self.json(req)['data']['results']
            requested_item = [i for i in data if i['id'] == str(self._fbw_item.ident)][0]
            self.assertEqual(requested_item['question']['choices'], original_choices)

        different = False
        for j in range(0, 10):
            url = '{0}assessment/banks/{1}/items?page=all'.format(self.endpoint,
                                                                  unquote(str(self._bank.ident)))
            req = self.get(url)
            self.ok(req)
            data = self.json(req)['data']['results']
            requested_item = [i for i in data if i['id'] == str(self._fbw_item.ident)][0]
            if requested_item['question']['choices'] != original_choices:
                different = True

        self.assertTrue(different)


class AssessmentOfferedTests(BaseTest):
    def setUp(self):
        super(AssessmentOfferedTests, self).setUp()

        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident)) + '/assessmentsoffered/'
        self.login()

    def tearDown(self):
        super(AssessmentOfferedTests, self).tearDown()

    def test_can_set_deadline(self):
        url = '{0}{1}'.format(self.url,
                              self._offered.ident)
        payload = {
            'startTime': {
                'year': 2016,
                'month': 1,
                'day': 1
            },
            'deadline': {
                'year': 2016,
                'month': 1,
                'day': 14
            }
        }

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['deadline'],
            {'hour': 0,
             'month': 1,
             'second': 0,
             'microsecond': 0,
             'year': 2016,
             'day': 14,
             'minute': 0}
        )
        self.assertEqual(
            data['startTime'],
            {'hour': 0,
             'month': 1,
             'second': 0,
             'microsecond': 0,
             'year': 2016,
             'day': 1,
             'minute': 0}
        )

    def test_cannot_get_taken_if_past_offereds_deadline(self):
        url = '{0}{1}'.format(self.url,
                              self._offered.ident)

        now = datetime.datetime.utcnow()
        nowish = now + datetime.timedelta(minutes=5)
        future = now + datetime.timedelta(hours=1)
        past = now - datetime.timedelta(hours=1)

        payload = {
            'deadline': {
                'year': nowish.year,
                'month': nowish.month,
                'day': nowish.day,
                'hour': nowish.hour,
                'minute': nowish.minute,
                'second': nowish.second,
                'microsecond': nowish.microsecond
            }
        }

        req = self.put(url,
                       payload)

        taken_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken'.format(self.endpoint,
                                                                                             unquote(str(self._bank.ident)),
                                                                                             unquote(str(self._offered.ident)))
        # now try to create a taken
        req = self.post(taken_url)
        self.ok(req)

        payload = {
            'deadline': {
                'year': future.year,
                'month': future.month,
                'day': future.day,
                'hour': future.hour,
                'minute': future.minute,
                'second': future.second,
                'microsecond': future.microsecond
            }
        }

        req = self.put(url,
                       payload)

        # now try to create a taken again
        req = self.post(taken_url)
        self.ok(req)

        # now, using past as a deadline, should get a 403

        payload = {
            'deadline': {
                'year': past.year,
                'month': past.month,
                'day': past.day,
                'hour': past.hour,
                'minute': past.minute,
                'second': past.second,
                'microsecond': past.microsecond
            }
        }

        req = self.put(url,
                       payload)

        req = self.post(taken_url)
        self.code(req, 403)


class AssessmentTakenTests(BaseTest):
    def create_assessment_offered_for_items(self, bank_id, item_ids):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)

        bank = self.am.get_bank(bank_id)
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)
        for item_id in item_ids:
            if isinstance(item_id, basestring):
                item_id = Id(item_id)
            bank.add_item(new_assessment.ident, item_id)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        return new_offered

    def create_item(self, bank_id):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        bank = self.am.get_bank(bank_id)
        form = bank.get_item_form_for_create([WRONG_ANSWER_ITEM_RECORD,
                                              SOURCEABLE_RECORD,
                                              PROVENANCE_ITEM_RECORD,
                                              MC_RANDOMIZED_ITEM_RECORD])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(FBW_ITEM_GENUS)
        new_item = bank.create_item(form)

        form = bank.get_question_form_for_create(item_id=new_item.ident,
                                                 question_record_types=[MC_RANDOMIZED_RECORD])
        form.set_text('foo2')
        # files get set after the form is returned, because
        # need the new_item
        # now manage the choices
        form.set_genus_type(MC_QUESTION_GENUS)
        for ind in range(0, 4):
            form.add_choice(str(ind), 'Choice ' + str(int(ind) + 1))
        question = bank.create_question(form)

        # set the first choice as correct
        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[MC_ANSWER_RECORD])
        choices = question.get_unrandomized_choices()
        right_answer = choices[0]['id']
        wrong_answer = choices[1]['id']
        form.add_choice_id(right_answer)
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        bank.create_answer(form)

        form = bank.get_answer_form_for_create(item_id=new_item.ident,
                                               answer_record_types=[MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer)
        form.set_genus_type(WRONG_ANSWER_GENUS)
        self._confused_lo_id = 'foo%3A123%40MIT.EDU'
        form.set_confused_learning_objective_ids([self._confused_lo_id])
        bank.create_answer(form)

        new_item = bank.get_item(new_item.ident)

        return new_item, right_answer, wrong_answer

    def create_taken_for_items(self, bank_id, item_ids):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        if not isinstance(item_ids, list):
            item_ids = [item_ids]

        bank = self.am.get_bank(bank_id)

        new_offered = self.create_assessment_offered_for_items(bank_id, item_ids)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return taken, new_offered

    def create_taken_in_sections(self, bank_id, sections):
        if isinstance(bank_id, basestring):
            bank_id = Id(bank_id)
        if not isinstance(sections, list):
            sections = [sections]

        bank = self.am.get_bank(bank_id)

        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'for testing with multiple sections'
        assessment = bank.create_assessment(form)

        for index, section in enumerate(sections):
            form = bank.get_assessment_part_form_for_create_for_assessment(assessment.ident,
                                                                           [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD])
            form.display_name = 'Part {0}'.format(str(index))
            part = bank.create_assessment_part_for_assessment(form)
            for item_id in section['itemIds']:
                bank.add_item(Id(item_id), part.ident)

        form = bank.get_assessment_offered_form_for_create(assessment.ident, [REVIEWABLE_OFFERED])
        offered = bank.create_assessment_offered(form)

        form = bank.get_assessment_taken_form_for_create(offered.ident, [REVIEWABLE_TAKEN,
                                                                         ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD])
        taken = bank.create_assessment_taken(form)
        return taken

    def setUp(self):
        super(AssessmentTakenTests, self).setUp()
        self._item2, self.right_answer_2, self.wrong_answer2 = self.create_item(self._bank.ident)
        self._taken, self._offered = self.create_taken_for_items(self._bank.ident, [self._item.ident, self._item2.ident])

        self.url = '{0}assessment/banks/{1}/assessmentstaken/{2}'.format(self.endpoint,
                                                                         unquote(str(self._bank.ident)),
                                                                         unquote(str(self._taken.ident)))
        self.login()

    def tearDown(self):
        super(AssessmentTakenTests, self).tearDown()

    def test_instructor_can_get_history_of_answered_questions(self):
        # let's answer the first question wrong
        url = '{0}/take'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item.ident))
        first_question_id = data['id']

        url = '{0}/submit'.format(self.url)
        payload = {
            'choiceIds': [self.wrong_answer],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/take'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item2.ident))
        self.assertNotEqual(data['id'], first_question_id)

        url = '{0}/history'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['id'], first_question_id)
        self.assertIn('response', data[0])
        self.assertEqual(data[0]['response']['choiceIds'], [self.wrong_answer])
        self.assertFalse(data[0]['response']['isCorrect'])

    def test_student_can_get_history_of_answered_questions(self):
        # let's answer the first question wrong
        self.login(non_instructor=True)
        url = '{0}/take'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item.ident))
        first_question_id = data['id']

        url = '{0}/submit'.format(self.url)
        payload = {
            'choiceIds': [self.right_answer],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload,
                        non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])

        url = '{0}/take'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item2.ident))
        self.assertNotEqual(data['id'], first_question_id)

        url = '{0}/history'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['id'], first_question_id)
        self.assertIn('response', data[0])
        self.assertEqual(data[0]['response']['choiceIds'], [self.right_answer])
        self.assertTrue(data[0]['response']['isCorrect'])

    def test_student_sees_same_choice_order_on_repeat_gets(self):
        self.login(non_instructor=True)
        url = '{0}/take'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item.ident))
        first_question_id = data['id']
        first_choices = data['choices']

        for i in range(0, 10):
            req = self.get(url, non_instructor=True)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(data['id'], first_question_id)
            self.assertEqual(data['choices'], first_choices)

    def test_history_includes_confused_lo_id_with_response(self):
        self.login(non_instructor=True)
        url = '{0}/take'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item.ident))
        first_question_id = data['id']

        url = '{0}/submit'.format(self.url)
        payload = {
            'choiceIds': [self.wrong_answer],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload,
                        non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/take'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertNotEqual(data['id'], str(self._item2.ident))
        self.assertNotEqual(data['id'], first_question_id)

        url = '{0}/history'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['id'], first_question_id)
        self.assertIn('response', data[0])
        self.assertEqual(data[0]['response']['choiceIds'], [self.wrong_answer])
        self.assertFalse(data[0]['response']['isCorrect'])
        self.assertEqual(data[0]['response']['confusedLearningObjectiveIds'][0],
                         self._confused_lo_id)

    def test_student_can_get_bulk_questions(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(len(data) == 2)
        self.assertNotEqual(data['data']['results'][0]['questions'][0]['id'],
                            str(item1.ident))
        self.assertNotEqual(data['data']['results'][0]['questions'][1]['id'],
                            str(item2.ident))
        self.assertEqual(data['data']['results'][0]['questions'][0]['text']['text'],
                         item1.get_question().get_text().text)
        self.assertEqual(data['data']['results'][0]['questions'][1]['text']['text'],
                         item2.get_question().get_text().text)
        # displayLabel present?
        self.assertEqual(data['data']['results'][0]['questions'][0]['displayName']['text'], '1')
        self.assertEqual(data['data']['results'][0]['questions'][1]['displayName']['text'], '2')

    def test_student_sees_same_choice_order_on_repeat_bulk_question_gets(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        original_question_1_id = data['data']['results'][0]['questions'][0]['id']
        original_question_2_id = data['data']['results'][0]['questions'][1]['id']
        original_choice_order_1 = data['data']['results'][0]['questions'][0]['choices']
        original_choice_order_2 = data['data']['results'][0]['questions'][1]['choices']

        for i in range(0, 10):
            req = self.get(url, non_instructor=True)
            self.ok(req)
            data = self.json(req)
            self.assertEqual(
                data['data']['results'][0]['questions'][0]['choices'],
                original_choice_order_1
            )
            self.assertEqual(
                data['data']['results'][0]['questions'][1]['choices'],
                original_choice_order_2
            )
            self.assertEqual(
                data['data']['results'][0]['questions'][0]['id'],
                original_question_1_id
            )
            self.assertEqual(
                data['data']['results'][0]['questions'][1]['id'],
                original_question_2_id
            )

    def test_responses_present_in_bulk_question_gets(self):
        # let's answer the first one wrong
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']

        choices = item1.get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        url = '{0}/{1}/submit'.format(url,
                                      question_1_id)
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(data['data']['results'][0]['questions'][0]['id'], question_1_id)
        self.assertIn('response', data['data']['results'][0]['questions'][0])
        self.assertFalse(data['data']['results'][0]['questions'][0]['response']['isCorrect'])
        self.assertEqual(data['data']['results'][0]['questions'][0]['response']['choiceIds'],
                         [wrong_answer['id']])
        self.assertEqual(data['data']['results'][0]['questions'][0]['response']['confusedLearningObjectiveIds'],
                         [self._confused_lo_id])
        self.assertIsNone(data['data']['results'][0]['questions'][1]['response'])

    def test_confused_los_present_in_responses_in_bulk_question_gets(self):
        # let's answer the first one wrong
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']

        choices = item1.get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        url = '{0}/{1}/submit'.format(url,
                                      question_1_id)
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(data['data']['results'][0]['questions'][0]['response']['confusedLearningObjectiveIds'],
                         [self._confused_lo_id])

    def test_feedback_present_for_wrong_answers(self):
        # let's answer the first one wrong
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']

        choices = item1.get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        url = '{0}/{1}/submit'.format(url,
                                      question_1_id)
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(data['feedback']['text'], 'wrong ...')

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(data['data']['results'][0]['questions'][0]['response']['feedback']['text'],
                         'wrong ...')

    def test_feedback_present_for_right_answers(self):
        # let's answer the first one right!
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']

        choices = item1.get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        url = '{0}/{1}/submit'.format(url,
                                      question_1_id)
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertEqual(data['feedback']['text'], 'right!')

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(data['data']['results'][0]['questions'][0]['response']['feedback']['text'],
                         'right!')

    # def test_right_answer_solution_returned_from_surrender_endpoint(self):
    #     item1 = self.create_fbw_mc_item(self._bank.ident)
    #     item2 = self.create_fbw_mc_item(self._bank.ident)
    #     taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
    #     url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
    #                                                                           unquote(str(self._bank.ident)),
    #                                                                           unquote(str(taken.ident)))
    #     req = self.client.get(url)
    #     self.ok(req)
    #     data = self.json(req)
    #     question_1_id = data['data']['results'][0]['id']
    #
    #     url = '{0}/{1}/surrender'.format(url,
    #                                     question_1_id)
    #     req = self.client.post(url)
    #     self.ok(req)
    #     data = self.json(req)
    #     self.assertEqual(data['data']['count'], 2)
    #     self.assertEqual(data['data']['results'][0]['genusTypeId'], str(RIGHT_ANSWER_GENUS))
    #     self.assertEqual(data['data']['results'][0]['feedback']['text'], 'right!')
    #     self.assertEqual(data['data']['results'][1]['genusTypeId'], str(WRONG_ANSWER_GENUS))
    #     self.assertEqual(data['data']['results'][1]['feedback']['text'], 'wrong ...')

    def test_surrender_endpoint_logs_incorrect_response(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']

        url = '{0}/{1}/surrender'.format(url,
                                         question_1_id)
        req = self.post(url)
        self.ok(req)

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results'][0]['questions']),
            2
        )
        question_1_map = [q for q in data['data']['results'][0]['questions'] if q['id'] == question_1_id][0]
        self.assertTrue(question_1_map['responded'])
        self.assertFalse(question_1_map['isCorrect'])
        self.assertIn('response', question_1_map)
        self.assertEqual('I surrendered', question_1_map['response']['displayName']['text'])
        self.assertIn('confusedLearningObjectiveIds', question_1_map['response'])
        self.assertIn('feedback', question_1_map['response'])
        self.assertEqual(question_1_map['response']['feedback']['text'], 'wrong ...')

    def test_can_submit_answers_to_questions_in_different_sections(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)

        sections = [{
            'itemIds': [str(item1.ident)]
        },{
            'itemIds': [str(item2.ident)]
        }]

        taken = self.create_taken_in_sections(self._bank.ident, sections)

        questions_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                                        unquote(str(self._bank.ident)),
                                                                                        unquote(str(taken.ident)))
        req = self.get(questions_url)
        self.ok(req)
        data = self.json(req)

        question_1_id = data['data']['results'][0]['questions'][0]['id']
        question_2_id = data['data']['results'][1]['questions'][0]['id']

        url = '{0}/{1}/submit'.format(questions_url,
                                      question_1_id)

        choices = item1.get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }

        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(data['feedback']['text'], 'wrong ...')

        url = '{0}/{1}/submit'.format(questions_url,
                                      question_2_id)

        choices = item2.get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-record-type%3Amulti-choice-with-files-and-feedback%40ODL.MIT.EDU'
        }

        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertEqual(data['feedback']['text'], 'right!')

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            2
        )
        question_1_map = [q['questions'][0] for q in data['data']['results'] if q['questions'][0]['id'] == question_1_id][0]
        question_2_map = [q['questions'][0] for q in data['data']['results'] if q['questions'][0]['id'] == question_2_id][0]
        self.assertTrue(question_1_map['responded'])
        self.assertFalse(question_1_map['isCorrect'])
        self.assertIn('response', question_1_map)
        self.assertIn('confusedLearningObjectiveIds', question_1_map['response'])
        self.assertIn('feedback', question_1_map['response'])
        self.assertEqual(question_1_map['response']['feedback']['text'], 'wrong ...')

        self.assertTrue(question_2_map['responded'])
        self.assertTrue(question_2_map['isCorrect'])
        self.assertIn('response', question_2_map)
        self.assertIn('confusedLearningObjectiveIds', question_2_map['response'])
        self.assertIn('feedback', question_2_map['response'])
        self.assertEqual(question_2_map['response']['feedback']['text'], 'right!')

    def test_can_surrender_to_questions_in_different_sections(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)

        sections = [{
            'itemIds': [str(item1.ident)]
        },{
            'itemIds': [str(item2.ident)]
        }]

        taken = self.create_taken_in_sections(self._bank.ident, sections)

        questions_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                                        unquote(str(self._bank.ident)),
                                                                                        unquote(str(taken.ident)))
        req = self.get(questions_url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']
        question_2_id = data['data']['results'][1]['questions'][0]['id']

        url = '{0}/{1}/surrender'.format(questions_url,
                                         question_1_id)

        req = self.post(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['feedback']['text'],
            'wrong ...'
        )

        url = '{0}/{1}/surrender'.format(questions_url,
                                         question_2_id)

        req = self.post(url)
        self.ok(req)
        self.assertEqual(
            data['feedback']['text'],
            'wrong ...'
        )

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            2
        )
        question_1_map = [q['questions'][0] for q in data['data']['results'] if q['questions'][0]['id'] == question_1_id][0]
        question_2_map = [q['questions'][0] for q in data['data']['results'] if q['questions'][0]['id'] == question_2_id][0]
        self.assertTrue(question_1_map['responded'])
        self.assertFalse(question_1_map['isCorrect'])
        self.assertIn('response', question_1_map)
        self.assertEqual('I surrendered', question_1_map['response']['displayName']['text'])
        self.assertIn('confusedLearningObjectiveIds', question_1_map['response'])
        self.assertIn('feedback', question_1_map['response'])
        self.assertEqual(question_1_map['response']['feedback']['text'], 'wrong ...')

        self.assertTrue(question_2_map['responded'])
        self.assertFalse(question_2_map['isCorrect'])
        self.assertIn('response', question_2_map)
        self.assertEqual('I surrendered', question_2_map['response']['displayName']['text'])
        self.assertIn('confusedLearningObjectiveIds', question_2_map['response'])
        self.assertIn('feedback', question_2_map['response'])
        self.assertEqual(question_2_map['response']['feedback']['text'], 'wrong ...')

    def test_surrendering_to_final_question_does_not_end_taken_for_single_section(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)
        taken, offered = self.create_taken_for_items(self._bank.ident, [item1.ident, item2.ident])
        taken_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              unquote(str(self._bank.ident)),
                                                                              unquote(str(taken.ident)))
        req = self.get(taken_url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']
        question_2_id = data['data']['results'][0]['questions'][1]['id']

        url = '{0}/{1}/surrender'.format(taken_url,
                                         question_1_id)
        req = self.post(url)
        self.ok(req)
        url = '{0}/{1}/surrender'.format(taken_url,
                                         question_2_id)
        req = self.post(url)
        self.ok(req)

        taken = self._bank.get_assessment_taken(taken.ident)
        self.assertFalse(taken.has_ended())

    def test_surrendering_to_final_question_does_not_end_taken_for_multiple_sections(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)

        sections = [{
            'itemIds': [str(item1.ident)]
        },{
            'itemIds': [str(item2.ident)]
        }]

        taken = self.create_taken_in_sections(self._bank.ident, sections)

        questions_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                                        unquote(str(self._bank.ident)),
                                                                                        unquote(str(taken.ident)))
        req = self.get(questions_url)
        self.ok(req)
        data = self.json(req)
        question_1_id = data['data']['results'][0]['questions'][0]['id']
        question_2_id = data['data']['results'][1]['questions'][0]['id']

        url = '{0}/{1}/surrender'.format(questions_url,
                                         question_1_id)

        req = self.post(url)
        self.ok(req)

        url = '{0}/{1}/surrender'.format(questions_url,
                                         question_2_id)

        req = self.post(url)
        self.ok(req)

        taken = self._bank.get_assessment_taken(taken.ident)
        self.assertFalse(taken.has_ended())

    def test_can_get_sections_for_taken(self):
        item1 = self.create_fbw_mc_item(self._bank.ident)
        item2 = self.create_fbw_mc_item(self._bank.ident)

        sections = [{
            'itemIds': [str(item1.ident)]
        },{
            'itemIds': [str(item2.ident)]
        }]

        taken = self.create_taken_in_sections(self._bank.ident, sections)
        sections_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/sections?raw'.format(self.endpoint,
                                                                                          unquote(str(self._bank.ident)),
                                                                                          unquote(str(taken.ident)))
        req = self.get(sections_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['type'], 'AssessmentSection')
        self.assertEqual(data[1]['type'], 'AssessmentSection')
        self.assertNotIn('AssessmentPart', data[0]['id'])
        self.assertNotIn('AssessmentPart', data[1]['id'])
        self.assertIn('learningObjectiveId', data[0])
        self.assertIn('learningObjectiveId', data[1])
        # self.assertNotIn('questions', data[0])
        # self.assertNotIn('questions', data[1])
        # this behavior changed with our attempt to make results faster
        self.assertIn('questions', data[0])
        self.assertIn('questions', data[1])

    def test_can_get_questions_for_single_section(self):
        item1 = self.create_fbw_mc_item(self._bank.ident, seed=0)
        item2 = self.create_fbw_mc_item(self._bank.ident, seed=1)

        sections = [{
            'itemIds': [str(item1.ident)]
        },{
            'itemIds': [str(item2.ident)]
        }]

        taken = self.create_taken_in_sections(self._bank.ident, sections)
        sections_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/sections?raw'.format(self.endpoint,
                                                                                          unquote(str(self._bank.ident)),
                                                                                          unquote(str(taken.ident)))
        req = self.get(sections_url)
        self.ok(req)
        sections_data = self.json(req)
        section_1_id = sections_data[0]['id']
        section_2_id = sections_data[1]['id']

        section_1_questions_url = '{0}assessment/banks/{1}/assessmentsections/{2}/questions?raw'.format(self.endpoint,
                                                                                                        unquote(str(self._bank.ident)),
                                                                                                        unquote(section_1_id))
        req = self.get(section_1_questions_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['text']['text'],
                         item1.get_question().get_text().text)

        section_2_questions_url = '{0}assessment/banks/{1}/assessmentsections/{2}/questions?raw'.format(self.endpoint,
                                                                                                        unquote(str(self._bank.ident)),
                                                                                                        unquote(section_2_id))
        req = self.get(section_2_questions_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['text']['text'],
                         item2.get_question().get_text().text)


class ScaffoldedAssessmentTakenTests(BaseTest):
    def create_mc_item(self, level=None):
        form = self._item_bank.get_item_form_for_create([MC_ITEM_RECORD,  # Take out WRONG_ANSWER_ITEM_RECORD and replace
                                                         SOURCEABLE_RECORD,
                                                         PROVENANCE_ITEM_RECORD,
                                                         MC_RANDOMIZED_ITEM_RECORD,
                                                         ITEM_WITH_SOLUTION_RECORD])  # include item solutions record
        form.display_name = 'FbW MC item'
        if level is not None:
            form.set_learning_objectives([Id('foo%3A{0}%40MIT'.format(level))])
        form.set_solution('this is a solution')
        item = self._item_bank.create_item(form)

        form = self._item_bank.get_question_form_for_create(item.ident,
                                                            [MC_RANDOMIZED_RECORD])

        form.display_name = "my question"
        form.set_text('what is life about? {0} - {1} - {2}'.format(str(randint(0, 100)),
                                                                   str(level),
                                                                   str(randint(0, 1000))))
        for index, choice in enumerate(['1', '42', '81']):
            form.add_choice(choice, 'Choice {0}'.format(str(index)))
        question = self._item_bank.create_question(form)

        choices = question.get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]

        form = self._item_bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(right_answer['id'])
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        form.set_feedback('right!')
        self._item_bank.create_answer(form)

        form = self._item_bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer['id'])
        if level is not None:
            lo_identifier = level + 1
        else:
            lo_identifier = 1
        self._confused_lo_id = 'foo%3A{0}%40MIT'.format(str(lo_identifier))  # use this as a proxy for level down
        form.set_confused_learning_objective_ids([self._confused_lo_id])
        form.set_genus_type(WRONG_ANSWER_GENUS)
        form.set_feedback('wrong ...')
        self._item_bank.create_answer(form)

        item = self._item_bank.get_item(item.ident)
        return item

    def create_scaffolded_assessment(self, num_waypoints=4):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        item4 = self.create_mc_item()

        scaffold_level_1 = []
        scaffold_level_2 = []
        for i in range(0, num_waypoints):
            scaffold_level_1.append(self.create_mc_item(level=1))
            scaffold_level_2.append(self.create_mc_item(level=2))

        # let's make two parts, each with two items (so really two magic parts, for a total of 4 magic parts)
        form = self._assessment_bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = "for testing"
        assessment = self._assessment_bank.create_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment(assessment.ident,
                                                                                        [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'parent part 1'
        form.set_minimum_proficiency(Id('foo%3A1%40MIT'))
        form.set_learning_objective_id('foo%3A1%40MIT')
        parent_part_1 = self._assessment_bank.create_assessment_part_for_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_1.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 1'
        form.set_item_ids([item1.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_1 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_1.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 2'
        form.set_item_ids([item2.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_2 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        # second "parent" part
        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment(assessment.ident,
                                                                                        [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'parent part 2'
        form.set_minimum_proficiency(Id('foo%3A2%40MIT'))
        form.set_learning_objective_id('foo%3A42%40MIT')

        parent_part_2 = self._assessment_bank.create_assessment_part_for_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_2.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 3'
        form.set_item_ids([item3.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_3 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_2.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 4'
        form.set_item_ids([item4.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_4 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        self._items = {
            'target': [item1, item2, item3, item4],
            'waypoint1': scaffold_level_1,
            'waypoint2': scaffold_level_2
        }

        return self._assessment_bank.get_assessment(assessment.ident)

    def create_taken(self):
        # make sure this data still shows up when taking the assessment
        assessment = self.create_scaffolded_assessment()
        offered_form = self._assessment_bank.get_assessment_offered_form_for_create(assessment.ident,
                                                                                    [REVIEWABLE_OFFERED])
        self._offered = self._assessment_bank.create_assessment_offered(offered_form)

        taken_form = self._assessment_bank.get_assessment_taken_form_for_create(self._offered.ident,
                                                                                [REVIEWABLE_TAKEN,
                                                                                 ADVANCED_QUERY_TAKEN])
        return self._assessment_bank.create_assessment_taken(taken_form)

    def setUp(self):
        super(ScaffoldedAssessmentTakenTests, self).setUp()
        self._item_bank = self._bank
        self._assessment_bank = self.create_test_bank('assessment bank',
                                                      'that houses assessments only')

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self._assessment_bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=self._assessment_bank.ident)
        self._taken = self.create_taken()
        self.url = '{0}assessment/banks/{1}/assessmentstaken/{2}'.format(self.endpoint,
                                                                         unquote(str(self._assessment_bank.ident)),
                                                                         unquote(str(self._taken.ident)))
        self.login(non_instructor=True)

    def tearDown(self):
        super(ScaffoldedAssessmentTakenTests, self).tearDown()

    def validate_number_sections_and_questions(self, sections, expected_tuple):
        expected_num_sections = expected_tuple[0]
        expected_num_q_per_section = expected_tuple[1]
        self.assertEqual(
            len(sections),
            expected_num_sections
        )
        for index, section in enumerate(sections):
            self.assertEqual(
                len(section['questions']),
                expected_num_q_per_section[index]
            )

    def test_scaffolded_magic_part_questions_appear_in_taken(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        self.assertEqual(
            len(data),
            2
        )

        self.validate_number_sections_and_questions(data, (2, (2, 2)))

        for index, question in enumerate(data[0]['questions']):
            self.assertEqual(self._items['target'][index].get_question().get_text().text,
                             question['text']['text'])
            self.assertEqual(
                question['displayName']['text'],
                str(index + 1)
            )
        for index, question in enumerate(data[1]['questions']):
            self.assertEqual(self._items['target'][index + 2].get_question().get_text().text,
                             question['text']['text'])
            self.assertEqual(
                question['displayName']['text'],
                str(index + 1)
            )

    def test_scaffolded_magic_part_adds_question_on_wrong_response(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_1 = data[0]['questions'][0]
        question_1_id = question_1['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_1_id)
        choices = self._items['target'][0].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']

        self.validate_number_sections_and_questions(data, (2, (3, 2)))

        question_display_names = [q['displayName']['text'] for q in data[0]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '1.1', '2']
        )

        question_display_names = [q['displayName']['text'] for q in data[1]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '2']
        )

    def test_scaffolded_magic_part_adds_question_on_wrong_response_for_last_section(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_4 = data[1]['questions'][1]
        question_4_id = question_4['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_4_id)
        choices = self._items['target'][3].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']

        self.validate_number_sections_and_questions(data, (2, (2, 3)))

        question_display_names = [q['displayName']['text'] for q in data[0]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '2']
        )

        question_display_names = [q['displayName']['text'] for q in data[1]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '2', '2.1']
        )

    def test_scaffolded_magic_part_does_not_add_question_on_right_response(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_4 = data[1]['questions'][1]
        question_4_id = question_4['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_4_id)
        choices = self._items['target'][3].get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']

        self.validate_number_sections_and_questions(data, (2, (2, 2)))

        question_display_names = [q['displayName']['text'] for q in data[0]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '2']
        )

        question_display_names = [q['displayName']['text'] for q in data[1]['questions']]
        self.assertEqual(
            question_display_names,
            ['1', '2']
        )

    def test_scaffolded_magic_part_adds_question_per_wrong_response_objective_id(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_4 = data[1]['questions'][1]
        question_4_id = question_4['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_4_id)
        choices = self._items['target'][3].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']

        self.validate_number_sections_and_questions(data, (2, (2, 3)))

        new_question = data[1]['questions'][2]
        self.assertTrue(any(w.get_question().get_text().text == new_question['text']['text']
                            for w in self._items['waypoint1']))

    def test_next_question_on_submit_right_answer_returns_target_single_section(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_1 = data[0]['questions'][0]
        question_1_id = question_1['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_1_id)
        choices = self._items['target'][0].get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])

        # only way to check that they are equal?
        # displayNames will be different because of the displayElements
        # ID will be different because of the session IDs
        self.assertEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][1].get_question().get_text().text
        )

    def test_next_question_on_submit_wrong_answer_returns_scaffold_single_section(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_1 = data[0]['questions'][0]
        question_1_id = question_1['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_1_id)
        choices = self._items['target'][0].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        # only way to check that they are equal?
        # displayNames will be different because of the displayElements
        # ID will be different because of the session IDs
        self.assertNotEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][1].get_question().get_text().text
        )
        self.assertTrue(any(w.get_question().get_text().text == data['nextQuestion']['text']['text']
                            for w in self._items['waypoint1']))

    def test_next_question_on_submit_right_answer_returns_target_second_section(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])

        # only way to check that they are equal?
        # displayNames will be different because of the displayElements
        # ID will be different because of the session IDs
        self.assertEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][3].get_question().get_text().text
        )

    def test_next_question_on_submit_wrong_answer_returns_scaffold_second_section(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        # only way to check that they are equal?
        # displayNames will be different because of the displayElements
        # ID will be different because of the session IDs
        self.assertNotEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][3].get_question().get_text().text
        )
        self.assertTrue(any(w.get_question().get_text().text == data['nextQuestion']['text']['text']
                            for w in self._items['waypoint1']))

    def test_no_next_question_on_right_answer_for_last_question(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_4 = data[1]['questions'][1]
        question_4_id = question_4['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_4_id)
        choices = self._items['target'][3].get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertIsNone(data['nextQuestion'])

    def test_getting_questions_shows_worked_solution_for_right_response(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        payload = {
            'choiceIds': [right_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        self.assertIn('response', question_3)
        self.assertEqual(
            question_3['response']['feedback']['text'],
            'this is a solution'
        )

    def test_getting_questions_shows_worked_solution_for_wrong_response(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        self.assertIn('response', question_3)
        self.assertEqual(
            question_3['response']['feedback']['text'],
            'this is a solution'
        )

    def test_getting_questions_shows_confused_lo_for_wrong_response(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        self.assertIn('response', question_3)
        self.assertEqual(
            question_3['response']['feedback']['text'],
            'this is a solution'
        )
        self.assertEqual(
            question_3['response']['confusedLearningObjectiveIds'],
            ['foo%3A1%40MIT']
        )

    def test_submitting_wrong_response_includes_worked_solution(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )

    def test_submitting_wrong_response_includes_confused_lo(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )
        self.assertEqual(
            data['confusedLearningObjectiveIds'],
            ['foo%3A1%40MIT']
        )

    def test_surrendering_returns_worked_solution(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/surrender'.format(self.url,
                                                   question_3_id)

        req = self.post(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['feedback']['text'],
            'this is a solution'
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        self.assertIn('response', question_3)
        self.assertEqual(
            question_3['response']['feedback']['text'],
            'this is a solution'
        )

    def test_surrendering_returns_is_correct_false(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/surrender'.format(self.url,
                                                   question_3_id)

        req = self.post(url)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        self.assertFalse(question_3['isCorrect'])

    def test_surrendering_to_target_returns_target_for_next_question(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/surrender'.format(self.url,
                                                   question_3_id)

        req = self.post(url)
        self.ok(req)
        data = self.json(req)
        self.assertIsNotNone(data['nextQuestion'])
        self.assertEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][3].get_question().get_text().text
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        self.assertEqual(
            len(data[1]['questions']),
            2
        )

    def test_surrendering_to_waypoint_returns_waypoint_with_same_lo_for_next_question(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        wrong_answer = [c for c in question_3['choices'] if c['name'] == 'Choice 1'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        self.assertEqual(
            len(data[1]['questions']),
            3
        )
        new_question = data[1]['questions'][1]
        self.assertTrue(any(new_question['text']['text'] in i.get_question().get_text().text
                            for i in self._items['waypoint1']))

        url = '{0}/questions/{1}/surrender'.format(self.url,
                                                   new_question['id'])

        req = self.post(url)
        self.ok(req)
        data = self.json(req)
        self.assertIsNotNone(data['nextQuestion'])
        self.assertNotEqual(
            data['nextQuestion']['text']['text'],
            self._items['target'][3].get_question().get_text().text
        )
        self.assertTrue(any(data['nextQuestion']['text']['text'] in i.get_question().get_text().text
                            for i in self._items['waypoint1']))
        self.assertEqual(
            data['nextQuestion']['learningObjectiveIds'],
            new_question['learningObjectiveIds']
        )

        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        self.assertEqual(
            len(data[1]['questions']),
            4
        )

    # cannot do authz tests with memcached managers...
    # def test_students_cannot_get_results(self):
    #     url = '{0}/questions'.format(self.url)
    #     req = self.get(url)
    #     self.ok(req)
    #     data = self.json(req)['data']['results']
    #     question_3 = data[1]['questions'][0]
    #     question_3_id = question_3['id']
    #
    #     url = '{0}/questions/{1}/submit'.format(self.url,
    #                                             question_3_id)
    #     choices = self._items['target'][2].get_question().get_choices()
    #     wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
    #     payload = {
    #         'choiceIds': [wrong_answer['id']],
    #         'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
    #     }
    #     req = self.post(url,
    #                     payload)
    #     self.ok(req)
    #     data = self.json(req)
    #     self.assertFalse(data['isCorrect'])
    #
    #     offered_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/results'.format(self.endpoint,
    #                                                                                   unquote(str(self._assessment_bank.ident)),
    #                                                                                   unquote(str(self._offered.ident)))
    #     req = self.get(offered_url, non_instructor=True)
    #     self.code(req, 403)

    def test_can_get_results_for_taken(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        offered_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/results'.format(self.endpoint,
                                                                                      unquote(str(self._assessment_bank.ident)),
                                                                                      unquote(str(self._offered.ident)))
        req = self.get(offered_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(data['data']['count'], 1)
        self.assertIn('sections', data['data']['results'][0])
        self.assertEqual(len(data['data']['results'][0]['sections']), 2)
        self.assertEqual(
            len(data['data']['results'][0]['sections'][0]['questions']),
            2
        )
        self.assertEqual(
            len(data['data']['results'][0]['sections'][1]['questions']),
            3
        )

        for index, question in enumerate(data['data']['results'][0]['sections'][0]['questions']):
            self.assertIn('learningObjectiveIds', question)
            self.assertIsNone(question['response'])

        for index, question in enumerate(data['data']['results'][0]['sections'][1]['questions']):
            self.assertIn('learningObjectiveIds', question)
            if index != 0:
                self.assertIsNone(question['response'])
            else:
                self.assertEqual(
                    question['response']['choiceIds'],
                    [wrong_answer['id']]
                )
                self.assertFalse(question['response']['isCorrect'])

        self.assertEqual(
            data['data']['results'][0]['sections'][1]['questions'][1]['learningObjectiveIds'],
            ['foo%3A1%40MIT']
        )

    def test_no_responses_but_questions_still_show_up_in_offered_results(self):
        # need to generate the taken questions first.
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)

        offered_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/results'.format(self.endpoint,
                                                                                      unquote(str(self._assessment_bank.ident)),
                                                                                      unquote(str(self._offered.ident)))
        req = self.get(offered_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(data['data']['count'], 1)
        self.assertIn('sections', data['data']['results'][0])
        self.assertEqual(
            len(data['data']['results'][0]['sections'][0]['questions']),
            2
        )
        self.assertEqual(
            len(data['data']['results'][0]['sections'][1]['questions']),
            2
        )
        for index, question in enumerate(data['data']['results'][0]['sections'][0]['questions']):
            self.assertIn('learningObjectiveIds', question)
            self.assertIsNone(question['response'])
        for index, question in enumerate(data['data']['results'][0]['sections'][1]['questions']):
            self.assertIn('learningObjectiveIds', question)
            self.assertIsNone(question['response'])

        self.assertEqual(
            data['data']['results'][0]['sections'][1]['questions'][1]['learningObjectiveIds'],
            []
        )

    def test_can_get_results_for_specific_agent_id(self):
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        section_2_id = data[1]['id']
        question_3 = data[1]['questions'][0]
        question_3_id = question_3['id']

        url = '{0}/questions/{1}/submit'.format(self.url,
                                                question_3_id)
        choices = self._items['target'][2].get_question().get_choices()
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]
        payload = {
            'choiceIds': [wrong_answer['id']],
            'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
        }
        req = self.post(url,
                        payload)
        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])

        offered_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/results?agentId={3}'.format(self.endpoint,
                                                                                                  unquote(str(self._assessment_bank.ident)),
                                                                                                  unquote(str(self._offered.ident)),
                                                                                                  self.username)
        req = self.get(offered_url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(data['data']['count'], 2)
        self.assertEqual(
            len(data['data']['results'][0]['questions']),
            2
        )
        self.assertEqual(
            len(data['data']['results'][1]['questions']),
            3
        )
        self.assertEqual(
            data['data']['results'][1]['id'],
            section_2_id
        )
        for index, question in enumerate(data['data']['results'][1]['questions']):
            self.assertIn('learningObjectiveIds', question)
            if index != 0:
                self.assertIsNone(question['response'])
            else:
                self.assertEqual(
                    question['response']['choiceIds'],
                    [wrong_answer['id']]
                )
                self.assertFalse(question['response']['isCorrect'])

        self.assertEqual(
            data['data']['results'][1]['questions'][1]['learningObjectiveIds'],
            ['foo%3A1%40MIT']
        )

    def test_exception_thrown_if_no_taken_for_agent_when_getting_results(self):
        # need to generate the taken questions first.
        url = '{0}/questions'.format(self.url)
        req = self.get(url)
        self.ok(req)

        offered_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/results?agentId=foo@acc.edu'.format(self.endpoint,
                                                                                                          unquote(str(self._assessment_bank.ident)),
                                                                                                          unquote(str(self._offered.ident)))
        req = self.get(offered_url)
        self.code(req, 500)
        self.message(req, 'Object not found.')


class ScaffoldedAssessmentTakenBottomLevelTests(BaseTest):
    # Make a slightly different test case than ScaffoldedAssessmentTakenTests
    # because we want to tweak the behavior of the tree
    # 1) it bottoms out at level = 2
    # 2) only one waypoint for each LO
    #
    # those will help us debug any corner cases at the bottom level
    def create_mc_item(self, level=None):
        form = self._item_bank.get_item_form_for_create([MC_ITEM_RECORD,  # Take out WRONG_ANSWER_ITEM_RECORD and replace
                                                         SOURCEABLE_RECORD,
                                                         PROVENANCE_ITEM_RECORD,
                                                         MC_RANDOMIZED_ITEM_RECORD,
                                                         ITEM_WITH_SOLUTION_RECORD])  # include item solutions record
        form.display_name = 'FbW MC item'
        if level is not None:
            form.set_learning_objectives([Id('foo%3A{0}%40MIT'.format(level))])
        form.set_solution('this is a solution')
        item = self._item_bank.create_item(form)

        form = self._item_bank.get_question_form_for_create(item.ident,
                                                            [MC_RANDOMIZED_RECORD])

        form.display_name = "my question"
        form.set_text('what is life about? {0} - {1} - {2}'.format(str(randint(0, 100)),
                                                                   str(level),
                                                                   str(randint(0, 1000))))
        for index, choice in enumerate(['1', '42', '81']):
            form.add_choice(choice, 'Choice {0}'.format(str(index)))
        question = self._item_bank.create_question(form)

        choices = question.get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]

        form = self._item_bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(right_answer['id'])
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        form.set_feedback('right!')
        self._item_bank.create_answer(form)

        form = self._item_bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer['id'])
        if level is not None:
            lo_identifier = level + 1
        else:
            lo_identifier = 1
        self._confused_lo_id = 'foo%3A{0}%40MIT'.format(str(lo_identifier))  # use this as a proxy for level down
        # let level 2 be the bottom of the tree
        if level != 2:
            form.set_confused_learning_objective_ids([self._confused_lo_id])
        form.set_genus_type(WRONG_ANSWER_GENUS)
        form.set_feedback('wrong ...')
        self._item_bank.create_answer(form)

        item = self._item_bank.get_item(item.ident)
        return item

    def create_scaffolded_assessment(self, num_waypoints=4):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        item4 = self.create_mc_item()

        scaffold_level_1 = []
        scaffold_level_2 = []
        for i in range(0, num_waypoints):
            scaffold_level_1.append(self.create_mc_item(level=1))
            scaffold_level_2.append(self.create_mc_item(level=2))

        # let's make two parts, each with two items (so really two magic parts, for a total of 4 magic parts)
        form = self._assessment_bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = "for testing"
        assessment = self._assessment_bank.create_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment(assessment.ident,
                                                                                        [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'parent part 1'
        form.set_minimum_proficiency(Id('foo%3A1%40MIT'))
        form.set_learning_objective_id('foo%3A1%40MIT')
        parent_part_1 = self._assessment_bank.create_assessment_part_for_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_1.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 1'
        form.set_item_ids([item1.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_1 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_1.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 2'
        form.set_item_ids([item2.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_2 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        # second "parent" part
        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment(assessment.ident,
                                                                                        [OBJECTIVE_BASED_ASSESSMENT_PART_RECORD,
                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'parent part 2'
        form.set_minimum_proficiency(Id('foo%3A2%40MIT'))
        form.set_learning_objective_id('foo%3A42%40MIT')

        parent_part_2 = self._assessment_bank.create_assessment_part_for_assessment(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_2.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 3'
        form.set_item_ids([item3.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_3 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        form = self._assessment_bank.get_assessment_part_form_for_create_for_assessment_part(parent_part_2.ident,
                                                                                             [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                              SIMPLE_SEQUENCE_PART_RECORD_TYPE])
        form.display_name = 'magic part 4'
        form.set_item_ids([item4.ident])
        form.set_item_bank_id(self._item_bank.ident)
        form.set_waypoint_quota(1)
        magic_part_4 = self._assessment_bank.create_assessment_part_for_assessment_part(form)

        self._items = {
            'target': [item1, item2, item3, item4],
            'waypoint1': scaffold_level_1,
            'waypoint2': scaffold_level_2
        }

        return self._assessment_bank.get_assessment(assessment.ident)

    def create_taken(self, num_waypoints=1):
        # make sure this data still shows up when taking the assessment
        assessment = self.create_scaffolded_assessment(num_waypoints)
        offered_form = self._assessment_bank.get_assessment_offered_form_for_create(assessment.ident,
                                                                                    [REVIEWABLE_OFFERED])
        self._offered = self._assessment_bank.create_assessment_offered(offered_form)

        taken_form = self._assessment_bank.get_assessment_taken_form_for_create(self._offered.ident,
                                                                                [REVIEWABLE_TAKEN,
                                                                                 ADVANCED_QUERY_TAKEN])
        return self._assessment_bank.create_assessment_taken(taken_form)

    def setUp(self):
        super(ScaffoldedAssessmentTakenBottomLevelTests, self).setUp()
        self._item_bank = self._bank
        self._assessment_bank = self.create_test_bank('assessment bank',
                                                      'that houses assessments only')

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self._assessment_bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=self._assessment_bank.ident)
        self._taken = self.create_taken(num_waypoints=1)
        self.url = '{0}assessment/banks/{1}/assessmentstaken/{2}'.format(self.endpoint,
                                                                         unquote(str(self._assessment_bank.ident)),
                                                                         unquote(str(self._taken.ident)))
        self.login(non_instructor=True)

    def tearDown(self):
        super(ScaffoldedAssessmentTakenBottomLevelTests, self).tearDown()

    def test_several_wrong_at_bottom_of_tree_still_generate_one_level_up_when_finally_right(self):
        # to test issue #24
        def get_questions():
            req = self.get(taken_url)
            return self.json(req)['data']['results']

        def submit_wrong_answer(question):
            wrong_choice = [c for c in question['choices'] if c['name'] == 'Choice 1'][0]
            payload = {
                'choiceIds': [wrong_choice['id']],
                'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
            }
            url = '{0}/{1}/submit'.format(taken_url,
                                          question['id'])
            req = self.post(url,
                            payload)
            self.ok(req)
            data = self.json(req)
            self.assertFalse(data['isCorrect'])

        def submit_right_answer(question):
            right_choice = [c for c in question['choices'] if c['name'] == 'Choice 0'][0]
            payload = {
                'choiceIds': [right_choice['id']],
                'type': 'answer-genus-type%3Amulti-choice-fbw%40ODL.MIT.EDU'
            }
            url = '{0}/{1}/submit'.format(taken_url,
                                          question['id'])
            req = self.post(url,
                            payload)
            self.ok(req)
            data = self.json(req)
            self.assertTrue(data['isCorrect'])

        taken_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                                    unquote(str(self._assessment_bank.ident)),
                                                                                    unquote(str(self._taken.ident)))
        sections = get_questions()
        self.assertEqual(len(sections[0]['questions']), 2)
        question_1 = sections[0]['questions'][0]

        submit_wrong_answer(question_1)

        sections = get_questions()
        self.assertEqual(len(sections[0]['questions']), 3)
        new_question = sections[0]['questions'][1]
        self.assertEqual(
            new_question['text']['text'],
            self._items['waypoint1'][0].get_question().get_text().text
        )

        submit_wrong_answer(new_question)

        for i in range(2, 8):
            sections = get_questions()
            self.assertEqual(len(sections[0]['questions']), i + 2)
            new_question = sections[0]['questions'][i]

            self.assertEqual(
                new_question['text']['text'],
                self._items['waypoint2'][0].get_question().get_text().text
            )

            submit_wrong_answer(new_question)

        sections = get_questions()
        self.assertEqual(len(sections[0]['questions']), 10)
        new_question = sections[0]['questions'][8]

        self.assertEqual(
            new_question['text']['text'],
            self._items['waypoint2'][0].get_question().get_text().text
        )

        submit_right_answer(new_question)

        # next new question should be waypoint1!
        sections = get_questions()
        self.assertEqual(len(sections[0]['questions']), 11)
        new_question = sections[0]['questions'][9]
        self.assertEqual(
            new_question['text']['text'],
            self._items['waypoint1'][0].get_question().get_text().text
        )
        self.assertNotEqual(
            new_question['text']['text'],
            self._items['waypoint2'][0].get_question().get_text().text
        )

        submit_right_answer(new_question)

        # next new question should be target 2
        # so we don't get a new one
        sections = get_questions()
        self.assertEqual(len(sections[0]['questions']), 11)
        new_question = sections[0]['questions'][10]
        self.assertEqual(
            new_question['text']['text'],
            self._items['target'][1].get_question().get_text().text
        )
        self.assertNotEqual(
            new_question['text']['text'],
            self._items['waypoint1'][0].get_question().get_text().text
        )


class AuthzHintTests(BaseTest):
    def setUp(self):
        super(AuthzHintTests, self).setUp()

        self.url = self.endpoint + 'assessment/banks/' + unquote(str(self._bank.ident))
        self.login()

    def tearDown(self):
        super(AuthzHintTests, self).tearDown()

    def test_can_get_take_assessment_hint(self):
        url = '{0}/assessments/cantake'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['canTake'])

        req = self.get(url, non_instructor=True)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['canTake'])


class AssessmentSectionTests(BaseTest):
    def create_mc_item(self, item_lo='foo%3A1%40ODL.MIT.EDU'):
        form = self._bank.get_item_form_for_create([WRONG_ANSWER_ITEM_RECORD,
                                                    SOURCEABLE_RECORD,
                                                    PROVENANCE_ITEM_RECORD,
                                                    MC_RANDOMIZED_ITEM_RECORD])
        form.display_name = 'FbW MC item'
        form.set_learning_objectives([Id(item_lo)])

        item = self._bank.create_item(form)

        form = self._bank.get_question_form_for_create(item.ident,
                                                       [MC_RANDOMIZED_RECORD])

        form.display_name = "my question"
        form.set_text('what is life about? {0}'.format(str(randint(0, 10))))
        for index, choice in enumerate(['1', '42', '81']):
            form.add_choice(choice, 'Choice {0}'.format(str(index)))
        question = self._bank.create_question(form)

        choices = question.get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]

        form = self._bank.get_answer_form_for_create(item.ident,
                                                     [MC_ANSWER_RECORD])
        form.add_choice_id(right_answer['id'])
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        form.set_feedback('right!')
        self._bank.create_answer(form)

        form = self._bank.get_answer_form_for_create(item.ident,
                                                     [MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer['id'])
        self._confused_lo_id = 'foo%3A123%40MIT'
        form.set_confused_learning_objective_ids([self._confused_lo_id])
        form.set_genus_type(WRONG_ANSWER_GENUS)
        form.set_feedback('wrong ...')
        self._bank.create_answer(form)

        item = self._bank.get_item(item.ident)
        return item

    def create_taken_for_assessment(self, assessment_id):
        form = self._bank.get_assessment_offered_form_for_create(assessment_id,
                                                                 [REVIEWABLE_OFFERED])
        form.display_name = 'FbW offered'
        offered = self._bank.create_assessment_offered(form)

        form = self._bank.get_assessment_taken_form_for_create(offered.ident,
                                                               [REVIEWABLE_TAKEN,
                                                                ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD])

        form.display_name = "taken record"
        return self._bank.create_assessment_taken(form)

    def get_magic_part(self, magic_part_id):
        apls = get_assessment_part_lookup_session(runtime=self._bank._catalog._runtime,
                                                  proxy=self._bank._catalog._proxy)
        apls.use_federated_bank_view()
        apls.use_unsequestered_assessment_part_view()
        return apls.get_assessment_part(magic_part_id)

    def num_parts(self, val, sequestered=False):
        if sequestered:
            self._bank.use_unsequestered_assessment_part_view()
        else:
            self._bank.use_sequestered_assessment_part_view()
        self.assertEqual(
            self._bank.get_assessment_parts().available(),
            int(val)
        )

    def setUp(self):
        super(AssessmentSectionTests, self).setUp()

        self.url = '{0}assessment/banks/{1}/assessments'.format(self.endpoint,
                                                                unquote(str(self._bank.ident)))

    def tearDown(self):
        super(AssessmentSectionTests, self).tearDown()

    def test_can_create_multiple_sections(self):
        payload = {
            'sections': [{
                "learningObjectiveId": "foo"
            },
            {
                "learningObjectiveId": "baz"
            }]
        }

        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            2
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.learning_objective_id,
            'foo'
        )
        second_part = assessment.get_next_assessment_part(first_part_id)
        self.assertEqual(
            second_part.learning_objective_id,
            'baz'
        )

    def test_can_assign_lo_id_to_section(self):
        payload = {
            'sections': [{
                "learningObjectiveId": "foo"
            },
            {
                "learningObjectiveId": "baz"
            }]
        }

        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            2
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.learning_objective_id,
            'foo'
        )
        second_part = assessment.get_next_assessment_part(first_part_id)
        self.assertEqual(
            second_part.learning_objective_id,
            'baz'
        )

    def test_can_assign_min_proficiency_to_section(self):
        payload = {
            'sections': [{
                "minimumProficiency": "1"
            },
            {
                "minimumProficiency": "3"
            }]
        }

        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            2
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.minimum_proficiency,
            '1'
        )
        second_part = assessment.get_next_assessment_part(first_part_id)
        self.assertEqual(
            second_part.minimum_proficiency,
            '3'
        )

    def test_can_update_section_lo(self):
        payload = {
            'sections': [{
                "learningObjectiveId": "foo"
            }]
        }

        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.learning_objective_id,
            'foo'
        )

        payload = {
            'sections': {
                'updatedSections': [{
                    "id": data['childIds'][0],
                    "learningObjectiveId": "baz"
                }]
            }
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.learning_objective_id,
            'baz'
        )

    def test_can_update_section_min_proficiency(self):
        payload = {
            'sections': [{
                "minimumProficiency": "1"
            }]
        }

        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.minimum_proficiency,
            '1'
        )

        payload = {
            'sections': {
                'updatedSections': [{
                    "id": data['childIds'][0],
                    "minimumProficiency": "42"
                }]
            }
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        first_part_id = assessment.get_child_ids().next()
        first_part = self._bank.get_assessment_part(first_part_id)
        self.assertEqual(
            first_part.minimum_proficiency,
            '42'
        )

    def test_can_assign_items_directly_to_section(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)]
            },{
                'itemIds': [str(item3.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            2
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        child_ids = assessment.get_child_ids()
        first_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(first_part_id)
        self.assertEqual(
            items.available(),
            2
        )
        self.assertEqual(str(items.next().ident), str(item1.ident))
        self.assertEqual(str(items.next().ident), str(item2.ident))

        second_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(second_part_id)
        self.assertEqual(
            items.available(),
            1
        )
        self.assertEqual(str(items.next().ident), str(item3.ident))

    def test_can_update_items_in_a_section(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        child_ids = assessment.get_child_ids()
        first_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(first_part_id)
        self.assertEqual(
            items.available(),
            2
        )
        self.assertEqual(str(items.next().ident), str(item1.ident))
        self.assertEqual(str(items.next().ident), str(item2.ident))

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': data['childIds'][0],
                    'itemIds': [str(item3.ident)]
                }]
            }
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)

        assessment = self._bank.get_assessment(Id(assessment_id))
        child_ids = assessment.get_child_ids()
        first_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(first_part_id)
        self.assertEqual(
            items.available(),
            1
        )
        self.assertEqual(str(items.next().ident), str(item3.ident))

    def test_can_remove_all_items_from_a_section(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}'.format(self.url,
                               assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['childIds']),
            1
        )

        assessment = self._bank.get_assessment(Id(assessment_id))
        child_ids = assessment.get_child_ids()
        first_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(first_part_id)
        self.assertEqual(
            items.available(),
            2
        )
        self.assertEqual(str(items.next().ident), str(item1.ident))
        self.assertEqual(str(items.next().ident), str(item2.ident))

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': data['childIds'][0],
                    'itemIds': []
                }]
            }
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)

        assessment = self._bank.get_assessment(Id(assessment_id))
        child_ids = assessment.get_child_ids()
        first_part_id = child_ids.next()
        items = self._bank.get_assessment_part_items(first_part_id)
        self.assertEqual(
            items.available(),
            0
        )

    def test_part_items_appear_when_getting_assessment_items(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)]
            },{
                'itemIds': [str(item3.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}/items'.format(self.url,
                                     assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            3
        )
        self.assertEqual(
            data['data']['results'][0]['id'],
            str(item1.ident)
        )
        self.assertEqual(
            data['data']['results'][1]['id'],
            str(item2.ident)
        )
        self.assertEqual(
            data['data']['results'][2]['id'],
            str(item3.ident)
        )

    def test_part_items_appear_in_assessment_items_even_for_one_part(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'scaffold': True
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}/items'.format(self.url,
                                     assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            2
        )
        self.assertEqual(
            data['data']['results'][0]['id'],
            str(item1.ident)
        )
        self.assertEqual(
            data['data']['results'][1]['id'],
            str(item2.ident)
        )

    def test_can_supply_section_flag_to_separate_assessment_items(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)]
            },{
                'itemIds': [str(item3.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}/items?sections'.format(self.url,
                                              assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            2
        )
        self.assertEqual(
            len(data['data']['results'][0]['questions']),
            2
        )
        self.assertIn('id', data['data']['results'][0])
        self.assertIn('learningObjectiveId', data['data']['results'][0])
        self.assertIn('minimumProficiency', data['data']['results'][0])
        self.assertEqual(
            data['data']['results'][0]['questions'][0]['id'],
            str(item1.ident)
        )
        self.assertEqual(
            data['data']['results'][0]['questions'][1]['id'],
            str(item2.ident)
        )
        self.assertEqual(
            len(data['data']['results'][1]['questions']),
            1
        )
        self.assertEqual(
            data['data']['results'][1]['questions'][0]['id'],
            str(item3.ident)
        )

    def test_cannot_update_assessment_items_via_items_endpoint_if_multiple_parts(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident)]
            },{
                'itemIds': [str(item3.ident)]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}/items'.format(self.url,
                                     assessment_id)
        payload = {
            'itemIds': [str(item2.ident)]
        }
        req = self.post(url,
                        payload)
        self.code(req, 500)

        req = self.put(url,
                       payload)
        self.code(req, 500)

    def test_section_metadata_shows_up_in_taken_questions_list(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        taken = self.create_taken_for_assessment(Id(assessment_id))
        url = '{0}taken/{1}/questions'.format(self.url,
                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            2
        )
        self.assertEqual(
            data['data']['results'][0]['learningObjectiveId'],
            'foo%3A123%40MIT'
        )
        self.assertEqual(
            data['data']['results'][0]['minimumProficiency'],
            ''
        )

        self.assertEqual(
            data['data']['results'][1]['learningObjectiveId'],
            ''
        )
        self.assertEqual(
            data['data']['results'][1]['minimumProficiency'],
            '2'
        )

    def test_section_items_show_up_in_taken_question_list(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        taken = self.create_taken_for_assessment(Id(assessment_id))
        url = '{0}taken/{1}/questions'.format(self.url,
                                              unquote(str(taken.ident)))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            2
        )
        self.assertEqual(
            len(data['data']['results'][0]['questions']),
            2
        )
        self.assertNotEqual(
            data['data']['results'][0]['questions'][0]['id'],
            str(item1.ident)
        )
        self.assertIn('id', data['data']['results'][0])
        # can't check on IDs restfully ... so check the question string ...
        self.assertEqual(item1.get_question().get_text().text,
                         data['data']['results'][0]['questions'][0]['text']['text'])
        self.assertNotEqual(
            data['data']['results'][0]['questions'][1]['id'],
            str(item2.ident)
        )
        self.assertEqual(item2.get_question().get_text().text,
                         data['data']['results'][0]['questions'][1]['text']['text'])

        self.assertEqual(
            len(data['data']['results'][1]['questions']),
            1
        )
        self.assertNotEqual(
            data['data']['results'][1]['questions'][0]['id'],
            str(item3.ident)
        )
        self.assertEqual(item3.get_question().get_text().text,
                         data['data']['results'][1]['questions'][0]['text']['text'])

    def test_section_metadata_shows_up_in_assessments_list_if_requested(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)

        req = self.get(self.url)
        self.ok(req)
        data = self.json(req)
        self.assertNotIn('sections', data['data']['results'][0])

        url = '{0}?sections&raw'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data[0])

        self.assertIn('id', data[0]['sections'][0])
        self.assertIn('id', data[0]['sections'][1])

        self.assertEqual(
            len(data[0]['sections'][0]['itemIds']),
            2
        )
        self.assertEqual(
            data[0]['sections'][0]['itemIds'][0],
            str(item1.ident)
        )
        self.assertEqual(
            data[0]['sections'][0]['itemIds'][1],
            str(item2.ident)
        )
        self.assertEqual(
            data[0]['sections'][0]['learningObjectiveId'],
            'foo%3A123%40MIT'
        )

        self.assertEqual(
            len(data[0]['sections'][1]['itemIds']),
            1
        )
        self.assertEqual(
            data[0]['sections'][1]['itemIds'][0],
            str(item3.ident)
        )
        self.assertEqual(
            data[0]['sections'][1]['minimumProficiency'],
            '2'
        )

    def test_section_metadata_shows_up_in_assessment_details_if_requested(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertNotIn('sections', data)

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        self.assertIn('id', data['sections'][1])

        self.assertEqual(
            len(data['sections'][0]['itemIds']),
            2
        )
        self.assertEqual(
            data['sections'][0]['itemIds'][0],
            str(item1.ident)
        )
        self.assertEqual(
            data['sections'][0]['itemIds'][1],
            str(item2.ident)
        )
        self.assertEqual(
            data['sections'][0]['learningObjectiveId'],
            'foo%3A123%40MIT'
        )

        self.assertEqual(
            len(data['sections'][1]['itemIds']),
            1
        )
        self.assertEqual(
            data['sections'][1]['itemIds'][0],
            str(item3.ident)
        )
        self.assertEqual(
            data['sections'][1]['minimumProficiency'],
            '2'
        )

    def test_new_section_data_shows_up_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'newSections': [{}]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            3
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertIn('id', data['sections'][1])
        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id)
        self.assertNotEqual(
            data['sections'][2]['id'],
            section_1_id
        )
        self.assertNotEqual(
            data['sections'][2]['id'],
            section_2_id
        )

    def test_deleting_a_section_reflects_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'oldSectionIds': [section_1_id]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            1
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_2_id)
        self.assertEqual(len(data['sections'][0]['itemIds']), 1)
        self.assertEqual(data['sections'][0]['itemIds'][0],
                         str(item3.ident))

    def test_assigning_a_section_lo_reflects_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': section_2_id,
                    'learningObjectiveId': 'baz%3A987%40MIT'
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            data['sections'][0]['learningObjectiveId'],
            'foo%3A123%40MIT')
        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id
        )
        self.assertEqual(
            data['sections'][1]['learningObjectiveId'],
            'baz%3A987%40MIT'
        )

    def test_changing_a_section_lo_reflects_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': section_1_id,
                    'learningObjectiveId': 'baz%3A987%40MIT'
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            data['sections'][0]['learningObjectiveId'],
            'baz%3A987%40MIT')
        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id
        )
        self.assertEqual(
            data['sections'][1]['learningObjectiveId'],
            ''
        )

    def test_can_change_quota_for_magic_parts(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT',
                'scaffold': True,
                'quota': 1
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertEqual(
            self.get_magic_part(Id(data['sections'][0]['childIds'][0])).object_map['waypointQuota'],
            1
        )

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'updatedSections': [{
                    'itemIds': [str(item1.ident), str(item2.ident)],
                    'id': section_1_id,
                    'learningObjectiveId': 'baz%3A987%40MIT',
                    'quota': 42,
                    'scaffold': True
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            data['sections'][0]['learningObjectiveId'],
            'baz%3A987%40MIT')

        self.assertEqual(
            self.get_magic_part(Id(data['sections'][0]['childIds'][0])).object_map['waypointQuota'],
            42
        )

        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id
        )
        self.assertEqual(
            data['sections'][1]['learningObjectiveId'],
            ''
        )

    def test_assigning_a_minimum_proficiency_reflects_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': section_1_id,
                    'minimumProficiency': '3'
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            data['sections'][0]['minimumProficiency'],
            '3')
        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id
        )
        self.assertEqual(
            data['sections'][1]['minimumProficiency'],
            '2'
        )

    def test_changing_a_minimum_proficiency_reflects_in_update_assessment_return_data(self):
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': section_2_id,
                    'minimumProficiency': '10'
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            data['sections'][0]['minimumProficiency'],
            '')
        self.assertEqual(
            data['sections'][1]['id'],
            section_2_id
        )
        self.assertEqual(
            data['sections'][1]['minimumProficiency'],
            '10'
        )

    def test_can_create_scaffolded_magic_parts(self):
        # 1 already in the system
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT',
                'scaffold': True
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        self.num_parts(3, sequestered=False)
        self.num_parts(5, sequestered=True)

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        self.assertIn('id', data['sections'][1])
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            2
        )
        self.assertNotIn('itemIds', data['sections'][0])
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            0
        )
        self.assertEqual(
            len(data['sections'][1]['itemIds']),
            1
        )

    def test_can_create_magic_parts_with_waypoint_quota(self):
        # 1 already in the system
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT',
                'scaffold': True,
                'quota': 1
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        self.num_parts(3, sequestered=False)
        self.num_parts(5, sequestered=True)

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        self.assertIn('id', data['sections'][1])
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            2
        )
        self.assertEqual(
            self.get_magic_part(Id(data['sections'][0]['childIds'][0])).object_map['waypointQuota'],
            1
        )
        self.assertEqual(
            self.get_magic_part(Id(data['sections'][0]['childIds'][1])).object_map['waypointQuota'],
            1
        )
        self.assertNotIn('itemIds', data['sections'][0])
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            0
        )
        self.assertEqual(
            len(data['sections'][1]['itemIds']),
            1
        )

    def test_can_update_assessment_with_scaffolded_magic_parts(self):
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']

        self.num_parts(2, sequestered=False)
        self.num_parts(2, sequestered=True)

        payload = {
            'sections': {
                'newSections': [{
                    'itemIds': [str(item2.ident), str(item3.ident)],
                    'learningObjectiveId': 'foo%3A123%40MIT',
                    'scaffold': True
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.num_parts(3, sequestered=False)
        self.num_parts(5, sequestered=True)

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertIn('id', data['sections'][1])
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            2
        )
        self.assertNotIn('itemIds', data['sections'][1])
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            0
        )
        self.assertEqual(
            len(data['sections'][0]['itemIds']),
            1
        )

    def test_can_update_assessment_with_magic_part_with_waypoint_quota(self):
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']

        self.num_parts(2, sequestered=False)
        self.num_parts(2, sequestered=True)

        payload = {
            'sections': {
                'newSections': [{
                    'itemIds': [str(item2.ident), str(item3.ident)],
                    'learningObjectiveId': 'foo%3A123%40MIT',
                    'scaffold': True,
                    'quota': 5
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            2
        )

        self.num_parts(3, sequestered=False)
        self.num_parts(5, sequestered=True)

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertIn('id', data['sections'][1])
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            2
        )

        self.assertEqual(
            self.get_magic_part(Id(data['sections'][1]['childIds'][0])).object_map['waypointQuota'],
            5
        )

        self.assertNotIn('itemIds', data['sections'][1])
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            0
        )
        self.assertEqual(
            len(data['sections'][0]['itemIds']),
            1
        )

    def test_can_remove_scaffolded_parts_from_assessment(self):
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident), str(item2.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT',
                'scaffold': True
            },{
                'itemIds': [str(item3.ident)],
                'minimumProficiency': '2'
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertIn('id', data['sections'][1])
        section_2_id = data['sections'][1]['id']

        self.num_parts(3, sequestered=False)
        self.num_parts(5, sequestered=True)

        payload = {
            'sections': {
                'oldSectionIds': [section_1_id]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            1
        )

        self.num_parts(2, sequestered=False)
        self.num_parts(2, sequestered=True)

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_2_id)
        self.assertEqual(len(data['sections'][0]['itemIds']), 1)
        self.assertEqual(data['sections'][0]['itemIds'][0],
                         str(item3.ident))

    def test_can_change_the_items_in_a_scaffolded_part(self):
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()
        payload = {
            'sections': [{
                'itemIds': [str(item1.ident)],
                'learningObjectiveId': 'foo%3A123%40MIT',
                'scaffold': True
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        url = '{0}/{1}?sections'.format(self.url,
                                        assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)

        self.assertIn('id', data['sections'][0])
        section_1_id = data['sections'][0]['id']
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            1
        )
        prev_part_id = data['sections'][0]['childIds'][0]
        self.assertNotIn('itemIds', data['sections'][0])

        self.num_parts(2, sequestered=False)
        self.num_parts(3, sequestered=True)

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': section_1_id,
                    'itemIds': [str(item2.ident), str(item3.ident)],
                    'learningObjectiveId': 'foo%3A123%40MIT',
                    'scaffold': True
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)
        self.assertIn('sections', data)
        self.assertEqual(
            len(data['sections']),
            1
        )

        self.num_parts(2, sequestered=False)
        self.num_parts(4, sequestered=True)

        self.assertIn('id', data['sections'][0])
        self.assertEqual(
            data['sections'][0]['id'],
            section_1_id)
        self.assertEqual(
            len(data['sections'][0]['childIds']),
            2
        )
        self.assertNotIn('itemIds', data['sections'][0])
        self.assertNotIn(prev_part_id, data['sections'][0]['childIds'])

    def test_can_set_only_lo_for_magic_sections(self):
        # 1 already in the system
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()

        lo_id = 'foo%3A1%40MIT'
        payload = {
            'sections': [{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 2,
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            },{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 5,
                'minimumProficiency': '2',
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']

        self.num_parts(3, sequestered=False)
        self.num_parts(10, sequestered=True)

        url = '{0}/{1}/items?sections'.format(self.url,
                                              assessment_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        # data = sections

        self.assertIn('id', data[0])
        self.assertIn('id', data[1])
        self.assertEqual(
            len(data[0]['childIds']),
            2
        )
        self.assertNotIn('itemIds', data[0])
        for q in data[0]['questions']:
            self.assertEqual(q['learningObjectiveIds'][0],
                             lo_id)
        self.assertEqual(
            len(data[1]['childIds']),
            5
        )
        self.assertNotIn('itemIds', data[1])
        for q in data[1]['questions']:
            self.assertEqual(q['learningObjectiveIds'][0],
                             lo_id)

    def test_can_bulk_update_assessment_sections(self):
        # 1 already in the system
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()

        lo_id = 'foo%3A1%40MIT'
        payload = {
            'sections': [{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 2,
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            },{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 5,
                'minimumProficiency': '2',
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        data = self.json(req)
        assessment_id = data['id']
        old_child_ids = data['childIds']

        self.num_parts(3, sequestered=False)
        self.num_parts(10, sequestered=True)

        url = '{0}/{1}'.format(self.url,
                               assessment_id)

        req = self.get(url + '?sections')
        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            len(data['sections'][0]['childIds']),
            2
        )
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            5
        )

        payload = {
            'sections': [{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 3,
                'learningObjectiveId': lo_id,
                'minimumProficiency': '10',
                'itemBankId': item1._my_map['assignedBankIds'][0]
            },{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 2,
                'minimumProficiency': '2',
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            },{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 1,
                'minimumProficiency': '5',
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            }]
        }
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            len(data['childIds']),
            3
        )
        for old_id in old_child_ids:
            self.assertNotIn(old_id, data['childIds'])

        self.assertEqual(
            data['sections'][0]['minimumProficiency'],
            '10'
        )
        self.assertEqual(
            data['sections'][1]['minimumProficiency'],
            '2'
        )
        self.assertEqual(
            data['sections'][2]['minimumProficiency'],
            '5'
        )

        self.assertEqual(
            len(data['sections'][0]['childIds']),
            3
        )
        self.assertEqual(
            len(data['sections'][1]['childIds']),
            2
        )
        self.assertEqual(
            len(data['sections'][2]['childIds']),
            1
        )

        for section in data['sections']:
            self.assertEqual(section['learningObjectiveId'],
                             lo_id)

    def test_can_edit_lo_only_magic_sections(self):
        # 1 already in the system
        self.num_parts(1, sequestered=False)
        self.num_parts(1, sequestered=True)
        item1 = self.create_mc_item()
        item2 = self.create_mc_item()
        item3 = self.create_mc_item()

        item4 = self.create_mc_item(item_lo='foo%3Aabc%40ODL.MIT.EDU')
        item5 = self.create_mc_item(item_lo='foo%3Aabc%40ODL.MIT.EDU')
        item6 = self.create_mc_item(item_lo='foo%3Aabc%40ODL.MIT.EDU')

        lo_id = 'foo%3A1%40MIT'
        lo2_id = 'foo%3Aabc%40MIT'
        payload = {
            'sections': [{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 2,
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            },{
                'type': str(LO_ASSESSMENT_SECTION),
                'quota': 5,
                'minimumProficiency': '2',
                'learningObjectiveId': lo_id,
                'itemBankId': item1._my_map['assignedBankIds'][0]
            }]
        }
        req = self.post(self.url,
                        payload)
        self.ok(req)
        assessment = self.json(req)

        payload = {
            'sections': {
                'updatedSections': [{
                    'id': assessment['childIds'][0],
                    'type': str(LO_ASSESSMENT_SECTION),
                    'quota': 6,
                    'learningObjectiveId': lo2_id,
                    'itemBankId': item1._my_map['assignedBankIds'][0]
                },{
                    'id': assessment['childIds'][1],
                    'type': str(LO_ASSESSMENT_SECTION),
                    'quota': 1,
                    'minimumProficiency': '2',
                    'learningObjectiveId': lo2_id,
                    'itemBankId': item1._my_map['assignedBankIds'][0]
                }]
            }
        }
        url = '{0}/{1}'.format(self.url,
                               assessment['id'])
        req = self.put(url,
                       payload)
        self.ok(req)
        data = self.json(req)

        self.num_parts(3, sequestered=False)
        self.num_parts(10, sequestered=True)

        url = '{0}/{1}/items?sections'.format(self.url,
                                              assessment['id'])
        req = self.get(url)
        self.ok(req)
        data = self.json(req)['data']['results']
        # data = sections

        self.assertIn('id', data[0])
        self.assertIn('id', data[1])
        self.assertEqual(
            len(data[0]['childIds']),
            6
        )
        self.assertNotIn('itemIds', data[0])
        for q in data[0]['questions']:
            self.assertEqual(q['learningObjectiveIds'][0],
                             lo2_id)
        self.assertEqual(
            len(data[1]['childIds']),
            1
        )
        self.assertNotIn('itemIds', data[1])
        for q in data[1]['questions']:
            self.assertEqual(q['learningObjectiveIds'][0],
                             lo2_id)


class SpawnableAssessmentTests(BaseTest):
    def create_mc_item(self, level=None):
        form = self._bank.get_item_form_for_create([MC_ITEM_RECORD,  # Take out WRONG_ANSWER_ITEM_RECORD and replace
                                                         SOURCEABLE_RECORD,
                                                         PROVENANCE_ITEM_RECORD,
                                                         MC_RANDOMIZED_ITEM_RECORD,
                                                         ITEM_WITH_SOLUTION_RECORD])  # include item solutions record
        form.display_name = 'FbW MC item'
        if level is not None:
            form.set_learning_objectives([Id('foo%3A{0}%40MIT'.format(level))])
        form.set_solution('this is a solution')
        item = self._bank.create_item(form)

        form = self._bank.get_question_form_for_create(item.ident,
                                                            [MC_RANDOMIZED_RECORD])

        form.display_name = "my question"
        form.set_text('what is life about? {0} - {1} - {2}'.format(str(randint(0, 100)),
                                                                   str(level),
                                                                   str(randint(0, 1000))))
        for index, choice in enumerate(['1', '42', '81']):
            form.add_choice(choice, 'Choice {0}'.format(str(index)))
        question = self._bank.create_question(form)

        choices = question.get_choices()
        right_answer = [c for c in choices if c['name'] == 'Choice 0'][0]
        wrong_answer = [c for c in choices if c['name'] == 'Choice 1'][0]

        form = self._bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(right_answer['id'])
        form.set_genus_type(RIGHT_ANSWER_GENUS)
        form.set_feedback('right!')
        self._bank.create_answer(form)

        form = self._bank.get_answer_form_for_create(item.ident,
                                                          [MC_ANSWER_RECORD])
        form.add_choice_id(wrong_answer['id'])
        if level is not None:
            lo_identifier = level + 1
        else:
            lo_identifier = 1
        self._confused_lo_id = 'foo%3A{0}%40MIT'.format(str(lo_identifier))  # use this as a proxy for level down
        form.set_confused_learning_objective_ids([self._confused_lo_id])
        form.set_genus_type(WRONG_ANSWER_GENUS)
        form.set_feedback('wrong ...')
        self._bank.create_answer(form)

        item = self._bank.get_item(item.ident)
        return item

    def setUp(self):
        super(SpawnableAssessmentTests, self).setUp()
        self._bank2 = self.create_test_bank('second bank',
                                            'that houses another assessment')
        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=self._bank2.ident)

        self.url = '{0}assessment/banks/{1}/assessments'.format(self.endpoint,
                                                                unquote(str(self._bank.ident)))

    def tearDown(self):
        super(SpawnableAssessmentTests, self).tearDown()

    def test_can_set_spawnable_state(self):
        # on create
        payload = {
            'name': 'Test Phase I',
            'recordTypeIds': [str(FBW_PHASE_I_ASSESSMENT_RECORD)],
            'hasSpawnedFollowOnPhase': True
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)
        self.assertTrue(assessment_1['hasSpawnedFollowOnPhase'])

        # on update
        payload = {
            'name': 'Test 2 of Phase I',
            'recordTypeIds': [str(FBW_PHASE_I_ASSESSMENT_RECORD)]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)
        self.assertNotEqual(assessment_1['id'], assessment_2['id'])
        self.assertFalse(assessment_2['hasSpawnedFollowOnPhase'])

        payload = {
            'hasSpawnedFollowOnPhase': True
        }
        url = '{0}/{1}'.format(self.url, unquote(assessment_2['id']))
        req = self.put(url, payload)
        self.ok(req)
        updated_assessment_2 = self.json(req)
        self.assertTrue(updated_assessment_2['hasSpawnedFollowOnPhase'])

    def test_can_set_source_taken_id(self):
        taken_id = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        # on create
        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)
        self.assertEqual(assessment_1['sourceAssessmentTakenId'], taken_id)

        # on update
        payload = {
            'name': 'Test 2 of Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)
        self.assertNotEqual(assessment_1['id'], assessment_2['id'])
        self.assertEqual(assessment_2['sourceAssessmentTakenId'], '')

        payload = {
            'sourceAssessmentTakenId': taken_id
        }
        url = '{0}/{1}'.format(self.url, unquote(assessment_2['id']))
        req = self.put(url, payload)
        self.ok(req)
        updated_assessment_2 = self.json(req)
        self.assertEqual(updated_assessment_2['sourceAssessmentTakenId'], taken_id)

    def test_can_query_on_source_taken_ids(self):
        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_1
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        payload = {
            'name': 'Test Phase II, 2',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_2
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        url = '{0}?raw&sourceAssessmentTakenId=foo%3A123%40ODL.MIT.EDU'.format(self.url)
        req = self.get(url)
        self.ok(req)
        results = self.json(req)
        self.assertEqual(len(results), 0)

        url = '{0}?raw&sourceAssessmentTakenId={1}'.format(self.url,
                                                           taken_id_1)
        req = self.get(url)
        self.ok(req)
        results = self.json(req)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['id'], assessment_1['id'])

        url = '{0}?raw&sourceAssessmentTakenId={1}&sourceAssessmentTakenId={2}'.format(self.url,
                                                                                       taken_id_1,
                                                                                       taken_id_2)
        req = self.get(url)
        self.ok(req)
        results = self.json(req)
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0]['id'], assessment_2['id'])
        self.assertEqual(results[1]['id'], assessment_1['id'])

    def test_can_get_bulk_results(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_1,
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        a1_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_1['id']))
        req = self.post(a1_offered_url)
        self.ok(req)
        offered_1 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a1_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_1['id']))
        req = self.post(a1_taken_url)
        self.ok(req)
        taken_1 = self.json(req)
        a1_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_1['id']))
        req = self.get(a1_questions_url)
        self.ok(req)

        payload = {
            'name': 'Test Phase II, 2',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_2
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        a2_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_2['id']))
        req = self.post(a2_offered_url)
        self.ok(req)
        offered_2 = self.json(req)

        bulk_results_url = '{0}assessment/banks/{1}/bulkofferedresults'.format(self.endpoint,
                                                                               unquote(str(self._bank.ident)))
        payload = {
            'assessmentOfferedIds': [offered_1['id'], offered_2['id']],
            'raw': True
        }
        req = self.get(bulk_results_url, data=payload)
        self.ok(req)
        results = self.json(req)
        self.assertNotEqual(results, [])
        self.assertEqual(len(results), 1)  # because we only attempted the first taken
        self.assertEqual(results[0]['id'], taken_1['id'])

    def test_can_get_bulk_results_via_spawnedresults_endpoint(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        payload = {
            'name': 'Test Phase I',
            'recordTypeIds': [str(FBW_PHASE_I_ASSESSMENT_RECORD)],
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        a1_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_1['id']))
        req = self.post(a1_offered_url)
        self.ok(req)
        offered_1 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a1_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_1['id']))
        req = self.post(a1_taken_url)
        self.ok(req)
        taken_1 = self.json(req)
        a1_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_1['id']))
        req = self.get(a1_questions_url)
        self.ok(req)
        taken_id_1 = taken_1['id']

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_1,
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        a2_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_2['id']))
        req = self.post(a2_offered_url)
        self.ok(req)
        offered_2 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a2_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_2['id']))
        req = self.post(a2_taken_url)
        self.ok(req)
        taken_2 = self.json(req)
        a2_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_2['id']))
        req = self.get(a2_questions_url)
        self.ok(req)

        spawned_results_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/spawnedresults?raw'.format(self.endpoint,
                                                                                                         unquote(str(self._bank.ident)),
                                                                                                         unquote(offered_1['id']))
        req = self.get(spawned_results_url)
        self.ok(req)
        results = self.json(req)
        self.assertNotEqual(results, [])
        self.assertEqual(len(results), 1)  # because we only attempted the first taken
        self.assertEqual(results[0]['id'], taken_2['id'])

    def test_can_get_bulk_results_via_spawnedresults_endpoint_with_no_phase_1_taken(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        payload = {
            'name': 'Test Phase I',
            'recordTypeIds': [str(FBW_PHASE_I_ASSESSMENT_RECORD)],
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        a1_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_1['id']))
        req = self.post(a1_offered_url)
        self.ok(req)
        offered_1 = self.json(req)

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': assessment_1['id'],
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        a2_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_2['id']))
        req = self.post(a2_offered_url)
        self.ok(req)
        offered_2 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a2_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_2['id']))
        req = self.post(a2_taken_url)
        self.ok(req)
        taken_2 = self.json(req)
        a2_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_2['id']))
        req = self.get(a2_questions_url)
        self.ok(req)

        spawned_results_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/spawnedresults?raw'.format(self.endpoint,
                                                                                                         unquote(str(self._bank.ident)),
                                                                                                         unquote(offered_1['id']))
        req = self.get(spawned_results_url)
        self.ok(req)
        results = self.json(req)
        self.assertNotEqual(results, [])
        self.assertEqual(len(results), 1)  # because we only attempted the first taken
        self.assertEqual(results[0]['id'], taken_2['id'])

    def test_can_get_bulk_results_via_spawnedresults_endpoint_with_combo_of_students(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        payload = {
            'name': 'Test Phase I',
            'recordTypeIds': [str(FBW_PHASE_I_ASSESSMENT_RECORD)],
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        a1_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_1['id']))
        req = self.post(a1_offered_url)
        self.ok(req)
        offered_1 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a1_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_1['id']))
        req = self.post(a1_taken_url)
        self.ok(req)
        taken_1 = self.json(req)
        a1_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_1['id']))
        req = self.get(a1_questions_url)
        self.ok(req)
        taken_id_1 = taken_1['id']

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': assessment_1['id'],
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        a2_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_2['id']))
        req = self.post(a2_offered_url)
        self.ok(req)
        offered_2 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a2_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_2['id']))
        req = self.post(a2_taken_url)
        self.ok(req)
        taken_2 = self.json(req)
        a2_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_2['id']))
        req = self.get(a2_questions_url)
        self.ok(req)

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_1,
            'sections': [{
                'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                'itemBankId': str(self._bank.ident),
                'waypointQuota': 1,
                'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                'quota': 1
            }]
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_3 = self.json(req)

        a3_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_3['id']))
        req = self.post(a3_offered_url)
        self.ok(req)
        offered_3 = self.json(req)

        # now create a taken and get questions, so there are some results to look at
        a3_taken_url = '{0}offered/{1}/assessmentstaken'.format(self.url,
                                                                unquote(offered_3['id']))
        req = self.post(a3_taken_url)
        self.ok(req)
        taken_3 = self.json(req)
        a3_questions_url = '{0}taken/{1}/questions'.format(self.url,
                                                           unquote(taken_3['id']))
        req = self.get(a3_questions_url)
        self.ok(req)

        spawned_results_url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/spawnedresults?raw'.format(self.endpoint,
                                                                                                         unquote(str(self._bank.ident)),
                                                                                                         unquote(offered_1['id']))
        req = self.get(spawned_results_url)
        self.ok(req)
        results = self.json(req)
        self.assertNotEqual(results, [])
        self.assertEqual(len(results), 2)
        self.assertEqual(results[1]['id'], taken_2['id'])
        self.assertEqual(results[0]['id'], taken_3['id'])

    def test_can_get_bulk_assessments_offered(self):
        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'name': 'Test Phase II',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_1
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_1 = self.json(req)

        a1_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_1['id']))
        req = self.post(a1_offered_url)
        self.ok(req)
        offered_1 = self.json(req)

        payload = {
            'name': 'Test Phase II, 2',
            'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
            'sourceAssessmentTakenId': taken_id_2
        }
        req = self.post(self.url, payload)
        self.ok(req)
        assessment_2 = self.json(req)

        a2_offered_url = '{0}/{1}/assessmentsoffered'.format(self.url,
                                                             unquote(assessment_2['id']))
        req = self.post(a2_offered_url)
        self.ok(req)
        offered_2 = self.json(req)

        payload = {
            'raw': True,
            'assessmentIds': [assessment_1['id'], assessment_2['id']]
        }
        bulk_offereds_url = '{0}assessment/banks/{1}/bulkassessmentsoffered'.format(self.endpoint,
                                                                                    unquote(str(self._bank.ident)))

        req = self.get(bulk_offereds_url, data=payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 2)
        self.assertEqual(data[1]['id'], offered_1['id'])
        self.assertEqual(data[0]['id'], offered_2['id'])

    def test_can_bulk_create_assessments_with_offereds_and_sections_sync(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'assessments': [{
                'bankId': str(self._bank.ident),
                'name': 'Test Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_1,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2017,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2017,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            },{
                'bankId': str(self._bank2.ident),
                'name': 'Another Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_2,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_2.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2018,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2018,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            }]
        }

        url = '{0}assessment/bulkassessments'.format(self.endpoint)
        self.assertEqual(self._bank.get_assessments().available(), 1)
        self.assertEqual(self._bank2.get_assessments().available(), 0)
        req = self.post(url,
                        payload=payload)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(len(data), 2)

        self.assertEqual(data[0]['assignedBankIds'], [str(self._bank.ident)])
        self.assertEqual(data[1]['assignedBankIds'], [str(self._bank2.ident)])

        self.assertEqual(len(data[0]['childIds']), 1)
        self.assertEqual(len(data[1]['childIds']), 1)

        assessment_1_id = Id(data[0]['id'])
        assessment_2_id = Id(data[1]['id'])

        offereds_1 = self._bank.get_assessments_offered_for_assessment(assessment_1_id)
        offereds_2 = self._bank2.get_assessments_offered_for_assessment(assessment_2_id)

        self.assertEqual(offereds_1.available(), 1)
        self.assertEqual(offereds_2.available(), 1)

        self.assertEqual(offereds_1.next().start_time.year, 2017)
        self.assertEqual(offereds_2.next().start_time.year, 2018)

    def test_can_bulk_create_assessments_with_offereds_and_sections_async(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'async': True,
            'assessments': [{
                'bankId': str(self._bank.ident),
                'name': 'Test Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_1,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2017,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2017,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            },{
                'bankId': str(self._bank2.ident),
                'name': 'Another Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_2,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_2.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2018,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2018,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            }]
        }

        url = '{0}assessment/bulkassessments'.format(self.endpoint)

        self.assertEqual(self._bank.get_assessments().available(), 1)
        self.assertEqual(self._bank2.get_assessments().available(), 0)
        req = self.post(url,
                        payload=payload)
        self.ok(req)
        task_id = self.json(req)
        self.assertEqual(len(task_id), 36)  # a task ID

        time.sleep(3)

        self.assertEqual(self._bank.get_assessments().available(), 2)
        self.assertEqual(self._bank2.get_assessments().available(), 1)

        # How can we make sure the task is logged in the database backend?
        # http://stackoverflow.com/questions/38231042/djcelery-not-storing-task-results-in-django-sqlite-db
        # engine = create_engine("sqlite:///task_results.sqlite3")
        # connection = engine.connect()
        # result = connection.execute("select * from celery_taskmeta")
        #
        # self.assertEqual(len(result), 1)
        #
        # for row in result:
        #     self.assertEqual(row['task_id'], task_id)
        #
        assessment_1 = self._bank.get_assessments().next()
        assessment_1 = self._bank.get_assessments().next()

        assessment_2 = self._bank2.get_assessments().next()

        self.assertEqual(len(assessment_1.object_map['childIds']), 1)
        self.assertEqual(len(assessment_2.object_map['childIds']), 1)

        offereds_1 = self._bank.get_assessments_offered_for_assessment(assessment_1.ident)
        offereds_2 = self._bank2.get_assessments_offered_for_assessment(assessment_2.ident)

        self.assertEqual(offereds_1.available(), 1)
        self.assertEqual(offereds_2.available(), 1)

        self.assertEqual(offereds_1.next().start_time.year, 2017)
        self.assertEqual(offereds_2.next().start_time.year, 2018)

    def test_can_get_bulk_job_status_async(self):
        item_1 = self.create_mc_item(level=1)
        item_2 = self.create_mc_item(level=1)
        item_3 = self.create_mc_item(level=1)
        item_4 = self.create_mc_item(level=1)

        taken_id_1 = 'assessment.AssessmentTaken%3A123%40ODL.MIT.EDU'
        taken_id_2 = 'assessment.AssessmentTaken%3A987%40ODL.MIT.EDU'

        payload = {
            'async': True,
            'assessments': [{
                'bankId': str(self._bank.ident),
                'name': 'Test Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_1,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_1.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2017,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2017,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            },{
                'bankId': str(self._bank2.ident),
                'name': 'Another Phase II',
                'recordTypeIds': [str(FBW_PHASE_II_ASSESSMENT_RECORD)],
                'sourceAssessmentTakenId': taken_id_2,
                'sections': [{
                    'type': "assessment-part-genus-type%3Afbw-specify-lo%40ODL.MIT.EDU",
                    'itemBankId': str(self._bank.ident),
                    'waypointQuota': 1,
                    'learningObjectiveId': str(item_2.learning_objective_ids.next()),
                    'quota': 1
                }],
                'assessmentsOffered': [{
                    'startTime': {
                        'year': 2018,
                        'month': 1,
                        'day': 1,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    },
                    'deadline': {
                        'year': 2018,
                        'month': 12,
                        'day': 31,
                        'hour': 0,
                        'minute': 0,
                        'second': 0
                    }
                }]
            }]
        }

        url = '{0}assessment/bulkassessments'.format(self.endpoint)
        self.assertEqual(self._bank.get_assessments().available(), 1)
        self.assertEqual(self._bank2.get_assessments().available(), 0)
        req = self.post(url,
                        payload=payload)
        self.ok(req)
        task_id = self.json(req)
        self.assertEqual(len(task_id), 36)  # a task ID

        time.sleep(3)

        url = '{0}assessment/bulkassessments/status?jobId={1}'.format(self.endpoint,
                                                                      task_id)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        if settings.CELERY_ALWAYS_EAGER:
            # To just test the logic, we have to test with this as True
            self.assertEqual(data[task_id], 'PENDING')
        else:
            self.assertEqual(data[task_id], 'SUCCESS')

    def test_get_bulk_job_status_invalid_id_returns_pending(self):
        url = '{0}assessment/bulkassessments/status?jobId=foo'.format(self.endpoint)

        req = self.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(data['foo'], 'PENDING')
