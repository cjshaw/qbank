# to test the authentication, need to follow the example in the README of
# https://github.com/etoccalino/django-rest-framework-httpsignature

# July 3, 2014, make this depend NOT on IS&T Membership, but rather Ortho3D hardcoded format
#  * Learner -> student user
#  * Instructor -> instructor user

import envoy
import os
import json
import time
import requests
import base64

from django.conf import settings
from django.core.management import call_command
from django.core.cache import cache

from rest_framework.test import APIRequestFactory, APITestCase

from http_signature.requests_auth import HTTPSignatureAuth

from urllib import unquote, quote

from copy import deepcopy

from dlkit.runtime.primordium import Id, Type

from utilities.general import activate_managers, get_session_data
from utilities.testing import configure_test_bucket, calculate_signature,\
    APISignatureAuthentication, add_user_authz_to_settings, configure_proxyable_authz,\
    create_test_request

from assessments_users.models import APIUser


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))


def setUpModule():
    call_command(
        'loaddata',
        '{0}/fixtures/test_data.json'.format(ABS_PATH),
        verbosity=0
    )


def tearDownModule():
    call_command('flush', interactive=False, verbosity=0)


class APITest(APITestCase):

    def _get_test_bank(self):
        req = create_test_request(self.user)
        activate_managers(req)
        am = get_session_data(req, 'am')
        querier = am.get_bank_query()
        querier.match_genus_type(Type('assessment.Bank%3Atest-bank%40ODL.MIT.EDU'), True)
        bank = am.get_banks_by_query(querier).next()
        return am.get_bank(bank.ident)  # to make sure we get a services bank

    def already_taken(self, _req):
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            ('{"detail": "Illegal state: you cannot do that because system '
             'conditions have changed. For example, the assessment has already been taken, '
             'or you have exceeded the number of allowed attempts."}')
        )

    def answer_not_found(self, _req):
        """
        Answer was not found -- bad answer_id passed in
        """
        ERRORS = ['{"detail": "Object not found."}']
        self.assertEqual(_req.status_code, 500)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def bad_input(self, _req):
        """
        Input was a bad format...i.e. a string or non-JSON thing
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "Poorly formatted input data."}'
        )

    def bad_multi_choice_response(self, _req):
        """
        choiceIds needs to be a list format, even for one item
        :param _req:
        :return:
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": "choiceIds should be a list for multiple-choice questions."}'
        )

    def clean_memberships(self):
        """
        Reset so user has no memberships
        """
        groups = self.mem.user_groups(self._username, full=True)
        sorted_groups = sorted(groups, key=lambda k: k['groupId'])
        for group in sorted_groups:
            self.mem.remove_user_from_group(self._username,
                                            group['groupId'],
                                            group['role'])
        self.wait_5s()

    def code(self, _req, _code):
        self.assertEqual(
            int(_req.status_code),
            int(_code)
        )

    def convert_user_to_app_user(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as a learner
        """
        self._username = self._app_user
        self.reset_header_to_new_user()

    def convert_user_to_bank_instructor(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as an instructor
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_bank_learner(self, item_id):
        """
        for the bank with longName item_id, add user
        to the Membership as a learner
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def convert_user_to_instructor(self):
        """
        Convert user to instructor in department,
        i.e. DepartmentAdmin
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_learner(self):
        """
        Convert user to a basic learner in the department,
        i.e. a DepartmentOfficer
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def create_test_bank(self, _auth, _name, _desc, _form=False):
        """
        helper method to create a test assessment bank
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        sig = calculate_signature(_auth, self.headers, 'POST', test_endpoint)
        payload = {
            "name": _name,
            "description": _desc,
            "genusTypeId": "assessment.Bank%3Atest-bank%40ODL.MIT.EDU"
        }
        self.sign_client(sig)
        if _form:
            req = self.client.post(test_endpoint, payload, format='multipart')
        else:
            req = self.client.post(test_endpoint, payload, format='json')
        # req.content should have the new bank item, with its ID (which
        # we will need to clean it up)
        self.assertEqual(req.status_code, 200)
        self.verify_text(req, 'Bank', _name, _desc)

        bank_id = Id(json.loads(req.content)['id'])

        configure_proxyable_authz(self._app_user,
                                  catalog_id=bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=bank_id)

        add_user_authz_to_settings('student',
                                   self._learner,
                                   catalog_id=bank_id)

        return unquote(str(bank_id))

    def delete_item(self, _auth, _url):
        """
        helper method that sends a delete request to the url
        calculates the signature for you
        """
        delete_sig = calculate_signature(_auth, self.headers, 'DELETE', _url)
        self.sign_client(delete_sig)
        req = self.client.delete(_url)
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.content, '')

    def encode(self, _file):
        try:
            _file.seek(0)
            return base64.b64encode(_file.read())
        except AttributeError:
            return base64.b64encode(_file)

    def extract_answers(self, item):
        """
        Extract the answer part of an item
        """
        if 'answers' in item:
            return item['answers']
        else:
            return item['data']['results'][0]['answers']

    def extract_question(self, item):
        """
        Extract the question part of an item
        """
        if 'question' in item:
            return item['question']
        else:
            return item['data']['results'][0]['question']

    def filename(self, filepath):
        return filepath.split('/')[-1]

    def include_choices(self, _req):
        """
        Check that an "include choices" error message is included
        :param _req:
        :return:
        """
        errors = ['{"detail": ["At least two choice set attribute(s) required for Ortho-3D items."]}',
                  '{"detail": ["Large and small image files attribute(s) required for Ortho-3D items."]}']
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertIn(
            _req.content,
            errors
        )

    def is_cloudfront_url(self, _url):
        self.assertIn(
            self._cloudfront_root,
            _url
        )

        expected_params = ['?Expires=', '&Signature=', '&Key-Pair-Id=APKAJNOKQILWTYL37W3A']

        for param in expected_params:
            self.assertIn(
                param,
                _url
            )

    def load(self, _req):
        return json.loads(_req.content)

    def message(self, _req, _msg, equal=False):
        data = self.load(_req)
        if equal:
            if isinstance(data['detail'], list):
                received = data['detail'][0]
            else:
                received = data['detail']
            self.assertEqual(
                str(_msg),
                str(received)
            )
        else:
            self.assertNotEqual(
                str(_msg),
                str(data['detail'])
            )

    def missing_orthoview(self, _req):
        self.assertEqual(
            _req.status_code,
            500
        )
        self.assertEqual(
            _req.content,
            '{"detail": ["All three view set attribute(s) required for Ortho-3D items."]}'
        )

    def need_items(self, _req):
        """
        Need items in assessment before creating an offering
        """
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": ["Cannot create an assessment offering for an assessment with no items."]}'
        )

    def no_results(self, req):
        self.ok(req)
        data = self.load(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

    def not_allowed(self, _req, _type):
        """
        verify that the _type method is not allowed. Status code 405
        and the text matches not allowed text
        """
        self.assertEqual(_req.status_code, 405)
        self.assertEqual(
            _req.content,
            '{"detail": "Method \'' + _type + '\' not allowed."}'
        )

    def not_empty(self, _req):
        """
        Verify that the bank is not empty and cannot be deleted
        """
        errors = ['{"detail": ["catalog is not empty"]}',
                  '{"detail": ["This Item is being used in one or more Assessments. ' +
                  'Delink it first, before deleting it."]}',
                  '{"detail": "Illegal state: you cannot do that because system ' +
                  'conditions have changed. For example, the assessment ' +
                  'has already been taken, or you have exceeded the number ' +
                  'of allowed attempts."}']
        self.assertEqual(_req.status_code, 500)
        self.assertIn(
            _req.content,
            errors
        )

    def not_responded(self, _req):
        self.ok(_req)
        self.assertEqual(
            _req.content,
            '{"responded": false}'
        )

    def ok(self, _req):
        """
        checks for status code 200 and that the request was okay
        """
        self.assertEqual(_req.status_code, 200)

    def reset_files(self):
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        self.choice1sm.seek(0)
        self.choice1big.seek(0)

    def reset_header_to_new_user(self):
        """
        When self._username changes, need to update the X-Api-Proxy in the header
        """
        self.headers['X-Api-Proxy'] = self._username

    def responded(self, _req, _correct=False):
        expected_response = {
            "responded": True,
            "isCorrect": _correct
        }
        self.ok(_req)
        response = json.loads(_req.content)
        for key, val in expected_response.iteritems():
            self.assertEqual(
                response[key],
                val
            )

    def set_up_edx_item(self, _auth):
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'

        metadata = {
            'attempts': 4,
            'markdown': 'fake markdown',
            'rerandomize': 'fake rerandomize',
            'showanswer': 'fake showanswer',
            'weight': 1.23
        }

        irt = {
            'difficulty': -1.34,
            'discrimination': 2.41
        }

        learning_objectives = ['mc3-objective:21273@MIT-OEIT']  # also test that this gets quote-safed

        post_sig = calculate_signature(_auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "attempts": metadata['attempts'],
            "markdown": metadata['markdown'],
            "showanswer": metadata['showanswer'],
            "weight": metadata['weight'],
            "difficulty": irt['difficulty'],
            "discrimination": irt['discrimination'],
            "name": item_name,
            "description": item_desc,
            "question": {
                "rerandomize": metadata['rerandomize'],
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
            "learningObjectiveIds": learning_objectives
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            if key == 'rerandomize':
                self.assertEqual(
                    item['question'][key],
                    value
                )
            else:
                self.assertEqual(
                    item[key],
                    value
                )

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

        for index, _id in enumerate(learning_objectives):
            self.assertEqual(
                str(item['learningObjectiveIds'][index]),
                str(quote(_id))
            )

        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        return {
            'bank_endpoint': bank_endpoint,
            'irt': irt,
            'item_endpoint': item_details_endpoint,
            'item_id': item_id,
            'item_name': item_name,
            'learning_objectives': learning_objectives,
            'metadata': metadata
        }

    def setUp(self):
        from django.contrib.auth.models import AnonymousUser
        from datetime import datetime
        from time import mktime
        from wsgiref.handlers import format_date_time

        super(APITest, self).setUp()
        envoy.run('mongo test_qbank_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_resource --eval "db.dropDatabase()"')

        configure_test_bucket()
        self.anon = AnonymousUser()
        self._app_user = 'tester@mit.edu'
        self._instructor = 'instructor@mit.edu'
        self._learner = 'student@mit.edu'
        self._username = self._instructor
        self.user = APIUser.objects.get(username=self._app_user)
        self.public_key = str(self.user.public_key)
        self.private_key = str(self.user.private_key)
        self.signature_headers = ['request-line', 'accept', 'date', 'host', 'x-api-proxy']
        self.headers = {
            'Date': format_date_time(mktime(datetime.now().timetuple())),
            'Host': 'testserver:80',
            'Accept': 'application/json',
            'X-Api-Key': self.public_key,
            'X-Api-Proxy': self._username
        }
        self.auth = HTTPSignatureAuth(key_id=self.public_key,
                                      secret=self.private_key,
                                      algorithm='hmac-sha256',
                                      headers=self.signature_headers)
        self.endpoint = '/api/v2/'
        self.factory = APIRequestFactory()

        project_root = ABS_PATH + '/tests/'

        self._manip_file = project_root + 'myAssetBundle.unity3d'
        self._manip_filename = self.filename(self._manip_file)
        self.manip = open(self._manip_file, 'rb')

        self._front = project_root + 'front.jpg'
        self._front_filename = self.filename(self._front)
        self.front = open(self._front, 'rb')

        self._side = project_root + 'side.jpg'
        self._side_filename = self.filename(self._side)
        self.side = open(self._side, 'rb')

        self._top = project_root + 'top.jpg'
        self._top_filename = self.filename(self._top)
        self.top = open(self._top, 'rb')

        self._choice0sm = project_root + 'choice0sm.jpg'
        self._choice0sm_filename = self.filename(self._choice0sm)
        self.choice0sm = open(self._choice0sm, 'rb')

        self._choice0big = project_root + 'choice0big.jpg'
        self._choice0big_filename = self.filename(self._choice0big)
        self.choice0big = open(self._choice0big, 'rb')

        self._choice1sm = project_root + 'choice1sm.jpg'
        self._choice1sm_filename = self.filename(self._choice1sm)
        self.choice1sm = open(self._choice1sm, 'rb')

        self._choice1big = project_root + 'choice1big.jpg'
        self._choice1big_filename = self.filename(self._choice1big)
        self.choice1big = open(self._choice1big, 'rb')

        self._aws_root = 'https://{0}.s3.amazonaws.com/'.format(settings.S3_TEST_BUCKET)
        self._cloudfront_root = 'https://{0}/'.format(settings.CLOUDFRONT_TEST_DISTRO)

        # add_user_authz_to_settings('instructor',
        #                            self._app_user)
        # configure_proxyable_authz(self._app_user)
        # add_user_authz_to_settings('instructor',
        #                            self._instructor)
        # add_user_authz_to_settings('student',
        #                            self._learner)

    def sign_client(self, sig, opt_headers=None):
        self.client.credentials()
        if opt_headers:
            self.client.credentials(
                HTTP_ACCEPT=opt_headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=opt_headers['Date'],
                HTTP_HOST=opt_headers['Host'],
                HTTP_X_API_KEY=opt_headers['X-Api-Key'],
                HTTP_X_API_PROXY=opt_headers['X-Api-Proxy'],
                HTTP_LTI_BANK=opt_headers['LTI-Bank'],
                HTTP_LTI_USER_ID=opt_headers['LTI-User-ID'],
                HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID=opt_headers['LTI-Tool-Consumer-Instance-GUID'],
                HTTP_LTI_USER_ROLE=opt_headers['LTI-User-Role']
            )
        else:
            self.client.credentials(
                HTTP_ACCEPT=self.headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=self.headers['Date'],
                HTTP_HOST=self.headers['Host'],
                HTTP_X_API_KEY=self.headers['X-Api-Key'],
                HTTP_X_API_PROXY=self.headers['X-Api-Proxy']
            )

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        self.manip.close()
        self.front.close()
        self.side.close()
        self.top.close()
        self.choice0sm.close()
        self.choice0big.close()
        self.choice1sm.close()
        self.choice1big.close()

        envoy.run('mongo test_qbank_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_qbank_resource --eval "db.dropDatabase()"')

        super(APITest, self).tearDown()

    def unauthorized(self, _req):
        """
        confirms that status code for unauthorized access is 403, and text is the
        error message
        """
        ERRORS = ['{"detail": "Authentication credentials were not provided."}',
                  '{"detail": "Bad signature"}',
                  '{"detail": "Permission denied."}']
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            ERRORS
        )

    def user_not_found(self, _req):
        """
        User is not found--bad public key
        """
        self.assertEqual(_req.status_code, 403)
        self.assertIn(
            _req.content,
            '{"detail": "No user found."}'
        )

    def verify_answers(self, _data, _a_strs, _a_types):
        """
        verify just the answers part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
                if 'data' in data:
                    data = data['data']['results']
            except:
                data = json.loads(_data)
        except:
            data = _data

        if 'answers' in data:
            answers = data['answers']
        elif 'results' in data:
            answers = data['results']
        else:
            answers = data

        if isinstance(answers, list):
            ind = 0
            for _a in _a_strs:
                if _a_types[ind] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                    self.assertEqual(
                        answers[ind]['text']['text'],
                        _a
                    )
                self.assertIn(
                    _a_types[ind],
                    answers[ind]['recordTypeIds']
                )
                ind += 1
        elif isinstance(answers, dict):
            if _a_types[0] == 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU':
                self.assertEqual(
                    answers['text']['text'],
                    _a_strs[0]
                )
            self.assertIn(
                _a_types[0],
                answers['recordTypeIds']
            )
        else:
            self.fail('Bad answer format.')

    def verify_data_length(self, _req, _length):
        """
        Check that the data array is of the specified length
        """
        data = json.loads(_req.content)
        self.assertEqual(
            len(data['data']),
            _length
        )

    def verify_first_angle(self, _req, _angle):
        """
        Check if the req angle matches expected
        :param _req:
        :param _angle:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]
        self.assertEqual(
            item['question']['firstAngle'],
            _angle
        )

    def verify_links(self, _req, _type):
        """
        helper method to check that basic _links are included in
        the returned object. Includes a check for self.
        Depending on the _type, it will also check for the drilldown
        links...like for bank, should have assessments, items
        """
        search_type = _type.lower()
        self.assertIn(
            '"_links": ',
            _req.content
        )
        self.assertIn(
            '"self": ',
            _req.content
        )
        if search_type == 'bank':
            self.assertIn(
                '"assessments": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'assessmentdetails':
            self.assertIn(
                '"offerings": ',
                _req.content
            )
            self.assertIn(
                '"takens": ',
                _req.content
            )
            self.assertIn(
                '"items": ',
                _req.content
            )
        elif search_type == 'service':
            self.assertIn(
                '"banks": ',
                _req.content
            )
            self.assertIn(
                '"itemTypes": ',
                _req.content
            )
        elif search_type == 'itemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'assessmentitemdetails':
            self.assertIn(
                '"question": ',
                _req.content
            )
            self.assertIn(
                '"answers": ',
                _req.content
            )
        elif search_type == 'itemfiles':
            self.assertIn(
                '"front": ',
                _req.content
            )
            self.assertIn(
                '"top": ',
                _req.content
            )
            self.assertIn(
                '"manip": ',
                _req.content
            )
            self.assertIn(
                '"side": ',
                _req.content
            )
        elif search_type == 'takenquestions':
            self.assertIn(
                '"data": ',
                _req.content
            )
        elif search_type == 'takenquestion':
            self.assertIn(
                '"files": ',
                _req.content
            )
            self.assertIn(
                '"submit": ',
                _req.content
            )

    def verify_manip_file(self, _req, _manip_id):
        """
        make sure the manipulatable file is in the object correctly
        Probably some set of Ids:
            "manipId": "repository.Asset%3A53aac9afea061a05081dec55%40birdland.mit.edu",
            "orthoViewSet": {}
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            self.assertEqual(
                item['data'][0]['question']['manipId'],
                _manip_id
            )
        elif 'question' in item:
            self.assertEqual(
                item['question']['manipId'],
                _manip_id
            )
        else:
            self.assertEqual(
                item['manipId'],
                _manip_id
            )

    def verify_no_data(self, _req):
        """
        Verify that the data object in _req is empty
        """
        data = json.loads(_req.content)
        self.assertEqual(
            data['data'],
            {'count': 0,
             'previous': None,
             'results': [],
             'next': None}
        )
        self.verify_data_length(_req, 4)

    def verify_offerings(self, _req, _type, _data):
        """
        Check that the offerings match...
        _type may not be used here; assume AssessmentOffered always?
        _data objects need to have an offering ID in them
        """
        def check_expected_against_one_item(expected, item):
            for key, value in expected.iteritems():
                if isinstance(value, basestring):
                    if key == 'name':
                        self.assertEqual(
                            item['displayName']['text'],
                            value
                        )
                    elif key == 'description':
                        self.assertEqual(
                            item['description']['text'],
                            value
                        )
                    else:
                        self.assertEqual(
                            item[key],
                            value
                        )
                elif isinstance(value, dict):
                    for inner_key, inner_value in value.iteritems():
                        self.assertEqual(
                            item[key][inner_key],
                            inner_value
                        )

        data = json.loads(_req.content)
        if 'data' in data:
            data = data['data']['results']
        elif 'results' in data:
            data = data['results']

        if isinstance(_data, list):
            for expected in _data:
                if isinstance(data, list):
                    for item in data:
                        if item['id'] == expected['id']:
                            check_expected_against_one_item(expected, item)
                            break
                else:
                    check_expected_against_one_item(expected, data)
        elif isinstance(_data, dict):
            if isinstance(data, list):
                for item in data:
                    if item['id'] == _data['id']:
                        check_expected_against_one_item(_data, item)
                        break
            else:
                check_expected_against_one_item(_data, data)

    def verify_ortho_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data']['results'][0]
        if 'answers' in item:
            item = item['answers'][0]
        self.assertEqual(
            item['integerValues']['frontFaceValue'],
            _data['frontFaceValue']
        )
        self.assertEqual(
            item['integerValues']['sideFaceValue'],
            _data['sideFaceValue']
        )
        self.assertEqual(
            item['integerValues']['topFaceValue'],
            _data['topFaceValue']
        )

    def verify_ortho_choices(self, _req, _expected):
        """
        make sure the files and IDs match. Assume two choices for now.
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            item = item['data']['results'][0]['questions'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for index, choice in enumerate(item['files']['choices']):
            self.assertEqual(
                choice['id'],
                _expected[index]['id']
            )
            if index == 0:
                expected_file_sm = self.choice0sm
                expected_file_big = self.choice0big
            else:
                expected_file_sm = self.choice1sm
                expected_file_big = self.choice1big

            self.is_cloudfront_url(choice['smallOrthoViewSet'])
            self.is_cloudfront_url(choice['largeOrthoViewSet'])

            expected_file_sm.seek(0)
            expected_file_big.seek(0)

            self.assertEqual(
                requests.get(choice['smallOrthoViewSet']).content,
                expected_file_sm.read()
            )

            self.assertEqual(
                requests.get(choice['largeOrthoViewSet']).content,
                expected_file_big.read()
            )

    def verify_ortho_files(self, _req, _expected):
        """
        make sure the files match
        Because these are signed URLs for AWS Cloudfront, the signature & expire times
        will not match -- verify this.
        NOTE -- some tests show that the expire times DO match. Is this because
        no time elapses between the two requests???
        :param _req:
        :param _expected:
        :return:
        """
        try:
            try:
                item = json.loads(_req.content)
            except:
                item = json.loads(_req)
        except:
            item = _req
        if 'data' in item:
            if 'questions' in item['data']['results'][0]:
                item = item['data']['results'][0]['questions'][0]
            else:
                item = item['data']['results'][0]
        if 'question' in item:
            item = self.extract_question(item)
        for file_label, file_data in item['files'].iteritems():
            if file_label in _expected:
                self.is_cloudfront_url(file_data)
                if 'http' in _expected[file_label]:
                    self.is_cloudfront_url(_expected[file_label])

                    # expire times should not match
                    # self.assertNotEqual(
                    #     file_data,
                    #     _expected[file_label]
                    # )

                    self.assertEqual(
                        file_data.split('?Expires')[0],
                        _expected[file_label].split('?Expires')[0]
                    )
                else:
                    # expected is the actual b64 encoded file
                    raw_file = requests.get(file_data).content
                    self.assertEqual(
                        base64.b64encode(raw_file),
                        _expected[file_label]
                    )

    def verify_ortho_multi_choice_answers(self, _req, _data):
        """
        First extract the expected answers from _data
        Then compare to the object in the req
        """
        item = json.loads(_req.content)
        if 'data' in item:
            item = item['data'][0]
        if 'answers' in item:
            item = item['answers'][0]

        if not isinstance(_data, list):
            raise Exception('_data should be a list')

        for ans in item['choiceIds']:
            self.assertIn(
                ans,
                _data
            )

    def verify_question(self, _data, _q_str, _q_type):
        """
        verify just the question part of an item. Should allow you to pass
        in either a request response or a json object...load if needed
        """
        try:
            try:
                data = json.loads(_data.content)
            except:
                data = json.loads(_data)
        except:
            data = _data
        if 'question' in data:
            question = data['question']
        elif 'data' in data:
            try:
                if 'questions' in data['data']['results'][0]:
                    question = data['data']['results'][0]['questions'][0]['question']
                else:
                    question = data['data']['results'][0]['question']
            except:
                question = data['data']['results'][0]['questions'][0]
        else:
            question = data

        self.assertEqual(
            question['text']['text'],
            _q_str
        )
        self.assertIn(
            _q_type,
            question['recordTypeIds']
        )

    def verify_questions_answers(self, _req, _q_str, _q_type, _a_str, _a_type, _id=None):
        """
        helper method to verify the questions & answers in a returned item
        takes care of all the language stuff
        Assumes multiple answers, to _a_str and _a_type must be lists
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                        item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req

        self.verify_question(data, _q_str, _q_type)

        self.verify_answers(data, _a_str, _a_type)

    def verify_question_status(self, question, responded, correct=None):
        self.assertEqual(
            question['responded'],
            responded
        )
        if correct:
            self.assertEqual(
                question['isCorrect'],
                correct  # True
            )
        else:
            try:
                self.assertNotIn(
                    'correct',
                    question
                )
            except:
                self.assertEqual(
                    question['isCorrect'],
                    correct  # False
                )

    def verify_submission(self, _req, _expected_result, _has_next=None):
        data = json.loads(_req.content)
        self.assertEqual(
            data['isCorrect'],
            _expected_result
        )
        if _has_next:
            self.assertEqual(
                data['hasNext'],
                _has_next
            )

    def verify_taking_files(self, _req):
        """
        Check that the taken files match the uploaded ones
        :param _req:
        :return:
        """
        data = json.loads(_req.content)
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)

        for file_label, file_url in data.iteritems():
            self.is_cloudfront_url(file_url)

        self.assertEqual(
            requests.get(data['manip']).content,
            self.manip.read()
        )
        self.assertEqual(
            requests.get(data['front']).content,
            self.front.read()
        )
        self.assertEqual(
            requests.get(data['side']).content,
            self.side.read()
        )
        self.assertEqual(
            requests.get(data['top']).content,
            self.top.read()
        )

    def verify_text(self, _req, _type, _name, _desc, _id=None, _genus=None):
        """
        helper method to verify the text in a returned object
        takes care of all the language stuff
        """
        req = json.loads(_req.content)
        if _id:
            data = None
            for item in req['data']['results']:
                if (item['id'] == _id or
                        item['id'] == quote(_id)):
                    data = item
            if not data:
                raise LookupError('Item with id: ' + _id + ' not found.')
        else:
            data = req
        self.assertEqual(
            data['displayName']['text'],
            _name
        )
        self.assertEqual(
            data['description']['text'],
            _desc
        )
        self.assertEqual(
            data['type'],
            _type
        )

        if _genus:
            self.assertEqual(
                data['genusTypeId'],
                _genus
            )

    def verify_types(self, _req):
        """
        Make sure all the types in mongo/assessment/records are returned
        """
        from dlkit.records.registry import QUESTION_RECORD_TYPES
        types_list = json.loads(_req.content)
        # self.assertEqual(len(types_list), len(ITEM_RECORD_TYPES))
        # right now ITEM_RECORD_TYPES is a dict with only one type...
        # TODO: fix this...once dlkit is fixed
        # canonical = QUESTION_RECORD_TYPES
        canonical = [
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Match Ortho Faces"
                },
                "description": {
                    "text": "Match the manipulatable object to the given faces."
                },
                "id": "question%3Amatch-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Define Ortho Faces"
                },
                "description": {
                    "text": "Define the best plane, top, and side faces of the manipulatable object."
                },
                "id": "question%3Adefine-ortho-faces%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Ortho View Set"
                },
                "description": {
                    "text": "From the choices provided, select the ortho view set that matches the given manipulatable."
                },
                "id": "question%3Achoose-viewset%40ODL.MIT.EDU"
            },
            {
                "recordType": "question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU",
                "displayName": {
                    "text": "Select Matching Manipulatable"
                },
                "description": {
                    "text": "From the choices provided, select the manipulatable that matches the given ortho view set."
                },
                "id": "question%3Achoose-manip%40ODL.MIT.EDU"
            },
            {
                'displayName': {
                    'text': 'edX Multiple Choice'
                },
                'description': {
                    'text': 'Multiple Choice question for edX.'
                },
                'id': 'question%3Amulti-choice-edx%40ODL.MIT.EDU',
                'recordType': 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
            }
        ]
        canonical_display_names = []
        canonical_descriptions = []
        canonical_ids = []
        canonical_record_types = []
        for item in canonical:
            canonical_display_names.append(item['displayName']['text'])
            canonical_descriptions.append(item['description']['text'])
            canonical_ids.append(item['id'])
            canonical_record_types.append(item['recordType'])

        ind = 0
        for item in types_list:
            self.assertEqual(
                item['displayName']['text'],
                canonical_display_names[ind]
            )
            self.assertEqual(
                item['description']['text'],
                canonical_descriptions[ind]
            )
            self.assertEqual(
                item['id'],
                canonical_ids[ind]
            )
            self.assertEqual(
                item['recordType'],
                canonical_record_types[ind]
            )
            ind += 1

    def wait_5s(self, default=5):
        """
        Trying to wait 5 seconds to let Membership catch up with group changes...
        """
        time.sleep(default)

    def wait_for_membership_to_appear(self):
        """
        Because it seems like the Membership service has a delay
        in creating new Departments -> membership showing up?
        Need to verify that user has membership.
        Seems to affect the instructor -> POST new bank
        """
        found_one = False
        while not found_one:
            self.wait_5s(15)
            if len(self.mem.user_groups(self._username, full=True)) > 0:
                found_one = True

    def wrong_taken_post_url(self, _req):
        """
        Can only create a taken from the /offering/ endpoint
        Trying from the /assessments/ endpoint should throw this error
        """
        msg = 'Can only create AssessmentTaken from an AssessmentOffered root URL.'
        self.assertEqual(_req.status_code, 500)
        self.assertEqual(
            _req.content,
            '{"detail": ["' + msg + '"]}'
        )


class UnauthenticatedUserTests(APITest):
    def setUp(self):
        super(UnauthenticatedUserTests, self).setUp()

    def tearDown(self):
        super(UnauthenticatedUserTests, self).tearDown()

    def test_unauthenticated_user_request_returns_unauthorized(self):
        """
        An unauthenticated request should respond with error
        """
        req = self.client.get(self.endpoint + 'assessment/')
        self.unauthorized(req)

    # deprecated -- when using memcached to store sessions, this doesn't
    # work -- the session_key / DLKit service managers get saved
    # with an authenticated user, because the sessionId is
    # constant across all test clients?
    # def test_unauthenticated_api_request_returns_unauthorized(self):
    #     """
    #     User should get a bad signature JSON if their signature doesn't match
    #     """
    #     fake_private = 'afakeprivateKey!23'
    #     auth = HTTPSignatureAuth(key_id=self.public_key,
    #                              secret=fake_private,
    #                              algorithm='hmac-sha256',
    #                              headers=self.signature_headers)
    #     test_endpoint = self.endpoint + 'assessment/'
    #     sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
    #     self.sign_client(sig)
    #     req = self.client.get(test_endpoint)
    #     self.unauthorized(req)
    #
    # deprecated -- when using memcached to store sessions, this doesn't
    # work -- the session_key / DLKit service managers get saved
    # with an authenticated user, because the sessionId is
    # constant across all test clients?
    # def test_incomplete_request_signature(self):
    #     """
    #     Make sure that all of the following headers are required:
    #     ['request-line','accept','date','host','x-api-proxy']
    #     """
    #     partial_headers = ['request-line','accept','date','host']
    #     auth = HTTPSignatureAuth(key_id=self.public_key,
    #                              secret=self.private_key,
    #                              algorithm='hmac-sha256',
    #                              headers=partial_headers)
    #     test_endpoint = self.endpoint + 'assessment/'
    #     sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
    #     self.sign_client(sig)
    #     req = self.client.get(test_endpoint)
    #     self.unauthorized(req)


class AssessmentServiceTests(APITest):
    def setUp(self):
        super(AssessmentServiceTests, self).setUp()
        self.test_endpoint = self.endpoint + 'assessment/'

    def tearDown(self):
        super(AssessmentServiceTests, self).tearDown()

    def test_cannot_delete_assessment_endpoint(self):
        """
        DELETE should do nothing here. Error code 405.
        """
        sig = calculate_signature(self.auth, self.headers, 'DELETE', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.delete(self.test_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_instructor_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        """
        sig = calculate_signature(self.auth, self.headers, 'GET', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.get(self.test_endpoint)
        self.ok(req)
        self.verify_links(req, 'Service')

    def test_authenticated_learner_assessment_get(self):
        """
        Should return a list of services at this level...i.e. links to banks/
        For someone who is just a department officer, this should also return
        the results (officers can lookup)
        """
        self.convert_user_to_learner()
        sig = calculate_signature(self.auth, self.headers, 'GET', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.get(self.test_endpoint)
        self.ok(req)
        self.verify_links(req, 'Service')

    def test_cannot_post_assessment_endpoint(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        sig = calculate_signature(self.auth, self.headers, 'POST', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.post(self.test_endpoint)
        self.not_allowed(req, 'POST')

    def test_cannot_put_assessment_endpoint(self):
        """
        Not implemented, should return an error code 405, not supported
        """
        sig = calculate_signature(self.auth, self.headers, 'PUT', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.put(self.test_endpoint)
        self.not_allowed(req, 'PUT')

    def test_authenticated_item_types_get(self):
        """
        Should return a list of item types supported by the service
        """
        test_endpoint = self.endpoint + 'assessment/types/items/'
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        sig = calculate_signature(auth, self.headers, 'GET', test_endpoint)
        self.sign_client(sig)
        req = self.client.get(test_endpoint)
        self.ok(req)
        self.verify_types(req)

    # deprecated -- when using memcached to store sessions, this doesn't
    # work -- the session_key / DLKit service managers get saved
    # with an authenticated user, because the sessionId is
    # constant across all test clients?
    # def test_bad_public_key_returns_unknown_user(self):
    #     """
    #     User should get a bad signature JSON if their public key is not found
    #     """
    #     fake_public = 'afakepublicKey!23'
    #     test_endpoint = self.endpoint + 'assessment/banks/'
    #     auth = HTTPSignatureAuth(key_id=fake_public,
    #                              secret=self.private_key,
    #                              algorithm='hmac-sha256',
    #                              headers=self.signature_headers)
    #     sup_headers = self.headers
    #     sup_headers['X-Api-Key'] = fake_public
    #     sig = calculate_signature(auth, sup_headers, 'GET', test_endpoint)
    #     self.sign_client(sig)
    #     req = self.client.get(test_endpoint)
    #     self.user_not_found(req)

    def test_lti_request_signature(self):
        """
        Make sure that lti headers are accepted:
        ['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role']
        """
        lti_headers = ['request-line', 'accept', 'date', 'host', 'x-api-proxy', 'lti-user-id',
                       'lti-tool-consumer-instance-guid', 'lti-user-role', 'lti-bank']
        test_endpoint = self.endpoint + 'assessment/'

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = '80'
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)
        self.ok(req)


class AssessmentBankTests(APITest):
    def setUp(self):
        super(AssessmentBankTests, self).setUp()
        self.test_endpoint = self.endpoint + 'assessment/banks/'

        self.bank = self._get_test_bank()
        bank_id = unquote(str(self.bank.ident))
        self.bank_endpoint = '{0}assessment/banks/{1}/'.format(self.endpoint,
                                                               bank_id)

    def tearDown(self):
        super(AssessmentBankTests, self).tearDown()

    def test_authenticated_instructor_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        sig = calculate_signature(self.auth, self.headers, 'GET', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.get(self.test_endpoint)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.

    def test_authenticated_learner_bank_get(self):
        """
        Should give you a list of banks and links.
        """
        self.convert_user_to_learner()
        sig = calculate_signature(self.auth, self.headers, 'GET', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.get(self.test_endpoint)
        self.ok(req)
        self.verify_links(req, 'BankList')
        self.assertIn(
            '"displayName": ',
            req.content
        )  # this only really tests that at least one bank exists...not great.

    def test_student_cannot_edit_bank(self):
        self.convert_user_to_learner()
        new_name = 'a new bank name'
        new_desc = 'a new bank description'

        payload = {
            "name": new_name,
            "description": new_desc
        }
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.bank_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_cannot_post_to_bank_details_endpoint(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.bank_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.bank_endpoint)
        self.not_allowed(req, 'POST')

    def test_authenticated_instructor_can_edit_bank(self):
        """
        User can create a new assessment bank. Need to do self-cleanup,
        because the Mongo backend is not part of the test database...that
        means Django will not wipe it clean after every test!
        Once a bank is created, user can GET it, PUT to update it, and
        DELETE it. POST should return an error code 405.
        Do these bank detail tests here, because we have a known
        bank ID
        """
        new_name = 'a new bank name'
        new_desc = 'a new bank description'

        # DepartmentAdmin should be able to edit the bank
        payload = {
            "name": new_name,
            "description": new_desc
        }

        self.convert_user_to_instructor()
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.bank_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req, 'Bank', new_name, new_desc)

    def test_authenticated_learner_can_get_bank(self):
        """
        Learners can do nothing with POST and CRuD
        """
        self.convert_user_to_learner()

        # verify that the bank now appears in the bank_details call
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.bank_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.bank_endpoint)
        self.ok(req)
        self.verify_text(req, 'Bank', self.bank.display_name.text, self.bank.description.text)
        self.verify_links(req, 'Bank')

    def test_learner_cannot_delete_bank(self):
        self.convert_user_to_learner()
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', self.bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.bank_endpoint)
        self.unauthorized(req)

    def test_cannot_put_to_banks_endpoint(self):
        """
        PUT should do nothing on this page. Should get an error code 405
        """
        sig = calculate_signature(self.auth, self.headers, 'PUT', self.test_endpoint)
        self.sign_client(sig)
        req = self.client.put(self.test_endpoint)
        self.not_allowed(req, 'PUT')


class AssessmentsTests(APITest):
    def setUp(self):
        super(AssessmentsTests, self).setUp()
        self.bank = self._get_test_bank()
        self.bank_id = unquote(str(self.bank.ident))
        self.assessment_endpoint = '{0}assessment/banks/{1}/assessments/'.format(self.endpoint,
                                                                                 self.bank_id)

        self.assessment_name = 'a really hard assessment'
        self.assessment_desc = 'meant to differentiate students'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": self.assessment_name,
            "description": self.assessment_desc
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_endpoint, payload, format='json')
        data = self.load(req)

        self.assessment_detail_endpoint = '{0}{1}/'.format(self.assessment_endpoint,
                                                           unquote(data['id']))

    def tearDown(self):
        super(AssessmentsTests, self).tearDown()

    def test_cannot_delete_assessments_endpoint(self):
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', self.assessment_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_put_assessments_endpoint(self):
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.assessment_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.assessment_endpoint)
        self.not_allowed(req, 'PUT')

    def test_learner_gets_no_assessments(self):
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_endpoint)
        self.no_results(req)

    def test_learner_cannot_create_assessment(self):
        self.convert_user_to_bank_learner(self.bank_id)
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        req = self.client.post(self.assessment_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_authenticated_instructor_assessments_crud(self):
        """
        Create a test bank and test all things associated with assessments
        and a single assessment
        DELETE on root assessments/ does nothing. Error code 405.
        GET on root assessments/ gets you a list
        POST on root assessments/ creates a new assessment
        PUT on root assessments/ does nothing. Error code 405.

        For a single assessment detail:
        DELETE will delete that assessment
        GET brings up the assessment details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         assessment_name,
                         assessment_desc,
                         assessment_id)
        self.verify_links(req, 'Assessment')

    def test_cannot_post_assessment_details_endpoint(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_detail_endpoint)
        self.not_allowed(req, 'POST')

    def test_instructor_can_get_assessment_details(self):
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         self.assessment_name,
                         self.assessment_desc)
        self.verify_links(req, 'AssessmentDetails')

    def test_learner_cannot_put_assessment_details(self):
        self.convert_user_to_bank_learner(self.bank_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.assessment_detail_endpoint)
        self.sign_client(put_sig)
        new_assessment_name = 'a new assessment name'
        new_assessment_desc = 'to trick students'
        payload = {
            "name": new_assessment_name,
            "description": new_assessment_desc
        }
        req = self.client.put(self.assessment_detail_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_instructor_can_put_assessment_details(self):
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.assessment_detail_endpoint)
        self.sign_client(put_sig)
        new_assessment_name = 'a new assessment name'
        new_assessment_desc = 'to trick students'
        payload = {
            "name": new_assessment_name,
            "description": new_assessment_desc
        }
        req = self.client.put(self.assessment_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_detail_endpoint)
        self.sign_client(get_sig)

        self.sign_client(get_sig)
        req = self.client.get(self.assessment_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Assessment',
                         new_assessment_name,
                         new_assessment_desc)
        self.verify_links(req, 'AssessmentDetails')


class ItemsTests(APITest):
    def setUp(self):
        super(ItemsTests, self).setUp()
        self.bank = self._get_test_bank()
        self.bank_id = unquote(str(self.bank.ident))
        self.items_endpoint = '{0}assessment/banks/{1}/items/'.format(self.endpoint,
                                                                      self.bank_id)

        self.item_name = 'a really complicated item'
        self.item_desc = 'meant to differentiate students'
        payload = {
            "name": self.item_name,
            "description": self.item_desc
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.items_endpoint, payload, format='json')
        data = self.load(req)
        self.item_detail_endpoint = '{0}{1}/'.format(self.items_endpoint,
                                                     unquote(data['id']))

    def tearDown(self):
        super(ItemsTests, self).tearDown()

    def test_cannot_delete_items_endpoint(self):
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', self.items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.items_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_put_items_endpoint(self):
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.items_endpoint)
        self.not_allowed(req, 'PUT')

    def test_learner_gets_no_results_for_items_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.no_results(req)

    def test_learner_cannot_create_item(self):
        self.convert_user_to_bank_learner(self.bank_id)
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_authenticated_instructor_items_crud(self):
        """
        Create a test bank and test all things associated with items
        and a single item
        DELETE on root items/ does nothing. Error code 405.
        GET on root items/ gets you a list
        POST on root items/ creates a new item
        PUT on root items/ does nothing. Error code 405.

        For a single item detail:
        DELETE will delete that item
        GET brings up the item details with offerings and taken
        POST does nothing. Error code 405.
        PUT lets user update the name or description
        """
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')

    def test_cannot_post_item_details(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.item_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.item_detail_endpoint)
        self.not_allowed(req, 'POST')

    def test_instructors_can_get_item_details(self):
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.item_detail_endpoint)

        self.ok(req)
        self.verify_text(req,
                         'Item',
                         self.item_name,
                         self.item_desc)
        self.verify_links(req, 'ItemDetails')

    def test_learner_cannot_get_item(self):
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.item_detail_endpoint)
        self.code(req, 500)

    def test_learer_cannot_edit_item(self):
        self.convert_user_to_bank_learner(self.bank_id)

        # PUT should not work for a Learner, throw unauthorized
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.item_detail_endpoint)
        self.sign_client(put_sig)
        new_item_name = 'a new item name'
        new_item_desc = 'to trick students'
        payload = {
            "name": new_item_name,
            "description": new_item_desc
        }
        req = self.client.put(self.item_detail_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_instructor_can_edit_item(self):
        new_item_name = 'a new item name'
        new_item_desc = 'to trick students'
        payload = {
            "name": new_item_name,
            "description": new_item_desc
        }

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         new_item_name,
                         new_item_desc)
        self.verify_links(req, 'ItemDetails')

    def test_deleting_bank_with_items_throws_exception(self):
        bank_endpoint = self.endpoint + 'assessment/banks/' + self.bank_id + '/'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', bank_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(bank_endpoint)
        self.not_empty(req)

    def test_authenticated_instructor_question_string_item_crud(self):
        """
        Test ability for user to POST a new question string and
        response string item
        """
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.ok(req)
        data = self.load(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type],
                                      item_id)
        self.verify_links(req, 'Item')

        # PUT should work for Instructor.
        # Can modify the question and answers, reflected in GET
        new_question_string = 'what day is it?'
        item_detail_endpoint = '{0}{1}/'.format(self.items_endpoint,
                                                item_id)

        payload = {
            'question': {
                'id': question_id,
                'questionString': new_question_string,
                'type': question_type
            }
        }

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_questions_answers(req,
                                      new_question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

    def test_instructor_can_edit_answer_using_item_detail_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        answer_id = self.extract_answers(item)[0]['id']

        item_detail_endpoint = '{0}{1}/'.format(self.items_endpoint,
                                                item_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        new_answer_string = 'Saturday'
        payload = {
            'answers': [{
                'id': answer_id,
                'responseString': new_answer_string,
                'type': answer_type
            }]
        }
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [new_answer_string],
                                      [answer_type])
        self.verify_links(req, 'ItemDetails')

    def test_cannot_delete_item_question_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           item_id)

        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', item_question_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_question_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_post_item_question_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           item_id)
        # POST to this root url also returns a 405
        post_sig = calculate_signature(self.auth, self.headers, 'POST', item_question_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_question_endpoint)
        self.not_allowed(req, 'POST')

    def test_can_get_item_question_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req, question_string, question_type)

    def test_can_put_item_question_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = unquote(item['question']['id'])

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           item_id)
        newer_question_string = 'yet another new question?'
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        payload = {
            "id": question_id,
            "questionString": newer_question_string,
            "type": question_type
        }
        req = self.client.put(item_question_endpoint, payload, format='json')
        self.ok(req)
        self.verify_question(req, newer_question_string, question_type)

    def test_cannot_delete_item_answers_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)

        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', item_answers_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_answers_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_put_item_answers_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = unquote(item['question']['id'])

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)
        # PUT to this root url also returns a 405
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_answers_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_answers_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_get_item_answers_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answers_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req, [answer_string], [answer_type])

    def test_can_add_answer_using_item_answers_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)
        second_answer_string = "a second answer"
        payload = [{
            "responseString": second_answer_string,
            "type": answer_type
        }]
        post_sig = calculate_signature(self.auth, self.headers, 'POST', item_answers_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answers_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req,
                            [answer_string, second_answer_string],
                            [answer_type, answer_type])
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answers_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [answer_string, second_answer_string],
                            [answer_type, answer_type])

    def test_exception_thrown_if_bad_answer_id_provided_in_item_answers_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)
        fake_item_answer_detail_endpont = item_answers_endpoint + 'fakeid/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', fake_item_answer_detail_endpont)
        self.sign_client(get_sig)
        req = self.client.get(fake_item_answer_detail_endpont)
        self.answer_not_found(req)

    def test_cannot_post_answer_detail(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])

        item_answer_detail_endpoint = '{0}{1}/answers/{2}/'.format(self.items_endpoint,
                                                                   item_id,
                                                                   answer_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', item_answer_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answer_detail_endpoint)
        self.not_allowed(req, 'POST')

    def test_can_get_answer_detail(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])

        item_answer_detail_endpoint = '{0}{1}/answers/{2}/'.format(self.items_endpoint,
                                                                   item_id,
                                                                   answer_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answer_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req, [answer_string], [answer_type])

    def test_can_put_answer_detail_endpoint(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])

        item_answer_detail_endpoint = '{0}{1}/answers/{2}/'.format(self.items_endpoint,
                                                                   item_id,
                                                                   answer_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_answer_detail_endpoint)
        self.sign_client(put_sig)
        newer_answer_string = 'yes, another one'
        payload = {
            "responseString": newer_answer_string,
            "type": answer_type
        }
        req = self.client.put(item_answer_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answer_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req, [newer_answer_string], [answer_type])

    def test_can_delete_answer_detail(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        answer_string = 'dessert'
        answer_type = 'answer-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "responseString": answer_string
            }]
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])

        item_answer_detail_endpoint = '{0}{1}/answers/{2}/'.format(self.items_endpoint,
                                                                   item_id,
                                                                   answer_id)
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', item_answer_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_answer_detail_endpoint)
        self.ok(req)

        item_answers_endpoint = '{0}{1}/answers/'.format(self.items_endpoint,
                                                         item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answers_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answers_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [],
                            [answer_type])
        self.assertEqual(
            self.load(req)['data']['count'],
            0
        )

    def test_can_create_item_with_files(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            }
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        req = self.client.post(self.items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])
        item_detail_endpoint = self.items_endpoint + item_id + '/'
        question_id = self.extract_question(item)['id']
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_question(req,
                             question_string,
                             question_type)


class AssessmentItemsTests(APITest):
    def setUp(self):
        super(AssessmentItemsTests, self).setUp()
        self.bank = self._get_test_bank()
        self.bank_id = unquote(str(self.bank.ident))
        bank_endpoint = self.endpoint + 'assessment/banks/' + self.bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        self.items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'

        # Use POST to create an item
        self.item_name = 'a really complicated item'
        self.item_desc = 'meant to differentiate students'
        self.question_string = 'what is pi?'
        self.question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": self.item_name,
            "description": self.item_desc,
            "question": {
                "type": self.question_type,
                "questionString": self.question_string
            }
        }
        req = self.client.post(self.items_endpoint, data=payload, format='json')
        self.ok(req)
        self.item = self.load(req)
        self.item_id = unquote(self.item['id'])

        # Now start working on the assessment/items endpoint, to test
        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        self.assessment_items_endpoint = assessment_detail_endpoint + 'items/'

    def tearDown(self):
        super(AssessmentItemsTests, self).tearDown()

    def test_cannot_delete_assessment_items_endpoint(self):
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', self.assessment_items_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_items_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_can_put_to_assessment_items_endpoint(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        assessments_endpoint = bank_endpoint + 'assessments/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'what is pi?'
        question_type = 'question-record-type%3Ashort-text-answer%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            }
        }
        req = self.client.post(items_endpoint, data=payload, format='json')
        self.ok(req)
        item1 = self.load(req)

        item_name = 'a really complicated item 2'
        item_desc = 'meant to differentiate students'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            }
        }
        req = self.client.post(items_endpoint, data=payload, format='json')
        self.ok(req)
        item2 = self.load(req)

        # Now start working on the assessment/items endpoint, to test
        # GET, POST for the items/ endpoint (others should throw error)
        # GET, DELETE for the items/<id> endpoint (others should throw error)
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # PUT should replace the entire list
        payload = {
            'itemIds': [item1['id'], item2['id']]
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_items_endpoint, payload, format='json')
        self.code(req, 202)
        data = self.load(req)
        self.assertEqual(data['data']['count'], 2)
        self.assertEqual(data['data']['results'][0]['id'], item1['id'])
        self.assertEqual(data['data']['results'][1]['id'], item2['id'])

    def test_authenticated_instructor_can_link_items_to_assessment(self):
        """
        Test link, view, delete of items to assessments
        """
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_items_endpoint)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         self.item_name,
                         self.item_desc,
                         quote(self.item_id))
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_links(req, 'AssessmentItems')

        # should now appear in the Assessment Items List
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         self.item_name,
                         self.item_desc,
                         self.item_id)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_links(req, 'AssessmentItems')

    def test_cannot_post_assessment_item_details(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')

        assessment_item_details_endpoint = '{0}{1}/'.format(self.assessment_items_endpoint,
                                                            self.item_id)

        post_sig = calculate_signature(self.auth,
                                       self.headers,
                                       'POST',
                                       assessment_item_details_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_item_details_endpoint)
        self.not_allowed(req, 'POST')

    def test_cannot_delete_item_in_assessment(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')

        item_detail_endpoint = '{0}{1}/'.format(self.items_endpoint,
                                                self.item_id)

        # Trying to delete the item now
        # should raise an error--cannot delete an item that is
        # assigned to an assignment!
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      item_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(item_detail_endpoint)
        self.not_empty(req)

    def test_can_delink_item_from_assessment(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)
        assessment_item_details_endpoint = '{0}{1}/'.format(self.assessment_items_endpoint,
                                                            self.item_id)
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_item_details_endpoint)

        self.sign_client(del_sig)
        req = self.client.delete(assessment_item_details_endpoint)
        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_items_endpoint)
        self.ok(req)
        self.verify_no_data(req)
        self.verify_links(req, 'AssessmentItems')

        item_detail_endpoint = '{0}{1}/'.format(self.items_endpoint,
                                                self.item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(unquote(item['id']), self.item_id)

    def test_authenticated_learner_cannot_link_items_to_assessment(self):
        """
        A learner should not be able to link items to assessments
        """
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        self.convert_user_to_bank_learner(self.bank_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.code(req, 500)

    def test_learner_cannot_put_assessment_items(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)

        self.convert_user_to_bank_learner(self.bank_id)

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', self.assessment_items_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.assessment_items_endpoint)
        self.unauthorized(req)

    def test_learners_cannot_get_assessment_items(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Now verify that learners cannot see this!
        self.convert_user_to_bank_learner(self.bank_id)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_items_endpoint)
        self.code(req, 500)

    def test_learner_cannot_post_assessment_items(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Now verify that learners cannot see this!
        self.convert_user_to_bank_learner(self.bank_id)

        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.code(req, 500)

    def test_learner_cannot_get_assessment_item_details(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)

        assessment_item_details_endpoint = '{0}{1}/'.format(self.assessment_items_endpoint,
                                                            self.item_id)

        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'GET',
                                      assessment_item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_item_details_endpoint)
        self.code(req, 500)

    def test_learner_cannot_delete_assessment_item_details(self):
        payload = {
            'itemIds': [quote(self.item_id)]
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_items_endpoint, payload, format='json')
        self.ok(req)

        assessment_item_details_endpoint = '{0}{1}/'.format(self.assessment_items_endpoint,
                                                            self.item_id)

        self.convert_user_to_bank_learner(self.bank_id)

        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      assessment_item_details_endpoint)

        self.sign_client(del_sig)
        req = self.client.delete(assessment_item_details_endpoint)
        self.unauthorized(req)


class AssessmentsOfferedTests(APITest):
    def setUp(self):
        super(AssessmentsOfferedTests, self).setUp()
        self.bank = self._get_test_bank()
        self.bank_id = unquote(str(self.bank.ident))
        self.assessments_endpoint = '{0}assessment/banks/{1}/assessments/'.format(self.endpoint,
                                                                                  self.bank_id)

        # Use POST to create an assessment
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])
        self.assessment_detail_endpoint = self.assessments_endpoint + assessment_id + '/'
        self.assessment_offering_endpoint = '{0}assessmentsoffered/'.format(self.assessment_detail_endpoint)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }
        offering_post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_offering_endpoint)
        self.sign_client(offering_post_sig)
        req = self.client.post(self.assessment_offering_endpoint, payload, format='json')
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        self.quote_safe_offering_id = offering['id']

        self.assessment_offering_detail_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}'.format(self.endpoint,
                                                                                                           self.bank_id,
                                                                                                           offering_id)

    def tearDown(self):
        super(AssessmentsOfferedTests, self).tearDown()

    def test_cannot_put_assessments_offered_endpoint(self):
        put_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'PUT',
                                      self.assessment_offering_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.assessment_offering_endpoint)
        self.not_allowed(req, 'PUT')

    def test_cannot_delete_assessments_offered_endpoint(self):
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      self.assessment_offering_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_offering_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_exception_thrown_if_send_non_list_or_non_dict_to_offered_endpoint(self):
        get_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'GET',
                                      self.assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_endpoint)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')

        # Use POST to create an offering
        # Inputting something other than a list or dict object should result in an error
        offering_post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_offering_endpoint)
        self.sign_client(offering_post_sig)
        bad_payload = 'this is a bad input format'
        req = self.client.post(self.assessment_offering_endpoint, bad_payload, format='json')
        self.bad_input(req)

    def test_authenticated_instructor_assessment_offered_crud(self):
        """
        Instructors should be able to add assessment offered.
        Cannot create offered unless an assessment has items

        """
        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }
        offering_post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_offering_endpoint)
        self.sign_client(offering_post_sig)
        req = self.client.post(self.assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])
        quote_safe_offering_id = offering['id']
        payload.update({
            'id': quote_safe_offering_id
        })
        self.verify_offerings(req,
                              'AssessmentOffering',
                              [payload])

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_endpoint)
        self.ok(req)
        self.verify_offerings(req,
                              'AssessmentOffering',
                              [payload])
        self.verify_links(req, 'AssessmentOffering')

    def test_cannot_post_offered_details_endpoint(self):
        post_sig = calculate_signature(self.auth,
                                       self.headers,
                                       'POST',
                                       self.assessment_offering_detail_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_offering_detail_endpoint)
        self.not_allowed(req, 'POST')

    def test_can_put_to_offered_detail_endpoint(self):
        new_start_time = {
            "startTime": {
                "day": 1,
                "month": 2,
                "year": 2015
            }
        }
        expected_payload = new_start_time
        expected_payload.update({
            "duration": {
                "days": 2
            },
            "id": self.quote_safe_offering_id
        })

        put_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'PUT',
                                      self.assessment_offering_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.assessment_offering_detail_endpoint,
                              new_start_time,
                              format='json')
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', [expected_payload])

        # PUT with a list of length 1 should also work
        new_duration = [{
            "duration": {
                "days": 5,
                "minutes": 120
            }
        }]
        expected_payload = new_duration
        expected_payload[0].update(new_start_time)
        expected_payload[0].update({
            "id": self.quote_safe_offering_id
        })
        req = self.client.put(self.assessment_offering_detail_endpoint,
                              new_duration,
                              format='json')
        self.ok(req)
        self.verify_offerings(req, 'AssessmentOffered', expected_payload)

    def test_can_delete_offered(self):
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      self.assessment_offering_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_offering_detail_endpoint)
        self.ok(req)

        get_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'GET',
                                      self.assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_endpoint)
        self.ok(req)
        self.verify_links(req, 'AssessmentOffered')
        self.verify_no_data(req)

    def test_can_create_multiple_offereds_with_list(self):
        payload = [{
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }, {
            "startTime": {
                "day": 9,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 20
            }
        }]
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_offering_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(self.assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering1_id = unquote(offering['results'][0]['id'])
        offering2_id = unquote(offering['results'][1]['id'])

        payload[0].update({
            'id': offering1_id
        })
        payload[1].update({
            'id': offering2_id
        })

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_endpoint)
        self.ok(req)
        self.verify_offerings(req,
                              'AssessmentOffering',
                              payload)
        self.verify_links(req, 'AssessmentOffering')

    def test_learner_cannot_create_offered(self):
        self.convert_user_to_bank_learner(self.bank_id)
        offering_post_sig = calculate_signature(self.auth, self.headers, 'POST', self.assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }
        req = self.client.post(self.assessment_offering_endpoint, payload, format='json')
        self.unauthorized(req)

    def test_learner_cannot_get_offereds_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_offering_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_endpoint)
        self.ok(req)
        data = self.load(req)
        self.assertEqual(data['data']['count'], 0)

    def test_learner_cannot_get_offered_details(self):
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.assessment_offering_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.assessment_offering_detail_endpoint)
        self.code(req, 500)

    def test_learner_cannot_put_offered_details(self):
        self.convert_user_to_bank_learner(self.bank_id)
        new_start_time = {
            "startTime": {
                "day": 1,
                "month": 2,
                "year": 2015
            }
        }
        put_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'PUT',
                                      self.assessment_offering_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(self.assessment_offering_detail_endpoint,
                              new_start_time,
                              format='json')
        self.unauthorized(req)

    def test_learner_cannot_delete_offered(self):
        """
        """
        self.convert_user_to_bank_learner(self.bank_id)
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      self.assessment_offering_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_offering_detail_endpoint)
        self.unauthorized(req)

    def test_cannot_delete_assessment_that_has_offereds(self):
        del_sig = calculate_signature(self.auth,
                                      self.headers,
                                      'DELETE',
                                      self.assessment_detail_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(self.assessment_detail_endpoint)
        self.not_empty(req)


class Ortho3DItemTests(APITest):
    def setUp(self):
        super(Ortho3DItemTests, self).setUp()
        self.bank = self._get_test_bank()
        self.bank_id = unquote(str(self.bank.ident))
        self.items_endpoint = '{0}assessment/banks/{1}/items/'.format(self.endpoint,
                                                                      self.bank_id)

        self.item_name = 'a really complicated item'
        self.item_desc = 'meant to differentiate students'
        self.question_string = 'can you manipulate this?'
        self.question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        self.answer_string = 'yes!'
        self.answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        self.payload = {
            "name": self.item_name,
            "description": self.item_desc,
            "question": {
                "type": self.question_type,
                "questionString": self.question_string
            },
            "answers": [{
                "type": self.answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(self.payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(self.payload['question'])
        stringified_payload['answers'] = json.dumps(self.payload['answers'])
        req = self.client.post(self.items_endpoint, stringified_payload)
        self.ok(req)
        self.item = json.loads(req.content)
        self.item_id = unquote(self.item['id'])

    def tearDown(self):
        super(Ortho3DItemTests, self).tearDown()

    def test_can_create_ortho_3d_item(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])
        req = self.client.post(self.items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])

    def test_can_get_ortho_3d_question_with_files_with_items_endpoint(self):
        items_with_files = self.items_endpoint + '?files'

        get_sig = calculate_signature(self.auth, self.headers, 'GET', items_with_files)
        self.sign_client(get_sig)
        req = self.client.get(items_with_files)
        self.ok(req)
        self.reset_files()
        expected_files = {
            'manip': self.encode(self.manip),
            'frontView': self.encode(self.front),
            'sideView': self.encode(self.side),
            'topView': self.encode(self.top)
        }
        expected_answer = self.payload['answers'][0]['integerValues']

        self.verify_text(req,
                         'Item',
                         self.item_name,
                         self.item_desc,
                         self.item_id)
        self.verify_questions_answers(req,
                                      self.question_string,
                                      self.question_type,
                                      [self.answer_string],
                                      [self.answer_type],
                                      self.item_id)
        self.verify_links(req, 'Item')
        self.verify_ortho_answers(req, expected_answer)
        self.verify_ortho_files(req, expected_files)

    def test_can_download_ortho_3d_question_with_files(self):
        item_files_endpoint = '{0}{1}/files/'.format(self.items_endpoint,
                                                     self.item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_files_endpoint)
        self.ok(req)
        self.verify_links(req, 'ItemFiles')

        files_list = json.loads(req.content)['data']
        for file_obj in files_list:
            # just check that 200 returned...don't verify file content
            # list of one-object dicts, like:
            # "data": [
            #     {
            #         "html_ex02_p05": "<base64 encoded data>"
            #     }
            # ]
            file_link = file_obj[file_obj.keys()[0]]
            self.is_cloudfront_url(file_link)
            req = requests.get(file_link)
            self.ok(req)

    def test_learner_cannot_get_ortho_3d_item_files(self):
        item_files_endpoint = '{0}{1}/files/'.format(self.items_endpoint,
                                                     self.item_id)
        self.convert_user_to_bank_learner(self.bank_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_files_endpoint)
        self.code(req, 500)

    def test_can_update_ortho_3d_question_manip_file(self):
        project_root = ABS_PATH + '/tests/'
        new_manip_path = project_root + 'image001.jpg'
        new_manip = open(new_manip_path, 'rb')

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           self.item_id)

        payload = {
            "manip": new_manip
        }
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        self.reset_files()
        new_manip.seek(0)
        expected_files_new_manip = {
            'manip': self.encode(new_manip),
            'frontView': self.encode(self.front),
            'sideView': self.encode(self.side),
            'topView': self.encode(self.top)
        }
        self.verify_question(req,
                             self.question_string,
                             self.question_type)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_ortho_files(req, expected_files_new_manip)

    def test_can_update_ortho_3d_front_view(self):
        project_root = ABS_PATH + '/tests/'
        new_front_path = project_root + 'front2.jpg'
        new_front = open(new_front_path, 'rb')

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           self.item_id)

        payload = {
            "frontView": new_front
        }
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        self.reset_files()
        new_front.seek(0)
        expected_files_new_top = {
            'manip': self.encode(self.manip),
            'frontView': self.encode(new_front),
            'sideView': self.encode(self.side),
            'topView': self.encode(self.top)
        }
        self.verify_question(req,
                             self.question_string,
                             self.question_type)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_ortho_files(req, expected_files_new_top)

    def test_can_update_ortho_3d_side_view(self):
        project_root = ABS_PATH + '/tests/'
        new_side_path = project_root + 'side2.jpg'
        new_side = open(new_side_path, 'rb')

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           self.item_id)

        payload = {
            "sideView": new_side
        }
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        self.reset_files()
        new_side.seek(0)
        expected_files_new_side = {
            'manip': self.encode(self.manip),
            'frontView': self.encode(self.front),
            'sideView': self.encode(new_side),
            'topView': self.encode(self.top)
        }
        self.verify_question(req,
                             self.question_string,
                             self.question_type)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_ortho_files(req, expected_files_new_side)

    def test_can_update_ortho_3d_top_view(self):
        project_root = ABS_PATH + '/tests/'
        new_top_path = project_root + 'top2.jpg'
        new_top = open(new_top_path, 'rb')

        item_question_endpoint = '{0}{1}/question/'.format(self.items_endpoint,
                                                           self.item_id)

        payload = {
            "topView": new_top
        }
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_question_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_question_endpoint, payload)
        self.ok(req)
        self.reset_files()
        new_top.seek(0)
        expected_files_new_top = {
            'manip': self.encode(self.manip),
            'frontView': self.encode(self.front),
            'sideView': self.encode(self.side),
            'topView': self.encode(new_top)
        }
        self.verify_question(req,
                             self.question_string,
                             self.question_type)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_question_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_question_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.verify_ortho_files(req, expected_files_new_top)

    def test_can_update_ortho_3d_answer_set(self):
        answer_id = self.extract_answers(self.item)[0]['id']

        item_answer_detail_endpoint = '{0}{1}/answers/{2}/'.format(self.items_endpoint,
                                                                   self.item_id,
                                                                   unquote(answer_id))

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_answer_detail_endpoint)
        self.sign_client(put_sig)
        new_response_set = {
            "frontFaceValue": 4,
            "sideFaceValue": 5,
            "topFaceValue": 6
        }
        payload = {
            'integerValues': new_response_set
        }
        req = self.client.put(item_answer_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_answers(req,
                            [self.answer_string],
                            [self.answer_type])
        self.verify_ortho_answers(req, new_response_set)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_answer_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_answer_detail_endpoint)
        self.ok(req)
        self.verify_answers(req,
                            [self.answer_string],
                            [self.answer_type])
        self.verify_ortho_answers(req, new_response_set)

    def test_exception_if_missing_ortho_3d_viewset_file(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'

        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }

        payload_missing_front = deepcopy(payload)
        payload_missing_front['manip'] = self.manip
        payload_missing_front['sideView'] = self.side
        payload_missing_front['topView'] = self.top
        self.manip.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        payload_missing_front['question'] = json.dumps(payload['question'])
        payload_missing_front['answers'] = json.dumps(payload['answers'])
        req = self.client.post(self.items_endpoint, payload_missing_front)
        self.missing_orthoview(req)

        payload_missing_top = deepcopy(payload)
        payload_missing_top['manip'] = self.manip
        payload_missing_top['frontView'] = self.front
        payload_missing_top['sideView'] = self.side
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        payload_missing_top['question'] = json.dumps(payload['question'])
        payload_missing_top['answers'] = json.dumps(payload['answers'])

        req = self.client.post(self.items_endpoint, payload_missing_top)
        self.missing_orthoview(req)

        payload_missing_side = deepcopy(payload)
        payload_missing_side['manip'] = self.manip
        payload_missing_side['frontView'] = self.front
        payload_missing_side['topView'] = self.top
        self.manip.seek(0)
        self.front.seek(0)
        self.top.seek(0)
        payload_missing_side['question'] = json.dumps(payload['question'])
        payload_missing_side['answers'] = json.dumps(payload['answers'])

        req = self.client.post(self.items_endpoint, payload_missing_side)
        self.missing_orthoview(req)


class AssessmentTakingTests(APITest):
    def get_endpoint(self, endpoint_name):
        taken_id = self.get_taken_id()
        endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/{3}/'.format(self.endpoint,
                                                                              self.bank_id,
                                                                              taken_id,
                                                                              endpoint_name)
        return endpoint

    def get_expected_files(self):
        item_detail_endpoint = '{0}assessment/banks/{1}/items/{2}/'.format(self.endpoint,
                                                                           self.bank_id,
                                                                           self.item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        expected_files = self.extract_question(json.loads(req.content))['files']
        return expected_files

    def get_taken_id(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = self.load(req)
        taken_id = unquote(taken['id'])
        return taken_id

    def setUp(self):
        super(AssessmentTakingTests, self).setUp()
        self.auth = HTTPSignatureAuth(key_id=self.public_key,
                                      secret=self.private_key,
                                      algorithm='hmac-sha256',
                                      headers=self.signature_headers)
        bank = self._get_test_bank()
        self.bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + self.bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        self.question_string = 'can you manipulate this?'
        self.question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": self.question_type,
                "questionString": self.question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(self.auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        self.item_id = unquote(item['id'])

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        self.assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + self.assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [self.item_id]
        }
        link_post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        self.offering_id = unquote(offering['id'])

    def tearDown(self):
        super(AssessmentTakingTests, self).tearDown()

    def test_instructor_can_get_takens_given_assessment_id(self):
        assessment_takens_endpoint = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                        self.bank_id,
                                                                                                        self.assessment_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', assessment_takens_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_takens_endpoint)
        self.ok(req)
        # TODO: create a taken and check that it's in the list

    def test_cannot_post_to_takens_url_with_assessment_id(self):
        assessment_takens_endpoint = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                        self.bank_id,
                                                                                                        self.assessment_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_takens_endpoint)
        self.wrong_taken_post_url(req)

    def test_cannot_delete_takens_url_with_assessment_id(self):
        assessment_takens_endpoint = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                        self.bank_id,
                                                                                                        self.assessment_id)
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', assessment_takens_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_takens_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_put_takens_url_with_assessment_id(self):
        assessment_takens_endpoint = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                        self.bank_id,
                                                                                                        self.assessment_id)

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', assessment_takens_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_takens_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_get_takens_given_assessment_offered_id(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', assessment_offering_takens_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(assessment_offering_takens_endpoint)
        self.ok(req)

    def test_can_post_to_create_new_taken_given_assessment_offered_id(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)

    def test_student_can_post_to_create_new_taken_given_assessment_offered_id(self):
        self.convert_user_to_bank_learner(self.bank_id)
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)

    def test_cannot_delete_takens_url_with_offered_id(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', assessment_offering_takens_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(assessment_offering_takens_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_put_takens_url_with_offered_id(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', assessment_offering_takens_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(assessment_offering_takens_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_get_taken_details(self):
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', taken_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_details_endpoint)
        self.ok(req)

    def test_student_can_get_taken_details(self):
        self.convert_user_to_bank_learner(self.bank_id)
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', taken_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_details_endpoint)
        self.ok(req)

    def test_cannot_put_taken_details(self):
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', taken_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_details_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_delete_taken_details(self):
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', taken_details_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_details_endpoint)
        self.ok(req)

    def test_student_cannot_delete_taken_details(self):
        self.convert_user_to_bank_learner(self.bank_id)
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', taken_details_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_details_endpoint)
        self.code(req, 403)

    def test_cannot_post_taken_details(self):
        taken_id = self.get_taken_id()
        taken_details_endpoint = '{0}assessment/banks/{1}/assessmentstaken/{2}/'.format(self.endpoint,
                                                                                        self.bank_id,
                                                                                        taken_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', taken_details_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_details_endpoint)
        self.not_allowed(req, 'POST')

    def test_posting_again_to_offered_takens_endpoint_returns_same_taken(self):
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = self.load(req)
        taken_id = unquote(taken['id'])

        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        data = self.load(req)
        self.assertEqual(taken_id,
                         unquote(data['id']))

    def test_student_posting_again_to_offered_takens_endpoint_returns_same_taken(self):
        self.convert_user_to_bank_learner(self.bank_id)
        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(
            self.endpoint,
            self.bank_id,
            self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = self.load(req)
        taken_id = unquote(taken['id'])

        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        data = self.load(req)
        self.assertEqual(taken_id,
                         unquote(data['id']))

    def test_can_get_take_endpoint(self):
        take_endpoint = self.get_endpoint('take')
        get_sig = calculate_signature(self.auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        expected_files = self.get_expected_files()
        self.verify_ortho_files(req, expected_files)

    def test_student_can_get_take_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        take_endpoint = self.get_endpoint('take')
        get_sig = calculate_signature(self.auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.ok(req)
        self.verify_question(req,
                             self.question_string,
                             self.question_type)
        self.convert_user_to_bank_instructor(self.bank_id)
        expected_files = self.get_expected_files()
        self.convert_user_to_bank_learner(self.bank_id)
        self.verify_ortho_files(req, expected_files)

    def test_cannot_delete_take_endpoint(self):
        take_endpoint = self.get_endpoint('take')
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', take_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(take_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_post_take_endpoint(self):
        take_endpoint = self.get_endpoint('take')
        post_sig = calculate_signature(self.auth, self.headers, 'POST', take_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(take_endpoint)
        self.not_allowed(req, 'POST')

    def test_cannot_put_take_endpoint(self):
        take_endpoint = self.get_endpoint('take')
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', take_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(take_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_get_files_endpoint(self):
        files_endpoint = self.get_endpoint('files')
        get_sig = calculate_signature(self.auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

    def test_student_can_get_files_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        files_endpoint = self.get_endpoint('files')
        get_sig = calculate_signature(self.auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

    def test_cannot_delete_files_endpoint(self):
        files_endpoint = self.get_endpoint('files')
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(files_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_post_files_endpoint(self):
        files_endpoint = self.get_endpoint('files')
        post_sig = calculate_signature(self.auth, self.headers, 'POST', files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(files_endpoint)
        self.not_allowed(req, 'POST')

    def test_cannot_put_files_endpoint(self):
        files_endpoint = self.get_endpoint('files')
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(files_endpoint)
        self.not_allowed(req, 'PUT')

    def test_cannot_delete_submit_endpoint(self):
        submit_endpoint = self.get_endpoint('submit')
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(submit_endpoint)
        self.not_allowed(req, 'DELETE')

    def test_cannot_get_submit_endpoint(self):
        submit_endpoint = self.get_endpoint('submit')
        get_sig = calculate_signature(self.auth, self.headers, 'GET', submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(submit_endpoint)
        self.not_allowed(req, 'GET')

    def test_cannot_put_submit_endpoint(self):
        submit_endpoint = self.get_endpoint('submit')
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(submit_endpoint)
        self.not_allowed(req, 'PUT')

    def test_can_submit_wrong_answer_to_submit_endpoint(self):
        submit_endpoint = self.get_endpoint('submit')
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

    def test_student_can_submit_wrong_answer_to_submit_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        submit_endpoint = self.get_endpoint('submit')
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False, _has_next=False)

    def test_after_submitting_taken_is_automatically_closed(self):
        taken_id = self.get_taken_id()
        submit_endpoint = self.get_endpoint('submit')
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')

        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                                        self.bank_id,
                                                                                                                        self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        new_taken = json.loads(req.content)
        new_taken_id = unquote(new_taken['id'])
        self.assertNotEqual(taken_id, new_taken_id)

    def test_student_after_submitting_taken_is_automatically_closed(self):
        self.convert_user_to_bank_learner(self.bank_id)
        taken_id = self.get_taken_id()
        submit_endpoint = self.get_endpoint('submit')
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, wrong_response, format='json')

        assessment_offering_takens_endpoint = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken/'.format(self.endpoint,
                                                                                                                        self.bank_id,
                                                                                                                        self.offering_id)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        new_taken = json.loads(req.content)
        new_taken_id = unquote(new_taken['id'])
        self.assertNotEqual(taken_id, new_taken_id)

    def test_can_submit_right_response_to_submit_endpoint(self):
        submit_endpoint = self.get_endpoint('submit')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

    def test_student_can_submit_right_response_to_submit_endpoint(self):
        self.convert_user_to_bank_learner(self.bank_id)
        submit_endpoint = self.get_endpoint('submit')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True, _has_next=False)

    def test_cannot_resubmit_after_submitting(self):
        submit_endpoint = self.get_endpoint('submit')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        req = self.client.post(submit_endpoint, right_response, format='json')
        self.already_taken(req)

    def test_student_cannot_resubmit_after_submitting(self):
        self.convert_user_to_bank_learner(self.bank_id)
        submit_endpoint = self.get_endpoint('submit')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        req = self.client.post(submit_endpoint, right_response, format='json')
        self.already_taken(req)

    def test_cannot_get_take_endpoint_again_after_submitting(self):
        submit_endpoint = self.get_endpoint('submit')
        take_endpoint = self.get_endpoint('take')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        get_sig = calculate_signature(self.auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.already_taken(req)

    def test_student_cannot_get_take_endpoint_again_after_submitting(self):
        self.convert_user_to_bank_learner(self.bank_id)
        submit_endpoint = self.get_endpoint('submit')
        take_endpoint = self.get_endpoint('take')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        get_sig = calculate_signature(self.auth, self.headers, 'GET', take_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(take_endpoint)
        self.already_taken(req)

    def test_cannot_get_files_endpoint_after_submitting(self):
        submit_endpoint = self.get_endpoint('submit')
        files_endpoint = self.get_endpoint('files')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        get_sig = calculate_signature(self.auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.already_taken(req)

    def test_student_cannot_get_files_endpoint_after_submitting(self):
        self.convert_user_to_bank_learner(self.bank_id)
        submit_endpoint = self.get_endpoint('submit')
        files_endpoint = self.get_endpoint('files')
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(submit_endpoint, right_response, format='json')

        get_sig = calculate_signature(self.auth, self.headers, 'GET', files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(files_endpoint)
        self.already_taken(req)

    def test_authenticated_instructor_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken

        NOTE: this tests the simple, question-dump (client in control)
        for taking assessments
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        # expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue": 5,
                    "topFaceValue": 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        # expected_files_2 = self.extract_question(item)['files']

        items_with_files = items_endpoint + '?files'
        get_sig = calculate_signature(auth, self.headers, 'GET', items_with_files)
        self.sign_client(get_sig)
        req = self.client.get(items_with_files)
        self.ok(req)
        expected_files_1 = json.loads(req.content)['data']['results'][0]['question']['files']
        expected_files_2 = json.loads(req.content)['data']['results'][1]['question']['files']

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', taken_questions_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_questions_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', taken_questions_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_questions_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_questions_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_questions_endpoint)
        self.not_allowed(req, 'POST')

        taken_with_files = taken_questions_endpoint + '?files'
        get_sig = calculate_signature(auth, self.headers, 'GET', taken_with_files)
        self.sign_client(get_sig)
        req = self.client.get(taken_with_files)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]['questions'][0]
        question_2 = questions['data']['results'][0]['questions'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_files_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_files_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_files_endpoint)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE should not work on the status URLs
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_status_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_status_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_status_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_status_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_status_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_status_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responsded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue": 4,
                "sideFaceValue": 5,
                "topFaceValue": 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_2_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

    def test_authenticated_learner_multi_question_taking(self):
        """
        Creating an assessment with multiple questions
        allows consumer to pick and choose which one is taken
        Same as above, but for a learner

        NOTE: this tests the simple, question-dump (client in control)
        for taking assessments
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        # expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue": 5,
                    "topFaceValue": 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        # expected_files_2 = self.extract_question(item)['files']

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'

        get_sig = calculate_signature(auth, self.headers, 'GET', item_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_1_endpoint)
        item_1 = json.loads(req.content)
        expected_files_1 = self.extract_question(item_1)['files']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_2_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_2_endpoint)
        item_2 = json.loads(req.content)
        expected_files_2 = self.extract_question(item_2)['files']

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Learner can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', taken_questions_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(taken_questions_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', taken_questions_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(taken_questions_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_questions_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_questions_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_endpoint)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]['questions'][0]
        question_2 = questions['data']['results'][0]['questions'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        # self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        # self.verify_ortho_files(question_2, expected_files_2)
        self.verify_question_status(question_1, False)
        self.verify_question_status(question_2, False)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_endpoint)
        self.not_allowed(req, 'POST')

        # verify the files are present when we get each question independently
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        self.ok(req)
        question_1_details = json.loads(req.content)
        self.verify_ortho_files(question_1_details, expected_files_1)

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_files_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_files_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_files_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_files_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_files_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_files_endpoint)
        self.not_allowed(req, 'POST')

        # POST, PUT, DELETE should not work on the status URLs
        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_status_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_status_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_status_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_status_endpoint)
        self.not_allowed(req, 'PUT')

        # POST to this root url also returns a 405
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_status_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_status_endpoint)
        self.not_allowed(req, 'POST')

        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, wrong_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, True, False)

        taken_questions_with_files = taken_questions_endpoint + '?files'
        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_with_files)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_with_files)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]['questions'][0]
        question_2 = questions['data']['results'][0]['questions'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, False)
        self.verify_question_status(question_2, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the question status now should return a responded right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, True, True)

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_with_files)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_with_files)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]['questions'][0]
        question_2 = questions['data']['results'][0]['questions'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        self.verify_question_status(question_1, True, True)
        self.verify_question_status(question_2, False)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_endpoint)
        question = json.loads(req.content)
        self.verify_question_status(question, False)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue": 4,
                "sideFaceValue": 5,
                "topFaceValue": 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_2_submit_endpoint, right_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

    # def test_authenticated_instructor_euler_crud(self):
    #     """
    #     Does the Euler-rotation type question work?
    #     :return:
    #     """
    #     self.fail('Finish writing the test!')
    #

    def test_authenticated_instructor_learner_form_posting(self):
        """
        All POST endpoints should accept multipart/form-data as well
        as just JSON.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])
        # expected_files_1 = self.extract_question(item)['files']

        # for this test, create a second item
        item_name_2 = 'a really complicated item'
        item_desc_2 = 'meant to differentiate students'
        question_string_2 = 'can you manipulate this second thing?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name_2,
            "description": item_desc_2,
            "question": {
                "type": question_type,
                "questionString": question_string_2
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 4,
                    "sideFaceValue": 5,
                    "topFaceValue": 0
                }
            }],
        }
        self.manip.seek(0)
        self.front.seek(0)
        self.side.seek(0)
        self.top.seek(0)
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_2_id = unquote(item['id'])
        question_2_id = unquote(self.extract_question(item)['id'])
        # expected_files_2 = self.extract_question(item)['files']

        item_1_endpoint = items_endpoint + item_1_id + '/'
        item_2_endpoint = items_endpoint + item_2_id + '/'

        items_with_files = items_endpoint + '?files'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_with_files)
        self.sign_client(get_sig)
        req = self.client.get(items_with_files)
        self.ok(req)
        expected_files_1 = json.loads(req.content)['data']['results'][0]['question']['files']
        expected_files_2 = json.loads(req.content)['data']['results'][1]['question']['files']

        # Now create an assessment, link the item2 to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='multipart')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id, item_2_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='multipart')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        payload_stringified = {
            'startTime': json.dumps(payload['startTime']),
            'duration': json.dumps(payload['duration'])
        }
        req = self.client.post(assessment_offering_endpoint, payload_stringified, format='multipart')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'

        taken_questions_with_files = taken_questions_endpoint + '?files'

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_with_files)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_with_files)
        self.ok(req)
        questions = json.loads(req.content)
        question_1 = questions['data']['results'][0]['questions'][0]
        question_2 = questions['data']['results'][0]['questions'][1]
        self.verify_links(req, 'TakenQuestions')

        self.verify_question(question_1,
                             question_string,
                             question_type)
        self.verify_ortho_files(question_1, expected_files_1)

        self.verify_question(question_2,
                             question_string_2,
                             question_type)
        self.verify_ortho_files(question_2, expected_files_2)

        # Because this is an ortho3D problem, we expect
        # that the files endpoint also works
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_files_endpoint = question_1_endpoint + 'files/'
        question_1_status_endpoint = question_1_endpoint + 'status/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'
        question_2_endpoint = taken_questions_endpoint + question_2_id + '/'
        question_2_files_endpoint = question_2_endpoint + 'files/'
        question_2_status_endpoint = question_2_endpoint + 'status/'
        question_2_submit_endpoint = question_2_endpoint + 'submit/'

        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_files_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_files_endpoint)
        self.ok(req)
        self.verify_taking_files(req)

        # checking on the question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.not_responded(req)

        # Can submit a wrong response
        wrong_response = {
            "integerValues": {
                "frontFaceValue": 0,
                "sideFaceValue": 1,
                "topFaceValue": 2
            }
        }

        # Check that DELETE returns error code 405--we don't support this
        del_sig = calculate_signature(auth, self.headers, 'DELETE', question_1_submit_endpoint)
        self.sign_client(del_sig)
        req = self.client.delete(question_1_submit_endpoint)
        self.not_allowed(req, 'DELETE')

        # PUT to this root url also returns a 405
        put_sig = calculate_signature(auth, self.headers, 'PUT', question_1_submit_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(question_1_submit_endpoint)
        self.not_allowed(req, 'PUT')

        # GET to this root url also returns a 405
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_submit_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_submit_endpoint)
        self.not_allowed(req, 'GET')

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        wrong_response_stringified = {
            'integerValues': json.dumps(wrong_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint, wrong_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=False)

        # checking on the question status now should return a responded but wrong
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, False)

        # can resubmit using the specific ID
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        right_response_stringified = {
            'integerValues': json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_1_submit_endpoint, right_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # checking on the question status now should return a responded, right
        get_sig = calculate_signature(auth, self.headers, 'GET', question_1_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_1_status_endpoint)
        self.responded(req, True)

        # checking on the second question status now should return a not-responded
        get_sig = calculate_signature(auth, self.headers, 'GET', question_2_status_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(question_2_status_endpoint)
        self.not_responded(req)

        # can submit to the second question in the assessment
        right_response = {
            "integerValues": {
                "frontFaceValue": 4,
                "sideFaceValue": 5,
                "topFaceValue": 0
            }
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', question_2_submit_endpoint)
        self.sign_client(post_sig)
        right_response_stringified = {
            'integerValues': json.dumps(right_response['integerValues'])
        }
        req = self.client.post(question_2_submit_endpoint, right_response_stringified, format='multipart')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

    def test_authenticated_instructor_items_first_angle(self):
        """
        Test that can set the firstAngle attribute of a question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, False)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id)
        self.verify_links(req, 'Item')
        self.verify_first_angle(req, False)

        # Now try to update the firstAngle
        item_detail_endpoint = items_endpoint + item_id + '/'
        payload = {
            'question': {
                'firstAngle': True
            }
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_detail_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_detail_endpoint, payload, format='json')
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_first_angle(req, True)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_links(req, 'ItemDetails')
        self.verify_first_angle(req, True)

        self.delete_item(auth, item_detail_endpoint)

        # now test that you can create an item initially with the firstAngle set
        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string,
                "firstAngle": True
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        self.verify_first_angle(req, True)

        item_id = unquote(json.loads(req.content)['id'])
        item_detail_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_detail_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_detail_endpoint)
        self.ok(req)
        self.verify_first_angle(req, True)

    # # BUILD A TEST FOR SENDING SINGLE ITEM ID TO ASSESSMENT, NOT A LIST

    def test_authenticated_instructor_ortho_3d_multi_choice_crud(self):
        """
        Test an instructor can upload a manipulatable AssetBundle file
        and multiple 2-image ortho choice sets to create a multiple
        choice question
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        self.convert_user_to_bank_learner(bank_id)

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "choiceId": 2
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])
        req = self.client.post(items_endpoint, stringified_payload)
        self.unauthorized(req)

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        self.convert_user_to_bank_instructor(bank_id)
        self.manip.seek(0)
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)

        # this should throw an exception -- need to include some choices!
        req = self.client.post(items_endpoint, stringified_payload)
        self.include_choices(req)

        # even with one choice set, should throw an exception
        stringified_payload['choice0small'] = self.choice0sm
        stringified_payload['choice0big'] = self.choice0big
        self.manip.seek(0)
        req = self.client.post(items_endpoint, stringified_payload)
        self.include_choices(req)

        # should be okay with two choices
        stringified_payload['choice1small'] = self.choice1sm
        stringified_payload['choice1big'] = self.choice1big
        self.manip.seek(0)
        self.choice0sm.seek(0)
        self.choice0big.seek(0)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])
        question_id = self.extract_question(item)['id']
        answer_id = self.extract_answers(item)[0]['id']

        expected_choices = self.extract_question(item)['choices']
        expected_answer = [self.extract_answers(item)[0]['choiceIds'][0]]

        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc)
        self.verify_questions_answers(req,
                                      question_string,
                                      question_type,
                                      [answer_string],
                                      [answer_type])
        self.verify_ortho_multi_choice_answers(req, expected_answer)

        # check files by going to detailed URL
        item_details_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)

        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip': self.encode(self.manip)})

        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        taken_questions_with_files = taken_questions_endpoint + '?files'
        # check that the questions are present in the taken
        get_sig = calculate_signature(auth, self.headers, 'GET', taken_questions_with_files)
        self.sign_client(get_sig)
        req = self.client.get(taken_questions_with_files)
        self.ok(req)
        self.verify_question(req,
                             question_string,
                             question_type)
        self.verify_ortho_choices(req, expected_choices)
        self.verify_ortho_files(req, {'manip': self.encode(self.manip)})

        # Submitting a non-list response is okay, if it is right
        bad_response = {
            'choiceIds': expected_answer[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, bad_response)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # Now can submit a response to this endpoint
        response = {
            'choiceIds': [expected_answer[0]]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # Should also be able to update items with multiple answers!
        # POST to the item_answers_endpoint
        item_answers_endpoint = item_details_endpoint + 'answers/'
        payload = {
            'type': answer_type,
            'choiceId': 1
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', item_answers_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(item_answers_endpoint, payload)
        self.ok(req)

        all_answers = json.loads(req.content)['results']  # should be a list of two objects
        self.assertEqual(
            json.loads(req.content)['count'],
            2
        )
        expected_answers = []
        for ans in all_answers:
            expected_answers.append(ans['choiceIds'][0])
        expected_answer_1 = expected_answers[0]
        expected_answer_2 = expected_answers[1]

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.verify_ortho_multi_choice_answers(req, expected_answers)

        # DELETE the old taken, create a new one so the updated answer
        # is a part of it
        self.delete_item(auth, taken_endpoint)
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # submitting a single answer (even if right) will be right;
        # behavior changed is that multiple answers, either is correct
        incomplete_response = {
            'choiceIds': [expected_answer_1]
        }
        taken_questions_endpoint = taken_endpoint + 'questions/'
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, incomplete_response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # TODO: submitting both answers is okay
        response = {
            'choiceIds': [expected_answer_1, expected_answer_2]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # TODO: submitting both answers in the wrong order is okay
        response = {
            'choiceIds': [expected_answer_2, expected_answer_1]
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

    def test_authenticated_instructor_can_set_item_genus_type_and_file_names(self):
        """
        When creating a new item, can define a specific genus type
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Instructor,
        # so this should show up in GET
        item_genus = 'question%3Achoose-viewset%40ODL.MIT.EDU'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        manip_name = 'A bracket'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Amulti-choice-ortho%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "genus": item_genus,
            "question": {
                "choiceNames": ["view 1", "view 2"],
                "genus": item_genus,
                "promptName": manip_name,
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "genus": item_genus,
                "type": answer_type,
                "choiceId": 1
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['choice0small'] = self.choice0sm
        stringified_payload['choice0big'] = self.choice0big
        stringified_payload['choice1small'] = self.choice1sm
        stringified_payload['choice1big'] = self.choice1big

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item_id = unquote(json.loads(req.content)['id'])
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         _genus=item_genus)

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)
        self.verify_text(req,
                         'Item',
                         item_name,
                         item_desc,
                         item_id,
                         _genus=item_genus)
        self.verify_links(req, 'Item')
        item = json.loads(req.content)
        question = self.extract_question(item)
        answer = self.extract_answers(item)[0]
        self.assertEqual(
            question['genusTypeId'],
            item_genus
        )
        self.assertEqual(
            answer['genusTypeId'],
            item_genus
        )

    def test_authenticated_instructor_edx_multi_choice_missing_parameters(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }

        params = ['questionString', 'choices', 'choiceId']
        for param in params:
            test_payload = deepcopy(payload)
            if param == 'choiceId':
                del test_payload['answers'][0][param]
            else:
                del test_payload['question'][param]
            req = self.client.post(items_endpoint,
                                   test_payload,
                                   format='json')
            self.code(req, 500)
            self.message(req,
                         '"' + param + '" required in input parameters but not provided.',
                         True)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_high(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 4
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.code(req, 500)
        self.message(req,
                     'Correct answer 4 is not valid. Not that many choices!',
                     True)

    def test_authenticated_instructor_edx_multi_choice_answer_index_too_low(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 0
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')

        self.code(req, 500)
        self.message(req,
                     'Correct answer 0 is not valid. Must be between 1 and # of choices.',
                     True)

    def test_authenticated_instructor_edx_multi_choice_answer_not_enough_choices(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you answer this?'
        question_choices = [
            'yes'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 1
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }

        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.code(req, 500)
        self.message(req,
                     '"choices" is shorter than 2.',
                     True)

    def test_authenticated_instructor_edx_multi_choice_with_named_choices(self):
        """
        Test an instructor can create named choices with a dict
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text': 'I hope so.',
             'name': 'yes'},
            {'text': 'I don\'t think I can.',
             'name': 'no'},
            {'text': 'Maybe tomorrow.',
             'name': 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)

    def test_authenticated_instructor_edx_multi_choice_crud(self):
        """
        Test an instructor can create and respond to an edX multiple choice
        type question.
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }
        req = self.client.post(items_endpoint,
                               payload,
                               format='json')
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'

        expected_answers = item['answers'][0]['choiceIds']

        # attach to an assessment -> offering -> taken
        # The user can now respond to the question and submit a response
        # first attach the item to an assessment
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # Instructor can now take the assessment
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # Only GET of this endpoint is supported
        taken_questions_endpoint = taken_endpoint + 'questions/'
        # Submitting a non-list response is okay, if it is right, because
        # service will listify it
        bad_response = {
            'choiceIds': expected_answers[0]
        }
        taken_question_details_endpoint = taken_questions_endpoint + item_id + '/'
        taken_submit_endpoint = taken_question_details_endpoint + 'submit/'
        post_sig = calculate_signature(auth, self.headers, 'POST', taken_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(taken_submit_endpoint, bad_response)
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

        # Now can submit a list response to this endpoint
        response = {
            'choiceIds': expected_answers
        }
        req = self.client.post(taken_submit_endpoint, response, format='json')
        self.ok(req)
        self.verify_submission(req, _expected_result=True)

    def test_authenticated_instructor_edx_multi_choice_with_files(self):
        """
        In case instructor wants to upload image files, etc., with the problem
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item--right now user is Learner,
        # so this should throw unauthorized
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            {'text': 'I hope so.',
             'name': 'yes'},
            {'text': 'I don\'t think I can.',
             'name': 'no'},
            {'text': 'Maybe tomorrow.',
             'name': 'maybe'}
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }

        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip

        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)

        self.assertIn('fileIds', item)
        self.assertTrue(len(item['fileIds']) == 1)
        self.assertIn('fileIds', item['question'])
        self.assertTrue(len(item['question']['fileIds']) == 1)

        # for newly created items, don't include the "files" attr
        # self.assertIn('files', item['question'])
        # self.assertTrue(len(item['question']['files']) == 1)
        self.assertEqual(
            item['fileIds']['manip'],
            item['question']['fileIds']['manip']
        )


class AuthorizationTests(APITest):
    def setUp(self):
        super(AuthorizationTests, self).setUp()
        self.auth = HTTPSignatureAuth(key_id=self.public_key,
                                      secret=self.private_key,
                                      algorithm='hmac-sha256',
                                      headers=self.signature_headers)
        bank = self._get_test_bank()
        self.bank_id = unquote(str(bank.ident))

        self.items_endpoint = self.endpoint + 'assessment/banks/' + self.bank_id + '/items'

        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc
        }
        req = self.client.post(self.items_endpoint, payload, format='json')
        self.item = self.load(req)
        self.item_id = unquote(self.item['id'])

    def tearDown(self):
        super(AuthorizationTests, self).tearDown()

    def test_authorization_hints_instructor(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        get_sig = calculate_signature(auth, self.headers, 'GET', authz_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(authz_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments', 'assessments_offered', 'assessments_taken',
                         'assessment_banks', 'items']
        nested_keys = ['can_create', 'can_delete', 'can_lookup', 'can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                self.assertTrue(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

    def test_authorization_hints_learner(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        authz_endpoint = bank_endpoint + 'authorizations/'
        self.convert_user_to_bank_learner(bank_id)

        get_sig = calculate_signature(auth, self.headers, 'GET', authz_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(authz_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_keys = ['assessments', 'assessments_offered', 'assessments_taken',
                         'assessment_banks', 'items']
        nested_keys = ['can_create', 'can_delete', 'can_lookup', 'can_update']

        for key, val in data.iteritems():
            self.assertTrue(isinstance(val, dict))
            self.assertIn(key, expected_keys)
            if key == 'assessments':
                self.assertIn('can_take', val)

            for verb, authz in val.iteritems():
                if (verb == 'can_lookup' and
                        key == 'assessments_taken'):
                    self.assertTrue(authz)
                elif (verb == 'can_lookup' and
                        key == 'assessment_banks'):
                    self.assertTrue(authz)
                elif verb == 'can_take':
                    self.assertTrue(authz)
                elif (verb == 'can_create' and
                      key == 'assessments_taken'):
                    self.assertTrue(authz)
                else:
                    self.assertFalse(authz)
                if verb != 'can_take':
                    self.assertIn(verb, nested_keys)
                else:
                    self.assertTrue(key == 'assessments')

    def test_correct_authz_behavior_for_proxy_users_and_back(self):
        # basically tests if the proxies and sessions are being handled
        # correctly on the qbank side, now that we are hashing IDs
        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.ok(req)  # app user should be like instructor
        data = self.load(req)
        self.assertEqual(data['data']['count'], 1)

        self.convert_user_to_bank_learner(self.bank_id)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.ok(req)  # student
        data = self.load(req)
        self.assertEqual(data['data']['count'], 0)
        # import pdb
        # pdb.set_trace()
        self.convert_user_to_app_user(self.bank_id)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', self.items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(self.items_endpoint)
        self.ok(req)
        data = self.load(req)
        # TODO
        # deprecate until we get sessions fixed in services
        # self.assertEqual(data['data']['count'], 0)

        add_user_authz_to_settings('instructor',
                                   self._app_user,
                                   catalog_id=self.bank_id)
        # simulate a cache / session clearing
        # cache.clear()
        req = self.client.get(self.items_endpoint)
        self.ok(req)
        data = self.load(req)
        # TODO
        # deprecate until we get sessions fixed in services
        # self.assertEqual(data['data']['count'], 1)

    def test_list_pagination(self):
        list_endpoints = ['banks', 'items', 'assessments', 'assessmentsoffered', 'assessmentstaken']
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        bank_endpoints = []

        for i in range(0, 20):
            name = 'atestbank ' + str(i)
            desc = 'for testing purposes only ' + str(i)
            bank_id = self.create_test_bank(auth, name, desc)
            bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
            bank_endpoints.append(bank_endpoint)

        banks_endpoint = self.endpoint + 'assessment/banks/'
        get_sig = calculate_signature(auth, self.headers, 'GET', banks_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(banks_endpoint)
        self.ok(req)
        data = self.load(req)

        expected_fields = ['count', 'next', 'previous', 'results']
        for field in expected_fields:
            self.assertIn(
                field,
                data['data']
            )

        self.assertNotEqual(
            data['data']['next'],
            None
        )

        self.assertEqual(
            data['data']['previous'],
            None
        )

        self.assertTrue(data['data']['count'] >= 20)

        self.assertEqual(
            len(data['data']['results']),
            10
        )

        page2 = data['data']['next']
        # total hack
        page2 = page2.replace('http://testserver:80', '')

        sig2 = calculate_signature(auth, self.headers, 'GET', page2)
        self.sign_client(sig2)
        req2 = self.client.get(page2)
        self.ok(req2)
        data2 = self.load(req2)
        self.assertNotEqual(
            data['data']['results'],
            data2['data']['results']
        )

        self.assertEqual(
            len(data2['data']['results']),
            10
        )

        self.assertTrue(data2['data']['count'] >= 20)

        if data2['data']['count'] > 20:
            self.assertNotEqual(
                data2['data']['next'],
                None
            )
        else:
            self.assertEqual(
                data2['data']['next'],
                None
            )

        self.assertNotEqual(
            data2['data']['previous'],
            None
        )

        for endpoint in list_endpoints:
            if endpoint != 'banks':  # because this was already checked
                url = bank_endpoints[0] + endpoint + '/'
                get_sig = calculate_signature(auth, self.headers, 'GET', banks_endpoint)
                self.sign_client(get_sig)

                req = self.client.get(banks_endpoint)
                self.ok(req)
                data = self.load(req)

                for field in expected_fields:
                    self.assertIn(
                        field,
                        data['data']
                    )

    def test_can_get_edxml_item_format_from_item_details(self):
        """
        Do this for learners, assume instructors can also get to this.
        Other tests should reveal if instructors cannot.
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        item_edxml_endpoint = item_details_endpoint + 'edxml/'

        # have to be instructor to use this endpoint
        self.convert_user_to_bank_learner(bank_id)

        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)

        req = self.client.get(item_edxml_endpoint)
        self.code(req, 500)

        self.convert_user_to_bank_instructor(bank_id)
        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_edxml_endpoint)
        self.ok(req)

        data = self.load(req)
        self.assertIn(
            'manip.generic',  # no file extension for manip, because not truly an edX includable file type...we only support .png, .jpg, etc.
            data['files'].keys()
        )
        self.reset_files()

        # test that the S3 URL is not given
        returned_url = data['files']['manip.generic']
        self.assertNotEqual(
            returned_url,
            self._aws_root + str(Id(bank_id).identifier) + '/' + self._manip_filename
        )

        # verify that a signed Cloudfront URL is returned, but don't verify the signature
        # assume that boto calculates the right signature?
        self.is_cloudfront_url(returned_url)

        self.assertEqual(
            str(data['data']),
            '<problem display_name="a really complicated item" max_attempts="0" rerandomize="never" showanswer="closed">\n <p>\n  can you manipulate this?\n </p>\n <multiplechoiceresponse>\n  <choicegroup direction="vertical">\n   <choice correct="false" name="Choice 1">\n    <text>\n     yes\n    </text>\n   </choice>\n   <choice correct="true" name="Choice 2">\n    <text>\n     no\n    </text>\n   </choice>\n   <choice correct="false" name="Choice 3">\n    <text>\n     maybe\n    </text>\n   </choice>\n  </choicegroup>\n </multiplechoiceresponse>\n</problem>'
        )

    def test_non_supported_item_formats_in_item_details(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'

        items_endpoint = bank_endpoint + 'items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_body,
                "choices": question_choices
            },
            "answers": [{
                "type": answer_type,
                "choiceId": answer
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['question'] = json.dumps(stringified_payload['question'])
        stringified_payload['answers'] = json.dumps(stringified_payload['answers'])

        req = self.client.post(items_endpoint,
                               stringified_payload)
        self.ok(req)
        item = self.load(req)
        item_id = unquote(item['id'])
        item_details_endpoint = bank_endpoint + 'items/' + item_id + '/'
        output_format = 'qti'
        item_edxml_endpoint = item_details_endpoint + output_format + '/'

        get_sig = calculate_signature(auth, self.headers, 'GET', item_edxml_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_edxml_endpoint)
        self.code(req, 500)
        self.message(req,
                     '"' + output_format + '" is not a supported item text format.',
                     True)

    # def test_non_supported_item_formats_in_assessment_taken(self):
    #     # CANNOT support the following, yet. Permissions will not work, because
    #     # need the Item (usually to fill in the right answer), yet learners who
    #     # access takens only cannot get to the item, only the question.
    #     self.fail('finish writing the test')

    def test_edX_item_metadata_on_create(self):
        # can add metadata like max_attempts to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for key, value in metadata.iteritems():
            if key == 'rerandomize':
                self.assertEqual(
                    item['question'][key],
                    value
                )
            else:
                self.assertEqual(
                    item[key],
                    value
                )

    def test_edX_item_metadata_update(self):
        # can update metadata like max_attempts to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        metadata = edx_setup_data['metadata']

        new_metadata = {
            'attempts': metadata['attempts'] - 1,
            'markdown': 'second fake markdown',
            'showanswer': 'second fake showanswer',
            'weight': metadata['weight'] / 2,
            'question': {
                'rerandomize': 'second fake rerandomize',
            }
        }

        for key, value in new_metadata.iteritems():
            new_payload = {
                key: value
            }
            put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
            self.sign_client(put_sig)
            req = self.client.put(item_details_endpoint,
                                  new_payload,
                                  format='json')
            self.ok(req)
            item = self.load(req)
            if key == 'question':
                self.assertEqual(
                    item['question']['rerandomize'],
                    value['rerandomize']
                )
                self.assertNotEqual(
                    item['question']['rerandomize'],
                    metadata['rerandomize']
                )
            else:
                self.assertEqual(
                    item[key],
                    value
                )
                self.assertNotEqual(
                    item[key],
                    metadata[key]
                )

            get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
            self.sign_client(get_sig)
            req = self.client.get(item_details_endpoint)
            self.ok(req)
            item = self.load(req)

            if key == 'question':
                self.assertEqual(
                    item['question']['rerandomize'],
                    value['rerandomize']
                )
                self.assertNotEqual(
                    item['question']['rerandomize'],
                    metadata['rerandomize']
                )
            else:
                self.assertEqual(
                    item[key],
                    value
                )
                self.assertNotEqual(
                    item[key],
                    metadata[key]
                )

    def test_edX_item_irt_on_create(self):
        # can add IRT like difficulty to an edX item, on creation
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for key, value in irt.iteritems():
            self.assertEqual(
                item['decimalValues'][key],
                value
            )

    def test_edX_item_irt_update(self):
        # can update IRT like difficulty to an edX item
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        irt = edx_setup_data['irt']

        new_irt = {
            'difficulty': irt['difficulty'] + 2,
            'discrimination': irt['discrimination'] * -1
        }
        for key, value in new_irt.iteritems():
            new_payload = {
                key: value
            }
            put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
            self.sign_client(put_sig)
            req = self.client.put(item_details_endpoint,
                                  new_payload,
                                  format='json')
            self.ok(req)
            item = self.load(req)
            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

            get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
            self.sign_client(get_sig)
            req = self.client.get(item_details_endpoint)
            self.ok(req)
            item = self.load(req)

            self.assertEqual(
                item['decimalValues'][key],
                value
            )
            self.assertNotEqual(
                item['decimalValues'][key],
                irt[key]
            )

    def test_item_query_irt_max_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?max_difficulty='

        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_difficulty = irt['difficulty'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_irt_min_difficulty(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_irt_max_and_min_difficulty_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_difficulty='

        min_difficulty = irt['difficulty'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 1
        query_endpoint_should_appear += str(max_difficulty)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_difficulty = irt['difficulty'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_difficulty) + '&max_difficulty='
        max_difficulty = irt['difficulty'] + 2
        query_endpoint_should_not_appear += str(max_difficulty)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_max_and_min_difficulty_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_difficulty='

        bad_min = irt['difficulty'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_difficulty='
        bad_max = irt['difficulty'] - 1
        query_endpoint_should_appear += str(bad_max)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.code(req, 500)
        self.message(req,
                     'max_difficulty cannot be less than min_difficulty',
                     True)

    # we don't support this yet
    # def test_item_query_irt_max_and_min_difficulty_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_max_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?max_discrimination='

        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(max_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        max_discrimination = irt['discrimination'] - 1
        query_endpoint_should_not_appear = query_endpoint + str(max_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_irt_min_discrimination(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_irt_max_and_min_discrimination_inclusive(self):
        # finds problems inside of the given range
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_discrimination='

        min_discrimination = irt['discrimination'] - 1
        query_endpoint_should_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 1
        query_endpoint_should_appear += str(max_discrimination)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            1
        )

        min_discrimination = irt['discrimination'] + 1
        query_endpoint_should_not_appear = query_endpoint + str(min_discrimination) + '&max_discrimination='
        max_discrimination = irt['discrimination'] + 2
        query_endpoint_should_not_appear += str(max_discrimination)
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_item_query_max_and_min_discrimination_flipped(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        irt = query_setup_data['irt']

        query_endpoint = bank_endpoint + 'items/?min_discrimination='

        bad_min = irt['discrimination'] + 1
        query_endpoint_should_appear = query_endpoint + str(bad_min) + '&max_discrimination='
        bad_max = irt['discrimination'] - 1
        query_endpoint_should_appear += str(bad_max)

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.code(req, 500)
        self.message(req,
                     'max_discrimination cannot be less than min_discrimination',
                     True)

    # we don't support this yet
    # def test_item_query_irt_max_and_min_discrimination_exclusive(self):
    #     # finds problems outside of the given range
    #     self.fail('finish writing the test')

    def test_item_query_irt_display_name(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_name = query_setup_data['item_name']

        query_endpoint = bank_endpoint + 'items/?display_name='

        word_in_name = item_name.split(' ')[2]
        query_endpoint_should_appear = query_endpoint + word_in_name

        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            2  # until we move this to a separate edX test, and out of AuthzTest
        )

        word_not_in_name = 'xkcd'
        query_endpoint_should_not_appear = query_endpoint + word_not_in_name
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint_should_not_appear)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint_should_not_appear)
        self.ok(req)
        results = self.load(req)
        self.assertEqual(
            results['data']['count'],
            0
        )

    def test_lti_consumer_instance_guid_defaults_to_ip(self):
        """
        Cannot count on the LTI consumers providing the lti_tool_consumer_instance_guid
        because it is only a recommended parameter. So default to the requester IP.
        """
        from assessments_users.models import LTIUser
        from utilities.general import id_generator
        lti_headers = ['request-line', 'accept', 'date', 'host', 'x-api-proxy', 'lti-user-id',
                       'lti-tool-consumer-instance-guid', 'lti-user-role', 'lti-bank']
        test_endpoint = self.endpoint + 'assessment/banks/'

        random_id = id_generator()

        add_user_authz_to_settings('student',
                                   random_id,
                                   authority='127.0.0.1')

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = ''
        modified_headers['LTI-User-Role'] = 'Instructor'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)

        self.ok(req)

        self.assertTrue(LTIUser.objects.filter(user_id=random_id).exists())
        self.assertEqual(
            len(LTIUser.objects.filter(user_id=random_id)),
            1
        )

        modified_headers = deepcopy(self.headers)
        modified_headers['LTI-User-ID'] = random_id
        modified_headers['LTI-Tool-Consumer-Instance-GUID'] = 'edu.mit.www'
        modified_headers['LTI-User-Role'] = 'Learner'
        modified_headers['LTI-Bank'] = 'NotARealBank'

        add_user_authz_to_settings('student',
                                   random_id,
                                   authority='edu.mit.www')

        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=lti_headers)
        sig = calculate_signature(auth, modified_headers, 'GET', test_endpoint)
        self.sign_client(sig, modified_headers)
        req = self.client.get(test_endpoint)

        self.ok(req)

        self.assertEqual(
            len(LTIUser.objects.filter(user_id=random_id)),
            2
        )

        self.assertNotEqual(
            LTIUser.objects.filter(user_id=random_id).first().consumer_guid,
            LTIUser.objects.filter(user_id=random_id).last().consumer_guid
        )

    def test_review_options_flag_works_for_during_and_after_attempt(self):
        """
        For the Reviewable offered and taken records
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt', 'afterDeadline', 'beforeDeadline', 'duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)
        # Can POST to create a new taken
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])

        # verify the "reviewWhetherCorrect" is True
        self.assertTrue(taken['reviewWhetherCorrect'])

        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        self.convert_user_to_bank_instructor(bank_id)

        self.delete_item(auth, taken_endpoint)
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set to only view correct after attempt
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            },
            "reviewOptions": {
                "whetherCorrect": {
                    "duringAttempt": False
                }
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt', 'afterDeadline', 'beforeDeadline', 'duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Can POST to create a new taken
        self.convert_user_to_bank_learner(bank_id)

        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'
        post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessment_offering_takens_endpoint)
        self.ok(req)
        taken = json.loads(req.content)
        taken_id = unquote(taken['id'])
        taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'

        # verify the "reviewWhetherCorrect" is False
        self.assertFalse(taken['reviewWhetherCorrect'])

        # now submitting an answer should let reviewWhetherCorrect be True
        right_response = {
            "integerValues": {
                "frontFaceValue": 1,
                "sideFaceValue": 2,
                "topFaceValue": 3
            }
        }

        finish_taken_endpoint = taken_endpoint + 'finish/'
        taken_questions_endpoint = taken_endpoint + 'questions/'
        question_1_endpoint = taken_questions_endpoint + question_1_id + '/'
        question_1_submit_endpoint = question_1_endpoint + 'submit/'

        post_sig = calculate_signature(auth, self.headers, 'POST', question_1_submit_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(question_1_submit_endpoint, right_response, format='json')
        self.ok(req)

        post_sig = calculate_signature(auth, self.headers, 'POST', finish_taken_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(finish_taken_endpoint)
        self.ok(req)

        get_sig = calculate_signature(auth, self.headers, 'GET', taken_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(taken_endpoint)
        self.ok(req)
        taken = json.loads(req.content)

        self.assertTrue(taken['reviewWhetherCorrect'])

    def test_can_update_review_options_flag(self):
        """
        For the Reviewable offered and taken records, can  change the reviewOptions
        flag on a created item
        :return:
        """
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to reviewOptions all true
        review_options = ['afterAttempt', 'afterDeadline', 'beforeDeadline', 'duringAttempt']
        for opt in review_options:
            self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

        payload = {
            "reviewOptions": {
                "whetherCorrect": {
                    "duringAttempt": False
                }
            }
        }
        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        offering_put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_offering_detail_endpoint)
        self.sign_client(offering_put_sig)
        req = self.client.put(assessment_offering_detail_endpoint, payload, format='json')

        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has duringAttempt = False
        review_options = ['afterAttempt', 'afterDeadline', 'beforeDeadline', 'duringAttempt']
        for opt in review_options:
            if opt == 'duringAttempt':
                self.assertFalse(offering['reviewOptions']['whetherCorrect'][opt])
            else:
                self.assertTrue(offering['reviewOptions']['whetherCorrect'][opt])

    def test_set_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

    def test_set_non_edx_item_learning_objectives_on_create(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'mc3-objective%3A21273%40MIT-OEIT'  # also test if this does NOT get re-quoted
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
            "learningObjectiveIds": [lo_test_id]
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            [lo_test_id]
        )

    def test_update_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        edx_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = edx_setup_data['bank_endpoint']
        item_details_endpoint = edx_setup_data['item_endpoint']
        los = edx_setup_data['learning_objectives']

        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)
        self.ok(req)
        item = self.load(req)

        for index, _id in enumerate(los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                quote(_id)
            )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds": new_los
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_details_endpoint, payload, format='json')
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )

    def test_update_non_edx_item_learning_objectives(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
            "learningObjectiveIds": los
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'
        get_sig = calculate_signature(auth, self.headers, 'GET', item_details_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(item_details_endpoint)

        self.ok(req)
        item = json.loads(req.content)
        self.assertEqual(
            item['learningObjectiveIds'],
            los
        )

        # now change the lo
        new_los = ["aNewLOId"]
        payload = {
            "learningObjectiveIds": new_los
        }
        put_sig = calculate_signature(auth, self.headers, 'PUT', item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_details_endpoint, payload, format='json')
        self.ok(req)
        item = self.load(req)
        for index, _id in enumerate(los):
            self.assertNotEqual(
                item['learningObjectiveIds'][index],
                _id
            )
        for index, _id in enumerate(new_los):
            self.assertEqual(
                item['learningObjectiveIds'][index],
                _id
            )

    def test_edx_item_query_learning_objective_id(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)

        query_setup_data = self.set_up_edx_item(auth)

        bank_endpoint = query_setup_data['bank_endpoint']
        item_details_endpoint = query_setup_data['item_endpoint']
        item_id = query_setup_data['item_id']
        los = query_setup_data['learning_objectives']
        query_endpoint = bank_endpoint + 'items/?learning_objective=' + los[0]  # can query with un-quote safed id
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/?learning_objective=wrongId'
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_non_edx_item_query_learning_objective_id(self):
        # not supported in their item records yet, so this should
        # return no results?
        # And yet it does work -- so there is something I do not
        # understand about query objects and items...
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        lo_test_id = 'myTestId'
        los = [lo_test_id]
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
            "learningObjectiveIds": los
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)
        item = json.loads(req.content)
        item_id = unquote(item['id'])

        item_details_endpoint = items_endpoint + item_id + '/'

        query_endpoint = bank_endpoint + 'items/?learning_objective=' + los[0]
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)

        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            item_id
        )

        # now test for no results
        query_endpoint = bank_endpoint + 'items/?learning_objective=wrongId'
        get_sig = calculate_signature(auth, self.headers, 'GET', query_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(query_endpoint)
        self.ok(req)
        data = json.loads(req.content)
        self.assertEqual(
            data['data']['count'],
            0
        )
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_set_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'
        self.delete_item(auth, assessment_offering_detail_endpoint)

        # try again, but set maxAttempts on create
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            },
            "maxAttempts": 2
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

    def test_can_update_max_attempts_on_assessment_offered(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # try again, but set maxAttempts on update
        offering_put_sig = calculate_signature(auth, self.headers, 'PUT', assessment_offering_detail_endpoint)
        self.sign_client(offering_put_sig)

        payload = {
            "maxAttempts": 2
        }

        self.sign_client(offering_put_sig)
        req = self.client.put(assessment_offering_detail_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)

        # verify that the offering has maxAttempts == 2
        self.assertEqual(offering['maxAttempts'],
                         2)

    def test_default_max_attempts_allows_infinite_attempts(self):
        # ok, don't really test to infinity, but test several
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            }
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts = None
        self.assertIsNone(offering['maxAttempts'])

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 25
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(assessment_offering_takens_endpoint)
            self.ok(req)
            taken = json.loads(req.content)
            taken_id = unquote(taken['id'])

            taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
            taken_endpoints.append(taken_endpoint)
            taken_finish_endpoint = taken_endpoint + 'finish/'
            post_sig = calculate_signature(auth, self.headers, 'POST', taken_finish_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(taken_finish_endpoint)
            self.ok(req)
            # finish the assessment taken, so next time we create one, it should
            # create a new one

    def test_max_attempts_throws_exception_if_taker_tries_to_exceed(self):
        auth = HTTPSignatureAuth(key_id=self.public_key,
                                 secret=self.private_key,
                                 algorithm='hmac-sha256',
                                 headers=self.signature_headers)
        bank = self._get_test_bank()
        bank_id = unquote(str(bank.ident))

        bank_endpoint = self.endpoint + 'assessment/banks/' + bank_id + '/'
        items_endpoint = bank_endpoint + 'items/'

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)

        # Use POST to create an item
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_string = 'can you manipulate this?'
        question_type = 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        answer_string = 'yes!'
        answer_type = 'answer-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU'
        payload = {
            "name": item_name,
            "description": item_desc,
            "question": {
                "type": question_type,
                "questionString": question_string
            },
            "answers": [{
                "type": answer_type,
                "integerValues": {
                    "frontFaceValue": 1,
                    "sideFaceValue": 2,
                    "topFaceValue": 3
                }
            }],
        }
        stringified_payload = deepcopy(payload)
        stringified_payload['manip'] = self.manip
        stringified_payload['frontView'] = self.front
        stringified_payload['sideView'] = self.side
        stringified_payload['topView'] = self.top
        stringified_payload['question'] = json.dumps(payload['question'])
        stringified_payload['answers'] = json.dumps(payload['answers'])

        post_sig = calculate_signature(auth, self.headers, 'POST', items_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(items_endpoint, stringified_payload)
        self.ok(req)

        item = json.loads(req.content)
        item_1_id = unquote(item['id'])
        question_1_id = unquote(self.extract_question(item)['id'])

        get_sig = calculate_signature(auth, self.headers, 'GET', items_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(items_endpoint)
        self.ok(req)

        # Now create an assessment, link the item to it,
        # and create an offering.
        # Use the offering_id to create the taken
        assessments_endpoint = bank_endpoint + 'assessments/'
        assessment_name = 'a really hard assessment'
        assessment_desc = 'meant to differentiate students'
        payload = {
            "name": assessment_name,
            "description": assessment_desc
        }
        post_sig = calculate_signature(auth, self.headers, 'POST', assessments_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(assessments_endpoint, payload, format='json')
        self.ok(req)
        assessment_id = unquote(json.loads(req.content)['id'])

        assessment_detail_endpoint = assessments_endpoint + assessment_id + '/'
        assessment_offering_endpoint = assessment_detail_endpoint + 'assessmentsoffered/'
        assessment_items_endpoint = assessment_detail_endpoint + 'items/'

        # POST should create the linkage
        payload = {
            'itemIds': [item_1_id]
        }
        link_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_items_endpoint)
        self.sign_client(link_post_sig)
        req = self.client.post(assessment_items_endpoint, payload, format='json')
        self.ok(req)

        # Use POST to create an offering
        offering_post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_endpoint)
        self.sign_client(offering_post_sig)

        payload = {
            "startTime": {
                "day": 1,
                "month": 1,
                "year": 2015
            },
            "duration": {
                "days": 2
            },
            "maxAttempts": 2
        }

        self.sign_client(offering_post_sig)
        req = self.client.post(assessment_offering_endpoint, payload, format='json')
        self.ok(req)
        offering = json.loads(req.content)
        offering_id = unquote(offering['id'])

        # verify that the offering has defaulted to maxAttempts == 2
        self.assertEquals(offering['maxAttempts'], 2)

        assessment_offering_detail_endpoint = bank_endpoint + 'assessmentsoffered/' + offering_id + '/'

        # Convert to a learner to test the rest of this
        self.convert_user_to_bank_learner(bank_id)

        num_attempts = 5
        # for deleting
        taken_endpoints = []
        assessment_offering_takens_endpoint = assessment_offering_detail_endpoint + 'assessmentstaken/'

        for attempt in range(0, num_attempts):
            # Can POST to create a new taken
            post_sig = calculate_signature(auth, self.headers, 'POST', assessment_offering_takens_endpoint)
            self.sign_client(post_sig)
            req = self.client.post(assessment_offering_takens_endpoint)
            if attempt >= payload['maxAttempts']:
                self.code(req, 403)
                self.message(req,
                             'Permission denied.',
                             True)
            else:
                self.ok(req)
                taken = json.loads(req.content)
                taken_id = unquote(taken['id'])

                taken_endpoint = bank_endpoint + 'assessmentstaken/' + taken_id + '/'
                taken_endpoints.append(taken_endpoint)
                taken_finish_endpoint = taken_endpoint + 'finish/'
                post_sig = calculate_signature(auth, self.headers, 'POST', taken_finish_endpoint)
                self.sign_client(post_sig)
                req = self.client.post(taken_finish_endpoint)
                self.ok(req)
                # finish the assessment taken, so next time we create one, it should
                # create a new one
