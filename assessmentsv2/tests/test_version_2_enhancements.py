# to test the authentication, need to follow the example in the README of
# https://github.com/etoccalino/django-rest-framework-httpsignature

# July 3, 2014, make this depend NOT on IS&T Membership, but rather Ortho3D hardcoded format
#  * Learner -> taaccct_student user
#  * Instructor -> taaccct_instructor user

import os
import json
import envoy
import string
import random
import requests

from django.conf import settings
from django.utils import unittest
from django.core.management import call_command

from rest_framework.test import APITestCase

from http_signature.requests_auth import HTTPSignatureAuth

from urllib import unquote, quote

from assessments_users.models import APIUser

from copy import deepcopy

# https://docs.djangoproject.com/en/1.6/topics/testing/tools/#overriding-settings
from django.test.utils import override_settings

from utilities.general import clean_id, get_session_data
from utilities.testing import configure_test_bucket, calculate_signature,\
    create_test_request, QBankBaseTest, add_user_authz_to_settings,\
    configure_proxyable_authz, remove_proxy_privileges

from dlkit.runtime.primordium import DataInputStream, Type
from dlkit.records.registry import ANSWER_GENUS_TYPES, ANSWER_RECORD_TYPES,\
    ITEM_RECORD_TYPES, QUESTION_RECORD_TYPES, ASSESSMENT_OFFERED_RECORD_TYPES,\
    ASSESSMENT_TAKEN_RECORD_TYPES, ITEM_GENUS_TYPES, ASSESSMENT_RECORD_TYPES

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

REVIEWABLE_OFFERED = Type(**ASSESSMENT_OFFERED_RECORD_TYPES['review-options'])
REVIEWABLE_TAKEN = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['review-options'])
UOC_PROBLEM_TYPE = Type(**ITEM_GENUS_TYPES['multi-file-submission'])
FILES_SUBMISSION_ANSWER_TYPE = Type(**ANSWER_RECORD_TYPES['files-submission'])
FILES_SUBMISSION_QUESTION_TYPE = Type(**QUESTION_RECORD_TYPES['files-submission'])
EDX_ITEM_RECORD_TYPE = Type(**ITEM_RECORD_TYPES['edx_item'])
SIMPLE_SEQUENCE_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])


def setUpModule():
    call_command(
        'loaddata',
        '{0}/fixtures/test_data.json'.format(ABS_PATH),
        verbosity=0
    )

def tearDownModule():
    call_command('flush', interactive=False, verbosity=0)


def delete_assessment_bank_and_contents(bank_id, user):
    from utilities.general import get_session_data
    from utilities.assessment import activate_managers
    from dlkit.runtime.primordium import Id

    test_request = create_test_request(user)
    activate_managers(test_request)
    am = get_session_data(test_request, 'am')
    bank = am.get_bank(Id(bank_id))

    while bank.get_assessments().available() > 0:
        for assessment in bank.get_assessments():
            while bank.get_assessments_offered_for_assessment(assessment.ident).available() > 0:
                for offered in bank.get_assessments_offered_for_assessment(assessment.ident):
                    while bank.get_assessments_taken_for_assessment_offered(offered.ident).available() > 0:
                        for taken in bank.get_assessments_taken_for_assessment_offered(offered.ident):
                            bank.delete_assessment_taken(taken.ident)
                    bank.delete_assessment_offered(offered.ident)
            bank.delete_assessment(assessment.ident)

    while bank.get_items().available() > 0:
        items = bank.get_items()
        for item in items:
            try:
                bank.delete_item(item.ident)
            except:
                pass

    am.delete_bank(Id(bank_id))


def delete_commenting_book_and_contents(book_id, user):
    from utilities.general import get_session_data
    from utilities.assessment import activate_managers
    from dlkit.runtime.primordium import Id

    test_request = create_test_request(user)
    activate_managers(test_request)
    cm = get_session_data(test_request, 'cm')
    book = cm.get_bank(Id(book_id))

    while book.get_comments().available() > 0:
        for comment in book.get_comments():
            book.delete_comment(comment.ident)

    cm.delete_bank(Id(book_id))


def random_name(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


class BaseTest(QBankBaseTest):

    def add_item(self, bank_id):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(UOC_PROBLEM_TYPE)
        form.add_text('so this is why', 'solution')
        new_item = bank.create_item(form)

        question_form = bank.get_question_form_for_create(new_item.ident, [FILES_SUBMISSION_QUESTION_TYPE])
        question_form.display_name = 'Question for ' + new_item.display_name.text
        question_form.description = ''
        new_question = bank.create_question(question_form)

        answer_form = bank.get_answer_form_for_create(new_item.ident, [FILES_SUBMISSION_ANSWER_TYPE])
        answer_form.display_name = 'Answer for ' + new_item.display_name.text
        new_answer = bank.create_answer(answer_form)

        return str(new_item.ident)

    def add_root_bank(self, bank_id):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        am.add_root_bank(clean_id(bank_id))

    def convert_user_to_bank_instructor(self):
        """
        for the bank with longName item_id, add user
        to the Membership as an instructor
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_bank_learner(self):
        """
        for the bank with longName item_id, add user
        to the Membership as a learner
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def convert_user_to_instructor(self):
        """
        Convert user to instructor in department,
        i.e. DepartmentAdmin
        """
        self._username = self._instructor
        self.reset_header_to_new_user()

    def convert_user_to_learner(self):
        """
        Convert user to a basic learner in the department,
        i.e. a DepartmentOfficer
        """
        self._username = self._learner
        self.reset_header_to_new_user()

    def create_taken_for_item(self, bank_id, item_id):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, clean_id(item_id))

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident,
                                                           [REVIEWABLE_OFFERED])
        new_offered = bank.create_assessment_offered(form)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident,
                                                         [REVIEWABLE_TAKEN])
        taken = bank.create_assessment_taken(form)
        return str(taken.ident), str(new_offered.ident), str(new_assessment.ident)

    def create_test_bank(self, _auth, _name, _desc, _form=False):
        """
        helper method to create a test assessment bank
        """
        test_endpoint = self.endpoint + 'assessment/banks/'
        sig = calculate_signature(_auth, self.headers, 'POST', test_endpoint)
        payload = {
            "name": _name,
            "description": _desc
        }
        self.sign_client(sig)
        if _form:
            req = self.client.post(test_endpoint, payload, format='multipart')
        else:
            req = self.client.post(test_endpoint, payload, format='json')
        # req.content should have the new bank item, with its ID (which
        # we will need to clean it up)
        self.ok(req)

        return unquote(self.json(req)['id'])

    def delete_item(self, _auth, _url):
        """
        helper method that sends a delete request to the url
        calculates the signature for you
        """
        delete_sig = calculate_signature(_auth, self.headers, 'DELETE', _url)
        self.sign_client(delete_sig)
        req = self.client.delete(_url)
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.content, '')

    def reset_all_headers(self):
        self.client.credentials()

    def reset_header_to_new_user(self):
        """
        When self._username changes, need to update the X-Api-Proxy in the header
        """
        self.headers['X-Api-Proxy'] = self._username

    def setUp(self):
        from django.contrib.auth.models import AnonymousUser
        from datetime import datetime
        from time import mktime
        from wsgiref.handlers import format_date_time

        super(BaseTest, self).setUp()

        self.anon = AnonymousUser()
        self._app_user = 'tester@mit.edu'
        self._instructor = 'instructor@mit.edu'
        self._learner = 'student@mit.edu'
        self._username = self._instructor
        self.app_user = APIUser.objects.get(username=self._app_user)
        self.user = APIUser.objects.get(username=self._username)
        self.public_key = str(self.app_user.public_key)
        self.private_key = str(self.app_user.private_key)
        self.signature_headers = ['request-line','accept','date','host','x-api-proxy']
        self.headers = {
            'Date'          : format_date_time(mktime(datetime.now().timetuple())),
            'Host'          : 'testserver:80',
            'Accept'        : 'application/json',
            'X-Api-Key'     : self.public_key,
            'X-Api-Proxy'   : self._username
        }
        self.auth = HTTPSignatureAuth(key_id=self.public_key,
                                      secret=self.private_key,
                                      algorithm='hmac-sha256',
                                      headers=self.signature_headers)

        self.json_headers = deepcopy(self.headers)
        self.json_headers.update({
            'Content-Type' : 'application/json'
        })

        self.endpoint = '/api/v2/'

        self.req = create_test_request(self.user)

        self._bank = self._get_test_bank()
        self._bank_id = unquote(str(self._bank.ident))

        # add_user_authz_to_settings('instructor',
        #                            self._app_user,
        #                            catalog_id=self._bank_id)

    def set_user(self, username):
        """
        """
        self.headers['X-Api-Proxy'] = username
        self.client.credentials(
            HTTP_X_API_PROXY=self.headers['X-Api-Proxy']
        )

    def sign_client(self, sig, opt_headers=None):
        self.client.credentials()
        if opt_headers:
            self.client.credentials(
                HTTP_ACCEPT=opt_headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=opt_headers['Date'],
                HTTP_HOST=opt_headers['Host'],
                HTTP_X_API_KEY=opt_headers['X-Api-Key'],
                HTTP_X_API_PROXY=opt_headers['X-Api-Proxy'],
                HTTP_LTI_BANK=opt_headers['LTI-Bank'],
                HTTP_LTI_USER_ID=opt_headers['LTI-User-ID'],
                HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID=opt_headers['LTI-Tool-Consumer-Instance-GUID'],
                HTTP_LTI_USER_ROLE=opt_headers['LTI-User-Role']
            )
        else:
            self.client.credentials(
                HTTP_ACCEPT=self.headers['Accept'],
                HTTP_AUTHORIZATION=sig,
                HTTP_DATE=self.headers['Date'],
                HTTP_HOST=self.headers['Host'],
                HTTP_X_API_KEY=self.headers['X-Api-Key'],
                HTTP_X_API_PROXY=self.headers['X-Api-Proxy']
            )

    def tearDown(self):
        # delete_assessment_bank_and_contents(self._bank_id, self.user)
        super(BaseTest, self).tearDown()


class AnswerTypeTests(BaseTest):
    def item_payload(self):
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = 'question-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'
        answer = 2
        answer_type = 'answer-record-type%3Amulti-choice-edx%40ODL.MIT.EDU'

        return {
            "name"                  : item_name,
            "description"           : item_desc,
            "question"              : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"               : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }

    def setUp(self):
        super(AnswerTypeTests, self).setUp()
        self.items = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(AnswerTypeTests, self).tearDown()

    def test_can_explicitly_set_right_answer(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['right-answer']))
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)
        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['right-answer']))
        )

    def test_default_answer_genus_is_right_answer_when_not_specified(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)
        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['right-answer']))
        )

    def test_can_set_wrong_answers_when_creating_answer(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)
        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        )

    def test_can_change_answer_genus_from_wrong_to_right(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            payload['answers'][0]['genus']
        )

        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])
        answer_details_endpoint = '{0}{1}/answers/{2}/'.format(self.items,
                                                               item_id,
                                                               answer_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', answer_details_endpoint)
        self.sign_client(put_sig)
        updated_payload = {
            'genus': str(Type(**ANSWER_GENUS_TYPES['right-answer']))
        }

        req = self.client.put(answer_details_endpoint,
                              updated_payload,
                              format='json')
        self.ok(req)
        updated_answer = self.json(req)
        self.assertEqual(
            updated_answer['genusTypeId'],
            updated_payload['genus']
        )

    def test_can_change_answer_genus_from_right_to_wrong(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['right-answer']))
        )

        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])
        answer_details_endpoint = '{0}{1}/answers/{2}/'.format(self.items,
                                                               item_id,
                                                               answer_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', answer_details_endpoint)
        self.sign_client(put_sig)
        updated_payload = {
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'bazz'
        }

        req = self.client.put(answer_details_endpoint,
                              updated_payload,
                              format='json')
        self.ok(req)
        updated_answer = self.json(req)
        self.assertEqual(
            updated_answer['genusTypeId'],
            updated_payload['genus']
        )
        self.assertEqual(
            updated_answer['feedback']['text'],
            updated_payload['feedback']
        )

    def test_can_edit_feedback_for_wrong_answers(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            payload['answers'][0]['genus']
        )

        self.assertEqual(item['answers'][0]['feedback']['text'], '')

        item_id = unquote(item['id'])
        answer_id = unquote(item['answers'][0]['id'])
        answer_details_endpoint = '{0}{1}/answers/{2}/'.format(self.items,
                                                               item_id,
                                                               answer_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', answer_details_endpoint)
        self.sign_client(put_sig)
        updated_payload = {
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'foo'
        }

        req = self.client.put(answer_details_endpoint,
                              updated_payload,
                              format='json')
        self.ok(req)
        updated_answer = self.json(req)
        self.assertEqual(
            updated_answer['genusTypeId'],
            updated_payload['genus']
        )
        self.assertEqual(
            updated_answer['feedback']['text'],
            updated_payload['feedback']
        )

    def test_can_add_wrong_answers_when_updating_item(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item_id = self.json(req)['id']

        new_answer_payload = {
            'answers': [{
                'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
                'type': payload['answers'][0]['type'],
                'choiceId': 1
            }]
        }

        item_details_endpoint = self.items + unquote(item_id) + '/'
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', item_details_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(item_details_endpoint,
                              new_answer_payload,
                              format='json')
        self.ok(req)
        updated_item = self.json(req)
        self.assertEqual(
            len(updated_item['answers']),
            2
        )

        new_answer = [a for a in updated_item['answers']
                      if a['genusTypeId'] == new_answer_payload['answers'][0]['genus']][0]
        self.assertEqual(
            new_answer['choiceIds'][0],
            updated_item['question']['choices'][0]['id']
        )

    def test_can_set_feedback_text_on_wrong_answer_on_item_create(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'foobar'
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)
        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        )
        self.assertEqual(
            item['answers'][0]['feedback']['text'],
            payload['answers'][0]['feedback']
        )

    def test_feedback_returned_when_student_submits_wrong_answer(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'what a novel idea'
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['answers'][0]['feedback']['text'],
            payload['answers'][0]['feedback']
        )

        item_id = item['id']
        wrong_choice_id = item['question']['choices'][1]['id']

        taken_id, offered_id, assessment_id = self.create_taken_for_item(self._bank_id, item_id)

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/{3}/submit/'.format(self.endpoint,
                                                                                          self._bank_id,
                                                                                          unquote(taken_id),
                                                                                          unquote(item_id))
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        wrong_answer_payload = {
            'choiceIds': [wrong_choice_id]
        }

        req = self.client.post(url,
                               data=wrong_answer_payload)

        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            payload['answers'][0]['feedback']
        )

    def test_solution_returned_when_student_submits_right_answer(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['solution'] = 'basket weaving'

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['texts']['solution']['text'],
            payload['solution']
        )
        item_id = item['id']
        right_choice_id = item['question']['choices'][1]['id']

        taken_id, offered_id, assessment_id = self.create_taken_for_item(self._bank_id, item_id)

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions'.format(self.endpoint,
                                                                              self._bank_id,
                                                                              unquote(taken_id))
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        question_id = data['data']['results'][0]['questions'][0]['id']

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/{3}/submit/'.format(self.endpoint,
                                                                                          self._bank_id,
                                                                                          unquote(taken_id),
                                                                                          unquote(question_id))
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        right_answer_payload = {
            'choiceIds': [right_choice_id]
        }

        req = self.client.post(url,
                               data=right_answer_payload)

        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])
        self.assertEqual(
            data['feedback']['text'],
            payload['solution']
        )

    def test_can_add_confused_learning_objectives_to_wrong_answer(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'foobar',
            'confusedLearningObjectiveIds': ['foo%3A1%40MIT', 'baz%3A2%40ODL']
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)
        self.assertEqual(
            item['answers'][0]['genusTypeId'],
            str(Type(**ANSWER_GENUS_TYPES['wrong-answer']))
        )
        self.assertEqual(
            item['answers'][0]['confusedLearningObjectiveIds'],
            payload['answers'][0]['confusedLearningObjectiveIds']
        )

    def test_wrong_answer_submission_returns_confused_los(self):
        post_sig = calculate_signature(self.auth, self.headers, 'POST', self.items)
        self.sign_client(post_sig)
        payload = self.item_payload()
        payload['answers'][0].update({
            'genus': str(Type(**ANSWER_GENUS_TYPES['wrong-answer'])),
            'feedback': 'what a novel idea',
            'confusedLearningObjectiveIds': ['foo%3A1%40MIT', 'baz%3A2%40ODL']
        })

        req = self.client.post(self.items,
                               payload,
                               format='json')
        self.ok(req)
        item = self.json(req)

        self.assertEqual(
            item['answers'][0]['confusedLearningObjectiveIds'],
            payload['answers'][0]['confusedLearningObjectiveIds']
        )

        item_id = item['id']
        wrong_choice_id = item['question']['choices'][1]['id']

        taken_id, offered_id, assessment_id = self.create_taken_for_item(self._bank_id, item_id)

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/{3}/submit/'.format(self.endpoint,
                                                                                          self._bank_id,
                                                                                          unquote(taken_id),
                                                                                          unquote(item_id))
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        wrong_answer_payload = {
            'choiceIds': [wrong_choice_id]
        }

        req = self.client.post(url,
                               data=wrong_answer_payload)

        self.ok(req)
        data = self.json(req)
        self.assertFalse(data['isCorrect'])
        self.assertEqual(
            data['confusedLearningObjectiveIds'],
            payload['answers'][0]['confusedLearningObjectiveIds']
        )


class APITest(BaseTest):
    def create_assessment_offered_for_item(self, bank_id, item_id):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(bank_id)
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, item_id)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        return new_offered

    def create_matching_repo(self, bank_id):
        from utilities.general import get_session_data, activate_managers

        activate_managers(self.req)
        rm = get_session_data(self.req, 'rm')
        return str(rm.get_repository(clean_id(bank_id)).ident)

    def create_resource_agent(self):
        from utilities.general import get_session_data, activate_managers

        activate_managers(self.req)
        resm = get_session_data(self.req, 'resm')

        bin = resm.get_bin(clean_id(self._bank_id))
        old_proxy = self.headers['X-Api-Proxy']
        self.headers['X-Api-Proxy'] = "MITx"
        files = {"avatar": open(ABS_PATH + '/tests/images/mitx_logo_rgb1.jpg', 'rb')}
        data = {"name": "MITx", "description": "a resource"}

        configure_proxyable_authz(self._app_user,
                                  catalog_id=bin.ident)
        add_user_authz_to_settings('student',
                                   'MITx')
        add_user_authz_to_settings('student',
                                   'MITx',
                                   catalog_id=bin.ident)

        url = self.endpoint + 'resource/bins/' + unquote(str(bin.ident)) + '/resources/'

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        req = self.client.post(url, data=data, files=files, headers=self.headers)

        self.headers['X-Api-Proxy'] = old_proxy
        return self.json(req)['id']

    def create_taken_for_item(self, bank_id, item_id):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)
        if isinstance(item_id, basestring):
            item_id = clean_id(item_id)

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(bank_id)

        new_offered = self.create_assessment_offered_for_item(bank_id, item_id)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return str(taken.ident), str(new_offered.ident)

    def no_results(self, req):
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

    def num_items(self, bank_id, val):
        from utilities.assessment import activate_managers
        from utilities.general import get_session_data

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')

        bank = am.get_bank(bank_id)
        self.assertEqual(
            bank.get_items().available(),
            val
        )

    def setUp(self):
        super(APITest, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id, self._offered_id = self.create_taken_for_item(self._bank_id, self._item_id)
        self.open_banks = [clean_id(self._bank_id).identifier]
        self._provider = self.create_resource_agent()

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(APITest, self).tearDown()

    def test_can_proxy_random_user_for_open_bank(self):
        self.reset_all_headers()
        random_username = random_name()
        self.set_user(random_username)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=self._bank_id)
        add_user_authz_to_settings('student',
                                   random_username)
        add_user_authz_to_settings('student',
                                   random_username,
                                   catalog_id=self._bank_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)

    def test_proxied_random_user_has_learner_role(self):
        self.reset_all_headers()
        random_username = random_name()

        configure_proxyable_authz(self._app_user,
                                  catalog_id=self._bank_id)
        add_user_authz_to_settings('student',
                                   random_username)
        add_user_authz_to_settings('student',
                                   random_username,
                                   catalog_id=self._bank_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/'
        self.set_user(random_username)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.code(req, 200)

        self.assertEqual(
            APIUser.objects.filter(username=random_username).count(),
            1
        )

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'

        self.set_user(random_username)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.no_results(req)

        self.assertEqual(
            APIUser.objects.filter(username=random_username).count(),
            1
        )

    def test_cannot_proxy_random_user_for_non_open_bank(self):
        self.reset_all_headers()
        random_username = random_name()

        remove_proxy_privileges(self._app_user)
        remove_proxy_privileges(self._app_user,
                                catalog_id=self._bank_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/'
        self.set_user(random_username)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.code(req, 403)

        self.assertEqual(
            APIUser.objects.filter(username=random_username).count(),
            0
        )

    def test_can_authenticate_instructor_for_open_bank(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)

    def test_proxied_random_user_gets_same_taken_each_time_if_not_finished(self):
        self.reset_all_headers()
        random_username = random_name()

        configure_proxyable_authz(self._app_user,
                                  catalog_id=self._bank_id)
        add_user_authz_to_settings('student',
                                   random_username)
        add_user_authz_to_settings('student',
                                   random_username,
                                   catalog_id=self._bank_id)

        offered = self.create_assessment_offered_for_item(self._bank_id, self._item_id)
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentsoffered/' + unquote(str(offered.ident)) + '/assessmentstaken/'

        self.set_user(random_username)

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url)

        self.ok(req)
        taken1 = self.json(req)
        taken1_id = taken1['id']

        req = self.client.post(url)
        self.ok(req)
        taken2 = self.json(req)
        taken2_id = taken2['id']

        self.assertEqual(
            taken1_id,
            taken2_id
        )

        url = '{0}assessment/banks/{1}/assessmentstaken/{2}/finish'.format(self.endpoint,
                                                                           self._bank_id,
                                                                           unquote(taken1_id))
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url)
        self.ok(req)

        url = '{0}assessment/banks/{1}/assessmentsoffered/{2}/assessmentstaken'.format(self.endpoint,
                                                                                       self._bank_id,
                                                                                       unquote(str(offered.ident)))

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url)

        self.ok(req)
        taken3 = self.json(req)
        taken3_id = taken3['id']

        self.assertNotEqual(
            taken1_id,
            taken3_id
        )

    def test_authenticated_user_can_proxy_random_user_for_non_open_bank_if_configured(self):
        self.reset_all_headers()
        remove_proxy_privileges(self._app_user)
        remove_proxy_privileges(self._app_user,
                                catalog_id=self._bank_id)

        random_username = random_name()
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/'
        headers = self.headers
        headers['X-Api-Proxy'] = random_username
        get_sig = calculate_signature(self.auth, headers, 'GET', url)
        self.client.credentials(
            HTTP_ACCEPT=headers['Accept'],
            HTTP_AUTHORIZATION=get_sig,
            HTTP_DATE=headers['Date'],
            HTTP_HOST=headers['Host'],
            HTTP_X_API_KEY=headers['X-Api-Key'],
            HTTP_X_API_PROXY=random_username
        )
        req = self.client.get(url)
        self.code(req, 403)

        self.assertEqual(
            APIUser.objects.filter(username=random_username).count(),
            0
        )

        random_username = random_name()

        configure_proxyable_authz(self._app_user)
        configure_proxyable_authz(self._app_user,
                                  catalog_id=self._bank_id)
        add_user_authz_to_settings('student',
                                   random_username)
        add_user_authz_to_settings('student',
                                   random_username,
                                   catalog_id=self._bank_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/'
        headers = self.headers
        headers['X-Api-Proxy'] = random_username
        get_sig = calculate_signature(self.auth, headers, 'GET', url)
        self.client.credentials(
            HTTP_ACCEPT=headers['Accept'],
            HTTP_AUTHORIZATION=get_sig,
            HTTP_DATE=headers['Date'],
            HTTP_HOST=headers['Host'],
            HTTP_X_API_KEY=headers['X-Api-Key'],
            HTTP_X_API_PROXY=random_username
        )
        req = self.client.get(url)
        self.ok(req)

        self.assertEqual(
            APIUser.objects.filter(username=random_username).count(),
            1
        )

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        get_sig = calculate_signature(self.auth, headers, 'GET', url)
        self.client.credentials(
            HTTP_ACCEPT=headers['Accept'],
            HTTP_AUTHORIZATION=get_sig,
            HTTP_DATE=headers['Date'],
            HTTP_HOST=headers['Host'],
            HTTP_X_API_KEY=headers['X-Api-Key'],
            HTTP_X_API_PROXY=random_username
        )
        req = self.client.get(url)
        self.num_items(clean_id(self._bank_id), 1)
        self.no_results(req)

    def test_can_get_item_without_bank_id(self):
        url = self.endpoint + 'assessment/items/' + unquote(self._item_id) + '/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['id'],
            self._item_id
        )

    def test_can_delete_item_without_bank_id(self):
        new_item_id = self.add_item(self._bank_id)
        url = self.endpoint + 'assessment/items/' + unquote(new_item_id) + '/'

        self.headers['X-Api-Proxy'] = 'luwenh@mit.edu'

        add_user_authz_to_settings('instructor',
                                   'luwenh@mit.edu')
        add_user_authz_to_settings('instructor',
                                   'luwenh@mit.edu',
                                   catalog_id=self._bank_id)

        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)
        req = self.client.delete(url)
        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.code(req, 500)
        self.message(req,
                     'Object not found.')

    def test_can_update_item_without_bank_id(self):
        url = self.endpoint + 'assessment/items/' + unquote(self._item_id) + '/'

        test_cases = [
            {'learningObjectiveIds': ['foo%3A1%40baz.MIT']},
            {'name': 'update test name'},
            {'description': 'update test description'}
        ]

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)
        for payload in test_cases:
            req = self.client.put(url,
                                  data=payload,
                                  format='json')
            self.ok(req)
            data = self.json(req)
            key = payload.keys()[0]
            if key == 'learningObjectiveIds':
                self.assertEqual(
                    data[key],
                    payload[key]
                )
            elif key == 'name':
                self.assertEqual(
                    data['displayName']['text'],
                    payload[key]
                )
            else:
                self.assertEqual(
                    data['description']['text'],
                    payload[key]
                )

    def test_can_get_assessment_offered_without_bank_id(self):
        url = self.endpoint + 'assessment/assessmentsoffered/' + unquote(self._offered_id) + '/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['id'],
            self._offered_id
        )

    def test_can_delete_assessment_offered_without_bank_id(self):
        new_offered = self.create_assessment_offered_for_item(self._bank_id, self._item_id)
        new_offered_id = str(new_offered.ident)
        url = self.endpoint + 'assessment/assessmentsoffered/' + unquote(new_offered_id) + '/'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)
        req = self.client.delete(url)
        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.code(req, 500)
        self.message(req,
                     'Object not found.')

    def test_can_update_assessment_offered_without_bank_id(self):
        url = self.endpoint + 'assessment/assessmentsoffered/' + unquote(self._offered_id) + '/'

        test_cases = [
            {'startTime': {
                'year': 2015,
                'month': 12,
                'day': 31
            }},
            {'gradeSystem': 'foo%3A1%40baz.edu'}
        ]

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)
        for payload in test_cases:
            req = self.client.put(url,
                                  data=payload,
                                  format='json')
            self.ok(req)
            data = self.json(req)
            key = payload.keys()[0]
            if key == 'startTime':
                for attr in ['year', 'month', 'day']:
                    self.assertEqual(
                        data[key][attr],
                        payload[key][attr]
                    )
            else:
                self.assertEqual(
                    data[key + 'Id'],
                    payload[key]
                )

    def test_can_set_item_provider_as_enclosed_asset_in_repository(self):
        repo_id = self.create_matching_repo(self._bank_id)
        url = self.endpoint + 'repository/repositories/' + unquote(repo_id) + '/assets/'

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        payload = {
            "enclosedObjectId": self._item_id,
            "providerId": self._provider
        }
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url,
                               data=payload,
                               format='json')
        self.code(req, 201)
        data = self.json(req)
        self.assertEqual(
            data['providerId'],
            self._provider
        )

    def test_can_set_item_provider_on_create(self):
        edx_mc_q = Type(**QUESTION_RECORD_TYPES['multi-choice-edx'])
        edx_mc_a = Type(**ANSWER_RECORD_TYPES['multi-choice-edx'])

        repo_id = self.create_matching_repo(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = str(edx_mc_q)
        answer = 2
        answer_type = str(edx_mc_a)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "providerId"    : self._provider,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(url,
                               data=payload,
                               format='json')
        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['providerId'],
            self._provider
        )

    def test_can_update_item_provider(self):
        repo_id = self.create_matching_repo(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/' + unquote(self._item_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)
        payload = {
            "providerId"    : self._provider,
        }
        req = self.client.put(url,
                              data=payload,
                              format='json')

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['providerId'],
            self._provider
        )

    def test_provider_id_in_item_details(self):
        repo_id = self.create_matching_repo(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/' + unquote(self._item_id)
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)
        payload = {
            "providerId"    : self._provider,
        }
        req = self.client.put(url,
                              data=payload,
                              format='json')

        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['providerId'],
            self._provider
        )

    def test_no_provider_id_in_item_details_if_not_set(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/' + unquote(self._item_id)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertNotIn(
            'providerId',
            data
        )

    def test_provider_id_in_question_details(self):
        repo_id = self.create_matching_repo(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        edx_mc_q = Type(**QUESTION_RECORD_TYPES['multi-choice-edx'])
        edx_mc_a = Type(**ANSWER_RECORD_TYPES['multi-choice-edx'])

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = str(edx_mc_q)
        answer = 2
        answer_type = str(edx_mc_a)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "providerId"    : self._provider,
            "question"      : {
                "type"           : question_type,
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(url,
                               data=payload,
                               format='json')

        self.ok(req)
        item_map = self.json(req)
        url += unquote(item_map['id'])
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['question']['providerId'],
            self._provider
        )

    def test_multiple_choice_questions_are_randomized_if_flag_set(self):
        repo_id = self.create_matching_repo(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=repo_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=repo_id)

        edx_mc_q = Type(**QUESTION_RECORD_TYPES['multi-choice-edx'])
        edx_mc_a = Type(**ANSWER_RECORD_TYPES['multi-choice-edx'])

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        item_name = 'a really complicated item'
        item_desc = 'meant to differentiate students'
        question_body = 'can you manipulate this?'
        question_choices = [
            'yes',
            'no',
            'maybe'
        ]
        question_type = str(edx_mc_q)
        answer = 2
        answer_type = str(edx_mc_a)
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        payload = {
            "name"          : item_name,
            "description"   : item_desc,
            "providerId"    : self._provider,
            "question"      : {
                "type"           : question_type,
                "rerandomize"    : "always",
                "questionString" : question_body,
                "choices"        : question_choices
            },
            "answers"       : [{
                "type"      : answer_type,
                "choiceId"  : answer
            }],
        }
        req = self.client.post(url,
                               data=payload,
                               format='json')

        self.ok(req)
        item = self.json(req)
        taken_id, offered_id = self.create_taken_for_item(self._bank_id, item['id'])
        taken_url = '{0}assessment/banks/{1}/assessmentstaken/{2}/questions/'.format(self.endpoint,
                                                                                     self._bank_id,
                                                                                     unquote(taken_id))
        get_sig = calculate_signature(self.auth, self.headers, 'GET', taken_url)
        self.sign_client(get_sig)
        req = self.client.get(taken_url)
        self.ok(req)
        data = self.json(req)
        order_1 = data['data']['results'][0]['questions'][0]['choices']

        num_same = 0
        for i in range(0, 10):
            req2 = self.client.get(taken_url)
            self.ok(req)
            data2 = self.json(req2)
            order_2 = data2['data']['results'][0]['questions'][0]['choices']

            if order_1 == order_2:
                num_same += 1

        self.assertFalse(num_same == 10)


class HierarchyTests(BaseTest):
    def num_banks(self, val):
        am = get_session_data(self.req, 'am')
        self.assertEqual(
            am.banks.available(),
            val
        )

    def query_node_hierarchy(self, bank_id, query_type='ancestors', value=1, expected_ids=()):
        query_url = '{0}assessment/hierarchies/nodes/{1}?{2}={3}'.format(self.endpoint,
                                                                         bank_id,
                                                                         query_type,
                                                                         value)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', query_url)
        self.sign_client(get_sig)
        req = self.client.get(query_url)
        self.ok(req)
        data = self.json(req)
        if query_type == 'ancestors':
            key = 'parentNodes'
        else:
            key = 'childNodes'
        self.assertEqual(
            len(data[key]),
            len(expected_ids)
        )
        if len(expected_ids) > 0:
            self.assertEqual(
                data[key][0]['id'],
                expected_ids[0]
            )

    def setUp(self):
        super(HierarchyTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]
        self.open_banks = [self._bank_id]

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(HierarchyTests, self).tearDown()

    def test_can_add_root_bank_to_hierarchy(self):
        self.num_banks(1)
        url = self.endpoint + 'assessment/hierarchies/roots/'

        payload = {
            'id'    : self._bank_id
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)
        self.num_banks(1)

    @unittest.skip('unimplemented test')
    def test_adding_duplicate_root_bank_to_hierarchy_throws_exception(self):
        self.fail('finish writing the test')

    def test_can_remove_root_bank_from_hierarchy(self):
        self.num_banks(1)
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/roots/' + self._bank_id + '/'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)
        req = self.client.delete(url)
        self.code(req, 204)
        self.num_banks(1)

    def test_removing_non_root_bank_from_hierarchy_throws_exception(self):
        self.num_banks(1)
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.num_banks(2)
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/roots/' + second_bank_id + '/'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)
        req = self.client.delete(url)
        self.code(req, 406)
        self.num_banks(2)
        # delete_assessment_bank_and_contents(second_bank_id, self.user)

    def test_unauthenticated_users_cannot_get_hierarchies(self):
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/roots/'
        req = self.client.get(url)
        self.code(req, 403)

    def test_can_get_root_bank_list(self):
        envoy.run('mongo test_qbank_relationship --eval "db.dropDatabase()"')
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/roots/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )

    def test_can_get_root_bank_details(self):
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/roots/' + self._bank_id + '/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            unquote(data['id']),
            self._bank_id
        )

    def test_getting_details_for_non_root_bank_throws_exception(self):
        second_bank_id = self.create_test_bank(self.auth,
                                              'another test bank',
                                              'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/roots/' + second_bank_id + '/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.code(req, 406)
        # delete_assessment_bank_and_contents(second_bank_id, self.user)

    def test_can_get_children_banks(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            second_bank_id
        )

    def test_can_get_parent_banks(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )

    def test_trying_to_add_child_to_non_root_bank_works(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [self._bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )

    def test_trying_to_add_parent_to_non_root_bank_works(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [self._bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )
        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )

    def test_trying_to_add_non_existent_child_to_node_throws_exception(self):
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : ['assessment.Bank%3A1234567890abcdefabcdef12%40bazzim.MIT.EDU']
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 500)

    def test_trying_to_add_non_existent_parent_to_node_throws_exception(self):
        self.add_root_bank(self._bank_id)
        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : ['assessment.Bank%3A1234567890abcdefabcdef12%40bazzim.MIT.EDU']
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 500)

    def test_child_id_required_to_add_child_bank(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'oops'   : second_bank_id
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 500)

    def test_parent_id_required_to_add_parent_bank(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'oops'   : self._bank_id
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 500)

    def test_can_add_child_bank_to_node(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

    def test_can_add_parent_bank_to_node(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [self._bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

    @unittest.skip('unimplemented test')
    def test_bad_root_id_in_child_detail_throws_exception(self):
        self.fail("finish writing the test")

    @unittest.skip('unimplemented test')
    def test_bad_child_id_in_child_detail_throws_exception(self):
        self.fail('finish writing the test')

    def test_can_remove_child_from_node(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        payload = {
            'ids': []
        }

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)

        req = self.client.put(url,
                              data=payload,
                              format='json')
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_remove_parents_with_put(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [self._bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        payload = {
            'ids': []
        }

        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)

        req = self.client.put(url,
                              data=payload,
                              format='json')
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_remove_parent_from_node(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/parents/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [self._bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        payload = {
            'ids': [self._bank_id]
        }

        delete_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(delete_sig)

        req = self.client.delete(url,
                                 data=payload,
                                 format='json')
        self.code(req, 204)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_add_child_with_post(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        third_bank_id = self.create_test_bank(self.auth,
                                              'a third test bank',
                                              'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        payload = {
            'ids': [third_bank_id]
        }

        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        req = self.client.post(url,
                               data=payload,
                               format='json')
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            2
        )

    def test_can_delete_child_nodes(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        payload = {
            'ids': [second_bank_id]
        }

        delete_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(delete_sig)

        req = self.client.delete(url,
                                 data=payload,
                                 format='json')
        self.code(req, 204)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    @unittest.skip('unimplemented test')
    def test_trying_to_remove_non_child_from_root_throws_exception(self):
        self.fail('finish writing the test')

    def test_can_get_ancestor_and_descendant_levels(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'another test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        self.query_node_hierarchy(second_bank_id, 'ancestors', 1, [quote(self._bank_id)])
        self.query_node_hierarchy(self._bank_id, 'ancestors', 1, [])
        self.query_node_hierarchy(second_bank_id, 'descendants', 1, [])
        self.query_node_hierarchy(self._bank_id, 'descendants', 1, [quote(second_bank_id)])

    def test_can_query_hierarchy_nodes_with_descendants(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'a child test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        third_bank_id = self.create_test_bank(self.auth,
                                               'another child test bank',
                                               'is here!')

        configure_proxyable_authz(self._app_user,
                                  catalog_id=third_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=third_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [third_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children?descendants=2'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)['data']['results'][0]
        self.assertTrue(len(data['childNodes']) == 1)
        self.assertTrue(unquote(data['id']) == second_bank_id)
        self.assertTrue(unquote(data['childNodes'][0]['id']) == third_bank_id)

    def test_can_query_hierarchy_nodes_with_ancestors(self):
        second_bank_id = self.create_test_bank(self.auth,
                                               'a child test bank',
                                               'is here!')
        self.add_root_bank(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=second_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=second_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + self._bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [second_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        third_bank_id = self.create_test_bank(self.auth,
                                              'another child test bank',
                                              'is here!')

        configure_proxyable_authz(self._app_user,
                                  catalog_id=third_bank_id)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=third_bank_id)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + second_bank_id + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [third_bank_id]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = self.endpoint + 'assessment/hierarchies/nodes/' + third_bank_id + '/parents?ancestors=2'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)['data']['results'][0]
        self.assertTrue(len(data['parentNodes']) == 1)
        self.assertTrue(unquote(data['id']) == second_bank_id)
        self.assertTrue(unquote(data['parentNodes'][0]['id']) == self._bank_id)


class StudentResponseTests(BaseTest):
    def setUp(self):
        super(StudentResponseTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id, self._offered_id, assessment_id = self.create_taken_for_item(self._bank_id, self._item_id)
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(StudentResponseTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    @unittest.skip('unimplemented test')
    def test_can_get_all_past_responses_to_a_question(self):
        # not currently supported in dlkit
        self.fail('finish writing the test')

    def test_can_view_the_last_response_to_a_question(self):
        # submit an answer to the item...
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/submit/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'my_solution_1' : self._file_response_1,
            'my_solution_2' : self._file_response_2,
            'type'          : 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
        }

        req = self.client.post(url,
                               data=payload)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/responses/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        # self.assertNotIn(
        #     'files',
        #     data
        # )

        self.assertIn(
            'my_solution_1',
            data['fileIds']
        )

        self.assertIn(
            'my_solution_2',
            data['fileIds']
        )

        # with files by default, until an actual LIST is returned
        # url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id.replace('.Item','.Question')) + '/responses/?files'
        # get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        # self.sign_client(get_sig)
        # req = self.client.get(url)
        # self.ok(req)
        # data = self.json(req)
        self.assertIn(
            'files',
            data
        )

        self.assertIn(
            'my_solution_1',
            data['files']
        )

        self.assertIn(
            'my_solution_2',
            data['files']
        )

        self.is_cloudfront_url(data['files']['my_solution_1'])
        self.is_cloudfront_url(data['files']['my_solution_2'])

    def test_can_submit_response_with_multiple_files(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/submit/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'my_solution_1' : self._file_response_1,
            'my_solution_2' : self._file_response_2,
            'type'          : 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
        }

        req = self.client.post(url,
                               data=payload)

        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['isCorrect'])

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/responses/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        data = self.json(req)
        self._file_response_1.seek(0)
        self._file_response_2.seek(0)
        for answer_file, answer_url in data['files'].iteritems():
            self.is_cloudfront_url(answer_url)

            file_data = requests.get(answer_url)
            self.assertEqual(
                file_data.content,
                payload[answer_file].read()
            )

    def test_student_can_surrender(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/surrender/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        req = self.client.post(url)
        self.ok(req)
        feedback = self.json(req)

        # this should be present because there is an item "solution" now
        self.assertIsNotNone(feedback['feedback'])

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/responses/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['displayName']['text'],
            'I surrendered'
        )

    def test_cannot_get_question_solution_without_answering(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/solution/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        self.assertEqual(
            req.content,
            '"No solution available."'
        )

    def test_can_get_question_solution_after_answering(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        question_id = data['data']['results'][0]['questions'][0]['id']

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(question_id) + '/submit/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'my_solution_1' : self._file_response_1,
            'type'          : 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
        }

        req = self.client.post(url,
                               data=payload)

        self.ok(req)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(question_id) + '/solution/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertIn('answers', data.keys())
        self.assertIn('explanation', data.keys())
        self.assertIn(
            'so this is why',
            data['explanation']['text']
        )

    def test_can_configure_review_solution(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentsoffered/' + unquote(self._offered_id) + '/'
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)

        payload = {
            'reviewOptions' : {
                'solution': {
                    'duringAttempt': True
                }
            }
        }

        req = self.client.put(url,
                              data=payload,
                              format='json')

        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertTrue(data['reviewOptions']['solution']['duringAttempt'])


class UOCProblemCreationTests(BaseTest):
    def setUp(self):
        super(UOCProblemCreationTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(UOCProblemCreationTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_can_create_file_submission_question(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)
        payload = {
            'question'      : {
                'questionString'    : 'Please upload the right files',
                'type'              : 'question-record-type%3Afiles-submission%40ODL.MIT.EDU'
            },
            'name'          : 'my UOC test item',
            'description'   : 'a great item',
            'answers'       : [{
                'type'      : 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
            }]
        }

        req = self.client.post(url,
                               data=payload,
                               format='json')

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            data['question']['text']['text'],
            'Please upload the right files'
        )

        self.assertEqual(
            data['question']['recordTypeIds'][0],
            'question-record-type%3Afiles-submission%40ODL.MIT.EDU'
        )

        self.assertEqual(
            data['answers'][0]['recordTypeIds'][0],
            'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
        )

        self.assertEqual(
            data['displayName']['text'],
            'my UOC test item'
        )

        self.assertEqual(
            data['description']['text'],
            'a great item'
        )


class CommentingTests(BaseTest):
    def create_matching_book(self, bank_id):
        from utilities.general import get_session_data, activate_managers

        if isinstance(bank_id, basestring):
            bank_id = clean_id(bank_id)

        activate_managers(self.req)
        cm = get_session_data(self.req, 'cm')
        return cm.get_book(bank_id)

    def create_test_book(self, _auth, _name, _desc, _form=False):
        """
        helper method to create a test book
        """
        test_endpoint = self.endpoint + 'commenting/books/'
        sig = calculate_signature(_auth, self.headers, 'POST', test_endpoint)
        payload = {
            "name": _name,
            "description": _desc
        }
        self.sign_client(sig)
        if _form:
            req = self.client.post(test_endpoint, payload, format='multipart')
        else:
            req = self.client.post(test_endpoint, payload, format='json')
        # req.content should have the new bank item, with its ID (which
        # we will need to clean it up)
        self.code(req, 201)

        return unquote(self.json(req)['id'])

    def send_comment(self, with_files=False):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'text'          : 'Good job',
        }

        if with_files:
            payload.update({
                'commentFile'   : self._file_response_2
            })

        req = self.client.post(url,
                               data=payload)

        return payload

    def setUp(self):
        super(CommentingTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')

        self.student_submits_response()

        self.book = self.create_matching_book(self._bank_id)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=self.book.ident)
        add_user_authz_to_settings('instructor',
                                   self._instructor,
                                   catalog_id=self.book.ident)

    def student_submits_response(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/submit/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'my_solution_1' : self._file_response_1,
            'type'          : 'answer-record-type%3Afiles-submission%40ODL.MIT.EDU'
        }

        req = self.client.post(url,
                               data=payload)

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(CommentingTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_can_add_comment_to_response(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'text'          : 'Good job',
            'commentFile'   : self._file_response_2
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)
        data = self.json(req)

        self.assertEqual(
            data['text']['text'],
            payload['text']
        )

        self.assertIn(
            'file',
            data
        )

        self.is_cloudfront_url(data['file'])

    def test_exception_thrown_if_text_attribute_not_sent_on_comment(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'commentFile'   : self._file_response_2
        }

        req = self.client.post(url,
                               data=payload)

        self.code(req, 500)
        self.message(req, '"text" required in input parameters but not provided.')

    def test_can_send_text_only_comment(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'text'   : 'A+'
        }

        req = self.client.post(url,
                               data=payload)

        self.code(req, 201)
        data = self.json(req)
        self.assertEqual(
            data['text']['text'],
            payload['text']
        )

        self.assertNotIn(
            'file',
            data
        )

    def test_can_get_all_comments_for_response(self):
        payload = self.send_comment(with_files=True)
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['text']['text'],
            payload['text']
        )

        self.assertNotIn(
            'file',
            data
        )

    def test_can_get_all_comments_with_files_for_response(self):
        payload = self.send_comment(with_files=True)
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/?files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['text']['text'],
            payload['text']
        )

        self.assertIn(
            'file',
            data['data']['results'][0]
        )

        self.is_cloudfront_url(data['data']['results'][0]['file'])

    def test_learners_can_view_comments(self):
        payload = self.send_comment(with_files=True)

        self.convert_user_to_learner()

        add_user_authz_to_settings('student',
                                   self._learner,
                                   catalog_id=self.book.ident)

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(self._item_id) + '/comments/?files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.ok(req)
        data = self.json(req)

        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['text']['text'],
            payload['text']
        )

        self.assertIn(
            'file',
            data['data']['results'][0]
        )

        self.is_cloudfront_url(data['data']['results'][0]['file'])

        self.convert_user_to_instructor()

    def test_exception_thrown_on_create_if_no_responses_yet(self):
        # need to create a new taken so the response in setUp doesn't get logged
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        question_id = data['data']['results'][0]['questions'][0]['id']

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(question_id) + '/comments/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'text'   : 'A+'
        }

        req = self.client.post(url,
                               data=payload)

        self.code(req, 500)
        self.message(req, 'this Item has not been attempted')

    def test_exception_thrown_on_get_if_no_responses_yet(self):
        # need to create a new taken so the response in setUp doesn't get logged
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        question_id = data['data']['results'][0]['questions'][0]['id']

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + unquote(self._taken_id) + '/questions/' + unquote(question_id) + '/comments/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)

        self.code(req, 500)
        self.message(req, 'this Item has not been attempted')


class BankColorTests(BaseTest):
    def setUp(self):
        super(BankColorTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(BankColorTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_can_create_bank_with_rgb_color_value(self):
        payload = {
            'name'          : 'a test bank',
            'description'   : 'for testing purposes only',
            'color'         : '0x00ff00'
        }

        banks_endpoint = self.endpoint + 'assessment/banks/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', banks_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(banks_endpoint, payload)
        self.ok(req)
        bank_obj = self.json(req)
        bank_id = unquote(bank_obj['id'])

        self.assertIn(
            'colorCoordinate',
            bank_obj
        )
        self.assertIn(
            'hexValue',
            bank_obj['colorCoordinate']
        )

        self.assertEqual(
            bank_obj['colorCoordinate']['hexValue'],
            payload['color'].replace('0x', '')
        )

        bank_endpoint = banks_endpoint + bank_id + '/'

        self.assertRaises(AssertionError,
                          self.delete_item,
                          self.auth,
                          bank_endpoint)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=bank_id)
        add_user_authz_to_settings('instructor',
                                   self._username,
                                   catalog_id=bank_id)
        self.delete_item(self.auth, bank_endpoint)

    def test_can_get_bank_with_rgb_color_value(self):
        payload = {
            'name'          : 'a test bank',
            'description'   : 'for testing purposes only',
            'color'         : '0x00ff00'
        }
        banks_endpoint = self.endpoint + 'assessment/banks/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', banks_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(banks_endpoint, payload)
        self.ok(req)

        bank_obj = self.json(req)
        bank_id = unquote(bank_obj['id'])
        bank_endpoint = banks_endpoint + bank_id + '/'

        get_sig = calculate_signature(self.auth, self.headers, 'GET', bank_endpoint)
        self.sign_client(get_sig)
        req = self.client.get(bank_endpoint)
        self.code(req, 403)

        configure_proxyable_authz(self._app_user,
                                  catalog_id=bank_id)
        add_user_authz_to_settings('instructor',
                                   self._username,
                                   catalog_id=bank_id)

        req = self.client.get(bank_endpoint)
        self.ok(req)
        bank_obj = self.json(req)

        self.assertIn(
            'colorCoordinate',
            bank_obj
        )
        self.assertIn(
            'hexValue',
            bank_obj['colorCoordinate']
        )

        self.assertEqual(
            bank_obj['colorCoordinate']['hexValue'],
            payload['color'].replace('0x', '')
        )

        self.delete_item(self.auth, bank_endpoint)


    def test_can_update_bank_with_rgb_color_value(self):
        payload = {
            'name'          : 'a test bank',
            'description'   : 'for testing purposes only',
            'color'         : '0x00ff00'
        }
        banks_endpoint = self.endpoint + 'assessment/banks/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', banks_endpoint)
        self.sign_client(post_sig)
        req = self.client.post(banks_endpoint, payload)
        self.ok(req)

        bank_obj = self.json(req)
        bank_id = unquote(bank_obj['id'])
        bank_endpoint = banks_endpoint + bank_id + '/'

        add_user_authz_to_settings(self._username, bank_id)

        second_payload = {
            'color'     : '0000ff'
        }

        configure_proxyable_authz(self._app_user,
                                  catalog_id=bank_id)
        add_user_authz_to_settings('instructor',
                                   self._username,
                                   catalog_id=bank_id)

        put_sig = calculate_signature(self.auth, self.json_headers, 'PUT', bank_endpoint)
        self.sign_client(put_sig)
        req = self.client.put(bank_endpoint, second_payload, format='json')
        self.ok(req)
        bank_obj = self.json(req)

        self.assertIn(
            'colorCoordinate',
            bank_obj
        )
        self.assertIn(
            'hexValue',
            bank_obj['colorCoordinate']
        )

        self.assertEqual(
            bank_obj['colorCoordinate']['hexValue'],
            second_payload['color'].replace('0x', '')
        )

        self.assertNotEqual(
            bank_obj['colorCoordinate']['hexValue'],
            payload['color'].replace('0x', '')
        )

        self.delete_item(self.auth, bank_endpoint)


class QuestionLOTests(BaseTest):
    def add_item(self, bank_id):
        from utilities.general import get_session_data, clean_id
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_item_form_for_create([])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.set_genus_type(UOC_PROBLEM_TYPE)

        self._lo = 'foo%40bar%3Abaz'

        form.set_learning_objectives([clean_id(self._lo)])
        new_item = bank.create_item(form)

        question_form = bank.get_question_form_for_create(new_item.ident, [FILES_SUBMISSION_QUESTION_TYPE])
        question_form.display_name = 'Question for ' + new_item.display_name.text
        question_form.description = ''
        new_question = bank.create_question(question_form)

        answer_form = bank.get_answer_form_for_create(new_item.ident, [FILES_SUBMISSION_ANSWER_TYPE])
        answer_form.display_name = 'Answer for ' + new_item.display_name.text
        new_answer = bank.create_answer(answer_form)

        return str(new_item.ident)

    def setUp(self):
        super(QuestionLOTests, self).setUp()
        self._item_id = self.add_item(self._bank_id)
        self._taken_id = self.create_taken_for_item(self._bank_id, self._item_id)[0]
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(QuestionLOTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_question_object_map_has_item_learning_objective_ids(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessmentstaken/' + \
              unquote(self._taken_id) + '/questions/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results'][0]['questions']),
            1
        )
        self.assertEqual(
            data['data']['results'][0]['questions'][0]['learningObjectiveIds'],
            [self._lo]
        )


class BulkItemAssignmentTests(BaseTest):
    def create_assessment_for_items(self, bank_id, item_ids):
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        for item_id in item_ids:
            bank.add_item(new_assessment.ident, clean_id(item_id))

        return str(new_assessment.ident)

    def setUp(self):
        super(BulkItemAssignmentTests, self).setUp()

        self._item1_id = self.add_item(self._bank_id)
        self._item2_id = self.add_item(self._bank_id)
        self._item3_id = self.add_item(self._bank_id)
        self._assessment_id = self.create_assessment_for_items(self._bank_id,
                                                               [self._item1_id, self._item2_id])
        self.open_banks = [self._bank_id]

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(BulkItemAssignmentTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_put_allows_for_bulk_item_assessment_assignment(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/' + \
              unquote(self._assessment_id) + '/items/'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)

        item_ids = [i['id'] for i in data['data']['results']]

        self.assertEqual(
            len(item_ids),
            2
        )
        self.assertIn(
            self._item1_id,
            item_ids
        )
        self.assertIn(
            self._item2_id,
            item_ids
        )
        self.assertNotIn(
            self._item3_id,
            item_ids
        )

        payload = {
            'itemIds': [self._item3_id]
        }
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/' + \
              unquote(self._assessment_id) + '/items/'
        put_sig = calculate_signature(self.auth, self.headers, 'PUT', url)
        self.sign_client(put_sig)

        req = self.client.put(url, payload, format='json')
        self.code(req, 202)
        data = self.json(req)

        item_ids = [i['id'] for i in data['data']['results']]

        self.assertEqual(
            len(item_ids),
            1
        )

        self.assertNotIn(
            self._item1_id,
            item_ids
        )
        self.assertNotIn(
            self._item2_id,
            item_ids
        )
        self.assertIn(
            self._item3_id,
            item_ids
        )


class ForceDeleteAssessmentTests(BaseTest):
    def create_assessment_with_taken_for_item(self, bank_id, item_id):
        from utilities.general import get_session_data, clean_id
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        bank.add_item(new_assessment.ident, clean_id(item_id))

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return str(new_assessment.ident)

    def create_test_bank(self, _name="test bank", _desc="for testing"):
        """
        helper method to create a test assessment bank
        """
        from utilities.general import get_session_data
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        form = am.get_bank_form_for_create([])
        form.display_name = _name
        form.description = _desc
        return am.create_bank(form)

    def setUp(self):
        super(ForceDeleteAssessmentTests, self).setUp()
        self._item1_id = self.add_item(self._bank_id)
        self._assessment_id = self.create_assessment_with_taken_for_item(self._bank_id,
                                                                         self._item1_id)
        self._bank2 = self.create_test_bank('second bank',
                                            'that houses another assessment')

        add_user_authz_to_settings('instructor',
                                   self._username,
                                   catalog_id=self._bank2.ident)
        configure_proxyable_authz(self._app_user, catalog_id=self._bank2.ident)
        configure_proxyable_authz(self._app_user, catalog_id=self._bank.ident)

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(ForceDeleteAssessmentTests, self).tearDown()

    def test_without_force_flag_cannot_delete(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/' + \
              unquote(self._assessment_id) + '/'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)

        req = self.client.delete(url)
        self.code(req, 500)
        self.message(req, 'Illegal state: you cannot do that because system ' +
                          'conditions have changed. For example, the assessment ' +
                          'has already been taken, or you have exceeded the number of ' +
                          'allowed attempts.')

    def test_with_force_flag_can_delete_the_test(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/' + \
              unquote(self._assessment_id) + '/?force'
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)

        req = self.client.delete(url)
        self.ok(req)

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.code(req, 500)

    def test_can_use_force_flag_when_takens_are_in_different_banks(self):
        url = '/api/v2/assessment/hierarchies/roots/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'id'   : str(self._bank2.ident)
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = '/api/v2/assessment/hierarchies/nodes/' + unquote(str(self._bank2.ident)) + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [str(self._bank.ident)]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        form = self._bank.get_assessment_offered_form_for_create(clean_id(self._assessment_id), [])
        offered = self._bank.create_assessment_offered(form)
        form = self._bank2.get_assessment_taken_form_for_create(offered.ident, [])
        taken = self._bank2.create_assessment_taken(form)

        url = '{0}assessment/banks/{1}/assessments/{2}?force'.format(self.endpoint,
                                                                     unquote(str(self._bank2.ident)),
                                                                     unquote(self._assessment_id))
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)

        req = self.client.delete(url)
        self.ok(req)

        url = '{0}assessment/banks/{1}/assessments/{2}'.format(self.endpoint,
                                                               unquote(str(self._bank2.ident)),
                                                               unquote(self._assessment_id))

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.code(req, 500)

    def test_can_use_force_flag_when_offereds_are_in_different_banks(self):
        url = '/api/v2/assessment/hierarchies/roots/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'id'   : str(self._bank2.ident)
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        url = '/api/v2/assessment/hierarchies/nodes/' + unquote(str(self._bank2.ident)) + '/children/'
        post_sig = calculate_signature(self.auth, self.headers, 'POST', url)
        self.sign_client(post_sig)

        payload = {
            'ids'   : [str(self._bank.ident)]
        }

        req = self.client.post(url,
                               data=payload)
        self.code(req, 201)

        form = self._bank2.get_assessment_offered_form_for_create(clean_id(self._assessment_id), [])
        offered = self._bank2.create_assessment_offered(form)
        form = self._bank2.get_assessment_taken_form_for_create(offered.ident, [])
        taken = self._bank2.create_assessment_taken(form)

        url = '{0}assessment/banks/{1}/assessments/{2}?force'.format(self.endpoint,
                                                                     unquote(str(self._bank2.ident)),
                                                                     unquote(self._assessment_id))
        del_sig = calculate_signature(self.auth, self.headers, 'DELETE', url)
        self.sign_client(del_sig)

        req = self.client.delete(url)
        self.ok(req)

        url = '{0}assessment/banks/{1}/assessments/{2}'.format(self.endpoint,
                                                               unquote(str(self._bank2.ident)),
                                                               unquote(self._assessment_id))

        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.code(req, 500)


class QueryTests(BaseTest):
    def add_item(self, bank_id):
        from utilities.general import get_session_data, clean_id
        from utilities.assessment import activate_managers

        activate_managers(self.req)
        am = get_session_data(self.req, 'am')
        bank = am.get_bank(clean_id(bank_id))
        form = bank.get_item_form_for_create([EDX_ITEM_RECORD_TYPE])
        form.display_name = 'a test item!'
        form.description = 'for testing with'
        form.add_file(DataInputStream(self._file_response_1), 'test_file')
        self._lo = 'foo@bar:baz'

        form.set_learning_objectives([clean_id(self._lo)])
        new_item = bank.create_item(form)

        question_form = bank.get_question_form_for_create(new_item.ident, [])
        question_form.display_name = 'Question for ' + new_item.display_name.text
        question_form.description = ''
        new_question = bank.create_question(question_form)

        answer_form = bank.get_answer_form_for_create(new_item.ident, [])
        answer_form.display_name = 'Answer for ' + new_item.display_name.text
        new_answer = bank.create_answer(answer_form)

        return str(new_item.ident)

    def setUp(self):
        super(QueryTests, self).setUp()

        self.user = APIUser.objects.get(username=self._instructor)
        self.req = create_test_request(self.user)

        project_root = ABS_PATH + '/tests/'
        self._file_response_1 = open(project_root + 'The_unintended_benefits_of_pollution_rules.pdf', 'rb')
        self._file_response_2 = open(project_root + 'Workplanfor2_002QBank.pdf', 'rb')
        self._item_id = self.add_item(self._bank_id)
        self._taken_id, offered_id, self._assessment_id = self.create_taken_for_item(self._bank_id,
                                                                                     self._item_id)
        self.open_banks = [self._bank_id]

    def tearDown(self):
        """
        Remove the test user from all groups in Membership
        Start from the smallest groupId because need to
        remove "parental" roles like for DepartmentAdmin / DepartmentOfficer
        """
        super(QueryTests, self).tearDown()

        self._file_response_1.close()
        self._file_response_2.close()

    def test_can_query_items_display_name(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?display_name=item!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._item_id
        )


        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?display_name=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_items_description(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?description=for%20test'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._item_id
        )

        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?description=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_items_display_name_with_files(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?display_name=item!&files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._item_id
        )

        self.assertIn(
            'test_file',
            data['data']['results'][0]['files']
        )

        self.is_cloudfront_url(data['data']['results'][0]['files']['test_file'])


        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?display_name=bang!&files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_items_description_with_files(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?description=for%20test&files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._item_id
        )

        self.assertIn(
            'test_file',
            data['data']['results'][0]['files']
        )

        self.is_cloudfront_url(data['data']['results'][0]['files']['test_file'])


        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/items/?display_name=bang!&files'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_assessment_display_name(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/?display_name=a%20test'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._assessment_id
        )


        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/?display_name=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_assessment_description(self):
        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/?description=ing%20with'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._assessment_id
        )


        url = self.endpoint + 'assessment/banks/' + self._bank_id + '/assessments/?description=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_assessment_banks_display_name(self):
        url = self.endpoint + 'assessment/banks/?display_name=test'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )

        url = self.endpoint + 'assessment/banks/?display_name=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_assessment_banks_description(self):
        url = self.endpoint + 'assessment/banks/?description=for'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            unquote(data['data']['results'][0]['id']),
            self._bank_id
        )


        url = self.endpoint + 'assessment/banks/?description=bang!'
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

    def test_can_query_assessment_taken_agent_id(self):
        url = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken?agent={3}'.format(self.endpoint,
                                                                                          unquote(self._bank_id),
                                                                                          unquote(self._assessment_id),
                                                                                          self._instructor)
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)
        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            1
        )

        self.assertEqual(
            data['data']['results'][0]['id'],
            self._taken_id
        )

        url = '{0}assessment/banks/{1}/assessments/{2}/assessmentstaken?agent=foo'.format(self.endpoint,
                                                                                          unquote(self._bank_id),
                                                                                          unquote(self._assessment_id))
        get_sig = calculate_signature(self.auth, self.headers, 'GET', url)
        self.sign_client(get_sig)

        req = self.client.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            len(data['data']['results']),
            0
        )

