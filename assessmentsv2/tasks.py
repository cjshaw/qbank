import logging

from celery import Task
from assessments_main.celery_app import app

from utilities.assessment import create_new_assessment
from utilities.general import clean_id


class ErrorHandlingTask(Task):
    abstract = True

    def on_failure(self, exc, task_id, targs, tkwargs, einfo):
        """
        :param exc:
        :param task_id:
        :param args: path, domain_repo, user (args to import_file)
        :param kwargs:
        :param einfo: Traceback (str(einfo))
        :return:
        """
        # print 'Task ID {0} to create bulk assessments failed: {1}'.format(task_id, exc)
        logging.error('Task ID {0} to create bulk assessments failed: {1}'.format(task_id, exc))
        # e-mail someone?

@app.task(base=ErrorHandlingTask)
def create_assessments_in_bulk(am, data):
    for assessment in data:
        bank = am.get_bank(clean_id(assessment['bankId']))
        create_new_assessment(am,
                              bank,
                              assessment)
