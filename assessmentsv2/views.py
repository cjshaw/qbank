import re
import json
import zipfile
import datetime
import cStringIO

from bson.errors import InvalidId

from django.conf import settings
from django.db import IntegrityError
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, QueryDict

from dlkit.json_.assessment.assessment_utilities import get_assessment_part_lookup_session

from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer, XMLRenderer
from rest_framework.permissions import AllowAny

from dlkit.runtime.errors import *
from dlkit.runtime.primordium import RGBColorCoordinate, DataInputStream, DateTime
from dlkit.records.registry import ANSWER_GENUS_TYPES,\
    ASSESSMENT_TAKEN_RECORD_TYPES, COMMENT_RECORD_TYPES, BANK_RECORD_TYPES,\
    ASSESSMENT_RECORD_TYPES, ASSESSMENT_PART_RECORD_TYPES, ASSESSMENT_PART_GENUS_TYPES

from bs4 import BeautifulSoup

from qbank.views import DLKitSessionsManager

from .types import *
from assessmentsv2.tasks import create_assessments_in_bulk
from utilities import assessment as autils
from utilities import general as gutils
from utilities import resource as resutils
from utilities import repository as rutils


ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD_TYPE = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['advanced-query'])
BANK_OBJECTIVE_BANK_RECORD_TYPE = Type(**BANK_RECORD_TYPES['objective-bank'])
COLOR_BANK_RECORD_TYPE = Type(**BANK_RECORD_TYPES['bank-color'])
FILE_COMMENT_RECORD_TYPE = Type(**COMMENT_RECORD_TYPES['file-comment'])
REVIEWABLE_TAKEN = Type(**ASSESSMENT_TAKEN_RECORD_TYPES['review-options'])
SIMPLE_SEQUENCE_ASSESSMENT_RECORD_TYPE = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])
WRONG_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['wrong-answer'])
RIGHT_ANSWER_GENUS = Type(**ANSWER_GENUS_TYPES['right-answer'])

OBJECTIVE_BASED_ASSESSMENT_PART_RECORD = Type(**ASSESSMENT_PART_RECORD_TYPES['objective-based'])
MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING = Type(**ASSESSMENT_PART_RECORD_TYPES['scaffold-down'])
SIMPLE_SEQUENCE_PART_RECORD_TYPE = Type(**ASSESSMENT_PART_RECORD_TYPES['simple-child-sequencing'])

LO_ASSESSMENT_SECTION = Type(**ASSESSMENT_PART_GENUS_TYPES['fbw-specify-lo'])

FBW_PHASE_I_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-i'])
FBW_PHASE_II_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['fbw-phase-ii'])


# https://stackoverflow.com/questions/20424521/override-jsonserializer-on-django-rest-framework/20426493#20426493
class DLJSONRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        data = gutils.clean_up_dl_objects(data)
        return super(DLJSONRenderer, self).render(data,
                                                  accepted_media_type,
                                                  renderer_context)

# Also need to render LTI XML...
class LTIXMLRenderer(XMLRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        data = gutils.clean_up_dl_objects(data)
        return super(LTIXMLRenderer, self).render(data,
                                                  accepted_media_type,
                                                  renderer_context)


# http://www.django-rest-framework.org/tutorial/3-class-based-views
class AssessmentService(DLKitSessionsManager):
    """
    List all available assessment services.
    api/v2/assessment/
    """

    def get(self, request, format=None):
        """
        List all available assessment services. For now, just 'banks'
        """
        try:
            data = {}
            gutils.update_links(request, data)
            return Response(data)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)


class AssessmentBanksList(DLKitSessionsManager):
    """
    List all available assessment banks.
    api/v2/assessment/banks/

    POST allows you to create a new assessment bank, requires two parameters:
      * name
      * description

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"name" : "a new bank","description" : "this is a test"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, format=None):
        """
        List all available assessment banks
        """
        try:
            if len(self.data) == 0:
                assessment_banks = self.am.banks
            else:
                querier = self.am.get_bank_query()

                allowable_query_terms = ['display_name', 'description', 'genus_type_id']
                if any(term in self.data for term in allowable_query_terms):
                    querier = autils.config_osid_object_querier(querier, self.data)
                    assessment_banks = self.am.get_banks_by_query(querier)
                else:
                    assessment_banks = self.am.banks

            banks = gutils.extract_items(request, assessment_banks)
            return Response(banks)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, format=None):
        """
        Create a new assessment bank, if authorized
        Create a new group in IS&T Membership service

        """
        try:
            if 'color' in self.data:
                form = self.am.get_bank_form_for_create([COLOR_BANK_RECORD_TYPE])
                hex_code = str(self.data['color'].replace('0x', ''))
                form.set_color_coordinate(RGBColorCoordinate(hex_code))
            elif 'objectiveBankId' in self.data:
                form = self.am.get_bank_form_for_create([BANK_OBJECTIVE_BANK_RECORD_TYPE])
                form.set_objective_bank_id(self.data['objectiveBankId'])
            else:
                form = self.am.get_bank_form_for_create([])

            form = gutils.set_form_basics(form, self.data)
            bank = self.am.create_bank(form)
            new_bank = gutils.convert_dl_object(bank)

            if 'aliasId' in self.data:
                # alias the new bank
                self.am.alias_bank(bank.ident, gutils.clean_id(self.data['aliasId']))
            # gutils.clear_cache()
            return Response(new_bank)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentBanksDetail(DLKitSessionsManager):
    """
    Shows details for a specific assessment bank.
    api/v2/assessment/banks/<bank_id>/

    GET, PUT, DELETE
    PUT will update the assessment bank. Only changed attributes need to be sent.
    DELETE will remove the assessment bank.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "a new bank"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, format=None):
        try:
            data = self.am.delete_bank(gutils.clean_id(bank_id))
            # gutils.clear_cache()
            return Response(data)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, format=None):
        try:
            bank = gutils.convert_dl_object(self.bank)
            gutils.update_links(request, bank)
            return Response(bank)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, format=None):
        try:
            form = self.am.get_bank_form_for_update(gutils.clean_id(bank_id))

            form = gutils.set_form_basics(form, self.data)
            if 'color' in self.data:
                hex_code = str(self.data['color'].replace('0x', ''))
                form.set_color_coordinate(RGBColorCoordinate(hexstr=hex_code))
            if 'objectiveBankId' in self.data:
                try:
                    form.set_objective_bank_id(self.data['objectiveBankId'])
                except AttributeError:
                    form._for_update = False
                    record = form.get_bank_form_record(BANK_OBJECTIVE_BANK_RECORD_TYPE)
                    record._init_metadata()
                    record._init_map()
                    form._for_update = True
                    form.set_objective_bank_id(self.data['objectiveBankId'])
            updated_bank = self.am.update_bank(form)
            self.bank = updated_bank
            bank = gutils.convert_dl_object(updated_bank)

            if 'aliasId' in self.data:
                # alias the new bank
                self.am.alias_bank(updated_bank.ident, gutils.clean_id(self.data['aliasId']))

            gutils.update_links(request, bank)

            # gutils.clear_cache()
            return Response(bank)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentsList(DLKitSessionsManager):
    """
    Get a list of all assessments in the specified bank
    api/v2/assessment/banks/<bank_id>/assessments/

    GET, POST
    POST creates a new assessment

    Note that "times" like duration and startTime for offerings should be
    input as JSON objects when using the RESTful API. Example:
        "startTime":{"year":2015,"month":1,"day":15}

    In this UI, you can put an object into the textarea below, and it will work fine.

    Note that duration only returns days / minutes / seconds

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    POST example (note the use of double quotes!!):
       {"name" : "an assessment","description" : "this is a hard pset","itemIds" : ["assessment.Item%3A539ef3a3ea061a0cb4fba0a3%40birdland.mit.edu"]}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, format=None):
        try:
            if len(self.data) == 0:
                assessments = self.bank.get_assessments()
            else:
                # TODO: flag for isolated_bank_view (make sure this gets passed down to assessmentLookupSession
                # TODO: get assessments_offered in a single call

                allowed_queries = ['display_name', 'description', 'genusTypeId',
                                   'sourceAssessmentTakenId', 'isolated',
                                   'withOffereds']
                if any(term in self.data for term in allowed_queries):
                    if 'isolated' in self.data:
                        self.bank._get_provider_session('assessment_query_session')
                        self.bank._get_provider_session('assessment_offered_lookup_session')
                        self.bank.use_isolated_bank_view()

                    querier = self.bank.get_assessment_query()

                    querier = autils.config_osid_object_querier(querier, self.data)

                    if 'sourceAssessmentTakenId' in self.data:
                        if isinstance(self.data, QueryDict):
                            taken_ids = self.data.getlist('sourceAssessmentTakenId')
                        else:
                            taken_ids = self.data['sourceAssessmentTakenId']
                        for source_taken_id in taken_ids:
                            querier.match_source_assessment_taken_id(gutils.clean_id(source_taken_id), True)

                    assessments = self.bank.get_assessments_by_query(querier)
                else:
                    assessments = self.bank.get_assessments()

            if 'raw' in self.data:
                if 'sections' in self.data:
                    data = []
                    for assessment in assessments:
                        assessment_map = assessment.object_map
                        assessment_map['sections'] = []
                        for child_id in assessment.get_child_ids():
                            assessment_part = self.bank.get_assessment_part(child_id)
                            assessment_map['sections'].append(assessment_part.object_map)
                        if 'withOffereds' in self.data:
                            assessment_map['offereds'] = [ao.object_map
                                                          for ao in self.bank.get_assessments_offered_for_assessment(assessment.ident)]
                        data.append(assessment_map)
                else:
                    if 'withOffereds' in self.data:
                        data = []
                        for assessment in assessments:
                            assessment_map = assessment.object_map
                            assessment_map['offereds'] = [ao.object_map
                                                          for ao in self.bank.get_assessments_offered_for_assessment(assessment.ident)]
                            data.append(assessment_map)
                    else:
                        data = gutils.extract_items(request, assessments, raw=True)
            else:
                # if 'withOffereds' in self.data:
                # not sure the best way to implement this here
                data = gutils.extract_items(request, assessments)

                # add in the section metadata, if available
                if 'sections' in self.data:
                    autils.update_with_section_metadata(self.bank,
                                                        data)
            return Response(data)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, format=None):
        try:
            new_assessment = autils.create_new_assessment(self.am,
                                                          self.bank,
                                                          self.data)

            full_assessment = self.bank.get_assessment(new_assessment.ident)
            data = gutils.convert_dl_object(full_assessment)

            # gutils.clear_cache()

            return Response(data)
        except (PermissionDenied, NotFound, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemsList(DLKitSessionsManager):
    """
    Return list of items in the given assessment bank. Make sure to embed
    the question and answers in the JSON.
    api/v2/assessment/banks/<bank_id>/items/

    GET, POST
    POST creates a new item

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       This UI: {"name" : "an assessment item","description" : "this is a hard quiz problem","question":{"type":"question-record-type%3Aresponse-string%40ODL.MIT.EDU","questionString":"Where am I?"},"answers":[{"type":"answer-record-type%3Aresponse-string%40ODL.MIT.EDU","responseString":"Here"}]}
   """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id=None, format=None):
        try:
            if bank_id is None:
                raise PermissionDenied

            allowable_query_terms = ['max_difficulty', 'min_difficulty',
                                     'max_discrimination', 'min_discrimination',
                                     'display_name', 'learning_objective',
                                     'description', 'genus_type_id']

            # num_attempts & num_weeks for FBW
            advanced_query_terms = ['num_attempts', 'num_weeks', 'agent']

            if not (any(t in self.data for t in allowable_query_terms) or
                    any(t in self.data for t in advanced_query_terms)):
                items = self.bank.get_items()
            else:
                querier = self.bank.get_item_query()

                if any(term in self.data for term in allowable_query_terms):
                    querier = autils.config_item_querier(querier, self.data)

                    items = self.bank.get_items_by_query(querier)
                elif any(term in self.data for term in advanced_query_terms):
                    # assume only instructors have access to this.
                    # must include an agent term in advanced queries
                    gutils.verify_keys_present(self.data, advanced_query_terms)
                    # first, get the FBW takens for the specified agent
                    if self.data['num_weeks'] == 0 or self.data['num_attempts'] == 0:
                        items = []
                    else:
                        querier = self.bank.get_assessment_taken_query()
                        agent_id = resutils.get_agent_id(self.data['agent'])
                        querier.match_taking_agent_id(agent_id, match=True)
                        if 'num_weeks' in self.data:
                            # next, filter out the takens by num_weeks
                            try:
                                week_cutoff = datetime.datetime.utcnow() - datetime.timedelta(weeks=self.data['num_weeks'])
                                querier.match_start_time(week_cutoff, True)
                            except AttributeError:
                                pass

                        takens = self.bank.get_assessments_taken_by_query(querier)
                        item_ids = []
                        if 'num_attempts' in self.data:
                            # then, filter out the items by wrong in first X attempts
                            # first, get items for each taken,
                            # then sort them by start time
                            # also use question._my_map['itemId'] because of our new assessment-session
                            #   unique IDs obscure the item...but we need it!
                            taken_items = {}
                            for taken in takens:
                                first_section = self.bank.get_first_assessment_section(taken.ident)
                                questions = self.bank.get_questions(first_section.ident)
                                for question in questions:
                                    orig_item_id = question._my_map['itemId']
                                    if orig_item_id not in taken_items:
                                        taken_items[orig_item_id] = []
                                    question_status = autils.get_question_status(self.bank,
                                                                                 first_section,
                                                                                 question.ident)
                                    if 'isCorrect' in question_status:
                                        correct = question_status['isCorrect']
                                    else:
                                        correct = None

                                    taken_items[orig_item_id].append(
                                        (correct, taken.get_actual_start_time()))

                            sorted_items = {}
                            for orig_item_id, responses in taken_items.iteritems():
                                sorted_items[orig_item_id] = sorted(responses, key=lambda k: k[1])  # sort by response time
                            # Now filter out by X wrong responses, None is ignored
                            for orig_item_id, responses in sorted_items.iteritems():
                                valid_responses = [r for r in responses if r[0] is not None]
                                if len(valid_responses) > self.data['num_attempts']:
                                    correct_responses = [r for r in valid_responses[0:self.data['num_attempts']] if r[0]]
                                else:
                                    correct_responses = [r for r in valid_responses if r[0]]
                                if len(correct_responses) == 0:
                                    item_ids.append(gutils.clean_id(orig_item_id))

                        # finally, get the items for those takens
                        if len(item_ids) > 0:
                            items = self.bank.get_items_by_ids(item_ids)
                        else:
                            items = []
                else:
                    items = self.bank.get_items()

            if 'raw' in self.data:
                data = gutils.extract_items(request, items, raw=True)
            else:
                data = gutils.extract_items(request, items)

            data = autils.update_json_response_with_item_metadata(self.bank,
                                                                  self.data,
                                                                  data,
                                                                  self.am)

            return Response(data)
        except (PermissionDenied, IntegrityError) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id=None, format=None):
        try:
            bank = new_item = None
            if bank_id is None:
                gutils.verify_keys_present(self.data, ['bankId'])
                bank_id = self.data['bankId']

            expected = ['name', 'description']
            gutils.verify_keys_present(self.data, expected)

            new_item = autils.create_new_item(self.bank, self.data)
            # create questions and answers if they are part of the
            # input data. There must be a better way to figure out
            # which attributes I should set, given the
            # question type?
            if 'question' in self.data:
                question = self.data['question']

                if isinstance(question, basestring):
                    question = json.loads(question)

                if 'rerandomize' in self.data and 'rerandomize' not in question:
                    question['rerandomize'] = self.data['rerandomize']

                q_type = Type(question['type'])
                qfc = self.bank.get_question_form_for_create(item_id=new_item.ident,
                                                             question_record_types=[q_type])
                qfc = autils.update_question_form(request, question, qfc, create=True)

                if 'genus' in question:
                    qfc.genus_type = Type(question['genus'])

                if ('fileIds' in new_item.object_map and
                        len(new_item.object_map['fileIds'].keys()) > 0):
                    # add these files to the question, too
                    file_ids = new_item.object_map['fileIds']
                    qfc = autils.add_file_ids_to_form(qfc, file_ids)

                new_question = self.bank.create_question(qfc)

            if 'answers' in self.data:
                answers = self.data['answers']
                if isinstance(answers, basestring):
                    answers = json.loads(answers)
                for answer in answers:
                    a_types = autils.get_answer_records(answer)

                    afc = self.bank.get_answer_form_for_create(new_item.ident,
                                                               a_types)

                    if 'multi-choice' in answer['type']:
                        # because multiple choice answers need to match to
                        # the actual MC3 ChoiceIds, NOT the index passed
                        # in by the consumer.
                        if not new_question:
                            raise NullArgument('Question')
                        afc = autils.update_answer_form(answer, afc, new_question)
                    else:
                        afc = autils.update_answer_form(answer, afc)

                    afc = autils.set_answer_form_genus_and_feedback(answer, afc)
                    new_answer = self.bank.create_answer(afc)

            full_item = self.bank.get_item(new_item.ident)
            return_data = gutils.convert_dl_object(full_item)

            # if there is a providerId in the data bundle,
            # create an enclosed asset for this item.
            # Return the providerId as part of the json.
            if ('providerId' in self.data and
                    self.data['providerId'] != '' and
                    self.data['providerId'] is not None):
                asset = rutils.set_enclosed_object_provider_id(request,
                                                               self.bank,
                                                               full_item,
                                                               self.data['providerId'])
                return_data.update({
                    'providerId': str(asset.provider_id)
                })

            # for convenience, also return the wrong answers
            try:
                wrong_answers = full_item.get_wrong_answers()
                for wa in wrong_answers:
                    return_data['answers'].append(wa.object_map)
            except AttributeError:
                pass
            # gutils.clear_cache()
            return Response(return_data)
        except (KeyError, IntegrityError, PermissionDenied, Unsupported,
                InvalidArgument, NullArgument) as ex:
            gutils.clean_up_post(bank, new_item)
            gutils.handle_exceptions(ex)


class AssessmentCanTakeAuthz(DLKitSessionsManager):
    """
    Get assessment hint if user can take assessments
    api/v2/assessment/banks/<bank_id>/assessments/cantake/

    GET
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, format=None):
        try:
            data = {
                'canTake': self.bank.can_take_assessments()
            }
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentDetails(DLKitSessionsManager):
    """
    Get assessment details for the given bank
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/

    GET, PUT, DELETE
    PUT to modify an existing assessment. Include only the changed parameters.
    DELETE to remove from the repository.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "an updated assessment"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, sub_id, format=None):
        try:
            try:
                data = self.bank.delete_assessment(gutils.clean_id(sub_id))
            except IllegalState:
                if 'force' in self.data:
                    self.bank.use_federated_bank_view()
                    for offered in self.bank.get_assessments_offered_for_assessment(gutils.clean_id(sub_id)):
                        for taken in self.bank.get_assessments_taken_for_assessment_offered(offered.ident):
                            taken_bank = self.am.get_bank(gutils.clean_id(taken.object_map['assignedBankIds'][0]))
                            taken_bank.delete_assessment_taken(taken.ident)
                        offered_bank = self.am.get_bank(gutils.clean_id(offered.object_map['assignedBankIds'][0]))
                        offered_bank.delete_assessment_offered(offered.ident)
                    data = self.bank.delete_assessment(gutils.clean_id(sub_id))
                else:
                    raise IllegalState
            # gutils.clear_cache()
            return Response(data)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            data = gutils.convert_dl_object(self.bank.get_assessment(gutils.clean_id(sub_id)))
            gutils.update_links(request, data)

            if 'sections' in self.data:
                autils.update_with_section_metadata(self.bank,
                                                    [data])
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, sub_id, format=None):
        try:
            assessment_id = gutils.clean_id(sub_id)

            if 'hasSpawnedFollowOnPhase' in self.data:
                form = self.bank.get_assessment_form_for_update(assessment_id)
                form.set_follow_on_phase_state(bool(self.data['hasSpawnedFollowOnPhase']))
                self.bank.update_assessment(form)

            if 'sourceAssessmentTakenId' in self.data:
                form = self.bank.get_assessment_form_for_update(assessment_id)
                form.set_source_assessment_taken_id(gutils.clean_id(self.data['sourceAssessmentTakenId']))
                self.bank.update_assessment(form)

            # if sections are defined, then this is a complicated assessment
            # and we need to manage parts!
            if 'sections' in self.data:
                if isinstance(self.data['sections'], dict) and any(key in self.data['sections'].keys() for key in ['newSections', 'oldSectionIds', 'updatedSections']):
                    if 'newSections' in self.data['sections']:
                        autils.add_sections_to_assessment(self.am,
                                                          self.bank,
                                                          assessment_id,
                                                          self.data['sections']['newSections'])
                    if 'oldSectionIds' in self.data['sections']:
                        for section_id in self.data['sections']['oldSectionIds']:
                            # recursively delete any child parts, if present
                            autils.delete_part_and_children(self.bank, gutils.clean_id(section_id))
                    if 'updatedSections' in self.data['sections']:
                        for section in self.data['sections']['updatedSections']:
                            # section = {
                            #    id: "123",
                            #    learningObjectiveId: "123"
                            #    itemIds: [""]
                            # }
                            part_id = gutils.clean_id(section['id'])
                            update_form = self.bank.get_assessment_part_form_for_update(part_id)
                            if 'learningObjectiveId' in section or 'minimumProficiency' in section:
                                if 'learningObjectiveId' in section:
                                    update_form.set_learning_objective_id(gutils.clean_id(section['learningObjectiveId']))
                                if 'minimumProficiency' in section:
                                    update_form.set_minimum_proficiency(gutils.clean_id(section['minimumProficiency']))
                            part = self.bank.update_assessment_part(part_id, update_form)
                            if 'type' not in section and 'itemIds' in section:
                                if 'scaffold' in section and section['scaffold']:
                                    # remove old child parts first and remove them as children
                                    for child_id in part.get_child_ids():
                                        self.bank.delete_assessment_part(child_id)
                                    # remove old items first, if any (assume want to convert them
                                    # to scaffold items / parts)
                                    for item_id in part.get_item_ids():
                                        self.bank.remove_item(item_id, part.ident)

                                    update_form = self.bank.get_assessment_part_form_for_update(part_id)
                                    update_form.set_children([])
                                    part = self.bank.update_assessment_part(part_id, update_form)

                                    for item_id in section['itemIds']:
                                        # need to set the bankId on the magic part
                                        item_bank = autils.get_object_bank(self.am,
                                                                           object_id=gutils.clean_id(item_id),
                                                                           object_type='item')
                                        magic_part_form = self.bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                                                            [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                                             SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                                        magic_part_form.display_name = 'magic part!'
                                        magic_part_form.set_item_ids([gutils.clean_id(item_id)])
                                        magic_part_form.set_item_bank_id(item_bank.ident)

                                        if 'quota' in section:
                                            magic_part_form.set_waypoint_quota(int(section['quota']))

                                        self.bank.create_assessment_part_for_assessment_part(magic_part_form)
                                else:
                                    # remove old items first
                                    for item_id in part.get_item_ids():
                                        self.bank.remove_item(item_id, part.ident)
                                    for item_id in section['itemIds']:
                                        self.bank.add_item(gutils.clean_id(item_id), part.ident)
                            elif 'type' in section and section['type'] == str(LO_ASSESSMENT_SECTION):
                                # remove old child parts first and remove them as children
                                for child_id in part.get_child_ids():
                                    self.bank.delete_assessment_part(child_id)
                                # remove old items first, if any (assume want to convert them
                                # to scaffold items / parts)
                                for item_id in part.get_item_ids():
                                    self.bank.remove_item(item_id, part.ident)

                                update_form = self.bank.get_assessment_part_form_for_update(part_id)
                                update_form.set_children([])
                                part = self.bank.update_assessment_part(part_id, update_form)

                                # need to set the bankId on the magic part
                                # so need this from the client
                                for i in range(0, int(section['quota'])):
                                    magic_part_form = self.bank.get_assessment_part_form_for_create_for_assessment_part(part.ident,
                                                                                                                        [MAGIC_ASSESSMENT_PART_FOR_SCAFFOLDING,
                                                                                                                         SIMPLE_SEQUENCE_PART_RECORD_TYPE])
                                    magic_part_form.display_name = 'magic part!'
                                    magic_part_form.set_item_bank_id(gutils.clean_id(section['itemBankId']))
                                    magic_part_form.set_learning_objective_ids([gutils.clean_id(section['learningObjectiveId'])])
                                    if 'waypointQuota' in section:
                                        waypoint_quota = section['waypointQuota']
                                    else:
                                        waypoint_quota = 1
                                    magic_part_form.set_waypoint_quota(waypoint_quota)

                                    self.bank.create_assessment_part_for_assessment_part(magic_part_form)
                else:
                    # do a bulk replace
                    assessment = self.bank.get_assessment(assessment_id)
                    for part_id in assessment.get_child_ids():
                        # recursively delete any child parts, if present
                        autils.delete_part_and_children(self.bank, part_id)
                    autils.add_sections_to_assessment(self.am,
                                                      self.bank,
                                                      assessment_id,
                                                      self.data['sections'])

            form = self.bank.get_assessment_form_for_update(assessment_id)
            form = gutils.set_form_basics(form, self.data)
            updated_assessment = self.bank.update_assessment(form)

            full_assessment = self.bank.get_assessment(assessment_id)
            data = gutils.convert_dl_object(full_assessment)
            if 'sections' in self.data:
                autils.update_with_section_metadata(self.bank,
                                                    [data])
            # gutils.clear_cache()
            return Response(data)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemDetails(DLKitSessionsManager):
    """
    Get item details for the given bank
    api/v2/assessment/banks/<bank_id>/items/<item_id>/

    GET, PUT, DELETE
    PUT to modify an existing item. Include only the changed parameters.
    DELETE to remove from the repository.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"name" : "an updated item"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, sub_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am, sub_id, object_type='item', bank_id=bank_id)
            data = bank.delete_item(gutils.clean_id(sub_id))
            return Response(data)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)
        except IllegalState as ex:
            gutils.handle_exceptions(type(ex)('This Item is being used in one or more '
                                              'Assessments. Delink it first, before '
                                              'deleting it.'))

    def get(self, request, sub_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am, sub_id, object_type='item', bank_id=bank_id)

            item = bank.get_item(gutils.clean_id(sub_id))
            data = gutils.convert_dl_object(item)

            gutils.update_links(request, data)

            if 'fileIds' in data:
                data['files'] = item.get_files()
            if data['question'] and 'fileIds' in data['question']:
                data['question']['files'] = item.get_question().get_files()

            try:
                autils.update_question_provider(self.rm, bank, data)
            except (NotFound, PermissionDenied):
                pass
            try:
                autils.update_question_provider(self.rm, bank, data['question'])
            except (TypeError, NotFound, PermissionDenied):
                pass

            # for convenience, also return the wrong answers
            try:
                wrong_answers = item.get_wrong_answers()
                for wa in wrong_answers:
                    data['answers'].append(wa.object_map)
            except AttributeError:
                pass

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, sub_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am, sub_id, object_type='item', bank_id=bank_id)

            if any(attr in self.data for attr in ['name', 'description', 'learningObjectiveIds',
                                                  'attempts', 'markdown', 'showanswer',
                                                  'weight', 'difficulty', 'discrimination']):
                form = bank.get_item_form_for_update(gutils.clean_id(sub_id))

                form = gutils.set_form_basics(form, self.data)

                if 'learningObjectiveIds' in self.data:
                    form = autils.set_item_learning_objectives(self.data, form)

                # update the item before the questions / answers,
                # because otherwise the old form will over-write the
                # new question / answer data
                # for edX items, update any metadata passed in
                if 'type' not in self.data:
                    if len(form._my_map['recordTypeIds']) > 0:
                        self.data['type'] = form._my_map['recordTypeIds'][0]
                    else:
                        self.data['type'] = ''

                form = autils.update_item_metadata(self.data, form)

                updated_item = bank.update_item(form)
            else:
                updated_item = bank.get_item(gutils.clean_id(sub_id))

            if 'question' in self.data:
                question = self.data['question']
                existing_question = updated_item.get_question()
                q_id = existing_question.ident

                if 'type' not in question:
                    question['type'] = existing_question.object_map['recordTypeIds'][0]

                if 'rerandomize' in self.data and 'rerandomize' not in question:
                    question['rerandomize'] = self.data['rerandomize']

                qfu = bank.get_question_form_for_update(q_id)
                qfu = autils.update_question_form(request, question, qfu)
                updated_question = bank.update_question(qfu)

            if 'answers' in self.data:
                for answer in self.data['answers']:
                    if 'id' in answer:
                        a_id = gutils.clean_id(answer['id'])
                        afu = bank.get_answer_form_for_update(a_id)
                        afu = autils.update_answer_form(answer, afu)

                        afu = autils.set_answer_form_genus_and_feedback(answer, afu)
                        bank.update_answer(afu)
                    else:
                        a_types = autils.get_answer_records(answer)
                        afc = bank.get_answer_form_for_create(gutils.clean_id(sub_id),
                                                              a_types)
                        afc = autils.set_answer_form_genus_and_feedback(answer, afc)
                        if 'multi-choice' in answer['type']:
                            # because multiple choice answers need to match to
                            # the actual MC3 ChoiceIds, NOT the index passed
                            # in by the consumer.
                            question = updated_item.get_question()
                            afc = autils.update_answer_form(answer, afc, question)
                        else:
                            afc = autils.update_answer_form(answer, afc)
                        bank.create_answer(afc)

            full_item = bank.get_item(gutils.clean_id(sub_id))

            return_data = gutils.convert_dl_object(full_item)

            if ('providerId' in self.data and
                    self.data['providerId'] != '' and
                    self.data['providerId'] is not None):
                asset = rutils.set_enclosed_object_provider_id(request,
                                                               bank,
                                                               full_item,
                                                               self.data['providerId'])
                return_data.update({
                    'providerId': str(asset.provider_id)
                })

            # for convenience, also return the wrong answers
            try:
                wrong_answers = full_item.get_wrong_answers()
                for wa in wrong_answers:
                    return_data['answers'].append(wa.object_map)
            except AttributeError:
                pass
            # gutils.clear_cache()
            return Response(return_data)
        except (PermissionDenied, Unsupported, InvalidArgument, NotFound) as ex:
            gutils.handle_exceptions(ex)


class SupportedItemTypes(DLKitSessionsManager):
    """
    Return list of supported item types with ids
    api/v2/assessment/types/items/

    GET
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, format=None):
        try:
            results = autils.supported_types()
            return Response(results)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)


class AssessmentItemsList(DLKitSessionsManager):
    """
    Get or link items in an assessment
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/items/

    GET, POST
    GET to view currently linked items
    POST to link a new item (appended to the current list)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"itemIds" : ["assessment.Item%3A539ef3a3ea061a0cb4fba0a3%40birdland.mit.edu"]}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        def get_child_part_items(part_id):
            _questions = []

            try:
                try:
                    _part = self.bank.get_assessment_part(part_id)
                except InvalidId:
                    apls = get_assessment_part_lookup_session(self.bank._osid_object._runtime,
                                                              self.bank._osid_object._proxy)
                    apls.use_federated_bank_view()
                    apls.use_unsequestered_assessment_part_view()
                    _part = apls.get_assessment_part(part_id)
                _questions += list(_part.get_items())
                if _part.has_children():
                    for _child_part_id in _part.get_child_ids():
                        _questions += get_child_part_items(_child_part_id)
                return _questions
            except AttributeError:
                # this could happen if a magic part is instantiated with
                # a learningObjectiveId, and not itemIds,
                # but you GET the part NOT in a section (i.e. for authoring)
                # Then, magic_part_lookup_session will try to initialize the
                # magic part, which will try to find an itemId that matches
                # the learningObjectiveId, but no section exists / is provided
                # so it throws an AttributeError (section._my_map)
                return _questions

        try:
            assessment_id = gutils.clean_id(sub_id)
            assessment = self.bank.get_assessment(assessment_id)
            child_ids = assessment.get_child_ids()
            if 'sections' in self.data:
                sections_data = []
                for child_id in child_ids:
                    part = self.bank.get_assessment_part(child_id)
                    section_map = part.object_map
                    questions = get_child_part_items(child_id)
                    questions_list = []
                    for question in questions:
                        question_map = question.object_map
                        if 'files' in self.data:
                            question_map['files'] = question.get_files()
                        questions_list.append(question_map)

                    section_map['questions'] = questions_list

                    sections_data.append(section_map)
                data = gutils.extract_items(request, sections_data, self.bank)
            else:
                items = []
                for child_part_id in child_ids:
                    items += list(get_child_part_items(child_part_id))
                data = gutils.extract_items(request, items)

                data = autils.update_json_response_with_item_metadata(self.bank,
                                                                      self.data,
                                                                      data,
                                                                      self.am)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, sub_id, format=None):
        try:
            assessment_id = gutils.clean_id(sub_id)

            if 'itemIds' in self.data:
                assessment = self.bank.get_assessment(assessment_id)
                child_ids = assessment.get_child_ids()
                if child_ids.available() > 1:
                    raise IllegalState('You cannot update the items with this endpoint -- use sections and PUT to the assessment endpoint')

                if isinstance(self.data, QueryDict):
                    items = self.data.getlist('itemIds')
                elif isinstance(self.data['itemIds'], basestring):
                    items = json.loads(self.data['itemIds'])
                else:
                    items = self.data['itemIds']

                if not isinstance(items, list):
                    try:
                        gutils.clean_id(items)  # use this as proxy to test if a valid OSID ID
                        items = [items]
                    except:
                        raise InvalidArgument

                for item_id in items:
                    self.bank.add_item(assessment_id,
                                  gutils.clean_id(item_id))

            items = self.bank.get_assessment_items(assessment_id)
            data = gutils.extract_items(request, items)
            return Response(data)
        except (PermissionDenied, InvalidArgument, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, sub_id, format=None):
        """Use put to support full-replacement of the item list"""
        try:
            assessment_id = gutils.clean_id(sub_id)

            if 'itemIds' in self.data:
                assessment = self.bank.get_assessment(assessment_id)
                child_ids = assessment.get_child_ids()
                if child_ids.available() > 1:
                    raise IllegalState('You cannot update the items with this endpoint -- use sections and PUT to the assessment endpoint')

                # first clear out existing items
                for item in self.bank.get_assessment_items(assessment_id):
                    self.bank.remove_item(assessment_id, item.ident)

                # now add the new ones
                if isinstance(self.data, QueryDict):
                    items = self.data.getlist('itemIds')
                elif isinstance(self.data['itemIds'], basestring):
                    items = json.loads(self.data['itemIds'])
                else:
                    items = self.data['itemIds']

                if not isinstance(items, list):
                    try:
                        gutils.clean_id(items)  # use this as proxy to test if a valid OSID ID
                        items = [items]
                    except:
                        raise InvalidArgument

                for item_id in items:
                    self.bank.add_item(assessment_id, gutils.clean_id(item_id))

            items = self.bank.get_assessment_items(assessment_id)
            data = gutils.extract_items(request, items)
            return gutils.UpdatedResponse(data)
        except (PermissionDenied, InvalidArgument, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentHierarchiesNodeChildrenList(DLKitSessionsManager):
    """
    List the children for a root bank.
    api/v2/assessment/hierarchies/nodes/<bank_id>/children/

    POST allows you to update the children list (bulk update)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"id": "assessment.Bank:54f9e39833bb7293e9da5b44@oki-dev.MIT.EDU"}

    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, format=None):
        """
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for child_id in self.data['ids']:
                child_bank = self.am.get_bank(gutils.clean_id(child_id))
                self.am.remove_child_bank(gutils.clean_id(bank_id),
                                          child_bank.ident)
            # gutils.clear_cache()
            return gutils.DeletedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, format=None):
        """
        List children of a node
        """
        try:
            if 'descendants' in self.data:
                descendant_levels = int(self.data['descendants'])
            else:
                descendant_levels = 1
            nodes = self.am.get_bank_nodes(gutils.clean_id(bank_id),
                                           0, descendant_levels, False)
            data = gutils.extract_items(request, nodes.get_child_bank_nodes())
            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, format=None):
        """
        Change POST to only add children. PUT can replace all (but dangerous,
        because of concurrency issues across multiple requests)
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for child_id in self.data['ids']:
                child_bank = self.am.get_bank(gutils.clean_id(child_id))
                try:
                    self.am.add_child_bank(gutils.clean_id(bank_id),
                                           child_bank.ident)
                except AlreadyExists:
                    pass  # just don't add it again
            return gutils.CreatedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, format=None):
        """
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            # first remove current child banks, if present
            try:
                self.am.remove_child_banks(gutils.clean_id(bank_id))
            except NotFound:
                pass

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for child_id in self.data['ids']:
                child_bank = self.am.get_bank(gutils.clean_id(child_id))
                self.am.add_child_bank(gutils.clean_id(bank_id),
                                       child_bank.ident)
            return gutils.CreatedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)


class AssessmentHierarchiesNodeDetails(DLKitSessionsManager):
    """
    List the bank details for a node bank.
    api/v2/assessment/hierarchies/nodes/<bank_id>/

    GET only. Can provide ?ancestors and ?descendants values to
              get nodes up and down the hierarchy.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, format=None):
        """
        List details of a node bank
        """
        try:
            if 'ancestors' in self.data:
                ancestor_levels = int(self.data['ancestors'])
            else:
                ancestor_levels = 0
            if 'descendants' in self.data:
                descendant_levels = int(self.data['descendants'])
            else:
                descendant_levels = 0
            include_siblings = False

            node_data = self.am.get_bank_nodes(gutils.clean_id(bank_id),
                                               ancestor_levels,
                                               descendant_levels,
                                               include_siblings)

            data = node_data.get_object_node_map()
            gutils.update_links(request, data)

            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)


class AssessmentHierarchiesNodeParentsList(DLKitSessionsManager):
    """
    List the parents for a root bank.
    api/v2/assessment/hierarchies/nodes/<bank_id>/parents/

    POST allows you to update the parent list
    DELETE allows you to remove a parent

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"id": "assessment.Bank:54f9e39833bb7293e9da5b44@oki-dev.MIT.EDU"}

    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, format=None):
        """
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for parent_id in self.data['ids']:
                parent_bank = self.am.get_bank(gutils.clean_id(parent_id))
                self.am.remove_child_bank(parent_bank.ident,
                                          gutils.clean_id(bank_id))
            # gutils.clear_cache()
            return gutils.DeletedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, format=None):
        """
        List ancestors of a node
        """
        try:
            if 'ancestors' in self.data:
                ancestor_levels = int(self.data['ancestors'])
            else:
                ancestor_levels = 1
            nodes = self.am.get_bank_nodes(gutils.clean_id(bank_id),
                                           ancestor_levels, 0, False)
            data = gutils.extract_items(request, nodes.get_parent_bank_nodes())
            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, format=None):
        """
        Change POST to only add parents.
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for parent_id in self.data['ids']:
                parent_bank = self.am.get_bank(gutils.clean_id(parent_id))
                try:
                    self.am.add_child_bank(parent_bank.ident,
                                           gutils.clean_id(bank_id))
                except AlreadyExists:
                    pass  # just don't add it again
            return gutils.CreatedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, format=None):
        """
        """
        try:
            gutils.verify_keys_present(self.data, 'ids')

            bank_id = gutils.clean_id(bank_id)
            # first remove current parent banks, if present
            try:
                current_parent_ids = self.am.get_parent_bank_ids(bank_id)
                for parent_id in current_parent_ids:
                    self.am.remove_child_bank(parent_id,
                                              bank_id)
            except NotFound:
                pass

            if not isinstance(self.data['ids'], list):
                self.data['ids'] = [self.data['ids']]
            for parent_id in self.data['ids']:
                parent_bank = self.am.get_bank(gutils.clean_id(parent_id))
                self.am.add_child_bank(parent_bank,
                                       bank_id)
            return gutils.CreatedResponse()
        except (PermissionDenied, NotFound, exceptions.NotAcceptable, KeyError) as ex:
            gutils.handle_exceptions(ex)


class AssessmentHierarchiesRootsList(DLKitSessionsManager):
    """
    List all available assessment hierarchy root nodes.
    api/v2/assessment/hierarchies/roots/

    POST allows you to add an existing bank as a root bank in
    the hierarchy.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
      {"id": "assessment.Bank:54f9e39833bb7293e9da5b44@oki-dev.MIT.EDU"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, format=None):
        """
        List all available root assessment banks
        """
        try:
            root_banks = self.am.get_root_banks()
            banks = gutils.extract_items(request, root_banks)
            return Response(banks)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, format=None):
        """
        Add a bank as a root to the hierarchy

        """
        try:
            gutils.verify_keys_present(self.data, ['id'])
            try:
                self.am.get_bank(gutils.clean_id(self.data['id']))
            except:
                raise InvalidArgument()

            self.am.add_root_bank(gutils.clean_id(self.data['id']))
            return gutils.CreatedResponse()
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentHierarchiesRootDetails(DLKitSessionsManager):
    """
    List the bank details for a root bank. Allow you to remove it as a root
    api/v2/assessment/hierarchies/roots/<bank_id>/

    DELETE allows you to remove a root bank.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, format=None):
        """
        Remove bank as root bank
        """
        try:
            root_bank_ids = self.am.get_root_bank_ids()
            if gutils.clean_id(bank_id) in root_bank_ids:
                self.am.remove_root_bank(gutils.clean_id(bank_id))
            else:
                raise exceptions.NotAcceptable('That bank is not a root.')
            return gutils.DeletedResponse()
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, format=None):
        """
        List details of a root bank
        """
        try:
            root_bank_ids = self.am.get_root_bank_ids()
            if gutils.clean_id(bank_id) in root_bank_ids:
                bank = self.am.get_bank(gutils.clean_id(bank_id))
            else:
                raise exceptions.NotAcceptable('That bank is not a root.')

            data = bank.object_map
            gutils.update_links(request, data)

            return Response(data)
        except (PermissionDenied, exceptions.NotAcceptable) as ex:
            gutils.handle_exceptions(ex)


class AssessmentItemDetails(DLKitSessionsManager):
    """
    Get item details for the given assessment
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/items/<item_id>/

    GET, DELETE
    GET to view the item
    DELETE to remove item from the assessment (NOT from the repo)
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, sub_id, item_id, format=None):
        try:
            data = self.bank.remove_item(gutils.clean_id(sub_id), gutils.clean_id(item_id))
            return Response(data)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, sub_id, item_id, format=None):
        view = ItemDetails.as_view()
        return view(request, bank_id=bank_id, sub_id=item_id, format=format)

    def put(self, request, bank_id, sub_id, item_id, format=None):
        view = ItemDetails.as_view()
        return view(request, bank_id, item_id, format)


class AssessmentsOffered(DLKitSessionsManager):
    """
    Get or create offerings of an assessment
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/assessmentsoffered/

    GET, POST
    GET to view current offerings
    POST to create a new offering (appended to the current offerings)

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
        [{"startTime" : {"year":2015,"month":1,"day":15},"duration": {"days":1}},{"startTime" : {"year":2015,"month":9,"day":15},"duration": {"days":1}}]
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            offerings = self.bank.get_assessments_offered_for_assessment(gutils.clean_id(sub_id))
            if 'raw' in self.data:
                data = gutils.extract_items(request, offerings, raw=True)
            else:
                data = gutils.extract_items(request, offerings)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, sub_id, format=None):
        # Cannot create offerings if no items attached to assessment
        try:
            # autils.check_assessment_has_items(bank, gutils.clean_id(sub_id))

            if isinstance(self.data, list):
                return_data = autils.set_assessment_offerings(self.bank,
                                                              self.data,
                                                              gutils.clean_id(sub_id))
                data = gutils.extract_items(request, return_data)['data']
            elif isinstance(self.data, dict):
                return_data = autils.set_assessment_offerings(self.bank,
                                                              [self.data],
                                                              gutils.clean_id(sub_id))
                data = gutils.convert_dl_object(return_data[0])
            else:
                raise InvalidArgument()
            # gutils.clear_cache()
            return Response(data)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)
        except LookupError as ex:
            gutils.handle_exceptions(type(ex)('Cannot create an assessment offering for '
                                              'an assessment with no items.'))


class AssessmentOfferedDetails(DLKitSessionsManager):
    """
    Get, edit, or delete offerings of an assessment
    api/v2/assessment/banks/<bank_id>/assessmentsoffered/<offered_id>/
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/assessments_offered/<offered_id>/

    GET, PUT, DELETE
    GET to view a specific offering
    PUT to edit the offering parameters
    DELETE to remove the offering

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
        This UI: {"startTime" : {"year":2015,"month":1,"day":15},"duration": {"days":5}}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, offering_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am,
                                          offering_id,
                                          object_type='assessment_offered',
                                          bank_id=bank_id)
            data = bank.delete_assessment_offered(gutils.clean_id(offering_id))
            return Response(data)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)
        except IllegalState as ex:
            gutils.handle_exceptions(type(ex)('There are still AssessmentTakens '
                                              'associated with this AssessmentOffered. '
                                              'Delete them first.'))

    def get(self, request, offering_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am,
                                          offering_id,
                                          object_type='assessment_offered',
                                          bank_id=bank_id)

            offering = bank.get_assessment_offered(gutils.clean_id(offering_id))
            data = gutils.convert_dl_object(offering)
            gutils.update_links(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, offering_id, bank_id=None, format=None):
        try:
            bank = autils.get_object_bank(self.am,
                                          offering_id,
                                          object_type='assessment_offered',
                                          bank_id=bank_id)

            if isinstance(self.data, list):
                if len(self.data) == 1:
                    return_data = autils.set_assessment_offerings(bank,
                                                                  self.data,
                                                                  gutils.clean_id(offering_id),
                                                                  update=True)
                    data = gutils.extract_items(request, return_data)['data']
                else:
                    raise InvalidArgument('Too many items.')
            elif isinstance(self.data, dict):
                return_data = autils.set_assessment_offerings(bank,
                                                              [self.data],
                                                              gutils.clean_id(offering_id),
                                                              update=True)
                data = gutils.convert_dl_object(return_data[0])
            else:
                raise InvalidArgument()
            # gutils.clear_cache()
            return Response(data)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentOfferedResults(DLKitSessionsManager):
    """
    Get the class results for an assessment offered (all the takens)
    api/v2/assessment/banks/<bank_id>/assessmentsoffered/<offered_id>/results/


    GET
    GET to view a specific offering
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, offering_id, bank_id=None, format=None):
        def taken_map(_taken):
            t_map = _taken.object_map
            t_map.update({
                'sections': autils.get_taken_section_map(_taken, update=False)
            })
            return t_map

        try:
            if bank_id is None:
                bank = autils.get_object_bank(self.am,
                                              offering_id,
                                              object_type='assessment_offered',
                                              bank_id=bank_id)
            else:
                bank = self.bank
            if 'isolated' in self.data:
                bank._get_provider_session('assessment_taken_query_session')
                bank._get_provider_session('assessment_taken_lookup_session')
                bank.use_isolated_bank_view()

            if 'agentId' in self.data:
                querier = bank.get_assessment_taken_query()
                agent_id = gutils.create_agent_id(self.data['agentId'])

                querier.match_taking_agent_id(agent_id, True)
                querier.match_assessment_offered_id(gutils.clean_id(offering_id), True)

                takens = bank.get_assessments_taken_by_query(querier)
                if takens.available() == 0:
                    raise NotFound('agentId not found')
                else:
                    taken = takens.next()
                # we need to replicate the sections data, so we can populate the
                # directives and target carousels
                sections = autils.get_taken_section_map(taken, update=False)
                if 'raw' in self.data:
                    data = gutils.extract_items(request, sections, self.bank, raw=True)
                else:
                    data = gutils.extract_items(request, sections, self.bank)
            else:
                takens = bank.get_assessments_taken_for_assessment_offered(gutils.clean_id(offering_id))

                data = [taken_map(t) for t in takens]

                if 'raw' in self.data:
                    data = gutils.extract_items(request, data, raw=True)
                else:
                    data = gutils.extract_items(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentOfferedSpawnedResults(DLKitSessionsManager):
    """
    Get the Fbw Spawned / Phase II class results for an assessment offered (all the takens)
    api/v2/assessment/banks/<bank_id>/assessmentsoffered/<offered_id>/spawnedresults/


    GET
    GET to view a specific offering
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, offering_id, format=None):
        def taken_map(_taken):
            t_map = _taken.object_map
            t_map.update({
                'sections': autils.get_taken_section_map(_taken, update=False)
            })
            return t_map
        try:
            # some students may not have taken phase 1, but will still
            # be assigned a phase 2. So we will have to assume
            # that they will have the original missionId in the
            # sourceAssessmentTakenId field -- so query on that.
            aqs = self.am.get_assessment_query_session(proxy=self.am._proxy)
            aqs.use_federated_bank_view()
            querier = aqs.get_assessment_query()
            querier.match_assessment_offered_id(gutils.clean_id(offering_id), True)
            try:
                original_assessment = aqs.get_assessments_by_query(querier).next()
                original_assessment_id = original_assessment.ident
            except StopIteration:
                original_assessment_id = None

            # first, get all the takens for the given offered,
            atqs = self.am.get_assessment_taken_query_session(proxy=self.am._proxy)
            atqs.use_federated_bank_view()
            querier = atqs.get_assessment_taken_query()
            querier.match_assessment_offered_id(gutils.clean_id(offering_id), True)
            takens = atqs.get_assessments_taken_by_query(querier)

            data = []
            if takens.available() > 0 or original_assessment_id is not None:
                # then, get all the assessments where the takenId is in "sourceAssessmentTakenId"
                aqs = self.am.get_assessment_query_session(proxy=self.am._proxy)
                aqs.use_federated_bank_view()
                querier = aqs.get_assessment_query()

                for taken in takens:
                    querier.match_source_assessment_taken_id(taken.ident, True)

                if original_assessment_id is not None:
                    querier.match_source_assessment_taken_id(original_assessment_id, True)

                assessments = aqs.get_assessments_by_query(querier)

                # only do this if there are some assessments...otherwise the taken
                # query below will fetch ALL takens in the system
                if assessments.available() > 0:
                    # then, get all the offereds for those assessments
                    aoqs = self.am.get_assessment_offered_query_session(proxy=self.am._proxy)
                    aoqs.use_federated_bank_view()
                    querier = aoqs.get_assessment_offered_query()

                    for assessment in assessments:
                        querier.match_assessment_id(assessment.ident, True)

                    offereds = aoqs.get_assessments_offered_by_query(querier)

                    if offereds.available() > 0:
                        # then, get the results for each of those offereds
                        querier = atqs.get_assessment_taken_query()

                        for offered in offereds:
                            querier.match_assessment_offered_id(offered.ident, True)

                        takens = atqs.get_assessments_taken_by_query(querier)

                        for taken in takens:
                            data.append(taken_map(taken))

            if 'raw' in self.data:
                data = gutils.extract_items(request, data, raw=True)
            else:
                data = gutils.extract_items(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentOfferedBulkResults(DLKitSessionsManager):
    """
    Get the class results for a list of assessments offered (all the takens)
    api/v2/assessment/banks/<bank_id>/bulkofferedresults/


    GET
    GET to view a specific offering
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, format=None):
        try:
            gutils.verify_keys_present(self.data, ['assessmentOfferedIds'])
            data = []
            if isinstance(self.data, QueryDict):
                offered_ids = self.data.getlist('assessmentOfferedIds')
            else:
                offered_ids = self.data['assessmentOfferedIds']

            if 'federated' in self.data:
                atqs = self.am.get_assessment_taken_query_session(proxy=self.am._proxy)
                atqs.use_federated_bank_view()
                querier = atqs.get_assessment_taken_query()
                session = atqs
            else:
                querier = self.bank.get_assessment_taken_query()
                session = self.bank

            for assessment_offered_id in offered_ids:
                querier.match_assessment_offered_id(gutils.clean_id(assessment_offered_id), True)

            takens = session.get_assessments_taken_by_query(querier)
            for taken in takens:
                taken_map = taken.object_map
                # let's get the questions first ... we need to inject that information into
                # the response, so that UI side we can match on the original, canonical itemId
                # also need to include the questions' learningObjectiveIds
                section_maps = []

                try:
                    sections = taken._get_assessment_sections()
                    for section in sections:
                        section_map = section.object_map
                        question_maps = []

                        questions = section.get_questions(update=False)
                        for index, question in enumerate(questions):
                            question_map = question.object_map
                            try:
                                response = section.get_response(question.ident).object_map
                            except IllegalState:
                                response = None
                            question_map.update({
                                'itemId': section._my_map['questions'][index]['itemId'],
                                'responses': [response]
                            })
                            question_maps.append(question_map)
                        section_map['questions'] = question_maps
                        section_maps.append(section_map)
                except KeyError:
                    # no sections -- never got the question
                    pass
                taken_map.update({
                    'sections': section_maps
                })
                data.append(taken_map)
            if 'raw' in self.data:
                data = gutils.extract_items(request, data, raw=True)
            else:
                data = gutils.extract_items(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class BulkAssessments(DLKitSessionsManager):
    """
    Create assessments in bulk, with offereds if provided
    api/v2/assessment/bulkassessments/

    Expects a list of assessment objects, each with a bankId
    {
        assessments:     [{
            bankId: 'assessment.Bank%3A123%40ODL.MIT.EDU',
            recordTypeIds': [''],
            name: 'Quiz 1',
            sections: [],
            assessmentsOffered: []
        }]
    }

    POST
    POST to create assessments in bulk, to the specified banks
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def post(self, request, format=None):
        try:
            gutils.verify_keys_present(self.data, ['assessments'])
            if 'async' in self.data and self.data['async']:
                async_task = create_assessments_in_bulk.apply_async((self.am,
                                                                     self.data['assessments']))
                data = async_task.id
            else:
                data = []
                for assessment in self.data['assessments']:
                    bank = self.am.get_bank(gutils.clean_id(assessment['bankId']))
                    new_assessment = autils.create_new_assessment(self.am,
                                                                  bank,
                                                                  assessment)
                    full_assessment = bank.get_assessment(new_assessment.ident)
                    data.append(full_assessment.object_map)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class BulkAssessmentsStatus(DLKitSessionsManager):
    """
    Get the current status of an asynch job
    api/v2/assessment/bulkassessments/status/

    Expects a jobId(s) in the CGI param

    GET
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, format=None):
        try:
            return_data = {}
            if 'jobId' in self.data:
                if not isinstance(self.data['jobId'], list):
                    self.data['jobId'] = [self.data['jobId']]
                for job_id in self.data['jobId']:
                    return_data[job_id] = create_assessments_in_bulk.AsyncResult(job_id).status
            return Response(return_data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class BulkAssessmentsOffered(DLKitSessionsManager):
    """
    Get offerings of a list of assessments
    api/v2/assessment/banks/<bank_id>/bulkassessmentsoffered/

    GET
    GET to view current offerings
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, format=None):
        try:
            gutils.verify_keys_present(self.data, ['assessmentIds'])
            data = []
            if isinstance(self.data, QueryDict):
                assessment_ids = self.data.getlist('assessmentIds')
            else:
                assessment_ids = self.data['assessmentIds']

            if 'federated' in self.data:
                aoqs = self.am.get_assessment_offered_query_session(proxy=self.am._proxy)
                aoqs.use_federated_bank_view()
                querier = aoqs.get_assessment_offered_query()
                session = aoqs
            else:
                querier = self.bank.get_assessment_offered_query()
                session = self.bank

            for assessment_id in assessment_ids:
                querier.match_assessment_id(gutils.clean_id(assessment_id), True)

            offereds = session.get_assessments_offered_by_query(querier)
            data += [offered.object_map for offered in offereds]

            if 'raw' in self.data:
                data = gutils.extract_items(request, data, raw=True)
            else:
                data = gutils.extract_items(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentsTaken(DLKitSessionsManager):
    """
    Get or link takens of an assessment. Input can be from an offering or from an assessment --
    so will have to take that into account in the views.
    api/v2/assessment/banks/<bank_id>/assessments/<assessment_id>/assessmentstaken/
    api/v2/assessment/banks/<bank_id>/assessmentsoffered/<offered_id>/assessmentstaken/

    POST can only happen from an offering (need the offering ID to create a taken)
    GET, POST
    GET to view current assessment takens
    POST to link a new item (appended to the current list) --
            ONLY from offerings/<offering_id>/takens/

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Create example: POST with no data.
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            # use canSearch assessment takens as proxy for learner vs. instructor
            # learners should ideally only see their takens...not everyone else's
            if not self.bank.can_search_assessments_taken():
                raise PermissionDenied('You are not authorized to view this.')

            if len(self.data) == 0:
                if 'assessment.AssessmentOffered' in sub_id:
                    takens = self.bank.get_assessments_taken_for_assessment_offered(
                        gutils.clean_id(sub_id))
                else:
                    takens = self.bank.get_assessments_taken_for_assessment(gutils.clean_id(sub_id))
            else:
                allowed_query_terms = ['display_name', 'description', 'agent']
                if any(term in self.data for term in allowed_query_terms):
                    querier = self.bank.get_assessment_taken_query()
                    querier = autils.config_osid_object_querier(querier, self.data)

                    if 'agent' in self.data:
                        # TODO: we'll have to define this for FBW differently
                        agent_id = resutils.get_agent_id(self.data['agent'])
                        querier.match_taking_agent_id(agent_id, match=True)

                    takens = self.bank.get_assessments_taken_by_query(querier)
                else:
                    if 'assessment.AssessmentOffered' in sub_id:
                        takens = self.bank.get_assessments_taken_for_assessment_offered(
                            gutils.clean_id(sub_id))
                    else:
                        takens = self.bank.get_assessments_taken_for_assessment(
                            gutils.clean_id(sub_id))
            if 'raw' in self.data:
                data = gutils.extract_items(request, takens, raw=True)
            else:
                data = gutils.extract_items(request, takens)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, sub_id, format=None):
        # when trying to create a taken for a user, check first
        # that a taken does not already exist, using
        # get_assessments_taken_for_taker_and_assessment_offered().
        # If it does exist, return that taken.
        # If one does not exist, create a new taken.
        try:
            # Kind of hokey, but need to get the sub_id type from a string...
            if 'assessment.AssessmentOffered' not in sub_id:
                raise Unsupported()
            # don't do the offered check here, because students don't have authz
            # to get offereds. Let the taken.has_ended() method try to create
            # a new one, which will throw IllegalState if now > deadline
            # # first, check if the deadline is passed on the offered
            # # if so, raise an error
            # offered = self.bank.get_assessment_offered(gutils.clean_id(sub_id))
            # try:
            #     deadline = offered.get_deadline()
            #     now = DateTime.utcnow()
            #
            #     if now > deadline:
            #         raise PermissionDenied('It is passed the deadline -- you cannot create an assessment taken for this assessment offered')
            # except IllegalState:
            #     pass

            # second, check if a taken exists for the user / offering
            user_id = self.am.effective_agent_id
            takens = self.bank.get_assessments_taken_for_taker_and_assessment_offered(user_id,
                                                                                      gutils.clean_id(sub_id))
            create_new_taken = False
            if takens.available() > 0:
                # return the first taken ONLY if not finished -- user has attempted this problem
                # before. If finished, create a new one.
                first_taken = takens.next()
                if first_taken.has_ended():
                    # create new one
                    create_new_taken = True
                else:
                    data = gutils.convert_dl_object(first_taken)
            else:
                # create a new taken
                create_new_taken = True

            if create_new_taken:
                # use our new Taken Record object, which has a "can_review_whether_correct()"
                # method.
                form = self.bank.get_assessment_taken_form_for_create(gutils.clean_id(sub_id),
                                                                      [ADVANCED_QUERY_ASSESSMENT_TAKEN_RECORD_TYPE,
                                                                       REVIEWABLE_TAKEN])
                data = gutils.convert_dl_object(self.bank.create_assessment_taken(form))

            return Response(data)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)
        except Unsupported as ex:
            gutils.handle_exceptions(type(ex)('Can only create AssessmentTaken from an '
                                              'AssessmentOffered root URL.'))


class AssessmentTakenDetails(DLKitSessionsManager):
    """
    Get a single taken instance of an assessment. Not used for much
    except to point you towards the /take endpoint...
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/

    GET, DELETE
    GET to view a specific taken
    DELETE to remove the taken

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
"""
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, taken_id, format=None):
        try:
            bank = autils.get_object_bank(self.am,
                                          taken_id,
                                          object_type='assessment_taken')
            data = bank.delete_assessment_taken(gutils.clean_id(taken_id))
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, taken_id, format=None):
        try:
            taken = self.bank.get_assessment_taken(gutils.clean_id(taken_id))
            data = gutils.convert_dl_object(taken)
            gutils.update_links(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class ItemQuestion(DLKitSessionsManager):
    """Edit question for an existing item

    api/v2/assessment/banks/<bank_id>/items/<item_id>/question/

    GET, PUT
    GET to get the question.
    PUT to modify the question.

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"questionString" : "What is 1 + 1?"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            existing_question = item.get_question()
            data = gutils.convert_dl_object(existing_question)
            if 'fileIds' in data:
                data.update({
                    'files': existing_question.get_files()
                })
            return Response(data)
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, sub_id, format=None):
        # TODO: handle updating of question files (manip and ortho view set)
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            existing_question = item.get_question()

            if 'type' not in self.data:
                # kind of a hack
                self.data['type'] = existing_question.object_map['recordTypeIds'][0]

            q_id = existing_question.ident
            qfu = self.bank.get_question_form_for_update(q_id)
            qfu = autils.update_question_form(request, self.data, qfu)
            updated_question = self.bank.update_question(qfu)

            full_item = self.bank.get_item(gutils.clean_id(sub_id))
            data = gutils.convert_dl_object(full_item)
            return Response(data)
        except (PermissionDenied, Unsupported, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemTextAsFormat(DLKitSessionsManager):
    """Request item text in specific format

    Returns the item text in a specific format. For example, edxml, QTI, etc.
    api/v2/assessment/banks/<bank_id>/items/<item_id>/<format>/

    GET
    GET to get the question.
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, output_format, sub_id=None, taken_id=None, question_id=None, format=None):
        try:
            supported_item_formats = ['edxml']
            if output_format not in supported_item_formats:
                raise InvalidArgument('"{0}" is not a supported item text format.'.format(output_format))

            if sub_id:
                item = self.bank.get_item(gutils.clean_id(sub_id))
            elif taken_id and question_id:
                # This works because DLKit makes the question and item have the
                # same ID. May not work in the future -- ?? is this guaranteed?
                item = self.bank.get_item(gutils.clean_id(question_id))
            else:
                raise LookupError
            if 'fileIds' in item.object_map:
                #  need to get the right extension onto the files
                file_labels = item.object_map['fileIds']
                files = item.get_files()
                data = {
                    'files' : {}
                }
                for label, content in file_labels.iteritems():
                    file_type_id = gutils.clean_id(content['assetContentTypeId'])
                    extension = file_type_id.identifier
                    data['files'][label + '.' + extension] = files[label]
            else:
                data = {}
            if output_format == 'edxml':
                if 'files' in data:
                    raw_edxml = item.get_edxml()
                    soup = BeautifulSoup(raw_edxml, 'xml')
                    labels = []
                    label_filename_map = {}
                    for filename in data['files'].keys():
                        label = filename.split('.')[0]
                        labels.append(label)
                        label_filename_map[label] = filename
                    attrs = {
                        'draggable'             : 'icon',
                        'drag_and_drop_input'   : 'img',
                        'files'                 : 'included_files',
                        'img'                   : 'src'
                    }
                    local_regex = re.compile('[^http]')
                    for key, attr in attrs.iteritems():
                        search = {attr : local_regex}
                        tags = soup.find_all(**search)
                        for item in tags:
                            if key == 'files' or item.name == key:
                                if item[attr] in labels:
                                    item[attr] = label_filename_map[item[attr]]
                    data['data'] = soup.find('problem').prettify()
                else:
                    data['data'] = item.get_edxml()

            return Response(data)
        except (PermissionDenied, NotFound, LookupError, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemFile(DLKitSessionsManager):
    """
    Download the given item file for an existing item
    api/v2/assessment/banks/<bank_id>/items/<item_id>/files/<file_name>

    GET
    GET to get the file (manipulatable, ortho viewset).

    TODO: Will need to modify this to fit a generic "file" record type
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, file_key, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            question = item.get_question()

            if file_key == 'manip':
                file_content_type = 'application/vnd.unity'
                filename = file_key + '.unity3d'
                file = question.manip
            elif file_key == 'front' or file_key == 'side' or file_key == 'top':
                file_content_type = 'image/jpeg'
                filename = file_key + '.jpg'
                view_key = file_key + '_view'
                file = getattr(question, view_key)
            elif file_key == 'all':
                filename = '3dfiles_' + re.sub(r'[^\w\d]', '', item.display_name.text) + '.zip'
                file = cStringIO.StringIO()
                file_content_type = 'application/zip'
                zf = zipfile.ZipFile(file, 'w')
                zf.writestr('manip.unity3d', question.manip.read())
                if question.has_ortho_view_set():
                    zf.writestr('front_view.jpg', question.front_view.read())
                    zf.writestr('side_view.jpg', question.side_view.read())
                    zf.writestr('top_view.jpg', question.top_view.read())
                zf.close()
            else:
                raise Exception()
            response = HttpResponse(content_type=file_content_type)
            response["Content-Disposition"] = "attachment; filename=" + filename
            try:
                # if it is a zip file, should use this
                response.write(file.getvalue())
            except:
                response.write(file.read())
            return response
        except (PermissionDenied, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemFilesList(DLKitSessionsManager):
    """
    Get a list of available files for this item
    api/v2/assessment/banks/<bank_id>/items/<item_id>/files/

    GET
    GET to get the file list (manipulatable, ortho viewset).

    TODO: Will need to modify this to fit a generic "file" record type
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            question = item.get_question()

            data = {
                '_links': {
                    'self': gutils.build_safe_uri(request)
                },
                'data': []
            }
            question_files = question.get_files()
            for label, link in question_files.iteritems():
                if 'View' in label and 'ortho' in question.object_map['recordTypeIds'][0]:
                    # for ortho3D questions, remove the View name
                    label = label.replace('View', '')
                data['_links'][label] = link
                data['data'].append({
                    label : link
                })

            return Response(data)
        except (PermissionDenied, NotFound, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class ItemAnswers(DLKitSessionsManager):
    """
    Edit answers for an existing item
    api/v2/assessment/banks/<bank_id>/items/<item_id>/answers/

    GET, POST
    GET to get current list of answers
    POST to add a new answer

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"responseString" : "2"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, sub_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            existing_answers = item.get_answers()

            data = gutils.extract_items(request, existing_answers)
            return Response(data)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, sub_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))

            if isinstance(self.data, list):
                for answer in self.data:
                    a_types = autils.get_answer_records(answer)
                    afc = self.bank.get_answer_form_for_create(gutils.clean_id(sub_id),
                                                          a_types)
                    afc = autils.update_answer_form(answer, afc)
                    afc = autils.set_answer_form_genus_and_feedback(answer, afc)
                    self.bank.create_answer(afc)
            elif isinstance(self.data, dict):
                a_types = autils.get_answer_records(self.data)
                afc = self.bank.get_answer_form_for_create(gutils.clean_id(sub_id),
                                                      a_types)
                afc = autils.set_answer_form_genus_and_feedback(self.data, afc)
                # for multi-choice-ortho, need to send the questions
                if 'multi-choice' in self.data['type']:
                    question = item.get_question()
                    afc = autils.update_answer_form(self.data, afc, question)
                else:
                    afc = autils.update_answer_form(self.data, afc)
                self.bank.create_answer(afc)
            else:
                raise InvalidArgument()

            new_item = self.bank.get_item(gutils.clean_id(sub_id))
            existing_answers = new_item.get_answers()
            data = gutils.extract_items(request, existing_answers)['data']
            return Response(data)
        except (PermissionDenied, Unsupported, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class Documentation(DLKitSessionsManager):
    """
    Shows the user documentation for talking to the RESTful service
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        return render_to_response('assessmentsv2/documentation.html',
                                  {'types': autils.supported_types()},
                                  RequestContext(request))


class FinishAssessmentTaken(DLKitSessionsManager):
    """
    "finish" the assessment to indicate that student has ended his/her attempt
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/finish/

    POST empty data
    """
    def post(self, request, bank_id, taken_id, format=None):
        try:
            # "finish" the assessment section
            # bank.finished_assessment_section(first_section.ident)
            self.bank.finish_assessment(gutils.clean_id(taken_id))
            data = {
                'success': True
            }
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class ItemAnswerDetails(DLKitSessionsManager):
    """
    Edit answers for an existing item answer
    api/v2/assessment/banks/<bank_id>/items/<item_id>/answers/<answer_id>/

    GET, PUT, DELETE
    GET to get this answer
    PUT to edit this answer
    DELETE to remove this answer

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (note the use of double quotes!!):
       {"responseString" : "2"}
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def delete(self, request, bank_id, sub_id, ans_id, format=None):
        try:
            # TODO: check that the answer is part of the sub_id item...
            data = self.bank.delete_answer(gutils.clean_id(ans_id))
            return Response(data)
        except (PermissionDenied, IllegalState) as ex:
            gutils.handle_exceptions(ex)

    def get(self, request, bank_id, sub_id, ans_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            answers = item.get_answers()  # need to get_answers() and filter out

            existing_answer = None
            for answer in answers:
                if answer.ident == gutils.clean_id(ans_id):
                    existing_answer = answer
                    break
            if not existing_answer:
                raise NotFound()

            data = gutils.convert_dl_object(existing_answer)
            gutils.update_links(request, data)
            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def put(self, request, bank_id, sub_id, ans_id, format=None):
        try:
            item = self.bank.get_item(gutils.clean_id(sub_id))
            try:
                answers = list(item.get_answers()) + list(item.get_wrong_answers())
            except AttributeError:
                answers = item.get_answers()

            answer = autils.find_answer_in_answers(gutils.clean_id(ans_id), answers)

            if 'type' not in self.data:
                self.data['type'] = answer.object_map['recordTypeIds'][0]

            a_id = gutils.clean_id(ans_id)
            afu = self.bank.get_answer_form_for_update(a_id)
            afu = autils.update_answer_form(self.data, afu)

            afu = autils.set_answer_form_genus_and_feedback(self.data, afu)

            updated_answer = self.bank.update_answer(afu)

            data = gutils.convert_dl_object(updated_answer)
            return Response(data)
        except (PermissionDenied, Unsupported, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class TakeAssessment(DLKitSessionsManager):
    """
    Get the next question available in the taken...DLkit tracks
    state of what is the next available. If files are included
    in the assessment type, they will be returned with
    the question text.
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/take/

    GET only is supported?
    GET to get the next uncompleted question for the given user

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, taken_id, format=None):
        try:
            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            question = self.bank.get_first_unanswered_question(first_section.ident)
            data = gutils.convert_dl_object(question)

            if 'fileIds' in data:
                data.update({
                    'files': question.get_files()
                })

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class TakeAssessmentFiles(DLKitSessionsManager):
    """
    Lists the files for the next assessment section, if it has
    any.
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/files/

    GET only is supported
    GET to get a list of files for the next unanswered section
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def get(self, request, bank_id, taken_id, format=None):
        try:
            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            question = self.bank.get_first_unanswered_question(first_section.ident)
            if 'question-record-type%3Alabel-ortho-faces%40ODL.MIT.EDU' in question.object_map['recordTypeIds']:
                question_files = question.get_files()
                data = {
                    'manip' : question_files['manip']
                }
                if question.has_ortho_view_set:
                    data['front'] = question_files['frontView']
                    data['side'] = question_files['sideView']
                    data['top'] = question_files['topView']
            else:
                raise LookupError('No files for this question.')
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound, LookupError) as ex:
            gutils.handle_exceptions(ex)


class SubmitAssessment(DLKitSessionsManager):
    """
    POST the student's response to the active item. DLKit
    tracks which question / item the student is currently
    interacting with.
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/submit/

    POST student response

    Note that for RESTful calls, you need to set the request header
    'content-type' to 'application/json'

    Example (for an Ortho3D manipulatable - label type):
        {"responseSet":{
                "frontFaceEnum" : 0,
                "sideFaceEnum"  : 1,
                "topFaceEnum"   : 2
            }
        }
    """
    renderer_classes = (DLJSONRenderer,BrowsableAPIRenderer)

    def post(self, request, bank_id, taken_id, format=None):
        try:
            question, real_section = autils.get_question_and_section(self.bank, taken_id)

            response_form = self.bank.get_response_form(assessment_section_id=real_section.ident,
                                                        item_id=question.ident)

            if 'type' not in self.data:
                # kind of a hack
                self.data['type'] = question.object_map['recordTypeIds'][0]
                self.data['type'] = self.data['type'].replace('question-record-type',
                                                              'answer-record-type')

            update_form = autils.update_response_form(self.data, response_form)
            self.bank.submit_response(real_section.ident, question.ident, update_form)
            # the above code logs the response in Mongo
            real_section = self.bank.get_assessment_section(real_section.ident)
            try:
                correct = real_section.is_correct(question.ident)
            except (AttributeError, IllegalState):
                # Now need to actually check the answers against the
                # item answers.
                answers = self.bank.get_answers(real_section.ident, question.ident)
                # compare these answers to the submitted response
                correct = autils.validate_response(self.data, answers)

            feedback = 'No feedback available.'

            # NOW (Jul 2016) this needs to happen after bank.get_answers(),
            # because you can't get answers when the taken is closed...
            # "finish" the assessment section if no more questions
            # bank.finished_assessment_section(first_section.ident)
            if not self.bank.has_next_question(real_section.ident, question.ident):
                self.bank.finish_assessment(gutils.clean_id(taken_id))

            data = {
                'isCorrect': correct,
                'feedback': feedback
            }
            # update with item solution, if available
            feedback = autils.update_json_response_with_feedback(self.bank,
                                                                 real_section,
                                                                 question.ident,
                                                                 correct)
            if feedback is not None:
                data.update({
                    'feedback': feedback
                })
            try:
                section = self.bank.get_assessment_section(real_section.ident)
                data.update({
                    'confusedLearningObjectiveIds': section.get_confused_learning_objective_ids(question.ident)
                })
            except IllegalState:
                pass

            # should send back if there are more questions, so the
            # client knows
            data['nextQuestion'] = autils.get_next_question(self.bank, real_section, question)

            # clearing here causes significant performance degredation
            # Note that the issue with getQuestions() does not seem to
            #   happen any more, perhaps with the state being saved in Redux?
            # gutils.clear_cache()  # need to do this too, otherwise the next getQuestions() call
                                  # might not get updated

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestions(DLKitSessionsManager):
    """
    Returns all of the questions for a given assessment taken.
    Assumes that only one section per assessment.
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/

    GET only
    """

    def get(self, request, bank_id, taken_id, format='json'):
        try:
            taken = self.bank.get_assessment_taken(gutils.clean_id(taken_id))
            taken._update_available_sections()
            # sections = taken._get_assessment_sections()
            # let's be consistent in the API -- why should only
            # one section be treated any differently?
            # i.e. in FbW, what if someone makes a mission with only
            # one directive ... the API should output the same format
            with_files = False
            if 'files' in self.data:
                with_files = True
            sections_data = autils.get_taken_section_map(taken,
                                                         update=True,
                                                         with_files=with_files,
                                                         bank=self.bank)
            if 'raw' in self.data:
                data = gutils.extract_items(request, sections_data, self.bank, raw=True)
            else:
                data = gutils.extract_items(request, sections_data, self.bank)
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenSections(DLKitSessionsManager):
    """
    Returns all of the sections for a given assessment taken.
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/sections/

    GET only
    """

    def get(self, request, bank_id, taken_id, format='json'):
        def object_map_with_section_id(section):
            map_ = section.object_map
            map_['id'] = str(section.ident)
            map_['type'] = 'AssessmentSection'
            return map_

        try:
            taken = self.bank.get_assessment_taken(gutils.clean_id(taken_id))
            sections = [taken._get_first_assessment_section()]
            previous_section = sections[0]
            while True:
                try:
                    next_section = taken._get_next_assessment_section(previous_section.ident)
                    sections.append(next_section)
                    previous_section = next_section
                except (StopIteration, IllegalState):
                    break

            #taken._update_available_sections()
            sections = [object_map_with_section_id(s) for s in sections]
            if 'raw' in self.data:
                data = gutils.extract_items(request, sections, self.bank, raw=True)
            else:
                data = gutils.extract_items(request, sections, self.bank)
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentSectionQuestions(DLKitSessionsManager):
    """
    Returns all of the questions for a given assessment section.
    Assumes that only one section per assessment.
    api/v2/assessment/banks/<bank_id>/assessmentsections/<section_id>/questions/

    GET only
    """

    def get(self, request, bank_id, section_id, format='json'):
        try:
            section = self.bank.get_assessment_section(gutils.clean_id(section_id))
            questions = self.bank.get_questions(section.ident)
            questions_list = []
            for question in questions:
                question_map = question.object_map
                if 'files' in self.data:
                    question_map['files'] = question.get_files()
                try:
                    if section.is_question_answered(question.ident):
                        response_map = autils.get_response_map(self.bank,
                                                               section,
                                                               question.ident)
                        question_map.update({
                            'responded': True,
                            'response': response_map,
                            'isCorrect': response_map['isCorrect']
                        })
                    else:
                        question_map.update({
                            'responded': False,
                            'response': None
                        })
                except NotFound:
                    question_map.update({
                        'responded': False,
                        'response': None
                    })
                questions_list.append(question_map)
            if 'raw' in self.data:
                data = gutils.extract_items(request, questions_list, self.bank, raw=True)
            else:
                data = gutils.extract_items(request, questions_list, self.bank)
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)



class AssessmentTakenQuestionDetails(DLKitSessionsManager):
    """
    Returns the specified question
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/

    GET only
    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            question, real_section = autils.get_question_and_section(self.bank,
                                                                     taken_id,
                                                                     question_id=gutils.clean_id(question_id))
            data = gutils.convert_dl_object(question)

            status = autils.get_question_status(self.bank,
                                                real_section,
                                                gutils.clean_id(question_id))
            data.update(status)

            gutils.update_links(request, data)

            if 'fileIds' in data:
                data['files'] = question.get_files()
            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionComments(DLKitSessionsManager):
    """
    Gets the instructor comments for this question
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/comments/

    GET, POST

    Example (for an Ortho3D manipulatable - label type):

    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            # try to get the book for this bank. If no book, create it using Jeff's work-around
            book = self.cm.get_book(gutils.clean_id(bank_id))

            # should probably use something like get_comments_for_reference(), but
            # for now, just loop through...
            comments = book.get_comments()

            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            try:
                response = self.bank.get_response(first_section.ident, gutils.clean_id(question_id))
                response.object_map
            except (NotFound, IllegalState) as ex:
                raise IllegalState(*ex.args)

            comments = [comment for comment in comments
                        if comment.get_reference_id() == response.ident]

            data = gutils.extract_items(request, comments)

            if 'files' in self.data:
                for comment in data['data']['results']:
                    if 'fileId' in comment:
                        comment_obj = book.get_comment(gutils.clean_id(comment['id']))
                        comment['file'] = comment_obj.get_file_url()

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)

    def post(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            gutils.verify_keys_present(self.data, ['text'])

            # try to get the book for this bank. If no book, create it using Jeff's work-around
            book = self.cm.get_book(gutils.clean_id(bank_id))

            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            try:
                response = self.bank.get_response(first_section.ident, gutils.clean_id(question_id))
            except (NotFound, IllegalState):
                raise IllegalState

            if 'files' in self.data:
                comment_type = [FILE_COMMENT_RECORD_TYPE]
            else:
                comment_type = []
            form = book.get_comment_form_for_create(response.ident, comment_type)

            if 'text' in self.data:
                form.set_text(self.data['text'])

            if 'files' in self.data:
                if len(self.data['files']) > 1:
                    raise ValueError('Only one file per comment')
                key = self.data['files'].keys()[0]
                form.set_file(DataInputStream(self.data['files'][key]), asset_name=key)

            new_comment = book.create_comment(form)

            data = new_comment.object_map

            # if 'files' in params:
            if 'fileId' in data:
                comment_obj = book.get_comment(gutils.clean_id(data['id']))
                data['file'] = comment_obj.get_file_url()

            return gutils.CreatedResponse(data)
        except (IllegalState, ValueError, KeyError, PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionFiles(DLKitSessionsManager):
    """
    Returns the files for the specified question
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/files/

    GET only
    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            question = self.bank.get_question(first_section.ident,
                                         gutils.clean_id(question_id))
            try:
                question_files = question.get_files()
                data = {
                    'manip': question_files['manip'],
                    'front': question_files['frontView'],
                    'side': question_files['sideView'],
                    'top': question_files['topView']
                }
            except:
                data = {'details': 'No files for this question.'}

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionResponses(DLKitSessionsManager):
    """
    Gets the student responses to a question
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/responses/

    GET only

    Example (for an Ortho3D manipulatable - label type):
        [{"integerValues": {
            "frontFaceValue" : 0,
            "sideFaceValue"  : 1,
            "topFaceValue"   : 2
        },{
            "frontFaceValue" : 3,
            "sideFaceValue"  : 1,
            "topFaceValue"   : 2
        }]
    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            first_section = self.bank.get_first_assessment_section(gutils.clean_id(taken_id))
            response = self.bank.get_response(first_section.ident, gutils.clean_id(question_id))
            data = response.object_map

            # if 'files' in params:
            if 'fileIds' in data:
                data['files'] = response.get_files()  # return files by default, until we have a list

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionSolution(DLKitSessionsManager):
    """
    Returns the solution / explanation when available
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/solution/

    GET only

    Example (for an Ortho3D manipulatable - label type):
        {}
    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            taken = self.bank.get_assessment_taken(gutils.clean_id(taken_id))
            try:
                solution = taken.get_solution_for_question(gutils.clean_id(question_id))
            except IllegalState:
                solution = 'No solution available.'

            return Response(solution)
        except (PermissionDenied, IllegalState, NotFound, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionStatus(DLKitSessionsManager):
    """
    Gets the current status of a question in a taken -- responded to or not, correct or incorrect
    response (if applicable)
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/status/

    GET only

    Example (for an Ortho3D manipulatable - label type):
        {"responded": True,
         "correct"  : False
        }
    """

    def get(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            question, real_section = autils.get_question_and_section(self.bank,
                                                                     taken_id,
                                                                     question_id=gutils.clean_id(question_id))

            data = autils.get_question_status(self.bank, real_section,
                                              gutils.clean_id(question_id))

            return Response(data)
        except (PermissionDenied, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionSubmit(DLKitSessionsManager):
    """
    Submits a student response for the specified question
    Returns correct or not
    Does NOTHING to flag if the section is done or not...
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/submit/

    POST only

    Example (for an Ortho3D manipulatable - label type):
        {"integerValues":{
                "frontFaceValue" : 0,
                "sideFaceValue"  : 1,
                "topFaceValue"   : 2
            }
        }
    """

    def post(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            question, real_section = autils.get_question_and_section(self.bank, taken_id, question_id)

            response_form = self.bank.get_response_form(assessment_section_id=real_section.ident,
                                                        item_id=question.ident)

            if isinstance(self.data, QueryDict):
                self.data = self.data.copy()
                if len(request.FILES) > 0:
                    # for some reason files are only copied as '',
                    # so need to re-set this, if sent
                    self.data['files'] = request.FILES

            if 'type' not in self.data:
                # kind of a hack
                self.data['type'] = question.object_map['recordTypeIds'][0]
                self.data['type'] = self.data['type'].replace('question-record-type',
                                                              'answer-record-type')

            update_form = autils.update_response_form(self.data, response_form)

            self.bank.submit_response(real_section.ident, question.ident, update_form)
            # the above code logs the response in Mongo
            real_section = self.bank.get_assessment_section(real_section.ident)
            try:
                correct = real_section.is_correct(question.ident)
            except (AttributeError, IllegalState):
                # Now need to actually check the answers against the
                # item answers.
                answers = self.bank.get_answers(real_section.ident, question.ident)

                # compare these answers to the submitted response
                correct = autils.validate_response(self.data, answers)

            feedback = 'No feedback available.'

            return_data = {
                'isCorrect': correct,
                'feedback': feedback
            }
            feedback = autils.update_json_response_with_feedback(self.bank,
                                                                 real_section,
                                                                 question.ident,
                                                                 correct)

            if feedback is not None:
                return_data.update({
                    'feedback': feedback
                })
            try:
                section = self.bank.get_assessment_section(real_section.ident)
                return_data.update({
                    'confusedLearningObjectiveIds': section.get_confused_learning_objective_ids(question.ident)
                })
            except IllegalState:
                pass

            # if there is a next question, add it
            return_data['nextQuestion'] = autils.get_next_question(self.bank, real_section, question)

            # clearing here causes significant performance degredation
            # Note that the issue with getQuestions() does not seem to
            #   happen any more, perhaps with the state being saved in Redux?
            # gutils.clear_cache()  # need to do this too, otherwise the next getQuestions() call
                                    # might not get updated

            return Response(return_data)
        except (PermissionDenied, IllegalState, NotFound, InvalidArgument) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionSurrender(DLKitSessionsManager):
    """
    Returns the answer if a student gives up and wants to just see the answer
    api/v2/assessment/banks/<bank_id>/assessmentstaken/<taken_id>/questions/<question_id>/surrender/

    POST only, no data

    Example (for an Ortho3D manipulatable - label type):
        {}
    """

    def post(self, request, bank_id, taken_id, question_id, format='json'):
        try:
            question, real_section = autils.get_question_and_section(self.bank, taken_id, question_id)

            response_form = self.bank.get_response_form(assessment_section_id=real_section.ident,
                                                        item_id=question.ident)

            response_form.display_name = 'I surrendered'
            self.bank.submit_response(real_section.ident, question.ident, response_form)
            # the above code logs the response in Mongo

            data = {
                'feedback': autils.update_json_response_with_feedback(self.bank,
                                                                      real_section,
                                                                      question.ident,
                                                                      False),
                'isCorrect': False,
                'nextQuestion': autils.get_next_question(self.bank, real_section, question)
            }

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class AssessmentTakenQuestionsAnswered(DLKitSessionsManager):
    """
    Returns the list of questions the student has already answered, with the
    latest response for each
    """
    def get(self, request, bank_id, taken_id, format='json'):
        try:
            question, real_section = autils.get_question_and_section(self.bank, taken_id)
            target_question = question
            previous_questions = []
            data = []
            while self.bank.has_previous_question(real_section.ident, target_question.ident):
                previous_question = self.bank.get_previous_question(real_section.ident, target_question.ident)
                previous_questions.append(previous_question)
                question_map = previous_question.object_map
                if 'fileIds' in question_map:
                    question_map.update({
                        'files': previous_question.get_files()
                    })
                question_map.update({
                    'response': autils.get_response_map(self.bank,
                                                        real_section,
                                                        previous_question.ident)
                })
                data.append(question_map)
                target_question = previous_question

            # do a reverse sort now to get them back into chronological order
            data.reverse()

            return Response(data)
        except (PermissionDenied, IllegalState, NotFound) as ex:
            gutils.handle_exceptions(ex)


class BankAuthorizations(DLKitSessionsManager):
    """
    Gets the authorizations for the given assessment bank

    GET only
    """

    def get(self, request, bank_id, format='json'):
        try:
            bank = autils.get_active_bank(request, bank_id)

            auths = {
                'assessments'       : {
                    'can_create': bank.can_author_assessments(),
                    'can_delete': bank.can_delete_assessments(),
                    'can_lookup': bank.can_lookup_assessments(),
                    'can_take'  : bank.can_take_assessments(),
                    'can_update': bank.can_update_assessments()
                },
                'assessments_offered'       : {
                    'can_create': bank.can_create_assessments_offered(),
                    'can_delete': bank.can_delete_assessments_offered(),
                    'can_lookup': bank.can_lookup_assessments_offered(),
                    'can_update': bank.can_update_assessments_offered()
                },
                'assessments_taken'       : {
                    'can_create': bank.can_create_assessments_taken(),
                    'can_delete': bank.can_delete_assessments_taken(),
                    'can_lookup': bank.can_lookup_assessments_taken(),
                    'can_update': bank.can_update_assessments_taken()
                },
                'assessment_banks'  : {
                    'can_create': self.am.can_create_banks(),
                    'can_delete': self.am.can_delete_banks(),
                    'can_lookup': self.am.can_lookup_banks(),
                    'can_update': self.am.can_update_banks()
                },
                'items'                 : {
                    'can_create': bank.can_create_items(),
                    'can_delete': bank.can_delete_items(),
                    'can_lookup': bank.can_lookup_items(),
                    'can_update': bank.can_update_items()
                }
            }

            gutils.set_session_data(request, 'bank', bank)
            return Response(auths)
        except PermissionDenied as ex:
            gutils.handle_exceptions(ex)
