import datetime
import os

from assessments_users.models import APIUser

from dlkit.runtime.primordium import Id

from urllib import unquote

from utilities import authorization as authzutils
from utilities import general as gutils
from utilities import resource as resutils
from utilities.testing import create_test_request, QBankBaseTest, add_user_authz_to_settings,\
    create_test_bank, get_super_authz_user_request, SUPER_USER_AUTHZ_GENUS



PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

NUM_AUTHZ_IN_FIXTURES = 664
NUM_ROOT_BANK_AUTHZ_IN_FIXTURES = 87


class DjangoTestCase(QBankBaseTest):
    """
    """
    def get_vault(self, req):
        return authzutils.get_vault(req)

    def set_up_authorization(self, agent_id, function_id, qualifier_id):
        vault = self.get_vault(self.req)

        if not isinstance(agent_id, Id):
            agent_id = resutils.get_agent_id(agent_id)

        if not isinstance(function_id, Id):
            function_id = Id(function_id)

        if not isinstance(qualifier_id, Id):
            qualifier_id = Id(qualifier_id)

        form = vault.get_authorization_form_for_create_for_agent(agent_id,
                                                                 function_id,
                                                                 qualifier_id,
                                                                 [])
        return vault.create_authorization(form)

    def setUp(self):
        super(DjangoTestCase, self).setUp()
        self.url = '/api/v2/authorization/'
        self.username = 'instructor@mit.edu'
        self.password = 'jinxem'
        self.user = APIUser.objects.create_user(username=self.username,
                                                password=self.password)
        self.student_name = 'student@mit.edu'
        self.student_password = 'blahblah'
        self.student = APIUser.objects.create_user(username=self.student_name,
                                                   password=self.student_password)
        self.req = create_test_request(self.user)

        gutils.activate_managers(self.req)

        self.fake_catalog_id = gutils.clean_id('assessment.Bank%3A55203f0be7dde0815228bb41%40bazzim.MIT.EDU')

        # set up instructor@mit.edu as an admin account with authz to
        # manage authorizations
        super_req = get_super_authz_user_request()
        vault = self.get_vault(super_req)
        authzutils.allow_user_to_add_authorizations(vault, self.username)

    def tearDown(self):
        super(DjangoTestCase, self).tearDown()


class BasicServiceTests(DjangoTestCase):
    """Test the views for getting the basic service calls

    """
    def setUp(self):
        super(BasicServiceTests, self).setUp()

    def tearDown(self):
        super(BasicServiceTests, self).tearDown()

    def test_authenticated_users_can_see_available_services(self):
        self.login()
        url = self.url
        req = self.get(url)
        self.ok(req)
        self.message(req, 'documentation')
        self.message(req, 'authorizations')

    def test_non_authenticated_users_cannot_see_available_services(self):
        url = self.url
        req = self.client.get(url)  # non-authenticated so don't use self.get()
        self.code(req, 403)

    def test_instructors_can_get_list_of_authorizations(self):
        url = self.url + 'authorizations/'
        req = self.get(url)
        self.ok(req)
        # Should have the 5 basic authorizations for the instructor account
        # to create / lookup / delete / search / update authorizations
        self.message(req, '"count": {0}'.format(str(5 + NUM_AUTHZ_IN_FIXTURES)))  # 548 from the fixture

    def test_learners_cannot_see_list_of_authorizations(self):
        url = self.url + 'authorizations/'
        req = self.get(url, non_instructor=True)
        self.code(req, 403)
        # self.ok(req)
        # self.message(req, '"count": 0')


class DocumentationTests(DjangoTestCase):
    """Test the views for getting the documentation

    """
    def setUp(self):
        super(DocumentationTests, self).setUp()

    def tearDown(self):
        super(DocumentationTests, self).tearDown()

    def test_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url)
        self.ok(req)
        self.message(req, 'Documentation for MIT Authorization Service, V2')

    def test_non_authenticated_users_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.client.get(url)  # don't use self.get() to be unauthenticated
        self.ok(req)
        self.message(req, 'Documentation for MIT Authorization Service, V2')

    def test_student_can_view_docs(self):
        url = self.url + 'docs/'
        req = self.get(url, non_instructor=True)
        self.ok(req)
        self.message(req, 'Documentation for MIT Authorization Service, V2')


class AuthorizationCrUDTests(DjangoTestCase):
    """Test the views for authorization crud

    """
    def num_authorizations(self, val):
        vault = self.get_vault(self.req)
        querier = vault.get_authorization_query()
        querier.match_genus_type(SUPER_USER_AUTHZ_GENUS, False)
        self.assertEqual(
            vault.get_authorizations_by_query(querier).available(),
            val + NUM_AUTHZ_IN_FIXTURES  # 548 build-in authorizations from the fixture
        )

    def setUp(self):
        super(AuthorizationCrUDTests, self).setUp()
        self.url += 'authorizations/'

    def tearDown(self):
        super(AuthorizationCrUDTests, self).tearDown()

    def test_privileged_user_can_get_authorizations(self):
        self.login()
        req = self.client.get(self.url)
        self.ok(req)
        data = self.json(req)
        # should see the five authz for the instructor
        self.assertEqual(
            data['data']['count'],
            5 + NUM_AUTHZ_IN_FIXTURES  # from the fixture
        )

    def test_unprivileged_user_cannot_get_authorizations(self):
        self.login(non_instructor=True)
        req = self.client.get(self.url)
        self.code(req, 403)

    def test_privileged_user_can_create_authorization(self):
        self.login()
        payload = {
            'agentId': self.student_name,
            'functionId': 'assessment.Item%3Alookup%40ODL.MIT.EDU',
            'qualifierId': str(self.fake_catalog_id)
        }

        self.num_authorizations(5)

        req = self.client.post(self.url,
                               data=payload,
                               format='json')
        self.created(req)
        data = self.json(req)
        self.assertEqual(
            data['functionId'],
            payload['functionId']
        )
        self.assertEqual(
            data['qualifierId'],
            payload['qualifierId']
        )
        self.assertEqual(
            data['agentId'],
            str(resutils.get_agent_id(self.student_name))
        )
        self.num_authorizations(6)

    def test_privileged_user_can_create_authorization_with_enddate(self):
        self.login()

        now = datetime.datetime.now()
        future = now + datetime.timedelta(days=180)

        payload = {
            'agentId': self.student_name,
            'endDate': {
                'year': future.year,
                'month': future.month,
                'day': future.day,
                'hour': future.hour,
                'minute': future.minute,
                'second': future.second,
                'microsecond': future.microsecond
            },
            'functionId': 'assessment.Item%3Alookup%40ODL.MIT.EDU',
            'qualifierId': str(self.fake_catalog_id)
        }

        self.num_authorizations(5)

        req = self.client.post(self.url,
                               data=payload,
                               format='json')
        self.created(req)
        data = self.json(req)
        self.assertEqual(
            data['functionId'],
            payload['functionId']
        )
        self.assertEqual(
            data['qualifierId'],
            payload['qualifierId']
        )
        self.assertEqual(
            data['agentId'],
            str(resutils.get_agent_id(self.student_name))
        )
        self.assertEqual(
            int(data['endDate']['year']),
            future.year
        )
        self.assertEqual(
            int(data['endDate']['month']),
            future.month
        )
        self.assertEqual(
            int(data['endDate']['day']),
            future.day
        )
        self.assertEqual(
            int(data['endDate']['hour']),
            future.hour
        )
        self.assertEqual(
            int(data['endDate']['minute']),
            future.minute
        )
        self.assertEqual(
            int(data['endDate']['second']),
            future.second
        )

        # I think MongoDB truncates this, so it never matches
        # self.assertEqual(
        #     int(data['endDate']['microsecond']),
        #     future.microsecond
        # )
        self.num_authorizations(6)

    def test_unprivileged_user_cannot_create_authorizations(self):
        self.login(non_instructor=True)
        payload = {
            'agentId': self.student_name,
            'functionId': 'assessment.Item%3Alookup%40ODL.MIT.EDU',
            'qualifierId': str(self.fake_catalog_id)
        }

        self.num_authorizations(5)

        req = self.client.post(self.url,
                               data=payload,
                               format='json')
        self.code(req, 403)
        self.num_authorizations(5)

    def test_privileged_user_can_delete_authorizations(self):
        self.login()
        authz = self.set_up_authorization(self.student_name,
                                          'assessment.Item%3Alookup%40ODL.MIT.EDU',
                                          self.fake_catalog_id)

        self.num_authorizations(6)

        url = '{0}{1}'.format(self.url,
                              unquote(str(authz.ident)))
        req = self.client.delete(url)
        self.deleted(req)
        self.num_authorizations(5)

    def test_unprivileged_user_cannot_delete_authorizations(self):
        self.login(non_instructor=True)
        authz = self.set_up_authorization(self.student_name,
                                          'assessment.Item%3Alookup%40ODL.MIT.EDU',
                                          self.fake_catalog_id)

        self.num_authorizations(6)

        url = '{0}{1}'.format(self.url,
                              unquote(str(authz.ident)))
        req = self.client.delete(url)
        self.code(req, 403)
        self.num_authorizations(6)

    def test_can_bulk_create_authorizations(self):
        self.login()
        payload = {
            'bulk': [
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Alookup%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Acreate%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Asearch%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Adelete%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Aupdate%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                }
            ]
        }

        self.num_authorizations(5)

        req = self.post(self.url,
                        payload)
        self.created(req)
        data = self.json(req)
        self.assertEqual(
            len(data),
            len(payload['bulk'])
        )

        functions = [f['functionId'] for f in payload['bulk']]

        for index, authz in enumerate(data):
            self.assertEqual(
                data[index]['agentId'],
                str(resutils.get_agent_id(payload['bulk'][index]['agentId']))
            )
            self.assertEqual(
                data[index]['qualifierId'],
                payload['bulk'][index]['qualifierId']
            )
            self.assertIn(
                data[index]['functionId'],
                functions
            )
            functions.remove(data[index]['functionId'])
        self.num_authorizations(5 + len(payload['bulk']))

    def test_can_bulk_delete_authorizations_by_agent_id(self):
        self.login()
        payload = {
            'bulk': [
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Alookup%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Acreate%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Asearch%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Adelete%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                },
                {
                    'agentId': self.student_name,
                    'functionId': 'assessment.Item%3Aupdate%40ODL.MIT.EDU',
                    'qualifierId': str(self.fake_catalog_id)
                }
            ]
        }
        req = self.post(self.url,
                        payload)
        self.created(req)

        req = self.client.get(self.url)
        self.ok(req)
        data = self.json(req)
        # should see the five authz for the instructor
        # plus the 5 new ones we just added
        self.assertEqual(
            data['data']['count'],
            5 + 5 + NUM_AUTHZ_IN_FIXTURES  # from the fixture
        )

        url = '{0}?agentId={1}'.format(self.url,
                                       self.student_name)
        req = self.client.delete(url)
        self.deleted(req)
        req = self.client.get(self.url)
        self.ok(req)
        data = self.json(req)
        # should not see the five authz for the instructor
        # minus the 5 we just added
        # minus the 36 that existed beforehand for students
        self.assertEqual(
            data['data']['count'],
            5 + NUM_AUTHZ_IN_FIXTURES - 36  # from the fixture
        )


class AuthorizationQueryTests(DjangoTestCase):
    """Test the views for authorization query

    """
    def setUp(self):
        super(AuthorizationQueryTests, self).setUp()
        self.authorization = self.set_up_authorization(self.student_name,
                                                       'assessment.Item%3Alookup%40ODL.MIT.EDU',
                                                       self.fake_catalog_id)
        self.url += 'authorizations/'
        self.login()

    def tearDown(self):
        super(AuthorizationQueryTests, self).tearDown()

    def test_unprivileged_users_cannot_query(self):
        url = '{0}?agentId=foo'.format(self.url)
        req = self.get(url, non_instructor=True)
        self.code(req, 403)

        url = '{0}?agentId={1}'.format(self.url,
                                       self.student_name)
        req = self.get(url, non_instructor=True)
        self.code(req, 403)

    def test_can_query_by_agent_id(self):
        url = '{0}?agentId=foo'.format(self.url)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

        url = '{0}?agentId={1}'.format(self.url,
                                       self.student_name)
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1 + 36  # 36 from the fixture
        )

    def test_can_query_by_function_id(self):
        url = '{0}?functionId={1}'.format(self.url,
                                          'assessment.Assessment%3Alookup%40ODL.MIT.EDU')
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0 + 6  # 6 from the fixture
        )
        url = '{0}?functionId={1}'.format(self.url,
                                          'assessment.Item%3Alookup%40ODL.MIT.EDU')
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1 + 6  # 6 from the fixture
        )

    def test_can_query_by_qualifier_id(self):
        url = '{0}?qualifierId={1}'.format(self.url,
                                           'assessment.Bank%3AROOT%40ODL.MIT.EDU')
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0 + NUM_ROOT_BANK_AUTHZ_IN_FIXTURES  # 81 from the fixture
        )

        url = '{0}?qualifierId={1}'.format(self.url,
                                           str(self.fake_catalog_id))
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )

    def test_can_query_by_multiple_parameters(self):
        url = '{0}?agentId=foo&functionId={1}'.format(self.url,
                                                      'assessment.Assessment%3Alookup%40ODL.MIT.EDU')
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            0
        )

        url = '{0}?agentId={1}&functionId={2}'.format(self.url,
                                                      self.student_name,
                                                      'assessment.Item%3Alookup%40ODL.MIT.EDU')
        req = self.get(url)
        self.ok(req)
        data = self.json(req)
        self.assertEqual(
            data['data']['count'],
            1
        )
