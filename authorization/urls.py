from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from authorization import views

urlpatterns = patterns('',
    url(r'^$',
        views.AuthorizationService.as_view()),
    url(r'^authorizations/?$',
        views.VaultAuthorizationsList.as_view()),
    url(r'^authorizations/(?P<authorization_id>[-.:@%\d\w]+)/?$',
        views.VaultAuthorizationDetails.as_view()),
    url(r'^docs/?$',
        views.Documentation.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)
