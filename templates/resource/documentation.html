<!DOCTYPE html>
<html lang='en' xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>Documentation for MIT Resource Service, V2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="MIT Resource Service Documentation, V2">
    <meta name="author" content="MIT ODL">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    {% load i18n %}
    {% load static %}

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.1/themes/ui-lightness/jquery-ui.min.css"/>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{ STATIC_URL }}css/vendor/jquery.tocify.css"/>
    <link rel="stylesheet" href="{{ STATIC_URL }}css/repository/style.css"/>

    <script data-main="{{ STATIC_URL }}js/main" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.11/require.min.js"></script>
</head>
<body class="portal">
    {% csrf_token %}
    <div id="docs_container" class="container-fluid">
        <div class="row-fluid">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div id="docs_nav"></div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                <div id="docs_inner_wrapper">
                    <h1>Introduction to the resource service, V2</h1>

                    <h2>Servers</h2>
                    <p>
                        We currently support three servers, a production server, a demo server,
                        and a development server. We encourage you to test your app against
                        the development server before switching to the demo / production service.
                        Note that data on the development and demo servers has no permanence or
                        guarantee against changes.
                    </p>
                    <p>
                        Development server:
                        <pre>https://qbank-dev.mit.edu/api/v2/resource/</pre>
                        Demo server:
                        <pre>https://qbank-demo.mit.edu/api/v2/resource/</pre>
                        Production server:
                        <pre>https://qbank.mit.edu/api/v2/resource/</pre>
                    </p>
                    <h2>Developer Notes</h2>
                    <p>
                        <ul>
                            <li>
                                Files will only be returned to you as expiring URLs,
                                for security reasons. If you want the original file, you
                                will have to download it yourself.
                            </li>
                            <li>You must use one of the authentication methods outlined
                            in this document. There is no guest access.</li>
                            <li>If you need a public / private keypair, contact
                            assessments-admin [at] mit [dot] edu.</li>
                        </ul>
                    </p>
                    <h1 id="authentications">Authentication</h1>
                    <h2>Introduction</h2>
                    <p class="">
                        RESTful API requires use of signature verfication via public / private key, like Amazon AWS.
                        Currently, we use a library that follows the draft IETF standards for signing HTTP requests:
                        <pre><a href="https://datatracker.ietf.org/doc/draft-cavage-http-signatures/">https://datatracker.ietf.org/doc/draft-cavage-http-signatures/</a></pre>
                        Apps that will use a signed HTTP request will need to pass in the username of the
                        user they with to authenticate. This should be in an 'X-Api-Proxy' header. The app's
                        public key should go in the 'X-Api-Key' header.
                    </p>
                    <h2>Touchstone Authentication</h2>
                    <p>
                        In a web browser, if you have been granted permission, you may
                        view an interactive version of the API. This can help with debugging
                        and testing, as it lets you POST, PUT, GET, and DELETE objects
                        within each endpoint of the service. To reach this endpoint,
                        add "/touchstone/" to the root URL of any call.
                        <pre>https://qbank-dev.mit.edu/api/v2/resource/</pre>
                        Becomes:
                        <pre>https://qbank-dev.mit.edu/touchstone/api/v2/resource/</pre>
                        <img class="doc_image" src="{{ STATIC_URL }}images/service_in_browser.png" alt="Screenshot of the API's UI in a browser."/>
                    </p>
                    <h2>Non-LTI Authentication</h2>
                    <p>
                        When signing an API request, the following headers MUST be included in the signature calculation:
                        <pre>['request-line','accept','date','host','x-api-proxy']</pre>
                        Where request-line include both method and path of the request, and x-api-proxy is the
                        kerberos username of the student / user. They MUST also appear in that
                        order. The HTTP_AUTHORIZATION header should look something like:
                        <pre>'Signature headers="request-line accept date host x-api-proxy",keyId="some-key",algorithm="hmac-sha256",signature="5CUB27wqW+oPx0ZI/xMhCaU3v/3My4JMeeP5PbWO9Xg="'</pre>
                        You can compare your hashing algorithm with the following input / output.
                        <pre>Headers     : {'Date': 'Tue, 17 Jun 2014 19:26:25 GMT', 'Host': 'testserver:80', 'X-Api-Proxy': 'cjshaw@mit.edu', 'X-Api-Key': 'afakepublicKey!23', 'Accept': 'application/json'}
Secret key  : mdnAf0vTHSU/Ap1qbzZDFblHdiepcPZZ7C7B6p98
Method      : 'GET'
Path        : '/api/v2/assessment/banks/'

Output      : 'Signature headers="request-line accept date host x-api-proxy",keyId="afakepublicKey!23",algorithm="hmac-sha256",signature="tbxfhvEDF/XNvcKiGLLklBpj8ewVVoXfTqqYVwwGV9Q="'</pre>
                        Note that the path requested must be un-encoded. Example:
                        <pre>'/api/v2/assessment/banks/assessment.Bank:53a071c7ea061a0abff13681@birdland.mit.edu/'</pre>
                        Instead of:
                        <pre>'/api/v2/assessment/banks/assessment.Bank%3A53a071c7ea061a0abff13681%40birdland.mit.edu/'</pre>
                    </p>
                    <h2>LTI Authentication</h2>
                    <p>
                        If passing in LTI student data, a second set of parameters needs to be used in the
                        signature calculation (in this order):
                        <pre>['request-line','accept','date','host','x-api-proxy','lti-user-id','lti-tool-consumer-instance-guid','lti-user-role','lti-bank']</pre>
                        Note that this is NOT needed for instructor actions -- do those with non-LTI
                        headers (as above), and make sure that the X-Api-Proxy user is a staff / instructor
                        in the Assessment Service.
                    </p>
                    <h3>Example Workflows</h3>
                    <p>
                        <p>You can see an example workflow from an LTI provider for an instructor
                            <a href="http://ds5yycsj1acmu.cloudfront.net/docs/media/LTI_provider_workflow_instructor.pdf">here</a>.
                        </p>
                        <p>You can see an example workflow from an LTI provider for a learner
                            <a href="http://ds5yycsj1acmu.cloudfront.net/docs/media/LTI_provider_workflow_learner.pdf">here</a>.
                        </p>
                    </p>
                    <h1>Bins</h1>
                    <h2>Introduction</h2>
                    <p>
                        Everything in the service is relative to a Bin.
                        Resources live inside of bins. The identifier part of each assessment /
                        bin ID will correlate (i.e. the bin for assessment
                        bank <code>assessment.Bank%3A5547c37cea061a6d3f0ffe71%40cs-macbook-pro</code>
                        will have ID <code>resource.Bin%3A5547c37cea061a6d3f0ffe71%40cs-macbook-pro</code>).
                    </p>
                    <h2>Authorizations</h2>
                    <p>
                        Authorizations are managed at the bin and resource level.
                        A person can be given authorizations to specific actions
                        inside of a bin, such as looking up resources, or
                        creating resources. Typically, these are restricted to faculty. All
                        students can look up their own resource, once it has been assigned.
                    </p>
                    <h2>Data Pagination</h2>
                    <p>
                        Note that all "LIST" type endpoints (bins, resources) will return
                        a list in the "data['results']" attribute, as well as a paginated
                        set of data. "next" and "previous" links are included for your
                        convenience. To get all data in a single page, you would append
                        <code>?page=all</code> to your request URL. To get a specific page,
                        you can include <code>?page=1</code>, substituting the desired
                        page number. Examples are below, in each section. Note that by
                        default, lists are returned in descending chronological order
                        (newest record first).
                    </p>
                        For "LIST" endpoints, by default the service does not
                        include file URLs for avatar files. This is to optimize performance,
                        because the file-signing process for AWS can take quite long for
                        a large list of items (and is rate-limited).
                        If you, as a developer, decide to get all the
                        files up front with the list, you can include a <code>?files</code>
                        parameter in your request.
                    </p>
                    <p>
                        Any "DETAILS" type endpoints
                        will return the attributes directly in the object. For example,
                        the bin list below shows two bins in the
                        "data" attribute. Below, we will see that individual bin
                        details are embedded directly in a return object.
                    </p>
                    <h2 id="binsList">Listing all bins</h2>
                    <p>
                        <pre>GET /api/v2/resource/bins/</pre>
                        A list of all resource bins on the server is returned.
                        Where possible, the service will always return a "_links" attribute
                        to indicate what other information is available.
                        <pre>{
    "data": {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "displayName": {
                    "text": "Crosslinks resources",
                    "languageTypeId": "639-2%3AENG%40ISO",
                    "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
                    "scriptTypeId": "15924%3ALATN%40ISO"
                },
                "description": {
                    "text": "for storing avatars and profiles",
                    "languageTypeId": "639-2%3AENG%40ISO",
                    "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
                    "scriptTypeId": "15924%3ALATN%40ISO"
                },
                "recordTypeIds": [],
                "genusType": "GenusType%3ADEFAULT%40dlkit.mit.edu",
                "type": "Bin",
                "id": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU",
                "_link": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin:555e340133bb727a2cd6a51a@oki-dev.MIT.EDU/"
            }
        ]
    },
    "_links": {
        "self": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/"
    }
}</pre>
                    </p>
                    <h3>Error Codes</h3>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to view bins with this service. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <h2>Creating</h2>
                    <p>
                        <pre>POST /api/v2/resource/bins/</pre>
                        A simple JSON structure needs to be sent to this endpoint, with
                        the bank name and description.
                        <pre>{"name" : "a new bin","description" : "this is a test"}</pre>
                        The new bin structure is returned.
                        <pre>{
    "displayName": {
        "text": "Crosslinks resources",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "description": {
        "text": "for storing avatars and profiles",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "recordTypeIds": [],
    "genusType": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "type": "Bin",
    "id": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU",
}</pre>
                    </p>
                    <p>
                        If you want to orchestrate the ID (where the central identifier matches
                        that of an assessment bank or repository), just pass in a <code>bankId</code>
                        parameter, like so:
                        <pre>{"name" : "a new bin",
    "description" : "this is a test",
    "bankId": "assessment.Bank%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU"
}</pre>
                    </p>
                    <h2>Managing a specific bin</h2>
                    <p>
                        Once you have a <code>bin_id</code>, you can view and
                        manipulate the specific bin.
                    </p>
                    <h3>Viewing</h3>
                    <p>
                        <pre>GET /api/v2/resource/bins/&lt;bin_id&gt;/</pre>
                        This returns the bank details. Note that the data is
                        embedded directly in the returned object, not
                        in a list or as a child of another attribute.
                        <pre>{
    "displayName": {
        "text": "Crosslinks resources",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "description": {
        "text": "for storing avatars and profiles",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "recordTypeIds": [],
    "genusType": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "_links": {
        "self": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU/",
        "resources": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU/resources/"
    },
    "type": "Bin",
    "id": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU"
}</pre>
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to view individual bins with this service. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>500: {"details": "Object not found"}</pre>
                        A bin with the specified <code>bin_id</code> was not
                        found. Please verify it exists in the service through the
                        <a href="#binsList">bins</a> endpoint.
                    </p>
                    <h3>Deleting</h3>
                    <p>
                        <pre>DELETE /api/v2/resource/bins/&lt;bin_id&gt;/</pre>
                        An empty, 204 response is returned when this is successful.
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to delete this bank with this service. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>406: {"details": "Bin is not empty."}</pre>
                        This bin has resources in it. You need to delete those first,
                        before you can delete the entire bin.
                    </p>
                    <h3>Updating</h3>
                    <p>
                        <pre>PUT /api/v2/resource/bins/&lt;bin_id&gt;/</pre>
                        Include the parameter that you wish to change.
                        <pre>{"name" : "My new bin"}</pre>
                        The updated bank structure is returned:
                        <pre>{
    "displayName": {
        "text": "My new bin",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "description": {
        "text": "for storing avatars and profiles",
        "languageTypeId": "639-2%3AENG%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net",
        "scriptTypeId": "15924%3ALATN%40ISO"
    },
    "recordTypeIds": [],
    "genusType": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "type": "Bin",
    "id": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU"
}</pre>
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to edit the attributes of the bin with this service. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>500: {"details": "Poorly formatted input data."}</pre>
                        Your input data is not in the expected format. Most likely
                        it is not proper JSON or lacks the proper content-type header.
                    </p>
                    <h1>Resources</h1>
                    <h2>Introduction</h2>
                    <p class="">
                        Resources have three editable attributes:
                        <ul>
                            <li>Name -- text field</li>
                            <li>Description -- text field</li>
                            <li>Avatar -- image file</li>
                        </ul>
                    </p>
                    <h2 id="resourcesList">Listing all resources in a bin</h2>
                    <p>
                        Because resources must belong to a bin, it is assumed that
                        your app will know the <code>bin_id</code> of interest.
                        <pre>GET /api/v2/resource/bins/&lt;bin_id&gt;/resources/</pre>
                        Since this is a LIST type call, resources are returned in the
                        "data" attribute.
                        <pre>{
    "data": {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "binId": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU",
                "group": null,
                "description": {
                    "text": "",
                    "languageTypeId": "639-2%3AENG%40ISO",
                    "scriptTypeId": "15924%3ALATN%40ISO",
                    "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
                },
                "recordTypeIds": [],
                "type": "Resource",
                "_link": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin:555e340133bb727a2cd6a51a@oki-dev.MIT.EDU/resources/resource.Resource:55677d6f33bb72f9137259e6@oki-dev.MIT.EDU/",
                "genusTypeId": "GenusType%3ADEFAULT%40dlkit.mit.edu",
                "displayName": {
                    "text": "mine",
                    "languageTypeId": "639-2%3AENG%40ISO",
                    "scriptTypeId": "15924%3ALATN%40ISO",
                    "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
                },
                "avatarId": "",
                "id": "resource.Resource%3A55677d6f33bb72f9137259e6%40oki-dev.MIT.EDU"
            }
        ]
    },
    "_links": {
        "self": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU/resources/"
    }
}</pre>
                    </p>
                    <p>
                        Note that including the <code>?avatar_urls</code> flag in the URL will
                        include an <code>avatarURL</code> field, if available:
                        <pre>{
    "binId": "resource.Bin%3A55686998ea061a1ea9ad4fb9%40cs-macbook-pro",
    "group": null,
    "description": {
        "text": "foobar",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "recordTypeIds": [],
    "avatarURL": "https://d1v4o60a4yrgi8.cloudfront.net/55686998ea061a1ea9ad4fb9/ps_2015_beam_2gages_1432906136.pdf?Expires=1432906437&Signature=NApihZN5eOySEw0b81~mcdxuaSMV3KN96kXxWDOOJM0fuwUN~vZPysIWsjlFCzxt9EZRZFYwwaJsG5ElbMMhT950SXRWGdfPhrf4jkkga8Cyi5tFeHX2oCuEqV7nzLuJeZu-9Tmf1BSN3-dzQCQveZt9YW3zkBPrGUJJlINfTBD01c9GLq7TVaSMe8mUvfw1IX~Zzn7yX1nqnsF6h3hSBCoBUKMt42R3odnxuaewhTm89yl8RLfu5OeG3769PMp9dijTToMOYFZvrzlIuVlkn8e6M-ervTDUYmEcWHBHUaug8bE4Rsj~W~9UhCINVdnYEZ7N3KLDkgP2GCt2Dc5Nfw__&Key-Pair-Id=APKAIGRK7FPIAJR675NA",
    "avatarId": "repository.Asset%3A55686998ea061a1ea9ad4fba%40cs-macbook-pro",
    "genusTypeId": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "displayName": {
        "text": "astudent",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "type": "Resource",
    "id": "resource.Resource%3A55686999ea061a1ea9ad4fbc%40cs-macbook-pro"
}</pre>
                    </p>
                    <h3>Error Codes</h3>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to view resources in this bin. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <h2>Creating</h2>
                    <p>
                        <pre>POST /api/v2/resource/bins/&lt;bin_id&gt;/resources/</pre>
                        To create a new resource, you need to supply at least one of the two base
                        attributes: name, or description. You can also include an avatar file.
                        <pre>{
    "name": "John Doe",
    "description": "Class of 2020"
}
Files: avatar = me.jpg</pre>
                    </p>
                    <h3>Error Codes</h3>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to create resources in this bin. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>500: {"details": "Poorly formatted input data."}</pre>
                        Your input data is not in the expected format. Most likely
                        it is not proper JSON.
                    </p>
                    <p>
                        <pre>500: {"details": "At least one of the following must be passed in: [\\"name\\", \\"description\\"]"}</pre>
                        You need to supply the missing <code><key></code> parameter in your
                        input parameters.
                    </p>
                    <h2>Managing a specific resource</h2>
                    <p>
                        Once you have a <code>bin_id</code> and an <code>resource_id</code>,
                        you can view and manipulate the specific resource.
                    </p>
                    <h3>Viewing</h3>
                    <p>
                        <pre>GET /api/v2/resource/bins/&lt;bin_id&gt;/resources/&lt;resource_id&gt;/</pre>
                        This returns the resource details.
                        <pre>{
    "binId": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU",
    "group": null,
    "description": {
        "text": "",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "recordTypeIds": [],
    "type": "Resource",
    "_links": {
        "self": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU/resources/resource.Resource%3A55677d6f33bb72f9137259e6%40oki-dev.MIT.EDU/"
    },
    "genusTypeId": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "displayName": {
        "text": "mine",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "avatarId": "",
    "id": "resource.Resource%3A55677d6f33bb72f9137259e6%40oki-dev.MIT.EDU"
}</pre>
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to view resource details for the specific bin. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>500: {"details": "Object not found"}</pre>
                        A resource with the specified <code>resource_id</code> (or a bin with its
                        <code>bin_id</code>) was not
                        found. Please verify it exists in the current bin through the
                        <a href="#resourcesList">resources</a> endpoint (or the
                        <a href="#bsList">bins</a> endpoint).
                    </p>
                    <h3>Deleting</h3>
                    <p>
                        <pre>DELETE /api/v2/resource/bins/&lt;bin_id&gt;/resources/&lt;resource_id&gt;/</pre>
                        An empty, 204 response is returned when this is successful.
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to delete resources in this bin. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <h3>Updating</h3>
                    <p>
                        <pre>PUT /api/v2/resource/bins/&lt;bin_id&gt;/resources/&lt;resource_id&gt;/</pre>
                        Include the parameter that you wish to change.
                        <pre>{
    "name": "John Doe II"
}</pre>
                        The updated item is returned. Note that students can only update their
                        own resource!
                        <pre>{
    "binId": "resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU",
    "group": null,
    "description": {
        "text": "",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "recordTypeIds": [],
    "type": "Resource",
    "_links": {
        "self": "https://qbank-dev.mit.edu/touchstone/api/v2/resource/bins/resource.Bin%3A555e340133bb727a2cd6a51a%40oki-dev.MIT.EDU/resources/resource.Resource%3A55677d6f33bb72f9137259e6%40oki-dev.MIT.EDU/"
    },
    "genusTypeId": "GenusType%3ADEFAULT%40dlkit.mit.edu",
    "displayName": {
        "text": "John Doe II",
        "languageTypeId": "639-2%3AENG%40ISO",
        "scriptTypeId": "15924%3ALATN%40ISO",
        "formatTypeId": "TextFormats%3APLAIN%40okapia.net"
    },
    "avatarId": "",
    "id": "resource.Resource%3A55677d6f33bb72f9137259e6%40oki-dev.MIT.EDU"
}</pre>
                    </p>
                    <h4>Error Codes</h4>
                    <p>
                        <pre>403: {"details": "Permission denied."}</pre>
                        You have authenticated to the server, but your username is not configured
                        to edit resources in the bin. Please contact an administrator.
                    </p>
                    <p>
                        <pre>404: {"details": "Not found"}</pre>
                        This usually indicates a problem with the server-side view.
                        Contact an administrator.
                    </p>
                    <p>
                        <pre>500: {"details": "Poorly formatted input data."}</pre>
                        Your input data is not in the expected format. Most likely
                        it is not proper JSON.
                    </p>
                    <h1>Help</h1>
                    <h2>Contact</h2>
                    Please reach out to assessments-admin [at] mit [dot] edu if you have
                    any issues or questions about this service.
                    <h6>v0.2 June 2015</h6>
                </div>
            </div>
        </div>
    </div>
</body>
</html>